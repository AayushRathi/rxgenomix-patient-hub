<?php

//configuration for CakeDC user plugin

return [
    'Users' => [
        'auth' => true,
        'controller' => 'RxgUsers.RxgUsers',
        'Registration' => [
            // determines if the register is enabled
            'active' => true,
            'reCaptcha' => false,
            // default role name used in registration
            'defaultRole' => 'user',
        ],
        'RememberMe' => [
            'active' => false
        ],
        'table' => 'RxgUsers.RxgUsers',
        'Profile' => [
            'route' => [
                'plugin' => 'RxgUsers',
                'controller' => 'RxgUsers',
                'action' => 'profile',
                'prefix' => false
            ]
        ],
        'Email' => [
            'mailerClass' => 'RxgUsers.RxgUsers'
        ],
        'Token' => ['expiration' => 7200] //seconds
    ],
    'Auth' => [
        'authorize' => ['CakeDC/Auth.SimpleRbac'],
        'authenticate' => [
            'all' => [
                'finder' => 'auth',
                'contain' => [
                    'LabOrderInitiatorTypes',
                    'Provider',
                ]
            ],
            'Form' => [
                'fields' => [
                    'username' => 'email',
                    'password' => 'password'
                ]
            ],
        ],
        'loginAction' => [
            'plugin' => 'RxgTheme',
            'controller' => 'HomePage',
            'prefix' => false,
            'action' => 'home'
        ],
        'loginRedirect' => [
            'plugin' => 'LabOrders',
            'controller' => 'LabOrders',
            'prefix' => false,
            'action' => 'index'
        ],
        'logoutRedirect' => [
            'plugin' => 'RxgUsers',
            'controller' => 'RxgUsers',
            'prefix' => false,
            'action' => 'login'
        ],
    ]
];
