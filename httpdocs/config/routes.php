<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * API routes
     */
    $routes->prefix('api', function ($routes) {
        $routes->connect('/login', ['plugin' => 'SystemManagement', 'controller' => 'ApiAuthentication', 'action' => 'login', 'allowWithoutToken' => true]);
        $routes->connect('/lab_orders', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
        $routes->connect('/lab_orders/:id', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'view']);
        $routes->connect('/reports/:id', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'results']);
        $routes->connect('/member_reports', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'member_reports']);
        $routes->connect('/qc', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'qc']);
        $routes->connect('/*', ['plugin' => 'SystemManagement', 'controller' => 'Api', 'allowWithoutToken' => true]);
    });
    // ajax routes
    $routes->connect('/patients/get_patient_by_date_of_birth', ['plugin' => 'Patients', 'controller' => 'Patients', 'action' => 'get_patient_by_date_of_birth']);


    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
        'secure' => true,
    ]));

    /**
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
     */
    $routes->applyMiddleware('csrf');

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    #$routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    /**
     * ...and the middleware user management parts
     */
    $routes->connect('/register', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'register']);
    $routes->connect('/login', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'login']);
    $routes->connect('/logout', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'logout']);
    $routes->connect('/profile', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'profile']);
    $routes->connect('/profile/edit', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'edit']);
    $routes->connect('/resetPassword', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'resetPassword']);
    $routes->connect('/changePassword', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'changePassword']);
    $routes->connect('/requestResetPassword', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'requestResetPassword']);
    $routes->connect('/validateEmail', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'validateEmail']);
    $routes->connect('/accountCreated', ['plugin' => 'RxgUsers', 'controller' => 'RxgUsers', 'action' => 'accountCreated']);

    /**
     * Globaly accessible content pages
     */
    $routes->connect('/privacy', ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'privacy']);
    $routes->connect('/consent', ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'consent']);
    $routes->connect('/tos', ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'tos']);
    $routes->connect('/npp', ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'natureOfPrivacy']);

    /**
     * Lab orders section
     */
    $routes->connect('/lab_orders', ['plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $routes->connect('/patients/:action', ['plugin' => 'Patients', 'controller' => 'Patients']);


    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});
