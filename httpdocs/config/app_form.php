<?php

return [
    // Used for button elements in button().
    'button' => '<button{{attrs}}>{{text}}</button>',
    // Used for checkboxes in checkbox() and multiCheckbox().
    'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}> ',
    // Input group wrapper for checkboxes created via control().
    'checkboxFormGroup' => '{{label}}',
    // Wrapper container for checkboxes.
    'checkboxWrapper' => '<div class="checkbox">{{label}}</div>',
    // Widget ordering for date/time/datetime pickers.
    'dateWidget' => '{{year}}{{month}}{{day}}{{hour}}{{minute}}{{second}}{{meridian}}',
    // Error message wrapper elements.
    'error' => '<div class="error-message">{{content}}</div>',
    // Container for error items.
    'errorList' => '<ul>{{content}}</ul>',
    // Error item wrapper.
    'errorItem' => '<li>{{text}}</li>',
    // File input used by file().
    'file' => '<input type="file" name="{{name}}"{{attrs}}>',
    // Fieldset element used by allControls().
    'fieldset' => '<fieldset{{attrs}}>{{content}}</fieldset>',
    // Open tag used by create().
    'formStart' => '<form{{attrs}}>',
    // Close tag used by end().
    'formEnd' => '</form>',
    // General grouping container for control(). Defines input/label ordering.
    'formGroup' => '{{label}}{{input}}',
    // Wrapper content used to hide other content.
    'hiddenBlock' => '<div style="display:none;">{{content}}</div>',
    // Generic input element.
    'input' => '<input type="{{type}}" class="form-control" name="{{name}}"{{attrs}}/>',
    // Submit input element.
    'inputSubmit' => '<input type="{{type}}"{{attrs}}/>',
    // Container element used by control().
    'inputContainer' => '<div class="input {{type}}{{required}}">{{content}}</div>',
    // Container element used by control() when a field has an error.
    'inputContainerError' => '<div class="input {{type}}{{required}} error">{{content}}{{error}}</div>',
    // Label element when inputs are not nested inside the label.
    'label' => '<label{{attrs}}>{{text}}</label>',
    // Label element used for radio and multi-checkbox inputs.
    'nestingLabel' => '{{hidden}}<label{{attrs}}>{{input}}{{text}}</label>',
    // Legends created by allControls()
    'legend' => '<legend>{{text}}</legend>',
    // Multi-Checkbox input set title element.
    'multicheckboxTitle' => '<legend>{{text}}</legend>',
    // Multi-Checkbox wrapping container.
    'multicheckboxWrapper' => '<fieldset{{attrs}}>{{content}}</fieldset>',
    // Option element used in select pickers.
    'option' => '<option value="{{value}}"{{attrs}}>{{text}}</option>',
    // Option group element used in select pickers.
    'optgroup' => '<optgroup label="{{label}}"{{attrs}}>{{content}}</optgroup>',
    // Select element,
    'select' => '<select name="{{name}}" class="form-control" {{attrs}}>{{content}}</select>',
    // Multi-select element,
    'selectMultiple' => '<select name="{{name}}[]" multiple="multiple"{{attrs}}>{{content}}</select>',
    // Radio input element,
    'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
    // Wrapping container for radio input/label,
    'radioWrapper' => '{{label}}',
    // Textarea input element,
    'textarea' => '<textarea name="{{name}}"{{attrs}}>{{value}}</textarea>',
    // Container for submit buttons.
    'submitContainer' => '<div class="submit">{{content}}</div>',
    //Confirm javascript template for postLink()
    'confirmJs' => '{{confirm}}',
    'dateTimePickerWidget' => '<input type="text" {{attrs}} name="{{name}}" value="" class="form-control lib24watch-datepicker">',
    // multi-input widget
    'multiInputWrapper' => '
            <div class="col-md-12"> 
                <div class="multiinput-list" name="{{mname}}" id="{{id}}" data-remove-last="{{removeLast}}" {{miattrs}}>
                    {{rows}}
                </div> 
            </div>',
    'multiInputRow' => '
            <div class="row multiinput-list-item" data-idx="{{midx}}" data-name="{{mname}}" >
                <div class="{{rowContentWrapperClass}}">
                        {{content}}
                </div>
                <div class="col-md-3 plus-minus-container">
                    {{multiInputPlus}}
                    {{multiInputMinus}}
                </div>
            </div>
    ',
    // Note for Plus and Minus template:
    //  {{mname}} will take on the value of the ID of $data['id'] if that was provided.
    //  Please see MultiInputWidget.php for more information.
    'multiInputPlus' => '
        <button class="btn btn-default btn-condensed mi-plus-button" id="add-multi-input-{{mname}}-{{midx}}" data-rowidx="{{midx}}" type="button" {{plusattrs}}>
            <i class="fa fa-plus"></i>
        </button>
    ',
    'multiInputMinus' => '
        <button class="btn btn-danger btn-condensed mi-minus-button" id="remove-multi-input-{{mname}}-{{midx}}"  data-rowidx="{{midx}}" type="button" {{minusattrs}}>
            <i class="fa fa-minus"></i>
        </button>
    ',
    'multiInputWrapperInternal1' => '
        <div class="mi-input-container {{requiredFirst}}" data-inputidx="{{inputIdxFirst}}">
            <span>{{beforeFirst}}</span>
            <div>{{inputFirst}}</div>
            <span class="after-block">{{afterFirst}}</span>
        </div>
    ',
    'multiInputWrapperInternal2' => '
        <div class="row">
            <div class="col-sm-5 col-md-6 mi-input-container nopadding-left {{requiredFirst}}" data-inputidx="{{inputIdxFirst}}">
                <span class="before-block">{{beforeFirst}}</span>
                <div>{{inputFirst}}</div>
                <span class="after-block">{{afterFirst}}</span>
            </div>
            <div class="col-sm-5 col-sm-offset-2 col-md-6 col-md-offset-0 mi-input-container {{requiredSecond}}" data-inputidx="{{inputIdxSecond}}">
                <span class="before-block">{{beforeSecond}}</span>
                <div>{{inputSecond}}</div>
                <span class="after-block">{{afterSecond}}</span>
            </div>
        </div>
    ',
    'multiInputWrapperInternal3' => '
        <div class="row">
            <div class="col-md-4 mi-input-container {{requiredFirst}}" data-inputidx="{{inputIdxFirst}}">
                <span class="before-block">{{beforeFirst}}</span>
                <div>{{inputFirst}}</div>
                <span class="after-block">{{afterFirst}}</span>
            </div>
            <div class="col-md-4 mi-input-container {{requiredSecond}}" data-inputidx="{{inputIdxSecond}}">
                <span class="before-block">{{beforeSecond}}</span>
                <div>{{inputSecond}}</div>
                <span class="after-block">{{afterSecond}}</span>
            </div>
            <div class="col-md-4 mi-input-container {{requiredThird}}" data-inputidx="{{inputIdxThird}}">
                <span class="before-block">{{beforeThird}}</span>
                <div>{{inputThird}}</div>
                <span class="after-block">{{afterThird}}</span>
            </div>
        </div>
    ',
    'multiInputWrapperInternal4' => '
        <div class="row">
            <div class="col-xs-6 col-sm-3 mi-input-container {{requiredFirst}}" data-inputidx="{{inputIdxFirst}}">
                <span class="before-block">{{beforeFirst}}</span>
                <div>{{inputFirst}}</div>
                <span class="after-block">{{afterFirst}}</span>
            </div>
            <div class="col-xs-6 col-sm-3 mi-input-container {{requiredSecond}}" data-inputidx="{{inputIdxSecond}}">
                <span class="before-block">{{beforeSecond}}</span>
                <div>{{inputSecond}}</div>
                <span class="after-block">{{afterSecond}}</span>
            </div>
            <div class="col-xs-6 col-sm-3 mi-input-container {{requiredThird}}" data-inputidx="{{inputIdxThird}}">
                <span class="before-block">{{beforeThird}}</span>
                <div>{{inputThird}}</div>
                <span class="after-block">{{afterThird}}</span>
            </div>
            <div class="col-xs-6 col-sm-3 mi-input-container {{requiredFourth}}" data-inputidx="{{inputIdxFourth}}">
                <span class="before-block">{{beforeFourth}}</span>
                <div>{{inputFourth}}</div>
                <span class="after-block">{{afterFourth}}</span>
            </div>
        </div>
    ',
    'multiInputInput' =>
        '<div class="{{type}}">
            <span class="before-block">{{before}}</span>
            <input type="{{type}}" {{attrs}} name="{{name}}" value="{{val}}" class="form-control">
            <span class="after-block">{{after}}</span>
            <span class="help-block">{{help}}</span>
            {{characters}}
         </div>',
    'multiInputCheckbox' =>
        '<div class="checkbox">
                <span class="before-block">{{before}}</span>
                <label for="{{for}}">{{text}}</label>
                <input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}/>
                <span class="after-block">{{after}}</span>
                <span class="help-block">{{help}}</span>
                {{characters}}
         </div>',
    'multiInputNestingLabel' => '
        <div class="col-md-12">{{input}}<label{{attrs}}>{{text}}</label></div>
    ',
    'multiInputAutocompleteWidget' => '
        <div class="form-group autocomplete-widget {{type}} {{required}}" {{attrs}}>
            {{autocompleteInput}}
            {{callbackField}}
            {{error}}
        </div>
    ',
    'multiInputFileUpload' => '
        <div class="col-md-9">
            <span class="before-block">{{before}}</span>
            {{image}}
            <span class="title">{{title}}</span>
            {{remove}}
            <div class="btn btn-danger btn-file">
                <span class="hidden-xs button-name">{{buttonName}}</span>
                <input type="file" name="{{name}}" value="{{value}}" title="{{buttonName}}" class="fileinput btn btn-danger"/>
            </div>
            <span class="after-block">{{after}}</span>
            <span class="help-block">{{help}}</span>
            {{characters}}
        </div>
    ',
    'lonelyMultiInputLabel' => '<label {{attrs}}>{{text}}</label>',
    'multiInputInputContainer' =>
        '<div class="form-group {{type}} {{required}}">
            {{content}}
            {{customError}}
        </div>',
    'autocompleteWidget' => '<div class="autocomplete-widget {{type}} {{required}}" {{attrs}}>
                    <label class="control-label">{{label}}</label> {{autocompleteInput}} {{callbackField}} {{error}}
                </div>',
    'timepickerWidget' => '<div class="input-group time">
                                    <span class="before-block">{{before}}</span>
                                    <input type="text" {{attrs}} name="{{name}}" value="" class="form-control lib24watch-timepicker">
                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-time"></span></span>
                                    <span class="after-block">{{after}}</span>
                                </div>
                                <span class="help-block">{{help}}</span>',
];
