CREATE TABLE `rxgenomix_db`.`sexes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`lab_order_statuses` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`provider_statuses` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`provider_types` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`ethnicities` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`discount_types` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`terms_of_service_types` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`email_alert_types` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`health_care_client_groups` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `health_care_client_id` INT UNSIGNED NULL,
  `parent_id` INT NULL,
  `title` VARCHAR(255) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `display_order` INT NULL DEFAULT 0,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`health_care_clients` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NULL,
  `lab_id` INT UNSIGNED NULL,
  `pharmacogenomics_provider_id` INT UNSIGNED NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`lab_orders` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` INT UNSIGNED NULL,
  `provider_id` INT UNSIGNED NULL,
  `lab_order_status_id` INT UNSIGNED NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`providers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rxg_user_id` CHAR(39) NULL,
  `provider_type_id` INT UNSIGNED NULL,
  `provider_type_other` VARCHAR(255) NULL,
  `health_care_client_id` INT UNSIGNED NULL,
  `npi_number` INT NULL,
  `provider_license_id` INT UNSIGNED NULL,
  `provider_lib24watch_state_abbr` CHAR(2) NULL,
  `facility_name` VARCHAR(255) NULL,
  `nabpep_id` INT NULL,
  `lib24watch_file_id` INT UNSIGNED NULL,
  `email` VARCHAR(255) NULL,
  `phone` VARCHAR(255) NULL,
  `address_1` VARCHAR(255) NULL,
  `address_2` VARCHAR(255) NULL,
  `city` VARCHAR(255) NULL,
  `lib24watch_state_abbr` CHAR(2) NULL,
  `postal_code` VARCHAR(10) NULL,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `provider_status_id` INT UNSIGNED NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`patients` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `rxg_user_id` CHAR(39) NULL,
  `first_name` VARCHAR(255) NULL,
  `last_name` VARCHAR(255) NULL,
  `ethnicity_id` INT UNSIGNED NULL,
  `sex_id` INT UNSIGNED NULL,
  `date_of_birth` DATE NULL,
  `email` VARCHAR(255) NULL,
  `phone` VARCHAR(45) NULL,
  `primary_care_provider_name` VARCHAR(255) NULL,
  `primary_care_provider_npi_number` INT NULL,
  `facility_name` VARCHAR(255) NULL,
  `facility_npi_number` INT NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`patient_medications` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` INT UNSIGNED NULL,
  `genedose_api_drug_id` INT UNSIGNED NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`lab_order_medications` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `lab_order_id` INT UNSIGNED NULL,
  `genedose_api_drug_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`promo_codes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  `discount_type_id` INT UNSIGNED NULL,
  `amount` DECIMAL(12,2) NULL DEFAULT 0,
  `is_active` TINYINT(1) NULL DEFAULT 0,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`terms_of_services` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `terms_of_service_type_id` INT UNSIGNED NULL,
  `description` TEXT NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`terms_of_service_checkboxes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `terms_of_service_id` INT UNSIGNED NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `rxgenomix_db`.`email_alerts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email_alert_type_id` INT UNSIGNED NULL,
  `subject` TEXT NULL,
  `body` TEXT NULL,
  `created` TIMESTAMP NULL,
  `modified` TIMESTAMP NULL,
  `created_by_lib24watch_user_id` INT UNSIGNED NULL,
  `modified_by_lib24watch_user_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`));
