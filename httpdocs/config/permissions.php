<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

return [
    'CakeDC/Auth.permissions' => [
        [
            'role' => ['user'],
            'prefix' => '*',
            'plugin' => 'RxgUsers',
            'controller' => 'RxgUsers',
            'action' => [
                'profile',
                'logout',
                'invite',
                'edit_profile',
                'edit',
            ],
        ],
        [
            'role' => ['user'],
            'prefix' => '*',
            'plugin' => 'LabOrders',
            'controller' => 'LabOrders',
            'action' => [
                'index',
                'view',
                'add',
                'edit',
                'tos',
                'promo',
                'patient',
                'provider',
                'medications',
                'shipping',
                'payment',
                'review',
                'confirmation',
                'complete',
                'download',
                'downloadMap',
                'reviewMap',
                'getRiskScore',
                'getMapProblemRationales',
                'addMedicationToLabOrder',
                'getMedicationInfo',
                'saveMapResultItem',
                'deleteMedicationResult',
                'select',
                'covid',
            ],
        ],
        [
            'role' => ['user'],
            'prefix' => '*',
            'plugin' => 'Patients',
            'controller' => 'Patients',
            'action' => [
                'getPatientByDateOfBirth'
            ],
        ],
        //specific actions allowed for the all roles in Users plugin
        [
            'role' => '*',
            'prefix' => '*',
            'plugin' => 'RxgUsers',
            'controller' => 'RxgUsers',
            'action' => [
                'register',
                'resetPassword',
                'validateEmail',
                'login',
                'logout',
                'accountCreated',
                'loginAs'
            ],
        ],
        [
            'role' => '*',
            'prefix' => '*',
            'plugin' => 'RxgTheme',
            'controller' => 'HomePage',
            'action' => [
                'home',
                'tos',
                'privacy',
                'consent',
                'natureOfPrivacy'
            ],
        ]
    ]
];
