<?php
return [
    'nestingLabel' => '<div>{{input}}<label{{attrs}}>{{text}}</label></div>',
    'input' => '<div class="{{type}}">
                    <span class="before-block">{{before}}</span>
                    <input type="{{type}}" {{attrs}} name="{{name}}" class="form-control">
                    <span class="after-block">{{after}}</span>
                    <span class="help-block">{{help}}</span>
                    {{characters}}
                </div>',
    'inputContainer' => '<div class="input {{type}} {{required}} user-form-container">{{content}}</div>',
    'inputContainerError' => '<div class="input {{type}} {{required}} user-form-container error">{{content}}{{error}}</div>',
    'checkboxContainer' => '<div class="input {{type}} {{required}} user-form-container inline-label">{{content}}</div>',
    'text' => '<div class="{{type}}">
                    <input type="{{type}}" {{attrs}} name="{{name}}" class="form-control">
                    {{characters}}
                </div>',
    'autocompleteWidget' => '<div class="user-form-container autocomplete-widget {{type}} {{required}}" {{attrs}}>
                    <label class="control-label">{{label}}</label> {{autocompleteInput}} {{callbackField}} {{error}}
                </div>',
    'formGroup' => '{{label}}{{input}}',
    'select' => '<div>
                    <span class="before-block">{{before}}</span>
                    <select name="{{name}}" {{attrs}} class="form-control">
                        {{content}}
                    </select>
                    <span class="after-block">{{after}}</span>
                    <span class="help-block">{{help}}</span>
                </div>',
    'dateTimePickerWidget' => '<input type="text" {{attrs}} name="{{name}}" value="" class="form-control lib24watch-datepicker">',
];
