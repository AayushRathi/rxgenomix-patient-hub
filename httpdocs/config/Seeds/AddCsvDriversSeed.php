<?php
use Migrations\AbstractSeed;

/**
 * AddCsvDrivers seed.
 */
class AddCsvDriversSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'CSV Crestar',
                'class' => 'App\Drivers\CsvCrestar',
                'is_active' => 1,
                'display_order' => 3,
            ],
            [
                'title' => 'CSV Resolve',
                'class' => 'App\Drivers\CsvResolve',
                'is_active' => 1,
                'display_order' => 4,
            ],
        ];

        $table = $this->table('drivers');
        $table->insert($data)->save();
    }
}
