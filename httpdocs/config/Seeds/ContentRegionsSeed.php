<?php
use Migrations\AbstractSeed;

/**
 * ContentRegions seed.
 */
class ContentRegionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'region' => 'account_approved_html',
                'lib24watch_locale' => NULL,
                'title' => 'account_approved_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p><strong>Congratulations! Your registration for the RxGenomix Hub has been approved by RxGenomix.</strong> You can now create new lab orders, access existing orders, manage your account and start putting the power of pharmacogenomics to work for everyone who counts on you. If you need help getting started or have any questions about the Hub, please contact us at <a href="mailto:support@rxgenomix.com">support@rxgenomix.com</a>.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 14:08:23',
                'modified' => '2019-09-18 18:34:16',
            ],
            [
                'id' => '2',
                'region' => 'tos',
                'lib24watch_locale' => NULL,
                'title' => 'tos',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Thank You for Your interest in RxGenomix, LLC (&ldquo;RxGenomix&rdquo;). At RxGenomix, we are engaged in the business of medication therapy management and related information technology services, primarily focused on bringing pharmacogenomics into clinical practice. RxGenomix has agreements with laboratory service providers to perform pharmacogenomics testing services that may help providers make more informed, individualized treatment decisions through our pharmacogenomic test combined with clinical decision support (&ldquo;RxGenomix Test&rdquo; or the &ldquo;Test&rdquo;).&nbsp; The Test is physician-ordered only. Your personal physician may order the Test for You or where appropriate, RxGenomix may assist You in accessing the services of a physician made available to You. Patients provide a cheek swab sample using the RxGenomix test kit (&ldquo;Kit&rdquo;), then RxGenomix works with a partner laboratory to run the Test and provides the ordering physician through our network of clinical pharmacists with a test report accessible on the RxGenomix HUB portal system (collectively the &ldquo;Services&rdquo; or &ldquo;Service&rdquo;) and are governed by the following terms of service (&ldquo;Terms&rdquo; or &ldquo;Terms of Service&rdquo;).&nbsp;</p>

<p>PLEASE READ THESE TERMS OF SERVICE CAREFULLY. YOU, MEANING YOURSELF OR ANY DELEGATE OR INDIVIDUAL AUTHORIZED TO ACT ON YOUR BEHALF OR AN ENTITY WITH WHICH YOU ARE AFFILIATED (collectively &ldquo;YOU&rdquo; or &ldquo;You&rdquo;), ACKNOWLEDGE THAT EACH TIME YOU USE THE SERVICE, ACCESS AND/OR BROWSE THIS WEBSITE OR ITS CONTENT THAT:&nbsp;</p>

<p>YOU HAVE READ AND UNDERSTAND ALL OF THESE TERMS AND CONDITIONS;&nbsp;</p>

<p>YOU AGREE TO BE BOUND BY ALL OF THESE TERMS OF SERVICE;&nbsp;</p>

<p>THESE TERMS OF SERVICE ARE THE LEGAL EQUIVALENT OF A SIGNED, WRITTEN CONTRACT BETWEEN YOU AND RXGENOMIX; AND&nbsp;</p>

<p>YOU HAVE FULL AUTHORITY TO BIND YOUR ENTITY TO ALL OF THESE TERMS OF SERVICE. IF YOU ARE NOT WILLING TO BE OR CANNOT BE BOUND BY ALL OF THESE TERMS OF SERVICE, INCLUDING WITHOUT LIMITATION THE PRECEDING AGREEMENT AND ACKNOWLEDGEMENT, THEN DO NOT ACCESS, BROWSE OR USE THIS WEBSITE OR ANY OF ITS CONTENT. MOREOVER, RXGENOMIX DOES NOT AND WILL NOT GRANT YOU ANY RIGHT OR LICENSE TO ACCESS, BROWSE OR USE THIS WEBSITE OR ITS CONTENT WITHOUT YOUR WILLINGNESS AND ABILITY TO BE BOUND BY ALL OF THE TERMS OF SERVICE OF THIS AGREEMENT.</p>

<p>WEBSITE</p>

<p>RxGenomix, LLC maintains the RxGenomix Websites (www.rxgenomix.com and hub.rxgenomix.com &quot;Website&quot;). The Website has been designed to provide general information about RxGenomix and its products and services. By using and or visiting the Website, You acknowledge that You have read, understood, and agree to be bound by the Terms, including our Privacy Policy (which is also available on the Website (www.RxGenomix.com/privacy). IF YOU DO NOT AGREE TO THESE TERMS, DO NOT USE THE WEBSITE.</p>

<p>The information on the Website is provided solely on an &quot;AS IS&quot;, &quot;WHERE IS&quot;, and &quot;AS AVAILABLE&quot; basis. RxGenomix is not engaged in rendering investment or medical advice via this Website, nor does it provide instruction on the appropriate use of products produced, supplied or under development by RxGenomix, its affiliates, related companies or its licensors or joint venture partners. Access and use of the Website is subject to the terms and conditions set forth herein and all applicable laws, statutes and/or regulations. RxGenomix does not, by or through this website, offer for sale any products or services directly to consumers. Tests available for requisition by RxGenomix are diagnostic tests that can only be ordered by an authorized healthcare provider.&nbsp;</p>

<p>PROPRIETARY RIGHTS</p>

<p>The Service is owned and operated by RxGenomix. The visual interfaces, graphics, design, compilation, information, data, computer code (including source code or object code), bioinformatics pipeline, Kit and all other elements of the Service (&ldquo;Materials&rdquo;) provided by RxGenomix are protected by all relevant intellectual property and proprietary rights and applicable laws. All Materials contained in the Service are the property of RxGenomix or our third-party licensors. Except as expressly authorized by RxGenomix, You may not make any other further use of the Materials. RxGenomix reserves all rights to the Materials not granted expressly in these Terms.&nbsp;</p>

<p>ELIGIBILITY</p>

<p>You must be at least eighteen (18) years of age or have a parent or guardian&rsquo;s permission for individuals under the age of eighteen (18) to receive a Kit or submit a Test. You must be at least thirteen (13) years of age or have a parent or guardian&rsquo;s permission for individuals under the age of thirteen (13) to access the Site. By agreeing to these Terms, You represent and warrant: (i) that You are at least thirteen (13) years of age or have parental/guardian permission to access the Site; (ii) that, You are at least 18 years of age or have a parental/guardian permission to provide a cheek swab sample; and (iii) that the Test was ordered for You by a physician licensed to do so in Your state.&nbsp;</p>

<p>PAYMENT</p>

<p>You may be required to pay a fee to receive Services. All fees are in U.S. Dollars. If You decide that You no longer want the Service, You will receive a refund on the purchasing card, if applicable, less a $35 cancellation fee to cover the cost of the Kit if the Kit has already been shipped to You. We will be unable to process a refund once our lab has received Your sample and the processing has begun.&nbsp;</p>

<p>INDEMNITY</p>

<p>You agree that You will be responsible for Your use of the Service, and You agree to defend, indemnify, and hold RxGenomix harmless and its officers, directors, employees, consultants, affiliates, subsidiaries, and agents (collectively &ldquo;RxGenomix Entities&rdquo;) from and against any and all claims, liabilities, damages, losses and expenses, including reasonable attorneys&#39; fees and costs, arising out of or in any way connected with (i) Your access to, use of, or alleged use of the Service, or (ii) Your violation of these Terms or any representation, warranty or agreements referenced herein, or any applicable law or regulation. We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by You (and without limiting Your indemnification obligations with respect to such matter), and in such case, You agree to cooperate with our defense of such claim.&nbsp;</p>

<p>In using this Site, You agree NOT to:</p>

<p>Send or otherwise transmit to or through this Website or to us through email any confidential information without proper authorization to disclose and/or unlawful, infringing, harmful, harassing, defamatory, threatening, hateful or otherwise objectionable material of any kind, any material that can cause harm or delay to this Website or computers of any kind, and any unsolicited advertising, solicitation or promotional materials;</p>

<p>Misrepresent Your identity or affiliation in any way;</p>

<p>Use this Website to disclose or obtain another&#39;s personal information, or collect information about users of this Website;</p>

<p>Gain unauthorized access to this Website, or assist others to gain unauthorized access, or to disclose users&#39; names or personally identifiable information, or to gain or provide unauthorized access to other computers or Websites connected or linked to this Website;</p>

<p>Launch or use any automated system, including without limitation, &quot;robots,&quot; &quot;spiders,&quot; or &quot;offline readers,&quot; that access this Website in a manner that sends more request messages to our servers in a given period of time than a human can reasonably produce in the same period by using a conventional web browser;</p>

<p>Send or otherwise transmit to or through this Website or to us through email chain letters, unsolicited messages, so-called &quot;spamming&quot; or &quot;phishing&quot; messages, messages marketing or advertising goods and services;</p>

<p>Transmit or otherwise make available any virus, worm, spyware or any other computer code, file or program that may or is intended to damage or hijack the operation of any hardware, software or telecommunications equipment;</p>

<p>Violate any applicable laws or regulations or these Terms;</p>

<p>Assist or permit any persons in engaging in any of the activities described above.</p>

<p>USER IDS AND PASSWORDS</p>

<p>Access to certain areas of the Website are limited by a user identifier (&quot;User ID&quot;) and password, which are selected and/or supplied as part of the registering for a service account with RxGenomix (&quot;RxGenomix Account&quot;). By registering, You represent, warrant and covenant that: (i) You are at least 18 years of age or You are the parent or guardian of a minor; (ii) You are using Your actual identity; (iii) You have provided only true, accurate, current and complete information about You during the registration process; and (iv) You will maintain and promptly update the information that You provide to keep it true, accurate, current and complete.&nbsp;</p>

<p>By logging onto the Website using any password, You represent, warrant and covenant that You are authorized to use such password and to engage in the activities that You conduct thereunder. You agree that You will be solely responsible for the maintenance and security of Your User ID and password. You also agree that You will be solely responsible for any activities conducted on or through this the Website. User IDs and passwords are personal and unique to each individual user. Users agree not to transfer a User ID and password to any third party except as may be required for a healthcare professional to act for or advise a user in consideration of the treatment, advice or care a user may receive.&nbsp;</p>

<p>RxGenomix reserves the right to deny or revoke access to the Website, or any part thereof, at any time in our sole discretion, with or without cause. If You wish to cancel a password, or if You become aware of any loss, theft or unauthorized use of a password, please notify RxGenomix immediately.&nbsp;</p>

<p>NO WARRANTIES</p>

<p>THE SERVICE, ALL MATERIALS AND/OR RELATED CONTENT INCLUDED IN THE SERVICE ARE PROVIDED ON AN &quot;AS IS,&quot; &quot;WHERE IS&quot;, AND &quot;AS AVAILABLE&quot; BASIS, WITHOUT WARRANTY OR CONDITION OF ANY KIND, EITHER EXPRESS OR IMPLIED. RXGENOMIX AND ANY OF ITS AFFILIATES SPECIFICALLY (BUT WITHOUT LIMITATION) DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, RELATING TO THE SERVICE, ALL MATERIALS AND/OR CONTENT AVAILABLE FOR, THROUGH OR IN CONNECTION WITH THE SERVICE, INCLUDING, BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.&nbsp;</p>

<p>NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE SERVICE, ANY MATERIALS AND/OR CONTENT AVAILABLE FOR, THROUGH OR IN CONNECTION WITH THE SERVICE WILL CREATE ANY WARRANTY REGARDING ANY OF THE RXGENOMIX ENTITIES, AS DEFINED ABOVE, OR THE SERVICE THAT IS NOT EXPRESSLY STATED IN THESE TERMS. YOU ASSUME ALL RISK FOR ALL DAMAGES THAT MAY RESULT FROM USE OF OR ACCESS TO THE SERVICE. ADDITIONALLY, BY USING THIS SERVICE YOU ACKNOWLEDGE THAT SERVICES RENDERED WILL BE BASED ON INFORMATION PROVIDED BY YOU AND THAT YOU ARE FULLY RESPONSIBLE FOR THE ACCURACY OF THIS INFORMATION. YOU AGREE THAT RXGENOMIX HAS NO OBLIGATION TO ENSURE THE CONFIDENTIALITY OR PRIVACY OF INFORMATION THAT YOU SEND INDEPENDENTLY TO YOUR HEALTHCARE PROVIDER.&nbsp;</p>

<p>SOME JURISDICTIONS MAY PROHIBIT A DISCLAIMER OF WARRANTIES AND YOU MAY HAVE OTHER RIGHTS THAT VARY FROM JURISDICTION TO JURISDICTION. IN SUCH EVENT AND ONLY TO THE EXTENT THAT A DISCLAIMER OF WARRANTIES DOES NOT APPLY TO YOU WILL THIS TERM BE AFFECTED. THE SAME WILL NOT INVALIDATE ANY OTHER TERM OR CONDITION CONTAINED IN THE TERMS FOR SERVICE WHICH SHALL OTHERWISE REMAIN IN FULL FORCE AND EFFECT.&nbsp;</p>

<p>LIMITATION OF LIABILITY</p>

<p>IN NO EVENT WILL RXGENOMIX ENTITIES BE LIABLE TO YOU FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES ARISING OUT OF OR RELATING TO ACCESS TO, USE OF, OR AN INABILITY TO ACCESS OR USE, THE SERVICE, OR ANY MATERIALS OR CONTENT ON THE SERVICE, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), STATUTE OR ANY OTHER LEGAL THEORY, WHETHER OR NOT THE ENTITIES HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE. YOU AGREE THAT THE AGGREGATE LIABILITY OF THE RXGENOMIX ENTITIES FOR ANY AND ALL CLAIMS ARISING OUT OF RELATING TO THE USE OF OR ANY INABILITY TO USE THE SERVICE (INCLUDING ANY MATERIALS OR CONTENT AVAILABLE THROUGH THE SERVICE), OR OTHERWISE UNDER THESE TERMS, WHETHER IN CONTRACT, TORT OR OTHERWISE, IS LIMITED TO THE GREATER OF (i) THE PRICE YOU PAID FOR THE SERVICE GIVING RISE TO LIABILITY OR (ii) $50 USD.&nbsp;</p>

<p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES. ACCORDINGLY, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IN SUCH EVENT AND ONLY TO THE EXTENT THAT AN EXCLUSION OR LIMITATION OF LIABILITY DOES NOT APPLY TO YOU WILL THIS TERM BE AFFECTED. THE SAME WILL NOT INVALIDATE ANY OTHER TERM OR CONDITION CONTAINED IN THE TERMS FOR SERVICE WHICH SHALL OTHERWISE REMAIN IN FULL FORCE AND EFFECT.&nbsp;</p>

<p>DISPUTE RESOLUTION AND ARBITRATION</p>

<p>Except for any disputes relating to intellectual property rights, obligations, or any infringement claims, any disputes with RxGenomix arising out of or relating to the Terms (&quot;Disputes&quot;) shall be governed by Tennessee law regardless of Your state or country of origin or where You access RxGenomix, and notwithstanding of any conflicts of law principles and the United Nations Convention for the International Sale of Goods. Any Disputes shall be resolved by final and binding arbitration under the rules and auspices of the American Arbitration Association, to be held in Franklin, Tennessee, in English, with a written decision stating legal reasoning issued by the arbitrator(s) at either party&#39;s request, and with arbitration costs and reasonable documented attorneys&#39; costs of both parties to be borne by the party that ultimately loses. Either party may obtain injunctive relief (preliminary or permanent) and orders to compel arbitration or enforce arbitral awards in any court of competent jurisdiction.&nbsp;</p>

<p>This Site is controlled and operated by RxGenomix, LLC. RxGenomix makes no representation that materials in the Website are appropriate or available for use in all locations worldwide. In all cases, You are solely responsible for compliance with local laws, if and to the extent local laws are applicable. Access to this Website from jurisdictions where the contents of this Website are illegal or penalized is prohibited.</p>

<p>INTELLECTUAL PROPERTY</p>

<p>It is the policy of RxGenomix to enforce its intellectual property rights to the fullest extent permitted by law. All product names, regardless of whether or not they appear in large print or with a trademark symbol, are the trademarks and service marks of RxGenomix, its affiliates, related companies or its licensors or joint venture partners, unless otherwise noted. All content of the Website, including any images or text, is protected by U.S. and foreign copyright laws and may not be distributed, downloaded, modified, reused, reposted or otherwise used except that users may view, use and download a single copy of this Website for a user&rsquo;s personal-informational, non-commercial use. Except as provided herein, no part of any content or software on this Website may be copied, downloaded or stored in a retrieval system for any other purpose, nor may it be redistributed for any purpose, without the express written permission of RxGenomix. The use or misuse of RxGenomix&rsquo;s trademarks, service marks copyrights or other materials, except as permitted herein, is expressly prohibited and may be a violation of copyright law, trademark law, communications regulations and statutes and other laws, statutes and/or regulations.</p>

<p>COLLECTION AND FURTHER USE OF YOUR INFORMATION</p>

<p>We may monitor the use of this Service by collecting and then de-identifying information. Data collection may occur through the use of cookies (www.RxGenomix.com/privacy). We may alternatively collect information about the number of page views for this Website or to click-throughs to each web page as well as the originating domain name of a user&rsquo;s Internet Service Provider. This information may be used to understand the visitor&#39;s use of the Website and Services. We will have no means reasonably available to us to ascertain the identity of individual users from aggregated information.&nbsp;</p>

<p>In order to obtain Services, You may voluntarily submit sensitive personal information, for example, information relating to Your health or Your personal or family medical history. You are not required to provide this information and, if You do not want to do so, You may simply not use this Service. By providing us with this information, however, You consent to us processing Your sensitive personal data for the purpose of providing Your Results, and for any other purpose contemplated by the Service (www.RxGenomix.com/privacy).&nbsp;</p>

<p>In the event that legal process requires, You give us permission to disclose personal data/information in order to comply with a legal or regulatory obligation.&nbsp;</p>

<p>We will keep Your data only for as long as is reasonably needed for the purposes set forth in these Terms of Service and in accordance with any applicable legal or ethical reporting or documentation retention requirements.&nbsp;</p>

<p>TERMINATION</p>

<p>RxGenomix may terminate use of this Website or any of our features or Services at any time and for any reason without notice for conduct violating these Terms. Upon any such termination, You must destroy all content obtained from this Website and all copies thereof. The provisions of these Terms concerning or applicable to You, as well as Website security, prohibited activities, copyrights, trademarks, user submissions, disclaimers, limitations of liability, indemnities and jurisdictional issues in connection with You or for which You may be involved shall survive any such termination. If use of this Website is terminated pursuant to these Terms, You will not attempt to use this Website under any name, real or assumed, and further agree that if You violate this restriction after being terminated, You indemnify and hold us harmless from any and all liability that we may incur therefore.</p>

<p>FORWARD-LOOKING STATEMENTS</p>

<p>This Website may contain forward-looking information. Such information is subject to a variety of significant uncertainties, including scientific, business, economic and financial factors, and therefore actual results may differ significantly from those presented.</p>

<p>GENERAL</p>

<p>Links to third-party websites or pages are provided for convenience only. We do not express any opinion on the content of any third-party pages and expressly disclaim liability for all third-party information and the use of it.&nbsp;</p>

<p>RxGenomix&rsquo;s Privacy Policy (&quot;Policy&quot;), as it may change from time to time, is a part of these terms and conditions for using the Website and is incorporated herein by this reference (www.RxGenomix.com/privacy). RxGenomix reserves the right, and You authorize RxGenomix, to the use and assignment of all information regarding Your use of this Website and all information provided by You in any manner consistent with RxGenomix&rsquo;s Privacy Statement.&nbsp;</p>

<p>UPDATES</p>

<p>RxGenomix may revise or modify these Terms from time-to-time. Such changes, revisions or modifications shall be effective immediately. It is incumbent upon You to be current with the terms of the Website. If You disagree with the Terms, Your sole remedy is to discontinue Your use of this Website. Any use of this Website by You after shall be deemed to constitute acceptance of the Terms as may be revised from time-to-time.&nbsp;</p>

<p>RxGenomix will use reasonable efforts to include accurate and up-to-date information on the Website, however, any information presented on the Website as of a particular date may only be accurate as of the date written below and RxGenomix disclaims any responsibility to update such information. Information about companies other than RxGenomix contained in news, press releases or otherwise that are posted on the Website should not be relied upon as being provided or endorsed by RxGenomix.&nbsp;</p>

<p>You must exercise caution, good sense and sound judgment in using this Website. You are prohibited from violating, or attempting to violate, the security of this Website. Any such violations may result in criminal and/or civil penalties against You. RxGenomix will investigate any alleged or suspected violations and if a criminal violation is suspected, we will cooperate with law enforcement agencies in their investigations.&nbsp;</p>

<p>SEVERABILITY</p>

<p>If any term or condition in these Terms of Service is or becomes illegal, invalid or unenforceable in any jurisdiction, that shall not affect the validity or enforceability in that jurisdiction of any other provision of these terms and conditions; or the validity or enforceability in other jurisdictions of that or any other provision of the terms and conditions contained in the Terms of Service.</p>

<p>ENTIRE AGREEMENT</p>

<p>You acknowledge that these Terms, together with the Terms &amp; Conditions, represent the entire agreement between You and RxGenomix with respect to the Service, and that no other representations or promises, verbal or otherwise, will affect these Terms of Service.&nbsp;</p>

<p>CONTACT INFORMATION</p>

<p>If You have any questions about these Terms, please let us know by calling (1-615-814-2911), emailing support@RxGenomix.com, or sending us a message using the contact form on our Support page.</p>

<p>PAGE UPDATED AS OF: September 16, 2019</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 15:25:09',
                'modified' => '2019-09-18 15:25:26',
            ],
            [
                'id' => '3',
                'region' => 'login_title_html',
                'lib24watch_locale' => NULL,
                'title' => 'login_title_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p style="float: right; text-align: right;">Trouble logging in?<br />
Contact Us at <a href="mailto:support@rxgenomix.com">support@rxgenomix.com</a></p>

<h2>Welcome to the RxGenomix Hub</h2>

<p>The pharmacogenomics service engine. Registered users log in below.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:32:36',
                'modified' => '2019-09-18 18:40:42',
            ],
            [
                'id' => '4',
                'region' => 'login_join_hub_html',
                'lib24watch_locale' => NULL,
                'title' => 'login_join_hub_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p><strong>Curious about how to become part of the RxGenomix Hub?</strong> Contact us at info@rxgenomix.com today.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:35:05',
                'modified' => '2019-09-18 18:37:37',
            ],
            [
                'id' => '5',
                'region' => 'login_register_html',
                'lib24watch_locale' => NULL,
                'title' => 'login_register_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p><strong>New to the RxGenomix Hub?</strong> Register for an account here. Applications will be reviewed by RxGenomix staff.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:38:11',
                'modified' => '2019-09-18 18:39:32',
            ],
            [
                'id' => '6',
                'region' => 'email_validated',
                'lib24watch_locale' => NULL,
                'title' => 'email_validated',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Success! Your information has been saved, your account has been validated and an RxGenomix specialist will review your information shortly. Your registration will need to be approved before any lab orders can be processed by the lab, but you can go ahead and enter the information to save new orders and more. As soon as your registration is approved, any saved orders will be converted to active orders and sent to the laboratory for processing.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:42:08',
                'modified' => '2019-09-18 18:42:22',
            ],
            [
                'id' => '7',
                'region' => 'register_required_fields_html',
                'lib24watch_locale' => NULL,
                'title' => 'register_required_fields_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Please fill out all fields possible. All fields marked with an <span style="color:#FF0000;"><strong>*</strong></span> are required.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:44:02',
                'modified' => '2019-09-18 18:44:26',
            ],
            [
                'id' => '8',
                'region' => 'account_denied_html',
                'lib24watch_locale' => NULL,
                'title' => 'account_denied_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Sorry, but we must be missing something and your registration has been denied. If we have questions, an RxGenomix specialist will be in touch soon and we&rsquo;ll use the contact information you entered. If you have questions or feel there&rsquo;s been an error, please contact us at <a href="mailto:support@rxgenomix.com">support@rxgenomix.com</a>.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:45:10',
                'modified' => '2019-09-18 18:47:06',
            ],
            [
                'id' => '9',
                'region' => 'account_pending_html',
                'lib24watch_locale' => NULL,
                'title' => 'account_pending_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Thanks for submitting your info. Your registration for the RxGenomix Hub will be reviewed by an RxGenomix specialist shortly. In the meantime, you can go ahead and save lab orders*, access any orders you have already saved, and manage your account. We&rsquo;ll be in touch shortly.&nbsp;</p>

<p><em>*Any saved lab orders will be immediately sent to the laboratory when your account is approved.</em></p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:48:32',
                'modified' => '2019-09-18 18:48:55',
            ],
            [
                'id' => '10',
                'region' => 'register_group_name_info_html',
                'lib24watch_locale' => NULL,
                'title' => 'register_group_name_info_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>This is your primary company name, e.g. John&rsquo;s Pharmacy or American Healthcare Co.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:49:27',
                'modified' => '2019-09-18 18:50:24',
            ],
            [
                'id' => '11',
                'region' => 'register_group_id_info_html',
                'lib24watch_locale' => NULL,
                'title' => 'register_group_id_info_html',
                'content_region_type' => 'html',
                'image_required_width' => NULL,
                'image_required_height' => NULL,
                'body' => '<p>Add any unique identifier you use for your clients/client groups.</p>
',
                'image_filename' => NULL,
                'image_filesize' => NULL,
                'image_mime_type' => NULL,
                'image_data' => NULL,
                'created_by_lib24watch_user_id' => '2',
                'modified_by_lib24watch_user_id' => '2',
                'show_on_template_home_page' => NULL,
                'created' => '2019-09-18 18:50:44',
                'modified' => '2019-09-18 18:50:56',
            ],
        ];

        $table = $this->table('content_regions');
        $table->insert($data)->save();
    }
}
