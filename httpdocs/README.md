#### 2021-07-23 Updated

# RxGenomix HUB

A platform that connects Labs to Pharmacogenomic providers to create individualized treatment and risk plans for patients.

## Environments

### BETA / Sprint & Review

#### Sprint
*For in sprint development*

**URL:** https://rxg-sprint.orases.com

**SVN:** https://svn.orases.com/rxgenomix/branches/sprint

#### Review
*For QA review while developer starts on next sprint*

**URL:** https://rxg-review.orases.com

**SVN:** https://svn.orases.com/rxgenomix/branches/review

#### Beta
*For hot fixes and pre-launch end to end testing*

**URL:** https://rxg-beta.orases.com

**SVN:** https://svn.orases.com/rxgenomix/trunk

### Production

**URL:** https://hub.rxgenomix.com

**SVN:** https://svn.orases.com/rxgenomix/trunk

## Lab Order Types

### PGX

The basis for the platform, a genetic test that provides risks for gene to drug interactions.

[Workflow Diagram](https://drive.google.com/file/d/1Ci5tgpjbInVTyDqiBoPaD7jgb2g8qRbk/view?usp=sharing)


### Covid

In 2020 RxG added a Covid testing workflow to the system. These results do not go to the PGX provider, the PDF result comes directly from the Lab that completed the test.

## Hub API

The Hub has an API that is used for various actions, it leverages OAuth2 Token issuance for security.
Management of API users and permissions is located here: https://hub.rxgenomix.com/admin/system_management/api_authentications

### Endpoints
* Security (Authorization and Token return)
* Lab Orders (List, and single item, NO creation)
* Reports (Generic endpoint, not used)
* Member Reports (Returns severity information based on passed client_id, card_id, person_code, and dob **Only used by Benecard**)
* QC (Used by Labs **ONLY** to approve or deny a report, the final check before a report is visible to the provider)

### Management
API users and permissions are managed via [Hub UI](https://hub.rxgenomix.com/admin/system_management/api_authentications)

API does minimal logging (login and error at the moment) and can be found [here](https://hub.rxgenomix.com/admin/system_management/api_logs)

## Events
As lab order process through the system they change "status", each one of these status changes is considered an event.

### Event logs
Global set of event logs
[Event Logs](https://hub.rxgenomix.com/admin/system_management/event_logs)

### Manually Triggering Events
Since the system depends on 3rd parties we build a basic section of the tool to manually trigger different actions the system normally doesn't automatically.

[Event Manager](https://hub.rxgenomix.com/admin/system_management/event_manager)

## Drug Mapping

In order to support a number of different providers the Hub has a complex system of mapping drugs via NDC 9 (what the reports and member reports API endpoint uses), RXCUI, and TSI's internal drug list.

## Connector / Driver Abstraction
In order to future proof the Hub and allow easier integrations with additional Labs and PGX providers, the way the hub connects and sends / receives data is abstracted out into:
1. Connectors - The method in which the hub "connects" to the 3rd party in order to send/receive data.
1. Drivers - The format of the data to send/receive.

### Currently supported Connectors
1. FTP
1. SFTP
1. REST

### Currently supported Drivers
1. HL7 (v2)
1. JSON (generic)
1. CSV (Crestar Specific)
1. CSV (Resolve Specific)
1. JSON (TSI Specific)

## Hosting

The site is hosted through Cloudnexa due to HIPAA requirements.

### Production
Access to production via SSH must be done via the Bastion host (18.234.79.201).

The MOTD (Message of the Day) on the bastion host lists the current production IP addresses (not this is manually managed by Orases, if you cannot connect to one of the IP addresses listed in the MOTD contact Mike / Meder to update).

Auto-scaling is configured and Cloudnexa patches instances every 2 weeks (Sunday 11pm EST).

### Cron Jobs

```bash
# RxG - Get results from Labs
*/25 * * * * /var/www/rxg/httpdocs/bin/cake checkForResults production

# RxG - Email Notification - Expired License Pharmacists #36953
0 12 * * * /var/www/rxg/httpdocs/bin/cake CheckExpiredLicensePharmacists production

# RxG - Email Notification - Inactive Pharmacists #36950
0 11 * * * /var/www/rxg/httpdocs/bin/cake CheckInactivePharmacists production

# RxG - Email Notification - Report Ready #36951
*/2 * * * * /var/www/rxg/httpdocs/bin/cake ReportReady production

# RxG - Queue (for jobs such as event manager reprocess lab orders)
*/2  *  *  *  *  cd /var/www/rxg/httpdocs && bin/cake queue runworker -q production

# RxG - Email Notification - Report Unviewed #36954
0 14 * * * /var/www/rxg/httpdocs/bin/cake ReportUnviewed production
```
