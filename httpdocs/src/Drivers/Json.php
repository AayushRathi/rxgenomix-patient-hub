<?php

namespace App\Drivers;

class Json extends Driver
{
    /**
     * @param $message
     * @return false|mixed|string
     */
    public function loadMessage($message)
    {
        $this->rawMessage = $message;

        $this->message = json_encode($message);
        return $this->message;
    }

    /**
     * @param \Cake\ORM\Entity $labOrderEntity
     * @return mixed
     * @throws \Exception
     */
    public function createLabOrder(\Cake\ORM\Entity $labOrderEntity)
    {
        $labOrderArray = $labOrderEntity->toArray();
        // TODO: Reformat as needed here

        return $this->loadMessage($labOrderArray);
    }

    public function createSubmit(\Cake\ORM\Entity $labOrderEntity)
    {
        $data = [];

        $data['sample']['sampleId'] = $labOrderEntity->sample_id;
        //$data['sample']['policy'] = 'strict';
        $date = date('Y-m-d');
        if ($labOrderEntity->created) {
            $date = lib24watchDate('%Y-%m-%d', $labOrderEntity->created, +5);
        }

        $npi = $clinician = '';
        foreach ($labOrderEntity->providers as $provider) {
            if ($provider->is_ordering_physician) {
                $npi = $provider->npi_number;
                $clinician = $provider->provider_name;
            }
        }


        $data['sample']['metadata'] = [
            'refver' => '2',
            'clinicianname' => $clinician,
            'npi' => $npi,
            'panel' => 'CRESTAR_COMP',
            'labTechnology' => 'PGX120',
            'labname' => 'Crestar',
            'specimen' => '',
            'datapolicy' => 'PGX120',
            'collected' => $date,
            'labreceived' => $date,
            'labprocessed' => $date,
        ];

        $data['sample']['patient'] = [
            'sn' => $labOrderEntity->patient->last_name,
            'givenName' => $labOrderEntity->patient->first_name,
            'sex' => $labOrderEntity->patient->sex->title,
            'dob' => lib24watchDate('%Y-%m-%d', $labOrderEntity->patient->date_of_birth, +5)
        ];

        $drugsArray = [];
        foreach ($labOrderEntity->medications as $medication) {
            $drugsArray[$medication->drug_id] = 'C';
        }
        if (!empty($drugsArray)) {
            $data['sample']['patient']['regimen'] = $drugsArray;
        }

        $snpsArray = [];
        foreach ($labOrderEntity->patient->snps as $snps) {
            $snpsArray[$snps->key] = $snps->value;
        }
        if (!empty($snpsArray)) {
            $data['sample']['snps'] = $snpsArray;
        }

        $geneCnvsArray = [];
        foreach ($labOrderEntity->patient->gene_cnvs as $cnvs) {
            $geneCnvsArray[$cnvs->key] = $cnvs->value;
        }
        if (!empty($geneCnvsArray)) {
            $data['sample']['geneCnvs'] = $geneCnvsArray;
        }

        return $this->loadMessage($data);
    }

    public function createReport(\Cake\ORM\Entity $labOrderEntity)
    {
        $medicationIds = [];
        foreach ((array)$labOrderEntity->medications as $medication) {
            $medicationIds[] = $medication->drug_id;
        }
        /*$lifestyleIds = [];
        foreach ((array)$labOrderEntity->patient->lifestyles as $lifestyle) {
            $lifestyleIds[] = $lifestyle->lifestyle_id;
        }*/
        $medicationIds  = array_unique(array_filter($medicationIds));
        $data = [
            'pgxid' => $labOrderEntity->uuid,
            'productIds' => $medicationIds,
            //'lifestyles' => $lifestyleIds
        ];

        return $this->loadMessage($data);
    }

    public function createPostProcess(array $data = null)
    {
        return $this->loadMessage($data);
    }
}
