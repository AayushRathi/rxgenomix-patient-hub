<?php

namespace App\Drivers;

use Cake\ORM\TableRegistry;

class CsvCrestar extends Driver
{

    public function loadMessage($message)
    {
        // TODO: Implement loadMessage() method.
    }

    /**
     * @param \Cake\ORM\Entity $labOrderEntity
     * @param $result
     * @return bool|string[]
     */
    public function parseResponse(\Cake\ORM\Entity $labOrderEntity, $result)
    {
        if ($result) {
            $lines = explode(PHP_EOL, $result);
            $patientGeneCnvsTable = TableRegistry::getTableLocator()->get('Patients.PatientGeneCnvs');
            $patientSnpsTable = TableRegistry::getTableLocator()->get('Patients.PatientSnps');

            $driver = $patientGeneCnvsTable->getConnection()->getDriver();
            $autoQuoting = $driver->isAutoQuotingEnabled();
            $driver->enableAutoQuoting(true);

            foreach ($lines as $index => $line) {
                $data = str_getcsv($line);
                if ($index == 0) {
                    // validate columns matches what we expect
                    if (count($data) != 4) {
                        return ['error' => 'Number of columns does not match expected!'];
                    }
                    continue;
                }
                if (isset($data[1])) {
                    switch ($data[1]) {
                        case 'snp':
                            $table = $patientSnpsTable;
                            $entity = $patientSnpsTable->newEntity();
                            break;
                        case 'cnv':
                            $table = $patientGeneCnvsTable;
                            $entity = $patientGeneCnvsTable->newEntity();
                            break;
                        default:
                            $entity = false;
                            break;
                    }
                    if ($entity) {
                        $entity->patient_id = $labOrderEntity->patient_id;
                        $entity->key = $data[2];
                        $entity->value = $data[3];
                        $table->save($entity);
                    }
                }
            }
            $driver->enableAutoQuoting($autoQuoting);
            return true;
        }
        return ['error' => 'Result is empty!'];
    }
}
