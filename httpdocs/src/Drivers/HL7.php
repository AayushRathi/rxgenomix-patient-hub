<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 2/28/19
 * Time: 11:02 AM
 */

namespace App\Drivers;
use Aranyasen\HL7 as BaseHL7;

class HL7 extends Driver
{
    /**
     * @var
     */
    protected $hl7;

    /**
     * @var array
     */
    protected $validSexes = ['M', 'F', 'U'];

    /**
     * HL7 constructor.
     * @param string|null $hl7String
     * @throws \Exception
     */
    public function __construct(string $hl7String = null)
    {
        if ($hl7String) {
            $this->loadMessage($hl7String);

        }
        $this->hl7 = new BaseHL7();
        $this->hl7->setFieldSeparator('&');
        $this->hl7->setComponentSeparator('^');
        $this->hl7->setSubComponentSeparator('|');
    }

    /**
     * @return BaseHL7\Message
     * @throws \Exception
     */
    public function createMessage()
    {
        $this->message = $this->hl7->createMessage();

        return $this->message;
    }

    /**
     * @param $message
     * @return BaseHL7\Message|mixed
     * @throws \Exception
     */
    public function loadMessage($message)
    {
        // Fix for Crestar crap HL7 Message Header
        $message = preg_replace('/^(MSH\|\^~\&|)/', 'MSH|^~\&|', $message);
        $this->rawMessage = $message;

        $this->message = $this->hl7->createMessage($message);

        return $this->message;
    }

    public function loadResults($message)
    {
        $message = $this->loadMessage($message);
    }

    /**
     * @param \Cake\ORM\Entity $labOrderEntity
     * @return mixed
     * @throws \Exception
     */
    public function createLabOrder(\Cake\ORM\Entity $labOrderEntity, bool $deIdentified = false)
    {
        if (!$this->message) {
            $this->createMessage();
        }

        $msh = $this->hl7->createMSH();
        $msh->setField(2, '^~\&');
        $msh->setField(3, 'RxGenomix');
        $msh->setField(4, 'RxGmx');
        $msh->setField(5, 'QSS');
        $msh->setField(6, $labOrderEntity->provider->health_care_client->crestar_client_id);

        $this->message->addSegment($msh);

        $patient = $labOrderEntity->patient;

        $patientSex = strtoupper(substr($patient->sex->title, 0, 1));

        if (!in_array($patientSex, $this->validSexes)) {
            $patientSex = 'U';
        }

        // Patient Information
        $pid = new BaseHL7\Segments\PID();
        if ($deIdentified) {
            $firstName = $patient->first_name;
            $firstName = substr_replace($firstName, str_repeat("X", (strlen($firstName) - 1)), 1, (strlen($firstName) - 1));
            $lastName = $patient->last_name;
            $lastName = substr_replace($lastName, str_repeat("X", (strlen($lastName) - 1)), 1, (strlen($lastName) - 1));
            $pid->setPatientName([
                $firstName,
                $lastName
            ]);
        } else {
            $pid->setPatientName([
                $patient->first_name,
                $patient->last_name
            ]);
        }

        $pid->setDateTimeOfBirth($patient->date_of_birth->i18nFormat('yyyyMMdd'));
        $pid->setPatientID($patient->id);
        $pid->setSex($patientSex);

        if ($patient->ethnicity && $patient->ethnicity->title) {
            $pid->setEthnicGroup($patient->ethnicity->title);
        }

        if (
            !is_null($labOrderEntity->shipping_address_1) &&
            !is_null($labOrderEntity->shipping_city) &&
            !is_null($labOrderEntity->shipping_lib24watch_state_abbr) &&
            !is_null($labOrderEntity->shipping_postal_code)
        ) {
            $pid->setPatientAddress($this->formatShippingAddress($labOrderEntity));
        }

        //$pid->setField($pid->size()+1, $patient->card_id);
        //$pid->setField($pid->size()+1, $patient->person_code);

        $this->message->addSegment($pid);

        // Insurance
        $in1 = new BaseHL7\Segments\IN1();
        // Note FEE is specific for Crestar, may need to set this on the Lab level (ie. in the database)
        $in1->setInsuranceCompanyID('FEE');
        $in1->setInsuranceCompanyName('FEE');
        $this->message->addSegment($in1);

        foreach ($labOrderEntity->providers as $provider) {
            if (
                trim($provider->provider_name) != '' &&
                trim($provider->npi_number) != '' &&
                $provider->is_ordering_physician == 1
            ) {
                $pv1 = new BaseHL7\Segments\PV1();
                $pv1->setAttendingDoctor($this->formatProvider($provider));
                $this->message->addSegment($pv1);
                break;
            }
        }

        // Order Control
        $orc = new BaseHl7\Segments\ORC();
        $orc->setPlacerOrderNumber($labOrderEntity->sample_id);
        $this->message->addSegment($orc);

        // Barcode
        if ($labOrderEntity->barcode) {
            $nte = new BaseHl7\Segments\NTE();
            $nte->setComment($labOrderEntity->barcode);
            $this->message->addSegment($nte);
        }

        // OBR

        // PGX
        if ($labOrderEntity->lab_order_type_id === 1) {
            $obr = new BaseHl7\Segments\OBR();
            $obr->setUniversalServiceID('RXG45');
            $obr->setObservationDateTime(lib24watchDate('%Y%m%d%H%M%S', $labOrderEntity->created));
        } else {
            $obr = new BaseHl7\Segments\OBR();
            $obr->setUniversalServiceID($labOrderEntity->specimen_barcode);
            $obr->setObservationDateTime(lib24watchDate('%Y%m%d%H%M%S', $labOrderEntity->created));
            $obr->setSpecimenReceivedDateTime(lib24watchDate('%Y%m%d%H%M%S', $labOrderEntity->specimenCollectionDateAndTime));
            $obr->setFillerField1($labOrderEntity->specimen_identifier);
            $obr->setSpecimenSource($labOrderEntity->specimenTypeFormatted);
        }

        $this->message->addSegment($obr);

        if ($labOrderEntity->lab_order_type_id === 1) {
            // Medications
            foreach ($labOrderEntity->medications as $medication) {
                //debug($medication);
                $rxo = new BaseHl7\Segment('RXO', [
                    $medication->drug_id . '^' .
                    $medication->drug->nondose_name
                ]);
                $this->message->addSegment($rxo);
            }
        } else if ($labOrderEntity->lab_order_type_id === 2 && !empty($labOrderEntity->patient->patient_icd10_codes)) {
            foreach ($labOrderEntity->patient->patient_icd10_codes as $icd10Code) {
                $dg1 = new BaseHl7\Segments\DG1();
                $dg1->setDiagnosisCodeDG1($icd10Code->icd10_code->code);
                $dg1->setDiagnosisDescription($icd10Code->icd10_code->title);
                $dg1->setDiagnosisCodingMethod('I10');
                $this->message->addSegment($dg1);
            }
        }

        return $this->message->toString(true);
    }

    /**
     * @param \Cake\ORM\Entity $entity
     * @return string
     */
    protected function formatShippingAddress(\Cake\ORM\Entity $entity)
    {
        return $entity->shipping_address_1 .
            '^' .
            $entity->shipping_address_2 .
            '^' .
            $entity->shipping_city .
            '^' .
            $entity->shipping_lib24watch_state_abbr .
            '^' .
            $entity->shipping_postal_code;
    }

    /**
     * @param \Cake\ORM\Entity $entity
     * @return string
     */
    protected function formatProvider(\Cake\ORM\Entity $entity)
    {
        return $entity->npi_number;
    }
}
