<?php

namespace App\Drivers;

use Cake\Collection\Collection;
use Cake\Console\ConsoleIo;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use LabOrders\Libs\LabOrders;
use SystemManagement\Libs\RxNav;
use SystemManagement\Libs\Translational;

class JsonTsi extends Driver
{
    use MailerAwareTrait;

    /**
     * @var array
     */
    protected $severityMapping = [
        'low' => 'Info',
        'moderate' => 'Warning',
        'high' => 'Danger',
    ];

    /**
     * @param $message
     * @return false|mixed|string
     */
    public function loadMessage($message)
    {
        $this->rawMessage = $message;

        $this->message = json_encode($message);

        return $this->message;
    }

    /**
     * @param \Cake\ORM\Entity $labOrderEntity
     * @return mixed
     * @throws \Exception
     */
    public function createLabOrder(\Cake\ORM\Entity $labOrderEntity)
    {
        $labOrderArray = $labOrderEntity->toArray();
        // TODO: Reformat as needed here

        return $this->loadMessage($labOrderArray);
    }

    /**
     *
     * Get the destination endpoint (between TSI sandbox and prod)
     *
     * @param string $compCode
     *
     * @return string
     *
     */
    private function getDestinationEndpoint()
    {
        // production
        if (getenv('RXG_ENV') == 'production') {
            return 'https://api.pgxportal.com';
        } else {
            return 'https://api-sandbox.pgxportal.com';
        }
    }

    /**
     *
     * Submit an order to TSI
     *
     * @param Entity $labOrderEntity
     * @param bool $deIdentified
     * @param string $compCode
     *
     */
    public function createSubmit(\Cake\ORM\Entity $labOrderEntity, bool $deIdentified = true, string $compCode = '')
    {

        $destinationEndpoint = $this->getDestinationEndpoint();
        $sourceEndpoint = \Cake\Core\Configure::read('App.fullBaseUrl') ?? '';

        if (!isset($labOrderEntity->base64_csv)) {
            throw new \Exception('Csv required');
        }

        if (!isset($labOrderEntity->specimen_value)) {
            throw new \Exception('Specimen value required');
        }

        $timestamp = (new \DateTime())->getTimestamp();
        if ($labOrderEntity->created) {
            $timestamp = $labOrderEntity->created->getTimestamp();
        }

        $date = date_format(date_timestamp_set(new \DateTime(), $timestamp)->setTimezone(new \DateTimeZone('America/New_York')), 'c');

        $logoPath = ROOT . '/plugins/RxgTheme/webroot/img/rxg-logo.png';

        if (!file_exists($logoPath)) {
            throw new \Exception('Could not find file');
        }

        $logo = new \Cake\Filesystem\File($logoPath);

        $npi = $clinician = '';
        foreach ($labOrderEntity->providers as $provider) {
            if ($provider->is_ordering_physician) {
                $npi = $provider->npi_number;
                $clinician = $provider->provider_name;
            }
        }

        if (!$npi || !$clinician) {
            throw new \Exception('No NPI found');
        }

        $clinicianParts = explode(' ', $clinician);

        if (!isset($labOrderEntity->provider) || !isset($labOrderEntity->provider->health_care_client) || !is_object($labOrderEntity->provider->health_care_client)) {
            throw new \Exception('No health care client found');
        }

        $medicationStatements = [];
        if (!empty($labOrderEntity->medications)) {
            $medicationStatements = self::generateMedicationStatements($labOrderEntity->medications, $date);
        }

        $patientFirstName = $labOrderEntity->patient->first_name;
        $patientLastName = $labOrderEntity->patient->last_name;
        $patientDob = lib24watchDate('%Y-%m-%d', $labOrderEntity->patient->date_of_birth, +5);
        $patientId = $labOrderEntity->patient->id;

        if ($deIdentified) {
            $patientFirstName = substr_replace($patientFirstName, str_repeat("X", (strlen($patientFirstName) - 1)), 1, (strlen($patientFirstName) - 1));
            $patientLastName = substr_replace($patientLastName, str_repeat("X", (strlen($patientLastName) - 1)), 1, (strlen($patientLastName) - 1));
        }

        $data = [
            'resourceType' => 'Bundle',
            'type' => 'message',
            'entry' => [
                [
                    'resource' => [
                        'resourceType' => 'MessageHeader',
                        'timestamp' => $date,
                        'id' => $labOrderEntity->sample_id,
                        'event' => [
                            'code' => 'procedurerequest-order',
                        ],
                        'source' => [
                            'endpoint' => $sourceEndpoint
                        ],
                        'destination' => [
                            'endpoint' => $destinationEndpoint . '/api/$process-message' // TODO: generate dynamically
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'ProcedureRequest',
                        'code' => [
                            'coding' => [
                                [
                                    'system' => 'https://help.pgxportal.com/wiki/display/SOF/GeneticTests',
                                    'code' => $compCode,
                                    'display' => 'Comprehensive Panel',
                                ]
                            ]
                        ],
                        'authoredOn' => $date
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Organization',
                        'identifier' => [
                            'value' => $labOrderEntity->provider->health_care_client->id
                        ],
                        'name' => $labOrderEntity->provider->health_care_client->title
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Practitioner',
                        'identifier' => [
                            'value' => $npi
                        ],
                        'name' => [
                            'family' => $clinician[1],
                            'given' => [
                                $clinician[0]
                            ]
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Provenance',
                        'signature' => [
                            [
                                'type' => [
                                    [
                                        'system' => 'http://hl7.org/fhir/valueset-signature-type',
                                        'code' => '1.2.840.10065.1.12.1.1'
                                    ]
                                ],
                                'contentType' => 'image/jpeg',
                                'blob' => '/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAA2AKcDAREAAhEBAxEB/8QAHQAAAgMAAwEBAAAAAAAAAAAABgcABQgCAwQJAf/EAB0BAQACAwEBAQEAAAAAAAAAAAAFBgEHCAQCAwn/2gAMAwEAAhADEAAAAfpdq/oLZ20OfeLFH7I6+8clCEIQhCA/7Ywg8UmobPR29WLxCEIYk1V0UKR03qTeXJeJd8co/RLRnUmXdi6e0lQdrpS26/YMJZ05aaJo6h7VVljpwzIRChtFG1HrrcQtIwxPHzBL4JaGfbtrHRlF2n88+Y++NR3/AEz2erwB/SPElL6482ibAPe2MEZSD1HrrcSzsFSBpiucc421qPoLF+2NCbi1B0NhbcPOnX9fP6c8fRfFzgxIRFjyj/Qhz2fX/P6+M6dGcYPCobDV9hp+hKRs9U2SmFEdMLGw1AqjZkDmq3deOQZkBbRr3xNf+/mYEJZV3O1do124ou4a5ftL2We1y5W5AYCchAZCYhCEIQgHBiQhDE2S8C085eC9GsLEvT0g4N47sE/lSjrFeMTAVy4DuwMikLU8YTngKAJwFFflWYNMNjyH4HZQA6eg6QsP/8QAJRAAAgMBAQABAwQDAAAAAAAABQYDBAcBAgAQExUWIDA1FyY3/9oACAEBAAEFAiulFqD7873nOUCgwr4/hvlRYrz8FuwUud/ay6X+AcJdiPVp7zZX6rZ+2WVocZNDgFDuu97xfYhbNR/yqIgmpOdGVS96zN9teYRzMOZtKDL1oNq9G8R2IpJasrDdZYba4YB23RWdYWO+1stZUFeWanAud13vvi6zCmim0xRz7BsXjx6T8p739CYn/T67JN7MeXNzjjzQWaHsCIJqEX52aaKuK4Y1K3FjXv34FY1Vp2+S06k823f1vxI/6g0/6bojn3rc+7H7k+9G5OUEedDDdRsLZ6dvaC+r19nX0ReurS7UVtBTrbCnWWpdi9bLUjVBx0YMSlkwGZNAUJW0XBFrpGHNFgssxEkRlXzNATphom/qcjYIWO6X7JK6wZGvT8tyMy/nCcTBTuSnXbRcPdkoRqQ1hG0OkKHL30JGRgfv1uGBo+5/AcPiFun+woJs3NpvtDOcNwsDk0kn0SdHiYiDGttUJJ1HqkPWV3YKDBfZp6dluagNVlOPRmAw72AINtsOrpo0zZBbKO9+/FfansakEbDKlkbTUxXl3ntldmlqYGOBjgKadcUM4LxExjClgGicxnaubIGkZaPVK+fqtWnaWA1wvInAJQRzPFZhIcUl/wAfCWXphW/rdahWHhKxPP0vK1Hwoqp9IAsly3nynbFcQFj8AdBjGQTMoAJwR3PFZhIkUBWI1pM8VJQq8sCFiv8A/8QAPBEAAgIBAwIEAwIJDQAAAAAAAgMBBAUGERIHEwAUISIVI0ExMggQFiQ0NVFxtSAmMDM3Q1JgYXN0dbT/2gAIAQMBAT8BwnSHB5Ppk7WDszdXlZxeWyqpWdX4PW+GHagaNpZVytG1vlJW54W0yh1gIXVfCNrfiZiImZmIiI3mZ9IiI+2Zn6RHjG5jE5hbG4jKY7KKScLazHXa10FMkYOAYdZjBA5GYKBKYnad9tv6LJZrD4YVHl8rjcUDyIUnkb1akLiCIIxUVlq4YQjMSUBvMRO8/iw/UDAZzUmS0vS87F/GTagnOQAU7RUXjWuDUaLjaU13Fxnvorw2IJleXKjn/K0f0g/KjQVTLV9SZCrZy6sk9eIkdsQV+lkL2PpzakDM5WRU0m10VWuWJF2gOQCJR0D0zdrWatHW83MzS+XcOsGOs1qlj3bJt46vbZcqbkBB865DPaRwkpGVeOpfxTp4GYq3cPaymRxtpdF1HG9xhtCzx7d1BChhlUOu1dtbCSMlXYBkK95iOmOtbek8Xk69bS+Y1DFi2t5OxgNNaZCotcJZK6tiIM+HONyH2zHp6bzns/itNY12Vy9mK1VUiA+km6w89+1WrJH3usM2mRAfugLHMlaFNYE9byITvI0Tm24ICnllZbxEAEtilnbpOoAY/YQTlOIl7ZZ9fGmdU4fVuOjJYdxmsT7Viu8O1bpv4wcosqgjgT4lBCazahozyS1g+viesuDrv1GrJUbNOMBbZSVAPRYflbAXLVTt1ET2IX6VTew3NhSVz8xkTx54/XuOdolet8lWfjaBea5V1wy+4ezlH4yvESpC4mbbFKkSMV10y8Ya+FDL5Z1qsdqchX0JnmYKPd8XcbEqhW/9YRLoWcfH7vichv6dz6+NManxercWOWxJN7PdOu9FgIXZqWliBnWsABsXDIW1TIlbWrNbAMGEJRPjV3VjBaXuFikIsZrLrKAbVpkC0Vmlx4IsWzg/zk+UbIrItMCfR0JKQg8D1ox2RylfD5nBZHBW7jk1625FfDvWThdYHrirUuq8w0gUowpuXyLdpqVBMjrrmG3LVXBljLdZWNabV5VonFK9NyggzXXKVCBHV58XcWntMTvA/TSOt7eqLtqpY0rmcCNarFgbGSW4FOKWirsr7lSvHc2Ln6EU8Rn0+vjSud07d19qnGY/S6MbmKwZGchmglHPIRTyNWs6OIKE1ebe1dp+xfOaoWWO66BONHa/RqrJZnDtxjsPlMKXF1R9ldgm9qw2pclZLBf6HZWtTt42nzCZGZ3KB1nq2pozCzl7SDtSVqvTrVFsFR2HukjIYYQkIdqsqxZKZGdxTIR6lE+B1bRraVq6qzgfBatmoi55Zp+ZeMW4500AKlibrT1EsvLgvmsiID2hRnE9byZDLlPRObs4RRF3MpLeELAJ2OWwmlaorINp5CWT2GfQjjxpbVuG1hQm/h3MntHC7VSyAqu0mlHIQsKE2BsY+q3Ja6u3Y4W4yWyA0Y91XoL5iuw0vRpfWzUuWUgxTQv6jJbVmMwQMWUQazGYIDiCid48dA2GGveAlIi7CZRbRifRgD2HiJftiHJUyI/xLGfp46/REa8ysxG0lh8YRTH2zMY0BiZ/14iI/uiI+nj8H79RZ3/tK38PR463tsMzujKM1TvVZN7hxwsNUZKwy7RQdPmMFxY1XGstkCRpi4chHvmCDXuvFKBC+k9xaFrFS0hbaKgUA8BUCxxUAKxCIEQGIGBjaI28dJcRn8ZqfUtm5p+7p/E5Ssywio5ZDWQwcjLaVJLJFcM8lWt2UqnthMqiZ4j93x04wlHKdS9X3byV2fg2RzFiopowYBdsZyytVrjPtI6y1O7XKJgGsB4bOSow6g6xx2jsNDr1GMqzImdOvjDJYIsj2pOzNtjFuEKi1e1uyLBGbVK7Miw2LjO9YryOf5DYAK1lf6NeuL7spcH3LCTy6yGSAuLEuQpg+4GpAtxjoMxi8NqskxLZU2i1K53nmzyVjjG0eu7OADO3rO0fsjx0GpUbsZ/PWpG3nfNqXNh+zLCUXElZdaEp9wMyVk7MPb9rfLcd9ucE6jSs2KlqxUrutUDayjYalZvqG5UpcVZpDJplqilbO3I8x9C32jx+EH+qdP8A/LyX/kX+Lp9/bBrv/b1B/H8b41l/MPqjhtWh8rE5+O1lJieK42hNHKyYhHoCknj8uMT62LiXFO/E58a9mdcdScBopUkeOxMi3LcZKB3csL2S5EO8e3GKq1Kzf7q5daqfWZieu7G9/RmPhRNpMdkXTSWcqG29J4uupQyMfLYKLT66mDG6ouHIx4VrzXddS69fpPcQhCwShCbbVqSpYwC1KWGKEAWsIgQABgRGIgYiI8dLMTqCjrXUF+3py9pzEZXH3bAVGLkaSLM5Sm6pUSfbSM+XTYvxWEUrEEyYgAjG3jB9VNNY3paeirFbOFlywepMbDk1KJY7zGXs5d1UpeeUXZ7IjfT3y8lJgQsha2wIyfTHVeM0bqkM1llXnVBoXqshj0ofY7llYiuYCzapr4RMe+e9vH0EvHU3VGM1lqqzmsWq6uk6lRrQvIpQmxyrVxS3kuvZuK4EUTx+dMyP3hH7PFLR3U3Ql3KUtGDQyOHyZxKbNltPnTEeYV3OTcsVSG/XUXbYal36lkQW06/LZCtUaEtay0vi6mYt1k6qxlcGLydSGFTK/KlhdCRlaHeSvEoDKVrSxDloetZiiazkn15pJjFxUw96AjshnHOxzW8Y9ot5ndqm2Rjb5lnDsezbk+GMkt9GYrUeIxE19T5uM3km2rFqWwMyNQLBc5prsmCmWlC3uNUR1641wbFKukKlZAx0/wBJZ3A6s1nlMnVWilmLdltBg2a7iaDMrdtBJLUZGrdLllsyBmJnjPrEx46m6HdrXD11UXJRk8a87FTzEmNewty+1ZqsYuDJEsiFsW6Fs2YkVkIg0mrrp635VEYS/wDCcJVYMV7Wod6bclNaY4PYkaN6wE2jVy7crpY4+5MSNqmWzl9JtIZrSSc+nL1grjbt1CpSFiu/uorrevmUIM4XOxB7S2+30+zxlenGrdMZ6zqHpxcTCrhGTcQxiEkuHH3WVBC3EY+3QhnIkQ1tWzSEhXXI5GXxjcJ1bz+Xxl/UmXTpuhi7S7Pk8cdUnWZHcWB2Kh26zxsJk0MLI3Xqri2TTRaUnHjqZoputcGqtTalOSx9nzdPzEkKHwajTYqNYAmSocBiwGws9nJUJcVmwo0hPVpmVpRqlWMq4WnVfXt7nTZeyRyqIrWxKi65+dLesOclOPrlXbZ5VmPlEq0hpDP4nqNqvUN6otWKygZeKTxtV2mybeXpWkckLOWr5ISZTzGOMxxLYpjx1K0o3V2mH0aawZlKr038ZDDFQk9cypySYWwwNim6yoeZCuHSlhzsvx0q0Ll9OWczmtSLEcte7dOv+dBcbFTcbNtxvWxgz5uxFceJF3B8jyn2sHfXmiqutsPFE3RUvVWzZx13h3IS6QlbFOCJEjq2VzwcAkMwYpfHIkCBInrxjVRihrYfKgqISvNPfRc3gMcBPuuu0LD5EYie7cxTbLZ99jutI5nROJ1Pisa8dV5yM1kLdttv2DuujDp3OqqxIIlypPdgLGtXRU5TVqriutf+QP/EAEERAAICAQMCBQEFAQsNAAAAAAIDAQQFBhESABMHFCEiMSMQFTJBUXEWFyAwNEBCQ2G00SYzNTdScnN0gZG14fD/2gAIAQIBAT8ByGucjT1eGBChXKl5ylSOCF3n3ebFMzZScNhIgHf5LXKGdwFFycrubo6iJmYiI3mfSIj5mf0jq5jshjjBeQo3KJsGSALlZ1YjGJ4yQC4Akhgo2mYiY39P4qljcjkSMcfQu3yXAywadV9klwU7DJwkDkYKfSJLbefj7MjpLLYzDUs5Z8t5S7CJFa2kVlA2lS6tNgJUIR3VxvHaa2VzMA6Fn7f4Wd139zalfSbiarlUTqLK9E73orWKta0+EwQiPKIsGIBLgWcxHMhiSmGeJmXruU6zp2EY+x7kC0rSXOV6e9FtqBrv2goL6aOHrA9yImD60aNLVjKDa+QRRq20nZXaucQACTvJV2RLQGHw0CrkMMnZoyIyXzOt9NIz9+k12cx2IlVc1Qu6axNkE8jlg83p3AeXGdon1j5/LrFYm9mrq6GPTLnnElPrxWpQ7c3OZPtWoN43KfxEQrCDaYAX72MRI1W6mxgZQhjahC95kpjeIHnZXbIZ+YLyO8j68Py6zeCyGn7nk8gsROR7iWqLnXsK3kYahkwMyO8TEiYg0J9rFgXp1+9xk3Kwx07SbE5auFo+SmqVQSddD+Vhsd2T9XioRWuWMP8AAExy429KW16mPTFJyrloexxcfCosu5RVedOzGntFcGHEiJG1kKmQVzKFQHhornFR2qcUGU+Pu9Ygxnc2/BAnbTbn9vkuW3rw/LrN4S9p+8VC+IdzgLlNSUmmwg5IRckiEDkJMDD3gBiYEJBEx1p7QOVzlaL7GpxuPKJJb7AkbXAO/Jqa48fojt6tc1AF8rlkQUjlfDa5TouyGOylPKorqY53oNUu2kebiUffsVj7S4JhiVhZ8Y9gmciE+FmOCuh+Ti7Xcy6AgdBcj5mrFa00RN0QclAv23Xusd4n0metQ6Yr4OsiwnO47Ky5/ZlNM1ka47ZH3S4PbPD28fiI3mPXrO4rMVtJ4O7bzjbuOcVPymMKG8anmKb3rnkRyJ+XUBoVvH0gOQV21zIzqLSbcDTxuRXdXkaOSjddhSTTAc0rsV+cGR/ylJka9p3+iyCiNo303gH6kyUY9DRREIbYc8wlgpUvYYmQGRkubmJTG0xtLIL4iep0/adnX4HFl95PTYbX7wD2VTNf22GlJlIrQo4Me6R8TERIfxiPUeGMBIV7OpsYnJMiOFGA5SZF+GA7tlFo4L+iUUt5/Ies7p/JadtxVyKx94yaLCSllayETsRJZIgXtn0NbAW4Nxk1jBhJagWDvEvtNAWKZmdPAxZxBAYFVxUEBjPoQlHtIZ9JGZievEwBLTXIoiZXkKZhM/0SnuLmY/t4MMf2FPXhjO+nKUT8RftxH9kebKdv2bzM/tmevFv/AErjP+Sd/e2deGK1Bi9SWoeNV8Ctc3JATmkkK1po2OJTESAHycYTMCya4wU+2JgtKaUMyaevq5tM5YbCrrIyMp5EZHN/lJyXukpneZ9ZnfrX+RxN3CYVNbLVsvfouFLbCziXtAqcBZssDkfDzLq6WMjmWx7RyL561lk7NHRWna1Vhp+8aWNVYNZcSKsnFpJiOUesC4zX3OMxyACUW62GJaR05c1FkpXVtTRCmI2HXRgyamecCmK4ga5KwbPVf1VQIgw+5yEQOcV4dVW8f3UZYnoP/PVa58IasvxJYOPOJ2KNwYtphPoS2FGxdeKoAeRwMMmAg12QYfpHEPMp39f0DkUx+UevXirZtVpxWKRBV8V5cyhS9wSxlZkJWgoj0IKSRTKl/C+9v88dl2rKVWEKsOUi2IBaSDDFVgVHDVw4ImBZC2RBhzieJesbdeEX8vy3/Apf3g/s1d/q80t/vYj/AMTd605/lVobJYAvffxM86O/qc7yy1QgZKfxGwbePnb0VXYuPTkPWlIjTGjMtqU4gbl/cMfy2mdlkVWnsM7T63TfYcv+sr1gZ8REx4WAvtaktyYrsgumuLJjDCrqYN5xsmC/GEtQtrAKdjmuO89M0rpVzDa3X1drWmTGtZXWbGMOZIzMyvyRGZTJERTJTMzMzv1rrIYi1prE1K+YrZjIULdZRWBOJstR5GyuxYYPNkx3WKqy6ZYckziREUzv1kdF5a3rIdQKdjooxkcTblbH2Yt9qimit0QsaZp5zNZnbjzHEokORBvPHWGFt5/DFj6R1gfNmu7laNq1cVFMl7lJefL/AGY7e0/nMdaQw9zAYZWPuHXKwuxZbzqMaatmtlgbG1KD5RE+v042n4merOotE6qrUbOpJtU8jRH6iULscbMlxJy1srqfBVWnHMBYdV6ZIwF231WYPVKNOZy9Yx1d7cFdaQFRsSMWYqwwyrHE82r8zVgyEYM2A1ZtUZCTYctg+FVlnnpfkaslPcLGLXbBe8+pBxGs8V7z6cEZEVB8KkAgYjUl/D5DIQ3CYycZTWhSICZiCsEqOMWDSJMBDCDgBwLXS4l+ZcwrDmz1q3UGKyuB03RpPNtnHV0LtgSGrgCChWQUQZiIns1ZxuEzvEb/ABPWidTr01kXHaWxtK6oVWOzAy5JrPml4AUiLOEyYGuTDcGycTJAIG5nhjQbOTqfeGTeE95GI2sBTh0e5YsK1VSUIE9uUHZuDw3gkWB+kWv9Q43UDMSzHuJs1674s8ktVwa01FxjuiMnHtL3Rv8AHr89UNY6fzeKTiNZV2SdcRheQAGsg5WPAHydefOV7UjxhvbB6bMwRugd+1N3J+H+Jx92rhsezNW7yDT5m4NgVpidpAu7YGu5UpZAtGKdZTGyHFloIgZ60VqVemsobrIMZStp8vY7W0tVIsFirAAUjDJWQyJBJDuthzG5iETqGNADQszgjvPyVl6m1/bYCtSGD+siRsrr/QNRlxiPNuhwJ2cCu5B6h1Dir+jsDiKtgzv0Sx02VShwCHl8fZQ3ZpjCz4tYA+2Z333j0jfrReeDT+bVasGQ0XqZVu8BkyhRxBrZAD6zKrK0nPGJOVwwBjc+teapx+ZTjcbhjmaFblZd9Aq6/MbElCxUYBMeXTLi5DHCfNbfIz1pXUr9M5GbQr8xVeHZuVuXCWLgoMGLLaRF6SjksiGYkZYqeItkobHhXdObxPyNAjmWHjVKtLDlM8iDgurbUqJn+rrXlpX+FXbCBiNTX8HfuqLA4ycbURXCv7p2O1K/QXsTBNhbOOwEcuc2xx77z7xn/Nvy/wCvX/b7P1/Z1/j1/wC/s/X/AO/T7P8AH+D+f8wj+M//xAA7EAACAwABAwMCBAMECAcAAAACAwEEBRIGERMAFCEVIgcjMTIQJEEWM0JRICUwNENScrRhcXR1gpS1/9oACAEBAAY/Al9MrzKp5/1DOzzgxsfU3+/GvM265w6EAtfuOalFWbDVpLnYT5e9b1MzPaI+Zmf0iP8AOfRszNGjorWUAxlG2i2CzmOUCZINkCUjMFETMT2+f9ks9TSoZwNkhUV63XqCwhjuULl7FwcjHyUD37R8z/C/09V93F2hNmCa1IDVsFTdCLQ1mQ0mFKWz2nypTDIiTRLVxy/0rGc7DpWEZrKKj0onvpDTtUqlyzFeCER5jFlorVL1qZMD5CGCKYRYt9KRWy7X31heV1Fiwn47srXXVgq2O0EJflVpD5gfJEFDPSupMujc169wFQurWCfdR5mSh0MBYP4nTZDAsxEGIGo45zEcvWglHT2puQ6wt0tzwYQKkKwBCj8dd/Yy4847zH2z+npujpvhFdcwMfHNrnF38aK6o+5rj7T2Ef2iJtZIKWxglbT0lrMxxKe+lLO0CMT2mT4VG0hKP6j9R7QXxz9e/wAtpEEF43paPjs1XcYKU2F9yiC7TEwQGxTI+5TDH59bq71N9WMWyyovi5TnaTwtWK3Csn8qA+KxOM2shag/ecTx5B1dfQ6hTL3HJAQd1o+LQdnojutQfNk1rmCIQSqXRDHeMZd6m6no7ZPH/X6mwjUvx/8APJBSfSj/AO/x7/4/6+h0s0meLyEhqnjAWK1gIEiQ8RJgQcAxZxK2MWQMAgMon0WcpL9bTCYFteqQglDC7cUvsly/PLvHZKE2DH9GwuZGCRmamPexrNpqkI+SuD5Xl40C0Pb1ra/MyRWsgqtDkXdhLXBHFfIKhZQugZsDSZBe0uTapqIloKVwMlX5dm9mH27fMR6sVn9OauMKK/nh98Gits+QV+IOdZMc/u5/rPxE/HrqLPpdPJoalcb3vdYZTyu+1v167Y4iuCX7lrAsO7T+axcG/wAjYE41ctlBuXoZM9m1nPW+WeN7K1rhIAH+6vAFt7x/x1SPfvPadOworElYTVRWA4WT3N5FMQZQUD4663vnvHzCpH9Zj1W6j1x+k17FZVrwMPzuiLP3VUjCwgm2XLkC8IhyAiIS/uzL0dqr0lrPyVzPPRlnGAEf3SyFVLFMCH/EJaHaP6lHqbmW0/yyhdis8YXbqsmOQi9YkY/ePytqmNQzsULaRAyB8DgFiW9QdKLas4ggYsqeJBrMZ7wQGPcTGY7EMzE/E+uRDEkrVoGuZ/wHPlVJD/lMrawP+k5j+vpXz+2zrxH/AIR7lpfH/wAimf8AzmfWx/7hX/7JXrpWnNcrlaZa0aMGS4vvO3TSVbkPeYNi+NcGQMkqLRyMfdMSKV/hraBIBCwUNlgrBYxxFYhGbAiAj9sDEcYj4iO3refaxLeJm6KDeqs0JiukxvSyrUWfEOfta9l6lz4w/LiZ4j+nrqe1bUD/AKVe1X1lsHkA237DwXY4z8SaAW3x8ongxguHs1SyGG26caJ3iOqjPKQFNiPHJPmybAaI1gX8M/JcRGxa/FxMjDl/Y/FCvYD+4uWg8kqYP7HqPTCY5DPY1NSs4+QYoZ7j66jlUSyVsqMUHz95+0d2+P8AM+IDP9Z+PWzs2ONnY9ysJc3sblKtKl7bAzPyJ37BPhzP1Z7fj37c+9ay+shtikTDqPYoDbWNq5U0kMKJJUsXMgfCY5D8T37R6xP/AFF//tQ/h1j/ANG3/wDtUPWV1KP5ebtRw0P6BHwqnpcoGP2rUVLTHv8ALrKmz88S9Y3Sa5kqOb2ZpceUR3aAXL/ch7x9ueuvWQz/AIdq2xf6zMT0rShZMqGy82agFKxsuUWcha4kf2HCrDUrMY7ri0Xb0tKPw1spSkBUpSrLAWpYRAgtYBmwIAAxAiIxAjEREREetq7awreFmaVG24axhMVU2PqFVtasouChnwKdchAwoIBXMRERjt6DqpL8mM0dbCvStlm2N7w5qM5diPEOeaPIRVG+EfdcSiQ5mvuUCeXnHUXZK5UfyusapHBByR/eivZPl2/bHi7T/WY9KydI6jbAWbjSmmxrq8hYdJjHJ9esczxn74lXbv8ApM+tGp0pFO9l6BflPsMq86ojyFDWKtOryNxKy4GSwuVnwAMJHf8AJXnVtSzXV1HnpAw0K0HNWbkrAbYTHBLfaXCWBFKwUxLQS4AIVShv06K+XcgI8Q67W0WM4x8CzkduubO0f47GWbj/AHOgzku8p6h1o177LLrEsiJkawunlNUHkKzsLFnNi5JKBQLPaIUFZCY9dVaOhXBVTUs2GUmDYS2WCelbsDJAsyNfdTQLscR279v1j0hdRq1aFBxureeSFDwaHjfXMwgiVJxC2LbAH2NUAQwLCYEZN36bk1zHwWdvvVZflE/Y01DTuPH3BL5cJCpRLn2kbFUvzQ2laaATFmzWKpxel3kUkHByLwmXCfuH7Z7fr8fp6ft9CWVwuyRSzMM0qIIafkOtAWYijZpQfeU+Rld9SCEEyXbyxn3N7TVg0s+wFj2tEqxNfMd4MfDWKyhwuXJJOb1tq0wzmqmyZL0uvVYpV+k/3NXzSUJdBLJT6zDGClcNEoMWQBdmqXE9gI5ipHUS8+vk1a7k2e5VTt3ylf5FmCqNtfzANAOfeaSCSx/dBu8Ur6k27lYF52iOpFR0WEsJnudOrZT3UBywOSVGU8xjj24z8zHptSqAnoVnKuZ8GcLEnB3W1RGXxEOqteuOUwENlRnPYPWrq7wRGlb4VUfzAWme27i+y03LM4n3L/APEp5j7Pv+hx3imbPbW67PPRtcOcLbIyBraPeCOu8J4tESiYIVOjkShGYzhr5eiK48QarnU2s4DHET8jbdNzu0fPktZzLDP3O8jJKZcPUmvGres2WWftjuFOG/JV1vkFS1fPuwQFCE1uXt6y4QAehy5u1I0zqleHOmyn3xUQaKDuDU5+4mqDzBJWIX4haYrk+ZRH8c6NK0NWdbTrY2dyBpe507YuOvVHxgfEmihswbOCo4fccd47/xy6Fy0Kbe091XMTINKbT69c7bliQAQBIV1mzu0ljMD2iZLtH+xHQ2rg0aZ26lEXEt7Ym1ecNesrjXU1n5jSiJOR8ax7saYLEij/Qp+Lf2s/l0G6//ACM5ccE1eoMxDMsfdZdr/Vt4/wCZuc+V33H+7XayeyY6s9te69zFYG5dwcSv0f0xma+aFjNUnne3336726DLjmw6MsCppRQ9vIsM7Js9dI9J27lnoW9c6Rd1N1I+tTRGras19D6T9KzB0lPXncjE9VxeGzYGsSVCULFpu/D7OsdTt19MvxXwxz9u/m0wdSU6jsjTixVp+CvoOoR+ZLD8HvmR2dCwLtHU3S1jqTQ36Zfh5b6wzruqjOHSzdCvdbnMSttCnTQ2u0uNkQOvwTILUpYdnHZ6D66s9ZXtB2pd6Uq6WIyjlryLGVtORVII8dMbo6vZw2H6RWzgrMshFavV8dYesFVurtDpSl0tqDh5udkVcxll1tdJFs9nZLRqWztVLTLPCpTT7RB168xz8sNc38GtLTmuy+HVXV+dZt0x409BmVl7Wd9QqR+niuRWh0wHZYuJoLEViIx1B1wjrez06dC3vxkYKKmUePm1cCxZr+36iG5TfctWbY1JsW3+6r+2CyLq6RXC0j0jjjs3Ojqmp+HtPrW5OOFMdLSv3bXtfYUrGrU0Fpo0oXN3uCmWLFd64bHCfIr8Tjo9TM19jovqRR5dldWitV6njVq1jQy31a1bxGdxAW0W11xX/rTl7XwKgUDSLCvMX0ridL1NbWAIV/P7PVCRbl5lvkBmP0zOBlxkVmrJV0/BaGe0DGOzM/tT/ZkUXp256GTkWep4vd6/06Yra6Xy3P4+48gZ6isEfLzfEIE+iMnD3Op9DO18O9saPUXT+DQsdU6v0+yrPmpXpsqjQyvFaJxab10HMUSqyFcRY1reu2GXUKSxr/Sc9K73UWRWydi9T1tqjVu0LqVIGi9lPsyo24pIm+vdW8oU0h4dF3tfqOxt5epr2sHqeWU6lWqi1uwJYNyohK+9Cjn6CZoSbrLZ9pbX7hjnd2ltdY1dVyMzV/EDp/J6XrguqaF9O0N2pj2LivJXlnPdsRdc7zyZKEETVNUdu3WFCr1bd6Uzuk7dPLp0cirnHftW30F3fq2qzQrWiOk0mkqpUR4FWUpKZIGrNjcboz6r1OxVXpGrtbOr0Zh51jf3Lzbjc7ygiwm3Ux82G1CtNmslxeSyulz8fZk6SUV92b1DqetXXoWsvNxeq9ToshXYtto09Afok9RVxJtQHSkUuUuWqR9Q4yeir6x1LpWs/TKvaqdYZ9HO6hxZKtXYGfeChUqIswXc7abkQ7mNnwQ6YrcApW9RFuLdAHoTbztPSyLc07fH3VF1nLtVHOp2OA80mc8fv8UrljednUtV9Cvb0FLr6pZW1sYy9lCQ8Slaycu7VXegFT4YY0Zd4eKZbKgARyql2ia/oQiGJaz7dzN0MoBSFfhSv0nptLWSVrBipYSmeNRMAjUshy6CKDRTk9Qq6rrEV++2y3qBQNXGlettsnZ0HGLjhg3GuUQwsOHBShCxuWK7C0rPT7umHOizYACxrFmbba8JFkJFkvnl7kQiwMfbDIH49ZXTZ1WzkYp5TM9Hu7UGo8Vi258lYh3nd4jSElDWHDu3ZvOJn03UvVLib1quFPQfla2pjlq01xxGrqRl26o3lwH5UE+CcKYBIthQAA9OeHPCsHSZtZhJrMahFMn1GUm91LMQsc0OZ39xDe7Cl092zz9XL9rOsj9TeNrWo1NbVoZOtaHjMWNLKp3EUbTpMYY0zR3ss7na8xGcli2L3R+B1Di0CbWFNjUv4Wnn2GiiKgZVnNzrPioSmu4b1byLF0jR4h2r/O11OjLwG2dnSo7P0Kjas52Rl4h16GdRzqlmMtzH200lLNzjoV1ttPccwUJ72VIIag3day/avexlpVFFd4+0p1SsCNj29HPCrWHy/JNBzv1dPpGjfDRRpVqxUlaGVsauPb9kxktZUNmbbreauw5kiBon8zPGR7z6yMf6ZNOr0/3nDdm3Lufo5REMi06epUsK0BKz35XJOwfvW9nW/O4ROL3TZVbbc3TvV9PSmxq6dm/e0Kz6Nlduzp2LbL7G+TOp9/z4X40wmAhPcPVzE2K/us28KxsI8jEyXicuwohakwas1PUtoGBxMGET6zumjplGPlFlnSrLsWFkssdyX0SJ4Nh7iFyFscTmHNkuU2JZJn3nWvVLadFtcadu3laupjN0KY/HtNCcu3V94rjAr7u5OhQAoWwsAGMitNBtD+z9aKeJbx79/Hv5lSFima1e9nWa9n25qCANLWNWf95I+X7/AFXwCo2vY1NE9is0dbXDSVsM8/PVjWG9GkV45sv5NO0XKGkBDK54erNfKU+Ju2iu37d25b0b9+2YAubFy9ec+y4+CwEYJnAIj7AHkXf/xAAfEAEBAQEBAAIDAQEAAAAAAAABEQAhMRBBIDBxUWH/2gAIAQEAAT8hKfXy4hvi0MQ/gBABQABVHgB1XgYR2iob/YDDARv6jILHVPYUYTAB8dnRJOOuLflfwefQ1UuNTZGzEbME7u6tc0bthBkwrw6NxAQSYOO7igQZVIpJUg5OL0pN/NMhRH5Iy7mMcR0ZfNwmF9wBifd5cagdA2yQC0p8ujfNKUIcPRisShKfeWb4qk+r8uc4Bpbr5xCBkbrNg6aSHPZnnooaNWzj+B1OxDJb9sjoPROS6YD8WpXdNEUD/WdF5MJC6R/lm6BEZbkjTnnA1IdWvmvkdWlrGRQifg095Ex3xgEp37bVJFVgKJMua4OJ2sr00DgsePQ7qOuIxCUysCfVS4+xCfwsUSkErxkH+B4f2PV+EcVEYgx1vC6WgjC86pOVBwQgmY0W2KWJtsfQMO/iN2izmSQXRHieGnw+mwrLFj39BkwfXppWDrm3QKmI+MQAfRlHHG+2JKZ/iDWc5atZKd8dFD5mNr9OGPMhzgZC705F32ow4lQpWJgT8XmhNLb6RL5jqUtbsmo+RAZnEFWTaKaNxhPG1afbqSB6nSadDLoGjogk/wCvB8Y4E/kUBFAe4EvXmkXVnPWrlg6VH6JMCJ6qA8MEE7JZJIKc0aosx7O1NQhO1nAH9kChVJ9gIHh4hvaIt5imxiFNXApWDBtIXWGlgAyXqqBFk32/ERUDmY16jCWMWu82nhKvTDwl+zzp17qU4zbh2vRWFjRelA6++ZwAESKPKQQwxrryRK7l4NFDEvCGDcJCs+OT8mEkCQqNnCLjjE6d9SBDLdxVEwa0w8BGPB37TLhVI/BBZj6m3/eQhY/gZM2sbB994Hq/ogfUehLUMv46v4qYYV9NJMk/n9gTVLAFT7hf3HQeyX064N2UtoWgePSCfCTsNFQME4/IWIN5/B746zSIZTrdQ/qBBfGaa0ub5CGRHmCbBAc/BY7Vi+FqRazQHVL7lcxhRlRKX8tnsNskb1uauNMHwWTsKH6Lf1DeSiWoCu9EoXcesLnX7f7U0I9/w9jqvVaZXA29bt70JEzPf3PJ6QCKZdjtfNkyUZf9wGDssTWl87npbzglHjfqtvVnZGYEQZ+ul39TgitSUsf22sIkcSJOA8ZLoEQSs6mi6kXCyg7EgQOQqRWEFHjHBOyfDc2cwhVBAAvAZZmUN+vtKHQxatGjhbj+pIIkClz2xqfgCRrqF3XaVM7T41Lbi+BOw2QcjVKTQEUeRUlMT+SHqCGo7DkcJG9AcpHHxY4Vwmkojmxsl8cdsiapPXZ5G7l9+5syA/1aP9hBwKyyLAztXmgZGYEKl1yQMOjM3IhncMTpzsj0ZvkIsTf/2gAMAwEAAgADAAAAEAMwAAABRgAAcehaV9n/AKXk6pFCQBwLUXV5L2a8cOMRoAAAQAAACAATAICCAKITAJZLDDDbJBJIf//EAB8RAQEBAQACAgMBAAAAAAAAAAERACEQMWGBIDBQwf/aAAgBAwEBPxAsgRUDfIAQBPGyZy4AVBAAqqACrMmRBhIxhCJFK/qQuDruIRFFgBHx3EJ33ccVR7h+KxKI3cvlK51G5KhDVEt20HId131VEpJBERMQGy71mFy7xxKwbeiCj2yLnMTCEEG+iFJgUJXFJCDAZnECMGvFbDE9AVGr4fOVTSMfHtOXuL1xaqAoE5RPgBmP5LEPHRMWX0L6Ue8liBDE3KUx/JJkQIKPQ9yVvjw9CB9rMy95i6mmQrKg5oVl4V2Kcx21N66g8VrkhwuHmgsS0FUdBxJQQ8wMhW4QHb+YQjwiayEnQwjLUO4exxD/AJ7Ts8gKWq/HXpA8Ljq7MARM9syh7sYcw0Is0dQYvQ7g1fF9F6f5frXWeQPwgKv3XUQaS3wQhEWEIYSWlScjIz07oBzdc2xfKHqNENQXAurrUF1iMwp/t6LqErCMbTB1NGtITOwQjYBQ7IcgqDel5B0cZoVdpIrUkEPMLsmN9GB6RwUeyq5wjOUyjWKU+NWsW9GDssT2o4Y/bAOxGxw6qCNY8oSUSKW/mJk1PQLjTwETWWHCoVVGhJFlBE7KJ7o1BHVQucs7j/mhGuKLMvetWYzZvn4lINtJPifKFANvSJLG4OrbYdOxZ5D1o/lLQgGApPSFB1H93ZX3e5bKnwOaVeTNogqErtayrAijAG/VUahiiYAmpqfJF0jFDmSMFEu7BoIVZGR61pwLqWGcJdxVvBCHzTyQMAE4b10kAGVg5JLap3i7cpAZjNhFzhMMBtO0BmF+GWmAGxTRh8qiNjLVHezd/v8A/8QAKBEBAQACAQMDBAMAAwAAAAAAAREAITFBUYFhkbEQcaHwIDDxwdHh/9oACAECAQE/EDtpxNEFkEizH9FACABUMAKqqABV0YyNj/8AQeAsFTSf1IuM0mP9KYCRKP0qgFlKVvNJiOP46lmutAIpbLlQcgE1emwiqK7FYHfphIojKFEMA3IuBxcisQ1phsZujjr8YTcXnj4MGH8UugywOxhJeIjbAeMm3EBxsGzXpEfjUFYhh4ikwygq7eQprzcIVAC00wouDrSwaPLJh49zFVr2mCDccZYlBL24W94zYVZgCnZGIiC0uKCEqhnwq4RGSLZtsgkuECNrp0qOOjHQHM3GiReIqDkaivIW1RlUyiScbp/OW880NXJELDACukimKKUEBl3NRqZPD1bgwgsRlFboqIkXGoW0q530TL2/XGWKOLRGg4E8dx1fp9tA50hkVitYyGZAnOpNUqGqEuExvJBURTJroBtl2U1phu2DxhyRFaDJNokraMRQPQmoE3L+3FDLK6jA6EAvCipFqPANu3EMcDCxTyXYEa0/YiGalvL6/o+x9X3ZqAbjnVeiP6ADa3dVfIHB05wFxQqgCwFMDRFcWeQESN8CTElSN52QkVzy/lpb49BI1HjhA8cDKXRvtPISCSnLXNsQLXkYSgGlvHv0NxpXXwn0uB+j87CUZeTtDY7uv61XmVRMoCVmzgysCuoRcD5RPsWwoACoOLlJkRDop8YL6rAWp9PIZkijYhX5witb0MRoqimJNoxbAVAnQcKTgrNiBRyc2jWhEmjoTen/ABKW0idVjciWdzlLAg5VUmEJqu2E5b9dTyydlSCtDyC0HiAA1FTjIgzWIFVYEZgtCg1DqMmgFM8YVC7iFgELbYeI/wA89vr/AMfwl8b/AKQvH8aRo6DnmO/vL6ZCHorVH7Af+7zQLzuH/bx+3CPCeXNPv1/euAIMmhq8eVzVSSaN3q+JrtZ65omhpbX11pOJvEngflU8ZoQl0XbWl175ArzILxrq9/tmqUgnfirv/enviQ3zfwa+fjWE6y6ls63jxzkls5Ddm756dXjIRwjaDdh7/rhG6kKb7c/dnacZODrFeeYp7azQGqu+vdJp9MAl1thVh128+mavTjuoPjc+z5x8eGj68uDP8H5yp440M+1yu/Xm7vvlX2njKnvfOXr3v55wU+eB+cvPrz85X/QfnOUFGXikJfOyecUe+jnlW3v693Fr+P3784M/wfnK79eePjjK23f704zjK297+ecFNeeB+crv15u775Xn0nBx2nGW5//EAB4QAQEBAAMAAwEBAAAAAAAAAAEAERAhMSAw8EGR/9oACAEBAAE/EA1CeJQNHGIfZIG+YiUgcoAFUBZ650WMmuGepL6VyYPJuyeVQI8DXogVSNikCYvjtfgKqRfIeDsLx4KalC0roBDJOTBFhpiE7uio9m1cCkRj3cnahcUoUe9ndVaYQ2Po9chkLPAMnEu+LXBJnZuJYarEd4jjlL97fL/ed9qhlX5j7wLQk8nTQN6uxErSSuWmROItLIWhPM+TX4aglCOyA+HGkTRHWLDALsRcDNbOpoxcZzhdqfE0iV2JXXPClQ3KqwBo0gn6UbZsdKYky63FvSE4oQwVzgliGUMWijZzmJBBlAYF+x1tIxfgSDShWuO+1KfSJt1iEF9cYZgXe0+b+fm/wsvdd7cshYwCkggPsZEB9uADeMOMzooQcEjgQq3kzxJ2oD1TC4wVFw90ys24rhl1QW6xC37ZkV5r1Zl6mHJzPxLXca8CQAtvoeSZBWcTiD8P99vJZ4jvS9/vL0LWwbiOIPCYC+eoqk05X6WXK6LSJLkzucZeBqQcJLmYpFrQO1QCBwjsaAY+gbUjMBMIrYUOCMNGiV1AftOX+yMUscqs+8PGlBm0ueVeQDeNt7D0oa2DngVaoynBraR/pkJ7Ez2MThUyMA30QQPNAt4cdteSikb8NbehNoi3W9pdEEDyZ/JE0i9l+2/1d0g3bXek5PRCyDGtOZcu0Ha1X7x4bSah1dGiYZMgZiJLMUuec+IcwCJw8jXpsaiVdzPna4xSgE2GaBF60IAkmU2QxrU7QZTySegwHxXOCTy1EhJXd8UU3Ed0+2tYchYnv9I1MKvG1FHZUo2b+OBHSQBEqagXPGQVwOUIGRF1lLj4FDT4vRvbrDZGBNGelLuYIsU+o7QGe71rD1FuYcoFwHO5Owhw60RYEa3yksrAxBE+ws3e7iL/AHzx7bdBTKzYvtPDDIFGP+WPPHEUJSS4dUPkz1uZt8t41Ugeh+H+xVZtLtmc0c6vxfWxf4wawJ8RvwHcs022t+OR57Z4DjBx67n2GgCDQQzQqZw0xWBrTOQ8Z1u8gKJvUkKxf3P7HukLIpGsST5DhKKs4L+DwWRkDDBX3KGjH4PcF/eN7mMnDZJSTkVtgFcrfIcBIFJHCq6Vh5w139UPazlRPwbO3kuMpNyq3TnfzKNjiglobVbDViNNZKTjJgD6Vi87ZDtbP4Ex8lBNFYWIWShS1pGAah90GuH+yftYhqRvH9OLGMzcl9sbW0x0ekXIUEMxtIKAmfk26pxPwEeqStG6+52OYUVc6a14+lRHakT1IU8tbGtV0Sf/2Q=='
                                //'blob' => base64_encode($logo->read())
                            ]
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Patient',
                        'identifier' => [
                            [
                                //'system' => 'InternalPatientId',
                                //'value' => $labOrderEntity->patient_id,
                                'system' => 'O_PatientId',
                                'value' => $labOrderEntity->patient_id
                            ]
                        ],
                        /*
                        'telecom' => [
                            [
                                'system' => 'phone',
                                'value' => $labOrderEntity->patient->phone
                            ]
                        ],
                         */
                        'name' => [
                            [
                                'family' => $patientLastName,
                                'given' => [
                                    $patientFirstName
                                ]
                            ]
                        ],
                        //'gender' => $labOrderEntity->patient->sex->title,
                        'birthDate' => $patientDob,
                        /*
                        'address' => [
                            [
                                'line' => [
                                    '<Line A>',
                                    '<Line B>'
                                ],
                                'city' => $city,
                                'state' => $state,
                                'postalCode' => $postalCode,
                                'country' => $country
                            ]
                        ]
                         */
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Coverage',
                        'identifier' => [
                            [
                                'value' => '1850324D',
                            ]
                        ],
                        'type' => [
                            'coding' => [
                                [
                                    'system' => 'http://hl7.org/fhir/coverage-selfpay',
                                    'code' => 'pay',
                                    'display' => 'PAY'
                                ]
                            ]
                        ],
                        'order' => 1,
                        'relationship' => [
                            'coding' => [
                                [
                                    'code' => 'self'
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'DiagnosticReport',
                        'presentedForm' => [
                            [
                                'contentType' => 'text/csv',
                                'title' => $labOrderEntity->id . '.csv',
                                'data' => $labOrderEntity->base64_csv
                            ]
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'Specimen',
                        'accessionIdentifier' => [
                            'value' => $labOrderEntity->specimen_value
                        ]
                    ]
                ]
            ]
        ];

        // If the lab provides a copy file (and the lab order has one encoded)
        $lab = $labOrderEntity->provider->health_care_client->lab;
        if ($lab->has_copy_file && isset($labOrderEntity->copy_file_base64_csv)) {
            $data['entry'][] = [
                'resource' => [
                    'resourceType' => 'DiagnosticReport',
                    'presentedForm' => [
                        [
                            'contentType' => 'text/csv',
                            'title' => $labOrderEntity->id .  $lab->copy_file_suffix . '.csv',
                            'data' => $labOrderEntity->copy_file_base64_csv
                        ]
                    ]
                ]
            ];
        }

        if (!empty($medicationStatements)) {
            foreach ($medicationStatements as $medicationStatement) {
                $data['entry'][] = [
                    'resource' => $medicationStatement
                ];
            }
        }

        return $this->loadMessage($data);
    }

    /**
     * Create the report json for Translational
     *
     * @param Entity $labOrderEntity
     * @param bool $useNewMedications
     * @param string $compCode
     *
     */
    public function createReport(\Cake\ORM\Entity $labOrderEntity, bool $useNewMedications = true, string $compCode = '')
    {
        $destinationEndpoint = $this->getDestinationEndpoint();
        $sourceEndpoint = \Cake\Core\Configure::read('App.fullBaseUrl') ?? '';

        $timestamp = (new \DateTime())->getTimestamp();
        if ($labOrderEntity->created) {
            $timestamp = $labOrderEntity->created->getTimestamp();
        }

        $date = date_format(date_timestamp_set(new \DateTime(), $timestamp)->setTimezone(new \DateTimeZone('America/New_York')), 'c');

        $medicationStatements = [];
        if (!empty($labOrderEntity->medications)) {
            $medicationStatements = self::generateMedicationStatements($labOrderEntity->medications, $date);
        }

        // Prevents TSI returning error "No Medication Statement resources found with UseNewMedications set to true"
        // see https://redmine.orases.com/issues/44408
        if (empty($medicationStatements)) {
            $useNewMedications = false;
        }

        $data = [
            'resourceType' => 'Bundle',
            'type' => 'message',
            'entry' => [
                [
                    'resource' => [
                        'resourceType' => 'MessageHeader',
                        'timestamp' => $date,
                        'id' => $labOrderEntity->sample_id,
                        'event' => [
                            'code' => 'diagnosticreport-provide',
                        ],
                        'source' => [
                            'endpoint' => $sourceEndpoint
                        ],
                        'destination' => [
                            'endpoint' => $destinationEndpoint . '/api/$process-message'
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'DiagnosticReport',
                        'extension' => [
                            [
                                'url' => 'DiagnosticReportDefinition',
                                'extension' => [
                                    [
                                        'url' => 'useNewMedications',
                                        'valueBoolean' => $useNewMedications ? 'true' : 'false'
                                    ],
                                    [
                                        'url' => 'providePDFReport',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'returnDetectedIssues',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'returnObservations',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'returnMedications',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'returnDosingGuidance',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'includeAlternatives',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'includePIMTable',
                                        'valueBoolean' => 'true'
                                    ],
                                    [
                                        'url' => 'includeDDIData',
                                        'valueBoolean' => 'false'
                                    ],
                                    [
                                        'url' => 'filterByMedication',
                                        'valueBoolean' => 'false'
                                    ],
                                    [
                                        'url' => 'maxNdcCodes',
                                        'valueInteger' => '0'
                                    ],
                                    [
                                        'url' => 'reportId',
                                        'valueString' => $compCode
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    'resource' => [
                        'resourceType' => 'ProcedureRequest',
                        'id' => $labOrderEntity->uuid
                    ]
                ],
            ]
        ];

        if (!empty($medicationStatements)) {
            foreach ($medicationStatements as $medicationStatement) {
                $data['entry'][] = [
                    'resource' => $medicationStatement
                ];
            }
        }

        return $this->loadMessage($data);
    }

    public function createPostProcess(array $data = null)
    {
        return $this->loadMessage($data);
    }

    /**
     * @param int|null $labOrderId
     * @param string|null $report
     * @return bool|mixed
     */
    public function parseReport(
        int $labOrderId = null,
        array $report = null,
        bool $generateConsolidatedOnly = false,
        $io = false
    )
    {
        // backup the json
        file_put_contents(ROOT . '/data/report_json_raw_' . $labOrderId . '_' . date('Y-m-d') . '_' . date('H:i:s') . '.json', json_encode($report));

        if ($labOrderId && $report) {
            $labOrderReportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
            $drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');
            $drugNdcsTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');
            $drugNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdcRxcui');
            $medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');

            $labOrderReport = $labOrderReportsTable->newEntity([], [
                'contain' => [
                    'ReportAdditional',
                    'ThrombosisProfile',
                    'Results.Categories',
                    'Results.ResultCitations',
                    'Results.ResultObservations',
                    'Results.ResultGenes',
                    'Results.ResultMedications',
                ]
            ]);

            $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')->find()
                ->where([
                    'LabOrders.id' => $labOrderId
                ])
                ->contain([
                    'Medications.Medication'
                ])
                ->firstOrFail();

            $medications = (new Collection($labOrderEntity->medications));
            $tsiMedicationIds = $medications->extract('medication.tsi_medication_id')->toArray();
            $tsiMedicationIds = array_filter(array_unique($tsiMedicationIds));

            $labOrderReport->lab_order_id = $labOrderId;

            if (isset($labOrderEntity->uuid) && mb_strlen($labOrderEntity->uuid) > 0) {
                $labOrderReport->uuid = $labOrderEntity->uuid;
            }

            $medicationNodes = Translational::getResources($report, 'Medication');

            // report medication nodes have more information than through /api/Medication
            $compareAgainstExisting = false;
            $medicationIds = Translational::extractMedicationIds($medicationNodes, $compareAgainstExisting);

            // If there are any new medications not stored by us, store them along with RXCUIs
            if (count($medicationIds) > 0) {
                $savePgxOnly = false;

                // provide endpoint doesnt return ndcs nor do we need to store related concepts (since we already have these)
                $pullNdcs = false;
                $storeRelatedConcepts = false;
                debug('saving medications');
                $inserted = Translational::saveMedications($medicationNodes, $savePgxOnly, $pullNdcs, $storeRelatedConcepts);
                debug('end of medications save');
            }

            $consolidatedReportData = [];
            $formattedReportData = [];
            $detectedIssuesRaw = Translational::getResources($report, 'DetectedIssue');
            $observationNodes = Translational::getResources($report, 'Observation');

            // getResource resets the original indexes
            $diagnosticReport = Translational::getResource($report, 'DiagnosticReport');

            $allelesTested = null;
            $observationCollection = null;
            $medicationCollection = null;
            $reportExtensions = [];

            if (!empty($observationNodes)) {
                $observationCollection = new Collection($observationNodes);
            }

            if (!empty($medicationNodes)) {
                $medicationCollection = new Collection($medicationNodes);
            }

            if (isset($diagnosticReport['resource']['extension']) && !empty($diagnosticReport['resource']['extension'])) {
                $reportExtensions = new Collection($diagnosticReport['resource']['extension']);
                $allelesTested = $reportExtensions->match([
                    'url' => 'https://help.pgxportal.com/wiki/display/SOF/AllelesTested'
                ]);
            }

            $issuesClean = [];
            foreach ($detectedIssuesRaw as $index => $issue) {
                $issueClean = [
                    'detected_issue_index' => $index+1,
                    'is_relevant' => 0
                ];

                $observationsClean = [];
                $genesClean = [];
                $medicationsClean = [];
                $citationsClean = [];

                // every detectedissue should have an extension node although lets test for it anyway
                if (
                    isset($issue['resource']['extension']) &&
                    !empty($issue['resource']['extension'])
                ) {
                    $extensions = new Collection($issue['resource']['extension']);

                    $propertyMap = [
                        'tsi_evidence_strength' => 'https://help.pgxportal.com/wiki/display/SOF/EvidenceStrength',
                        'tsi_title' => 'https://help.pgxportal.com/wiki/display/SOF/Title',
                        'tsi_description' => 'https://help.pgxportal.com/wiki/display/SOF/Description',
                        'tsi_disease_name' => 'https://help.pgxportal.com/wiki/display/SOF/DiseaseName',
                        'tsi_issue_type_model1' => 'https://help.pgxportal.com/wiki/display/SOF/IssueTypeModel1',
                        'tsi_issue_type_model2' => 'https://help.pgxportal.com/wiki/display/SOF/IssueTypeModel2',
                        'tsi_issue_type_model3' => 'https://help.pgxportal.com/wiki/display/SOF/IssueTypeModel3',
                        'tsi_issue_type_model4' => 'https://help.pgxportal.com/wiki/display/SOF/IssueTypeModel4',
                    ];

                    foreach ($propertyMap as $column => $url) {
                        $match = $extensions->firstMatch([
                            'url' => $url
                        ]);

                        if ($match) {
                            $issueClean[$column] = $match['valueString'];

                            // backfill old columns just incase
                            if ($url === 'https://help.pgxportal.com/wiki/display/SOF/Title') {
                                $issueClean['interpretation'] = $match['valueString'];
                            } elseif ($url === 'https://help.pgxportal.com/wiki/display/SOF/Description') {
                                $issueClean['activity'] = $match['valueString'];
                            }
                        }
                    }

                    $citations = $extensions->match([
                        'url' => 'http://hl7.org/fhir/StructureDefinition/cqif-basic-citation'
                    ]);

                    if ($citations) {
                        foreach ($citations as $citation) {
                            $citationsClean[] = [
                                'description' => $citation['valueString']
                            ];
                        }
                    }
                }

                if (
                    isset($issue['resource']['category']) &&
                    isset($issue['resource']['category']['coding']) &&
                    isset($issue['resource']['category']['coding'][0])
                ) {
                    $codingCollection = new Collection($issue['resource']['category']['coding']);
                    $firstMatch = $codingCollection->firstMatch([
                        'system' => 'http://hl7.org/fhir/v3/ActCode'
                    ]);

                    if ($firstMatch) {
                        $issueClean['tsi_actcode_code'] = $firstMatch['code'];
                        $issueClean['tsi_actcode_display'] = $firstMatch['display'];
                    }
                }

                if (isset($issue['resource']['id'])) {
                    $issueClean['tsi_detected_issue_id'] = $issue['resource']['id'];
                }

                if (isset($issue['resource']['severity'])) {
                    $issueClean['tsi_severity'] = $issue['resource']['severity'];
                    $issueClean['severity'] = $this->severityMapping[$issue['resource']['severity']] ?? 'Not Found';
                }

                if (isset($issue['resource']['detail'])) {
                    $issueClean['tsi_detail'] = $issue['resource']['detail'];
                }

                if (
                    isset($issue['resource']['implicated']) &&
                    is_array($issue['resource']['implicated']) &&
                    count($issue['resource']['implicated']) > 0
                ) {
                    $i = 0;

                    foreach ($issue['resource']['implicated'] as $implicated) {
                        $medicationMatch = preg_match('#Medication/(\d+)#', $implicated['reference'], $medicationMatches);
                        $observationMatch = preg_match('#Observation/([a-zA-Z0-9].+)$#', $implicated['reference'], $observationMatches);
                        $isPhenotype = false;
                        $isGenotype = false;

                        if ($observationMatch) {
                            if (strpos(mb_strtolower($observationMatches[1]), 'phenotype') !== false) {
                                $isPhenotype = true;
                            }

                            if (strpos(mb_strtolower($observationMatches[1]), 'genotype') !== false) {
                                $isGenotype = true;
                            }
                        }

                        if ($implicated['reference'] === 'www.genenames.org') {
                            $genesTable = TableRegistry::getTableLocator()->get('SystemManagement.Genes');
                            $geneEntity = $genesTable->findOrCreate([
                                'approved_symbol' => $implicated['display']
                            ]);

                            $geneRow = [
                                'gene_id' => $geneEntity->id,
                            ];

                            if (!is_null($allelesTested)) {
                                // there really should be just one match, nevertheless
                                $gene = $allelesTested->firstMatch([
                                    'id' => $implicated['display']
                                ]);

                                if ($gene && isset($gene['valueString'])) {
                                    $geneRow['alleles_tested'] = $gene['valueString'];
                                }
                            }

                            $genesClean[] = $geneRow;
                        } else if ($medicationMatch) {
                            $tsiMedicationId = $medicationMatches[1];

                            if (in_array($tsiMedicationId, $tsiMedicationIds)) {
                                $issueClean['is_relevant'] = 1;
                                $issueClean['is_relevant_original'] = 1;
                            }

                            $medicationEntity = $medicationTable
                                ->find()
                                ->select([
                                    'id',
                                    'tsi_medication_id',
                                    'prealert_trigger',
                                    'category',
                                    'drug_class',
                                    'trade_names'
                                ])
                                ->where([
                                    'Medications.tsi_medication_id' => $tsiMedicationId
                                ])
                                ->first();

                            $resultMedicationEntity = [];

                            if ($medicationEntity) {
                                $resultMedicationEntity['medication_id'] = $medicationEntity->id;
                            }

                            $resultMedicationEntity['tsi_medication_id'] = $tsiMedicationId;

                            if (isset($medicationEntity->category) && mb_strlen($medicationEntity->category) > 0) {
                                $resultMedicationEntity['tsi_medication_category'] = $medicationEntity->category;
                            }
                            if (isset($medicationEntity->drug_class) && mb_strlen($medicationEntity->drug_class) > 0) {
                                $resultMedicationEntity['tsi_medication_class'] = $medicationEntity->drug_class;
                            }
                            if (isset($medicationEntity->trade_names) && mb_strlen($medicationEntity->trade_names) > 0) {
                                $resultMedicationEntity['tsi_medication_tradenames'] = $medicationEntity->trade_names;
                            }

                            // generate consolidated data from 2 sources
                            $ndcsClean = [];

                            if ($medicationEntity) {
                                // source 1: by referencing the drug based on the tsi medication id
                                // if the TSI medication was found, find all drugs mapped to it
                                $drugEntities = $drugsTable
                                    ->find()
                                    ->contain(['Ndc'])
                                    ->where([
                                        'Drugs.medication_id' => $medicationEntity->id,
                                        'Drugs.is_combo' => 0
                                    ])
                                    ->toArray();
                            } else {
                                throw new \Exception('pull drug entities by name?');
                            }

                            if (count($drugEntities) === 0 ) {
                                #throw new \Exception('Need drug entities to pull');
                            } else {
                                foreach ($drugEntities as $drug) {
                                    if (isset($drug['ndc']) && !empty($drug['ndc'])) {
                                        foreach ($drug['ndc'] as $ndc) {
                                            if (!in_array((string)$ndc->ndc, $ndcsClean)) {
                                                // this gets trimmed but sprintf('%09d') reverses it in the API
                                                $ndcsClean[] = (string)$ndc->ndc;
                                            }
                                        }

                                    }
                                }
                            }

                            // flag to optionally merge tsi stored ndcs with ours, uniquely
                            $mergeNdcs = false;

                            // source 2: by getting the ndcs scraped from TSI's medications
                            if ($mergeNdcs) {
                                $tsiNdcRxcui = TableRegistry::getTableLocator()->get('SystemManagement.TsiNdcRxcui');
                                $ndcs = $tsiNdcRxcui->find()
                                    ->select([
                                        'ndc_9'
                                    ])
                                    ->where([
                                        'tsi_medication_id' => $tsiMedicationId
                                    ])
                                    ->toArray();

                                if (!empty($ndcs)) {
                                    foreach ($ndcs as $ndc) {
                                        if (!in_array((string)$ndc['ndc_9'], $ndcsClean)) {
                                            // this gets trimmed but sprintf('%09d') reverses it in the API
                                            $ndcsClean[] = (string)$ndc['ndc_9'];
                                        }
                                    }
                                }
                            }

                            if (!empty($ndcsClean)) {
                                foreach ($ndcsClean as $ndcCode) {
                                    // sprintf('%09d') reverses this in the api
                                    $ndcCode = (int)$ndcCode;
                                    $row = [
                                        'drug_id' => $ndcCode,
                                        'severity' => $issueClean['severity'] ?? 'Not Found',
                                        'message' => $issueClean['interpretation'] ?? 'Not Found',
                                        'activity' => $issueClean['activity'] ?? 'Not Found',
                                    ];

                                    $consolidatedReportData[] = $row;
                                }
                            }

                            $medicationsClean[] = $resultMedicationEntity;
                        } else if ($observationMatch && !is_null($observationCollection) && $isPhenotype) {
                            $observationId = $observationMatches[1];
                            $observationNode = $observationCollection->firstMatch([
                                'resource.id' => $observationId
                            ]);

                            $observationRow = [
                                'reference' => $implicated['reference'],
                                'display' => $implicated['display'],
                                'type' => 'phenotype',
                            ];

                            if ($observationNode) {
                                // remove patient reference
                                unset($observationNode['resource']['subject']);

                                $observationRow = array_merge($observationRow, [
                                    'tsi_observation_id' => $observationNode['resource']['id'],
                                    'label_system' => $observationNode['resource']['code']['coding'][0]['system'] ?? null,
                                    'label_code' => $observationNode['resource']['code']['coding'][0]['code'] ?? null,
                                    'label_display' => $observationNode['resource']['code']['coding'][0]['display'] ?? null,
                                    'coding_text' => $observationNode['resource']['code']['coding']['text'] ?? null,
                                    'value_coding_1_system' => $observationNode['resource']['valueCodeableConcept']['coding'][0]['system'] ?? null,
                                    'value_coding_1_code' => $observationNode['resource']['valueCodeableConcept']['coding'][0]['code'] ?? null,
                                    'value_coding_1_display' => $observationNode['resource']['valueCodeableConcept']['coding'][0]['display'] ?? null,
                                    'value_coding_2_system' => $observationNode['resource']['valueCodeableConcept']['coding'][1]['system'] ?? null,
                                    'value_coding_2_code' => $observationNode['resource']['valueCodeableConcept']['coding'][1]['code'] ?? null,
                                    'value_coding_2_display' => $observationNode['resource']['valueCodeableConcept']['coding'][1]['display'] ?? null,
                                    'component_coding_1_system' => $observationNode['resource']['component'][0]['code']['coding'][0]['system'] ?? null,
                                    'component_coding_1_code' => $observationNode['resource']['component'][0]['code']['coding'][0]['code'] ?? null,
                                    'component_coding_1_display' => $observationNode['resource']['component'][0]['code']['coding'][0]['display'] ?? null,
                                    'component_value_coding_1_system' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['system'] ?? null,
                                    'component_value_coding_1_code' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['code'] ?? null,
                                    'component_value_coding_1_display' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['display'] ?? null,
                                    'comment' => $observationNode['resource']['comment'] ?? null,
                                    'raw' => serialize($observationNode)
                                ]);

                                if (isset($observationNode['resource']['related'][0]['type']) && $observationNode['resource']['related'][0]['type'] === 'derived-from') {
                                    $observationRow['genotype_reference'] = $observationNode['resource']['related'][0]['target']['reference'];
                                }
                            }

                            $observationsClean[] = $observationRow;
                        } else if ($observationMatch && !is_null($observationCollection) && $isGenotype) {
                            $observationId = $observationMatches[1];
                            $observationNode = $observationCollection->firstMatch([
                                'resource.id' => $observationId
                            ]);

                            $observationRow = [
                                'reference' => $implicated['reference'],
                                'display' => $implicated['display'],
                                'type' => 'genotype',
                            ];

                            if ($observationNode) {
                                // remove patient reference
                                unset($observationNode['resource']['subject']);

                                $observationRow = array_merge($observationRow, [
                                    'tsi_observation_id' => $observationNode['resource']['id'],
                                    'label_system' => $observationNode['resource']['code']['coding'][0]['system'] ?? null,
                                    'label_code' => $observationNode['resource']['code']['coding'][0]['code'] ?? null,
                                    'label_display' => $observationNode['resource']['code']['coding'][0]['display'] ?? null,
                                    'coding_text' => $observationNode['resource']['code']['coding']['text'] ?? null,
                                    'value_string' => $observationNode['resource']['valueString'] ?? null,
                                    'component_coding_1_system' => $observationNode['resource']['component'][0]['code']['coding'][0]['system'] ?? null,
                                    'component_coding_1_code' => $observationNode['resource']['component'][0]['code']['coding'][0]['code'] ?? null,
                                    'component_coding_1_display' => $observationNode['resource']['component'][0]['code']['coding'][0]['display'] ?? null,
                                    'component_value_coding_1_system' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['system'] ?? null,
                                    'component_value_coding_1_code' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['code'] ?? null,
                                    'component_value_coding_1_display' => $observationNode['resource']['component'][0]['valueCodeableConcept']['coding'][0]['display'] ?? null,
                                    'comment' => $observationNode['resource']['comment'] ?? null,
                                    'raw' => serialize($observationNode)
                                ]);

                            }

                            $observationsClean[] = $observationRow;
                        } else if ($observationMatch && !is_null($observationCollection)) {
                            $observationId = $observationMatches[1];
                            $observationNode = $observationCollection->firstMatch([
                                'resource.id' => $observationId
                            ]);

                            $observationRow = [
                                'reference' => $implicated['reference'],
                                'display' => $implicated['display'],
                                'type' => 'other',
                            ];

                            if ($observationNode) {
                                throw new \Exception('New type of observation');
                            }

                            $observationsClean[] = $observationRow;
                        }
                    }

                    // set associations
                    $issueClean['result_citations'] = $citationsClean;
                    $issueClean['result_genes'] = $genesClean;
                    $issueClean['result_observations'] = $observationsClean;
                    $issueClean['result_medications'] = $medicationsClean;

                    $issuesClean[] = $issueClean;
                }
            }

            $formattedReportData['results'] = $issuesClean;

            $labOrderReport = $labOrderReportsTable->patchEntity(
                $labOrderReport,
                self::replaceKeys($formattedReportData),
                [
                    'associated' => [
                        'Results.ResultCitations',
                        'Results.ResultObservations',
                        'Results.ResultGenes',
                        'Results.ResultMedications',
                    ]
                ]
            );

            // if we are only generating consolidated data (post-processing and to not override reports)
            if ($generateConsolidatedOnly) {

                $checkExistingRecords = true;
                $this->saveConsolidatedData($labOrderId, $consolidatedReportData, $checkExistingRecords, $io);

                return true;
            }

            if (!$labOrderReportsTable->save($labOrderReport, [
                'associated' => [
                    'Results.ResultCitations',
                    'Results.ResultObservations',
                    'Results.ResultGenes',
                    'Results.ResultMedications',

                    //'ThrombosisProfile',
                    //'Results.Genes.Rsids',
                    //'Results.Categories'
                ]
            ])) {
                // TODO: log that the report couldn't be saved
                debug('could not save');
                return false;
            } else {
                debug('creating consolidated records');
                $checkExistingRecords = false;
                $this->saveConsolidatedData($labOrderId, $consolidatedReportData, $checkExistingRecords);

                // generate editable detected issues for the MAP review
                $labOrders = new LabOrders();
                $labOrders::generateNoteEntitiesForResults($labOrderReport->id);

                // generate medications that weren't found in TSI as results
                $labOrders::generateLabOrderNoteEntitiesForResults($labOrderReport->id);
            }

            return $labOrderReport->id;
        } else {
            return false;
        }
    }

    /**
     * @param int $labOrderId
     * @param array $consolidatedReportData
     *
     * @return void
     */
    private function saveConsolidatedData(
        int $labOrderId,
        array $consolidatedReportData,
        bool $checkExistingRecords = false,
        $io = false
    ) {
        $start = microtime();

        // create a consolidated record for the API to pull from (this is due to Benecard's 300ms response time requirement)
        $consolidatedGeneticReportsTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.ConsolidatedGeneticReports');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $consolidatedGeneticReportDrugsTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.ConsolidatedGeneticReportDrugs');
        $consolidatedGeneticReportEntity = $consolidatedGeneticReportsTable->newEntity();

        $labOrder = $labOrdersTable->get($labOrderId, [
            'contain' => [
                'Patient'
            ]
        ]);
        $consolidatedGeneticReportEntity->member_id = $labOrder->patient->getMemberId();

        if ($checkExistingRecords) {
            $existingRecords = $consolidatedGeneticReportsTable
                ->find()
                ->where([
                    'member_id' => $consolidatedGeneticReportEntity->member_id
                ])
                ->enableHydration(false)
                ->toArray();

            // attempt to detect prior consolidated records
            if ($io instanceof ConsoleIo && !empty($existingRecords)) {
                $io->out(count($existingRecords) . ' records found for ' . $consolidatedGeneticReportEntity->member_id);

                foreach ($existingRecords as $index => $record) {
                    debug($record);
                    $count = $consolidatedGeneticReportDrugsTable
                        ->find()
                        ->where([
                            'consolidated_genetic_report_id' => $record['id']
                        ])
                        ->count();

                    $io->out('Consolidated Report #' . ($index+1) . ' has ' . $count . ' records.');

                    if ($io->ask('Delete this consolidated report? [y/n]' === 'y')) {

                        $deleted = $consolidatedGeneticReportDrugsTable->deleteAll([
                            'consolidated_genetic_report_id' => $record['id']
                        ]);

                        $io->out($deleted);
                    }
                }
            }
        }

        $consolidatedGeneticReportEntity = $consolidatedGeneticReportsTable->save($consolidatedGeneticReportEntity);

        foreach ($consolidatedReportData as $row) {
            $entity = $consolidatedGeneticReportDrugsTable->newEntity();
            $entity->consolidated_genetic_report_id = $consolidatedGeneticReportEntity->id;
            $entity = $consolidatedGeneticReportDrugsTable->patchEntity($entity, $row);
            $consolidatedGeneticReportDrugsTable->save($entity);
        }

        debug('Created ' . count($consolidatedReportData) . ' consolidated report drug records');

        $end = microtime(true);

        debug('finished consolidating records, took: ' . ($end-$start) . ' seconds.');
    }


    /**
     * @param array $input
     * @return array
     */
    protected static function replaceKeys(array $input)
    {
        $return = [];
        foreach ($input as $key => $value) {
            if (!ctype_lower($key)) {
                $key = \Cake\Utility\Inflector::underscore($key);
            }
            if (is_array($value)) {
                $value = self::replaceKeys($value);
            }

            $return[$key] = $value;
        }
        return $return;
    }

    /**
     * @param array $medications An array of medications
     *
     * @return array|Exception
     */
    protected static function generateMedicationStatements(array $medications, string $date) {
        /*
        $validMedications = true;
        foreach ($medications as $medication) {
            if (
                (is_null($medication->medication_id) && is_null($medication->rxcui_id)) ||
                !isset($medication->drug) ||
                $medication->drug['is_searchable'] != 1
            ) {
                debug($medication);
                $validMedications = false;
                break;
            }
        }

        if ($validMedications === false) {
            throw new \Exception('Invalid medications - possibly bound before drugs.is_searchable update');
        }
        */

        $medicationStatements = [];
        foreach ($medications as $medication) {
            // skip medications that havent been mapped
            if (
                (is_null($medication->medication_id) && is_null($medication->rxcui_id)) ||
                !isset($medication->drug) ||
                $medication->drug['is_searchable'] != 1
            ) {
                continue;
            }

            if (!is_null($medication->medication_id)) {
                $medicationEntity = TableRegistry::getTableLocator()->get('SystemManagement.Medications')->find()
                    ->where([
                        'Medications.id' => $medication->medication_id
                    ])
                    ->firstOrFail();

                $medicationStatements[] = [
                    'resourceType' => 'MedicationStatement',
                    'medicationCodeableConcept' => [
                        'coding' => [[
                            'system' => 'TSI',
                            'code' => $medicationEntity->tsi_medication_id,
                            'display' => $medicationEntity->ingredient_title,
                        ]]
                    ],
                    'effectiveDateTime' => $date
                ];
            } else if (!is_null($medication->rxcui_id)) {
                $rxcuiEntity = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis')->find()
                    ->where([
                        'Rxcuis.id' => $medication->rxcui_id
                    ])
                    ->firstOrFail();

                $medicationStatements[] = [
                    'resourceType' => 'MedicationStatement',
                    'medicationCodeableConcept' => [
                        'coding' => [[
                            'system' => 'http://www.nlm.nih.gov/research/umls/rxnorm',
                            'code' => $rxcuiEntity->rxcui,
                            'display' => $rxcuiEntity->title,
                        ]]
                    ],
                    'effectiveDateTime' => $date
                ];
            }
        }

        return $medicationStatements;
    }
}
