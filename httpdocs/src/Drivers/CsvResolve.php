<?php

namespace App\Drivers;

use Cake\ORM\TableRegistry;

class CsvResolve extends Driver
{

    public function loadMessage($message)
    {
        // TODO: Implement loadMessage() method.
    }

    /**
     * @param \Cake\ORM\Entity $labOrderEntity
     * @param $result
     * @return bool|string[]
     */
    public function parseResponse(\Cake\ORM\Entity $labOrderEntity, $result)
    {
        if ($result) {
            $lines = explode(PHP_EOL, $result);
            $patientGeneCnvsTable = TableRegistry::getTableLocator()->get('Patients.PatientGeneCnvs');
            $patientSnpsTable = TableRegistry::getTableLocator()->get('Patients.PatientSnps');

            $driver = $patientGeneCnvsTable->getConnection()->getDriver();
            $autoQuoting = $driver->isAutoQuotingEnabled();
            $driver->enableAutoQuoting(true);

            foreach ($lines as $index => $line) {
                $data = str_getcsv($line, "\t");
                if ($index == 0) {
                    // validate columns matches what we expect
                    if (strtolower($data[1]) != 'assay id' || strtolower($data[3]) != 'ncbi snp reference') {
                        return ['error' => 'Columns do not match what is expected!'];
                    }
                    continue;
                }
                if (isset($data[3]) && isset($data[5])) {
                    $table = $patientSnpsTable;
                    $entity = $patientSnpsTable->newEntity();
                    $entity->patient_id = $labOrderEntity->patient_id;
                    $entity->key = $data[3];
                    $entity->value = $data[5];
                    $table->save($entity);
                }
            }
            $driver->enableAutoQuoting($autoQuoting);
            return true;
        }
        return ['error' => 'Result is empty!'];
    }
}
