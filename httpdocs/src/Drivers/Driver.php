<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 2/28/19
 * Time: 12:27 PM
 */

namespace App\Drivers;


abstract class Driver
{

    /**
     * @var
     */
    protected $rawMessage;
    protected $message;

    /**
     * @param $message
     * @return mixed
     */
    abstract public function loadMessage($message);

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

}