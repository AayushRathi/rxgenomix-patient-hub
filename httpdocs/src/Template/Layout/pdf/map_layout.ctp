<!doctype html>
<html>
<head>
    <?php /* cache => false is necessary for running this through the command line */ ?>
    <?php echo $this->Less->less('/less/theme-pdf.less', ['cache' => false, 'css' => ['fullBase' => true]]); ?>
    <?php echo $this->Less->less('/lab_orders/less/map.less', ['cache' => false, 'css' => ['fullBase' => true]]);?>
</head>
<body style="padding-right:50px;">
    <main class="map-main">
        <?=$this->fetch('content');?>
        <?=$this->fetch('css');?>
    </main>
</body>
</html>
