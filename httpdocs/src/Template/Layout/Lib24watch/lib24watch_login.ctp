<?php
use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <?php
    echo $this->element('Lib24watch.head', [
        'cache' => [
            'key' => 'lib24watch_head',
            'config' => 'cell_cache'
        ]
    ]);
    ?>
</head>
<body>
<div class="login-container">
    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <?php
            echo $this->fetch('content');
            ?>
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; <?= h(date('Y')); ?>
                <?= h(Configure::read('Lib24watch.site_name')); ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php
