<?php

/**
 * ValidationHelper
 *
 * Reusable validation constants and methods so for example one regex constant can be used in multiple plugins
 *
 */

namespace App\Util;

final class ValidationHelper
{
    const NON_PARENTHESIS_PHONE_REGEX = '/^(1?(-?\d{3})-?)?(\d{3})(-?\d{4})$/m';

    const PARENTHESIS_PHONE_REGEX = '/^(1?(-?\(?\d{3}\)?)-?)? ?(\d{3})(-?\d{4})$/m';
}
