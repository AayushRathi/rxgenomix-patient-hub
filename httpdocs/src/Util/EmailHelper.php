<?php

namespace App\Util;

use Lib24watch\Model\Table\Lib24watchSettingsTable;

final class EmailHelper
{
    /**
     * Get a list of rxg admin emails so RXG can be CC'd on emails
     *
     * @return null|array
     */
    public static function getRxgAdmins()
    {
        $separator = ';';

        $rxgAdmins = Lib24watchSettingsTable::readSettingStatic('rxgAdmins', 'Lib24watch', null);

        if (is_null($rxgAdmins) || empty(explode($separator, $rxgAdmins))) {
            throw new \Exception('24W setting `rxgAdmins` must be defined');
        }

        $rxgAdmins = explode($separator, $rxgAdmins);

        if (count($rxgAdmins) > 0) {
            return $rxgAdmins;
        }

        return null;
    }

    /*
     * Prepopulate the App.fullBaseUrl otherwise resources dont load in emails
     *
     * @return void
     *
     */
    public static function establishBaseUrl() {
        if (\Cake\Core\Configure::read('App.fullBaseUrl') === false) {
            $fullBaseUrl = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('fullBaseUrl', 'Lib24watch', null);
            if (is_null($fullBaseUrl)) {
                throw new \Exception('24W setting fullBaseUrl must be defined');
            }

            \Cake\Core\Configure::write('App.fullBaseUrl', 'http://' . $fullBaseUrl);
        }
    }
}
