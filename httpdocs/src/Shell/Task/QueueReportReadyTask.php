<?php

namespace App\Shell\Task;

use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Queue\Shell\Task\QueueTask;
use Queue\Shell\Task\QueueTaskInterface;

class QueueReportReadyTask extends QueueTask implements QueueTaskInterface {

    /**
     * Seq => Days Delayed
     */
    const SCHEDULE = [
        '1' => 10,
        '2' => 20,
        '3' => 70
    ];

    /**
     * @var int 
     */
    public $timeout = 20;

    /**
     * @var int
     */
    public $retries = 1;

    const MAX_ATTEMPTS = 3;

    /**
     * @param array $data
     */
    public function run(array $data, $jobId)
    {
        \Cake\Log\Log::debug('READY TASK SHELL!!');
        \Cake\Log\Log::debug($data['labOrderId']);

        $jobsTable = TableRegistry::getTableLocator()->get('Queue.QueuedJobs');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');

        $labOrder = $labOrdersTable->find()
            ->where([
                'LabOrders.id' => $data['labOrderId'],
                'LabOrders.enable_email_followup' => true,
                'LabOrders.last_sent_sequence <' => max(array_keys(self::SCHEDULE))
            ])
            ->first();

        if ($labOrder) {
            if ($labOrder->last_sent_sequence === 0) {
                $cadenceNotice = 'First Reminder';
            } else if ($labOrder->last_sent_sequence === 1) {
                $cadenceNotice = 'Second Reminder';
            } else if ($labOrder->last_sent_sequence === 2) {
                $cadenceNotice = 'Final Reminder';
            }
        }

        $mailer = $this->getMailer('Providers.Providers');
        $email = $labOrder->provider->email;

        $mailer->send('reportReady', [
            $email,
            $labOrder,
            $cadenceNotice
        ]);

        $updates = [
            'last_sent_sequence' => $labOrder->last_sent_sequence + 1,
            'last_sent_sequence_date'
        ];

        $labOrder = $labOrder->patchEntity(
            $labOrder,
            $updates
        );

        if (!$labOrdersTable->save($labOrder)) {
            throw new \Exception('Nope');
        }
    }
}
