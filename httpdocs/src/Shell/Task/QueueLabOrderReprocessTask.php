<?php

namespace App\Shell\Task;

use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use LabOrders\Libs\LabOrders;
use Queue\Shell\Task\QueueTask;
use Queue\Shell\Task\QueueTaskInterface;

class QueueLabOrderReprocessTask extends QueueTask implements QueueTaskInterface {

    use MailerAwareTrait;

    public function run(array $data, $jobId) {
        $jobsTable = TableRegistry::getTableLocator()->get('Queue.QueuedJobs');

        if (
            !isset($data['environment']) || !isset($data['labOrderId']) || !isset($data['email'])
        ) {
            $jobsTable->updateAll(
                ['status' => 'Insufficient data provided'],
                ['id' => $jobId]
            );

            return true;
        }

        $labOrders = new LabOrders;
        $labOrderProcessedIds = $labOrders->processLabOrders([$data['labOrderId']], [
            'singleRun' => true,
            //'updateStatus' => true,
            //'logEvent' => true,
            //'generateCoverLetter' => true,
        ], $data['environment']);

        $success = false;

        $emailMessage = 'Failed to reprocess lab order #' . $data['labOrderId'] . '. Check the event logs for more information.';
        if (!empty($labOrderProcessedIds)) {
            if (is_int($labOrderProcessedIds[0])) {
                $emailMessage = 'Successfully reprocessed lab order #' . $data['labOrderId'];
                $success = true;
            } else {
                $emailMessage = 'Failed to process lab order. Error:' . $labOrderProcessedIds[0];
            }
        }

        $mailer = $this->getMailer('LabOrders.LabOrders');
        $mailer->send('labOrderReprocess', [
            $emailMessage,
            $data['email'],
            $success
        ]);

        return true;
    }
}
