<?php

namespace App\Shell\Task;

use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use LabOrders\Libs\LabOrders;
use Queue\Shell\Task\QueueTask;
use Queue\Shell\Task\QueueTaskInterface;

class QueueReportRepullTask extends QueueTask implements QueueTaskInterface 
{
    /**
     * @var int
     */
    public $retries = 1;

    use MailerAwareTrait;

    public function run(array $data, $jobId) {
        $jobsTable = TableRegistry::getTableLocator()->get('Queue.QueuedJobs');

        if (
            !isset($data['environment']) || !isset($data['labOrderId']) || !isset($data['email'])
        ) {
            $jobsTable->updateAll(
                ['status' => 'Insufficient data provided'],
                ['id' => $jobId]
            );

            return true;
        }

        $labOrders = new LabOrders;
        $labOrderReportId = $labOrders->getPgxReport($data['labOrderId'], [
            'updateStatus' => true,
            'logEvent' => true,
            'generateCoverLetter' => true,
        ], $data['environment']);

        $emailMessage = 'Failed to repull report.';
        if ($labOrderReportId) {
            $emailMessage = 'Successfully repulled report for lab order #' . $data['labOrderId'] . '.';
        }

        $mailer = $this->getMailer('LabOrders.LabOrders');
        $mailer->send('repullProcess', [
            $emailMessage,
            $data['email']
        ]);

        return true;
    }
}
