<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Lib24watch\Model\Table\Lib24watchSettingsTable;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        if ($this->getRequest()->getParam('prefix') !== 'admin') {
            $this->defaultHelpers['Form']['templates'] = 'Lib24watch.default_form';
        }

        $this->loadComponent('RequestHandler', [
            //'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->loadTheme();
    }

    /**
     * Load project theme
     */
    private function loadTheme()
    {
        if ($this->getRequest()->getParam('prefix', null) !== "admin") {
            $this->viewBuilder()->setTheme('RxgTheme');
            $this->loadComponent("RxgTheme.Theme");
        }
    }

    /**
     * @param Event $event
     *
     * @return void
     * @throws \Exception
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        if (!is_null($this->viewBuilder()->getTheme()) &&
            $this->viewBuilder()->getTheme() !== "Lib24watch" &&
            \Cake\Core\App::classname($this->viewBuilder()->getTheme() . ".Theme", "Controller/Component", "Component")
        ) {
            $this->registerHelpers();
        }

        /**
         * Override 24Watch Login layout
         */
        if ($this->getRequest()->getParam('plugin') === 'Lib24watch' &&
            $this->getRequest()->getParam('controller') === 'Login' &&
            $this->getRequest()->getParam('action') !== 'lock'
        ) {
            // src/Template/Layout/Lib24watch/
            $this->viewBuilder()->setLayoutPath('Lib24watch');
        }
    }

    /**
     * Load Helpers specific to Rxg
     *
     * @return bool
     */
    protected function registerHelpers()
    {
        $helperItems = $this->viewBuilder()->getHelpers();
        $widgets = $helperItems['Form']['widgets'];

        $this->viewBuilder()->setHelpers(
            [
                'RxgTheme.SassCompiler',
                'Form' => [
                    'templates' => 'app_form',
                    'widgets' => $widgets
                ]
            ]
        );

        return true;
    }
}
