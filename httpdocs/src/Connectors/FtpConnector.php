<?php

namespace App\Connectors;

use FileRaven\Processor\FtpConnection;
use Cake\Filesystem\File;

class FtpConnector extends Connector
{

    /**
     * @var string
     */
    protected $connectionType = 'ftp';

    /**
     * @param array $params
     * @return FtpConnector
     * @throws \Exception
     */
    public function createConnection(array $params)
    {
        $this->connection = new FtpConnection(
            $params['host'],
            $params['username'],
            $params['password'],
            $params['submit_path'],
            $this->connectionType,
            $params['port']
        );
        return $this;
    }

    /**
     * @param $params
     * @param $data
     * @return bool|void
     */
    public function post($params, $data)
    {
        $file = new File($params['local_file'], true, 644);

        $result = $file->write($data, 'w', true);
        if ($result) {
            $result = $this->connection->uploadFtp($params['remote_file'], $params['local_file']);
            $this->connection->terminateConnection();
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param $params
     * @return bool
     * @throws \Exception
     */
    public function get($params)
    {
        $localFile = $params['local_file'];
        //$localFile .= $params['local_file'] . '_' . time();
        $remoteFile = $params['remote_file'];
        $result = $this->connection->downloadFile($localFile, $remoteFile);
        $this->connection->terminateConnection();
        if (!$result) {
            return false;
        }
        $file = new File($localFile);
        if ($file->exists()) {
            return $file->read();
        } else {
            return false;
        }
    }

    /**
     * @param string $remoteDirectory
     * @return false|array
     */
    public function getList(string $remoteDirectory)
    {
        $result = $this->connection->getFilesList($remoteDirectory);
        $this->connection->terminateConnection();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
}
