<?php

namespace App\Connectors;

use Cake\Http\Client;

class RestConnector extends Connector
{
    /**
     * @var
     */
    protected $format;

    /**
     * @var
     */
    protected $type;

    /**
     * @var array
     */
    protected $standardContentTypes = ['application/json', 'application/xml', 'application/pdf'];

    /**
     * @var
     */
    protected $response;

    /**
     * @param $params
     * @return $this
     * @throws \Exception
     */
    public function createConnection($params)
    {
        $scope = [
            'host' => $params['host'],
            'scheme' => $params['scheme'],
            'port' => $params['port'],
        ];

        if (isset($params['headers'])) {
            $scope['headers'] = $params['headers'];
        }

        if (isset($params['timeout'])) {
            $scope['timeout'] = $params['timeout'];
        }

        switch ($params['authentication_type_id']) {
            case 1: // basic auth
                $scope['auth'] = ['username' => $params['username'], 'password' => $params['password']];
                break;
            case 2:
            case 3:
                $scope['headers']['Content-Type'] = 'application/json';
                $scope['headers']['Accept'] = 'application/json';
                $authClient = new Client($scope);

                $response = $authClient->post($params['authentication_path'], json_encode([
                    'grant_type' => 'password',
                    'username' => $params['username'],
                    'password' => $params['password'],
                    'client_id' => 'rxgenomix'
                ]), ['type' => 'json']);
                if ($result = $response->getJson()) {
                    $token = $result['access_token'];
                    $this->format = 'getJson';
                    $this->type = 'json';
                } elseif ($result = $response->getXml()) {
                    $token = $result->access_token;
                    $this->format = 'getXml';
                    $this->type = 'xml';
                } else {
                    throw new \Exception('Unknown response for token issuance.');
                }
                $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);
                break;
            case 4:
                $scope['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
                $scope['headers']['Accept'] = 'application/json';
                $authClient = new Client($scope);

                $response = $authClient->post($params['authentication_path'], [
                    'grant_type' => 'password',
                    'username' => $params['username'],
                    'password' => $params['password'],
                ], ['type' => 'json']);

                if ($result = $response->getJson()) {
                    $token = $result['access_token'];
                    $this->format = 'getJson';
                    $this->type = 'json';
                } else {
                    throw new \Exception('Unknown response for token issuance.');
                }
                $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);
                break;
        }

        $this->connection = new Client($scope);
        return $this;
    }

    /**
     * @param $params
     * @param $data
     * @return mixed
     */
    public function post($params, $data)
    {
        if (is_array($data)) {
            $data = json_encode($data);
        }
        $this->response = $this->connection->post($params['submit_path'], $data, ['type' => $this->type]);
        return $this->response->{$this->format}();
    }

    /**
     * @param $params
     * @param array $data
     * @return mixed
     */
    public function get($params, $data = [])
    {
        $response = $this->connection->get($params['request_path'], $data);
        // exception for non json / xml content types
        if (!in_array($response->getHeaderLine('content-type'), $this->standardContentTypes)) {
            return $response->getStringBody();
        } else {
            return $response;
        }
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

}
