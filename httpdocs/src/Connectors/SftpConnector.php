<?php

namespace App\Connectors;

class SftpConnector extends FtpConnector
{
    protected $connectionType = 'sftp';
}