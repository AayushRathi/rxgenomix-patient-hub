<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 3/14/19
 * Time: 9:28 AM
 */

namespace App\Connectors;


abstract class Connector
{
    /**
     * @var
     */
    protected $connection;

    public function post($params, $data) {}

    public function get($params) {}
}