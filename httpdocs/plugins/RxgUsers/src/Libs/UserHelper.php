<?php

namespace RxgUsers\Libs;

final class UserHelper {

    /**
     * Check if the user has access to a specific workflow indiciated by lab_order_initiator_type_id
     *
     * @param int $initiatorTypeId
     * @param array $user
     *
     * @return bool
     */
    public static function hasInitiatorTypeId(int $initiatorTypeId, $user) {
        if (isset($user['lab_order_initiator_types']) && !empty($user['lab_order_initiator_types'])) {
            foreach ($user['lab_order_initiator_types'] as $type) {
                if ($initiatorTypeId == $type['id']) {
                    return true;
                }
            }
        }

        return false;
    }
}
