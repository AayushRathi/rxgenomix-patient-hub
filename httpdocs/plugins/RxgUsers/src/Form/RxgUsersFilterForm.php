<?php

namespace RxgUsers\Form;

use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\Validation\Validator;
use Lib24watch\Form\FilterForm;

class RxgUsersFilterForm extends FilterForm
{

    /**
     * Your subclass must implement this method. It will receive the query to
     * be filtered, and an array of $criteria from the form. It should apply
     * where(), matching(), etc. to $query as appropriate, based on $criteria.
     *
     * Note that empty and whitespace-only values have already been removed
     * from $criteria, so you don't need to test each criterion manually.
     *
     * @param Query $query
     * @param array $criteria
     * @return void
     */
    protected function filterQuery(Query $query, array $criteria)
    {
        if (isset($criteria['provider_type_id'])) {
            $query->where(
                [
                    'Provider.provider_type_id' => $criteria['provider_type_id']
                ]
            );
        }

        if (isset($criteria['npi_number'])) {
            $query->where(
                [
                    'Provider.npi_number LIKE' => $this->escapeLikeCondition($criteria['npi_number']),
                ]
            );
        }

        if (isset($criteria['last_name'])) {
            $query->where(
                [
                    'Users.last_name LIKE' => $this->escapeLikeCondition($criteria['last_name'])
                ]
            );
        }

        if (isset($criteria['first_name'])) {
            $query->where(
                [
                    'Users.first_name LIKE' => $this->escapeLikeCondition($criteria['first_name'])
                ]
            );
        }

        if (isset($criteria['email'])) {
            $query->where(
                [
                    'Users.email LIKE' => $this->escapeLikeCondition($criteria['email'])
                ]
            );
        }

        if (isset($criteria['active'])) {
            $query->where(
                [
                    'Users.active' => $criteria['active']
                ]
            );
        }
    }
}
