<?php
namespace RxgUsers\Controller;

use Cake\Core\Configure;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Utility\Inflector;
use Cake\Validation\Validator;
use CakeDC\Users\Controller\Component\UsersAuthComponent;
use CakeDC\Users\Controller\Traits\LoginTrait;
use CakeDC\Users\Controller\Traits\RegisterTrait;
use CakeDC\Users\Controller\Traits\ProfileTrait;
use CakeDC\Users\Controller\Traits\ReCaptchaTrait;
use CakeDC\Users\Controller\Traits\SimpleCrudTrait;
use Cake\Event\Event;
use Cake\Cache\Cache;
use CakeDC\Users\Exception\TokenExpiredException;
use CakeDC\Users\Exception\UserAlreadyActiveException;
use CakeDC\Users\Exception\UserNotFoundException;
use CakeDC\Users\Exception\WrongPasswordException;
use Providers\Libs\TokenHandler;
use Exception;

class RxgUsersController extends AppController {

    use ProfileTrait {
        profile as baseProfile;
    }
    use SimpleCrudTrait {
        edit as editProfile;
    }
    use LoginTrait {
        login as baseLogin;
    }
    use RegisterTrait {
        requestResetPassword as baseRequestResetPassword;
        register as baseRegister;
        validateEmail as baseValidateEmail;
    }
    use ReCaptchaTrait;
    use MailerAwareTrait;

    /**
     * @return array
     */
    public function implementedEvents()
    {
        return [
                'Users.Component.UsersAuth.beforeRegister' => 'beforeRegister',
                'Users.Component.UsersAuth.afterRegister' => 'afterRegister',
                'Users.Component.UsersAuth.afterLogin' => 'afterLogin',
                'Users.Component.UsersAuth.afterResetPassword' => 'afterResetPassword'
            ] + parent::implementedEvents();
    }

    /*
     * Unset force password reset
     *
     */
    public function afterResetPassword($event)
    {
        $forcePasswordResetSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('enableForcePasswordReset', 'LabOrders', false);

        if ($forcePasswordResetSetting) {
            $user = $this->Users->get($event->data['user']['id']);

            $securitySettingsTable = TableRegistry::getTableLocator()->get('SystemManagement.SecuritySettings');
            $passwordCadenceDays = $securitySettingsTable
                ->find()
                ->where([
                    'setting_key' => 'password_cadence'
                ])
                ->extract('setting_value')
                ->first();

            if (is_null($passwordCadenceDays) || $passwordCadenceDays < 0 || $passwordCadenceDays > 365) {
                throw new \Exception('Invalid password cadence');
            }

            $today = new \Cake\I18n\Date;
            $today->modify('+' . $passwordCadenceDays . ' days');

            $user['force_password_reset'] = 0;
            $user['password_expires'] = $today;

            $this->Users->save($user);
        }
    }

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        if (!isset($this->ContentRegionLoader)) {
            $this->loadComponent('ContentRegions.ContentRegionLoader');
        }

        if ($this->ContentRegionLoader->regionExists('tos')) {
            $this->ContentRegionLoader->loadRegions(['tos']);
        }

        if ($this->ContentRegionLoader->regionExists('login_title_html')) {
            $this->ContentRegionLoader->loadRegions(['login_title_html']);
        }

        if ($this->ContentRegionLoader->regionExists('login_register_html')) {
            $this->ContentRegionLoader->loadRegions(['login_register_html']);
        }

        if ($this->ContentRegionLoader->regionExists('login_join_hub_html')) {
            $this->ContentRegionLoader->loadRegions(['login_join_hub_html']);
        }

        if ($this->ContentRegionLoader->regionExists('register_complete_html')) {
            $this->ContentRegionLoader->loadRegions(['register_complete_html']);
        }

        if ($this->ContentRegionLoader->regionExists('register_required_fields_html')) {
            $this->ContentRegionLoader->loadRegions(['register_required_fields_html']);
        }

        parent::initialize();

        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->loadComponent('AddressVerification.AddressVerification');
        $this->loadModel('CakeDC/Users.Users');
        $this->loadModel('RxgUsers.RxgUsers');
        $this->loadModel('Providers.Providers');

        $id = $this->Auth->user('id');
        $providerEntity = $this->Providers->find()->where(['rxg_user_id' => $id])->first();

        $providerStatus = 0;
        if ($providerEntity) {
            $providerStatus = $providerEntity->provider_status_id;
        }
        $this->set('providerStatus', $providerStatus);
    }

    /**
     * @param Event $event
     * @return void|parent
     * @throws \Exception
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['register', 'login', 'resetPassword', 'validateEmail', 'loginAs']);
    }

    /**
     * @param Event $event
     * @return mixed|void
     */
    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);

        $this->viewBuilder()->setHelpers(
            [
                'CakeDC/Users.User',
                'Form' => [
                    'errorClass' => 'form-error form-control',
                    'templates' => 'users_form',
                    'widgets' => [
                        "autocomplete" => [
                            "Lib24watch\View\Widget\AutocompleteWidget",
                            "text",
                            "_view"
                        ],
                        'datetimepicker' => [
                            'Lib24watch\View\Widget\DateTimePickerWidget',
                            'select',
                            '_view'
                        ]
                    ]
                ]
            ]
        );
    }

    /**
     * @param $event
     */
    public function afterLogin($event)
    {
        $user = $this->Users->get($event->data['user']['id']);
        $user['last_logged_in'] = date('Y-m-d H:i:s', time());
        $this->Users->save($user);
    }

    /**
     *
     */
    public function profile()
    {
        $this->baseProfile();

        $providerEntity = TableRegistry::getTableLocator()->get('Providers.Providers')
            ->find()
            ->where([
                'rxg_user_id' => $this->Auth->user('id')
            ])
            ->firstOrFail();

        $this->set(compact('providerEntity'));
    }

    /**
     *
     */
    public function login()
    {
        $this->baseLogin();
        $this->set('suppressThemeFlashMessages', true);
        $this->set('hideTitle', true);
    }

    /**
     * @return \Cake\Http\Response|void|null
     */
    public function edit()
    {
        $id = $this->Auth->user('id');

        //copy and pasted from CakeDC SimpleCrudTrait to avoid redirecting to list action
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => ['Provider']
        ]);
        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
        if (!$this->request->is(['patch', 'post', 'put'])) {
            return;
        }
        $entity = $table->patchEntity(
            $entity,
            $this->request->getData(),
            [
                'validate' => 'default',
                'associated' => [
                    'Provider' => [
                        'validate' => false
                    ]
                ]
            ]
        );

        if ($this->request->getData()) {
            if (!filter_var($this->request->getData('email'), FILTER_VALIDATE_EMAIL)) {
                $entity->setError('email', 'That is not a valid email address.');
            }
        }
        $singular = 'user';
        if (empty($entity->getErrors())) {
            if ($table->save($entity)) {
                $this->Flash->success(__d('CakeDC/Users', 'The {0} has been saved', $singular));

                return $this->redirect(['action' => 'profile']); // overridden redirect
            } else {
                $this->Flash->error(__d('CakeDC/Users', 'The {0} could not be saved', $singular));
            }
        }
    }

    /**
     *
     */
    public function register()
    {
        $this->baseRegister();

        $params = $this->getRequest()->getQueryParams();

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('lib24watchStateOptions', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->toArray());

        $this->loadModel('Lib24watch.Lib24watchCountries');
        $this->set('lib24watchCountryOptions', $this->Lib24watchCountries->find('list', [
            'keyField' => 'country_code',
            'valueField' => 'title'
        ])->toArray());

        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1
        ])->toArray());

        $this->loadModel('Providers.ProviderTypes');
        $this->set('providerTypes', $this->ProviderTypes->find('dropdown'));

        $email = '';
        if(isset($params['email']) && $params['email']) {
            $email = $params['email'];
        }
        $this->set('email', $email);

        $this->loadModel('HealthCareClients.HealthCareClientGroups');
        $healthCareClientGroups = [];
        $groups = $this->HealthCareClientGroups->find();
        foreach ($groups as $group) {
            $healthCareClientGroups[$group->health_care_client_id][$group->id] = $group->title;
        }
        $this->set('healthCareClientGroups', $healthCareClientGroups);

        $role = '';
        if(isset($params['role']) && $params['role']) {
            $role = $params['role'];
        }
        $this->set('role', $role);
    }

    /**
     * @param $event
     * @return |null
     */
    public function beforeRegister($event)
    {
        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();
            /*
            $verifiedResult = $this->AddressVerification->verify(
                $data['address_1'],
                $data['address_2'],
                $data['city'],
                $data['lib24watch_state_abbr'],
                $data['postal_code']
            );

            if(isset($verifiedResult['Error'])){
                \Cake\Log\Log::debug('Address verification failed for ' . $data['email']);
                $data['address_verified'] = false;
                $data['address_verification_error'] = join(', ', $verifiedResult['Error']);
             */

            $data['address_verified'] = true;
            $data['active'] = 1;

            $event->setData($data);

            return $event;
        }

        return null;
    }

    /**
     * @param $event
     * @return \Cake\Http\Response|null
     */
    public function afterRegister($event)
    {
        if ($this->getRequest()->getData()) {
            $user = $event->data['user'];
            $data = $this->getRequest()->getData();

            $provider = $this->Providers->newEntity();

            // set this to the reformatted phone number
            // TODO: possibly patchEntity / validate $provider?
            if (isset($user->phone) && mb_strlen($user->phone) > 0) {
                $data['phone'] = $user->phone;
            }

            $data['provider_status_id'] = 1;
            $data['is_active'] = 1;
            $data['rxg_user_id'] = $user->id;

            $provider = $this->Providers->patchEntity(
                $provider,
                $data,
                [
                    'validate' => 'default'
                ]
            );

            // extend registration token to 1 day
            $rxgUser = $this->RxgUsers->find()->where([
                'id' => $user->id
            ])->first();

            if (
                $rxgUser &&
                isset($rxgUser->token_expires) &&
                $rxgUser->token_expires instanceof \Cake\I18n\FrozenTime
            ) {
                \Cake\Log\Log::debug($rxgUser->token_expires);

                $time = \Cake\I18n\Time::createFromFormat(
                    'Y-m-d H:i:s',
                    $rxgUser->token_expires->format('Y-m-d H:i:s'),
                    'UTC'
                );

                $time->addDays(1);

                \Cake\Log\Log::debug($time);

                $rxgUser = $this->RxgUsers->patchEntity(
                    $rxgUser,
                    [
                        'token_expires' => $time
                    ],
                    [
                        'validate' => false
                    ]
                );

                if (!$this->RxgUsers->save($rxgUser)) {
                    $message = 'The user could not be saved.';
                    $this->Flash->error($message);
                    return $this->redirect(['action' => 'register']);
                }
            }

            if (empty($provider->getErrors())) {
                $this->Providers->save($provider);

                // set the initiator types
                $this->loadModel('LabOrders.LabOrderInitiatorTypesUsers');
                foreach ([2,3] as $typeId) {
                    $initTypeUser = $this->LabOrderInitiatorTypesUsers->newEntity();
                    $initTypeUser->user_id = $rxgUser->id;
                    $initTypeUser->lab_order_initiator_type_id = $typeId;
                    $this->LabOrderInitiatorTypesUsers->save($initTypeUser);
                }
                // Clear dashboard cache
                Cache::clear('dashboard_Providers');

                // TODO: possibly load this from content region, though maybe not since there is a text wildcard with first_name
                $message = 'Thank you for submitting your information, ' . $user['first_name'] .'. Next we’ll need your help to validate
                    the account, so we’ve sent an email to the address you used to register. Just open the email and click the
                    link to validate, or you can copy and paste the full address to your browser. If you do not receive the email in your inbox after a few minutes, please check your junk/spam folder. The link will expire within 30 minutes of initial registration. You will not be able to log in if you do not activate your account through the link in the email. If you need assistance activating your account, please contact <a href="mailto:info@rxgenomix.com">info@rxgenomix.com</a>.';

                $this->Flash->success($message, ['escape' => false]);
                return $this->redirect(['action' => 'login']);
            } else {
                \Cake\Log\Log::debug($provider->getErrors());
                $message = 'The user could not be saved.';
                $this->Flash->error($message);
                return $this->redirect(['action' => 'register']);
            }
        }
    }

    /**
     *
     */
    public function requestResetPassword()
    {
        if ($this->getRequest()->getData()) {
            $rxgUser = $this->RxgUsers->find()->where([
                'email' => $this->getRequest()->getData('reference')
            ])->first();
            if (!$rxgUser) {
                $msg = 'Email address not found.';
                $this->Flash->error($msg);
            } else {
                $this->baseRequestResetPassword();
            }
        }
    }

    public function accountCreated(){

    }

    /**
     * Validate an email
     *
     * @param string $token token
     * @return void
     */
    public function validateEmail($token = null)
    {
        $validatedSuccessfully = 'Success! Your information has been saved, your account has been validated and an
        RxGenomix specialist will review your information shortly. Your registration will need to be approved before any
        lab orders can be processed by the lab, but you can go ahead and enter the information to save new orders and
        more. As soon as your registration is approved, any saved orders will be converted to active orders and sent
        to the laboratory for processing.';

// TODO: discover method for loading content region values in controller/component
//        if ($this->ContentRegionLoader->regionExists('email_validated')) {
//            $this->ContentRegionLoader->loadRegions(['email_validated']);
//            $validatedSuccessfully = $this->ContentRegionLoader->getRegion(['email_validated']);
//        }

        try {
            try {
                $result = $this->getUsersTable()->validate($token, 'activateUser');
                if ($result) {
                    $this->Flash->success(__d('CakeDC/Users', $validatedSuccessfully));

                    $userEntity = $this->getUsersTable()
                        ->find()
                        ->where([
                            'id' => $result->id
                        ])
                        ->firstOrFail();

                    // send account awaiting approval email
                    $mailer = $this->getMailer('RxgUsers.RxgUsers');
                    $mailer->send(
                        'accountAwaitingApproval',
                        [
                            $userEntity
                        ]
                    );

                } else {
                    $this->Flash->error(__d('CakeDC/Users', 'User account could not be validated'));
                }
            } catch (UserAlreadyActiveException $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'User already active'));
            }
        } catch (UserNotFoundException $ex) {
            $this->Flash->error(__d('CakeDC/Users', 'Invalid token or user account already validated'));
        } catch (TokenExpiredException $ex) {
            $event = $this->dispatchEvent(UsersAuthComponent::EVENT_ON_EXPIRED_TOKEN, ['type' => 'email']);
            if (!empty($event) && is_array($event->result)) {
                return $this->redirect($event->result);
            }
            $this->Flash->error(__d('CakeDC/Users', 'Token already expired'));
        }

        return $this->redirect(['action' => 'login']);
    }

    /**
     * Change password
     *
     * @return mixed
     */
    public function changePassword()
    {
        $user = $this->getUsersTable()->newEntity();
        $id = $this->Auth->user('id');
        if (!empty($id)) {
            $user->id = $this->Auth->user('id');
            $validatePassword = true;
            //@todo add to the documentation: list of routes used
            $redirect = Configure::read('Users.Profile.route');
        } else {
            $user->id = $this->request->getSession()->read(Configure::read('Users.Key.Session.resetPasswordUserId'));
            $validatePassword = false;
            if (!$user->id) {
                $this->Flash->error(__d('CakeDC/Users', 'User was not found'));
                $this->redirect($this->Auth->getConfig('loginAction'));

                return;
            }
            //@todo add to the documentation: list of routes used
            $redirect = $this->Auth->getConfig('loginAction');
        }
        $this->set('validatePassword', $validatePassword);
        if ($this->request->is('post')) {
            try {
                $validator = $this->getUsersTable()->validationPasswordConfirm(new Validator());
                $validator = $this->getUsersTable()->validationPasswordStrength($validator);
                if (!empty($id)) {
                    $validator = $this->getUsersTable()->validationCurrentPassword($validator);
                }
                $user = $this->getUsersTable()->patchEntity(
                    $user,
                    $this->request->getData(),
                    ['validate' => $validator]
                );
                if ($user->getErrors()) {
                    $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                } else {
                    $result = $this->getUsersTable()->changePassword($user);
                    if ($result) {
                        $event = $this->dispatchEvent(UsersAuthComponent::EVENT_AFTER_CHANGE_PASSWORD, ['user' => $result]);
                        if (!empty($event) && is_array($event->result)) {
                            return $this->redirect($event->result);
                        }
                        $this->Flash->success(__d('CakeDC/Users', 'Password has been changed successfully'));

                        return $this->redirect($redirect);
                    } else {
                        $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                    }
                }
            } catch (UserNotFoundException $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'User was not found'));
            } catch (WrongPasswordException $wpe) {
                $this->Flash->error($wpe->getMessage());
            } catch (Exception $exception) {
                $this->Flash->error(__d('CakeDC/Users', 'Password could not be changed'));
                $this->log($exception->getMessage());
            }
        }

        $expired = false;
        if (!is_null($this->getRequest()->getQuery('expired'))) {
            $expired = true;
        }

        $this->set(compact('expired', 'user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Log In As Site Owner
     *
     * Method that handles token parameter for instant login,
     * if token param is passed it auto-logs administration
     * users in, checking against the lib24watch_user_tokens table
     * deletes tokens afterwards
     *
     * @param null|string $tokenValue base64_encoded token value
     *
     * @return mixed
     */
    public function loginAs($tokenValue = null)
    {
        if (is_null($tokenValue)) {
            throw new \Cake\Http\Exception\NotFoundException();
        }

        $this->loadModel('Lib24watch.Lib24watchUserTokens');
        $tokenHandler = new TokenHandler();

        if ($tokenValue !== null) {
            $tokenEntity = $this->Lib24watchUserTokens->find()
                ->where(['token_value' => $tokenValue])
                ->first();

            $userUuid = $tokenHandler->decodeToken($tokenValue);

            // get user, only site owners can be logged in as (for now), if not, remove Sites.owner_user_id matching
            #$providerEntity = TableRegistry::getTableLocator()->get('Providers.Providers')
            $userEntity = TableRegistry::getTableLocator()->get('RxgUsers.RxgUsers')
                //->find('auth')
                ->find()
                ->contain([
                    'Provider',
                    'LabOrderInitiatorTypes'
                ])
                ->matching('Provider', function($q) use ($userUuid) {
                    return $q->andWhere([
                        'Provider.id' => $userUuid,
                        'Provider.is_active' => 1
                    ]);
                })
                ->where(
                    [
                        'RxgUsers.active' => 1
                    ]
                )
                ->firstOrFail();

            $userEntity->isLoggedInAs = true;

            if ($tokenEntity) {
                // check if user is already logged in
                $this->Auth->setUser($userEntity->toArray());
                $this->Lib24watchUserTokens->delete($tokenEntity);
            }
        }

        // send to dashboard, if not successful, redirects to /login
        return $this->redirect($this->Auth->redirectUrl());
    }
}
