<?php
namespace RxgUsers\Controller\Admin;

use RxgUsers\Controller\AppController;
use RxgUsers\Form\RxgUsersFilterForm;

class RxgUsersController extends AppController
{
    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'RxgUsers' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * init
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('CakeDC/Users.Users');
        $this->loadModel('Providers.ProviderTypes');
    }

    /**
     * index
     */
    public function index()
    {
        $query = $this->Users->find('all')->contain('Provider.Type');
        $filterForm = new RxgUsersFilterForm($query, true);
        $rxgUsersQuery = $this->paginate($query);
        $providers = $this->ProviderTypes->find('dropdown');
        $this->set(compact('rxgUsersQuery', 'filterForm', 'providers'));
    }

    public function edit(string $userId = null){
        $this->set('userId', $userId);

        if ($userId) {
            $userEntity = $this->Users->get($userId);
        } else {
            $this->redirectWithDefault(
                [
                    'plugin' => 'RxgUsers',
                    'controller' => 'WiaUsers',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                'User not found.'
            );
        }

        $this->set("userEntity", $userEntity);

        if ($this->request->getData()){
            // set data
            $data = $this->request->getData();

            $data['active'] = 1;

            $userEntity = $this->Users->patchEntity(
                $userEntity,
                $data
                , [
                    'validate' => false
                ]
            );

            // validate
            $errors = $userEntity->getErrors();

            // validation check
            if (empty($errors)) {
                // save data
                if (!$this->Users->save(
                    $userEntity, [
                        'associated' => []
                    ]
                )) {
                    throw new \Exception("Unable to save User");
                }

                $this->redirectWithDefault(
                    [
                        'plugin' => 'RxgUsers',
                        'controller' => 'RxgUsers',
                        'action' => 'index',
                        'prefix' => 'admin'
                    ],
                    'User updated.'
                );
            }
        }
    }
}