<?php
$pluginTitle = $lib24watchModules['RxgUsers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Users';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <?php
            echo $this->Form->create(
                $filterForm,
                [
                    'url' => [
                        'plugin' => 'RxgUsers',
                        'controller' => 'RxgUsers',
                        'action' => 'index',
                        'prefix' => 'admin'
                    ]
                ]
            );
            ?>
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="panel-title">Filter Users</h3>
                        <ul class="panel-controls">
                            <li>
                                <a class="panel-collapse" title="Hide" href="#"><span class="fa fa-angle-down"></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">

                <?php
                echo $this->Form->control(
                    'provider_type_id',
                    [
                        'type' => 'select',
                        'label' => 'Provider Type',
                        'options' => $providers,
                        'empty' => true
                    ]
                );

                echo $this->Form->control(
                    'first_name',
                    [
                        'type' => 'text',
                        'label' => 'First Name',
                    ]
                );

                echo $this->Form->control(
                    'last_name',
                    [
                        'type' => 'text',
                        'label' => 'Last Name',
                    ]
                );

                echo $this->Form->control(
                    'npi_number',
                    [
                        'type' => 'text',
                        'label' => 'NPI #',
                    ]
                );

                echo $this->Form->control(
                    'email',
                    [
                        'type' => 'text',
                        'label' => 'Email',
                    ]
                );

                echo $this->Form->control(
                    'active',
                    [
                        'type' => 'checkbox',
                        'label' => 'Active'
                    ]
                );
                ?>
            </div>
            <div class="panel-footer">
                <ul class="list-inline text-right">
                    <li>
                        <input type="submit" class="btn btn-condensed btn-default" id="apply-filter" value="Apply Filter">
                    </li>
                    <li>
                        <?php
                        echo $this->Html->link(
                            'Reset Filter',
                            [
                                'plugin' => 'RxgUsers',
                                'controller' => 'RxgUsers',
                                'action' => 'index',
                                'prefix' => 'admin',
                                '?' => [
                                    'reset' => '1'
                                ]
                            ],
                            [
                                'class' => 'btn btn-condensed btn-danger'
                            ]
                        )
                        ?>
                    </li>
                </ul>
            </div>
            <?= $this->Form->end(); ?>
        </div>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($rxgUsersQuery->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'provider.type.title',
                                        'label' => 'Provider',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'last_name',
                                        'label' => 'Last Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'first_name',
                                        'label' => 'First Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'email',
                                        'label' => 'Email Address',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'provider.npi_number',
                                        'label' => 'NPI #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'actions',
                                        'tdClass' => 'actions',
                                        'label' => '',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            $buttons = '';
                                            if($row['active']) {
                                                $buttons .= $this->Html->link(
                                                    '<span class="fa fa-minus-circle"></span> Disable',
                                                    [
                                                        'plugin' => 'RxgUsers',
                                                        'controller' => 'RxgUsers',
                                                        'action' => 'activateUser',
                                                        'prefix' => 'admin',
                                                        $row['id'],
                                                        '?' => [ 'active' => false ]
                                                    ],
                                                    [
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-warning'
                                                    ]
                                                );
                                            } else {
                                                $buttons .= $this->Html->link(
                                                    '<span class="fa fa-plus-circle"></span> Enable',
                                                    [
                                                        'plugin' => 'RxgUsers',
                                                        'controller' => 'RxgUsers',
                                                        'action' => 'activateUser',
                                                        'prefix' => 'admin',
                                                        $row['id'],
                                                        '?' => [ 'active' => true ]
                                                    ],
                                                    [
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-success'
                                                    ]
                                                );
                                            }

                                            $buttons .= $this->Html->link(
                                                '<span class="fa fa-edit"></span> Edit',
                                                [
                                                    'plugin' => 'RxgUsers',
                                                    'controller' => 'RxgUsers',
                                                    'action' => 'edit',
                                                    'prefix' => 'admin',
                                                    $row['id']
                                                ],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-default'
                                                ]
                                            );

                                            return $buttons;
                                        }
                                    ]
                                ],
                                $rxgUsersQuery
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
