<?php
$pluginTitle = $lib24watchModules['RxgUsers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Users"),
    [
        'plugin' => 'RxgUsers',
        'controller' => 'RxgUsers',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($userId ? "Edit" : "Add") . " User";
$this->Breadcrumbs->add($pageHeading);
//echo $this->Html->script('/rxg_users/js/admin/edit.js', ['block' => 'extra-scripts']);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($userEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
            				echo $this->Form->control(
            					'first_name',
            					[
            						'type' => 'text',
            						'label' => 'First Name'
            					]
            				);
            				echo $this->Form->control(
            					'last_name',
            					[
            						'type' => 'text',
            						'label' => 'Last Name',
                                    'required' => true
            					]
            				);

                            echo $this->Form->control(
                                'email',
                                [
                                    'type' => 'text',
                                    'label' => 'Email',
                                    'required' => true
                                ]
                            );
            				echo $this->Form->control(
            					'active',
            					[
            						'type' => 'checkbox',
            						'label' => 'Active'
            					]
            				);
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
