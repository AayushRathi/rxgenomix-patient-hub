<a href="javascript:void(0)"><span class="fa fa-user"></span><span class="xn-text"><?php echo h($lib24watchModules['RxgUsers']['title']); ?></span></a>
<ul>
        <li class="xn-title">Users</li>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Users",
                [
                    'plugin' => 'RxgUsers',
                    'controller' => 'RxgUsers',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'title' => 'Browse Users',
                    'escape' => false
                ]
            );
            ?>
        </li>
</ul>