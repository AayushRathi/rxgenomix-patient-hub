<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$activationUrl = [
    '_full' => true,
    'plugin' => 'RxgUsers',
    'controller' => 'RxgUsers',
    'action' => 'validateEmail',
    isset($token) ? $token : ''
];
?>
<p>
    <?= __d('CakeDC/Users', "Thank you for submitting your information, {0}. Just click the link below to validate your email address, or you can copy and paste the full address to your browser. Once you do that and your email is successfully validated, you’ll be ready to log in.", isset($first_name) ? $first_name : '') ?>
</p>
<p>
    <?= __d('CakeDC/Users', 'An RxGenomix specialist will review your information shortly. Your registration will need to be approved before any lab orders can be processed by the lab, but you can go ahead and enter the information to save new orders and more. As soon as your registration is approved, any saved orders will be converted to active orders and sent to the laboratory for processing.') ?>
</p>
<p>
    <strong><?= $this->Html->link(__d('CakeDC/Users', 'Activate your account here'), $activationUrl) ?></strong>
</p>
<?php
$activationUrlFull = $this->Url->build($activationUrl, [
    'fullBase' => true
]);
?>
<p><?= $this->Html->link($activationUrlFull, $activationUrl); ?></p>
<p>
    <?= __d('CakeDC/Users', 'Thank you') ?>,
</p>
