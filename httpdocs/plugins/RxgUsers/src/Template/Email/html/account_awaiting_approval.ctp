<?php
$dashboardUrl = [
    '_full' => true,
    'plugin' => 'Lib24watch',
    'controller' => 'Lib24watch',
    'action' => 'index',
    'prefix' => 'admin'
];
?>
    <p>INCOMING! <?=$firstName;?> <?=$lastName;?> has registered and is requesting access to the RxGenomix Hub. Please verify their credentials/licensing at your earliest convenience.
</p>
<p>
    <?= __d('CakeDC/Users', 'Thank you') ?>.
</p>
