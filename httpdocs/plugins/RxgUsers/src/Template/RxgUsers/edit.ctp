<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
use Cake\Core\Configure;

$this->assign('title', 'RxGenomix User Profile');
$Users = ${$tableAlias};
?>
<div class="users form large-10 medium-9 columns">
    <?= $this->Form->create($Users); ?>
    <div class="panel">
        <h2><?= __d('CakeDC/Users', 'Edit Profile') ?></h2>
        <?php
        echo $this->Form->control('email', ['label' => __d('CakeDC/Users', 'Email')]);
        echo $this->Form->control('first_name', ['label' => __d('CakeDC/Users', 'First name')]);
        echo $this->Form->control('last_name', ['label' => __d('CakeDC/Users', 'Last name')]);
        echo $this->Form->control(
            'provider.license_expiration_date', [
                'label' => __d('CakeDC/Users', 'License Expiration Date'),
                'type' => 'datetimepicker',
            ]);
        ?>
        <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), [ 'class' => 'btn btn-condensed btn-primary pull-right']) ?>
    </div>

    <?= $this->Form->end() ?>
</div>
