<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

use Cake\Core\Configure;

$this->assign('title', '');
?>

<div class="users form raised-container">
    <div class="center-container">
        <?= $this->Form->create() ?>
        <div class="side-menu__item panel">
            <?= $this->Flash->render() ?>
            <?php
                if ($this->ContentRegionLoader->regionExists('login_title_html')) {
                    echo $this->ContentRegionLoader->getRegion('login_title_html');
                } ?>

            <?= $this->Form->control(
                'email',
                [
                    'label' => __d('CakeDC/Users', 'Email'),
                    'required' => true,
                    'autocomplete' => 'off',
                    'readonly' => true,
                    'onfocus' => 'this.removeAttribute("readonly");',
                    'value' => ''
                ]
            ) ?>
            <?= $this->Form->control(
                'password',
                [
                    'label' => __d('CakeDC/Users', 'Password'),
                    'required' => true,
                    'autocomplete' => 'off',
                    'readonly' => true,
                    'onfocus' => 'this.removeAttribute("readonly");',
                    'value' => ''
                ]
            ) ?>
            <?php
            if (Configure::read('Users.reCaptcha.login')) {
                echo $this->User->addReCaptcha();
            }
            if (Configure::read('Users.RememberMe.active')) {
                echo $this->Form->control(Configure::read('Users.Key.Data.rememberMe'), [
                    'type' => 'checkbox',
                    'label' => __d('CakeDC/Users', 'Remember me'),
                    'checked' => Configure::read('Users.RememberMe.checked')
                ]);
            }
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->button(__d('CakeDC/Users', 'Login'), ['class' => 'btn primary btn-large', 'style' => 'width: 100%;']); ?>
                </div>
                <div class="col-md-12">
                    <?= $this->Html->link(__d('CakeDC/Users', 'Reset Password'), ['action' => 'requestResetPassword'], ['class' => 'btn secondary', 'style' => 'width: 100%;margin-top: 9px;']); ?>
                </div>
                <?php
                    $registrationActive = Configure::read('Users.Registration.active');
                    if ($registrationActive) {
                ?>
                    <div class="col-md-12">
                        <?=$this->Html->link(__d('CakeDC/Users', 'Register'), ['action' => 'register'], ['class' => 'btn primary btn-large', 'style' => 'width: 100%;margin-top: 9px;']);?>
                    </div>
                <?php
                    }
                ?>
                <?php
                    if ($this->ContentRegionLoader->regionExists('login_register_html')) {
                ?>
                    <div class="col-md-12" style="margin-top:9px;">
                        <?= $this->ContentRegionLoader->getRegion('login_register_html');?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?= implode(' ', $this->User->socialLoginList()); ?>
        <?= $this->Form->end() ?>
    </div>
</div>
<div class="row text-center">
    <div class="col-md-12">
        <?php
        if ($this->ContentRegionLoader->regionExists('login_join_hub_html')) {
            echo $this->ContentRegionLoader->getRegion('login_join_hub_html');
        } ?>
    </div>
</div>
