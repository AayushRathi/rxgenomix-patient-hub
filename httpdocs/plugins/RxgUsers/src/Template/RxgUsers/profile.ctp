<?php

use App\Util\FormattingHelper;

/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$this->assign('title', 'Profile');
$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Profile');
?>

<div class="user-profile">
    <h3><?= __d('CakeDC/Users', '{0} {1}', $user->first_name, $user->last_name)?></h3>
    <?php //@todo add to config ?>
    <?= $this->Html->link(__d('Users', 'Change Password'), [
            'plugin' => 'RxgUsers',
            'controller' => 'RxgUsers',
            'action' => 'changePassword'
        ],[
            'escape' => false,
            'class' => 'btn btn-condensed btn-primary'
    ]); ?>
    <?= $this->Html->link(__d('Users', 'Edit Profile'), [
        'plugin' => 'RxgUsers',
        'controller' => 'RxgUsers',
        'action' => 'edit'
    ],[
        'escape' => false,
        'class' => 'btn btn-condensed btn-primary'
    ]); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <h4 class="subheader"><?= __d('CakeDC/Users', 'Name') ?></h4>
                <p><?= h($user->first_name) . ' ' . h($user->last_name) ?></p>
                <h4 class="subheader"><?= __d('CakeDC/Users', 'Email') ?></h4>
                <p><?= h($user->email) ?></p>
                <h4 class="subheader"><?= __d('CakeDC/Users', 'License Expiration') ?></h4>
                <p><?php 
                    if (isset($providerEntity->license_expiration_date)) {
                        echo h($providerEntity->license_expiration_date->format('m/d/Y'));
                    } else {?>N/A<?php } ?></p>
            </div>
            <div class="col-md-6">
                <h4 class="subheader">Provider</h4>
<?php 
/* Commenting out per #37363
                <p><?= h($user->provider->facility_name) ?></p>
 */?>
                <p>
                    <?= h($user->provider->address_1) ?><br/>
                    <?= h($user->provider->address_2) ?><br/>
                    <?= h($user->provider->city) ?>, <?= h($user->provider->lib24watch_state_abbr) ?> <?= h($user->provider->postal_code) ?>
                </p>
                <p><?php 
                    if (!is_null($user->provider->phone)) {
                        echo FormattingHelper::formatPhone($user->provider->phone);
                    } else {?>N/A<?php } ?></p>
            </div>
        </div>
    </div>
</div>
