<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
use Cake\Core\Configure;

$this->assign('title', 'Register');

$roles = [
    [ 'value' => 'provider', 'text' => 'I am the provider.'],
    [ 'value' => 'patient', 'text' => 'I am the patient.'],
];

$groupJs = 'var groupId = null;';

if (isset($user->health_care_client_group_id) && $user->health_care_client_group_id != '') {
    $groupJs = 'var groupId = ' . $user->health_care_client_group_id . ';';
}

echo $this->Html->scriptBlock('var healthCareClientGroups = ' . json_encode($healthCareClientGroups) . ';');
echo $this->Html->scriptBlock($groupJs);
echo $this->Html->script('/rxg_theme/js/other/jquery.mask.js');

// date picker
$datePickerFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
    "datePickerFormat",
    "Lib24watch",
    "YYYY-MM-DD"
);
$datePickerPublicFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
    "datePickerPublicFormat",
    "Lib24watch",
    "YYYY-MM-DD"
);

echo $this->Html->scriptBlock("var datePickerFormatFromSetting='" . $datePickerFormatFromSetting . "';");
echo $this->Html->scriptBlock("var datePickerPublicFormatFromSetting='" . $datePickerPublicFormatFromSetting . "';");
?>

<script>
$(function() {
    if ($.fn.mask) {
        $('#phone').mask('(000) 000-0000');
    }

    if ($.fn.autocomplete) {
        $('#health_care_client_search').autocomplete( "option", "autoFocus", true );
    }
});
</script>
<div class="users form">
    <?= $this->Form->create($user, [
        'id' => 'registration-form',
        /*
        'context' => [
            'validator' => [
                'Provider' => 'default'
            ]
        ]*/
    ]); ?>
    <div class="panel">
        <div class="center-container">
            <!--<div class="register-step-1">
                <?=$this->Form->radio('role',$roles , [ 'class' => 'applicant_or_rep', 'value' => $role, 'required' => true, 'value' => 'applicant'])?>
                <a href="javascript: moveNext();" class="btn primary pull-right">Next</a>
            </div>-->
            <div class="register-step-2">
                Already have an account? <?= $this->Html->link(
                                'Click HERE to log in.',
                                [
                                    'plugin' => 'RxgUsers',
                                    'controller' => 'RxgUsers',
                                    'action' => 'login',
                                    'prefix' => false
                                ],
                                [
                                    'title' => 'Click HERE to log in.'
                                ]
                            );
                ?>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <h2>Provider Information</h2>
                        <p>
                            <?php
                            if ($this->ContentRegionLoader->regionExists('register_required_fields_html')) {
                                echo $this->ContentRegionLoader->getRegion('register_required_fields_html');
                            } ?>
                        </p>
                        <?php
                        echo $this->Form->autocomplete(
                            'health_care_client_search',
                            [
                                'label' => 'Health Care Client Name',
                                'id' => 'health_care_client_search',
                                'required' => true,
                                'autocompleteVars' => [
                                    'sourceUrl' => $this->Url->build(
                                        [
                                            'plugin' => 'HealthCareClients',
                                            'controller' => 'HealthCareClients',
                                            'action' => 'autocompleteHealthCareClients',
                                            'prefix' => false,
                                            '?' => [
                                                // example of passing parameters
                                                'active' => 1
                                            ],
                                            '_method' => 'GET'
                                        ],
                                        [ 'escape' => false ]
                                    ),
                                    // example of callback field, storing id value
                                    'callbackField' => $this->Form->control(
                                        'health_care_client_id', 
                                        [
                                            'type' => 'hidden'
                                        ]
                                    ),
                                    'method' => 'GET'
                                ],
                                'placeholder' => 'Enter all or part of company name for suggestions...',
                                'templateVars' => [
                                    'help' => 'This is your primary company name, e.g. John’s Pharmacy or American Healthcare Co.'
                                ]
                            ]
                        );

                        echo $this->Form->control(
                            'health_care_client_group_id',
                            [
                                'label' => 'Group Name',
                                'type' => 'select',
                                'empty' => true,
                                'options' => [],
                                'templateVars' => [
                                    'help' => 'Add any unique identifier you use for your clients/client groups.'
                                ]
                            ]
                        );
                        echo $this->Form->control(
                            'lab_order_initiator_type',
                            [
                                'type' => 'hidden',
                                'value' => 2 // TODO: For MVP, all accounts are for Providers. This will change for direct consumer.
                            ]
                        );
                        echo $this->Form->control(
                            'first_name',
                            [
                                'label' => __d('CakeDC/Users', 'First name'),
                                'required' => true
                            ]
                        );
                        echo $this->Form->control(
                            'last_name',
                            [
                                'label' => __d('CakeDC/Users', 'Last name'),
                                'required' => true
                            ]
                        );
                        echo $this->Form->control(
                            'provider_type_id',
                            [
                                'label' => 'Provider Type',
                                'required' => true,
                                'type' => 'select',
                                'empty' => true,
                                'options' => $providerTypes
                            ]
                        );
                        echo $this->Form->control(
                            'npi_number',
                            [
                                'label' => 'Provider NPI #',
                                'required' => true,
                                'type' => 'text'
                            ]
                        );
                        echo $this->Form->control(
                            'provider_license_id',
                            [
                                'label' => 'Provider License ID',
                                'required' => true,
                                'type' => 'text'
                            ]
                        );

                        $expirationRequired = false;

                        // if this is a pharmacist, require an expiration
                        if (isset($user->provider_type_id) && $user->provider_type_id == 1) {
                            $expirationRequired = true;
                        }

                        echo $this->Form->control(
                            'license_expiration_date',
                            [
                                'type' => 'datetimepicker',
                                'label' => 'License Expiration Date',
                                'required' => true, // just set it to true for now
                                'placeholder' => 'MM-DD-YYYY',
                            ]
                        );
                        echo $this->Form->control(
                            'provider_lib24watch_state_abbr',
                            [
                                'type' => 'select',
                                'label' => 'State of License ID',
                                'empty' => true,
                                'options' => $lib24watchStateOptions,
                                'required' => true
                            ]
                        );
                        /* Commenting out for now per #37363
                        echo $this->Form->control(
                            'facility_name',
                            [
                                'label' => 'Facility',
                                'required' => false,
                                'type' => 'text'
                            ]
                        );
                         */
                        echo $this->Form->control(
                            'nabpep_id',
                            [
                                'label' => 'NABPe-P ID',
                                'required' => false,
                                'type' => 'text'
                            ]
                        );
                        ?>

                        <?php
                        // TODO Training docs upload
                        echo $this->Form->control(
                            'email',
                            [
                                'label' => 'Email Address',
                                'type' => 'text'
                            ]
                        );
                        echo $this->Form->control(
                            'phone',
                            [
                                'label' => 'Phone Number',
                                'required' => true,
                                'type' => 'text'
                            ]
                        );
                        ?>

                        <?php
                        echo $this->Form->control(
                            'address_1',
                            [
                                'label' => 'Street Address',
                                'required' => true,
                                'type' => 'text'
                            ]
                        );
                        echo $this->Form->control(
                            'address_2',
                            [
                                'label' => false,
                                'required' => false,
                                'type' => 'text'
                            ]
                        );
                        echo $this->Form->control(
                            'city',
                            [
                                'label' => 'City',
                                'required' => true,
                                'type' => 'text'
                            ]
                        );
                        echo $this->Form->control(
                            'lib24watch_state_abbr',
                            [
                                'type' => 'select',
                                'label' => 'State',
                                'empty' => true,
                                'options' => $lib24watchStateOptions,
                                'required' => true
                            ]
                        );
                        echo $this->Form->control(
                            'postal_code',
                            [
                                'type' => 'text',
                                'label' => 'Zip',
                                'required' => true
                            ]
                        );

                        // The acknowledgement below is required by USPS Corporate Branding guidelines.
                        // https://www.usps.com/business/web-tools-apis/general-api-developer-guide.htm#_Toc423593904
                        //<div>Information provided by $this->Html->link('www.usps.com', 'http://www.usps.com', [ 'target' => '_blank'])</div>
                        ?>

                        <br>
                        <h2>Account Credentials</h2>
                        <p>You will use the following password to log in to the RxGenomix Portal.</p>
                        <?php

                        echo $this->Form->control(
                            'password',
                            [
                                'type' => 'password',
                                'label' => 'Password',
                                'required' => true,
                                'templateVars' => [
                                    'help' => 'Password needs to contain at least one upper, one lower, one number, one special character and 8 character minimum'
                                ]
                            ]
                        );
                        echo $this->Form->control(
                            'password_confirm',
                            [
                                'type' => 'password',
                                'label' => 'Confirm Password',
                                'required' => true
                            ]
                        );
                        ?>
                        <a href="javascript:;" data-toggle="modal" data-target="#tosModal" style="text-decoration: underline;">
                            View Terms of Service
                        </a>
                        <?php

                        if (Configure::read('Users.Tos.required')) {
                            echo $this->Form->control('tos', ['type' => 'checkbox', 'label' => __d('CakeDC/Users', 'Accept TOS conditions?'), 'required' => true]);
                        }
                        if (Configure::read('Users.reCaptcha.registration')) {
                            echo $this->User->addReCaptcha();
                        }
                        ?>
                    </div>
                </div>
                <div class="pull-right">
                    <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), [ 'class' => 'btn primary']) ?>
                </div>
            </div>
        </div>

    </div>

    <?= $this->Form->end() ?>

    <div class="modal" role="dialog" id="tosModal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">RxGenomix, LLC Terms of Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body tos-body">
                    <?php
                    if ($this->ContentRegionLoader->regionExists('tos')) {
                        echo $this->ContentRegionLoader->getRegion('tos');
                    } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <?php
        $this->Html->scriptStart(['block' => 'extra-scripts']);
        echo 'var registerStep = ' . ($role ? 2 : 1) . ';';
        $this->Html->scriptEnd();
    ?>


    <?= $this->Html->script('/Lib24watch/js/datepicker', ['block' => 'extra-scripts']); ?>
    <?php echo $this->Html->script('/rxg_users/js/register', ['once' => true, 'block' => 'extra-scripts']); ?>

</div>
