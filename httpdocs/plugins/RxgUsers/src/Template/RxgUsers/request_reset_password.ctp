<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$this->assign('title', 'Reset Password');
?>

<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create('User') ?>
    <div class="center-container">
        <h2><?= __d('CakeDC/Users', 'Please enter your email to reset your password') ?></h2>
        <?= $this->Form->control('reference', ['label' => 'An email will be sent with further instructions to recover your account']) ?>
        <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), [ 'class' => 'btn primary btn-large', 'style' => 'width: 100%;']); ?>
    </div>
    <?= $this->Form->end() ?>
</div>
