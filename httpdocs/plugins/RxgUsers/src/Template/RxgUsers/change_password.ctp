<?php
$this->assign('title', 'Change Password');
?>
<div class="users form">
    <?= $this->Flash->render('auth') ?>
    <?= $this->Form->create($user) ?>
    <div class="panel">
        <h2><?= __d('CakeDC/Users', 'Please enter the new password') ?></h2>
        <?php
if (isset($expired) && $expired) {
?>
    <div class="alert alert-danger"><p>Your password has expired.</p></div>
<?php } ?>
        <?php if ($validatePassword) : ?>
            <?= $this->Form->control('current_password', [
                'type' => 'password',
                'required' => true,
                'label' => __d('CakeDC/Users', 'Current password')]);
            ?>
        <?php endif; ?>
        <?= $this->Form->control('password', [
            'type' => 'password',
            'required' => true,
            'templateVars' => [
                'help' => 'Password needs to contain at least one upper, one lower, one number, one special character and 8 character minimum'
            ],
            'label' => __d('CakeDC/Users', 'New password')]);
        ?>
        <?= $this->Form->control('password_confirm', [
            'type' => 'password',
            'required' => true,
            'label' => __d('CakeDC/Users', 'Confirm password')]);
        ?>
        <?= $this->Form->button(__d('CakeDC/Users', 'Submit'), [
            'class' => 'btn btn-condensed btn-primary'
        ]); ?>
    </div>
    <?= $this->Form->end() ?>
</div>
