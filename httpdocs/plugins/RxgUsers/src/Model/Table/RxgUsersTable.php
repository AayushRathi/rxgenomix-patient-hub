<?php
namespace RxgUsers\Model\Table;

use App\Util\ValidationHelper;
use ArrayObject;
use Cake\Event\Event;
use CakeDC\Users\Model\Table\UsersTable as BaseUsersTable;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

class RxgUsersTable extends BaseUsersTable {

    /**
     * @param array $config
     *
     */
    public function initialize($config){
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('email');
        $this->hasOne(
            'Provider',
            [
                'className' => 'Providers.Providers',
                'foreignKey' => 'rxg_user_id',
                'bindingKey' => 'id'
            ]
        );

        $this->belongsToMany('LabOrderInitiatorTypes', [
            'through' => 'LabOrderInitiatorTypesUsers',
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'lab_order_initiator_type_id',
        ]);
    }

    /**
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // With the new phone mask on /register, the phone mask would be (XXX) XXX-XXXX
        // But still need to support existing phone numbers in the system (within profile editing) so can't force it
        if (
            isset($data['phone']) &&
            preg_match(ValidationHelper::PARENTHESIS_PHONE_REGEX, $data['phone'])
        ) {
            $data['phone'] = preg_replace('#\(|\)#', '', $data['phone']);
            $data['phone'] = preg_replace('#-#', '', $data['phone']);
            $data['phone'] = preg_replace('#\s+#', '', $data['phone']);
        }
    }

    /**
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->allowEmpty('token');

        $validator
            ->add('token_expires', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('token_expires');

        $validator
            ->allowEmpty('api_token');

        $validator
            ->add('activation_date', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('activation_date');

        $validator
            ->add('tos_date', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('tos_date');

        $validator
            ->add('phone', 'custom', [
                'rule' => [$this, 'phoneNumber'],
                'message' => 'Please provide a valid US phone number'
            ]);

        $validator
            ->add('health_care_client_search', 'custom', [
                'rule' => function ($value, $context) {
                    $healthCareClientEntity = null;
                    if (
                        isset($context['data']['health_care_client_id']) &&
                        $context['data']['health_care_client_id'] > 0
                    ) {
                        $healthCareClientEntity = \Cake\ORM\TableRegistry::getTableLocator()
                            ->get('HealthCareClients.HealthCareClients')
                            ->find()
                            ->where([
                                'id' => (int)$context['data']['health_care_client_id']
                            ])
                            ->first();
                    }

                    if (is_null($healthCareClientEntity)) {
                        return false;
                    }

                    return true;
                },
                'message' => 'Valid Health Care Client required'
            ]);

        $validator
            ->requirePresence('health_care_client_group_id', [$this, 'validateGroups'])
            ->notEmpty('health_care_client_group_id', 'Group is required', [$this, 'validateGroups']);

        $validator
            ->requirePresence('email', true, 'Please provide a valid email')
            ->maxLength('email', 255, 'Email must not exceed 255 characters')
            ->notEmpty('email', 'Email is required')
            ->email('email', false, 'Must be a proper email');

        return $validator;
    }

    /**
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationAddress(Validator $validator)
    {
        $validator
            ->add('postal_code', 'custom', [
                'rule' => function($value, $context){
                    if(isset($context['data']['address_verified']) && $context['data']['address_verified'] === false){
                        return $context['data']['address_verification_error'];
                    }
                    return true;
                }
            ]);

        return $validator;
    }

    /**
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationPasswordStrength(Validator $validator)
    {
        $validator
            ->add('password', 'custom', [
                'rule' => [$this, 'passwordStrength'],
                'message' => 'Password need contain at least one upper, one lower, one number, one special character and 8 character minimum'
            ]);

        return $validator;
    }

    /**
     * Wrapper for all validation rules for register
     * @param Validator $validator Cake validator object.
     *
     * @return Validator
     */
    public function validationRegister(Validator $validator)
    {
        $validator = $this->validationDefault($validator);
        $validator = $this->validationPasswordConfirm($validator);
        $validator = $this->validationPasswordStrength($validator);
        $validator = $this->validationAddress($validator);

        return $validator;
    }

    /**
     * @param string $username
     * @param string $email
     *
     * @return \Cake\ORM\Query
     */
    public function findByUsernameOrEmail($username, $email){
        return $this->find()->where([
            'OR' => [
                ['email' => $username],
                ['email' => $email],
            ]
        ]);
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    public function passwordStrength($value)
    {
        return (bool)preg_match('/(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W])/', $value);
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    public function phoneNumber($value)
    {
        return (bool)preg_match(ValidationHelper::NON_PARENTHESIS_PHONE_REGEX, $value);
    }

    /**
     * Validate whether a health care client group id is required based on if there are groups for a health care client
     *
     * @param array $context
     *
     * @return bool
     */
    public function validateGroups($context) {
        if (
            isset($context['data']['health_care_client_id']) &&
            (int)$context['data']['health_care_client_id'] > 0
        ) {
            $groupCount = TableRegistry::getTableLocator()->get('HealthCareClients.HealthCareClientGroups')
                ->find()
                ->where([
                    'HealthCareClientGroups.health_care_client_id' => $context['data']['health_care_client_id']
                ])
                ->count();

            if ($groupCount > 0) {
                return true;
            }
        }

        return false;
    }
}
