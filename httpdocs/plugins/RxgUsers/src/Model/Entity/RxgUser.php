<?php
namespace RxgUsers\Model\Entity;

use CakeDC\Users\Model\Entity\User;

class RxgUser extends User {
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'is_superuser' => false,
        'role' => true,
    ];

    /**
     * Obtain full name of user
     *
     * @return string|null
     */
    protected function _getFullName()
    {
        $fullName = null;

        if (isset($this->_properties['first_name']) && isset($this->_properties['last_name'])) {
            $fullName = trim($this->_properties['first_name']) . ' ' . trim($this->_properties['last_name']);
        }

        return $fullName;
    }

    /**
     * Check if the password has expired
     *
     * @return bool
     */
    protected function _getHasPasswordExpired()
    {
        $today = new \Cake\I18n\FrozenTime();

        if (
            isset($this->password_expires) && 
            !is_null($this->password_expires) &&
            $this->password_expires instanceof \Cake\I18n\FrozenTime &&
            $this->password_expires->format('U') < $today->format('U')
        ) {
            return true;
        }

        return false;
    }
}
