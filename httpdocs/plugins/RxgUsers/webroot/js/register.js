"use strict";

let historyQueue = [1];

if(registerStep){
    historyQueue.push(registerStep);
}

let applicantType = '';
let companyRecordExist = null;
let validator = null;

const moveNext = function(){
    let currentStep = historyQueue[historyQueue.length - 1];
    let nextStep = currentStep + 1;
    let currentStepPanel = $('.register-step-' + currentStep);

    switch (currentStep) {
        case 1:
            nextStep = 2;
            break;
        case 2:
            let valid = true;
            let firstInvalidField = null;

            currentStepPanel.find('input, select, radio, checkbox').each(function(i, item){
                valid = validator.element(item) && valid;
                if(!valid && !firstInvalidField){ firstInvalidField = item; }
            });

            if(!valid) {
                firstInvalidField.focus();
                return;
            }

            nextStep = 3;
            break;
        case 3:
            companyRecordExist = $('.has_company_record:checked').val();
            applicantType = $('.applicant_or_rep:checked').val();

            if(applicantType === 'applicant') {
                if (companyRecordExist === 'no_has') {
                    $('.company-fields').show();
                    nextStep = 6;
                } else {
                    nextStep = 4;
                }
            } else {
                nextStep = (companyRecordExist === 'no_has') ? 5 : 7;
            }

            break;
        case 4:
        case 7:
            //validate company id
            nextStep = 6;
            break;
        default:
            break;
    }

    currentStepPanel.fadeOut(200, function(){
        $('.register-step-' + nextStep).fadeIn(200);
    });

    historyQueue.push(nextStep);
};

const movePrevious = function(){
    if(historyQueue.length > 1) {
        $('.register-step-' + historyQueue.pop()).fadeOut(200, function(){
            $('.register-step-' + historyQueue[historyQueue.length - 1]).fadeIn(200);
        });
    }
};

$(document).ready(function(){
    //validator = $('#registration-form').validate(); // FIXME
    $('.register-step-' + historyQueue[historyQueue.length - 1]).fadeIn(200);

    var typeField = $('#health-care-client-id');
    var searchField = $('#health_care_client_search');
    var subTypeField = $('#health-care-client-group-id');
    var subTypeWrapper = subTypeField.closest('div.user-form-container');
    subTypeWrapper.hide();

    let showHideGroup = function () {
        var typeId = typeField.val();
        if (healthCareClientGroups[typeId]) {
            subTypeField.find('option').remove();
            subTypeField.append($('<option>').text('').attr('value', ''));
            $.each(healthCareClientGroups[typeId], function (i, value) {
                let option = $('<option>').text(value).attr('value', i);
                if (groupId && groupId == i) {
                    option.prop('selected', true);
                }
                subTypeField.append(option);
            });
            subTypeField.attr('required', '');
            subTypeField.addClass('required').closest('.user-form-container').addClass('required');
            subTypeWrapper.show();
        } else {
            subTypeField.find('option').remove();
            subTypeField.removeAttr('required');
            subTypeField.removeClass('required').closest('.user-form-container').removeClass('required');
            subTypeWrapper.hide();
        }
    };

    searchField.on('autocompleteclose', function () {
        showHideGroup();
    });

    searchField.on('blur', function () {
        if (searchField.val().length === 0) {
            subTypeField.find('option').remove();
            subTypeField.removeAttr('required');
            subTypeField.removeClass('required').closest('.user-form-container').removeClass('required');
            subTypeWrapper.hide();
        }
    });

    // on load for errors
    showHideGroup();

    $('#provider-type-id').on('change', function() {
        let nabpeId = $('#nabpep-id');
        if ($(this).val() == 1) {
            nabpeId.attr('required', true)
                .closest('.user-form-container')
                .addClass('required');
        } else {
            nabpeId.attr('required', false)
                .closest('.user-form-container')
                .removeClass('required');
        }
    });

});
