<?php
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin('RxgUsers', function ($routes) {
    $routes->fallbacks(DashedRoute::class);
});