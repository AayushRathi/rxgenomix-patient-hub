<?php
use Migrations\AbstractMigration;

class AddHealthCareClientIdToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('health_care_client_id', 'integer', [ 'limit' => 10 ]);
        $table->update();
    }
}
