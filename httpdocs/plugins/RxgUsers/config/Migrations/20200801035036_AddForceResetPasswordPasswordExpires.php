<?php
use Migrations\AbstractMigration;

class AddForceResetPasswordPasswordExpires extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('users')
            ->addColumn('force_password_reset', 'boolean', [
                'default' => false
            ])
            ->addColumn('password_expires', 'datetime', [
                'null' => true,
                'default' => null
            ])
            ->update();
    }
}
