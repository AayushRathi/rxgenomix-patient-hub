<?php
use Migrations\AbstractMigration;

class AddLabOrderInitiatorTypeToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('lab_order_initiator_type', 'integer', [ 'limit' => 10 ]);
        $table->update();
    }
}
