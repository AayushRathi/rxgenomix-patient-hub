<?php
use Migrations\AbstractMigration;

class RemoveLabOrderIdFromInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoices');
        $table->removeColumn('lab_order_id');
        $table->update();
    }
}
