<?php
use Migrations\AbstractMigration;

class DropInvoiceLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('invoice_lab_orders');
        $table->drop()->save();
    }
}
