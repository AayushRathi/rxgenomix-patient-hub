<?php
use Cake\Core\Plugin;
use Cake\Core\Configure;

Plugin::load('CakePdf', ['bootstrap' => true]);
Configure::write('CakePdf', [
    'engine' => 'CakePdf.DomPdf',
    'margin' => [
        'bottom' => 15,
        'left' => 50,
        'right' => 30,
        'top' => 45
    ],
    'orientation' => 'landscape',
    'download' => true
]);
