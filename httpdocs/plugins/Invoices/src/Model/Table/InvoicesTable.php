<?php
namespace Invoices\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class InvoicesTable
 * @package Invoices\Model\Table
 */
class InvoicesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Status',
            [
                'className' => 'Invoices.InvoiceStatuses',
                'foreignKey' => 'invoice_status_id'
            ]
        );

        $this->belongsTo(
            'HealthCareClient',
            [
                'className' => 'HealthCareClients.HealthCareClients',
                'foreignKey' => 'health_care_client_id',
            ]
        );

        $this->hasMany(
            'LabOrders',
            [
                'className' => 'LabOrders.LabOrders',
                'foreignKey' => 'invoice_id',
                'sort' => 'LabOrders.created'
            ]
        );

    }
}
