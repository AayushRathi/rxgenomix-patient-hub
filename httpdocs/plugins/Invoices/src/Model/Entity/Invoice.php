<?php

namespace Invoices\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class Invoice extends Entity
{
    /**
     * @var Array The lab order line items grouped by health care client group for outputting the invoice.
     */
    protected $_lineGroups;

    /**
     * @return Array|array
     */
    public function getLineGroups()
    {
        if (is_null($this->_lineGroups)) {
            $this->_lineGroups = $groups = [];
            foreach ($this->lab_orders as $labOrder) {
                $groups[] = $labOrder->provider->health_care_client_group_id;
            }
            $groups = array_unique($groups);

            $groupsTable = TableRegistry::getTableLocator()->get('HealthCareClients.HealthCareClientGroups');
            $this->_lineGroups = $groupsTable->find()->whereInList('id',
                $groups)->order('display_order, title')->indexBy('id')->toArray();

            foreach ($this->lab_orders as $labOrder) {
                $groupId = $labOrder->provider->health_care_client_group_id;
                if (!isset($this->_lineGroups[$groupId]->lab_orders)) {
                    $this->_lineGroups[$groupId]->lab_orders = [];
                }

                $this->_lineGroups[$groupId]->lab_orders[] = $labOrder;
            }
        }

        return $this->_lineGroups;
    }
}