<html>
<style type="text/css">
    @page {
        margin: 0cm 0cm;
    }

    header {
        position: fixed;
        top: 0cm;
        left: 0cm;
        right: 0cm;
        height: 5cm;
    }

    header img {
        margin: 2cm 1cm 0 2cm;
        height: 1.5cm;
    }

    header h1 {
        color: #1b6d85;
        text-align: center;
        margin-top: -1cm;
    }

    header div {
        margin: 0 2cm 0 2cm;
    }

    footer {
        color: #222;
        font-size: 0.25cm;
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 2cm;
        padding: 0 2cm 0 2cm;
        text-align: right;
    }

    footer .pagenum:before {
        content: counter(page);
    }

    body {
        font-family: 'Open Sans', sans-serif;
        margin-top: 5.15cm;
        margin-left: 2cm;
        margin-right: 2cm;
        margin-bottom: 2cm;
    }

    tr.even { background-color: #ddd; }

    table.line-items { border: 1px solid #1b6d85; display: table; width: 100%; margin: 0; padding: 0; border-collapse: collapse; }
    table.line-items thead { background-color: #1b6d85; color: white; font-weight: bold;}
    table.line-items tr { margin: 0; padding: 0;}
    table.line-items td { margin: 0; border-left: 1px solid #1b6d85; }
    table.line-items td:first {border: 0px;}

    .pagebreak { page-break-after: always; }
</style>
<body>
<header>
    <img src="<?= \Cake\Core\Plugin::path('RxgTheme') . 'webroot' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'rxg-logo.png'; ?>" />
    <h1>INVOICE</h1>
    <div>Invoice No: <?= $invoice->id ?></div>
    <div>Invoice Date: <?= $invoice->created->format('m/d/Y') ?></div>
</header>
<footer>
    Page <span class="pagenum"></span>
</footer>
<main>
    <?= $this->fetch('content') ?>
</main>
</body>
</html>