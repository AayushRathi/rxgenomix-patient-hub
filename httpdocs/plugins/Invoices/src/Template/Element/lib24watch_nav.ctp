<a href="javascript:void(0)"><span class="fa fa-money"></span><span class="xn-text"><?php echo h($lib24watchModules['Invoices']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('invoices_view', 'Invoices')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Invoices",
                [
                    'plugin' => 'Invoices',
                    'controller' => 'Invoices',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Invoices'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>