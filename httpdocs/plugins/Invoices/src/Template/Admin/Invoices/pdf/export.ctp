<?php foreach ($invoice->getLineGroups() as $group) : ?>
    <h2><?= $invoice->health_care_client->title ?>: <?= $group->title ?></h2>
    <table class="line-items pagebreak">
        <thead>
        <tr>
            <th>Lab Order #</th>
            <th>Created</th>
            <th>Modified</th>
            <th>Collection Date</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($group->lab_orders as $i => $labOrder) :?>
            <tr class="<?= $i % 2 ? 'even' : 'odd' ?>">
                <td><?= $labOrder->id ?></td>
                <td><?= $labOrder->created->format('m/d/Y') ?></td>
                <td><?= $labOrder->modified ? $labOrder->modified->format('m/d/Y') : '' ?></td>
                <td><?= $labOrder->collection_date ? $labOrder->collection_date->format('m/d/Y') : '' ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php endforeach ?>

Total: $<?= number_format($invoice->original_amount_due, 2, '.', ',') ?>
