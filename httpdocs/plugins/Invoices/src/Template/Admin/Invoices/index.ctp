<?php
$pluginTitle = $lib24watchModules['Invoices']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Invoices';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Invoices',
                'items' => [
                    [
                        'name' => 'patient_name',
                        'type' => 'text',
                        'label' => 'Patient Name',
                        'function' => function($queryObj, $value) {
                            return $queryObj->matching('LabOrders.Patient', function ($q) use ($value) {
                                return $q->where(['OR' => [
                                        ['first_name LIKE' => '%' . $value . '%'],
                                        ['last_name LIKE' => '%' . $value . '%'],
                                        ['concat(first_name, " ", last_name) LIKE' => '%' . $value . '%']
                                    ]
                                ]);
                            });
                        }
                    ],
                    [
                        'name' => 'lab_order',
                        'type' => 'text',
                        'label' => 'Lab Order #',
                        'function' => function ($queryObj, $value) {
                            return $queryObj->matching('LabOrders', function ($q) use ($value) {
                                return $q->where(['LabOrders.id' => $value]);
                            });
                        }
                    ],
                    [
                        'name' => 'sample_id',
                        'type' => 'text',
                        'label' => 'Sample ID',
                        'function' => function ($queryObj, $value) {
                            return $queryObj->matching('LabOrders', function ($q) use ($value) {
                                return $q->where(['LabOrders.sample_id LIKE' => '%' . $value . '%']);
                            });
                        }
                    ],
                    [
                        'name' => 'invoice_status_id',
                        'type' => 'select',
                        'label' => 'Invoice Status',
                        'options' => $statuses,
                        'empty' => true,
                        'default' => '',
                    ],
                    [
                        'name' => 'created',
                        'type' => 'datetimepicker',
                        'label' => 'Date Labs Ordered',
                        'function' => function ($queryObj, $value) {
                            return $queryObj->matching('LabOrders', function ($q) use ($value) {
                                return $q->where(['DATE(LabOrders.created)' => $value]);
                            });
                        }
                    ],
                    [
                        'name' => 'id',
                        'alias' => 'Invoices.',
                        'type' => 'text',
                        'label' => 'Batch Number',
                    ]
                ],
                $query,
                $this
            ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false /*$this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'Invoices',
                            'controller' => 'Labs',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Lab'
                        ]
                    )*/
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'label' => 'Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'date'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->Html->link(
                                                'Export as PDF',
                                                [
                                                    'plugin' => 'Invoices',
                                                    'controller' => 'Invoices',
                                                    'action' => 'export',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary'
                                                ]
                                            ) . $this->Html->link(
                                                'View',
                                                [
                                                    'plugin' => 'Invoices',
                                                    'controller' => 'Invoices',
                                                    'action' => 'view',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary'
                                                ]
                                            );
                                        }
                                    ]
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
