<?php
$pluginTitle = $lib24watchModules['Invoices']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Invoice"),
    [
        'plugin' => 'Invoices',
        'controller' => 'Invoices',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = "View Invoice";
$this->Breadcrumbs->add($pageHeading);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-2 text-right">
                                Invoice #
                            </div>
                            <div class="col-md-10">
                                <?= $invoice->id ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-right">
                                Original Amount Due
                            </div>
                            <div class="col-md-10">
                                $<?= number_format($invoice->original_amount_due, 2, '.', ',') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-right">
                                Final Amount Due
                            </div>
                            <div class="col-md-10">
                                $<?= number_format($invoice->original_amount_due, 2, '.', ',') ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-right">
                                Status
                            </div>
                            <div class="col-md-10">
                                <?php
                                if ($invoice->status) {
                                    echo $invoice->status->title;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <?=
                        $this->HTML->link('Export as PDF', [
                            'plugin' => 'Invoices',
                            'controller' => 'Invoices',
                            'action' => 'export',
                            'prefix' => 'admin',
                            $invoice->id
                        ], [
                            'escape' => false,
                            'class' => 'btn btn-md btn-primary'
                        ]) ?>
                    </div>
                </div>
                <br />
                <?php foreach ($invoice->getLineGroups() as $group) :?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?= $invoice->health_care_client->title ?>: <?= $group->title ?>
                        </div>
                        <div class="panel-body">
                            <?= $this->cell('Lib24watch.Table::display', [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'label' => 'Lab Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Created',
                                        'type' => 'datetime'
                                    ],
                                    [
                                        'name' => 'modified',
                                        'label' => 'Modified',
                                        'type' => 'datetime'
                                    ],
                                    [
                                        'name' => 'collection_date',
                                        'label' => 'Collection Date',
                                        'type' => 'datetime'
                                    ]
                                ],
                                $group->lab_orders
                            ]) ?>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
