<?php

namespace Invoices\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use CakePdf\Pdf\CakePdf;

/**
 * Class InvoiceJobCommand
 * @package Invoices\Command
 */
class InvoiceJobCommand extends Command
{
    use MailerAwareTrait;

    /**
     * Send invoices if HCC gets them on this date
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $healthCareClientsTable = TableRegistry::getTableLocator()->get('HealthCareClients.HealthCareClients');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $invoicesTable = TableRegistry::getTableLocator()->get('Invoices.Invoices');

        $healthCareClients = $healthCareClientsTable->find()->where([
            $healthCareClientsTable->getAlias() . '.has_contract' => 1,
            $healthCareClientsTable->getAlias() . '.is_active' => 1,
            'invoice_day_of_month' => date('j', time()),
            'invoice_email !=' => ''
        ]);
        $io->out($healthCareClients->count() . ' clients are invoiced on this date.');

        $mailer = $this->getMailer('Invoices.Invoices');

        foreach ($healthCareClients as $key => $healthCareClient) {
            $labOrders = $labOrdersTable->find()->where([
                'invoice_id IS' => null,
                'is_deleted' => 0,
                'Provider.health_care_client_id' => $healthCareClient->id,
                'lab_order_status_id' => 7
            ])->contain([
                'Provider.HealthCareClientGroup'
            ])->order(['HealthCareClientGroup.id' => 'ASC']);

            $io->out($healthCareClient->title . ' has ' . $labOrders->count() . ' lab orders to invoice.');

            if ($labOrders->count() > 0) {
                $total = $healthCareClient->fee_per_lab_order * $labOrders->count();

                $invoiceEntity = $invoicesTable->newEntity([
                    'health_care_client_id' => $healthCareClient->id,
                    'original_amount_due' => round($total, 2),
                    'invoice_status_id' => 1
                ], [
                    'contain' => ['LabOrders', 'LabOrders.provider', 'HealthCareClient'],
                ]);

                $invoiceEntity->lab_orders = [];
                foreach ($labOrders as $labOrder) {
                    $invoiceEntity->lab_orders[] = $labOrder;
                }
                $invoiceEntity->health_care_client = $healthCareClient;

                $invoicesTable->save($invoiceEntity);

                $io->out('Generating invoice PDF');

                $CakePdf = new CakePdf;
                $CakePdf->templatePath('Admin' . DS . 'Invoices' . DS . 'pdf');
                $CakePdf->template('Invoices.export', 'Invoices.invoice');
                $CakePdf->viewVars(['invoice' => $invoiceEntity]);

                $io->out('Sending invoice #' . $invoiceEntity->id . ' email to ' . $healthCareClient->title . ' (' . $healthCareClient->invoice_email . ')...');
                $mailer->send(
                    'invoiceCreated',
                    [
                        $invoiceEntity,
                        $CakePdf->output(),
                    ]
                );
            }
        }
    }
}
