<?php

namespace Invoices\Mailer;

use Cake\Datasource\EntityInterface;
use Cake\Mailer\Mailer;

class InvoicesMailer extends Mailer
{

    /**
     * @param EntityInterface $invoice
     * @param string $attachment
     * @throws \Exception
     */
    protected function invoiceCreated(EntityInterface $invoice, string $attachment)
    {
        $this
            ->to($invoice->health_care_client->invoice_email)
            ->setSubject("New Invoice")
            ->setAttachments([
                'Invoice_' . $invoice->id . '.pdf' => [
                    'data' => $attachment,
                ]
            ])
            ->setViewVars(['invoice' => $invoice])
            ->setTemplate('Invoices.invoiceCreated')
            ->setEmailFormat('html')
            ->setLayout('RxgTheme.email');
    }
}
