<?php
namespace Invoices\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'Invoices', 'Invoices');
    }

    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'invoices_view' => 'View Invoices',
            'invoices_edit' => 'Edit Invoices',
        ];
    }
}
