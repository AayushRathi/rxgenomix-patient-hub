<?php

namespace Invoices\Controller\Admin;

use Invoices\Controller\AppController;

/**
 * Class InvoicesController
 * @package Invoices\Controller\Admin
 */
class InvoicesController extends AppController
{
    /**
     * init
     */
    public function initialize()
    {
        $this->loadModel('Invoices.Invoices');
        $this->loadModel('Invoices.InvoiceStatuses');

        $this->set('statuses', $this->InvoiceStatuses->find('dropdown'));

        parent::initialize();
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('invoices_index', 'Invoices',
            'You do not have permission to access this resource.');
        $query = $this->Invoices->find()->contain([
            'HealthCareClient',
            'Status',
        ]);

        $this->set('query', $query);
    }

    public function export(int $invoiceId)
    {
        $this->requirePluginPermission('invoices_export', 'Invoices',
            'You do not have permission to access this resource.');
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->layout = 'Invoices.invoice';

        $invoice = $this->Invoices->get($invoiceId, [
            'contain' => [
                'HealthCareClient',
                'LabOrders' => [
                    'sort' => 'LabOrders.created'
                ],
                'LabOrders.Provider'
            ]
        ]);

        $this->viewBuilder()->setOptions([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'Invoice_' . $invoiceId . '.pdf',
            ]
        ]);

        $this->set('invoice', $invoice);
    }

    /**
     * @param int|null $invoiceId
     */
    public function view(int $invoiceId = null)
    {
        $this->requirePluginPermission('invoices_view', 'Invoices',
            'You do not have permission to access this resource.');

        $invoice = $this->Invoices->get($invoiceId, [
            'contain' => ['Status', 'HealthCareClient', 'LabOrders', 'LabOrders.Provider']
        ]);
        $this->set(compact('invoice'));
    }
}