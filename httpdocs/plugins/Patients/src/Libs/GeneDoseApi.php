<?php
namespace Patients\Libs;

use Cake\Http\Client;
use Cake\Core\Configure;

/**
 * Class GeneDoseApi
 * @package Patients\Libs
 */
class GeneDoseApi
{
    /**
     * @var Client
     */
    protected $http;

    /**
     * @var string
     */
    protected $domain;
    Protected $prodDomain = 'https://api.coriell-services.com/';
    Protected $devDomain = 'https://api-dev.coriell-services.com/';

    /**
     * GeneDoseApi constructor.
     */
    public function __construct()
    {
        if (env('RXG_ENV') == 'production') {
            $this->domain = $this->prodDomain;
        } else {
            $this->domain = $this->devDomain;
        }
        
        $token = $this->authenticate();
        $this->http = new Client([
            'headers' => ['Authorization' => 'Bearer ' . $token]
        ]);
    }

    /**
     * @return mixed
     */
    protected function authenticate()
    {
        $geneDose = Configure::read('GeneDose.api');
        
        $http = new Client();
        $response = $http->post($this->domain . 'oauth2/token/wrap', [
            'grant_type' => $geneDose['grant_type'],
            'username' => $geneDose['username'],
            'password' => $geneDose['password'],
            'client_id' => $geneDose['client_id']
        ]);
        $response = json_decode($response->getJson());
        return $response->access_token;
    }
}
