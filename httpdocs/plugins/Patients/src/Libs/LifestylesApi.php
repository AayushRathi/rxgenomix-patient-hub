<?php
namespace Patients\Libs;

/**
 * Class LifestylesApi
 * @package Patients\Libs
 */
class LifestylesApi extends GeneDoseApi
{
    /**
     * @return bool|mixed
     */
    public function getList()
    {

        $response = $this->http->get($this->domain . 'lifestyle');
        if ($response->isOk()) {
            return json_decode($response->getJson());
        } else {
            return false;
        }
    }
}
