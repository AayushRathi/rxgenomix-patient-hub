<?php
namespace Patients\Libs;

/**
 * Class DrugApi
 * @package Patients\Libs
 */
class DrugApi extends GeneDoseApi
{

    /**
     * @param string|null $drugName
     * @return bool|mixed
     */
    public function getDrugsByName(string $drugName = null)
    {
        $response = $this->http->get($this->domain . 'drug/search?q=' . $drugName);
        if ($response->isOk()) {
            return json_decode($response->getJson());
        } else {
            return false;
        }
    }

    /**
     * @param int|null $drugId
     * @return bool|mixed
     */
    public function getDrugById(int $drugId = null)
    {
        $response = $this->http->get($this->domain . 'drug/' . $drugId);
        if ($response->isOk()) {
            return json_decode($response->getJson());
        } else {
            return false;
        }
    }
}
