<?php
namespace Patients\Controller\Admin;

use Patients\Controller\AppController;

/**
 * Class PatientsController
 * @package Patients\Controller\Admin
 */
class PatientsController extends AppController
{

    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'Patients' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Patients.Patients');
    }

    /**
     *
     */
    public function index()
    {
        $this->requirePluginPermission('patients_view', 'Patients',
            'You do not have permission to access this resource.');
        $query = $this->Patients->find();
        $this->paginate($query);
        $this->set('query', $query);
    }

    /**
     * @param int|null $patientId
     * @throws \Exception
     */
    public function edit(int $patientId = null)
    {
        $this->requirePluginPermission('patients_edit', 'Patients',
            'You do not have permission to access this resource.');

        if ($patientId) {
            $entity = $this->Patients->get($patientId);
        } else {
            $entity = $this->Patients->newEntity();
        }

        $this->set('patientId', $patientId);
        $this->set('patientEntity', $entity);

        if($this->getRequest()->getData()) {
            $entity = $this->Patients->patchEntity($entity, $this->getRequest()->getData());
            $errors = $entity->getErrors();

            if (empty($errors)) {

                if (!$this->Patients->save($entity)) {
                    throw new \Exception('Could not save Patient');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'Patients',
                            'controller' => 'Patients',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Patient ' . ($patientId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());

        $this->loadModel('SystemManagement.Sexes');
        $this->set('sexes', $this->Sexes->find('dropdown'));

        $this->loadModel('SystemManagement.Ethnicities');
        $this->set('ethnicities', $this->Ethnicities->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->order([
            'abbr' => 'ASC'
        ])->toArray());
    }

    public function change_status(int $patientId = null)
    {
        $this->requirePluginPermission('patients_edit', 'Patients',
            'You do not have permission to edit patients');

        $patientEntity = $this->Patients->get($patientId);


        if ($patientEntity->is_active) {
            $patientEntity->is_active = 0;
            $text = 'active';
        } else {
            $patientEntity->is_active = 1;
            $text = 'inactive';
        }

        if (!$this->Patients->save($patientEntity)) {
            throw new \Exception("Couldn't update active status for Patient with ID ({$patientId})");
        }

        $this->Lib24watchLogger->write("Changed status of Patient '" . $patientEntity->first_name . ' ' . $patientEntity->last_name . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'Patients',
                'controller' => 'Patients',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Patient '" . $patientEntity->first_name . ' ' . $patientEntity->last_name . "' is now " . $text
        );
    }
}
