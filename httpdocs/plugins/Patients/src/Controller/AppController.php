<?php
namespace Patients\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'patients_view' => 'View Patients',
            'patients_edit' => 'Edit Patients',
            'patients_set_health_care_client' => 'Set Health Care Client for Patients',
            'patients_delete' => 'Delete Patients'
        ];
    }
}
