<?php
namespace Patients\Controller;

use Cake\Core\Configure;
/**
 * Class PatientsController
 * @package Patients\Controller
 */
class PatientsController extends AppController
{

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->loadModel('CakeDC/Users.Users');
        $this->loadModel('Patients.Patients');
        if (!isset($this->ContentRegionLoader)) {
            $this->loadComponent('ContentRegions.ContentRegionLoader');
        }

        parent::initialize();
    }

    /**
     * @throws \HttpException
     */
    public function getPatientByDateOfBirth()
    {
        if (!$this->getRequest()->is('ajax')) {
            throw new \HttpException('Only Ajax accepted');
        }

        $user = $this->Auth->user();

        if (!$user) {
            // must be logged to view.
            throw new \Exception('You must be logged in to view this resource');
        }

        $this->loadModel('Providers.Providers');
        $provider = $this->Providers->find()
            ->contain([
                'HealthCareClientGroup.ChildGroups'
            ])
            ->where([
                'rxg_user_id' => $user['id']
        ])->first();
        $providerGroups = [];
        $providerGroups[] = $provider->health_care_client_group->id;
        if (!empty($provider->health_care_client_group->child_groups)) {
            foreach ($provider->health_care_client_group->child_groups as $childGroup) {
                $providerGroups[] = $childGroup->id;
                $providerGroups[] = $childGroup->title;
            }
        }

        $ymdPattern = '/^[\d]{4}[-][\d]{2}[-][\d]{2}$/';
        $mdyPattern = '/^[\d]{2}[-][\d]{2}[-][\d]{4}$/';

        $dob = $this->getRequest()->getData('dob');

        if (!is_null($dob) && (preg_match($ymdPattern, $dob) || preg_match($mdyPattern, $dob))) {
            if (preg_match($mdyPattern, $dob)) {
                $dateTime = \DateTime::createFromFormat("m-d-Y", $dob);
                $dob = $dateTime->format('Y-m-d');
            }

            $params = [
                'date_of_birth' => $dob,
                'health_care_client_id' => $user['health_care_client_id'],
                'provider' => $provider,
                'client_id' => $providerGroups
            ];

            $results = $this->Patients->find('byDateOfBirth', $params)->toArray();
        } else {
            $results = [];
        }
        // disable debugging
        Configure::write('debug', 0);
        \header('Content-type: application/json');
        $this->viewBuilder()->setLayout('ajax');

        echo json_encode($results);
        exit;
    }

}
