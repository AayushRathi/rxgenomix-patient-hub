<?php
namespace Patients\Command;

use App\Application;
use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\CommandRunner;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class ImportBenecardMembersCommand
 * @package Patients\Command
 */
class ImportBenecardMembersCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Starting');
        $patientsTable = TableRegistry::getTableLocator()->get('Patients.Patients');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $labOrderProvidersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderProviders');

        //$csv = ROOT . '/data/benecard-members.csv';
        $csv = ROOT . '/data/benecard-members-second-only.csv';

        $handle = fopen($csv, 'r');

        if (!$handle) {
            $io->out('Could not open file!');
        }

        // Need to skip a number of lines on the private CSV files (and 1 line on the public)
        for ($i=0; $i < 1; $i++) {
            $void = fgetcsv($handle, 0, ',');
        }

        $mapping = [
            'patient' => [
                2 => 'first_name',
                3 => 'last_name',
                5 => 'ethnicity_id',
                6 => 'sex_id',
                7 => 'lib24watch_state_abbr',
                8 => 'client_id',
                9 => 'card_id',
                10 => 'person_code',
                11 => 'email',
                12 => 'phone',
            ],
            'lab_order' => [
                0 => 'sample_id',
            ]
        ];

        while (($data = fgetcsv($handle, 0, ',')) !== false) {
            debug($data);
            $patientEntity = $patientsTable->newEntity();
            foreach ($mapping['patient'] as $column => $field) {
                $patientEntity->$field = $data[$column];
            }
            $patientEntity->date_of_birth = date('Y-m-d', strtotime($data[1]));
            $patientEntity->health_care_client_id = 1;
            $patientEntity->ethnicity_other = 'IMPORT';
            $patientEntity = $patientsTable->save($patientEntity);
            $io->out('Patient created: ' . $patientEntity->date_of_birth);

            // generate lab order
            $labOrderEntity = $labOrdersTable->newEntity();
            $labOrderEntity->patient_id = $patientEntity->id;
            $labOrderEntity->provider_id = 114;
            $labOrderEntity->lab_order_status_id = 1;
            $labOrderEntity->sample_id = 'Sample' . $patientEntity->id;
            $labOrderEntity->user_id = 'e29ec9d4-2097-4677-b5b6-7a5516c71f6e';
            $labOrderEntity->lab_order_workflow_status_id = 18;
            $labOrderEntity->tos_accepted = 1;
            $labOrderEntity->collection_date = date('Y-m-d');
            $labOrderEntity = $labOrdersTable->save($labOrderEntity);
            $io->out('Lab Order created: ' . $labOrderEntity->sample_id);

            // create provider
            $providerEntity = $labOrderProvidersTable->newEntity();
            $providerEntity->lab_order_id = $labOrderEntity->id;
            $providerEntity->npi_number = $data[13];
            $providerEntity->provider_name = $data[14];
            $providerEntity->is_primary_care_provider = 1;
            $providerEntity->is_ordering_physician = 1;
            $labOrderProvidersTable->save($providerEntity);
            $io->out('Lab Order provider created: ' . $providerEntity->npi_number);

            // send to Lab
            //Reload labOrder
            $order = $labOrdersTable->get($labOrderEntity->id, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient.Sex',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames'
                ]
            ]);
            // Actually submit to lab here
            $healthCareClient = $order->provider->health_care_client;
            $lab = new Lab($healthCareClient->lab_id);

            // HL7 spits out warnings, silence them!
            Configure::write('debug', 0);

            $payload = $lab->getDriver()->createLabOrder($order);
            $connectionInformation = $healthCareClient->lab->toArray();

            $labOrderFileName = ROOT . '/data/lab_orders/' . $order->id . '.hl7';
            $connectionInformation['local_file'] = $labOrderFileName;
            $connectionInformation['remote_file'] = $connectionInformation['submit_path'] . '/' . $order->id . '.hl7';

            Configure::write('debug', 1);
            $result = $lab->getConnector()->createConnection($connectionInformation)->post($connectionInformation, $payload);

            if ($result) {

                $order->lab_order_status_id = 3;
                $labOrdersTable->save($order);
                $io->out('Lab Order Submitted');
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $order->id,
                    $order->lab_order_status_id,
                    'Submit to Lab',
                    'Success'
                );
            } else {
                // submit to lab failed, log it and let the user know
                $order->lab_order_status_id = 9;
                $labOrdersTable->save($order);

                $io->out('Lab Order Failed Submit: ' . $patientEntity->date_of_birth);
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $order->id,
                    $order->lab_order_status_id,
                    'Submit to Lab',
                    'Submit to Lab Failed',
                    $result
                );
            }
        }

    }
}
