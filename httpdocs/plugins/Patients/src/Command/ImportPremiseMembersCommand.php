<?php
namespace Patients\Command;

use App\Application;
use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\CommandRunner;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class ImportPremiseMembersCommand
 * @package Patients\Command
 */
class ImportPremiseMembersCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Starting');
        $patientsTable = TableRegistry::getTableLocator()->get('Patients.Patients');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $labOrderProvidersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderProviders');
        $labOrderMedicationsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications');
        $drugNdcTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');

        $csv = ROOT . '/data/premise-orders.csv';

        $initialImport = false;

        $handle = fopen($csv, 'r');

        if (!$handle) {
            $io->out('Could not open file!');
        }

        // Need to skip a line
        for ($i=0; $i < 1; $i++) {
            $void = fgetcsv($handle, 0, ',');
        }

        $mapping = [
            'patient' => [
                0 => 'date_of_birth',
                1 => 'first_name',
                2 => 'last_name',
                5 => 'ethnicity_id',
                6 => 'sex_id',
                7 => 'lib24watch_state_abbr',
                8 => 'email',
                9 => 'phone',
                12 => 'collection_date',
            ],
            'lab_order' => [
                0 => 'sample_id',
            ]
        ];

        while (($data = fgetcsv($handle, 0, ',')) !== false) {
            if (trim($data[0]) == '') {
                continue;
            }
            if (!$initialImport) {
                // ATTENTION this is meant to be ran after all orders have passed QC
                // not the initial import, just change the first / last name and resubmit to Coriell, repull reports
                $patientEntity = $patientsTable
                    ->find()->where([
                        'phone' => $data[9],
                        'date_of_birth' => $data[0]
                    ])->first();
                $patientEntity->first_name = $data[1];
                $patientEntity->last_name = $data[2];
                $patientEntity = $patientsTable->save($patientEntity);
                $io->out('Patient updated: ' . $patientEntity->last_name);

                // recreate pgx and repull reports
                $labOrder = $labOrdersTable
                    ->find()
                    ->contain([
                        'Patient.Sex',
                        'Patient.Medications',
                        'Medications',
                        'Patient.Snps',
                        'Patient.GeneCnvs',
                        'Patient.Lifestyles',
                        'Providers',
                        'Provider.HealthCareClient.Lab',
                        'Provider.HealthCareClient.PharmacogenomicProvider'
                    ])
                    ->where([
                        'patient_id' => $patientEntity->id
                    ])
                    ->first();
                $healthCareClient = $labOrder->provider->health_care_client;
                // after processing the return result we post to the PGx provider for this health care client
                $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

                $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();
                $payload = $pharmacogenomicProvider->getDriver()->createSubmit($labOrder);

                Configure::write('debug', 0);
                $result = $pharmacogenomicProvider
                    ->getConnector()
                    ->createConnection($pharmacogenomicConnectionInformation)
                    ->post($pharmacogenomicConnectionInformation, $payload);
                if (is_array($result) && isset($result['success']) && $result['success'] == 'true') {
                    $labOrder->uuid = $result['uuid'];
                    $success = true;
                } elseif ($result instanceof \SimpleXMLElement && $result->success == 'true') {
                    $labOrder->uuid = $result->uuid;
                    $success = true;
                } else {
                    $io->out('Error creating PGX report: ' . json_encode((array)$result));
                    $success = false;
                }
                if ($success) {
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Submit to PgX',
                        'Success'
                    );
                    $labOrdersTable->save($labOrder);

                    // get reports
                    $payload = $pharmacogenomicProvider->getDriver()->createReport($labOrder);

                    $pharmacogenomicConnectionInformation['headers'] = ['Accept' => 'application/json'];
                    $pharmacogenomicConnectionInformation['submit_path'] = $pharmacogenomicConnectionInformation['request_path'];

                    $report = $pharmacogenomicProvider
                        ->getConnector()
                        ->createConnection($pharmacogenomicConnectionInformation)
                        ->post($pharmacogenomicConnectionInformation, $payload);
                    $labOrderReportId = $pharmacogenomicProvider->parseReport($labOrder->id, $report);

                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Lab Report Parsed',
                        'Success'
                    );
                    $payload = json_decode($payload, true);

                    $payload['template'] = 'pgx/cls/clsmain_001';
                    // download the pdf as well
                    $pharmacogenomicConnectionInformation['headers'] = [
                        'Accept' => 'application/pdf',
                        'Content-Type' => 'application/pdf'
                    ];
                    $pharmacogenomicConnectionInformation['request_path'] = $pharmacogenomicConnectionInformation['report_path'];
                    $report = $pharmacogenomicProvider->getConnector()
                        ->createConnection($pharmacogenomicConnectionInformation)
                        ->get($pharmacogenomicConnectionInformation, $payload);

                    $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
                    file_put_contents($labOrderReportPdfFileName, $report->getStringBody());

                    $io->out('Reports pulled successfully for ');

                    $target = '25f0a720-5fc3-4b6c-9848-8283ea0281b3';
                    $data = [
                        'pgxid' => $labOrder->uuid,
                        'target' => $target
                    ];

                    $payload = $pharmacogenomicProvider->getDriver()->createPostProcess($data);

                    $connectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();
                    $connectionInformation['submit_path'] = '/report/postprocess';
                    $connectionInformation['headers'] = [];
                    $connection = $pharmacogenomicProvider
                        ->getConnector()
                        ->createConnection($connectionInformation);
                    $result = $connection->post($connectionInformation, $payload);

                    $response = $connection->getResponse();
                    if ($response->isOk()) {
                        $io->out('Associated in Coriell successfully.');
                    }
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Lab Report PDF Downloaded',
                        'Success'
                    );
                }
            } else {
                $patientEntity = $patientsTable->newEntity();
                foreach ($mapping['patient'] as $column => $field) {
                    if ($column == 5 && strlen($data[$column]) > 1) {
                        $patientEntity->$field = 8;
                        $patientEntity->ethnicity_other = trim($data[$column]);
                    } elseif (in_array($column, [1,2])) {
                        // de-identify first last name
                        $name = trim($data[$column]);
                        $name = substr_replace($name, str_repeat("X", (strlen($name) - 1)), 1, (strlen($name) - 1));
                        $patientEntity->$field = $name;
                    } else {
                        $patientEntity->$field = trim($data[$column]);
                    }
                }
                $patientEntity->health_care_client_id = 90;
                $patientEntity = $patientsTable->save($patientEntity);
                $io->out('Patient created: ' . $patientEntity->date_of_birth);

                // generate lab order
                $labOrderEntity = $labOrdersTable->newEntity();
                $labOrderEntity->patient_id = $patientEntity->id;
                $labOrderEntity->provider_id = 111; // Cari @ RxG
                $labOrderEntity->user_id = 'a9145275-41f9-4d4d-8db3-e6af0331ac88'; // Cari @ RxG
                $labOrderEntity->lab_order_status_id = 1;
                $labOrderEntity->sample_id = $data[11];
                $labOrderEntity->lab_order_workflow_status_id = 18;
                $labOrderEntity->tos_accepted = 1;
                $labOrderEntity->collection_date = $data[12];
                $labOrderEntity = $labOrdersTable->save($labOrderEntity);
                $io->out('Lab Order created: ' . $labOrderEntity->sample_id);

                // medications
                $medications = explode(',', trim($data[10]));
                foreach ($medications as $medication) {
                    if (trim($medication) != '') {
                        $temp = explode('-', trim($medication));
                        $ndc9 = $temp[0] . $temp[1];
                        $drugEntity = $drugNdcTable->find()->where(['ndc' => $ndc9])->first();
                        if ($drugEntity) {
                            $medicationEntity = $labOrderMedicationsTable->newEntity();
                            $medicationEntity->lab_order_id = $labOrderEntity->id;
                            $medicationEntity->drug_id = $drugEntity->drug_id;
                            $labOrderMedicationsTable->save($medicationEntity);
                            $io->out('Lab Order medication created: ' . $ndc9);
                        }
                    }
                }

                // create provider
                $providerEntity = $labOrderProvidersTable->newEntity();
                $providerEntity->lab_order_id = $labOrderEntity->id;
                $providerEntity->provider_name = $data[13];
                $providerEntity->npi_number = $data[14];
                $providerEntity->is_primary_care_provider = 1;
                $providerEntity->is_ordering_physician = 1;
                $labOrderProvidersTable->save($providerEntity);
                $io->out('Lab Order provider created: ' . $providerEntity->npi_number);

                // send to Lab
                //Reload labOrder
                /*$order = $labOrdersTable->get($labOrderEntity->id, [
                    'contain' => [
                        'WorkflowStatus',
                        'Patient.Sex',
                        'Providers',
                        'Provider.Users',
                        'Provider.HealthCareClient.Lab',
                        'Medications.Drug.BrandNames'
                    ]
                ]);
                // Actually submit to lab here

                $healthCareClient = $order->provider->health_care_client;
                $lab = new Lab($healthCareClient->lab_id);

                // HL7 spits out warnings, silence them!
                Configure::write('debug', 0);

                $payload = $lab->getDriver()->createLabOrder($order);
                $connectionInformation = $healthCareClient->lab->toArray();

                $labOrderFileName = ROOT . '/data/lab_orders/' . $order->id . '.hl7';
                $connectionInformation['local_file'] = $labOrderFileName;
                $connectionInformation['remote_file'] = $connectionInformation['submit_path'] . '/' . $order->id . '.hl7';

                Configure::write('debug', 1);
                $result = $lab->getConnector()->createConnection($connectionInformation)->post($connectionInformation, $payload);

                if ($result) {
                    $order->lab_order_status_id = 3;
                    $labOrdersTable->save($order);
                    $io->out('Lab Order Submitted');
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $order->id,
                        $order->lab_order_status_id,
                        'Submit to Lab',
                        'Success'
                    );
                } else {
                    // submit to lab failed, log it and let the user know
                    $order->lab_order_status_id = 9;
                    $labOrdersTable->save($order);

                    $io->out('Lab Order Failed Submit: ' . $patientEntity->date_of_birth);
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $order->id,
                        $order->lab_order_status_id,
                        'Submit to Lab',
                        'Submit to Lab Failed',
                        $result
                    );
                }*/
            }
        }

    }
}
