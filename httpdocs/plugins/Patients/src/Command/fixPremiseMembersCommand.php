<?php
namespace Patients\Command;

use App\Application;
use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\CommandRunner;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class fixPremiseMembersCommand
 * @package Patients\Command
 */
class fixPremiseMembersCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Starting');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $labOrders = $labOrdersTable->find()->where(['provider_id' => 111]);

        foreach ($labOrders as $labOrder) {
            $labOrder->barcode = $labOrder->sample_id;
            $labOrder->sample_id = 'Sample' . $labOrder->patient_id;
            $labOrdersTable->save($labOrder);
        }
    }
}
