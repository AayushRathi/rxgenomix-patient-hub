<?php
namespace Patients\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class PatientMedicationsTable
 * @package Patients\Model\Table
 */
class PatientMedicationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'Drug',
            [
                'className' => 'SystemManagement.Drugs',
                'foreignKey' => 'drug_id'
            ]
        );

    }
}
