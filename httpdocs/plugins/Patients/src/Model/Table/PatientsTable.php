<?php
namespace Patients\Model\Table;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use App\Util\ValidationHelper;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class PatientsTable
 * @package Patients\Model\Table
 */
class PatientsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Ethnicities',
            [
                'className' => 'SystemManagement.Ethnicities',
                'foreignKey' => 'ethnicity_id'
            ]
        );

        $this->belongsTo(
            'Sex',
            [
                'className' => 'SystemManagement.Sexes',
                'foreignKey' => 'sex_id'
            ]
        );

        $this->belongsTo(
            'Ethnicity',
            [
                'className' => 'SystemManagement.Ethnicities',
                'foreignKey' => 'ethnicity_id'
            ]
        );

        $this->belongsTo(
            'HealthCareClient',
            [
                'className' => 'HealthCareClients.HealthCareClients',
                'foreignKey' => 'health_care_client_id'
            ]
        );

        $this->hasMany(
            'Snps',
            [
                'className' => 'Patients.PatientSnps',
                'foreignKey' => 'patient_id'
            ]
        );

        $this->hasMany(
            'GeneCnvs',
            [
                'className' => 'Patients.PatientGeneCnvs',
                'foreignKey' => 'patient_id'
            ]
        );

        $this->hasMany(
            'Lifestyles',
            [
                'className' => 'Patients.PatientLifestyles',
                'foreignKey' => 'patient_id'
            ]
        );

        $this->hasMany(
            'Medications',
            [
                'className' => 'Patients.PatientMedications',
                'foreignKey' => 'patient_id'
            ]
        );

        $this->hasMany(
            'LabOrders',
            [
                'className' => 'LabOrders.LabOrders',
                'foreignKey' => 'patient_id'
            ]
        );

        /*
        $this->belongsToMany(
            'Icd10Codes',
            [
                'className' => 'SystemManagement.Icd10Codes',
                'foreignKey' => 'icd10_code_id',
                'targetForeignKey' => 'patient_id',
                'joinTable' => 'patient_icd10_codes',
                'through' => 'Patients.PatientIcd10Codes'
            ]
        );*/

        $this->hasMany(
            'PatientIcd10Codes',
            [
                'className' => 'Patients.PatientIcd10Codes',
                'foreignKey' => 'patient_id'
            ]
        );
    }

    /**
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // With the new phone mask on /register, the phone mask would be (XXX) XXX-XXXX
        // But still need to support existing phone numbers in the system (within profile editing) so can't force it
        if (
            isset($data['phone']) &&
            preg_match(ValidationHelper::PARENTHESIS_PHONE_REGEX, $data['phone'])
        ) {
            $data['phone'] = preg_replace('#\(|\)#', '', $data['phone']);
            $data['phone'] = preg_replace('#-#', '', $data['phone']);
            $data['phone'] = preg_replace('#\s+#', '', $data['phone']);
        }
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationIcd10Codes(Validator $validator)
    {
        return $validator
            ->requirePresence('patient_icd10_codes')
            ->notEmpty('patient_icd10_codes')
            ->add('patient_icd10_codes', 'custom', [
                'rule' => function($data, $context) {
                    $data = array_filter($data);

                    if (empty($data)) {
                        return false;
                    }

                    return true;
                }
        ]);
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('first_name')
            ->notEmpty('first_name', 'First Name is required')
            ->requirePresence('last_name')
            ->notEmpty('last_name', 'Last Name is required')
            ->requirePresence('sex_id')
            ->notEmpty('sex_id', 'Sex is required')
            ->requirePresence('phone')
            ->notEmpty('phone', 'Phone is required')
            ->requirePresence('date_of_birth')
            ->notEmpty('date_of_birth', 'Date of Birth is required')
            ->requirePresence('lib24watch_state_abbr')
            ->notEmpty('lib24watch_state_abbr', 'State is required')
            ;
    }

    /**
     * @param $query
     * @param $options
     * @return bool
     */
    public function findByDateOfBirth($query, $options)
    {
        if (isset($options['date_of_birth']) && isset($options['health_care_client_id'])) {
            $where = [
                'date_of_birth' => $options['date_of_birth'],
                'health_care_client_id' => $options['health_care_client_id']
            ];

            if (isset($options['client_id']) && !empty($options['client_id'])) {
                $where['client_id IN'] = $options['client_id'];
            }

            $query->where($where);
            // If matching to lab orders, group by patients because they can have multiple lab orders
            // But an explicit select is necessary to not select LabOrders which run into the aggregation
            // And aren't really used in the js to prepopulate patient data (_matchingData field it would be )
            if (isset($options['provider']) && $options['provider']->provider_status_id == 1) {
                $providerId = $options['provider']->id;
                $query = $query->select($this)->matching('LabOrders', function ($q) use ($providerId) {
                    return $q->where(['LabOrders.provider_id' => $providerId]);
                })->group('Patients.id');
            }

            return $query;
        }
        return false;
    }
}
