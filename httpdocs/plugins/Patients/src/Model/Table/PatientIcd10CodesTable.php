<?php

namespace Patients\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class PatientIcd10CodesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('patient_icd10_codes');

        $this->belongsTo('Patients', [
            'className' => 'Patients.Patients',
            'foreignKey' => 'patient_id'
        ]);

        $this->belongsTo('LabOrders', [
            'className' => 'LabOrders.LabOrders',
            'foreignKey' => 'lab_order_id'
        ]);

        $this->belongsTo('Icd10Codes', [
            'className' => 'SystemManagement.Icd10Codes',
            'foreignKey' => 'icd10_code_id',
        ]);
    }
}
