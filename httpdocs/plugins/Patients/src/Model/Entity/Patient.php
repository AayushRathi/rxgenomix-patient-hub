<?php
namespace Patients\Model\Entity;

use Cake\ORM\Entity;

class Patient extends Entity
{
    /**
     * @var String The benecard specific member id.
     */
    protected $_memberId;

    public function _getDateOfBirthFormatted($offset)
    {
        $defaultOffset = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('default_display_offset', 'LabOrders', null);
        if (is_null($defaultOffset)) {
            throw new \Exception('Offset setting required');
        }

        if (isset($this->date_of_birth) && is_object($this->date_of_birth)) {
            return lib24watchDate('%m/%d/%Y', $this->date_of_birth, +5);
        }

        return null;
    }

    /**
     * Get the formatted ethnicity
     * @return string
     *
     *
     */
    public function _getEthnicityFormatted()
    {
        $ethnicity = null;

        if (!is_null($this->ethnicity) && isset($this->ethnicity->title) && mb_strlen($this->ethnicity->title) > 0) {
            return $this->ethnicity->title;
        } else if (mb_strlen($this->ethnicity_other) > 0) {
            return $this->ethnicity_other;
        }

        return $ethnicity;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getMemberId()
    {
        if (is_null($this->_memberId)) {
            $temp = [
                $this->client_id,
                $this->card_id,
                $this->person_code,
                lib24watchDate('%Y-%m-%d', $this->date_of_birth, +5)
            ];
            $this->_memberId = implode('-', $temp);
        }
        return $this->_memberId;
    }
}
