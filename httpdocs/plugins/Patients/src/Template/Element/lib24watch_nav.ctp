<a href="javascript:void(0)"><span class="fa fa-group"></span><span class="xn-text"><?php echo h($lib24watchModules['Patients']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('patients_view', 'Patients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Patients",
                [
                    'plugin' => 'Patients',
                    'controller' => 'Patients',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Patients'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('patients_edit', 'Patients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Patient",
                [
                    'plugin' => 'Patients',
                    'controller' => 'Patients',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Patient'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>