<?php
$pluginTitle = $lib24watchModules['Patients']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Patients';
$this->Breadcrumbs->add($pageHeading);

$datePickerPublicFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
    "datePickerPublicFormat",
    "Lib24watch",
    "YYYY-MM-DD"
);

echo $this->Html->scriptBlock("var datePickerPublicFormatFromSetting='" . $datePickerPublicFormatFromSetting . "';");
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Patients',
                'items' => [
                    [
                        'name' => 'patient_name',
                        'fields' => ['first_name', 'last_name'],
                        'type' => 'text',
                        'label' => 'Patient Name',
                        'function' => function ($query, $value) {
                            return $query->where([
                                'OR' => [
                                    ['first_name LIKE' => '%' . $value .'%'],
                                    ['last_name LIKE' => '%' . $value .'%'],
                                    ['CONCAT(first_name, " ", last_name) LIKE' => '%' . $value .'%'],
                                ]
                            ]);
                        }
                    ],
                    [
                        'name' => 'date_of_birth',
                        'type' => 'datetimepicker',
                        'label' => 'Date of Birth'
                    ],
                    [
                        'name' => 'phone',
                        'type' => 'text',
                        'label' => 'Phone'
                    ],
                    [
                        'name' => 'facility_name',
                        'type' => 'text',
                        'label' => 'Facility'
                    ],
                    [
                        'name' => 'primary_care_provider_name',
                        'type' => 'text',
                        'label' => 'PCP'
                    ]
                ],
                $query,
                $this
            ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'Patients',
                            'controller' => 'Patients',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Patient'
                        ]
                    )
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'first_name',
                                        'label' => 'Patient Name',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->Html->link(
                                                $row->first_name . ' ' . $row->last_name,
                                                [
                                                    'plugin' => 'Patients',
                                                    'controller' => 'Patients',
                                                    'action' => 'edit',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ]
                                            );
                                        }
                                    ],
                                    [
                                        'name' => 'date_of_birth',
                                        'label' => 'DOB',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return lib24watchDate('shortDateFormat', $row->date_of_birth, +5);
                                        }
                                    ],
                                    [
                                        'name' => 'email',
                                        'label' => 'Email',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'phone',
                                        'label' => 'Phone',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'Patients',
                                                'controller' => 'Patients',
                                                'row' => $row,
                                                'editOnly' => true,

                                            ]);
                                        }
                                    ]
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to
                            <?php
                            echo $this->Html->link(
                                'Add a Patient',
                                [
                                    'plugin' => 'Patients',
                                    'controller' => 'Patients',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Patient'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
