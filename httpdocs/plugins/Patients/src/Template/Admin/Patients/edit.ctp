<?php
$pluginTitle = $lib24watchModules['Patients']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Patients"),
    [
        'plugin' => 'Patients',
        'controller' => 'Patients',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($patientId ? "Edit" : "Add") . " Patient";
$this->Breadcrumbs->add($pageHeading);
// There is no Patient JS
//echo $this->Html->script('/Patients/js/admin/Patients/edit.js', ['block' => 'extra-scripts']);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($patientEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
            				echo $this->Form->control(
            					'first_name',
            					[
            						'type' => 'text',
            						'label' => 'First Name',
                                    'required' => true
            					]
            				);
            				echo $this->Form->control(
            					'last_name',
            					[
            						'type' => 'text',
            						'label' => 'Last Name',
                                    'required' => true
            					]
            				);
                            if ($this->Lib24watchPermissions->hasPermission('patients_set_health_care_client', 'Patients')) {
                                echo $this->Form->control(
                                    'health_care_client_id',
                                    [
                                        'type' => 'select',
                                        'label' => 'Health Care Client',
                                        'empty' => true,
                                        'options' => $healthCareClients
                                    ]
                                );
                            }
                            echo $this->Form->control(
                                'ethnicity_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Ethnicity',
                                    'empty' => true,
                                    'options' => $ethnicities
                                ]
                            );
                            echo $this->Form->control(
                                'sex_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Sex',
                                    'empty' => true,
                                    'options' => $sexes
                                ]
                            );
                            echo $this->Form->control(
                                'address_1',
                                [
                                    'label' => 'Street Address',
                                    'required' => true,
                                    'type' => 'text'
                                ]
                            );
                            echo $this->Form->control(
                                'address_2',
                                [
                                    'label' => 'Street Address Cont.',
                                    'required' => false,
                                    'type' => 'text'
                                ]
                            );
                            echo $this->Form->control(
                                'city',
                                [
                                    'label' => 'City',
                                    'required' => true,
                                    'type' => 'text'
                                ]
                            );
                            echo $this->Form->control(
                                'lib24watch_state_abbr',
                                [
                                    'type' => 'select',
                                    'label' => 'State',
                                    'empty' => true,
                                    'options' => $states
                                ]
                            );
                            echo $this->Form->control(
                                'postal_code',
                                [
                                    'type' => 'text',
                                    'label' => 'Zip',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'date_of_birth',
                                [
                                    'type' => 'datetimepicker',
                                    'label' => 'Date of Birth',
                                    'required' => true,
                                    'templateVars' => [
                                            'help' => 'Click to choose a date using the dialog, or enter a date using the format MM-DD-YYYY.'
                                    ]
                                ]
                            );
                            echo $this->Form->control(
                                'email',
                                [
                                    'type' => 'text',
                                    'label' => 'Email',
                                    'required' => false
                                ]
                            );
                            echo $this->Form->control(
                                'phone',
                                [
                                    'type' => 'text',
                                    'label' => 'Phone',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'primary_care_provider',
                                [
                                    'type' => 'text',
                                    'label' => 'Primary Care Provider Name',
                                    'required' => false
                                ]
                            );
                            echo $this->Form->control(
                                'primary_care_provider_npi_number',
                                [
                                    'type' => 'text',
                                    'label' => 'PCP NPI #',
                                    'required' => false
                                ]
                            );
                            echo $this->Form->control(
                                'facility_name',
                                [
                                    'type' => 'text',
                                    'label' => 'Facility Name',
                                    'required' => false
                                ]
                            );
                            echo $this->Form->control(
                                'facility_npi_number',
                                [
                                    'type' => 'text',
                                    'label' => 'Facility NPI #',
                                    'required' => false
                                ]
                            );
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
