<?php
use Migrations\AbstractMigration;

class AddLib24watchStateAbbrToPatients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('patients');
        $table->addColumn('lib24watch_state_abbr', 'string', [
            'default' => null,
            'limit' => 2,
            'null' => false,
        ]);
        $table->update();
    }
}
