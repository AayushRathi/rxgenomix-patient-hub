<?php
use Migrations\AbstractMigration;

class FixHealthCareClientColumn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('patients')
            ->changeColumn('health_care_client_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('health_care_client_id', 'health_care_clients', 'id')
            ->update();
    }
}
