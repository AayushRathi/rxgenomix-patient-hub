<?php
use Migrations\AbstractMigration;

class FixClientIds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // This needs to be reviewed
        /*
        $this->query('
                update patients
                inner join health_care_clients on (
                    health_care_clients.id = patients.health_care_client_id
                )
                left join health_care_client_groups hccg on(hccg.id = patients.client_id)
                inner join health_care_client_groups hccg2 on(hccg2.title = patients.client_id)
                set client_id = hccg2.id
                where
                    hccg.id is null
                and patients.client_id is not null
                and hccg2.id is not null;
        ');
         */
    }
}
