<?php
use Migrations\AbstractMigration;

class AddFksToPatients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('patients')
            ->addForeignKey('rxg_user_id', 'users', 'id')
            ->addForeignKey('ethnicity_id', 'ethnicities', 'id')
            /* Data needs to be fixed for HCCs and client_id
            ->addForeignKey('health_care_client_id', 'health_care_clients', 'id')
            ->addForeignKey('client_id', 'clients', 'id')
            */
            ->addForeignKey('sex_id', 'sexes', 'id')
            ->update();
    }
}
