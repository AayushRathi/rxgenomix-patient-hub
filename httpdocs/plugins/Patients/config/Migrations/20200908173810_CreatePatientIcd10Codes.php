<?php
use Migrations\AbstractMigration;

class CreatePatientIcd10Codes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('patient_icd10_codes');

        $table
            ->addColumn('id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false,
                'autoIncrement' => true,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('patient_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('icd10_code_id', 'integer', [
                'limit' => 11,
                'signed' => false,
                'null' => false,
            ])
            ->addColumn('lab_order_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false,
            ])
            ->create();

        $table
            ->addForeignKey('patient_id', 'patients', 'id')
            ->addForeignKey('lab_order_id', 'lab_orders', 'id')
            ->addForeignKey('icd10_code_id', 'icd10_codes', 'id')
            ->update();
    }
}
