<?php
use Migrations\AbstractMigration;

class ChangeClientIdOnPatients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('patients');
        $table->changeColumn('client_id', 'string', [
            'limit' => 255
        ]);
        $table->update();
    }
}
