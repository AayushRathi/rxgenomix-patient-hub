<?php
use Migrations\AbstractMigration;

class AddAddressFieldsToPatients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('patients')
            ->addColumn('address_1', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('address_2', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('city', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('postal_code', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 10
            ])
            ->update();
    }
}
