<?php
use Migrations\AbstractSeed;

/**
 * ConnectionTypes seed.
 */
class ConnectionTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'JSON API',
                'is_active' => '1',
                'display_order' => '0',
            ],
            [
                'id' => '2',
                'title' => 'SFTP',
                'is_active' => '1',
                'display_order' => '1',
            ],
        ];

        $table = $this->table('connection_types');
        $table->insert($data)->save();
    }
}
