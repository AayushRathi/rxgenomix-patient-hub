<?php
use Migrations\AbstractSeed;

/**
 * DiscountTypes seed.
 */
class DiscountTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'Percentage',
                'is_active' => '1',
                'display_order' => '0',
            ],
            [
                'id' => '2',
                'title' => 'Fixed Rate',
                'is_active' => '1',
                'display_order' => '1',
            ],
        ];

        $table = $this->table('discount_types');
        $table->insert($data)->save();
    }
}
