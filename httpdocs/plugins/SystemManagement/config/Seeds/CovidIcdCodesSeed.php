<?php
use Migrations\AbstractSeed;

/**
 * CovidIcd10Codes seed.
 */
class CovidIcdCodesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'code' => 'Z03.818',
                'title' => 'Suspected exposure to COVID-19',
                'description' => null,
                'is_active' => 1,
                'display_order' => 1,
            ],
            [
                'id' => 2,
                'code' => 'Z20.828',
                'title' => 'Known Exposure to COVID-19',
                'description' => null,
                'is_active' => 1,
                'display_order' => 2,
            ],
            [
                'id' => 3,
                'code' => 'J12.89',
                'title' => 'Pneumonia (COVID-19)',
                'description' => null,
                'is_active' => 1,
                'display_order' => 3,
            ],
            [
                'id' => 4,
                'code' => 'J20.8',
                'title' => 'Acute Bronchitis (COVID-19)',
                'description' => null,
                'is_active' => 1,
                'display_order' => 4,
            ],
            [
                'id' => 5,
                'code' => 'J40',
                'title' => 'Bronchitis (COVID-19)',
                'description' => null,
                'is_active' => 1,
                'display_order' => 4,
            ],
            [
                'id' => 6,
                'code' => 'R05',
                'title' => 'Cough',
                'description' => null,
                'is_active' => 1,
                'display_order' => 6,
            ],
            [
                'id' => 7,
                'code' => 'R06.02',
                'title' => 'Shortness of Breath',
                'description' => null,
                'is_active' => 1,
                'display_order' => 7,
            ],
            [
                'id' => 8,
                'code' => 'R50.9',
                'title' => 'Fever, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 8,
            ],
            [
                'id' => 9,
                'code' => 'J01.90',
                'title' => 'Acute Sinusitis, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 9,
            ],
            [
                'id' => 10,
                'code' => 'J02.9',
                'title' => 'Acute Pharyngitis, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 10,
            ],
            [
                'id' => 11,
                'code' => 'J06.9',
                'title' => 'Acute Upper Respiratory Infection, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 11,
            ],
            [
                'id' => 12,
                'code' => 'J18.9',
                'title' => 'Pneumonia, Unspecified organism',
                'description' => null,
                'is_active' => 1,
                'display_order' => 12,
            ],
            [
                'id' => 13,
                'code' => 'J20.9',
                'title' => 'Acute Bronchitis, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 13,
            ],
            [
                'id' => 14,
                'code' => 'J32.9',
                'title' => 'Chronic Sinusitis, Unspecified',
                'description' => null,
                'is_active' => 1,
                'display_order' => 14,
            ],
            [
                'id' => 15,
                'code' => 'Z11.59',
                'title' => 'Encounter for screening for other viral diseases (asymptomatic)',
                'description' => null,
                'is_active' => 1,
                'display_order' => 15,
            ],
            [
                'id' => 16,
                'code' => 'B97.29',
                'title' => 'Other viral pneumonia',
                'description' => null,
                'is_active' => 1,
                'display_order' => 16
            ],
            [
                'id' => '17',
                'code' => 'J22',
                'title' => 'Lower Respiratory Infection (COVID-19)',
                'description' => null,
                'is_active' => 1,
                'display_order' => 17
            ]
        ];

        $table = $this->table('icd10_codes');
        $table->insert($data)->save();
    }
}
