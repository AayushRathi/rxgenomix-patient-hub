<?php
use Migrations\AbstractSeed;

/**
 * ApiPermissions seed.
 */
class ApiPermissionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'plugin' => 'LabOrders',
                'permission' => 'list',
            ],
            [
                'id' => '2',
                'plugin' => 'LabOrders',
                'permission' => 'lookup',
            ],
            [
                'id' => '3',
                'plugin' => 'LabOrders',
                'permission' => 'member_reports',
            ],
            [
                'id' => '4',
                'plugin' => 'LabOrders',
                'permission' => 'qc',
            ],
        ];

        $table = $this->table('api_permissions');
        $table->insert($data)->save();
    }
}
