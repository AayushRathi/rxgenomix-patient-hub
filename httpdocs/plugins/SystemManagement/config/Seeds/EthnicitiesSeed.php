<?php
use Migrations\AbstractSeed;

/**
 * Ethnicities seed.
 */
class EthnicitiesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'Black or African-American',
                'is_active' => '1',
                'display_order' => '0',
            ],
            [
                'id' => '2',
                'title' => 'Asian',
                'is_active' => '1',
                'display_order' => '1',
            ],
            [
                'id' => '3',
                'title' => 'White or Caucasian',
                'is_active' => '1',
                'display_order' => '2',
            ],
            [
                'id' => '4',
                'title' => 'Hispanic or Latino',
                'is_active' => '1',
                'display_order' => '3',
            ],
            [
                'id' => '5',
                'title' => 'American Indian or Alaska Native',
                'is_active' => '1',
                'display_order' => '4',
            ],
            [
                'id' => '6',
                'title' => 'Ashkenazi/Sephardi Jewish',
                'is_active' => '1',
                'display_order' => '5',
            ],
            [
                'id' => '7',
                'title' => 'Native Hawaiian or Other Pacific Islander',
                'is_active' => '1',
                'display_order' => '6',
            ],
            [
                'id' => '8',
                'title' => 'Other',
                'is_active' => '1',
                'display_order' => '7',
            ],
        ];

        $table = $this->table('ethnicities');
        $table->insert($data)->save();
    }
}
