<?php
use Migrations\AbstractSeed;

/**
 * Sexes seed.
 */
class SecuritySeeds extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'setting_key' => 'password_cadence',
            'setting_value' => 90
        ];

        $table = $this->table('security_settings');
        $table->insert($data)->save();
    }
}
