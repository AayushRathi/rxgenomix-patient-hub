<?php
use Migrations\AbstractSeed;

/**
 * AuthenticationTypes seed.
 */
class AuthenticationTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'Basic Auth',
                'is_active' => '1',
                'display_order' => '0',
            ],
            [
                'id' => '2',
                'title' => 'Bearer Token',
                'is_active' => '1',
                'display_order' => '1',
            ],
            [
                'id' => '3',
                'title' => 'OAuth 2.0',
                'is_active' => '1',
                'display_order' => '2',
            ],
            [
                'id' => 4,
                'title' => 'OAuth POST Body / JSON Response',
                'is_active' => '1',
                'display_order' => '3'
            ]
        ];

        $table = $this->table('authentication_types');
        $table->insert($data)->save();
    }
}
