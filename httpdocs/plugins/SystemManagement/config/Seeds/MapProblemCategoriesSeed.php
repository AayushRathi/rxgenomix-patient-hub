<?php
use Migrations\AbstractSeed;

/**
 * MapProblemCategories seed.
 */
class MapProblemCategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'title' => 'Unnecessary medication therapy',
                'is_active' => 1,
                'display_order' => 0
            ],
            [
                'id' => 2,
                'title' => 'Needs additional medication therapy',
                'is_active' => 1,
                'display_order' => 1
            ],
            [
                'id' => 3,
                'title' => 'Ineffective medication ',
                'is_active' => 1,
                'display_order' => 2
            ],
            [
                'id' => 4,
                'title' => 'Dosage too low',
                'is_active' => 1,
                'display_order' => 3
            ],
            [
                'id' => 5,
                'title' => 'Needs additional monitoring',
                'is_active' => 1,
                'display_order' => 4
            ],
            [
                'id' => 6,
                'title' => 'Adverse medication event',
                'is_active' => 1,
                'display_order' => 5
            ],
            [
                'id' => 7,
                'title' => 'Dosage too high',
                'is_active' => 1,
                'display_order' => 6
            ],
            [
                'id' => 8,
                'title' => 'Needs additional monitoring',
                'is_active' => 1,
                'display_order' => 7
            ],
            [
                'id' => 9,
                'title' => 'Adherence',
                'is_active' => 1,
                'display_order' => 8
            ],
            [
                'id' => 10,
                'title' => 'Cost',
                'is_active' => 1,
                'display_order' => 9
            ],
        ];

        $table = $this->table('map_problem_categories');
        $table->insert($data)->save();
    }
}
