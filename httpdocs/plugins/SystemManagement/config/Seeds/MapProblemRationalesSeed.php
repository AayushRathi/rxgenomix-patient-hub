<?php
use Migrations\AbstractSeed;

/**
 * MapProblemRationales seed.
 */
class MapProblemRationalesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'title' => 'Duplicate Therapy',
                'is_active' => 1,
                'map_problem_category_id' => 1,
                'display_order' => 0
            ],
            [
                'id' => 2,
                'title' => 'No medical indication at this time',
                'is_active' => 1,
                'map_problem_category_id' => 1,
                'display_order' => 1
            ],
            [
                'id' => 3,
                'title' => 'Nonmedication therapy more appropriate',
                'is_active' => 1,
                'map_problem_category_id' => 1,
                'display_order' => 2
            ],
            [
                'id' => 4,
                'title' => 'Addiction/recreational medication use',
                'is_active' => 1,
                'map_problem_category_id' => 1,
                'display_order' => 3
            ],
            [
                'id' => 5,
                'title' => 'Treating avoidable adverse medication reaction',
                'is_active' => 1,
                'map_problem_category_id' => 1,
                'display_order' => 4
            ],
            [
                'id' => 6,
                'title' => 'Preventive therapy',
                'is_active' => 1,
                'map_problem_category_id' => 2,
                'display_order' => 0
            ],
            [
                'id' => 7,
                'title' => 'Untreated condition ',
                'is_active' => 1,
                'map_problem_category_id' => 2,
                'display_order' => 1
            ],
            [
                'id' => 8,
                'title' => 'Synergistic therapy',
                'is_active' => 1,
                'map_problem_category_id' => 2,
                'display_order' => 2
            ],
            [
                'id' => 9,
                'title' => 'More effective medication available',
                'is_active' => 1,
                'map_problem_category_id' => 3,
                'display_order' => 0
            ],
            [
                'id' => 10,
                'title' => 'Condition refractory to medication ',
                'is_active' => 1,
                'map_problem_category_id' => 3,
                'display_order' => 0
            ],
            [
                'id' => 11,
                'title' => 'Dosage form inappropriate',
                'is_active' => 1,
                'map_problem_category_id' => 3,
                'display_order' => 1
            ],
            [
                'id' => 12,
                'title' => 'Dose too low',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 0
            ],
            [
                'id' => 13,
                'title' => 'Frequency inappropriate',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 1
            ],
            [
                'id' => 14,
                'title' => 'Incorrect administration',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 2
            ],
            [
                'id' => 15,
                'title' => 'Medication interaction',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 0
            ],
            [
                'id' => 16,
                'title' => 'Incorrect storage',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 1
            ],
            [
                'id' => 17,
                'title' => 'Duration inappropriate',
                'is_active' => 1,
                'map_problem_category_id' => 4,
                'display_order' => 2
            ],
            [
                'id' => 18,
                'title' => 'Medication requires monitoring',
                'is_active' => 1,
                'map_problem_category_id' => 5,
                'display_order' => 0
            ],
            [
                'id' => 19,
                'title' => 'Undesirable effect',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 0
            ],
            [
                'id' => 20,
                'title' => 'Unsafe medication for the patient',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 1
            ],
            [
                'id' => 21,
                'title' => 'Medication interaction',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 2
            ],
            [
                'id' => 22,
                'title' => 'Incorrect administration',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 3
            ],
            [
                'id' => 23,
                'title' => 'Allergic reaction',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 4
            ],
            [
                'id' => 24,
                'title' => 'Dosage increase/decrease too fast',
                'is_active' => 1,
                'map_problem_category_id' => 6,
                'display_order' => 5
            ],
            [
                'id' => 25,
                'title' => 'Dose too high',
                'is_active' => 1,
                'map_problem_category_id' => 7,
                'display_order' => 0
            ],
            [
                'id' => 26,
                'title' => 'Frequency inappropriate',
                'is_active' => 1,
                'map_problem_category_id' => 7,
                'display_order' => 1
            ],
            [
                'id' => 27,
                'title' => 'Duration inappropriate',
                'is_active' => 1,
                'map_problem_category_id' => 7,
                'display_order' => 2
            ],
            [
                'id' => 28,
                'title' => 'Medication interaction',
                'is_active' => 1,
                'map_problem_category_id' => 7,
                'display_order' => 3
            ],
            [
                'id' => 29,
                'title' => 'Medication requires monitoring',
                'is_active' => 1,
                'map_problem_category_id' => 8,
                'display_order' => 0
            ],
            [
                'id' => 30,
                'title' => 'Does not understand instructions',
                'is_active' => 1,
                'map_problem_category_id' => 9,
                'display_order' => 0
            ],
            [
                'id' => 31,
                'title' => 'Patient prefers not to take',
                'is_active' => 1,
                'map_problem_category_id' => 9,
                'display_order' => 1
            ],
            [
                'id' => 32,
                'title' => 'Patient forgets to take',
                'is_active' => 1,
                'map_problem_category_id' => 9,
                'display_order' => 2
            ],
            [
                'id' => 33,
                'title' => 'Medication product not available',
                'is_active' => 1,
                'map_problem_category_id' => 9,
                'display_order' => 3
            ],
            [
                'id' => 34,
                'title' => 'Cannot swallow/administer medication',
                'is_active' => 1,
                'map_problem_category_id' => 9,
                'display_order' => 4
            ],
            [
                'id' => 35,
                'title' => 'More cost-effective medication available*',
                'is_active' => 1,
                'map_problem_category_id' => 10,
                'display_order' => 0
            ],
            [
                'id' => 36,
                'title' => 'Cannot afford medication product',
                'is_active' => 1,
                'map_problem_category_id' => 10,
                'display_order' => 1
            ],
        ];

        $table = $this->table('map_problem_rationales');
        $table->insert($data)->save();
    }
}
