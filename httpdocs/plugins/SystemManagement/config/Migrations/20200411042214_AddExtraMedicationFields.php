<?php
use Migrations\AbstractMigration;

class AddExtraMedicationFields extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medications')
            ->addColumn('is_brand', 'boolean', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('is_pgx_relevant', 'boolean', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('prealert_trigger', 'boolean', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('category', 'string', [
                'limit' => 255,
                'null' => true,
                'default' => null,
            ])
            ->addColumn('drug_class', 'string', [
                'limit' => 255,
                'null' => true,
                'default' => null,
            ])
            ->addColumn('trade_names', 'string', [
                'limit' => 255,
                'null' => true,
                'default' => null,
            ])
            ->update();
    }
}
