<?php
use Migrations\AbstractMigration;

class AddInRxcuiMinRxcuiToDrugs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs')
            ->addColumn('in_rxcui_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'after' => 'rxcui_id',
                'default' => null,
                'null' => true
            ])
            ->addColumn('min_rxcui_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'after' => 'rxcui_id',
                'default' => null,
                'null' => true
            ])
            ->addForeignKey('in_rxcui_id', 'rxcuis', 'id')
            ->addForeignKey('min_rxcui_id', 'rxcuis', 'id')
            ->update();
    }
}
