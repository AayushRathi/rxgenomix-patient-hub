<?php
use Migrations\AbstractMigration;

class AddMarkForReviewToDrugs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs')
            ->addColumn('mark_for_review', 'boolean', [
                'default' => 0
            ])
            ->update();
    }
}
