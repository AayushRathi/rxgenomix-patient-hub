<?php
use Migrations\AbstractMigration;

class RemoveDrugNdcUniqueIndex extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->query('ALTER TABLE drug_ndc_rxcui DROP INDEX `drug_ndc_id`, ADD UNIQUE `drug_ndc_id` (`drug_ndc_id`, `rxcui_id`);');
    }
}
