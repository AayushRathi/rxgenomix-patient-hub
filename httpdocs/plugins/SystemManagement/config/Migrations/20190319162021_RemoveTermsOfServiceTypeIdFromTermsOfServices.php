<?php
use Migrations\AbstractMigration;

class RemoveTermsOfServiceTypeIdFromTermsOfServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('terms_of_services');
        $table->removeColumn('terms_of_service_type_id');
        $table->update();
    }
}
