<?php
use Migrations\AbstractMigration;

class AddCodeTextToMedications extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medications')
            ->addColumn('code_text', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->update();
    }
}
