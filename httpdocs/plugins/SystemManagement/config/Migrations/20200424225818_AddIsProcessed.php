<?php
use Migrations\AbstractMigration;

class AddIsProcessed extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs');
        $table->addColumn('is_processed', 'boolean', [
            'default' => 0,
            'comment' => 'Mark if the drug was processed by UpdateDrugs which fills in mapping information. Only used by command.'
        ])->update();
    }
}
