<?php
use Migrations\AbstractMigration;

class UpdateMedicationsTsiRxcuiIdToNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('ALTER TABLE `medications` CHANGE COLUMN `tsi_rxcui_id` `tsi_rxcui_id` int(10) UNSIGNED DEFAULT NULL COMMENT "TSI stores the RXCUI for a medication based on the tty value of IN, we are using SCD for now so storing both"');
    }
}
