<?php
use Migrations\AbstractMigration;

class CreateIcd10CodesTable extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('icd10_codes')
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'null' => false,
                'autoIncrement' => true,
                'signed' => false
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 255
            ])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 255
            ])
            ->addColumn('description', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => 0
            ])
            ->addColumn('display_order', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false,
            ])
            ->addPrimaryKey(['id'])
            ->create();
    }
}
