<?php
use Migrations\AbstractMigration;

class AddDrugNdcRxcui extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drug_ndc_rxcui');

        $table->addPrimaryKey(['drug_ndc_id'])
            ->addColumn('drug_ndc_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false
            ])
            ->addColumn('rxcui', 'integer', [
                'limit' => 11,
                'signed' => false
            ])
            ->addIndex(['drug_ndc_id', 'rxcui', 'rxcui_updated'], ['unique' => true])
            ->addColumn('rxcui_updated', 'date', [
                'default' => null,
                'null' => true
            ])
            ->create();

        $table->addForeignKey(
            'drug_ndc_id',
            'drug_ndc',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )->update();
    }
}
