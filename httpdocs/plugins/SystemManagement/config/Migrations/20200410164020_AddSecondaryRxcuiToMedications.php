<?php
use Migrations\AbstractMigration;

class AddSecondaryRxcuiToMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // reset medications because we restructured and now require storing the RXCUI that TSI stores
        //$this->query('TRUNCATE TABLE medications');

        $table = $this->table('medications')
            ->addColumn('tsi_rxcui_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false,
                'comment' => 'TSI stores the RXCUI for a medication based on the tty value of IN, we are using SCD for now so storing both'
            ])
            ->addForeignKey(
                'tsi_rxcui_id',
                'rxcui',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )->update();
    }
}
