<?php
use Migrations\AbstractMigration;

class RenameRxcuiIdToTsiRxcuiAndAddRxcuiToDrugs extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs')
            ->addColumn('tsi_rxcui_id', 'integer', [
                'limit' => 10,
                'null' => true,
                'signed' => false,
                'after' => 'medication_id'
            ])->update();
        
        $this->table('drugs')->addForeignKey('tsi_rxcui_id', 'rxcuis','id')->update();
    }
}
