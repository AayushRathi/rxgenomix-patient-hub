<?php
use Migrations\AbstractMigration;

class AdjustRxcuiIdForMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('medications')
            ->removeColumn('rxcui_id')
            ->dropForeignKey('rxcui_id')
            ->update();

        $this->table('medications')
            ->addColumn('rxcui_id', 'integer', [
                    'limit' => 10,
                    'null' => true,
                    'comment' => 'This is intended to be the only SBC Rxcui found from aggregating the NDC9s we have for a given drug and hopefully we dont have more than one',
                    'signed' => false
                ])
            ->addForeignKey(
                'rxcui_id',
                'rxcui',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'CASCADE'
                ]
            )->update();
    }
}
