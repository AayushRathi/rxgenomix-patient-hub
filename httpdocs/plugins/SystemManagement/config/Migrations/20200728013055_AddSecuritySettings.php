<?php
use Migrations\AbstractMigration;

class AddSecuritySettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('security_settings')
            ->addColumn('setting_key', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('setting_value', 'integer', [
                'limit' => 10,
                'signed' => false,
                'default' => null,
                'null' => true
            ])
            ->create();
    }
}
