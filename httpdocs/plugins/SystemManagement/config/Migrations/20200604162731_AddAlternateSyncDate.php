<?php
use Migrations\AbstractMigration;

class AddAlternateSyncDate extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medications');

        $table->addColumn('alternate_sync_date', 'datetime', [
            'default' => null,
            'null' => true,
        ])->update();
    }
}
