<?php
use Migrations\AbstractMigration;

class AddUniqueTsiMedicationIdToMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medications')
            ->addIndex(['tsi_medication_id'], ['unique' => true])
            ->update();
    }
}
