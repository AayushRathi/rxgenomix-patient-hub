<?php
use Migrations\AbstractMigration;

class UpdateDrugsWithTsiIdAndIsSearchable extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs')
            ->addColumn('medication_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
                'signed' => false,
            ])
            ->addColumn('rxcui_id', 'integer', [
                'limit' => 10,
                'null' => true,
                'signed' => false
            ])
            ->addColumn('is_searchable', 'boolean', [
                'default' => 1
            ])
            ->update();
        
        $this->table('drugs')->addForeignKey('rxcui_id', 'rxcui','id')->addForeignKey('medication_id', 'medications', 'id')->update();
    }
}
