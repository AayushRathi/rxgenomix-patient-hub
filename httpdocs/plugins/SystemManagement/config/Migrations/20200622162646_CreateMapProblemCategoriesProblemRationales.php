<?php
use Migrations\AbstractMigration;

class CreateMapProblemCategoriesProblemRationales extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('map_problem_categories')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'null' => false,
                'limit' => 255,
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => true,
                'signed' => false,
            ])
            ->addColumn('display_order', 'integer', [
                'null' => false,
                'limit' => 10,
                'signed' => false,
            ])
            ->create();

        $table = $this->table('map_problem_rationales')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'null' => false,
                'limit' => 255
            ])
            ->addColumn('map_problem_category_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => true,
                'signed' => false,
            ])
            ->addColumn('display_order', 'integer', [
                'null' => false,
                'limit' => 10,
                'signed' => false,
            ])
            ->create();

        $table = $this->table('map_problem_rationales')
            ->addForeignKey(
                'map_problem_category_id',
                'map_problem_categories',
                'id'
            )->update();
    }
}
