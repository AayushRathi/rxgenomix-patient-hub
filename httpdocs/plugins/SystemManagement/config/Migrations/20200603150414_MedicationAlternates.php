<?php
use Migrations\AbstractMigration;

class MedicationAlternates extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medication_alternates');

        $table
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('medication_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('alternate_medication_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('is_active', 'boolean', [
                'default' => 1
            ])
            ->create();

        $table
            ->addForeignKey('medication_id', 'medications', 'id')
            ->addForeignKey('alternate_medication_id', 'medications', 'id')
            ->update();
    }
}
