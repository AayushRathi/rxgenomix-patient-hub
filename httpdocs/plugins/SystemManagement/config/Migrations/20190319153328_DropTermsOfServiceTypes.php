<?php
use Migrations\AbstractMigration;

class DropTermsOfServiceTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('terms_of_service_types');
        $table->drop()->save();
    }
}
