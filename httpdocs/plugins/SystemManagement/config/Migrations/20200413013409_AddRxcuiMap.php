<?php
use Migrations\AbstractMigration;

class AddRxcuiMap extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('rxcui_map');

        $table->addColumn('id', 'integer', [
            'autoIncrement' => true,
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ])->addColumn('parent_rxcui_id', 'integer', [
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ])->addColumn('child_rxcui_id', 'integer', [
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ])->addPrimaryKey(['id'])->create();

        $table->addForeignKey(
            'parent_rxcui_id',
            'rxcui',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )
        ->addForeignKey(
            'child_rxcui_id',
            'rxcui',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )->update();
    }
}
