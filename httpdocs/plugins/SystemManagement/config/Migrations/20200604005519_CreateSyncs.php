<?php
use Migrations\AbstractMigration;

class CreateSyncs extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('syncs')
            ->addColumn('id', 'integer', [
                'limit' => 10,
                'autoIncrement' => true,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('type', 'enum', [
                'values' => [
                    'fetch_medications_from_tsi',
                    'fetch_tsi_alternates',
                    'update_drugs',
                    'fetch_rxcui_for_ndc'
                ]
            ])
            ->addColumn('initial_start', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('last_started', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('last_finished', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('metadata', 'json', [
                'default' => null,
                'null' => true
            ])
            ->create();
    }
}
