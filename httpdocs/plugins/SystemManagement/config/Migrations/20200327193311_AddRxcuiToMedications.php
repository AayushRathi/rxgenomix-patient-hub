<?php
use Migrations\AbstractMigration;

class AddRxcuiToMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medications');

        $table->addColumn('rxcui_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false
            ])->update();

        $table->addForeignKey(
            'rxcui_id',
            'rxcui',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )->update();
    }
}
