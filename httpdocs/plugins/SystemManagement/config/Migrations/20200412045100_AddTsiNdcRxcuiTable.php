<?php
use Migrations\AbstractMigration;

class AddTsiNdcRxcuiTable extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tsi_ndc_rxcui');

        $table
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'limit' => 10,
                'signed' => false,
                'null' => false
            ])
            ->addColumn('ndc_11', 'string', [
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('ndc_9', 'string', [
                'limit' => 9,
                'null' => false,
            ])
            ->addColumn('rxcui_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false
            ])
            ->addColumn('tsi_medication_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false
            ])
            ->addColumn('created', 'date')
            ->addPrimaryKey(['id'])
            ->create();

        $table->addForeignKey(
            'rxcui_id',
            'rxcui',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )->update();

        $table->addForeignKey(
            'tsi_medication_id',
            'medications',
            'tsi_medication_id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->update();
    }
}
