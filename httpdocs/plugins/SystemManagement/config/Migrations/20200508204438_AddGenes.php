<?php
use Migrations\AbstractMigration;

class AddGenes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('genes')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('approved_symbol', 'string', [
                'null' => false,
                'limit' => 255
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('created', 'date')
            ->create();
    }
}
