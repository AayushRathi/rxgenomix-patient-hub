<?php
use Migrations\AbstractMigration;

class AddRxcuis extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('rxcui');
        $table->addColumn('id', 'integer', [
            'autoIncrement' => true,
            'default' => null,
            'limit' => 10,
            'null' => false,
            'signed' => false,
        ])->addColumn('rxcui', 'integer', [
            'limit' => 10,
            'null' => false,
            'signed' => false
        ])->addColumn('rxcui_updated', 'date', [
            'default' => null,
            'null' => true
        ])->addColumn('title', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 255
        ])->addColumn('tty', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 4
        ])->addColumn('is_active', 'boolean', [
            'default' => 0
        ])->addPrimaryKey(['id'])->create();

        $table = $this->table('drug_ndc_rxcui');

        $table
            ->removeColumn('rxcui')
            ->removeColumn('rxcui_updated')
            ->addColumn('rxcui_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->update();

        $this->query('TRUNCATE TABLE drug_ndc_rxcui');
        $table->addForeignKey(
            'rxcui_id',
            'rxcui',
            'id',
            [
                'update' => 'CASCADE',
                'delete' => 'CASCADE'
            ]
        )->update();
    }
}
