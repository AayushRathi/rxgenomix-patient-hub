<?php
use Migrations\AbstractMigration;

class AddIsManuallySetMedication extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs');
        $table->addColumn('is_manually_set_medication', 'boolean', [
            'default' => 0,
            'comment' => 'If there were multiple drug_rxcui records that had matching tsi medications, we have to manually set. This is to flag to QA it'
        ])->update();
    }
}
