<?php
use Migrations\AbstractMigration;

class AddMappedTypeIdToDrugs extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('drugs')
            ->addColumn('mapped_type_id', 'integer', [
                'null' => true,
                'default' => null,
                'limit' => 10,
                'signed' => false,
            ])
            ->addColumn('mapped_date', 'date', [
                'null' => true,
                'default' => null,
            ])
            ->update();
    }
}
