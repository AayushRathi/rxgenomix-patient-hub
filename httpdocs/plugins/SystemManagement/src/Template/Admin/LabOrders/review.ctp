<?php
    $this->assign('title', 'Lab Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Lab Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Lab Order');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3 class="panel-title">View Lab Order</h3>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title-decoration">Lab Order #<?= $order->id; ?></h2>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Patient Information</h3>
                                <span>Name:</span> <?= $order->patient->first_name . ' ' . $order->patient->last_name; ?>
                                <br>
                                <span>Ethnicity:</span>
                                <?php
                                    if (isset($ethnicities[$order->patient->ethnicity_id]) && $order->patient->ethnicity_id != 8) {
                                        echo $ethnicities[$order->patient->ethnicity_id];
                                    } elseif ($order->patient->ethnicity_id == 8) {
                                        echo $order->patient->ethnicity_other;
                                    }
                                    ?>
                                <br>
                                <span>Sex:</span>
                                <?php
                                    if (isset($sexes[$order->patient->sex_id])) {
                                        echo $sexes[$order->patient->sex_id];
                                    }
                                ?>
                                <br>
                                <span>State:</span> <?= $order->patient->lib24watch_state_abbr; ?>
                                <br>
                                <span>Client ID:</span> <?= $order->patient->client_id; ?>
                                <br>
                                <span>Card ID:</span> <?= $order->patient->card_id; ?>
                                <br>
                                <span>Person Code:</span> <?= $order->patient->person_code; ?>
                                <br>
                                <span>Date of Birth:</span> <?= lib24watchDate('shortDateFormat', $order->patient->date_of_birth, +5); ?>
                                <br>
                                <span>Email:</span> <?= $order->patient->email; ?>
                                <br>
                                <span>Phone:</span> <?= $order->patient->phone; ?>
                            </div>
                            <div class="col-md-6">
                                <h3>Shipping Information (if applicable)</h3>
                                <span>Shipping Name:</span> <?= $order->shipping_first_name . ' ' . $order->patient->shipping_last_name; ?>
                                <br>
                                <span>Shipping Address:</span>
                                <br>
                                <?= $order->shipping_address_1; ?>
                                <br>
                                <?= $order->shipping_address_2; ?>
                                <br>
                                <?= $order->shipping_city . ', ' . $order->shipping_lib24watch_state_abbr . ' ' . $order->shipping_postal_code; ?>
                                <br>
                                <span>Sample Kit ID #</span> <?= $order->sample_id; ?>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Provider Information</h3>
                                <?php
                                echo $this->cell(
                                    'Lib24watch.Table::display',
                                    [
                                        '',
                                        'items' => [
                                            [
                                                'name' => 'provider_name',
                                                'label' => 'Name',
                                                'type' => 'text'
                                            ],
                                            [
                                                'name' => 'npi_number',
                                                'label' => 'NPI #',
                                                'type' => 'text'
                                            ],
                                            [
                                                'name' => 'is_primary_care_provider',
                                                'label' => 'Primary Care Provider',
                                                'type' => 'status'
                                            ],
                                            [
                                                'name' => 'is_ordering_physician',
                                                'label' => 'Ordering Physician',
                                                'type' => 'status'
                                            ]
                                        ],
                                        $order->providers
                                    ]
                                );
                                ?>
                            </div>
                            <div class="col-md-12">
                                <h3>Medications</h3>
                                <?php
                                echo $this->cell(
                                    'Lib24watch.Table::display',
                                    [
                                        '',
                                        'items' => [
                                            /*
                                            [
                                                'name' => 'drug.brand_names',
                                                'label' => 'Brand Names',
                                                'type' => 'custom',
                                                'function' => function ($row) {
                                                    $brandNames = [];
                                                    foreach ($row->drug->brand_names as $brand) {
                                                        $brandNames[] = $brand->brand_name;
                                                    }
                                                    $brandNames = array_unique($brandNames);
                                                    return implode(', ', $brandNames);
                                                }
                                            ],
                                             */
                                            [
                                                #'name' => 'drug.specificproductname',
                                                'name' => 'drug.nondose_name',
                                                'label' => 'Name',
                                                'type' => 'text'
                                            ],
                                            [
                                                'name' => 'mapped',
                                                'label' => 'Mapped?',
                                                'type' => 'custom',
                                                'function' => function($row) use ($latestReportEntity) {
                                                    $severity = null;
                                                    $mapped = false;
                                                    if (isset($latestReportEntity->results) && isset($row->medication)) {
                                                        foreach ($latestReportEntity->results as $result) {
                                                            if (isset($result->result_medications) && !empty($result->result_medications)) {
                                                                foreach ($result->result_medications as $med) {
                                                                    if (
                                                                        isset($med['tsi_medication_id']) && 
                                                                        isset($row->medication) &&
                                                                        $med['tsi_medication_id'] == $row->medication->tsi_medication_id
                                                                    ) {
                                                                        $severity = $result['tsi_severity'];
                                                                        $mapped = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    $html = $mapped ? '<strong>Yes</strong>' : '';

                                                    if (!is_null($severity)) {
                                                        $html.= '<br>' . $severity;
                                                    }

                                                    return $html;
                                                }
                                            ],
                                            [
                                                'name' => 'medication.ingredient_title',
                                                'label' => 'TSI Medication',
                                                'type' => 'text'
                                            ],
                                            [
                                                'name' => 'foo',
                                                'label' => 'Alternative Medications',
                                                'class' => 'foo',
                                                'type' => 'custom',
                                                'function' => function ($row) use ($labOrderId) {
                                                    $link = '';
                                                    if (
                                                        isset($row['medication']) &&
                                                        isset($row['medication']['tsi_medication_id'])
                                                    ) {
                                                        $link = '<a class="get-alternatives" href="/admin/system_management/lab_orders/getAlternatives/' . $labOrderId . '/' . $row['medication']['tsi_medication_id'] . '">Get Alternative Medications</a>';
                                                    }

                                                    return $link;
                                                }
                                            ]
                                        ],
                                        $order->medications
                                    ]
                                );
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
                                <script>
                                $('.get-alternatives').on('click', function(e) {
                                    let $el = this;
                                    let $parentEl = $(this).parent();
                                    e.preventDefault();

                                    $.ajax({
                                        dataType: 'json',
                                        url: this.href,
                                        success:function(json) {
                                            window.json = json;
                                            if (json.success && json.data && json.data.alternates && json.data.alternates.length) {
                                                let $ol = $('<ol>');
                                                json.data.alternates.forEach(function(row) {
                                                    let $li = $('<li>');
                                                    let html = '<strong>Medication: ' + row.ingredientTitle + '</strong><br>';
                                                    if (row.drugsList) {
                                                        html+= row.drugsList;

                                                        $li.html(html);

                                                        $li.appendTo($ol);
                                                    }
                                                });

                                                $ol.appendTo($parentEl);
                                            }
                                        }
                                    });
                                });
                                </script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3 class="panel-title">Event Log</h3>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Status',
                                        'type' => 'lookup',
                                        'list' => $statuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => 'Action',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'message',
                                        'label' => 'Message',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'details',
                                        'label' => 'Details',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            if ($row->details != 'null') {
                                                return $row->details;
                                            } else {
                                                return false;
                                            }
                                        }
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'date',
                                        'format' => 'shortDateFormat timeFormat'
                                    ]
                                ],
                                $order->events
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
