<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse API Logs';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->cell('Lib24watch.Table::filter', [
            'API Logs',
            'items' => [
                [
                    'name' => 'ApiAuthentication_username',
                    'type' => 'select',
                    'options' => ['' => ''] + $apiUsers,
                    'label' => 'User',
                    'field' => 'ApiAuthentication.username',
                    'function' => function($queryObj, $value) {
                        return $queryObj->matching('ApiAuthentication', function ($q) use ($value) {
                            return $q->where(['username LIKE' => "%$value%"]);
                        });
                    }
                ],
                [
                    'name' => 'ip',
                    'type' => 'text',
                    'label' => 'IP Address',
                ],
                [
                    'name' => 'apilogs_created',
                    'type' => 'datetimepicker',
                    'label' => 'Created',
                    'function' => function($queryObj, $value) {
                        return $queryObj->where(['date(ApiLogs.created)' => $value]);
                    }
                ],
                [
                    'name' => 'request',
                    'type' => 'text',
                    'label' => 'Request',
                ],
                [
                    'name' => 'message',
                    'type' => 'text',
                    'label' => 'Message',
                ],
            ],
            $query,
            $this
        ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'api_authentication.username',
                                        'label' => 'User',
                                        'type' => 'text',
                                    ],
                                    [
                                        'name' => 'ip',
                                        'label' => 'IP Address',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'date'
                                    ],
                                    [
                                        'name' => 'request',
                                        'label' => 'Request',
                                        'type' => 'text',
                                    ],
                                    [
                                        'name' => 'message',
                                        'label' => 'Message',
                                        'type' => 'text',
                                    ],
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
