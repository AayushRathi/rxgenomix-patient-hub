<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Promo Codes';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Promo Codes',
                'items' => [
                    [
                        'name' => 'health_care_client_id',
                        'type' => 'select',
                        'label' => 'Health Care Client',
                        'empty' => true,
                        'options' => $healthCareClients
                    ],
                    [
                        'name' => 'code',
                        'type' => 'text',
                        'label' => 'Promo Code'
                    ],
                    [
                        'name' => 'created',
                        'type' => 'datetimepicker',
                        'label' => 'Date Created'
                    ],
                    [
                        'name' => 'is_active',
                        'type' => 'checkbox',
                        'label' => 'Active'
                    ]
                ],
                $query,
                $this
            ]);                
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'SystemManagement',
                            'controller' => 'PromoCodes',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Promo Code'
                        ]
                    )
                ]
            );
            ?>            
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'health_care_client_id',
                                        'label' => 'Health Care Client',
                                        'type' => 'lookup',
                                        'list' => $healthCareClients
                                    ],
                                    [
                                        'name' => 'code',
                                        'label' => 'Promo Code',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date Created',
                                        'type' => 'date'
                                    ],
                                    [
                                        'name' => 'description',
                                        'label' => 'Description',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'SystemManagement',
                                                'controller' => 'PromoCodes',
                                                'row' => $row
                                            ]);
                                        }
                                    ]                               
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to 
                            <?php
                            echo $this->Html->link(
                                'Add a Promo Code',
                                [
                                    'plugin' => 'SystemManagement',
                                    'controller' => 'PromoCodes',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Promo Code'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
