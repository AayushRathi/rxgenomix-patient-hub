<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Promo Codes"),
    [
        'plugin' => 'SystemManagement',
        'controller' => 'PromoCodes',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($promoCodeId ? "Edit" : "Add") . " Promo Code";
$this->Breadcrumbs->add($pageHeading);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($promoCodeEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
                            echo $this->Form->control(
                                'code',
                                [
                                    'type' => 'text',
                                    'label' => 'Promo Code',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'description',
                                [
                                    'type' => 'text',
                                    'label' => 'Description',
                                    'required' => false,
                                ]
                            );
                            echo $this->Form->control(
                                'discount_type_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Discount Type',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $discountTypes
                                ]
                            );
                            echo $this->Form->control(
                                'amount',
                                [
                                    'type' => 'text',
                                    'label' => 'Discount Amount',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'health_care_client_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Health Care Client',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $healthCareClients
                                ]
                            );
            				echo $this->Form->control(
            					'is_active',
            					[
            						'type' => 'checkbox',
            						'label' => 'Active'
            					]
            				);
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
