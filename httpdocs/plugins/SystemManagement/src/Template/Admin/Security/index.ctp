<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Security';
$this->Breadcrumbs->add($pageHeading);

echo $this->Html->css('/system_management/css/admin/security.css');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false
                ]
            );
            ?>
            <div class="panel-body">
            <?php
                if (isset($cadenceSet) && $cadenceSet) {
                ?>
                    <div role="alert" class="alert alert-success"><button data-dismiss="alert" class="close" type="button"><span aria-hidden="true">Ã</span><span class="sr-only">Close</span></button>Password cadence was reset.</p>
                    </div>
<?php
                }
                echo $this->Form->create($passwordCadenceEntity);
                echo $this->Form->control('setting_value', [
                    'label' => 'Password Reset Cadence',
                    'type' => 'text',
                    'required' => true,
                ]);
                echo $this->Form->button('Force Password Resets', [
                    'class' => 'btn-force-password-reset btn-danger',
                    'name' => 'force_password_reset',
                    'onclick' => 'return confirmReset();'
                ]);
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
                echo $this->Form->end();
            ?>
            </div>
        </div>
    </div>
</div>

                <script>
                function confirmReset() {
                    return confirm("Are you sure you would like to require all users to reset their password?");
                }
                </script>
