<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Manually Manage Events';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'label' => 'Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'sample_id',
                                        'label' => 'Sample #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'lab',
                                        'label' => 'Lab',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            if (
                                                isset($row->provider) &&
                                                !empty ($row->provider) &&
                                                isset($row->provider->health_care_client) && 
                                                !empty($row->provider->health_care_client) && 
                                                isset($row->provider->health_care_client->lab) && 
                                                !empty($row->provider->health_care_client->lab)
                                            ) {
                                                return $row->provider->health_care_client->lab->lab_name;
                                            } else {
                                                return 'N/A';
                                            }
                                        }
                                    ],
                                    [
                                        'name' => 'patient_id',
                                        'label' => 'Patient Name',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->patient ? $row->patient->first_name . ' ' . $row->patient->last_name : '';
                                        }
                                    ],
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Kit Status',
                                        'type' => 'lookup',
                                        'list' => $statuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) use ($environment) {

                                            $returnHtml = '';

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-files-o"></span> Resubmit to Bioinformatics',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'resubmit_pharmacogenomics',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-primary'
                                                    ]
                                                );
                                            }

                                            $returnHtml .= $this->Html->link(
                                                '<span class="fa fa-flask"></span> Resubmit to Lab',
                                                [
                                                    'plugin' => 'SystemManagement',
                                                    'controller' => 'EventManager',
                                                    'action' => 'resubmit_lab',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-default'
                                                ]
                                            );

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-files-o"></span> Set status to report ready',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'set_report_ready',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );
                                            }

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-files-o"></span> Fill in MAP with missing medications',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'fill_medications',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );
                                            }

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-lock"></span> Reassociate PGX',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'reassociate_pgx',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );
                                            }

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml.= $this->Html->link(
                                                    '<span class="fa fa-flask"></span> Reset Report Ready',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'reset_report_ready',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );
                                            }

                                            if ($row['lab_order_type_id'] === 1) {
                                                if (isset($environment) && $environment === 'dev') {
                                                    $returnHtml.= $this->Html->link(
                                                        '<span class="fa fa-flask"></span> Clear medications (ONLY FOR TESTING)',
                                                        [
                                                            'plugin' => 'SystemManagement',
                                                            'controller' => 'EventManager',
                                                            'action' => 'delete_medications',
                                                            $row->id
                                                        ],[
                                                            'escape' => false,
                                                            'class' => 'btn btn-condensed btn-default'
                                                        ]
                                                    );
                                                }
                                            }

                                            if ($row['lab_order_type_id'] === 1) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-bar-chart-o"></span> Repull Reports',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'repull_reports',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-primary'
                                                    ]
                                                );
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-bug"></span> Resubmit to QC',
                                                    [
                                                        'plugin' => 'SystemManagement',
                                                        'controller' => 'EventManager',
                                                        'action' => 'resubmit_qc',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );
                                                if ($row->provider && $row->provider->health_care_client_id == 1) {
                                                    $returnHtml .= $this->Html->link(
                                                        '<span class="fa fa-cloud-upload"></span> Resubmit to Webservice #1',
                                                        [
                                                            'plugin' => 'SystemManagement',
                                                            'controller' => 'EventManager',
                                                            'action' => 'resubmit_web1',
                                                            $row->id
                                                        ],[
                                                            'escape' => false,
                                                            'class' => 'btn btn-condensed btn-primary'
                                                        ]
                                                    );
                                                }

                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-eye"></span> View',
                                                    [
                                                        'plugin' => 'LabOrders',
                                                        'controller' => 'LabOrders',
                                                        'action' => 'view',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-default'
                                                    ]
                                                );

                                            }
                                            return $returnHtml;
                                        }
                                    ]
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
