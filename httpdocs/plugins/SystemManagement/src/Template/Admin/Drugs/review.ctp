<?php
use SystemManagement\Libs\DrugHelper;
use SystemManagement\Libs\RxNav;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3 class="panel-title">Review Drug</h3>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title"><?=$drugEntity->specificproductname;?></h3>
                                <h6 class="card-subtitle mb-2"><?=$drugEntity->nondose_name;?></h6>
                                <h6 class="text-muted"><?=$pharmacogenomicProvider->pharmacogenomicProviderEntity->host;?></h6>

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Combo</th>
                                            <th>Medication</th>
                                            <th>Rxcui</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?=$drugEntity->is_combo ? 'Yes' : 'No';?></td>
                                            <td>
                                                <?=$drugEntity->tsi_medication->ingredient_title ?? 'N/A';?>
                                                #<?=$drugEntity->medication_id;?>
                                            </td>
                                            <td><?=$drugEntity->rxcui_id ?? 'N/A';?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <h6>drug_rxcui records : <?=count($drugEntity->rxcui);?></h6>
                                <?php if (isset($drugEntity->rxcui) && !empty($drugEntity->rxcui)) {
                                    echo $this->cell(
                                        'Lib24watch.Table::display',
                                        [
                                            '',
                                            'items' => [
                                                [
                                                    'name' => 'id',
                                                    'label' => 'Id',
                                                    'type' => 'text'
                                                ],
                                                [
                                                    'name' => 'rxcui',
                                                    'label' => 'Rxcui',
                                                    'type' => 'text'
                                                ],
                                                [
                                                    'name' => 'rxcui_entity_title',
                                                    'label' => 'Rxcui Entity - Title',
                                                    'type' => 'custom',
                                                    'function' => function($row) {
                                                        if (isset($row->rxcui_entity) && !empty($row->rxcui_entity)) {
                                                            return $row->rxcui_entity->title;
                                                        }
                                                    }
                                                ],
                                                [
                                                    'name' => 'rxcui_entity_synonym',
                                                    'label' => 'Rxcui Entity - Synonym',
                                                    'type' => 'custom',
                                                    'function' => function($row) {
                                                        if (isset($row->rxcui_entity) && !empty($row->rxcui_entity)) {
                                                            return $row->rxcui_entity->synonym;
                                                        }
                                                    }
                                                ],
                                                [
                                                    'name' => 'rxcui_entity_tty',
                                                    'label' => 'Rxcui Entity - Tty',
                                                    'type' => 'custom',
                                                    'function' => function($row) {
                                                        if (isset($row->rxcui_entity) && !empty($row->rxcui_entity)) {
                                                            return $row->rxcui_entity->tty;
                                                        }
                                                    }
                                                ]
                                            ],
                                            $drugEntity->rxcui
                                        ]
                                    );
                                } ?>

                                <h6>ndc rxcui records : <?=count($drugEntity->ndc_rxcuis);?></h6>
                                <?php if (isset($drugEntity->ndc_rxcuis) && !empty($drugEntity->ndc_rxcuis)) {
                                    echo $this->cell(
                                        'Lib24watch.Table::display',
                                        [
                                            '',
                                            'items' => [
                                                [
                                                    'name' => 'id',
                                                    'label' => 'Id',
                                                    'type' => 'text'
                                                ],
                                                [
                                                    'name' => 'rxcui',
                                                    'label' => 'Rxcui',
                                                    'type' => 'text'
                                                ],
                                                [
                                                    'name' => 'title',
                                                    'label' => 'Title',
                                                    'type' => 'text'
                                                ]
                                            ],
                                            $drugEntity->ndc_rxcuis
                                        ]
                                    );
                                }

                                if (empty($drugEntity->ndc_rxcuis) && empty($drugEntity->rxcui)) {
                                    $name = DrugHelper::extractSimpleName($drugEntity->nondose_name);

                                    $rxcuisFound = [];
                                    if ($name) {
                                        $rxcuis = RxNav::getRxcuisByName($name);
                                        if ($rxcuis) {
                                            foreach ($rxcuis as $rxcui) {
                                                $fullInfo = RxNav::getRxcuiFullInfo($rxcui);
                                                $related = RxNav::getRelated($rxcui);

                                                if ($fullInfo) {
                                                    if ($fullInfo['tty'] === 'SCDF') {
                                                        dd($related);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                ?>
                                <p class="card-text">fdsjlfdsjkl</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
