<?php
$pageHeading = "Browse Unmapped Drugs";
$this->assign('title', 'Unmapped Drugs');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => 'Drugs',
                    'showPagination' => false,
                    'extraControls' => false
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if (isset($drugs) && $drugs->isEmpty() == false) {
                        ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Drug Id</th>
                                <th>Combo</th>
                                <th>Non specific name</th>
                                <th>Specific name</th>
                                <th>Mapped TSI Medication</th>
                                <th>Needs Review</th>
                                <th>Review</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($drugs as $drug) {
                                $tsiMedicationName = null;
                                if (isset($drug->_matchingData) && isset($drug->_matchingData['TsiMedication']['ingredient_title']) && mb_strlen($drug->_matchingData['TsiMedication']['ingredient_title']) > 0) {
                                    $tsiMedicationName = $drug->_matchingData['TsiMedication']['ingredient_title'];
                                }

                                $tsiInMedicationName = true;
                                $rowClass = '';
                                $description = '';

                                if ($tsiMedicationName && stripos($tsiMedicationName, $drug->nondose_name) !== false) {
                                    $tsiInMedicationName = false;
                                }

                                if ($tsiInMedicationName === false) {
                                    $rowClass = ' danger ';
                                } else if (count(explode(',', $drug->nondose_name)) > 1) {
                                    $description = ' Has multiple names, may need review';
                                    $rowClass = ' warning';
                                }
                                ?>
                                <tr class="drug-row <?=$rowClass;?>" title="<?php echo $description;?>">
                                    <td><?php echo h($drug->id); ?></td>
                                    <td><?php echo $drug->is_combo ? 'Yes' : 'No';?></td>
                                    <td><?php echo h($drug->specificproductname); ?></td>
                                    <td><strong><?php echo h($drug->nondose_name); ?></strong></td>
                                    <td><strong><?php echo h($tsiMedicationName ?? '');?></strong></td>
                                    <td><?php
                                $checked = '';
                                if (isset($drug->mark_for_review) && (bool)$drug->mark_for_review) {
                                    $checked = 'checked="checked"';
                                }
                                    ?><input <?php echo $checked;?> class="review-checkbox" type="checkbox" data-drug-id="<?=$drug->id;?>"></td>
                                    <td><?php echo $this->Html->link(
                                        'Review',
                                        [
                                            'plugin' => 'SystemManagement',
                                            'prefix' => 'admin',
                                            'controller' => 'Drugs',
                                            'action' => 'review',
                                            $drug->id
                                        ],
                                        [
                                            'title' => 'Review Drug'
                                        ]
                                    );?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        ?>
                        <p>
                            No <q>drug</q> found.
                        </p>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
            <script>
            $('.review-checkbox').click(function() {
                let el = this;
                let drugId = $(this).data('drug-id');

                if (this.checked && drugId > 0) {
                    $.ajax({
                        url: '/admin/system_management/drugs/setreview/' + drugId,
                        method: 'POST',
                        dataType: 'json',
                        success: function(json) {
                            if (json.success) {
                                $(el).closest('td').append($('<p>Marked for review</p>'));
                            }
                        }
                    });
                }
            });
            </script>

