<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Api Users';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Api Users',
                'items' => [
                    [
                        'name' => 'health_care_client_id',
                        'type' => 'select',
                        'label' => 'Health Care Client',
                        'options' => $healthCareClients
                    ],
                    [
                        'name' => 'username',
                        'type' => 'text',
                        'label' => 'Username'
                    ],
                    [
                        'name' => 'is_active',
                        'type' => 'checkbox',
                        'label' => 'Active'
                    ]
                ],
                $query,
                $this
            ]);                
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'SystemManagement',
                            'controller' => 'ApiAuthentications',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add an Api User'
                        ]
                    )
                ]
            );
            ?>            
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'health_care_client_id',
                                        'label' => 'Health Care Client',
                                        'type' => 'lookup',
                                        'list' => $healthCareClients
                                    ],
                                    [
                                        'name' => 'username',
                                        'label' => 'Username',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'SystemManagement',
                                                'controller' => 'ApiAuthentications',
                                                'row' => $row
                                            ]);
                                        }
                                    ]                               
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to 
                            <?php
                            echo $this->Html->link(
                                'Add an Api User',
                                [
                                    'plugin' => 'SystemManagement',
                                    'controller' => 'ApiAuthentications',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add an Api User'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
