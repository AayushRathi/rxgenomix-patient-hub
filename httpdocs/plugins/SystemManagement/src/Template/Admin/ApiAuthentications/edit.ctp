<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse API Users"),
    [
        'plugin' => 'SystemManagement',
        'controller' => 'ApiAuthentications',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($apiAuthenticationId ? "Edit" : "Add") . " Api User";
$this->Breadcrumbs->add($pageHeading);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($apiAuthenticationEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
                            echo $this->Form->control(
                                'health_care_client_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Health Care Client',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $healthCareClients
                                ]
                            );
                            echo $this->Form->control(
                                'username',
                                [
                                    'type' => 'text',
                                    'label' => 'Username',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'password',
                                [
                                    'type' => 'password',
                                    'label' => 'Password',
                                    'value' => ''
                                ]
                            );
                            echo $this->Form->control(
                                'confirm_password',
                                [
                                    'type' => 'password',
                                    'label' => 'Confirm Password'
                                ]
                            );
            				echo $this->Form->control(
            					'is_active',
            					[
            						'type' => 'checkbox',
            						'label' => 'Active'
            					]
            				);
                        ?>
                    </div>
            	</div>
                <div class="row">
                    <div class="col-md-10">
                        <h3>Permissions</h3>
                        <?php
                        $hasPermissions = [];
                        foreach ((array)$apiAuthenticationEntity->permissions as $permission) {
                            $hasPermissions[] = $permission->api_permission_id;
                        }
                        foreach ($apiPermissions as $id => $title) {
            				$hasPermission = false;
                            if (in_array($id, $hasPermissions)) {
                                $hasPermission = true;
                            }
                            echo $this->Form->control(
            					'permissions['.$id.']',
            					[
            						'type' => 'checkbox',
            						'label' => $title,
                                    'checked' => $hasPermission
            					]
            				);
                        }
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
