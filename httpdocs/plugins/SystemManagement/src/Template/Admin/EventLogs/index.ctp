<?php
$pluginTitle = $lib24watchModules['SystemManagement']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Event Logs';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->cell('Lib24watch.Table::filter', [
            'API Logs',
            'items' => [
                [
                    'name' => 'health_care_client_id',
                    'type' => 'select',
                    'label' => 'Health Care Client',
                    'options' => $healthCareClients,
                    'empty' => true
                ]
            ],
            $query,
            $this
        ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'lab_order_id',
                                        'label' => 'Lab Order ID',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'health_care_client_id',
                                        'label' => 'Health Care Client',
                                        'type' => 'lookup',
                                        'list' => $healthCareClients
                                    ],
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Lab Order Status',
                                        'type' => 'lookup',
                                        'list' => $labOrderStatuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => 'Action',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'message',
                                        'label' => 'Message',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'details',
                                        'label' => 'Details',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            if (json_decode($row->details, true) !== false) {
                                                $details = json_decode($row->details, true);
                                            } elseif (is_array($row->details)) {
                                                $details = $arr;
                                            } elseif ($row->details) {
                                                $details = [(string) $row->details];
                                            }
                                            if ($details) {
                                                return implode(PHP_EOL, $details);
                                            }
                                        }
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return lib24watchDate('shortDateFormat timeFormat', $row->created);
                                        }
                                    ]
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
