<a href="javascript:void(0)"><span class="fa fa-gears"></span><span class="xn-text"><?php echo h($lib24watchModules['SystemManagement']['title']); ?></span></a>
<ul>
    <li class="xn-title">Promo Codes</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('promo_codes_view', 'SystemManagement')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Promo Codes",
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'PromoCodes',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Promo Codes'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('promo_codes_edit', 'SystemManagement')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Promo Code",
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'PromoCodes',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Promo Code'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
    <li class="xn-title">API Users</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('systemmanagement_api_users_view', 'SystemManagement')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Api Users",
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'ApiAuthentications',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Api Users'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('systemmanagement_api_users_edit', 'SystemManagement')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Api User",
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'ApiAuthentications',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Api User'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
    <li class="xn-title">API Logs</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('systemmanagement_api_logs_view', 'SystemManagement')) {
        ?>
        <li>
            <?= $this->Html->link('<span class="fa fa-cloud"></span> Browse Api Logs',
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'ApiLogs',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Api Logs'
                ]);
            ?>
        </li>
        <?php
    }
    ?>
    <li class="xn-title">Event Logs</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('systemmanagement_event_logs_view', 'SystemManagement')) {
        ?>
        <li>
            <?= $this->Html->link('<span class="fa fa-tasks"></span> Browse Event Logs',
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'EventLogs',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Event Logs'
                ]);
            ?>
        </li>
        <?php
    }
    ?>
    <?php
    if ($this->Lib24watchPermissions->isImmutableSuperuser()) {
        ?>
        <li class="xn-title">Event Manager</li>
        <li>
            <?= $this->Html->link('<span class="fa fa-fire-extinguisher"></span> Event Manager',
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'EventManager',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Manually Trigger Events'
                ]);
            ?>
        </li>
        <?php
    }
    ?>
    <?php
    if ($this->Lib24watchPermissions->isImmutableSuperuser()) {
        ?>
        <li class="xn-title">Security</li>
        <li>
            <?= $this->Html->link('<span class="fa fa-fire-extinguisher"></span> Security',
                [
                    'plugin' => 'SystemManagement',
                    'controller' => 'Security',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Security'
                ]);
            ?>
        </li>
        <?php
    }
    ?>
</ul>
