<?php
echo $this->Html->link(
    '<span class="fa fa-edit"></span> Edit',
    [
        'plugin' => $plugin,
        'controller' => $controller,
        'action' => 'edit',
        'prefix' => 'admin',
        $row->id
    ],[
        'escape' => false,
        'class' => 'btn btn-condensed btn-primary'
    ]
);
if (!isset($editOnly) || (isset($editOnly) && !$editOnly)) {
    $icon = 'fa-play';
    $text = 'Activate';
    $btn = 'btn-success';
    if ($row->is_active) {
        $icon = 'fa-pause';
        $text = 'Deactivate';
        $btn = 'btn-warning';
    }
    echo $this->Html->link(
        '<span class="fa ' . $icon . '"></span> ' . $text,
        [
            'plugin' => $plugin,
            'controller' => $controller,
            'action' => 'change_status',
            'prefix' => 'admin',
            $row->id
        ],[
            'escape' => false,
            'class' => 'btn btn-condensed ' . $btn
        ]
    );
    if (!isset($row->is_active) || !$row->is_active) {
        echo $this->Html->link(
            '<span class="fa fa-trash-o"></span> Delete',
            [
                'plugin' => $plugin,
                'controller' => $controller,
                'action' => 'delete',
                'prefix' => 'admin',
                $row->id
            ],[
                'escape' => false,
                'onclick' => "if(confirm('Are you sure you want to delete this item?')) { return true;} return false;",
                'class' => 'btn btn-condensed btn-danger'
            ]
        );
    }
}
?>
