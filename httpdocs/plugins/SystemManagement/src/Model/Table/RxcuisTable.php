<?php
namespace SystemManagement\Model\Table;

class RxcuisTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rxcuis');

        $this->hasMany(
            'DrugNdcRxcui', 
            [
                'className' => 'SystemManagement.DrugNdcRxcui',
                'foreignKey' => 'rxcui_id',
                'saveStrategy' => 'replace'
            ]
        );
    }
}
