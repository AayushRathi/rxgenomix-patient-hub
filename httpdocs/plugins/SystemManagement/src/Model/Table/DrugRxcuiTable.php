<?php
namespace SystemManagement\Model\Table;

/**
 * Class DrugRxcuiTable
 * @package SystemManagement\Model\Table
 */
class DrugRxcuiTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('drug_rxcui');

        $this->belongsTo('Drugs', [
            'className' => 'SystemManagement.Drugs',
            'foreignKey' => 'drug_id'
        ]);
    }
}
