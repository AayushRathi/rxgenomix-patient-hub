<?php

namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class MapProblemCategoriesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('map_problem_categories');

        $this->hasMany('MapProblemRationales', [
            'className' => 'SystemManagement.MapProblemRationales',
            'foreignKey' => 'map_problem_category_id'
        ]);
    }

    /**
     * @param bool $isActive
     *
     * @return array
     */
    public function getList(bool $isActive=true)
    {
        return $this->find()
            ->where([
                'MapProblemCategories.is_active' => $isActive
            ])
            ->order(['display_order' => 'asc'])
            ->combine('id', 'title')
            ->toArray();
    }
}
