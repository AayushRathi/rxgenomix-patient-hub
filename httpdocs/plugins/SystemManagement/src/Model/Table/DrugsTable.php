<?php
namespace SystemManagement\Model\Table;

/**
 * Class DrugsTable
 * @package SystemManagement\Model\Table
 */
class DrugsTable extends ListTable
{
    /**
     * @var string
     */
    public $valueField = 'specificproductname';

    /**
     * @var array
     */
    public $where = [];
    public $order = ['specificproductname' => 'ASC'];

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->hasMany(
            'Ndc',
            [
                'className' => 'SystemManagement.DrugNdc',
                'foreignKey' => 'drug_id',
                'saveStrategy' => 'replace'
            ]
        );

        $this->hasMany(
            'Rxcui',
            [
                'className' => 'SystemManagement.DrugRxcui',
                'foreignKey' => 'drug_id',
                'saveStrategy' => 'replace',
                'conditions' => [
                    'Rxcui.rxcui IS NOT' => null
                ]
            ]
        );

        $this->hasMany(
            'BrandNames',
            [
                'className' => 'SystemManagement.DrugBrandNames',
                'foreignKey' => 'drug_id',
                'saveStrategy' => 'replace'
            ]
        );

        $this->belongsTo(
            'TsiMedication',
            [
                'className' => 'SystemManagement.Medications',
                'foreignKey' => 'medication_id'
            ]
        );
    }

    /**
     * @return \Cake\ORM\Query
     */
    public function findAutocompleteWithBrandNames()
    {
        $query = $this->find();
        return $query->matching('BrandNames')
            ->select([
                'Drugs.id',
                'Drugs.nondose_name',
                'Drugs.is_bulk_powder',
                'value' => $query->func()->concat([
                    'Drugs.specificproductname' => 'identifier',
                    ' (',
                    'GROUP_CONCAT(BrandNames.brand_name SEPARATOR ", ")' => 'identifier',
                    ')'
                ]),
                'label' => $query->func()->concat([
                    'Drugs.specificproductname' => 'identifier',
                    ' (',
                    'GROUP_CONCAT(BrandNames.brand_name SEPARATOR ", ")' => 'identifier',
                    ')'
                ])
            ])->group('Drugs.id')
            ->where(['Drugs.is_bulk_powder' => 0])
            ->order(['Drugs.is_combo', 'Drugs.nondose_name', 'Drugs.specificproductname']);
    }

    /**
     * @param array $drugNames
     * @return array|bool
     */
    public function getNdcsByDrugNames(array $drugNames = [])
    {
        if (!empty($drugNames)) {
            $ndcs = [];
            $whereNondose = $whereBrandname = [];
            foreach ($drugNames as $drugName) {
                $whereBrandname[] = ['brand_name LIKE' => '%' . $drugName . '%'];
                $whereNondose[] = ['nondose_name LIKE' => '%' . $drugName . '%'];
            }
            $query = $this->find()
                ->contain(['Ndc'])
                ->where([
                    'OR' => $whereNondose
                ]);
            foreach ($query as $result) {
                foreach ($result->ndc as $ndc) {
                    $ndcId = (int)$ndc->ndc;
                    if ($ndcId) {
                        $ndcs[] = $ndcId;
                    }
                }
            }
            $query = $this->find()
                ->contain(['Ndc'])
                ->matching('BrandNames', function ($q) use ($whereBrandname) {
                    return $q->where([
                        'OR' => $whereBrandname
                    ]);
                });
            foreach ($query as $result) {
                foreach ($result->ndc as $ndc) {
                    $ndcId = (int)$ndc->ndc;
                    if ($ndcId) {
                        $ndcs[] = $ndcId;
                    }
                }
            }
            return array_values(array_filter(array_unique($ndcs)));
        }
        return false;
    }


    /**
     * @param string|null $brandName
     * @return bool
     */
    public function getIdByBrandName(string $brandName = null)
    {
        if ($brandName) {
            $query = $this->find();
            $result = $query->matching(
                'BrandNames', function ($q) use ($brandName) {
                    return $q->where(['brand_name' => $brandName]);
            })->first();
            if (!$result) {
                $result = $query->where(['specificproductname LIKE' => $brandName])->first();
            }
            if ($result) {
                return $result->id;
            }
        }
        return false;
    }
}
