<?php
namespace SystemManagement\Model\Table;

/**
 * Class ApiAuthenticationPermissionsTable
 * @package SystemManagement\Model\Table
 */
class ApiAuthenticationPermissionsTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'Permission',
            [
                'className' => 'SystemManagement.ApiPermissions',
                'foreignKey' => 'api_permission_id'
            ]
        );
    }
}
