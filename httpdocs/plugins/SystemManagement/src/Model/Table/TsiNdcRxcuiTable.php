<?php

namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class TsiNdcRxcuiTable extends Lib24watchTable
{
    public function initialize($config) 
    {
        parent::initialize($config);

        $this->setTable('tsi_ndc_rxcui');

        $this->belongsTo(
            'Rxcui',
            [
                'className' => 'SystemManagement.Rxcui',
                'foreignKey' => 'rxcui_id'
            ]
        );
    }
}
