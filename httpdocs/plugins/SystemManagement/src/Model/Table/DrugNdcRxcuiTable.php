<?php

namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class DrugNdcRxcuiTable extends Lib24watchTable
{
    public function initialize($config) 
    {
        parent::initialize($config);

        $this->belongsTo(
            'DrugNdc',
            [
                'className' => 'SystemManagement.DrugNdc',
                'foreignKey' => 'drug_ndc_id'
            ]
        );

        $this->belongsTo(
            'Rxcui',
            [
                'className' => 'SystemManagement.Rxcui',
                'foreignKey' => 'rxcui_id'
            ]
        );
    }
}
