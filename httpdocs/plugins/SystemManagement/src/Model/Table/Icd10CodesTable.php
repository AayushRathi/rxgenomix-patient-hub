<?php

namespace SystemManagement\Model\Table;

class Icd10CodesTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('icd10_codes');
    }
}
