<?php
namespace SystemManagement\Model\Table;

/**
 * Class TermsOfServicesTable
 * @package SystemManagement\Model\Table
 */
class TermsOfServicesTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->hasMany('Checkboxes', [
            'className' => 'SystemManagement.TermsOfServiceCheckboxes',
            'foreignKey' => 'terms_of_service_id',
            'bindingKey' => 'id'
        ]);
    }
}
