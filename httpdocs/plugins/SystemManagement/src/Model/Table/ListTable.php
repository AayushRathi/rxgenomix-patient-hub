<?php
namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class ListTable
 * @package SystemManagement\Model\Table
 */
class ListTable extends Lib24watchTable
{

    /**
     * @var string
     */
    public $keyField = 'id';
    public $valueField = 'title';

    /**
     * @var array
     */
    public $where = ['is_active' => 1];
    public $order = ['display_order' => 'ASC'];

    /**
     * @return array|\Cake\ORM\Query
     */
    public function findDropdown()
    {
        return $this
            ->find('list', [
                'keyField' => $this->keyField,
                'valueField' => $this->valueField
            ])
            ->where($this->where)
            ->order($this->order)
            ->toArray();
    }
}