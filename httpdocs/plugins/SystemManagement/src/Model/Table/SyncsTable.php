<?php

namespace SystemManagement\Model\Table;

class SyncsTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('syncs');
    }
}
