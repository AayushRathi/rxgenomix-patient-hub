<?php

namespace SystemManagement\Model\Table;

use Cake\Validation\Validator;

/**
 * SecuritySettingsTable
 * @package SystemManagement\Model\Table
 */
class SecuritySettingsTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    /**
     * @param Validator $validator
     *
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->add('setting_value', 'custom', [
            'rule' => function($row, $context) {
                if ($row > 365 || $row < 0) {
                    return false;
                }

                return true;
            },
            'message' => 'Invalid cadence'
        ]);

        return $validator;
    }
}
