<?php
namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class MedicationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize($config) 
    {
        parent::initialize($config);

        $this->hasMany(
            'TsiNdcRxcui',
            [
                'className' => 'SystemManagement.TsiNdcRxcui',
                'foreignKey' => 'medication_id'
            ]
        );

        $this->hasMany(
            'Drugs',
            [
                'className' => 'SystemManagement.Drugs',
                'foreignKey' => 'medication_id'
            ]
        );

        $this->belongsToMany(
            'Alternates',
            [
                'className' => 'SystemManagement.Medications',
                'through' => 'SystemManagement.MedicationAlternates',
                'targetForeignKey' => 'alternate_medication_id',
                'foreignKey' => 'medication_id'
            ]
        );
    }
}
