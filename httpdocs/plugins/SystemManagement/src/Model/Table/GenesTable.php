<?php

namespace SystemManagement\Model\Table;

/**
 * Class GenesTable
 * @package SystemManagement\Model\Table
 */

class GenesTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('genes');
    }
}
