<?php
namespace SystemManagement\Model\Table;

/**
 * Class DrugNdcTable
 * @package SystemManagement\Model\Table
 */
class DrugNdcTable extends ListTable
{

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'Drugs',
            [
                'className' => 'SystemManagement.Drugs',
                'foreignKey' => 'drug_id'
            ]
        );

        $this->hasMany(
            'DrugNdcRxcui', 
            [
                'className' => 'SystemManagement.DrugNdcRxcui',
                'foreignKey' => 'drug_ndc_id',
            ]
        );
    }

}
