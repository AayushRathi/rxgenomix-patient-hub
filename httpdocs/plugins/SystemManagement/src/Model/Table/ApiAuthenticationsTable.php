<?php
namespace SystemManagement\Model\Table;

use Cake\Validation\Validator;

/**
 * Class ApiAuthenticationsTable
 * @package SystemManagement\Model\Table
 */
class ApiAuthenticationsTable extends ListTable
{

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->hasMany(
            'Permissions',
            [
                'className' => 'SystemManagement.ApiAuthenticationPermissions',
                'foreignKey' => 'api_authentication_id',
                'saveStrategy' => 'replace'
            ]
        );

        $this->hasMany(
            'ApiLogs',
            [
                'className' => 'SystemManagement.ApiLogs',
                'foreignKey' => 'api_authentication_id',
            ]
        );
    }


    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('health_care_client_id')
            ->notEmpty('health_care_client_id', 'Health Care Client is required')
            ->requirePresence('username')
            ->notEmpty('username', 'Username is required')
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'Password is required', 'create')
            ->requirePresence('confirm_password', 'create')
            ->notEmpty('confirm_password', 'Password confirmation is required', 'create')
            ;
    }
}
