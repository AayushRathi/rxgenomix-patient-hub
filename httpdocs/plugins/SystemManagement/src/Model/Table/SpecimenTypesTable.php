<?php

namespace SystemManagement\Model\Table;

class SpecimenTypesTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('specimen_types');
    }
}
