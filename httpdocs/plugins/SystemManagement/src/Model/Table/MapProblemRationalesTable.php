<?php

namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class MapProblemRationalesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('map_problem_rationales');

        $this->belongsTo('MapProblemCategories', [
            'className' => 'SystemManagement.MapProblemCategories',
            'foreignKey' => 'map_problem_category_id',
        ]);
    }

    /**
     * @param int $mapProblemCategoryId
     *
     * @return array
     */
    public function getList(int $mapProblemCategoryId = 0)
    {
        return $this->find()
            ->matching('MapProblemCategories', function($q) use ($mapProblemCategoryId) {
                return $q->andWhere([
                    'MapProblemCategories.id' => $mapProblemCategoryId
                ]);
            })
            ->order(['MapProblemRationales.display_order' => 'asc'])
            ->combine('id', 'title')
            ->toArray();
    }
}
