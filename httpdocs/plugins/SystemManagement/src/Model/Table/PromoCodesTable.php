<?php
namespace SystemManagement\Model\Table;

use Cake\Validation\Validator;

/**
 * Class PromoCodesTable
 * @package SystemManagement\Model\Table
 */
class PromoCodesTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('code')
            ->notEmpty('code', 'Promo Code is required')
            ->regex('code', '/^[a-zA-z0-9?]*$/', 'Promo Code must be alpha numeric only.')
            ->requirePresence('discount_type_id')
            ->notEmpty('discount_type_id', 'Discount type is required')
            ->requirePresence('amount')
            ->notEmpty('amount', 'Amount is required')
            ->numeric('amount', 'Discount Amount must be a number.')
            ->add('amount', 'money', ['rule' => 'money', 'message' => 'Discount Amount must be a positive number.'])
            ->range('amount', [0, 9999999999.99], 'Amount must be between 0 and 9,999,999,999.99')
            ->requirePresence('health_care_client_id')
            ->notEmpty('health_care_client_id', 'Health Care Client is required')
            ;
    }
}
