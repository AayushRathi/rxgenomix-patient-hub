<?php
namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class RxcuiMapTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize($config) 
    {
        parent::initialize($config);

        $this->belongsTo(
            'ParentRxcui',
            [
                'className' => 'SystemManagement.Rxcuis',
                'targetForeignKey' => 'id',
                'foreignKey' => 'parent_rxcui_id'
            ]
        );

        $this->belongsTo(
            'ChildRxcui',
            [
                'className' => 'SystemManagement.Rxcuis',
                'targetForeignKey' => 'id',
                'foreignKey' => 'child_rxcui_id'
            ]
        );
    }
}
