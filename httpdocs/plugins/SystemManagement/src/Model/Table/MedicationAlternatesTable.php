<?php
namespace SystemManagement\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

class MedicationAlternatesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize($config)
    {
        parent::initialize($config);

        $this->belongsTo('ParentMedication', [
            'className' => 'Medications.Medications',
            'foreignKey' => 'medication_id'
        ]);

        $this->belongsTo('AlternateMedication', [
            'className' => 'Medications.Medications',
            'foreignKey' => 'alternate_medication_id'
        ]);
    }
}
