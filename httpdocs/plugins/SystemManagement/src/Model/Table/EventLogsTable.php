<?php
namespace SystemManagement\Model\Table;

/**
 * Class EventLogsTable
 * @package SystemManagement\Model\Table
 */
class EventLogsTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');
    }
}
