<?php
namespace SystemManagement\Model\Table;

/**
 * Class ApiLogsTable
 * @package SystemManagement\Model\Table
 */
class ApiLogsTable extends ListTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'ApiAuthentication',
            [
                'className' => 'SystemManagement.ApiAuthentications',
                'foreignKey' => 'api_authentication_id'
            ]
        );

        $this->addBehavior('Lib24watch.Author');
    }
}
