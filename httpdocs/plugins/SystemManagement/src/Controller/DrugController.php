<?php
namespace SystemManagement\Controller;

use Cake\Core\Configure;
/**
 * Class DrugController
 * @package SystemManagement\Controller
 */
class DrugController extends AppController
{

    /**
     * @throws \HttpRequestException
     */
    public function autocompleteDrugs()
    {
        if (!$this->request->is('ajax')) {
            throw new \HttpRequestException('Invalid request');
        }
        $list = [];

        $this->loadModel('SystemManagement.Drugs');

        // auto-fill checking
        $drugId = $this->request->getQuery('id');
        if ($drugId) {
            $drug = $this->Drugs->get($drugId, [
                'contain' => [
                    'BrandNames'
                ]
            ]);
            $brandNames = [];
            foreach ($drug->brand_names as $brandName) {
                $brandNames[] = $brandName->brand_name;
            }
            $list[] = [
                'id' => $drug->id,
                'value' => $drug->specificproductname . implode($brandNames),
                'label' => $drug->specificproductname . implode($brandNames),
            ];
        }

        if (!is_null($this->request->getQuery('q', null))) {
            $drugsQuery = $this->Drugs->find('autocompleteWithBrandNames');
            $drugsQuery->andWhere([
                # Removed from refs #40534
                #'Drugs.is_searchable' => 1,
                #'Drugs.is_processed' => 1,
                #'Drugs.id <' => 9000 // MEDER REMOVE
            ]);

            $searchQuery = mb_strtolower(trim($this->request->getQuery('q', null)));
            $searchValues = [$searchQuery];
            # Temporary experimental per #40534
            # $searchValues = explode(' ', $searchQuery);
            foreach ($searchValues as $searchTerm) {
                $drugsQuery->andWhere(function ($exp) use ($searchTerm) {
                    return $exp->or_([
                        'Drugs.specificproductname LIKE' =>
                            '%' . str_replace('%', '%%', trim($searchTerm)) . '%',
                        'BrandNames.brand_name LIKE' =>
                            '%' . str_replace('%', '%%', trim($searchTerm)) . '%',
                    ]);
                });
            }
            $list = $drugsQuery->limit(20)->toArray();
        }

        // disable debugging
        Configure::write('debug', 0);
        \header('Content-type: application/json');
        $this->viewBuilder()->setLayout('ajax');

        echo json_encode($list);
        exit;
    }
}
