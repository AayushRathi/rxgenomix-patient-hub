<?php
namespace SystemManagement\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'SystemManagement', 'System Management');
    }

    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'promo_codes_view' => 'View Promo Codes',
            'promo_codes_edit' => 'Edit Promo Codes',
            'promo_codes_delete' => 'Delete Promo Codes',
            'systemmanagement_api_users_view' => 'View Api Users',
            'systemmanagement_event_logs_view' => 'View Event Logs',
            'systemmanagement_api_logs_view' => 'View API Logs',
            'systemmanagement_api_users_edit' => 'Edit Api Users',
            'systemmanagement_api_users_delete' => 'Delete Api Users'
        ];
    }
}
