<?php

namespace SystemManagement\Controller\Admin;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use SystemManagement\Controller\AppController;

class SecurityController extends AppController
{
    public $helpers = ['Paginator'];

    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $this->requirePluginPermission('systemmanagement_security', 'SystemManagement', 'You do not have permission to access Security Settings');

        $securitySettingsTable = TableRegistry::getTableLocator()->get('SystemManagement.SecuritySettings');
        $passwordCadenceEntity = $securitySettingsTable
            ->find()
            ->where([
                'setting_key' => 'password_cadence'
            ])
            ->firstOrFail();

        $cadenceSet = false;
        if ($this->getRequest()->getData()) {
            $isReset = $this->getRequest()->getData('force_password_reset') === '';

            $passwordCadenceEntity = $securitySettingsTable->patchEntity(
                $passwordCadenceEntity,
                $this->getRequest()->getData(),
                [
                    'validation' => 'default'
                ]
            );

            if (empty($passwordCadenceEntity->getErrors()) && $securitySettingsTable->save($passwordCadenceEntity)) {
                $cadenceSet = true;
            }

            if ($isReset) {
                $forcePasswordResetSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('enableForcePasswordReset', 'LabOrders', false);

                if ($forcePasswordResetSetting === false ) {
                    return $this->redirectWithDefault([
                        'plugin' => 'SystemManagement',
                        'controller' => 'Security',
                        'action' => 'index',
                        'prefix' => 'admin',
                    ], 'enableForcePasswordReset setting must be defined and set to true to force password resets', 'warning');
                } else {
                    $users = $this->resetSiteUsers($passwordCadenceEntity);

                    return $this->redirectWithDefault([
                        'plugin' => 'SystemManagement',
                        'controller' => 'Security',
                        'action' => 'index',
                        'prefix' => 'admin',
                    ], 'Passwords Reset', 'success');
                }
            }
        }

        $this->set(compact('cadenceSet', 'passwordCadenceEntity'));
    }

    /**
     * Reset midend and backend passwords
     *
     */
    protected function resetSiteUsers(Entity $passwordCadenceEntity)
    {
        $usersTable = TableRegistry::getTableLocator()->get('RxgUsers.RxgUsers');

        $securitySettingsTable = TableRegistry::getTableLocator()->get('SystemManagement.SecuritySettings');
        $passwordCadenceDays = $securitySettingsTable
            ->find()
            ->where([
                'setting_key' => 'password_cadence'
            ])
            ->extract('setting_value')
            ->first();

        if (is_null($passwordCadenceDays) || $passwordCadenceDays < 0 || $passwordCadenceDays > 365) {
            throw new \Exception('Invalid password cadence');
        }

        $today = new \Cake\I18n\Date;
        $today->modify('+' . $passwordCadenceDays . ' days');

        // midend reset
        $users = $usersTable->updateAll([
            'force_password_reset' => 1,
            'password_expires' => $today
        ], [
            'id IS NOT' => null
        ]);

        $lib24watchUsersTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchUsers');
        $lib24watchSettingsTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchSettings');

        $lib24watchUsers = $lib24watchUsersTable->updateAll([
            'force_password_reset' => 1,
        ], [
            'is_immutable' => 0
        ]);

        $lib24watchSettingsTable::writeSettingStatic(
            'daysPasswordExpires',
            $passwordCadenceEntity->setting_value > 0 ? $passwordCadenceEntity->setting_value : 0,
            'Lib24watch'
        );

        $lib24watchSettingsTable::writeSettingStatic(
            'enforcePasswordsToExpire',
            true,
            'Lib24watch'
        );

        $lib24watchSettingsTable::writeSettingStatic(
            'enforcePasswordStrength',
            true,
            'Lib24watch'
        );

        return true;
    }
}
