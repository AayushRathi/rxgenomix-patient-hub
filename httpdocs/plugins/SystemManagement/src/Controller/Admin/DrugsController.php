<?php
namespace SystemManagement\Controller\Admin;

use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Controller\AppController;

class DrugsController extends AppController
{
    public $helpers = ['Paginator'];

    public $paginate = [
        'Drugs' => [
            'limit' => 50,
            'maxLimit' => 50, // override the limit
            'order' => [
                'Drugs.id' => 'ASC'
            ]
        ]
    ];

    public function initialize()
    {
        $this->loadModel('SystemManagement.Drugs');

        parent::initialize();
    }

    public function index()
    {
        $isImmutable = (bool)$this->currentLib24watchUser['is_immutable'];

        if (!$isImmutable) {
            throw new \Exception('No access');
        }
        
        $query = $this->Drugs->find();
        $recordCount = $query->select([
            'Drugs.id',
            'count' => $query->func()->count('Drugs.id')
        ])
        ->group('Drugs.id')
        ->where([
            'AND' => [
                'Drugs.medication_id IS' => null,
                'Drugs.rxcui_id IS' => null
            ]
        ])
        ->matching('Rxcui')
        ->having(['count >=' => 1])
        ->count();

        $drugQuery = $this->Drugs->find();

        $drugEntities = $drugQuery
            ->select([
                'Drugs.id',
                'Drugs.mark_for_review',
                'Drugs.is_combo',
                'Drugs.specificproductname',
                'Drugs.nondose_name',
                'TsiMedication.id',
                'TsiMedication.ingredient_title',

                #'drug_rxcui_count' => $drugQuery->func()->count('Rxcui.id')
            ])
            ->group('Drugs.id')
            ->contain(
                [
                    'TsiMedication'
                ]
            )
            #->leftJoinWith('Rxcui')
            ->where([
                'AND' => [
                    'Drugs.medication_id IS' => null,
                    'Drugs.rxcui_id IS' => null
                ]
                #'Drugs.mark_for_review' => 1,
            /*
            'OR' => [
                'Drugs.is_manually_set_medication' => 1,
                //'Drugs.is_combo' => 1
            ]*/
            ]);
        //->having(['count >=' => 1])

        $this->set('drugs', $this->paginate($drugEntities));
    }

    public function review(int $drugId = 0)
    {
        $drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');
        $rxcuisTable = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis');

        $tsiId = 2;
        $pharmacogenomicProvider = new PharmacogenomicProvider($tsiId);

        $drugEntity = $drugsTable
            ->find()
            ->where([
                'Drugs.id' => $drugId
            ])
            ->contain([
                'Ndc.DrugNdcRxcui',
                'Rxcui',
                'TsiMedication'
            ])
            ->formatResults(function($results) use ($rxcuisTable) {
                return $results->map(function($row) use ($rxcuisTable) {

                    if (isset($row['rxcui'])) {
                        foreach ($row['rxcui'] as &$rxcui) {
                            $rxcuiEntity = $rxcuisTable->find()
                                ->where([
                                    'Rxcuis.rxcui' => $rxcui->rxcui
                                ])
                                ->first();

                            if ($rxcuiEntity) {
                                $rxcui['rxcui_entity'] = $rxcuiEntity;
                            }
                        }
                    }

                    $rxcuiIds = [];
                    if (isset($row['ndc']) && !empty($row['ndc'])) {
                        foreach ($row['ndc'] as $ndc) {
                            if (isset($ndc['drug_ndc_rxcui']) && !empty($ndc['drug_ndc_rxcui'])) {
                                foreach ($ndc['drug_ndc_rxcui'] as $ndcRxcuiEntity) {
                                    $rxcuiIds[] = $ndcRxcuiEntity->rxcui_id;
                                }
                            }
                        }
                    }

                    $rxcuiIds = array_unique($rxcuiIds);

                    $ndcRxcuis = [];

                    if (!empty($rxcuiIds)) {
                        $ndcRxcuis = $rxcuisTable->find()
                            ->where([
                                'Rxcuis.id IN' => $rxcuiIds
                            ])
                            ->toArray();
                    }

                    $row['ndc_rxcuis'] = $ndcRxcuis;

                    return $row;
                });
            })
            ->firstOrFail();

        #dd($drugEntity);

        $this->set(compact('drugEntity', 'pharmacogenomicProvider'));
    }

    public function setreview(int $drugId = 0)
    {
        $drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');
        $drugEntity = $drugsTable
            ->find()
            ->where([
                'Drugs.id' => $drugId
            ])
            ->first();

        $result = [
            'success' => false
        ];

        if (!is_null($drugEntity)) {
            $drugEntity = $drugsTable->patchEntity($drugEntity, [
                'mark_for_review' => 1
            ]);

            if ($drugsTable->save($drugEntity)) {
                $result['success'] = true;
            }
        }

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($result));
    }
}
