<?php
namespace SystemManagement\Controller\Admin;

use Cake\Core\Configure;
use Cake\Http\Client;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Controller\AppController;

class EventManagerController extends AppController
{
    use MailerAwareTrait;

    public function initialize()
    {
        $this->loadModel('LabOrders.LabOrders');
        parent::initialize();
    }

    public function index()
    {
        $this->loadModel('LabOrders.LabOrderStatuses');

        $query = $this->LabOrders->find()->contain([
            'WorkflowStatus',
            'Patient',
            'Providers',
            'Provider.Users',
            'Provider.HealthCareClient.Lab',
            'Medications.Drug.BrandNames'
        ])->where([
            'is_deleted' => 0
        ])->order(['LabOrders.created' => 'DESC']);

        $statuses = $this->LabOrderStatuses->find('dropdown');

        $this->set('environment', $this->getEnvironment());

        $this->set(compact('query'));
        $this->set(compact('statuses'));
    }

    public function delete_medications(int $labOrderId = null)
    {
        if ($this->getEnvironment() !== 'dev') {
            throw new \Exception('Dev only');
        }

        //Load labOrder
        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Reports',
                'Medications.Drug.BrandNames'
            ]
        ]);

        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }

        $labOrderMedicationsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications')
            ->deleteAll([
                'lab_order_id' => $labOrderId
            ]);

        \SystemManagement\Libs\EventLogs::write(
            $order->provider->health_care_client_id,
            $order->id,
            $order->lab_order_status_id,
            'Deleted medications',
            'Success'
        );

        return $this->redirectWithDefault([
            'plugin' => 'SystemManagement',
            'controller' => 'EventManager',
            'action' => 'index',
            'prefix' => 'admin'
        ], 'Lab Order medications have been cleared for lab order #' . $labOrderId);
    }

    public function fill_medications(int $labOrderId = null)
    {
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }

        //Load labOrder
        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Reports',
                'Medications.Drug.BrandNames'
            ]
        ]);

        $labOrderReportId = $order->reports[0]->id;

        if (!$labOrderReportId) {
            throw new \Exception('Report not found');
        }

        $labOrdersInstance = new LabOrders;
        $labOrdersInstance->generateLabOrderNoteEntitiesForResults($labOrderReportId);

        /*
        if ($order->lab_order_status_id < 7) {
            throw new \Exception('Lab order status is not report ready');
        }*/

        \SystemManagement\Libs\EventLogs::write(
            $order->provider->health_care_client_id,
            $order->id,
            $order->lab_order_status_id,
            'Filled in MAP with missing medications',
            'Success'
        );

        return $this->redirectWithDefault([
            'plugin' => 'SystemManagement',
            'controller' => 'EventManager',
            'action' => 'index',
            'prefix' => 'admin'
        ], 'Lab Order has been filled in with missing medications');
    }

    public function set_report_ready(int $labOrderId = null)
    {
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }

        //Load labOrder
        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames'
            ]
        ]);

        if ($order->lab_order_status_id == 7) {
            throw new \Exception('Lab order status is already set to report ready');
        }

        if ($order->lab_order_status_id < 4) {
            throw new \Exception('Lab order must be submitted to processor');
        }

        $order = $this->LabOrders->patchEntity(
            $order,
            [
                'lab_order_status_id' => 7,
            ]
        );

        if (!$this->LabOrders->save($order)) {
            throw new \Exception('Unable to set report ready');
        }

        \SystemManagement\Libs\EventLogs::write(
            $order->provider->health_care_client_id,
            $order->id,
            $order->lab_order_status_id,
            'QC Passed (Manually)',
            'Success'
        );

        if ($this->getEnvironment() === 'production') {
            $email = $order->provider->email;
            if ($order->provider->health_care_client_id == 1) {
                $email = 'clinical.services@benecard.com';
            }
            // Send report ready email
            $mailer = $this->getMailer('Providers.Providers');
            $mailer->send('reportReady', [
                $email,
                $labOrder
            ]);
        }

        return $this->redirectWithDefault([
            'plugin' => 'SystemManagement',
            'controller' => 'EventManager',
            'action' => 'index',
            'prefix' => 'admin'
        ], 'Lab Order report status set to report ready');
    }

    /**
     * @param int $labOrderId
     */
    public function reset_report_ready(int $labOrderId = null)
    {
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }

        //Load labOrder
        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames'
            ]
        ]);

        $order = $this->LabOrders->patchEntity(
            $order,
            [
                'enable_email_followup' => 0,
                'last_sent_sequence' => 0,
                'last_sent_sequence_date' => null,
                'enable_email_followup_start_date' => null,
                'first_seen_provider_id' => null
            ]
        );

        if (!$this->LabOrders->save($order)) {
            throw new \Exception('Unable to reset report ready');
        }

        return $this->redirectWithDefault([
            'plugin' => 'SystemManagement',
            'controller' => 'EventManager',
            'action' => 'index',
            'prefix' => 'admin'
        ], 'Lab Order report ready reset successfully.');
    }

    /**
     * @param int|null $labOrderId
     * @throws \Exception
     */
    public function resubmit_lab(int $labOrderId = null)
    {
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }

        //Load labOrder
        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient.Sex',
                'Patient.Ethnicity',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames'
            ]
        ]);

        // Actually submit to lab here
        $healthCareClient = $order->provider->health_care_client;
        $lab = new Lab($healthCareClient->lab_id);

        // HL7 spits out warnings, silence them!
        Configure::write('debug', 0);

        $payload = $lab->getRequestDriver()->createLabOrder($order);
        $connectionInformation = $healthCareClient->lab->toArray();

        $labOrderFileName = ROOT . '/data/lab_orders/' . $order->id . '.hl7';
        $connectionInformation['local_file'] = $labOrderFileName;
        $connectionInformation['remote_file'] = $connectionInformation['submit_path'] . '/' . $order->id . '.hl7';

        Configure::write('debug', 1);
        $result = $lab->getConnector()->createConnection($connectionInformation)->post($connectionInformation, $payload);

        if ($result) {
            $order->lab_order_status_id = 3;
            $this->LabOrders->save($order);

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $order->id,
                $order->lab_order_status_id,
                'Submit to Lab',
                'Success'
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'Lab Order resubmitted to Lab successfully.');

        } else {
            // submit to lab failed, log it and let the user know
            $order->lab_order_status_id = 9;
            $this->LabOrders->save($order);

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $order->id,
                $order->lab_order_status_id,
                'Submit to Lab',
                'Submit to Lab Failed',
                $result
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'There was an error submitting, verify Lab credentials.',
                'danger');
        }
    }

    /**
     *
     * Resubmit pharmacogenomics
     *
     * @param int $labOrderId
     */
    public function resubmit_pharmacogenomics(int $labOrderId = null)
    {
        if ($labOrderId === 0) {
            throw new \Exception('Invalid lab order id');
        }

        $labOrders = new LabOrders;
        $labOrder = $this->LabOrders->find()->where([
            'LabOrders.id' => $labOrderId
        ])->contain([
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames'
        ])->firstOrFail();

        $environment = 'dev';

        if (
            (isset($isProduction) && $isProduction) ||
            getenv('RXG_ENV') == 'production'
        ) {
            $environment = 'production';
        }

        if ($labOrder) {
            TableRegistry::getTableLocator()->get('Queue.QueuedJobs')->createJob(
                'LabOrderReprocess',
                [
                    'labOrderId' => $labOrder->id,
                    'email' => $this->currentLib24watchUser->email,
                    'environment' => $environment
                ]
            );

            $healthCareClient = $labOrder->provider->health_care_client;
            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Triggered manual reprocess',
                'Processing'
            );

            $message = 'Lab Order has been queued to reprocess. You will be emailed once its done.';
            $status = 'Success';
        } else {
            $message = 'Could not add lab order to reprocessing queue';
            $status = 'Error';
        }

        return $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ],
            $message,
            $status
        );
    }

    /**
     * @param int|null $labOrderId
     * @throws \Exception
     */
    public function reassociate_pgx(int $labOrderId = null)
    {
        $labOrder = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider'
            ]
        ]);

        $healthCareClient = $labOrder->provider->health_care_client;

        $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);
        $target = '25f0a720-5fc3-4b6c-9848-8283ea0281b3';
        $data = [
            'pgxid' => $labOrder->uuid,
            'target' => $target
        ];

        $payload = $pharmacogenomicProvider->getDriver()->createPostProcess($data);

        $connectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();
        $connectionInformation['submit_path'] = '/report/postprocess';
        $connectionInformation['headers'] = [];
        $connection = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($connectionInformation);
        $result = $connection->post($connectionInformation, $payload);

        $response = $connection->getResponse();
        if ($response->isOk()) {

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'PGX associated in CLS successfully',
                'Success'
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'Success associating PGX in CLS'
            );
        } else {
            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'There was an error associating pgx: ' . json_encode((array)$result),
                'danger');
        }
    }

    /**
     *
     * Repull reports
     *
     * @param int $labOrderId
     */
    public function repull_reports(int $labOrderId = 0)
    {
        if ($labOrderId === 0) {
            throw new \Exception('Invalid lab order id');
        }

        $labOrders = new LabOrders;
        $labOrder = $this->LabOrders->find()->where([
            'LabOrders.id' => $labOrderId
        ])->firstOrFail();

        $environment = 'dev';

        if (isset($isProduction) && $isProduction) {
            $environment = 'production';
        }

        if ($labOrder) {
            TableRegistry::getTableLocator()->get('Queue.QueuedJobs')->createJob(
                'ReportRepull',
                [
                    'labOrderId' => $labOrder->id,
                    'email' => $this->currentLib24watchUser->email,
                    'environment' => $environment
                ]
            );

            $message = 'Lab Order has been queued to reprocess. You will be emailed once its done.';
            $status = 'Success';
        } else {
            $message = 'Could not add lab order to reprocessing queue';
            $status = 'Error';
        }

        return $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ],
            $message,
            $status
        );
    }

    /**
     * @param int|null $labOrderId
     * @throws \Exception
     */
    public function resubmit_qc(int $labOrderId = null)
    {
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }
        $labOrder = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider'
            ]
        ]);

        $healthCareClient = $labOrder->provider->health_care_client;
        $lab = new Lab($healthCareClient->lab_id);

        $this->loadModel('LabOrders.LabOrderReports');

        // get the latest
        $labOrderReport = $this->LabOrderReports->find()
            ->where(['lab_order_id' => $labOrderId])
            ->order(['id' => 'DESC'])->first();
        $labOrderReportId = $labOrderReport->id;

        $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
        $connectionInformation = $healthCareClient->lab->toArray();
        $connectionInformation['local_file'] = $labOrderReportPdfFileName;
        $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
        $report = file_get_contents($labOrderReportPdfFileName);
        $result = $lab->getConnector()
            ->createConnection($connectionInformation)
            ->post($connectionInformation, $report);
        if ($result) {
            $labOrder->lab_order_status_id = 5;
            $this->LabOrders->save($labOrder);

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Post Report Back to QC',
                'Success'
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'Lab Order resubmitted to QC successfully.');
        } else {
            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Post Report Back to QC',
                'Fail',
                $result
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'There was an error submitting lab order to QC',
                'danger');
        }
    }

    /**
     * @param int|null $labOrderId
     * @throws \Exception
     */
    public function resubmit_web1(int $labOrderId = null)
    {
        error_reporting(0);
        if (!$labOrderId) {
            throw new \Exception('Lab order id required');
        }
        $labOrder = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'Patient'
            ]
        ]);

        $scope['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
        $authClient = new Client($scope);

        $response = $authClient->post(Configure::read('Benecard.api.endpoint') . '/oauth2/endpoint/OAuthProvider/token',
            http_build_query([
                'grant_type' => 'client_credentials',
                'client_id' => Configure::read('Benecard.api.username'),
                'client_secret' => Configure::read('Benecard.api.password')
            ]));
        $result = $response->getJson();
        $token = $result['access_token'];
        $this->format = 'getJson';
        $this->type = 'json';
        $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);

        $scope['headers']['Content-Type'] = 'application/json';
        $client = new Client($scope);
        $data = [
            'clientId' => $labOrder->patient->client_id,
            'cardId' => $labOrder->patient->card_id,
            'personCode' => sprintf('%02d', $labOrder->patient->person_code),
            'dob' => lib24watchDate('%Y-%m-%d', $labOrder->patient->date_of_birth, +5),
            'testDate' => lib24watchDate('%Y-%m-%d', $labOrder->collection_date, +5),
            'reportUrl' => 'https://' . $this->request->host() . '/lab_orders/lab_orders/download/' . $labOrder->id,
        ];

        $response = $client->post(Configure::read('Benecard.api.endpoint') . '/RxGenomixEndpoint/rest/member', json_encode($data));
        if (!$response->isOk()) {
            \SystemManagement\Libs\EventLogs::write(
                1,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Posting to Benecard Webservice #1 Failed',
                'Failure',
                (array)$response->getJson()
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'There was an error submitting to Benecard web 1: ' . $response->getStringBody(),
                'danger');
        } else {
            \SystemManagement\Libs\EventLogs::write(
                1,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Posted to Benecard Webservice #1',
                'Success'
            );

            $this->redirectWithDefault([
                'plugin' => 'SystemManagement',
                'controller' => 'EventManager',
                'action' => 'index',
                'prefix' => 'admin'
            ], 'Lab Order resubmitted to Benecard Web 1 successfully.');
        }
    }

    private function getEnvironment()
    {
        $environment = 'dev';
        if (
            (isset($isProduction) && $isProduction) ||
            getenv('RXG_ENV') == 'production'
        ) {
            $environment = 'production';
        }

        return $environment;
    }
}
