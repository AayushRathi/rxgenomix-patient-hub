<?php
namespace SystemManagement\Controller\Admin;

use Cake\Http\Client;
use SystemManagement\Controller\AppController;
use Cake\Utility\Inflector;

class ApiAuthenticationsController extends AppController
{

    public function initialize()
    {
        $this->loadModel('SystemManagement.ApiAuthentications');
        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1,
            'has_contract' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());
        parent::initialize();
    }

    public function index()
    {
        $this->requirePluginPermission('systemmanagement_api_users_view', 'SystemManagement',
            'You do not have permission to edit Api Users');

        $query = $this->ApiAuthentications->find();
        $this->set('query', $query);
    }

    public function edit(int $apiAuthenticationId = null)
    {
        $this->requirePluginPermission('systemmanagement_api_users_edit', 'SystemManagement',
            'You do not have permission to edit Api Users');

        $this->set('apiAuthenticationId', $apiAuthenticationId);

        if ($apiAuthenticationId) {
            $apiAuthenticationEntity = $this->ApiAuthentications->get($apiAuthenticationId, [
                'contain' => [
                    'Permissions'
                ]
            ]);
        } else {
            $apiAuthenticationEntity = $this->ApiAuthentications->newEntity([], [
                'contain' => [
                    'Permissions'
                ]
            ]);
        }
        $this->set('apiAuthenticationEntity', $apiAuthenticationEntity);

        $this->loadModel('SystemManagement.ApiPermissions');
        $apiPermissions = $this->ApiPermissions->find('list', [
            'keyField' => 'id',
            'valueField' => function ($row) {
                return $row['plugin'] . ': ' . Inflector::humanize($row['permission']);
            }
        ])->toArray();
        $this->set('apiPermissions', $apiPermissions);

        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();
            $permissions = [];
            foreach ($apiPermissions as $id => $title) {
                if (in_array($id, array_keys($data['permissions']))) {
                    $permissions[] = [
                        'api_permission_id' => $id
                    ];
                }
            }
            $data['permissions'] = $permissions;

            if (isset($data['password']) && isset($data['confirm_password'])) {
                if ($data['password'] != $data['confirm_password']) {
                    $apiAuthenticationEntity->setError('password', 'Passwords must match');
                } else {
                    $this->loadModel('Lib24watch.Lib24watchUsers');
                    $data['password'] = $this->Lib24watchUsers->lib24watchPasswordHash($data['password']);
                }
            }

            $apiAuthenticationEntity = $this->ApiAuthentications->patchEntity($apiAuthenticationEntity, $data, [
                'contain' => [
                    'Permissions'
                ]
            ]);

            $errors = $apiAuthenticationEntity->getErrors();
            if (empty($errors)) {
                if (!$this->ApiAuthentications->save($apiAuthenticationEntity)) {
                    throw new \Exception('Could not save API Authentication');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'SystemManagement',
                            'controller' => 'ApiAuthentications',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'API Authentication ' . ($apiAuthenticationId) ? 'edited.' : 'added.'
                    );
                }
            }
        }
    }

    /**
     * @param int|null $apiAuthenticationId
     * @throws \Exception
     */
    public function change_status(int $apiAuthenticationId = null)
    {
        $this->requirePluginPermission('systemmanagement_api_users_edit', 'SystemManagement',
            'You do not have permission to edit Api Users');

        $apiAuthenticationEntity = $this->ApiAuthentications->get($apiAuthenticationId);


        if ($apiAuthenticationEntity->is_active) {
            $apiAuthenticationEntity->is_active = 0;
            $text = 'active';
        } else {
            $apiAuthenticationEntity->is_active = 1;
            $text = 'inactive';
        }

        if (!$this->ApiAuthentications->save($apiAuthenticationEntity)) {
            throw new \Exception("Couldn't update active status for Api User with ID ({$apiAuthenticationId})");
        }

        $this->Lib24watchLogger->write("Changed status of Api User '" . $apiAuthenticationEntity->username . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'ApiAuthentications',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Api User '" . $apiAuthenticationEntity->username . "' is now " . $text
        );
    }

    /**
     * @param int|null $apiAuthenticationId
     * @throws \Exception
     */
    public function delete(int $apiAuthenticationId = null)
    {
        $this->requirePluginPermission('systemmanagement_api_users_delete', 'SystemManagement',
            'You do not have permission to delete Api Users');

        $apiAuthenticationEntity = $this->ApiAuthentications->get($apiAuthenticationId);

        if (!$this->ApiAuthentications->delete($apiAuthenticationEntity)) {
            throw new \Exception("Couldn't update active status for Api User with ID ({$apiAuthenticationId})");
        }

        $this->Lib24watchLogger->write("Deleted Api User '" . $apiAuthenticationEntity->username . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'ApiAuthentications',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Api User '" . $apiAuthenticationEntity->username . "' deleted."
        );
    }

    public function webserviceOne(int $labOrderId = null)
    {
        if ($labOrderId) {
            $this->loadModel('LabOrders.LabOrders');

            $labOrder = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'Patient'
                ]
            ]);
            if ($labOrder->lab_order_status_id != 7) {
                throw new \Exception('Lab Order did not pass QC, exiting.');
            }
            $scope['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
            $authClient = new Client($scope);

            $response = $authClient->post('https://www.benecardpbf.com:8595/oauth2/endpoint/OAuthProvider/token',
                http_build_query([
                    'grant_type' => 'client_credentials',
                    'client_id' => 'rxGenomix',
                    'client_secret' => 'secret'
                ]));
            $result = $response->getJson();
            $token = $result['access_token'];
            $this->format = 'getJson';
            $this->type = 'json';
            $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);

            $scope['headers']['Content-Type'] = 'application/json';
            $client = new Client($scope);
            $data = [
                'clientId' => $labOrder->patient->client_id,
                'cardId' => $labOrder->patient->card_id,
                'personCode' => $labOrder->patient->person_code,
                'dob' => lib24watchDate('%Y-%m-%d', $labOrder->patient->date_of_birth, +5),
                'testDate' => lib24watchDate('%Y-%m-%d', $labOrder->collection_date, +5),
                'reportUrl' => $this->request->host() . '/lab_orders/lab_orders/download/' . $labOrder->id,
            ];

            $response = $client->post('https://www.benecardpbf.com:8595/RxGenomixEndpoint/rest/member', json_encode($data));
            debug(json_encode($data));
            dd($response->getJson());
        }
    }

}
