<?php
namespace SystemManagement\Controller\Admin;

use SystemManagement\Controller\AppController;

/**
 * Class PromoCodesController
 * @package SystemManagement\Controller\Admin
 */
class PromoCodesController extends AppController
{

    /**
     * init
     */
    public function initialize()
    {
        $this->loadModel('SystemManagement.PromoCodes');
        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1,
            'has_contract' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());

        parent::initialize();
    }

    /**
     * index
     */
    public function index()
    {
        $query = $this->PromoCodes->find();
        $this->set('query', $query);
    }

    /**
     * @param int|null $promoCodeId
     * @throws \Exception
     */
    public function edit(int $promoCodeId = null)
    {
        $this->set('promoCodeId', $promoCodeId);

        if ($promoCodeId) {
            $promoCodeEntity = $this->PromoCodes->get($promoCodeId);
        } else {
            $promoCodeEntity = $this->PromoCodes->newEntity();
        }
        $this->set('promoCodeEntity', $promoCodeEntity);

        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            if (!isset($data['is_active'])) {
                $data['is_active'] = 0;
            }

            $promoCodeEntity = $this->PromoCodes->patchEntity($promoCodeEntity, $data);

            $errors = $promoCodeEntity->getErrors();
            if (empty($errors)) {
                if (!$this->PromoCodes->save($promoCodeEntity)) {
                    throw new \Exception('Could not save Promo Code');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'SystemManagement',
                            'controller' => 'PromoCodes',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Promo Code ' . ($promoCodeId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $this->loadModel('SystemManagement.DiscountTypes');
        $this->set('discountTypes', $this->DiscountTypes->find('dropdown'));
    }

    /**
     * @param int|null $promoCodeId
     * @throws \Exception
     */
    public function change_status(int $promoCodeId = null)
    {
        $this->requirePluginPermission('promo_codes_edit', 'SystemManagement',
            'You do not have permission to edit promo codes');

        $promoCodeEntity = $this->PromoCodes->get($promoCodeId);


        if ($promoCodeEntity->is_active) {
            $promoCodeEntity->is_active = 0;
            $text = 'active';
        } else {
            $promoCodeEntity->is_active = 1;
            $text = 'inactive';
        }

        if (!$this->PromoCodes->save($promoCodeEntity)) {
            throw new \Exception("Couldn't update active status for Promo Code with ID ({$promoCodeId})");
        }

        $this->Lib24watchLogger->write("Changed status of Promo Code '" . $promoCodeEntity->code . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'PromoCodes',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Promo Code '" . $promoCodeEntity->code . "' is now " . $text
        );
    }

    /**
     * @param int|null $promoCodeId
     * @throws \Exception
     */
    public function delete(int $promoCodeId = null)
    {
        $this->requirePluginPermission('promo_codes_delete', 'SystemManagement',
            'You do not have permission to delete promo codes');

        $promoCodeEntity = $this->PromoCodes->get($promoCodeId);

        if (!$this->PromoCodes->delete($promoCodeEntity)) {
            throw new \Exception("Couldn't update active status for Promo Code with ID ({$promoCodeId})");
        }

        $this->Lib24watchLogger->write("Deleted Promo Code '" . $promoCodeEntity->code . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'PromoCodes',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Promo Code '" . $promoCodeEntity->code . "' deleted."
        );
    }
}