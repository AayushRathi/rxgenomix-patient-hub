<?php
namespace SystemManagement\Controller\Admin;

use SystemManagement\Controller\AppController;

class EventLogsController extends AppController
{

    public function initialize()
    {
        $this->loadModel('SystemManagement.EventLogs');
        parent::initialize();
    }

    public function index()
    {
        $this->requirePluginPermission('systemmanagement_event_logs_view', 'SystemManagement',
            'You do not have permission to view Event Logs');

        $query = $this->EventLogs->find()->order('EventLogs.created DESC');
        $this->set('query', $query);

        $this->loadModel('HealthCareClients.HealthCareClients');

        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1,
            'has_contract' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());

        $this->loadModel('LabOrders.LabOrderStatuses');

        $this->set('labOrderStatuses', $this->LabOrderStatuses->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->toArray());
    }
}