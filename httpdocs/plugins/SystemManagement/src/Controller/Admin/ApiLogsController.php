<?php
namespace SystemManagement\Controller\Admin;

use SystemManagement\Controller\AppController;

class ApiLogsController extends AppController
{

    public function initialize()
    {
        $this->loadModel('SystemManagement.ApiLogs');
        parent::initialize();
    }

    public function index()
    {
        $this->requirePluginPermission('systemmanagement_api_users_view', 'SystemManagement',
            'You do not have permission to view Api Logs');

        $query = $this->ApiLogs->find()->contain('ApiAuthentication')->order('ApiLogs.created DESC');

        $this->loadModel('SystemManagement.ApiAuthentications');

        $apiUsers = $this->ApiAuthentications->find('list', [
            'keyField' => 'username',
            'valueField' => 'username'
        ])->where([
            'is_active' => 1
        ])->order([
            'username' => 'ASC'
        ])->toArray();

        $this->set('apiUsers', $apiUsers);
        $this->set('query', $query);
    }

    public function test()
    {
        $this->loadModel('SystemManagement.Drugs');

        dd($this->Drugs->find('withBrandNames')->toArray());
    }
}
