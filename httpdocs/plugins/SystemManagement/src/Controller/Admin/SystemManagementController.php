<?php
namespace SystemManagement\Controller\Admin;

use LabOrders\Libs\LabOrders;
use SystemManagement\Controller\AppController;

class SystemManagementController extends AppController
{
    public function test()
    {
        $labOrder = new LabOrders();
        $labOrder->checkForLabResult();
    }
}
