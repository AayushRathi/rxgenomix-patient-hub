<?php
namespace SystemManagement\Controller\Api;

use Cake\Event\Event;
use Cake\View\ViewBuilder;
use RestApi\Controller\ApiController as RestApiController;
use Cake\Core\Configure;

/**
 * Class ApiController
 * @package SystemManagement\Controller\Api
 */
class ApiController extends RestApiController
{

    /**
     * init
     */
    public function initialize()
    {
        parent::initialize();
        unset($this->responseStatus);
        Configure::write('ApiRequest.responseType', 'json');
    }


    /**
     * Before render callback.
     *
     * @param Event $event The beforeRender event.
     * @return \Cake\Network\Response|null
     */
    public function beforeRender(Event $event)
    {
        $this->responseStatus = true;
        parent::beforeRender($event);

        $response = false;

        $this->response = $this->response->withStatus($this->httpStatusCode);
        $this->response->getStatusCode($this->httpStatusCode);

        if (!empty($this->apiResponse)) {
            $response = $this->apiResponse;
        }
        if ($response) {
            $this->set('response', $response);
            $this->set('responseFormat', $this->responseFormat);
        }
        $this->viewBuilder()->setClassName('SystemManagement.Api');
        return null;
    }

    public function index()
    {
        $this->return404();
    }

    /**
     * @param string $plugin
     * @param string $permission
     * @return bool
     */
    protected function checkPermission(string $plugin, string $permission)
    {
        $apiUserPermissions = json_decode($this->jwtPayload->permissions);
        if (!in_array($plugin.':'.$permission, $apiUserPermissions)) {
            $this->httpStatusCode = 403;
            $this->apiResponse['successMessage'] = 'NOK';
            $this->apiResponse['success'] = false;
            $this->apiResponse['response'] = 'You do not have permission to view that resource.';
            return false;
        }
        return true;
    }

    /**
     *
     */
    protected function return404()
    {
        $this->set('noBody', true);
        $this->httpStatusCode = 404;
    }

}
