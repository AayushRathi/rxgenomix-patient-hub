<?php
namespace SystemManagement\Controller\Api;

use RestApi\Utility\JwtToken;
use Lib24watch\Model\Table\Lib24watchSettingsTable;
use SystemManagement\Libs\ApiLogs;

/**
 * Class ApiAuthenticationController
 * @package SystemManagement\Controller\Api
 */
class ApiAuthenticationController extends ApiController
{

    /**
     * @throws \Exception
     */
    public function login()
    {
        $this->request->allowMethod('post');

        $this->loadModel('SystemManagement.ApiAuthentications');
        $this->loadModel('Lib24watch.Lib24watchUsers');

        $data = $this->getRequest()->getData();
        if (!isset($data['grant_type']) || (isset($data['grant_type']) && $data['grant_type'] != 'Bearer')) {
            $this->httpStatusCode = 400;
            $this->apiResponse['success'] = false;
            $this->apiResponse['successMessage'] = 'Malformed request; grant_type must be of type Bearer';

            ApiLogs::write(1, $this->getRequest()->clientIp(), 'login', 'Invalid credentials');
        } else {
            $apiAuthenticationEntity = $this->ApiAuthentications->find()->where([
                'username' => $data['username'],
                'is_active' => 1
            ])
                ->contain(['Permissions.Permission'])
                ->first();

            if (!isset($data['password']) ||
                !$apiAuthenticationEntity || (
                    isset($data['password']) &&
                    $apiAuthenticationEntity &&
                    !password_verify($data['password'], $apiAuthenticationEntity->password)
                )
            ) {
                // fail with forbidden
                $this->httpStatusCode = 403;
                $this->apiResponse['success'] = false;
                $this->apiResponse['successMessage'] = 'Invalid credentials';
                if ($apiAuthenticationEntity) {
                    ApiLogs::write($apiAuthenticationEntity->id, $this->getRequest()->clientIp(), 'login', 'Invalid credentials');
                } else {
                    ApiLogs::write(1, $this->getRequest()->clientIp(), 'login', 'Invalid credentials');
                }
            } else {
                $permissions = [];
                foreach ($apiAuthenticationEntity->permissions as $permission) {
                    $permissions[] = $permission->permission->plugin .
                        ':' . $permission->permission->permission;
                }
                $expiration = Lib24watchSettingsTable::readSettingStatic('api_timeout_seconds', 'SystemManagement', 14400);
                $payload = [
                    'exp' => (time() + $expiration),
                    'api_authentication_id' => $apiAuthenticationEntity->id,
                    'health_care_client_id' => $apiAuthenticationEntity->health_care_client_id,
                    'username' => $data['username'],
                    'permissions' => json_encode($permissions)
                ];

                $token = JwtToken::generateToken($payload);
                $this->apiResponse['access_token'] = $token;
                $this->apiResponse['token_type'] = 'Bearer';
                $this->apiResponse['expires_in'] = $expiration;
                $this->apiResponse['message'] = 'Logged in successfully';
                ApiLogs::write($apiAuthenticationEntity->id, $this->getRequest()->clientIp(), 'login', $data['username'] . ' Logged in to API successfully');
            }
        }
    }
}
