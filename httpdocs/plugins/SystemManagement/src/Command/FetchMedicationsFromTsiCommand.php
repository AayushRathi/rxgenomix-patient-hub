<?php
namespace SystemManagement\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\Translational;
use SystemManagement\Libs\SyncManager;

/**
 * FetchMedicationsFromTsi command.
 */
class FetchMedicationsFromTsiCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $tsiId = 2;
        $pharmaTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');
        $pharmaEntity = $pharmaTable->find()
            ->select([
                'username',
                'password',
                'host', 
                'scheme',
                'port',
                'request_path',
                'medication_path',
                'submit_path',
                'authentication_path',
                'authentication_type_id'
            ])
            ->where([
                'id' => $tsiId
            ])
            ->firstOrFail();

        $connectionInformation = $pharmaEntity->toArray();
        $connectionInformation['timeout'] = 30;

        $originalRequestPath = $connectionInformation['request_path'];

        $pharmacogenomicProvider = new PharmacogenomicProvider($tsiId);

        $connection = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($connectionInformation);

        // ?isPgxRelevant=true does *not* properly filter medications so we have to remove it
        // there are 729 pages currently, 10 per page, so 72900ish records total which would take ~2 hours to run
        $connectionInformation['headers'] = [
            'Accept' => 'application/json'
        ];

        $type = 'fetch_medications_from_tsi';

        $syncManager = SyncManager::getInstance();
        $start = $syncManager->start($type);

        Translational::processMedications($connection, $connectionInformation, $syncManager);
    }
}
