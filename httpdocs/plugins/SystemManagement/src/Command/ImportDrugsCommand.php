<?php
namespace SystemManagement\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

/**
 * Class ImportDrugsCommand
 * @package SystemManagement\Command
 */
class ImportDrugsCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {

        $start = microtime(true);

        $csv = 'https://genedose-public.s3.amazonaws.com/share/drugs/drugs-latest.csv';

        $handle = fopen($csv, 'r');
        // Need to skip a number of lines on the private CSV files (and 1 line on the public)
        for ($i=0; $i < 1; $i++) {
            $void = fgetcsv($handle, 0, ',');
        }

        $drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');

        while (($data = fgetcsv($handle, 0, ',')) !== false) {
            $entity = $drugsTable->find()
                ->where(['id' => $data[0]])
                ->contain([
                    'Ndc',
                    'Rxcui',
                    'BrandNames'
                ])->first();
            if (!$entity) {
                $entity = $drugsTable->newEntity([], [
                    'contain' => [
                        'Ndc',
                        'Rxcui',
                        'BrandNames'
                    ]
                ]);
                $entity->id = $data[0];
            }
            $entity->specificproductname = $data[1];
            $entity->nondose_name = $data[5];

            $isBulkPowder = false;
            if (preg_match('~bulk\spowder~i', $data[1])) {
                $isBulkPowder = true;
            }

            $entity->is_combo = $this->determineCombo($data[1], $io);
            $entity->is_bulk_powder = $isBulkPowder;

            $hasMany = [];

            $ndcs = str_replace(['{', '}', '-'], '', $data[2]);
            $ndcs = explode(',', $ndcs);

            $ndcsClean = [];
            foreach ($ndcs as $ndc) {
                if (trim($ndc) != '') {
                    $ndcsClean[] = [
                        'drug_id' => $data[0],
                        'ndc' => $ndc
                    ];
                }
            }

            $hasMany['ndc'] = $ndcsClean;

            $rxcuis = str_replace(['{', '}'], '', $data[3]);
            $rxcuis = explode(',', $rxcuis);

            $rxcuisClean = [];
            foreach ($rxcuis as $rxcui) {
                if (trim($rxcui) != '') {
                    $rxcuisClean[] = [
                        'drug_id' => $data[0],
                        'rxcui' => $rxcui
                    ];
                }
            }

            $hasMany['rxcui'] = $rxcuisClean;

            $brands = str_replace(['{', '}','"', '\\'], '', $data[4]);
            $brands = explode(',', $brands);

            $brandsClean = [];
            foreach ($brands as $brand) {
                if (trim($brand) != '') {
                    $brandsClean[] = [
                        'drug_id' => $data[0],
                        'brand_name' => $brand
                    ];
                }
            }

            $hasMany['brand_names'] = $brandsClean;

            $drugsTable->patchEntity($entity, $hasMany);

            if (!$drugsTable->save($entity)) {
                $io->out('Could not save drug: ' . $data[1]);
            }
        }

        $end = microtime(true);
        $time = $end - $start;
        $io->success('Import Completed. Took ' . round($time, 4) . 's');
    }


    protected function determineCombo(string $specificProductName, $io)
    {
        $specificProductName = str_replace(
            [
                ', extended release',
                ', suspension',
                ', solution',
                ', gastro-resistant',
                ', biphasic release',
                ', liquid filled',
                ', liquid',
                ', modified release',
                ', cleanser',
                ', calibrated',
                ', sprinkles'
            ], '', strtolower($specificProductName)
        );

        if (
            strpos($specificProductName, ',') !== false &&
            !preg_match('/[0-9]\,[0-9]/', $specificProductName)
        ) {
            return true;
        }
        return false;
    }
}
