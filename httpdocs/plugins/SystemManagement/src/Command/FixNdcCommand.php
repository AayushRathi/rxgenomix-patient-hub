<?php
namespace SystemManagement\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

/**
 * Class FixNdcCommand
 * @package SystemManagement\Command
 */
class FixNdcCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $drugNdcTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');
        $consolidatedGeneticReportsTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.ConsolidatedGeneticReports');

        $reports = $consolidatedGeneticReportsTable->find()->contain(['Drugs']);

        foreach ($reports as $report) {
            $newDrugs = [];
            if (!empty($report->drugs)) {
                foreach ($report->drugs as $drug) {
                    if ($drug->drug_id) {
                        $drugNdcs = $drugNdcTable->find()->where(['drug_id' => $drug->drug_id]);
                        foreach ($drugNdcs as $drugNdc) {
                            $newDrugs[] = [
                                'consolidated_genetic_report_id' => $drug->consolidated_genetic_report_id,
                                'drug_id' => $drugNdc->ndc,
                                'severity' => $drug->severity,
                                'message' => $drug->message,
                                'activity' => $drug->activity
                            ];
                        }
                    }
                }
            }
            $data = [
                'drugs' => $newDrugs
            ];
            $consolidatedGeneticReportsTable->patchEntity($report, $data);
            if (!$consolidatedGeneticReportsTable->save($report)) {
                $io->out('Could not save consolidated report: ' . $report->id);
            }
        }

    }

}
