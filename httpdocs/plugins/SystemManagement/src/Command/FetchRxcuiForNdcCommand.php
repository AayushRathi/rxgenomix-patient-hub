<?php
namespace SystemManagement\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use SystemManagement\Command\ImportTranslationalIdCommand;
use SystemManagement\Libs\RxcuiImporter;
use SystemManagement\Libs\RxNav;


/**
 * FetchRxcuiForNdc command.
 */
class FetchRxcuiForNdcCommand extends Command
{

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $start = microtime(true);

        $rxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis');
        $existingRxcuis = $rxcuiTable->find()
            ->select([
                'id'
            ])
            ->extract('id')
            ->toArray();

        // pull existing ndc/rxcuis that are up to date to not include
        $drugNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdcRxcui');

        $existingRecords = $drugNdcRxcuiTable->find()
            ->select([
                'drug_ndc_id'
            ])
            ->extract('drug_ndc_id')
            ->toArray();

        $whereConditions = [];

        // Exclude existing records
        if (!empty($existingRecords)) {
            $whereConditions['DrugNdc.id NOT IN'] = $existingRecords;
        }

        // Import specific drugs
        /*
        $drugIds = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications')
            ->find()
            ->select([
                'drug_id'
            ])
            ->where([
                #'lab_order_id' => 316
                'lab_order_id IS NOT' => null
            ])
            ->extract('drug_id')
            ->toArray();

        if (!empty($drugIds)) {
            $whereConditions['drug_id IN'] = $drugIds;
        }
        */

        $importer = new RxcuiImporter($io);
        $importer->process($whereConditions);

        $end = microtime(true);
        $time = $end - $start;
        $io->success('Import for RXCUIs Completed. Took ' . round($time, 4) . 's');
    }
}
