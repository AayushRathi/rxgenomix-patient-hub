<?php
namespace SystemManagement\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use SystemManagement\Libs\DrugHelper;
use SystemManagement\Libs\DrugMapper;
use SystemManagement\Libs\RxNav;
use SystemManagement\Libs\Translational;

/**
 * UPDATE drugs SET is_searchable = 0, medication_id = null, rxcui_id = null WHERE is_combo = 1;
 * UpdateDrugs command.
 */
class UpdateDrugsCommand extends Command
{
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $drugInstance = DrugMapper::getInstance();

        $drugInstance->setConsoleIo($io);

        // We cant rely on drugs.is_combo to be accurate because the pattern can be complicated
        //$drugInstance->fixComboDrugs();

        $drugInstance->run();
    }
}
