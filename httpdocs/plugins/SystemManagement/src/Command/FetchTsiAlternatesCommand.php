<?php
namespace SystemManagement\Command;

use Cake\Collection\Collection;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\SyncManager;
use SystemManagement\Libs\Translational;

/**
 * FetchTsiAlternates command.
 */
final class FetchTsiAlternatesCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $medicationsTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
        $alternatesTable = TableRegistry::getTableLocator()->get('SystemManagement.MedicationAlternates');

        $tsiId = 2;
        $pharmaTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');
        $pharmaEntity = $pharmaTable->find()
            ->select([
                'username',
                'password',
                'host', 
                'scheme',
                'port',
                'request_path',
                'medication_path',
                'submit_path',
                'authentication_path',
                'authentication_type_id'
            ])
            ->where([
                'id' => $tsiId
            ])
            ->firstOrFail();

        $connectionInformation = $pharmaEntity->toArray();
        $connectionInformation['timeout'] = 120;

        $originalRequestPath = $connectionInformation['request_path'];

        $pharmacogenomicProvider = new PharmacogenomicProvider($tsiId);

        $connection = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($connectionInformation);

        // ?isPgxRelevant=true does *not* properly filter medications so we have to remove it
        // there are 729 pages currently, 10 per page, so 72900ish records total which would take ~2 hours to run
        $connectionInformation['headers'] = [
            'Accept' => 'application/json'
        ];

        $nowDate = \Cake\I18n\FrozenDate::now();

        $medications = $medicationsTable
            ->find()
            ->select([
                'id',
                'tsi_medication_id',
                'alternate_sync_date'
            ])
            ->order([
                'id' => 'ASC'
            ])
            ->where([
                'tsi_medication_id IS NOT' => null,
                'OR' => [
                    [
                        'alternate_sync_date IS NOT' => null,
                        'DATE(alternate_sync_date) !=' => $nowDate->format('Y-m-d')
                    ],
                    'alternate_sync_date IS' => null
                ]
            ]);

        $type = 'fetch_tsi_alternates';
        $syncManager = SyncManager::getInstance();
        $syncManager->start($type);

        foreach ($medications as $medicationEntity) {
            $io->out('Processing medication # ' . $medicationEntity->id);
            $connectionInformation['request_path'] = 'api/Medication/' . $medicationEntity->tsi_medication_id . '/Alternatives';
            $rawResponse = $connection->get($connectionInformation);
            $response = json_decode($rawResponse, true);
            $medicationData = [
                'alternate_sync_date' => \Cake\I18n\FrozenTime::now(),
            ];

            debug($response);

            $syncEntity = $syncManager->getSync();
            $syncData = [];

            if (is_null($response)) {
                $syncData['metadata']['errors'][] = 'Unable to decode JSON from URL ' . $connectionInformation['request_path'];

                $syncManager->saveSync($syncEntity, $syncData);

                $this->saveMedication($medicationEntity, $medicationData);

                throw new \Exception('Unable to decode JSON');
            }

            $validResponse = Translational::validateBundleByType('collection', $response);

            if (!$validResponse) {
                $syncData['metadata']['errors'][] = 'Unable to validate response from URL ' . $connectionInformation['request_path'];

                $syncManager->saveSync($syncEntity, $syncData);

                $this->saveMedication($medicationEntity, $medicationData);

                continue;
            }

            if ($response['total'] < 1) {
                continue;
            }

            $alternateMedications = (new Collection($response['entry']));

            if ($alternateMedications->count() > 0) {
                $alternateIds = [];
                foreach ($alternateMedications as $alternate) {
                    $validMedication = Translational::validateMedicationNode($alternate);

                    if (!$validMedication) {
                        throw new \Exception('Invalid medication');
                    }

                    $alternateId = $alternate['resource']['id'];

                    $alternateIds[] = $alternateId;
                }
            }

            # TODO: to test is_active=0
            # unset($alternateIds[count($alternateIds)-1]);

            if (count($alternateIds) > 0) {
                $alternateMedicationEntities = $medicationsTable
                    ->find()
                    ->select([
                        'tsi_medication_id',
                        'id'
                    ])
                    ->where([
                        'tsi_medication_id IN' => $alternateIds
                    ])
                    ->toArray();

                $foundTsiMedicationIds = (new Collection($alternateMedicationEntities))->extract('tsi_medication_id')->toArray();
                $foundMedicationIds = (new Collection($alternateMedicationEntities))->extract('id')->toArray();
                $missingTsiIds = array_diff($alternateIds, $foundTsiMedicationIds);

                if (count($missingTsiIds) > 0) {
                    throw new \Exception('Missing TSI medications - run fetchMedicationsFromTsi to re-sync');
                }

                $io->out('Setting alternates for medication #' . $medicationEntity->id);

                $medicationData['alternates'] = [
                    '_ids' => $foundMedicationIds
                ];

                $this->saveMedication($medicationEntity, $medicationData);

                // de-activate to keep old associations
                $update = $alternatesTable->updateAll(
                    [
                        'is_active' => false
                    ],
                    [
                        'alternate_medication_id NOT IN' => $foundMedicationIds,
                        'medication_id' => $medicationEntity->id
                    ]
                );
            }
        }
    }

    /**
     * Save the medication
     *
     * @param Entity $medicationEntity
     * @param array $medicationData
     *
     * @return void
     */
    private function saveMedication(Entity $medicationEntity, $medicationData)
    {
        $medicationsTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
        $associated = [];
        if (
            isset($medicationData['alternates']) &&
            !empty($medicationData['alternates'])
        ) {
            $associated = [
                'Alternates'
            ];
        }

        $medicationEntity = $medicationsTable->patchEntity(
            $medicationEntity,
            $medicationData,
            $associated
        );

        if (!$medicationsTable->save($medicationEntity, [
            $associated
        ])) {
            throw new \Exception('Could not save alternates');
        }
    }
}
