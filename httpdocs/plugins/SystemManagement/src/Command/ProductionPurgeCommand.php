<?php
namespace SystemManagement\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class ProductionPurgeCommand
 * @package SystemManagement\Command
 */
class ProductionPurgeCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        // patients
        $tables = [
            'Patients.Patients',
            'Patients.PatientMedications',
            'Patients.PatientSnps',
            'Patients.PatientGeneCnvs',
            'LabOrders.LabOrders',
            'LabOrders.LabOrderReports',
            'LabOrders.LabOrderReportResults',
            'LabOrders.LabOrderReportThrombosisProfiles',
            'LabOrders.LabOrderReportResultGenes',
            'LabOrders.LabOrderReportResultGeneRsids',
            'LabOrders.LabOrderReportResultCategories',
            'LabOrders.LabOrderProviders',
            'LabOrders.LabOrderMedications',
            'PharmacogenomicProviders.ConsolidatedGeneticReports',
            'PharmacogenomicProviders.ConsolidatedGeneticReportDrugs'
        ];

        foreach ($tables as $table) {
            $io->out('Purging ' . $table);
            $tableInstance = TableRegistry::getTableLocator()->get($table);
            $numDeleted = $tableInstance->deleteAll([]);
            $io->out($numDeleted . ' were deleted.');
        }
        $io->out('Complete');
        exit;
    }

}
