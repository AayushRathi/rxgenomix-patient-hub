<?php
namespace SystemManagement\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Patients\Libs\LifestylesApi;

/**
 * Class ImportLifestylesCommand
 * @package SystemManagement\Command
 */
class ImportLifestylesCommand extends Command
{

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $start = microtime(true);
        $lifestylesApi = new LifestylesApi();

        $results = $lifestylesApi->getList();

        $lifestylesTable = TableRegistry::getTableLocator()->get('SystemManagement.Lifestyles');

        foreach ($results as $result) {
            $entity = $lifestylesTable->get($result->id);

            if (!$entity) {
                $entity = $lifestylesTable->newEntity();
                $entity->id = $result->id;
            }
            $entity->title = $result->name;

            if (!$lifestylesTable->save($entity)) {
                $io->out('Could not save lifestyle entry');
            }
        }

        $end = microtime(true);
        $time = $end - $start;
        $io->success('Import Completed. Took ' . round($time, 4) . 's');
    }

}