<?php
namespace SystemManagement\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;

/**
 * FindPgxRelevantDrugs command.
 */
class FindPgxRelevantDrugsCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $drugNdcTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');

        $drugIds = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications')
            ->find()
	    ->select('drug_id')
	    ->distinct()
            ->where([
                'drug_id is not' => null
            ])
	    ->order(['drug_id' => 'asc'])
            ->extract('drug_id')
            ->toArray();

	/*
        $drugIds = TableRegistry::getTableLocator()->get('SystemManagement.Drugs')
            ->find()
            ->where([
                'id is not' => null
            ])
            ->order(['id' => 'asc'])
            ->extract('id')
            ->toArray();
	*/

        $drugReport = new File('data/drug_report.txt');
        if (!$drugReport->writable()) {
            throw new \Exception('File is not writable');
        }

        $string = '';
        $columns = [
	    'drug_id',
	    'total_ndc_count',
	    'total_ndc_found_in_nih',
	    'total_rxcui_count_nih',
	    'found_rxcui_from_nih',
	    'total_rxcui_coriell',
	    'found_rxcui_coriell',
	];

        $string.= implode(',', $columns);
        $string.= "\n";

        foreach ($drugIds as $drugId) {
            $row = [];

	    $row[] = $drugId;

            $ndcCount = $drugNdcTable
                ->find()
                ->where([
                    'drug_id' => $drugId
                ])
                ->count();

            $row[] = $ndcCount;

            $matchingNdcCount = $drugNdcTable
                ->find()
                ->matching('DrugNdcRxcui')
                ->where([
                    'drug_id' => $drugId
                ])
                ->count();

            $row[] = $matchingNdcCount;

            $rxcuiIds = $drugNdcTable
                ->find()
                ->matching('DrugNdcRxcui')
                ->where([
                    'drug_id' => $drugId
                ])
		->extract(function ($drugNdc) {
		    return $drugNdc->_matchingData['DrugNdcRxcui']->rxcui_id;
		})
                ->toArray();

	    // remove duplicate values
	    $rxcuiIds = array_unique(array_filter($rxcuiIds));

	    // reset keys 
	    $rxcuiIds = array_values(array_unique(array_filter($rxcuiIds)));

	    $row[] = count($rxcuiIds);

	    $rxcuisFoundByNdc = '';
	    $rxcuisFound = 0;

	    if (count($rxcuiIds) > 0) {
		$rxcuis = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis')
		    ->find()
		    ->select([
			'Rxcuis.rxcui'
		    ])
		    ->where([
			'Rxcuis.id IN' => $rxcuiIds
		    ])
		    ->extract('rxcui')
		    ->toArray();

		    $rxcuisFoundByNdc = '[' . implode(',', $rxcuis) . ']';
		    $rxcuisFound = count($rxcuis);
	    }

	    $row[] = $rxcuisFound;
	    $row[] = $rxcuisFoundByNdc;

	    // coriell rxcuis
	    $coriellRxcuis = TableRegistry::getTableLocator()->get('SystemManagement.DrugRxcui')
		->find()
		->distinct()
		->select([
		    'DrugRxcui.rxcui'
		])
		->where([
		    'DrugRxcui.drug_id' => $drugId
		])
		->extract('rxcui')
		->toArray();

	    $coriellRxcuisCount = count($coriellRxcuis);
	    $coriellRxcuisFound = '[' . implode(',', $coriellRxcuis) . ']';

	    $row[] = $coriellRxcuisCount;
	    $row[] = $coriellRxcuisFound;

	    $string.= implode(',', $row);
	    $string.= "\n";
        }

        if (!$drugReport->write($string)) {
            throw new \Exception('Could not write');
        }
        exit;

            /*
            ->extract(function ($drugNdc) {
                return [
                    'drug_id' => $drugNdc->drug_id,
                    'rxcui_id' => $drugNdc->_matchingData['DrugNdcRxcui']->rxcui_id
                ];
            })
            ->toArray();
             */

        dd($rxcuis);

        $tsiId = 2;
        $pharmaTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');
        $pharmaEntity = $pharmaTable->find()
            ->select([
                'username',
                'password',
                'host', 
                'scheme',
                'port',
                'request_path',
                'medication_path',
                'submit_path',
                'authentication_path',
                'authentication_type_id'
            ])
            ->where([
                'id' => $tsiId
            ])
            ->firstOrFail();

        $connectionInformation = $pharmaEntity->toArray();
        $connectionInformation['timeout'] = 30;

        $originalRequestPath = $connectionInformation['request_path'];

        $pharmacogenomicProvider = new PharmacogenomicProvider($tsiId);

        $connection = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($connectionInformation);
    }
}
