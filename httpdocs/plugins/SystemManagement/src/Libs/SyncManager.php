<?php

namespace SystemManagement\Libs;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class SyncManager
{
    private static $instance = null;

    private $syncTable;

    private $type;

    private $syncEntity;

    private function __construct()
    {
        $this->syncTable = TableRegistry::getTableLocator()->get('SystemManagement.Syncs');
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new SyncManager;
        }

        return self::$instance;
    }

    /**
     * @param string $type
     */
    public function start(string $type)
    {
        $this->type = $type;

        $nowTime = \Cake\I18n\FrozenTime::now();
        $nowDate = \Cake\I18n\FrozenDate::now();

        $syncEntity = $this->syncTable->find()
            ->where([
                'type' => $this->type,
                'last_finished IS' => null,
                'DATE(last_started)' => $nowDate->format('Y-m-d') // TODO: what if we go into the next day between runs
            ])
            ->order([
                'last_started' => 'desc'
            ])
            ->first();

        if (is_null($syncEntity)) {
            $syncEntity = $this->syncTable->newEntity([
                'initial_start' => $nowTime,
                'last_started' => $nowTime,
                'type' => $this->type,
            ]);
        } else {
            $syncEntity->last_started = $nowTime;
        }

        if (!$this->syncTable->save($syncEntity)) {
            throw new \Exception('Unable to save sync entity');
        }

        return true;
    }

    public function getSync()
    {
        $nowDate = \Cake\I18n\FrozenDate::now();

        return $this->syncTable->find()
            ->where([
                'type' => $this->type,
                'last_finished IS' => null,
                'DATE(last_started)' => $nowDate->format('Y-m-d')
            ])
            ->order([
                'last_started' => 'desc'
            ])
            ->first();
    }

    /**
     * @param Entity $syncEntity
     */
    public function saveSync(Entity $syncEntity, $syncData)
    {
        $syncEntity = $this->syncTable->patchEntity(
            $syncEntity,
            $syncData
        );

        if (!$this->syncTable->save($syncEntity)) {
            throw new \Exception('Unable to save sync entity');
        }
    }
}
