<?php
namespace SystemManagement\Libs;

use Cake\ORM\TableRegistry;

/**
 * Class EventLogs
 * @package SystemManagement\Libs
 */
class EventLogs
{

    public static function write(
        int $healthCareClientId = null,
        int $labOrderId = null,
        int $labOrderStatusId = null,
        string $action = null,
        string $message = null,
        array $details = null,
        string $requestDetails = null
    ): bool
    {
        if ($healthCareClientId) {
            try {
                $eventLogsTable = TableRegistry::getTableLocator()->get('SystemManagement.EventLogs');
                $eventLogEntity = $eventLogsTable->newEntity([
                    'health_care_client_id' => $healthCareClientId,
                    'lab_order_id' => $labOrderId,
                    'lab_order_status_id' => $labOrderStatusId,
                    'action' => $action,
                    'message' => $message,
                    'details' => json_encode($details),
                    'request_details' => $requestDetails
                ]);
                return (bool)$eventLogsTable->save($eventLogEntity);
            } catch (\Exception $e) {
                // if the log failed to write we are in some trouble.
                // doh!
                return false;
            }
        }
        return false;
    }

}
