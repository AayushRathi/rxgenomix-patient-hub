<?php

namespace SystemManagement\Libs;

use Cake\Collection\Collection;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

final class DrugMapper
{
    const CHUNK_LIMIT = 1000;

    private static $instance = null;

    private $drugsTable;

    private $connectionInformation;

    private $connection;

    private function __construct()
    {
        $version = RxNav::getVersion();
        $dateTimeObject = \DateTime::createFromFormat('d-M-Y', $version);
        $this->rxNavVersionDate = $dateTimeObject->format('Y-m-d');
        
        $this->drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');
        $this->rxcuisTable = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis');
        $this->medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');

        $this->establishConnection();
    }

    private function establishConnection() {
        $tsiId = 2;
        $pharmaTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');
        $tsiNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.TsiNdcRxcui');
        $pharmaEntity = $pharmaTable->find()
            ->select([
                'username',
                'password',
                'host', 
                'scheme',
                'port',
                'request_path',
                'medication_path',
                'submit_path',
                'authentication_path',
                'authentication_type_id'
            ])
            ->where([
                'id' => $tsiId
            ])
            ->firstOrFail();

        $connectionInformation = $pharmaEntity->toArray();
        $connectionInformation['timeout'] = 60;
        $originalRequestPath = $connectionInformation['request_path'];

        $this->connectionInformation = $connectionInformation;

        $pharmacogenomicProvider = new PharmacogenomicProvider($tsiId);

        $connection = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($connectionInformation);

        $this->connection = $connection;
    }

    /**
     * @param ConsoleIo $io
     */
    public function setConsoleIo(ConsoleIo $io) {
        $this->io = $io;
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new DrugMapper;
        }

        return self::$instance;
    }

    public function run()
    {
        $query = $this->drugsTable->find();
	$query->andWhere([
            'Drugs.mapped_date IS' => null,
	]);

        $recordCount = $query->count();
        $this->io->out('Found '. $recordCount . ' records');

        if ($recordCount > 0) {
            $drugEntities = $query
                ->contain([
                    'Rxcui'
                ])
                ->enableHydration(false)
                ->toArray();

            // add a delay in between loops to not get firewalled out of whatever API
            $batches = array_chunk($drugEntities, self::CHUNK_LIMIT);

            $this->io->out('Dividing into ' . count($batches) . ' batches');

            foreach ($batches as $batchIndex => $batch) {
                $this->io->out('Processing batch ' . ($batchIndex+1) . ' of ' . count($batches));
                foreach ($batch as $drug) {
                    $this->mapDrug($drug);
                }
            }
        }

    }

    /**
     * Attempt to map the drug
     *
     * @param array $drug
     *
     */
    public function mapDrug(array $drug) {
        $mapTypeId = null;
        $isManuallySet = false;
        $toReview = false;
        $scdIds = [];
        $sbdIds = [];
        $otherIds = [];
        $tsiMedicationIds = [];

        $medicationEntity = null;
        $rxcuiId = null;
        $inRxcuiId = null;
        $minRxcuiId = null;

        $skipIds = [26];

        if (in_array($drug['id'], $skipIds)) {
            return false;
        }

        // There are 3 scenarios we need to account for
        // 1. Rely on drug_rxcui and if there is an SCD rxcui found, map the very first one (if it doesn't match it will be marked for review by QA)
        // 2. Rely on drug_Rxcui for rxcuis and if there are just SBD rxcuis then lookup through NIH to find the first SCD and map it. if it doesn't match it will be marked for review by QA
        // 3. If drug_rxcui has no matches, rely on our local drug_ndc_rxcui which is an aggregation of the drug_ndcs after a lookup to NIH's RXCUI api to try to find a possible SCD rxcui to use
        // 4. If a drug is a combo (is_combo=1), extract the "MIN" or specify multiple rxcuis
        $rxcuisClean = [];
        $rxcuiEntities = [];
        $replacedRxcuiEntities = [];

        debug($drug);
        debug('drug["rxcui"] count ' . count($drug['rxcui']));

        // try getting rxcuis from the drug_rxcui table
        $rxcuiEntities = $this->getRxcuiEntitiesFromRxcuis($drug['rxcui']);

        // try getting NIH rxcuis
        if (empty($rxcuiEntities)) {
            $rxcuiEntities = RxNav::getNihRxcuisByDrugId($drug['id']);
        }

        if (!empty($rxcuiEntities)) {
            $rxcuiTypes = (new Collection($rxcuiEntities))->groupBy('tty')->toArray();

            $maxPerType = 0;
            foreach ($rxcuiTypes as $type => $rxcuis) {
                if (count($rxcuis) > $maxPerType) {
                    $maxPerType = count($rxcuis);
                }
            }

            // if we only have one rxcui type, process it
            if (count(array_keys($rxcuiTypes)) === 1) {
                debug('only one rxcui type');
                debug($rxcuiTypes);
                if (!isset($rxcuiTypes['SCD'])) {
                    // exchange sbds for scds
                    if (isset($rxcuiTypes['SBD']) && !empty($rxcuiTypes['SBD'])) {
                    } else if (isset($rxcuiTypes['BPCK']) && !empty($rxcuiTypes['BPCK'])) {
                        $minRxcuiEntity = RxNav::findAndSaveFirstRxcuiByType($rxcuiTypes['BPCK'][0], 'MIN', $this->rxNavVersionDate);

                        if ($minRxcuiEntity) {
                            $minRxcuiId = $minRxcuiEntity->id;
                        }
                    }
                } else {
                    $rxcuis = RxNav::findAndSaveRxcuisByTypes($rxcuiTypes['SCD'][0], ['MIN', 'IN'], $this->rxNavVersionDate);
                    if ($rxcuis) {
                        $rxcuis = (new Collection($rxcuis))->groupBy('tty')->toArray();

                        if (isset($rxcuis['MIN']) && count($rxcuis['MIN']) === 1) {
                            $minRxcuiId = $rxcuis['MIN'][0]->id;
                            $rxcuiEntities = [$rxcuis['MIN'][0]];
                        } else if (isset($rxcuis['MIN']) && count($rxcuis['MIN']) > 1) {
                        }
                    }
                }
            // there are more than one type of rxcui so we need complex scenarios
            } else if ($maxPerType >1) {
            // theres just a max of one type of rxcui so prioritize then
            } else {
                // drug #21529 goes here
                if (isset($rxcuiTypes['SCD'])) {
                    $minRxcuiEntity = RxNav::findAndSaveFirstRxcuiByType($rxcuiTypes['SCD'][0], 'MIN', $this->rxNavVersionDate);
                    if ($minRxcuiEntity) {
                        $rxcuiEntities = [$minRxcuiEntity];
                        $minRxcuiId = $minRxcuiEntity->id;
                    }
                } else if (isset($rxcuiTypes['BPCK']) && isset($rxcuiTypes['SBD'])) {
                    debug($rxcuiTypes);
                    $rxcuis = RxNav::findAndSaveRxcuisByTypes($rxcuiTypes['BPCK'][0], ['SCD'], $this->rxNavVersionDate);
                    if ($rxcuis) {
                        $rxcuiEntities = [$rxcuis[0]];
                    }
                }
            }
        }

        debug($drug);
        debug('a1');

        if (empty($rxcuiEntities)) {
            $rxcuiNodes = RxNav::getNihRxcuisByDrugId($drug['id']);

            // lookup by name if this is not a combo
            if (empty($rxcuiNodes) && $drug['is_combo'] === false) {
                $cleanName = preg_match(DrugHelper::DRUG_CLEAN_PATTERN, $drug['specificproductname'], $matches);

                if ($cleanName) {
                    $cleanName = trim($matches[1]);
                    // try lookup by name
                    $this->medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
                    $lookupMedication = $this->medicationTable->find()
                        ->where([
                            'LOWER(Medications.ingredient_title) LIKE' => '%' . $cleanName . '%',
                        ])
                        ->order([
                            'is_brand' => 'ASC'
                        ])
                        ->first();

                    // bind to the first found medication but mark that it was manually set
                    if ($lookupMedication && $lookupMedication->tsi_rxcui_id > 0 && $lookupMedication->tsi_medication_id > 0) {
                        $medicationResult = [];
                        $tsiMedicationIds[] = [
                            'tsi_medication_id' => $lookupMedication->tsi_medication_id,
                            'tsi_rxcui_id' => $lookupMedication->tsi_rxcui_id
                        ];

                        $mapTypeId = 1;
                        $isManuallySet = true;
                    }
                }
            } elseif (!empty($rxcuiNodes)) {
                $rxcuiAttemptNodes = $rxcuisTable
                    ->find()
                    ->where([
                        'Rxcuis.rxcui IN' => $rxcuiNodes,
                        'Rxcuis.tty IS NOT' => null
                    ])
                    ->toArray();

                if (!empty($rxcuiAttemptNodes)) {
                    if ($drug['is_combo'] === false) {
                        foreach ($rxcuiAttemptNodes as $node) {
                            if (isset($node->tty) && mb_strlen($node->tty) > 0 && mb_strtoupper($node->tty) === 'SCD') {
                                $rxcuiEntities[] = $node;
                                $scdIds[] = $node->id;
                                break;
                            }

                            if (isset($node->tty) && mb_strlen($node->tty) > 0 && mb_strtoupper($node->tty) === 'SBD') {
                                $scdEntity = RxNav::findAndSaveFirstRxcuiByType($node, 'SCD', $this->rxNavVersionDate);

                                if ($scdEntity && isset($scdEntity->tty) && mb_strtoupper($scdEntity->tty) === 'SCD') {
                                    $rxcuiEntities[] = $scdEntity;
                                    $scdIds[] = $scdEntity->id;
                                    break;
                                }
                            }
                        }
                    } else {
                        debug($drug);
                        debug('create clause for MIN');
                    }
                }
            }
        }

        if (!empty($rxcuiEntities)) {
            // Case 1. Try to bind the first SCD if this is not a combination
            if ($drug['is_combo'] === false) {
                foreach ($rxcuiEntities as $rxcuiEntity) {
                    if ($rxcuiEntity->tty !== 'SCD') {
                        continue;
                    }

                    $medicationPath = $this->connectionInformation['medication_path'] . '?code=http://www.nlm.nih.gov/research/umls/rxnorm|%s';
                    $this->connectionInformation['request_path'] = sprintf($medicationPath, $rxcuiEntity->rxcui);
                    // its died within 30 seconds plenty of times
                    $this->connectionInformation['timeout'] = 30*5;
                    $this->connectionInformation['headers'] = ['Accept' => 'application/json'];

                    $medicationResult = Translational::processMedicationByRxcui(
                        $drug,
                        $this->connectionInformation, 
                        $this->connection, 
                        $rxcuiEntity
                    );

                    if (!is_array($medicationResult) || !isset($medicationResult['rxcui_id']) || !isset($medicationResult['tsi_medication_id'])) {
                        continue;
                    }

                    $firstMappedMedication = $this->medicationTable->find()
                        ->where([
                            'tsi_medication_id' => $medicationResult['tsi_medication_id']
                        ])
                        ->firstOrFail();

                    #if (self::IO_PROMPT && $io->ask('Manually map the first SCD to a medication found in TSI? ' . $rxcuiEntity->title . ' to ' . $firstMappedMedication->ingredient_title . '?') === 'y') {
                    $tsiMedicationIds[] = $medicationResult;
                    $mapTypeId = 2;
                    #}
                }
            }


            if (count($tsiMedicationIds) === 0) {
                foreach ($rxcuiEntities as $rxcuiEntity) {
                    if ($rxcuiEntity->tty !== 'SCD' && $rxcuiEntity->tty !== 'BPCK' && $rxcuiEntity->tty !== 'MIN') {
                        debug($minRxcuiId);
                        debug($mapTypeId);
                        debug($rxcuiId);
                        debug($rxcuiEntities);
                        continue;
                    }

                    if ($minRxcuiId && count($rxcuiEntities) === 1) {
                        $rxcuisFound = $rxcuiEntities;
                    } else if ($drug['is_combo'] === false) {
                        $rxcuisFound = RxNav::findAndSaveRxcuisByType($rxcuiEntity, 'IN', $this->rxNavVersionDate);
                    } else {
                        $rxcuisFound = RxNav::findAndSaveRxcuisByType($rxcuiEntity, 'MIN', $this->rxNavVersionDate);
                        if (is_array($rxcuisFound) && !empty($rxcuisFound)) {
                        }
                    }

                    if (count($rxcuisFound) > 1) {
                        $toReview = true;
                    }

                    if (!is_null($rxcuisFound) && count($rxcuisFound) > 0) {
                        $firstFoundRxcuiEntity = $rxcuisFound[0];
                        $medicationPath = $this->connectionInformation['medication_path'] . '?code=http://www.nlm.nih.gov/research/umls/rxnorm|%s';
                        $this->connectionInformation['request_path'] = sprintf($medicationPath, $firstFoundRxcuiEntity->rxcui);
                        // its died within 30 seconds plenty of times
                        $this->connectionInformation['timeout'] = 30*5;
                        $this->connectionInformation['headers'] = ['Accept' => 'application/json'];

                        $medicationResult = Translational::processMedicationByRxcui(
                            $drug,
                            $this->connectionInformation, 
                            $this->connection, 
                            $firstFoundRxcuiEntity
                        );

                        if (!is_array($medicationResult) || !isset($medicationResult['rxcui_id']) || !isset($medicationResult['tsi_medication_id'])) {
                            continue;
                        }

                        $firstMappedMedication = $this->medicationTable->find()
                            ->where([
                                'tsi_medication_id' => $medicationResult['tsi_medication_id']
                            ])
                            ->firstOrFail();

                        $tsiMedicationIds[] = $medicationResult;
                        $mapTypeId = 3;
                        break;
                    }
                }
            }
        }

        // establish a "primary" rxcui to use to pass to TSI
        if (count($tsiMedicationIds) === 0) {
            debug($rxcuiEntities);
            foreach ($rxcuiEntities as $rxcuiEntity) {
                if ($rxcuiEntity->tty !== 'SCD') {
                    continue;
                }

                if ($drug['is_combo'] === false && DrugHelper::compareDrugStrings($rxcuiEntity, $drug['specificproductname'])) {
                    $mapTypeId = 4;
                    $isManuallySet = true;
                    $tsiMedicationIds[] = [
                        'rxcui_id' => $rxcuiEntity->id
                    ];
                    break;
                } elseif ($drug['is_combo'] === true && DrugHelper::compareDrugStringsCombo($rxcuiEntity, $drug['specificproductname'])) {
                    $mapTypeId = 5;
                    $isManuallySet = true;
                    $tsiMedicationIds[] = [
                        'rxcui_id' => $rxcuiEntity->id
                    ];
                    break;
                } elseif (self::IO_PROMPT) {
                    #if ($io->ask('Manually map the first SCD?' . $rxcuiEntity->title . ' to ' . $drug['specificproductname'] . '?') === 'y') {
                    $toReview = true;
                    $isManuallySet = true;
                    $tsiMedicationIds[] = [
                        'rxcui_id' => $rxcuiEntity->id
                    ];
                    $mapTypeId = 6;
                    break;
                    #}
                } else {
                    $toReview = true;
                    $isManuallySet = true;
                    $tsiMedicationIds[] = [
                        'rxcui_id' => $rxcuiEntity->id
                    ];
                    $mapTypeId = 7;
                    break;
                }
            }

            if (empty($tsiMedicationIds)) {
                debug($drug['specificproductname']);
                debug('tried to compare but nothing mapped');
            }
        }

        debug(count($sbdIds));

        // does this condition even happen?
        if (empty($tsiMedicationIds) && count($sbdIds) > 0 && count($scdIds) < 1) {
            debug(count($sbdIds));
            debug($rxcuiEntities);
            debug($drug);
            exit;
        }

        $drugEntity = $this->drugsTable
            ->find()
            ->where([
                'Drugs.id' => $drug['id']
            ])
            ->firstOrFail();

        $data = [
            'medication_id' => null,
            'tsi_rxcui_id' => null,
            'rxcui_id' => null,
            'is_searchable' => 0,
            'is_processed' => 1,
            'mark_for_review' => 0,
            'mapped_type_id' => null,
            'mapped_date' => null
        ];

        if ($minRxcuiId) {
            $data['min_rxcui_id'] = $minRxcuiId;
        }

        if ($drug['is_combo'] == 0 && $minRxcuiId) {
            $mapTypeId = 8;
            $data['is_combo'] = 1;
        }

        if (!empty($tsiMedicationIds)) {
            $uniqueIds = array_unique((new Collection($tsiMedicationIds))->extract('tsi_medication_id')->toArray());

            if (count($uniqueIds) > 1) {
                if (count($uniqueIds) > 1) {
                    $isManuallySet = true;
                }
                // mark for review
                $medicationEntity = $this->medicationTable
                    ->find()
                    ->where([
                        'Medications.tsi_medication_id' => $tsiMedicationIds[0]['tsi_medication_id']
                    ])
                    ->firstOrFail();

                $data['medication_id'] = $medicationEntity->id;
                $data['tsi_rxcui_id'] = $tsiMedicationIds[0]['tsi_rxcui_id'] ?? null;
                $data['rxcui_id'] = $tsiMedicationIds[0]['rxcui_id'] ?? null;
                $data['is_searchable'] = 0;
            } elseif ($isManuallySet && isset($tsiMedicationIds[0]['tsi_medication_id'])) {
                $medicationEntity = $this->medicationTable
                    ->find()
                    ->where([
                        'Medications.tsi_medication_id' => $tsiMedicationIds[0]['tsi_medication_id']
                    ])
                    ->firstOrFail();

                $data['medication_id'] = $medicationEntity->id;
                $data['tsi_rxcui_id'] = $tsiMedicationIds[0]['tsi_rxcui_id'];
                $data['rxcui_id'] = null;
                $data['is_searchable'] = 1;
            } elseif ($isManuallySet && isset($tsiMedicationIds[0]['rxcui_id'])) {
                $rxciEntity = $this->rxcuisTable
                    ->find()
                    ->where([
                        'Rxcuis.id' => $tsiMedicationIds[0]['rxcui_id']
                    ])
                    ->firstOrFail();

                $data['medication_id'] = null;
                $data['tsi_rxcui_id'] = null;
                $data['rxcui_id'] = $tsiMedicationIds[0]['rxcui_id'];
                $data['is_searchable'] = 1;
            } elseif (count($uniqueIds) === 1) {
                $medicationEntity = $this->medicationTable
                    ->find()
                    ->where([
                        'Medications.tsi_medication_id' => $tsiMedicationIds[0]['tsi_medication_id']
                    ])
                    ->firstOrFail();

                $data['medication_id'] = $medicationEntity->id;
                $data['tsi_rxcui_id'] = $tsiMedicationIds[0]['tsi_rxcui_id'] ?? null;
                $data['rxcui_id'] = $tsiMedicationIds[0]['rxcui_id'] ?? null;
                $data['is_searchable'] = 1;
            } 
        }

        if ($isManuallySet) {
            $data['is_manually_set_medication'] = true;
        }

        if ($mapTypeId) {
            $data['mapped_type_id'] = $mapTypeId;
            $data['mapped_date'] = \Cake\I18n\FrozenDate::now();
        }

        if ($toReview) {
            $data['mark_for_review'] = 1;
            $data['is_searchable'] = 0;
        }

        if (is_null($data['medication_id'])) {
            unset($data['medication_id']);
        }

        if (is_null($mapTypeId)) {
            debug($drug);
            exit;
        }

        #if (is_null($data['rxcui_id'])) {
        #unset($data['rxcui_id']);
        #}

        $drugEntity = $this->drugsTable->patchEntity($drugEntity, $data);

        if (!$this->drugsTable->save($drugEntity)) {
            throw new \Exception('Failed to save drug');
        }

        debug($drugEntity);
    }

    /**
     * Fix incorrectly marked is_combo drugs (after ImportDrugs) because we need this to be correct
     */
    public function fixComboDrugs()
    {
        $sql = "
select
    *
from
	drugs
where
	is_combo = 0
and replace(
	replace(
		replace(
			replace(
				replace(
					replace(
						replace(
							replace(
								replace(
									replace(
										replace(
											specificproductname ,
											', sprinkles' ,
											''
										) ,
										', calibrated' ,
										''
									) ,
									', cleanser' ,
									''
								) ,
								', modified release' ,
								''
							) ,
							', liquid' ,
							''
						) ,
						', liquid filled' ,
						''
					) ,
					', biphasic release' ,
					''
				) ,
				', suspension' ,
				'' 		) ,
			', gastro-resistant' ,
			''
		) ,
		', solution' ,
		''
	) ,
	', extended release' ,
	''
) LIKE '%,%'	
";

        $connection = \Cake\Datasource\ConnectionManager::get('default');

        if (!$connection) {
            throw new \Exception('Connection not found');
        }

        $stmt = $connection->execute($sql);

        $records = $stmt->fetchAll('assoc');

        debug('Found ' . count($records) . ' records.');
        $drugIds = [];

        if ($records && !empty($records)) {
            foreach ($records as $index => $record) {
                $productname = preg_replace('/([\d+]),([\d+])/', "$1$2", $record['specificproductname']);
                $productname = DrugHelper::extractSimpleName($productname);
                $productname = DrugHelper::stripCommasFromAmount($productname);

                debug($productname);
                debug(strpos($productname, ','));
                if (strpos($productname, ',') === false) {
                    #debug('Removing ' . $productname);
                    unset($records[$index]);
                } else {
                    debug($record['id']);
                    debug(DrugHelper::extractSimpleName($record['specificproductname']));
                    debug($productname);
                    #exit;
                    $drugIds[] = $record['id'];
                }
            }
        }

        if (empty($drugIds)) {
            dd('No drug ids specified');
        }

        $drugEntities = TableRegistry::getTableLocator()->get('SystemManagement.Drugs')
            ->find()
            ->contain([
                'Rxcui'
            ])
            ->where([
                'Drugs.id IN' => $drugIds
            ])
            ->enableHydration(false)
            ->toArray();

        debug(count($drugEntities));

        foreach ($drugEntities as $drugEntity) {
            #if (/*$drugEntity['id'] == 3170 || $drugEntity['id'] == 3468 || $drugEntity['id'] == 7894 || */$drugEntity['id'] == 17710) {
                $this->mapDrug($drugEntity);
            #}
        }
    }

    /**
     * @param string $ttyFilter Filter by a specific TTY (SCD, SBD)
     */
    public function getRxcuiEntitiesFromRxcuis(array $rxcuis, string $ttyFilter = '', bool $isActive = true) {
        if (empty($rxcuis)) {
            return [];
        }

        $rxcuisClean = [];
        $rxcuiEntities = [];
        foreach ($rxcuis as $record) {
            if (!is_null($record)) {
                $rxcuisClean[] = $record['rxcui'];
            }
        }

        if (!empty($rxcuisClean)) {
            foreach ($rxcuisClean as $id) {
                $rxcuiEntity = RxNav::createOrFind($id, $this->rxNavVersionDate);

                if (isset($rxcuiEntity->is_active) && $rxcuiEntity->is_active == $isActive) {
                    $hasTty = isset($rxcuiEntity->tty) && mb_strlen($rxcuiEntity->tty) > 0;

                    // dont include if we are filtering by a tty and it doesnt match the tty
                    if (
                        !$hasTty || 
                        (mb_strlen(trim($ttyFilter)) > 0 && mb_strtoupper($rxcuiEntity->tty) !== mb_strtoupper($ttyFilter))
                    ) {
                        debug('Filtering by ' . $ttyFilter . ' so skipping because TTY is ' . $rxcuiEntity->tty);
                        continue;
                    }

                    $rxcuiEntities[] = $rxcuiEntity;
                }
            }
        }

        return $rxcuiEntities;
    }

}
