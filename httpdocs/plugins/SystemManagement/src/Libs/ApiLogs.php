<?php
namespace SystemManagement\Libs;

use Cake\ORM\TableRegistry;

/**
 * Class ApiLogs
 * @package SystemManagement\Libs
 */
class ApiLogs
{

    /**
     * @param string|null $ip
     * @param string|null $request
     * @param string|null $message
     * @return bool
     */
    public static function write(string $authId = null, string $ip = null, string $request = null, string $message = null): bool
    {
        if ($authId && $ip && $request && $message) {
            $apiLogTable = TableRegistry::getTableLocator()->get('SystemManagement.ApiLogs');
            $apiLogEntity = $apiLogTable->newEntity([
                'api_authentication_id' => $authId,
                'ip' => $ip,
                'request' => $request,
                'message' => $message
            ]);
            return (bool)$apiLogTable->save($apiLogEntity);
        }
    }

}