<?php

namespace SystemManagement\Libs;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Xml;

final class RxNav
{
    const ENDPOINT_VERSION = 'https://rxnav.nlm.nih.gov/REST/version.json';
    const ENDPOINT_RXCUI = 'https://rxnav.nlm.nih.gov/REST/rxcui.json?idtype=NDC&id=%s';
    const ENDPOINT_RXCUI_STATUS = 'https://rxnav.nlm.nih.gov/REST/rxcui/%s/status.json';
    const ENDPOINT_RXCUI_PROPERTIES = 'https://rxnav.nlm.nih.gov/REST/rxcui/%s/properties.json';
    const ENDPOINT_RELATED = 'https://rxnav.nlm.nih.gov/REST/rxcui/%s/allrelated.json';
    const ENDPOINT_NAME = 'https://rxnav.nlm.nih.gov/REST/rxcui.json?name=%s';

    const DEFAULT_TIMEOUT = 10; // seconds
    const DO_LOG = false;

    public static $conceptTtys = ['SCD', 'SBD', 'BPCK', 'GPCK', 'IN'];

    /**
     * Get rxcuis by name
     *
     * @param string $name
     *
     */
    public static function getRxcuisByName(string $name)
    {
        if (mb_strlen($name) === 0) {
            return false;
        }

        $ch = curl_init();
        $endpoint = sprintf(self::ENDPOINT_NAME, urlencode($name));

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
    
        if ($raw === false || $curl_error !== 0) {
            return false;
        }

        try {
            $json = json_decode($raw, true);

            if (!isset($json['idGroup']) || !isset($json['idGroup']['rxnormId'])) {
                return false;
            }

            return $json['idGroup']['rxnormId'];

        } catch (\Exception $e) {
            return false;
        }
    }

    /** 
     * @param Entity $drugEntity
     *
     * @return null|string
     */
    public static function getNihRxcuisByDrugId(int $drugId)
    {
        $drugNdcTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');

        $rxcuiIds = $drugNdcTable
            ->find()
            ->matching('DrugNdcRxcui')
            ->where([
                'drug_id' => $drugId,
            ])
            ->extract(function ($drugNdc) {
                return $drugNdc->_matchingData['DrugNdcRxcui']->rxcui_id;
            })
            ->toArray();

        // remove duplicate values
        $rxcuiIds = array_unique(array_filter($rxcuiIds));

        // reset keys 
        $rxcuiIds = array_values(array_unique(array_filter($rxcuiIds)));

        if (empty($rxcuiIds)) {
            return null;
        }

	$rxcuiEntities = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis')
	    ->find()
	    ->where([
		'Rxcuis.id IN' => $rxcuiIds
	    ])
	    ->toArray();

	return $rxcuiEntities;
    }

    // Drugs can have many RXCUIs so we have to find the more general RXCUI
    // The SCD (Semantic Clinical Drug) describes the Ingredient + Strength + Dose Form
    // which is what we want, rather than say the SBD which includes the brand name which is less generic
    //
    public static function getPrimaryRxcui(array $ids)
    {
        $primaryEntity = null;
        $scdEntities = TableRegistry::getTableLocator()->get('SystemManagement.Rxcui')
            ->find()
            ->where([
                'Rxcui.tty IN' => 'SCD',
                'Rxcui.is_active' => true,
                'Rxcui.id IN' => $ids
            ])
            ->toArray();

        if (count($scdEntities) > 1) {
            throw new \Exception('Do a lookup to determine the primary SCD entity');
        }

        $primaryEntity = $scdEntities[0] ?? null;

        return $primaryEntity;
    }

    /**
     * @param Entity $rxcuiEntity
     * @param string $tty
     * @param string $rxNavVersionDate
     */
    public static function findAndSaveFirstRxcuiByType(
        Entity $rxcuiEntity,
        string $tty,
        $rxNavVersionDate
    ) {
        $relatedInfo = self::getRelated($rxcuiEntity->rxcui);

        if ($relatedInfo) {
            $collection = new \Cake\Collection\Collection($relatedInfo['conceptGroup']);

            $group = $collection->firstMatch([
                'tty' => mb_strtoupper($tty)
            ]);

            if ($group && isset($group['conceptProperties']) && !empty($group['conceptProperties'])) {
                foreach ($group['conceptProperties'] as $item) {
                    $rxcuiEntity = self::createOrFind($item['rxcui'], $rxNavVersionDate);

                    if ($rxcuiEntity) {
                        return $rxcuiEntity;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param Entity $rxcuiEntity
     * @param string $tty
     * @param string $rxNavVersionDate
     */
    public static function findAndSaveRxcuisByTypes(
        Entity $rxcuiEntity,
        array $ttys,
        $rxNavVersionDate
    ) {
        $relatedInfo = self::getRelated($rxcuiEntity->rxcui);
        $rxcuiEntities = [];

        if ($relatedInfo) {
            $collection = new \Cake\Collection\Collection($relatedInfo['conceptGroup']);

            foreach ($ttys as $tty) {
                $group = $collection->firstMatch([
                    'tty' => mb_strtoupper($tty)
                ]);

                if ($group && isset($group['conceptProperties']) && !empty($group['conceptProperties'])) {
                    foreach ($group['conceptProperties'] as $item) {
                        $rxcuiEntity = self::createOrFind($item['rxcui'], $rxNavVersionDate);

                        if ($rxcuiEntity) {
                            $rxcuiEntities[] = $rxcuiEntity;
                        }
                    }
                }
            }
        }

        return $rxcuiEntities;
    }

    /**
     * @param Entity $rxcuiEntity
     * @param string $tty
     * @param string $rxNavVersionDate
     */
    public static function findAndSaveRxcuisByType(
        Entity $rxcuiEntity,
        string $tty,
        $rxNavVersionDate
    ) {
        $relatedInfo = self::getRelated($rxcuiEntity->rxcui);
        $rxcuiEntities = [];

        if ($relatedInfo) {
            $collection = new \Cake\Collection\Collection($relatedInfo['conceptGroup']);

            $group = $collection->firstMatch([
                'tty' => mb_strtoupper($tty)
            ]);

            if ($group && isset($group['conceptProperties']) && !empty($group['conceptProperties'])) {
                foreach ($group['conceptProperties'] as $item) {
                    $rxcuiEntity = self::createOrFind($item['rxcui'], $rxNavVersionDate);

                    if ($rxcuiEntity) {
                        $rxcuiEntities[] = $rxcuiEntity;
                    }
                }
            }
        }

        return $rxcuiEntities;
    }

    /**
     * @param Entity $rxcuiEntity
     * @param string $rxNavVersionDate
     *
     */
    public static function storeRelatedConcepts(Entity $parentRxcuiEntity, string $rxNavVersionDate)
    {
        $concepts = array_diff(self::$conceptTtys, [$parentRxcuiEntity->tty]);

        $storedEntities = [];

        if (count($concepts) > 0) {
            $relatedInfo = self::getRelated($parentRxcuiEntity->rxcui);
            if ($relatedInfo) {
                $collection = new \Cake\Collection\Collection($relatedInfo['conceptGroup']);

                foreach ($concepts as $concept) {
                    $group = $collection->firstMatch([
                        'tty' => mb_strtoupper($concept)
                    ]);

                    if ($group && isset($group['conceptProperties']) && !empty($group['conceptProperties'])) {
                        foreach ($group['conceptProperties'] as $item) {
                            $rxcuiEntity = self::createOrFind($item['rxcui'], $rxNavVersionDate);

                            if ($rxcuiEntity) {
                                $storedEntities[] = $rxcuiEntity;
                            }
                        }
                    }
                }
            }
        }

        if (count($storedEntities) > 0) {
            $rxcuiMapTable = TableRegistry::getTableLocator()->get('SystemManagement.RxcuiMap');
            $parentId = $parentRxcuiEntity->id;

            $rxcuiMapTable->deleteAll([
                'parent_rxcui_id' => $parentId
            ]);

            $inserts = [];
            foreach ($storedEntities as $entity) {
                // dont relate to itself
                if ($entity->id !== $parentId) {
                    $inserts[] = $rxcuiMapTable->newEntity([
                        'parent_rxcui_id' => $parentId,
                        'child_rxcui_id' => $entity->id
                    ]);
                }
            }

            try {
                $save = $rxcuiMapTable->saveMany($inserts);
            } catch (\Exception $e) {
                debug($e->getMessage());
                debug('Could not store rxcui map');
            }
        }

        return $storedEntities;
    }

    /**
     * @param string $rxcui The rxcui code
     * @param string $rxNavVersionDate the version date of the RxNav API, Y-m-d string to compare to
     *
     * @return null|Entity
     *
     **/
    public static function createOrFind(
        string $rxcui, 
        string $rxNavVersionDate
    ) {
        $rxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.Rxcuis');

        // Find or create the rxcui record
        $rxcuiEntity = $rxcuiTable->findOrCreate([
            'rxcui' => $rxcui,
        ]);

        $insertOrUpdateRxcui = true;
        if (
            isset($rxcuiEntity->rxcui_updated) && 
            $rxcuiEntity->rxcui_updated instanceof \Cake\I18n\FrozenDate &&
            $rxcuiEntity->rxcui_updated->format('Y-m-d') === $rxNavVersionDate
        ) {
            $insertOrUpdateRxcui = false;
        }

        if ($insertOrUpdateRxcui) {
            // grab the latest info
            $rxcuiInfo = self::getRxcuiInfo($rxcui);

            if ($rxcuiInfo === false) {
                return null;
            }

            // additional info
            $extraInfo = self::getRxcuiFullInfo($rxcui);

            // Update it
            $rxcuiEntity->title = $rxcuiInfo['minConceptGroup']['minConcept'][0]['name'] ?? null;
            $rxcuiEntity->tty = $rxcuiInfo['minConceptGroup']['minConcept'][0]['tty'] ?? null;
            // Status can be "Retired" so not all of them are active
            $rxcuiEntity->is_active = $rxcuiInfo['status'] === 'Active' ? 1 : 0;
            $rxcuiEntity->rxcui_updated = $rxNavVersionDate;

            $rxcuiEntity->synonym = $extraInfo['synonym'] ?? null;

            if (!$rxcuiTable->save($rxcuiEntity)) {
                throw new \Exception('Unable to save rxcui');
            }
        }

        return $rxcuiEntity;
    }

    /**
     * @param string $rxcui - The rxcui
     *
     **/
    public static function getRelated(string $rxcui)
    {
        if (mb_strlen($rxcui) === 0) {
            return false;
        }

        $ch = curl_init();
        $endpoint = sprintf(self::ENDPOINT_RELATED, $rxcui);

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
    
        if ($raw === false || $curl_error !== 0) {
            return false;
        }

        try {
            $json = json_decode($raw, true);

            if (
                !isset($json['allRelatedGroup']) || 
                !isset($json['allRelatedGroup']['rxcui']) ||
                !isset($json['allRelatedGroup']['conceptGroup']) ||
                empty($json['allRelatedGroup']['conceptGroup'])
            ) {
                return false;
            }

            return $json['allRelatedGroup'];

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $rxcui - The rxcui
     *
     **/
    public static function getRxcuiFullInfo(string $rxcui)
    {
        if (mb_strlen($rxcui) === 0) {
            return false;
        }

        $ch = curl_init();
        $endpoint = sprintf(self::ENDPOINT_RXCUI_PROPERTIES, $rxcui);

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
    
        if ($raw === false || $curl_error !== 0) {
            return false;
        }

        try {
            $json = json_decode($raw, true);

            if (!isset($json['properties'])) {
                return false;
            }

            return $json['properties'];

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $rxcui - The rxcui
     *
     **/
    public static function getRxcuiInfo(string $rxcui)
    {
        if (mb_strlen($rxcui) === 0) {
            return false;
        }

        $ch = curl_init();
        $endpoint = sprintf(self::ENDPOINT_RXCUI_STATUS, $rxcui);

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
    
        if ($raw === false || $curl_error !== 0) {
            return false;
        }

        try {
            $json = json_decode($raw, true);

            if (!isset($json['rxcuiStatus'])) {
                return false;
            }

            return $json['rxcuiStatus'];

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $ndc - The 9 digit NDC
     *
     **/
    public static function getRxcuiFromNdc(string $ndc)
    {
        if (mb_strlen($ndc) === 0) {
            return false;
        }

        $ch = curl_init();
        $endpoint = sprintf(self::ENDPOINT_RXCUI, $ndc);

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
    
        if ($raw === false || $curl_error !== 0) {
            return false;
        }

        try {
            $json = json_decode($raw, true);

            if (!isset($json['idGroup']) || !isset($json['idGroup']['rxnormId'])) {
                return false;
            }

            return $json['idGroup']['rxnormId'];

        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getVersion()
    {
        $lib24watchLogsTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchLogs');

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::ENDPOINT_VERSION);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER	, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::DEFAULT_TIMEOUT);

        $raw = curl_exec($ch);

        $curl_error = curl_errno($ch);
        $curl_error_message = curl_error($ch);

        curl_close($ch);

        // if no error, parse
        if ($raw === false || $curl_error !== 0) {
            if (self::DO_LOG) {
                $message = 'Could not parse version endpoint';
                if (mb_strlen($curl_error_message) > 0) {
                    $message.= ' .(' . $curl_error_message . ')';
                }

                $lib24watchLogsTable->write(null, 'RxNav::getVersion', $message, 'SystemManagement');
            }
            // log
            
            return false;
        }

        try {
            $json = json_decode($raw, true);
            $versionString = $json['version'];

            // validate format
            $dateTime = \DateTime::createFromFormat('d-M-Y', $versionString);

            if ($dateTime) {
                return $versionString;
            }

            if (self::DO_LOG) {
                $message = 'Version in wrong format';

                $lib24watchLogsTable->write(null, 'RxNav::getVersion', $message, 'SystemManagement');
            }

        } catch (\Cake\Utility\Exception\XmlException $e) {
            if (self::DO_LOG) {
                $message = 'Could not build XML after request';
                $lib24watchLogsTable->write(null, 'RxNav::getVersion', $message, 'SystemManagement');
            }
        }

        return false;
    }
}
