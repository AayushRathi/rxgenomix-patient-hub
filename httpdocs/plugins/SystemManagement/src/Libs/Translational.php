<?php

namespace SystemManagement\Libs;

use App\Connectors\Connector;
use Cake\Collection\Collection;
use Cake\Filesystem\File;
use Cake\I18n\FrozenTime;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use SystemManagement\Libs\RxNav;
use SystemManagement\Libs\SyncManager;

final class Translational
{
    public static $bundleTypes = [
        'collection',
        'message',
        'searchset'
    ];

    /**
     * @param array $drug
     * @param array $connectionInformation
     * @param Connector $connection
     * @param Entity $rxcuiEntity
     *
     */
    public static function processMedicationByRxcui(
        array $drug,
        array $connectionInformation,
        Connector $connection,
        Entity $rxcuiEntity
    ) {
        $medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
        $tsiNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.TsiNdcRxcui');
        $result = $connection->get($connectionInformation);

        if (!$result) {
            return false;
        }

        $response = json_decode($result, true);

        $validResponse = self::validateBundleByType('searchset', $response);

        if ($validResponse['total'] > 1) {
            throw new \Exception('More than 1 medication');
        }

        $medicationNode = self::getResource($response, 'Medication');

        if (!$medicationNode) {
            return false;
        }

        $ingredientTitle = $medicationNode['resource']['ingredient'][0]['itemReference']['display'] ?? null;

        // medication #8301 has an empty strong for display, but text has something to compare with
        if (is_null($ingredientTitle) || $ingredientTitle === '') {
            $ingredientTitle = $medicationNode['resource']['code']['text'] ?? null;
        }

        if (!$ingredientTitle || !isset($drug['nondose_name']) || mb_strlen($drug['nondose_name'] === 0)) {
            debug($medicationNode);
            debug($drug['nondose_name']);
            debug('No ingredient title');
            return false;
        }

        // If the medication isnt found (case insensitive) in the drug name then don't map it
        if (stripos($ingredientTitle, $drug['nondose_name'] !== false)) {
            dd('stripos doesnt match');
            return false;
        }

        $existingMedicationEntity = $medicationTable
            ->find()
            ->where([
                'Medications.tsi_medication_id' => (int)$medicationNode['resource']['id']
            ])
            ->first();

        $isPgxRelevantOnly = false;
        $pullNdcs = true;
        $storeRelatedConcepts = false;

        $version = RxNav::getVersion();
        $versionDateTime = \DateTime::createFromFormat('d-M-Y', $version);

        if (is_null($versionDateTime)) {
            return false;
        }

        $medicationData = self::createMedication(
            $medicationNode,
            $isPgxRelevantOnly,
            $pullNdcs,
            $storeRelatedConcepts,
            $versionDateTime
        );

        if (is_null($medicationData) || $medicationData === false) {
            return false;
        }

        if (isset($medicationData['tsi_medication_id'])) {
            if ($existingMedicationEntity) {
                unset($medicationData['created']);
                $entity = $medicationTable->patchEntity($existingMedicationEntity, $medicationData);
            } else {
                $entity = $medicationTable->newEntity($medicationData);
            }

            $ndcCodes = $entity['ndc_codes'] ?? [];
            unset($entity['ndc_codes']);

            if (!$medicationTable->save($entity)) {
                return false;
            }

            if (isset($ndcCodes) && !empty($ndcCodes)) {
                $ndcInserts = [];
                $existingNdcs = $tsiNdcRxcuiTable->find()
                    ->where([
                        'ndc_11 IN' => $ndcCodes,
                        'tsi_medication_id' => $medicationData['tsi_medication_id']
                    ])
                    ->indexBy('ndc_11')
                    ->toArray();

                foreach ($medicationData['ndc_codes'] as $ndcCode) {
                    $exists = array_key_exists($ndcCode, $existingNdcs);

                    $ndcRow = [
                        'ndc_11' => $ndcCode,
                        'ndc_9' => substr($ndcCode, 0, -2),
                        'tsi_medication_id' => $medicationData['tsi_medication_id'],
                        'rxcui_id' => $entity->tsi_rxcui_id,
                        'created' => new \Cake\I18n\FrozenDate
                    ];

                    if ($exists) {
                        $ndcInserts[] = $tsiNdcRxcuiTable->patchEntity($existingNdcs[$ndcCode], $ndcRow);
                    } else {
                        $ndcInserts[] = $tsiNdcRxcuiTable->newEntity($ndcRow);
                    }
                }

                if (!empty($ndcInserts)) {
                    try {
                        $saved = $tsiNdcRxcuiTable->saveMany($ndcInserts);
                    } catch (\Exception $e) {
                        var_dump($ndcInserts);
                        debug($e->getMessage());
                    }
                }
            }
        }

        return [
            'rxcui_id' => $rxcuiEntity->id,
            'tsi_medication_id' => (int)$medicationData['tsi_medication_id'],
        ];
    }

    /**
     * @param Connector $connection
     * @param array $connectionInformation
     *
     */
    public static function processMedications(
        Connector $connection, 
        array $connectionInformation,
        SyncManager $syncManager
    )
    {
        $syncEntity = $syncManager->getSync();
        $syncData = [];

        if (is_null($syncEntity)) {
            throw new \Exception('No sync entity found');
        }

        if (is_null($syncEntity->metadata)) {
            $syncEntity->metadata = [
                'processed' => []
            ];

            $connectionInformation['request_path'] = $connectionInformation['medication_path'] . '?page=1';
        } else {
            if (!isset($syncEntity->metadata['next_url'])) {
                throw new \Exception('No next url');
            }

            $syncData = [
                'metadata' => $syncEntity->metadata
            ];

            $connectionInformation['request_path'] = $syncEntity->metadata['next_url'];
        }

        $firstQuery = $connection->get($connectionInformation);

        $response = json_decode($firstQuery, true);

        if (is_null($response)) {
            $syncData['metadata']['errors'][] = 'Unable to decode JSON from URL ' . $connectionInformation['request_path'];

            $syncManager->saveSync($syncEntity, $syncData);

            throw new \Exception('Unable to decode JSON');
        }

        $validResponse = self::validateBundleByType('searchset', $response);

        if (!$validResponse) {
            $syncData['metadata']['errors'][] = 'Unable to validate response from URL ' . $connectionInformation['request_path'];

            $syncManager->saveSync($syncEntity, $syncData);

            throw new \Exception('Unable to validate response');
        }

        // page count
        $collection = new Collection($response['link']);

        $nextNode = $collection->firstMatch([
            'relation' => 'next'
        ]);

        $lastNode = $collection->firstMatch([
            'relation' => 'last'
        ]);

        $medicationNodes = self::getResources($response, 'Medication');

        // we are now saving all medications in order to pull in all alternate medications in the MAP - 2020-06-04
        $savePgxOnly = false;

        $pullNdcs = true;
        $storeRelatedConcepts = true;
        $inserted = self::saveMedications($medicationNodes, $savePgxOnly, $pullNdcs, $storeRelatedConcepts);

        // log that we parsed this page
        if ($inserted) {
            $parseUrl = parse_url($connectionInformation['request_path']);

            if (!isset($parseUrl['query'])) {
                throw new \Exception('No query set');
            }

            parse_str($parseUrl['query'], $splitQuery);

            $medicationCount = 0;
            if (isset($inserted['savedMedications']) && !empty($inserted['savedMedications'])) {
                $medicationCount = count($inserted['savedMedications']);
            }

            $ndcCount = 0;
            if (isset($inserted['savedNdcs']) && !empty($inserted['savedNdcs'])) {
                $ndcCount = count($inserted['savedNdcs']);
            }

            $syncData['metadata']['processed'][$splitQuery['page']] = [
                'medications_inserted' => $medicationCount,
                'ndcs_inserted' => $ndcCount
            ];

            if (is_null($nextNode)) {
                $syncManager->saveSync($syncEntity, $syncData);
            }
        }

        // keep going whether new medications were found/inserted or not
        if (!is_null($nextNode)) {
            $connectionInformation['request_path'] = $nextNode['url'];

            $syncData['metadata']['next_url'] = $connectionInformation['request_path'];

            $syncManager->saveSync($syncEntity, $syncData);

            debug($syncEntity);

            self::processMedications($connection, $connectionInformation, $syncManager);
        }

        return true;
    }

    /**
     * @param string $type The type of request
     * @param array $response
     *
     */
    public static function validateBundleByType(string $type, array $response)
    {
        if (
            !array_key_exists('resourceType', $response) ||
            $response['resourceType'] !== 'Bundle' ||
            !array_key_exists('type', $response) ||
            mb_strtolower($response['type']) !== mb_strtolower($type) ||
            !array_key_exists('entry', $response) ||
            empty($response['entry'])
        ) {
            return false;
        }

        if (!in_array(mb_strtolower($type), self::$bundleTypes)) {
            throw new \Exception('Unknown bundle type');
        }

        // Specific bundle type conditions
        if (mb_strtolower($type === 'searchset')) {
            if (
                !array_key_exists('total', $response) ||
                $response['total'] <= 0
                #!array_key_exists('link', $response) ||
                #empty($response['link'])
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $response
     *
     */
    public static function getMedicationId(array $response)
    {
        $medicationId = null;

        if ($valid) {
            return (int)$response['entry'][0]['resource']['id'];
        }

        return $medicationId;
    }

    /**
     * @param array $medicationStatements
     */
    public static function extractMedicationIds(array $medicationStatements, bool $compareAgainstExisting = false)
    {
        $medicationIds = [];
        if (!empty($medicationStatements)) {
            foreach ($medicationStatements as $medicationStatement) {
                if (isset($medicationStatement['resource']['id']) && (int)$medicationStatement['resource']['id'] > 0) {
                    $medicationIds[] = (int)$medicationStatement['resource']['id'];
                }
            }
        }

        // Filter by existing medications
        if ($compareAgainstExisting && count($medicationIds) > 0) {
            $medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
            $existingMedicationIds = $medicationTable
                ->find()
                ->select([
                    'tsi_medication_id',
                ])
                ->where([
                    'Medications.tsi_medication_id IN' => array_unique($medicationIds),
                ])
                ->extract('tsi_medication_id')
                ->toArray();

            if (!empty($existingMedicationIds)) {
                $medicationIds = array_diff($medicationIds, $existingMedicationIds);
            }
        }

        return $medicationIds;
    }

    /**
     * @param array $response
     * @param string $type
     *
     * @return array
     *
     **/
    public static function getResources(array $response, string $type)
    {
        $resources = [];

        foreach ($response['entry'] as $resource) {
            if (mb_strtolower($resource['resource']['resourceType']) === mb_strtolower($type)) {
                $resources[] = $resource;
            }
        }

        return $resources;
    }

    /**
     * @param string $path The path to save to
     * @param array $presentedForm Array that contains the pdf encoded
     *
     */
    public static function savePdf(string $path, array $presentedForm)
    {
        if (
            !array_key_exists('contentType', $presentedForm) || 
            $presentedForm['contentType'] !== 'application/pdf' ||
            !array_key_exists('data', $presentedForm)
            //!array_key_exists('language', $presentedForm) ||
            //!array_key_exists('hash', $presentedForm)
        ) {
            throw new \Exception('Unable to validate PDF');
        }

        $directory = dirname($path);

        if (!is_writable($directory)) {
            throw new \Exception('Directory not writable');
        }

        $file = new File($path, true);
        $file->write(base64_decode($presentedForm['data']));
        $file->close();

        return true;
    }

    /**
     * @param array $response
     * @param string $type
     *
     * @return array
     *
     **/
    public static function getResource(array $response, string $type)
    {
        foreach ($response['entry'] as $resource) {
            if (mb_strtolower($resource['resource']['resourceType']) === mb_strtolower($type)) {
                return $resource;
            }
        }

        return null;
    }

    public static function extractPdf(array $resource)
    {
        if (mb_strtolower($resource['resourceType']) !== 'provenance') {
            return false;
        }
    }

    /**
     * @param array $medication
     *
     * @return bool
     */
    public static function validateMedicationNode(array $medication): bool
    {
        if (
            !array_key_exists('resource', $medication) ||
            !array_key_exists('resourceType', $medication['resource']) ||
            !array_key_exists('id', $medication['resource']) ||
            !array_key_exists('code', $medication['resource']) ||
            !array_key_exists('extension', $medication['resource']) ||
            !array_key_exists('ingredient', $medication['resource']) ||
            count($medication['resource']['ingredient']) === 0 ||
            /*
             * !array_key_exists('coding', $medication['resource']['code']) || 
             * tsi #6931 doesnt have an rxcui so check for it
             * 2020-05-15 - making it bind anyway
             */
            !array_key_exists('itemReference', $medication['resource']['ingredient'][0]) ||
            !array_key_exists('reference', $medication['resource']['ingredient'][0]['itemReference']) ||
            !array_key_exists('display', $medication['resource']['ingredient'][0]['itemReference'])
        ) {
            return false;
        }

        return true;
    }

    /**
     * @param array $medicationIds
     * @param array $medicationNodes
     *
     */
    public static function saveMedications(
        array $medicationNodes, 
        bool $isPgxRelevantOnly=false,
        bool $pullNdcs = true,
        bool $storeRelatedConcepts = true
    )
    {
        $medicationInserts = [];
        $ndcInserts = [];
        $medicationTable = TableRegistry::getTableLocator()->get('SystemManagement.Medications');
        $tsiNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.TsiNdcRxcui');

        $tsiMedicationIds = [];
        foreach ($medicationNodes as $medication) {
            if (
                isset($medication['resource']['id']) && 
                (int)$medication['resource']['id'] > 0
            ) {
                $tsiMedicationIds[] = (int)$medication['resource']['id'];
            }
        }

        if (!empty($tsiMedicationIds)) {
            $medicationInserts = $medicationTable
                ->find()
                ->where([
                    'Medications.tsi_medication_id IN' => $tsiMedicationIds
                ])
                ->indexBy('tsi_medication_id')
                ->toArray();
        }

        $version = RxNav::getVersion();
        $versionDateTime = \DateTime::createFromFormat('d-M-Y', $version);

        if (is_null($versionDateTime) || (($versionDateTime instanceof \DateTime) === false )) {
            dd('Could not generate version');
            return false;
        }

        foreach ($medicationNodes as $medication) {
            if (
                isset($medication['resource']['id']) && 
                (int)$medication['resource']['id'] > 0 && // tsi id
                // isset($medication['resource']['code']['coding']) && // removed because for example tsi #8595 doesnt have an rxcui entity
                isset($medication['resource']['extension']) &&  // metadata
                !empty($medication['resource']['extension'])
            ) {
                if (isset($medication['resource']['id']) && $medication['resource']['id'] == '8595') {
                    debug($medication);
                }

                $medicationData = self::createMedication(
                    $medication,
                    $isPgxRelevantOnly,
                    $pullNdcs,
                    $storeRelatedConcepts,
                    $versionDateTime
                );

                if (isset($medication['resource']['id']) && $medication['resource']['id'] == '8595') {
                    debug($medicationData);
                }

                if (isset($medicationData['tsi_medication_id'])) {
                    $exists = array_key_exists($medicationData['tsi_medication_id'], $medicationInserts);

                    if ($exists) {
                        unset($medicationData['created']);
                        $medicationInserts[$medicationData['tsi_medication_id']] = $medicationTable->patchEntity($medicationInserts[$medicationData['tsi_medication_id']], $medicationData);
                    } else {
                        $medicationInserts[$medicationData['tsi_medication_id']] = $medicationTable->newEntity($medicationData);
                    }

                    if (isset($medicationData['ndc_codes']) && !empty($medicationData['ndc_codes'])) {
                        $existingNdcs = $tsiNdcRxcuiTable->find()
                            ->where([
                                'ndc_11 IN' => $medicationData['ndc_codes'],
                                'tsi_medication_id' => $medicationData['tsi_medication_id']
                            ])
                            ->indexBy('ndc_11')
                            ->toArray();

                        foreach ($medicationData['ndc_codes'] as $ndcCode) {
                            $exists = array_key_exists($ndcCode, $existingNdcs);

                            $ndcRow = [
                                'ndc_11' => $ndcCode,
                                'ndc_9' => substr($ndcCode, 0, -2),
                                'tsi_medication_id' => $medicationData['tsi_medication_id'],
                                'rxcui_id' => $medicationData['tsi_rxcui_id'],
                                'created' => new \Cake\I18n\FrozenDate
                            ];

                            if ($exists) {
                                $ndcInserts[] = $tsiNdcRxcuiTable->patchEntity($existingNdcs[$ndcCode], $ndcRow);
                            } else {
                                $ndcInserts[] = $tsiNdcRxcuiTable->newEntity($ndcRow);
                            }
                        }

                        unset($medicationData['ndc_codes']);
                    }
                }
            }
        }

        $medicationInserts = array_values($medicationInserts);

        $savedMedications = false;
        $savedNdcs = false;

        if (!empty($medicationInserts)) {
            try {
                $savedMedications = $medicationTable->saveMany($medicationInserts);
            } catch (\Exception $e) {
                debug($e->getMessage());
            }
        }

        if (!empty($ndcInserts)) {
            try {
                $savedNdcs = $tsiNdcRxcuiTable->saveMany($ndcInserts);
            } catch (\Exception $e) {
                var_dump($ndcInserts);
                debug($e->getMessage());
            }
        }

        return compact('savedNdcs', 'savedMedications');
    }

    /**
     * @param array $medication
     * @param bool $isPgxRelevantOnly
     * @param string $rxcui
     */
    public static function createMedication(
        array $medication,
        bool $isPgxRelevantOnly = false,
        bool $pullNdcs = true,
        bool $storeRelatedConcepts = true,
        \DateTime $versionDateTime
    )
    {
        if (self::validateMedicationNode($medication) === false) {
            return false;
        }

        $medicationRow = null;
        $hasRxcui = false;
        $validateRxcui = false;
        $rxNode = null;
        $rxcuiNodeCollection = null;

        if (isset($medication['resource']['code']['coding'])) {
            $rxcuiNodeCollection = new Collection($medication['resource']['code']['coding']);
            $rxNode = $rxcuiNodeCollection->firstMatch([
                'system' => 'http://www.nlm.nih.gov/research/umls/rxnorm'
            ]);
        }

        if ($validateRxcui && is_null($rxcuiNode)) {
            debug('Validating RXCUIs in TSI and none found');
            return false;
        }

        $extensions = new Collection($medication['resource']['extension']);

        // Although TSI assigns just one RXCUI to one medication
        $tsiMedicationId = $medication['resource']['id'];

        $isPgxRelevantNode = $extensions->firstMatch([
            'url' => 'https://help.pgxportal.com/wiki/display/SOF/IsPgxRelevant'
        ]);

        // skip non pgx relevant medications if specified
        if ($isPgxRelevantOnly && $isPgxRelevantNode && $isPgxRelevantNode['valueBoolean'] === false) {
            debug('skip medication because filtering by pgx relevant only');
            return false;
        }

        $dateTime = FrozenTime::now();

        $rxNavVersionDate = $versionDateTime->format('Y-m-d');

        $medicationRow = [
            'tsi_medication_id' => $tsiMedicationId,
            'ingredient_title' => $medication['resource']['ingredient'][0]['itemReference']['display'] ?? '',
            'created' => $dateTime,
            'modified' => $dateTime,
        ];

        if ($rxNode) {
            $tsiRxcuiEntity = RxNav::createOrFind($rxNode['code'], $rxNavVersionDate);
            if ($tsiRxcuiEntity && $storeRelatedConcepts) {
                $relatedEntities = RxNav::storeRelatedConcepts($tsiRxcuiEntity, $rxNavVersionDate);
            }

            if ($tsiRxcuiEntity) {
                $medicationRow['tsi_rxcui_id'] = $tsiRxcuiEntity->id;
            }
        }

        if (isset($medication['resource']['isBrand']) && mb_strlen($medication['resource']['isBrand'] > 0)) {
            $medicationRow['is_brand'] = (bool)$medication['resource']['isBrand'] ? 1 : 0;
        }

        // there are really 3 titles of an ingredient
        // code/text, ingredient[0]/title and /code/coding
        if (isset($medication['resource']['code']['text'])) {
            $medication['code_text'] = $medication['resource']['code']['text'];
        }

        $propertyMap = [
            'is_pgx_relevant' => 'https://help.pgxportal.com/wiki/display/SOF/IsPgxRelevant',
            'prealert_trigger' => 'https://help.pgxportal.com/wiki/display/SOF/PrealertTrigger',
            'category' => 'https://help.pgxportal.com/wiki/display/SOF/Category',
            'drug_class' => 'https://help.pgxportal.com/wiki/display/SOF/DrugClass',
            'trade_names' => 'https://help.pgxportal.com/wiki/display/SOF/TradeNames'
        ];

        foreach ($propertyMap as $column => $url) {
            $match = $extensions->firstMatch([
                'url' => $url
            ]);

            if ($match) {
                if (isset($match['valueString'])) {
                    $medicationRow[$column] = $match['valueString'];
                } elseif (isset($match['valueBoolean'])) {
                    $medicationRow[$column] = $match['valueBoolean'];
                }
            }
        }

        if ($medicationRow && $pullNdcs && !is_null($rxcuiNodeCollection)) {
            $ndcs = $rxcuiNodeCollection->match([
                'system' => 'ndc11'
            ]);

            if ($ndcs) {
                $ndcCodes = $ndcs->extract('code')->toArray();

                $medicationRow['ndc_codes'] = $ndcCodes;
            }
        }

        return $medicationRow;
    }
}
