<?php

namespace SystemManagement\Libs;

use Cake\Console\ConsoleIo;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use SystemManagement\Libs\RxNav;

class RxcuiImporter
{
    protected $logger = null;

    protected $io = null;

    // Should we log anything
    const DO_LOG = false;

    // Limit per page
    const ROW_LIMIT = 500;

    public function __construct(ConsoleIo $io)
    {
        $this->io = $io;
    }

    public function process(array $whereConditions=[], $options=[])
    {
        $version = RxNav::getVersion();
        $dateTimeObject = \DateTime::createFromFormat('d-M-Y', $version);
        $rxNavVersionDate = $dateTimeObject->format('Y-m-d');

        $lib24watchLogsTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchLogs');

        if ($version === false) {
            if (self::DO_LOG) {
                $message = 'Could not get version';
                $lib24watchLogsTable->write(null, 'FetchRxcuiForNdc Shell', $message, 'SystemManagement');
            }
        }

        // pull existing ndc/rxcuis that are up to date to not include
        $drugNdcRxcuiTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdcRxcui');

        $drugNdcTable = TableRegistry::getTableLocator()->get('SystemManagement.DrugNdc');
        $query = $drugNdcTable->find();

        $defaultWhereConditions = [
            // TODO: version clause
            'DrugNdc.drug_id IS NOT' => null,
            'DrugNdc.ndc !=' => 'NULL', // the string NULL is saved
            'DrugNdc.ndc IS NOT' => null
            //'DrugNdc.id IN' => [419839, 419840]
        ];

        $whereConditions = array_merge($whereConditions, $defaultWhereConditions);

        $recordCount = $query
            ->andWhere($whereConditions)
            //->notMatching('DrugNdcRxcui')
            ->count();

        if ($recordCount === 0 && self::DO_LOG) {
            $message = 'No records could be found';
            $lib24watchLogsTable->write(null, 'FetchRxcuiForNdc Shell', $message, 'SystemManagement');
        }

        $pages = (int)ceil($recordCount / self::ROW_LIMIT);

        $insertCount = 0;

        $limit = $pages+1;

        for ($i = 1; $i<($limit); $i++) {
            $this->io->out('Processing batch #' . $i . ' of ' . $pages);
            $offset = (($i-1) * self::ROW_LIMIT);

            $inserts = [];

            $query = $drugNdcTable->find();
            $drugNdcRecords = $query
                ->select([
                    'DrugNdc.drug_id',
                    'DrugNdc.id',
                    'DrugNdc.ndc'
                ])
                ->offset($offset)
                //->order(['DrugNdc.id' => 'ASC'])
                ->andWhere($whereConditions)
                ->enableHydration(false)
                //->notMatching('DrugNdcRxcui')
                ->limit(self::ROW_LIMIT)
                ->toArray();

            if (!empty($drugNdcRecords)) {
                foreach ($drugNdcRecords as $record) {
                    $rxcuiCodes = RxNav::getRxcuiFromNdc($record['ndc']);

                    if ($rxcuiCodes == false || !is_array($rxcuiCodes) || count($rxcuiCodes) < 1) {
                        continue;
                    }

                    $j = 0;

                    // technically there can be multiple rxcuis for an ndc but usually theres one
                    foreach ($rxcuiCodes as $rxcui) {
                        $rxcuiEntity = RxNav::createOrFind($rxcui, $rxNavVersionDate);

                        $row = [
                            'drug_ndc_id' => $record['id'],
                            'rxcui_id' => $rxcuiEntity->id,
                        ];

                        $inserts[] = $row;

                        $j++;

                        // pause every other X request to the API by 1 second, throttle is 20 requests per second
                        if ($j%15===0) {
                            sleep(1);
                        }
                    }
                }

                // process inserts
                if (!empty($inserts)) {
                    $entities = $drugNdcRxcuiTable->newEntities($inserts);

                    try {
                        $entitySave = $drugNdcRxcuiTable->saveMany($entities);
                        $insertCount+= count($entities);
                    } catch (\Exception $e) {
                        var_dump($e->getMessage());
                        // If we dont have a DrugNdc.id NOT IN condition, it will fail
                        $this->io->out('Could not save batch #' . $i . ' of ' . $pages);
                    }

                }
            }
        }

        $this->io->out('Inserted ' . $insertCount . ' records into drug_ndc_rxcui');
    }
}
