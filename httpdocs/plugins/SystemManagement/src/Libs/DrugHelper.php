<?php

namespace SystemManagement\Libs;

use Cake\Collection\Collection;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use SystemManagement\Libs\RxNav;

final class DrugHelper
{
    // change this to have IO prompts
    const IO_PROMPT = false;

    const DRUG_CLEAN_PATTERN = '/([A-Za-z ]+)(\d+ ?(?:\.\d+)?)( ?mg)/i';
    const DRUG_PATTERN_SCD = '/([A-Za-z ]+)(\d+ ?(?:\.\d+)?)( ?mg) (.+)/i';

    const RXCUI_DELIMITER = ' / ';
    const DRUG_DELIMITER = ',';

    const STRENGTH_REGEX_RXCUI = '/(\d+) (MG|G|%|mL)/';
    const STRENGTH_REGEX_DRUG = '/(\d+)(mg|g|%|ml)/';

    const INGREDIENT_REGEX_AS = '/(.+)(?:\(as) (.+)[^)]/';

    /**
     * @param Entity $rxcuiEntity The rxcui entity
     * @param string $drugTitle the drug title
     *
     * @return bool
     */
    public static function compareDrugStringsCombo(Entity $rxcuiEntity, $drugTitle, bool $isCombo = false)
    {
        $rxcuiTitle = $rxcuiEntity->title;

        // lower case the strength
        $rxcuiTitle = preg_replace_callback(
            self::STRENGTH_REGEX_RXCUI,
            function($matches) {
                return $matches[1] . mb_strtolower($matches[2]);
            },
            $rxcuiTitle
        );

        $rxcuiTitle = self::normalizeDrugString(self::RXCUI_DELIMITER, $rxcuiTitle);

        $drugTitle = self::normalizeDrugString(self::DRUG_DELIMITER, $drugTitle);

        if ($rxcuiTitle == $drugTitle) {
            return true;
        }

        if (isset($rxcuiEntity->synonym) && mb_strlen($rxcuiEntity->synonym) > 0) {
            // try to compare by the alias
            $rxcuiSynonymTitle = $rxcuiEntity->synonym;

            // lower case the strength
            $rxcuiSynonymTitle = preg_replace_callback(
                self::STRENGTH_REGEX_RXCUI,
                function($matches) {
                    return $matches[1] . mb_strtolower($matches[2]);
                },
                $rxcuiSynonymTitle
            );

            $compareUsingAlias = true;
            $rxcuiSynonymTitle = self::normalizeDrugString(self::RXCUI_DELIMITER, $rxcuiSynonymTitle, $compareUsingAlias);

            if ($rxcuiSynonymTitle == $drugTitle) {
                return true;
            }

            #debug($rxcuiSynonymTitle);
            #debug($drugTitle);
            #exit;
        }

        return false;
    }

    /**
     * @param string $delimiter The delimiter to split by
     * @param string $drugString The drug string
     * @param bool $compareAlias RXCUIs have a synonym and we can compare by this if the original doesnt work
     *
     * @return string
     */
    private static function normalizeDrugString(string $delimiter, string $drugString, bool $compareAlias = false)
    {
        $doses = array_map('trim', explode($delimiter, $drugString));
        $lastSegment = array_pop($doses);

        $split = preg_split(self::STRENGTH_REGEX_DRUG, $lastSegment, null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        if ($compareAlias) {
            foreach ($doses as $index => $dose) {
                $doses[$index] = preg_replace_callback(
                    self::INGREDIENT_REGEX_AS,
                    function($matches) {
                        return trim(preg_replace('/\)$/', '', end($matches)));
                    },
                    $dose
                );
            }

            $split[0] = preg_replace_callback(
                self::INGREDIENT_REGEX_AS,
                function($matches) {
                    return trim(preg_replace('/\)$/', '', end($matches))) . ' ';
                },
                $split[0]
            );
        }

        $splitLast = array_pop($split);
        $doses[] = (implode('', $split));
        sort($doses);
        $doses[] = trim($splitLast);

        return mb_strtolower(implode(',', $doses));
    }

    /**
     * @param Entity $rxcuiEntity The rxcui entity
     * @param string $drugTitle the drug title
     *
     * @return bool
     */
    public static function compareDrugStrings(Entity $rxcuiEntity, $drugTitle, bool $isCombo = false)
    {
        preg_match(self::DRUG_PATTERN_SCD, mb_strtolower($rxcuiEntity->title), $drugOneMatches);
        preg_match(self::DRUG_PATTERN_SCD, mb_strtolower($drugTitle), $drugTwoMatches);

        if ($drugOneMatches && $drugTwoMatches && count($drugOneMatches) === count($drugTwoMatches)) {
            $ingredientOne = trim($drugOneMatches[1]);
            $strengthNumOne = trim($drugOneMatches[2]);
            $strengthTextOne = trim($drugOneMatches[3]);
            $doseOne = trim($drugOneMatches[4]);

            $ingredientTwo = trim($drugTwoMatches[1]);
            $strengthNumTwo = trim($drugTwoMatches[2]);
            $strengthTextTwo = trim($drugTwoMatches[3]);
            $doseTwo = trim($drugTwoMatches[4]);

            if (
                $ingredientOne == $ingredientTwo &&
                $strengthNumOne == $strengthNumTwo &&
                $strengthTextOne == $strengthTextTwo &&
                $doseOne == $doseTwo
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Extract a simple name from a drug entity nondose name
     *
     * @param string $name
     *
     * @return string $name
     */
    public static function extractSimpleName(string $name)
    {
        $name = str_replace(
            [
                ', extended release',
                ', suspension',
                ', solution',
                ', gastro-resistant',
                ', biphasic release',
                ', liquid filled',
                ', liquid',
                ', modified release',
                ', cleanser',
                ', calibrated',
                ', sprinkles'
            ], '', strtolower($name)
        );

        return $name;
    }

    /**
     * Remove commas from specific product name amount
     *
     * @param string $name
     *
     * @return string $name
     */
    public static function stripCommasFromAmount(string $name)
    {
        debug($name);
        $name = preg_replace('#(\d+),(\d+)#', "$1$2", $name);
        debug($name);

        return $name;
    }
}
