<?php
namespace AddressVerification\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithCommonSharedComponents
{
    public static function getPluginName()
    {
        return "Address Verification";
    }

    public static function getPluginCommonSharedComponents()
    {
        return [
            "AddressVerification"
        ];
    }
}
