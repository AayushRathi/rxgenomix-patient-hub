<?php
namespace AddressVerification\Controller\Component;

use AddressVerification\UspsVerifier;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * AddressVerification component
 */
class AddressVerificationComponent extends Component
{
    protected $verifier;
    protected $controller;

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->controller = $this->_registry->getController();
        $this->verifier = new UspsVerifier();
    }

    public function verify($address1 = '', $address2 = '', $city = '', $state = '', $zip5 = '', $zip4 = ''){
        return $this->verifier->verify($address1, $address2, $city, $state, $zip5, $zip4);
    }
}
