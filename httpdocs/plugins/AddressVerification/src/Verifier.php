<?php

namespace AddressVerification;

interface Verifier {
    public function verify($address1, $address2, $city, $state, $zip5, $zip4);
}

