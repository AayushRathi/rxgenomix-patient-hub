<?php

namespace AddressVerification;

use Cake\Http\Client;
use Cake\Utility\Xml;

class UspsVerifier implements Verifier
{
    public const API_HOST = 'https://secure.shippingapis.com';
    const USER_ID = '846ORASE2217';

    public function verify($address1, $address2, $city, $state, $zip5, $zip4)
    {
        $address = [
            'Address1' => $address1,
            'Address2' => $address2,
            'City' => $city,
            'State' => $state,
            'Zip5' => $zip5,
            'Zip4' => $zip4
        ];

        return $this->requestVerification($address, 'ShippingAPI.dll', 'Verify');
    }

    private function requestVerification($address, $path, $action){

        $http = new Client();

        $requestXML = [
          'AddressValidateRequest' => [
              '@USERID' => self::USER_ID,
              'Address' => $address
          ]
        ];
        try {
            $response = $http->get(self::API_HOST . '/' . $path,
                [
                    'API' => $action,
                    'XML' => Xml::build($requestXML)->asXML()
                ],
                [
                    'type' => 'text/xml'
                ]
            );

            $xmlResponse = Xml::toArray(Xml::build($response->getStringBody()));
            if(isset($xmlResponse['AddressValidateResponse']['Address']['Error'])){
                $result = [
                    'Error' => [
                        $xmlResponse['AddressValidateResponse']['Address']['Error']['Description']
                    ],
                    'Address' => $address
                ];
            } else {
                $result = [
                    'Address' => $xmlResponse['AddressValidateResponse']['Address']
                ];
            }
        } catch (\Exception $e) {
            // timed out, bypass validation
            $result = false;
        }
        return $result;
    }
}
