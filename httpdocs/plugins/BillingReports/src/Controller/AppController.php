<?php

namespace BillingReports\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'BillingReports', 'Billing Report');
    }

    public static function getPluginIcon()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('icon', 'BillingReports', 'fa-money');
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'billing_reports_view' => 'View Billing Reports'
        ];
    }
}
