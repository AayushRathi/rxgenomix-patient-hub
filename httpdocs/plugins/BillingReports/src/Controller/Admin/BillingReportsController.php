<?php

namespace BillingReports\Controller\Admin;

use BillingReports\Controller\AppController;

/**
 * Class BillingReportsController
 * @package BillingReports\Controller\Admin
 */
class BillingReportsController extends AppController
{
    /**
     * init
     */
    public function initialize()
    {
        $this->loadModel('LabOrders.LabOrders');
        $this->loadModel('LabOrders.LabOrderTypes');
        $this->loadModel('HealthCareClients.HealthCareClients');

        $this->set('healthCareClientOptions', $this->HealthCareClients
            ->find('list', [
                'keyField' => 'id',
                'valueField' => 'title'
            ])->where([
                'has_contract' => 1
            ])->order([
                'title' => 'ASC'
            ])->toArray());

        parent::initialize();
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('billing_reports_view', 'BillingReports',
            'You do not have permission to access this resource.');
        $query = $this->LabOrders->find()->contain([
            'Patient',
            'Provider.HealthCareClient'
        ])->where([
            'LabOrders.lab_order_status_id >= 3',
            'LabOrders.is_deleted' => 0
        ])
        ->order([
            'LabOrders.created' => 'DESC'
        ]);

        $labOrderTypesList = $this->LabOrderTypes->find()->where([
            'is_active' => 1
        ])->order(['display_order'])->combine('id', 'title');

        $labOrderTypesList = $labOrderTypesList;

        $this->set(compact('query', 'labOrderTypesList'));

        if ($this->getRequest()->getData()) {
            $this->getRequest()->getSession()->write('billing_reports_sess', json_encode($this->getRequest()->getData()));
        }

        if ($this->getRequest()->getQuery('export') == 1) {
            $query = $this->filterQuery($query);

            $headers = [
                'Order #',
                'Date',
                'Patient Name',
                'Client',
                'Card ID'
            ];
            $rows = [];
            foreach ($query as $record) {
                $rows[] = [
                    $record->id,
                    lib24watchDate('shortDateFormat', $record->created),
                    $record->patient->first_name . ' ' . $record->patient->last_name,
                    $record->provider->health_care_client->title,
                    $record->patient->card_id
                ];
            }
            if (!empty($rows)) {
                $this->loadComponent("Lib24watch.Lib24watchCsvWriter");
                $filename = 'Billing-Report-' . date('Y-m-d_h_ia', time()) . '.csv';
                $this->Lib24watchCsvWriter->writeCsv($rows, $headers, null, null, $filename);
            } else {
                $this->redirectWithDefault(
                    [
                        'plugin' => 'BillingReports',
                        'controller' => 'BillingReports',
                        'action' => 'index',
                        'prefix' => 'admin'
                    ],
                    'No results to export',
                    'danger'
                );
            }
        }
    }

    /**
     * @param $query
     * @return mixed
     */
    protected function filterQuery($query)
    {
        $filters = json_decode($this->getRequest()->getSession()->read('billing_reports_sess'));
        foreach ($filters as $key => $value) {
            if (trim($value) != '') {
                switch ($key) {
                    case 'patient_name':
                        $query->matching('Patient', function ($q) use ($value) {
                            return $q->where(['OR' => [
                                    ['first_name LIKE' => '%' . $value . '%'],
                                    ['last_name LIKE' => '%' . $value . '%'],
                                    ['concat(first_name, " ", last_name) LIKE' => '%' . $value . '%']
                                ]
                            ]);
                        });
                        break;
                    case 'client_name':
                        $query->matching('Provider', function ($q) use ($value) {
                            return $q->where(['Provider.health_care_client_id' => $value]);
                        });
                        break;
                    case 'created_from':
                        $query->where(['DATE(LabOrders.created) >=' => $value]);
                        break;
                    case 'lab_order_type_id':
                        $query->andWhere([
                            'lab_order_type_id' => $value
                        ]);
                        break;
                    case 'created_to':
                        $query->where(['DATE(LabOrders.created) <=' => $value]);
                        break;
                }
            }
        }
        return $query;
    }
}
