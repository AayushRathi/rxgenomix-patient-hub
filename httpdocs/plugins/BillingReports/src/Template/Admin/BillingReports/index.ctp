<?php

/**
 * @var array $healthCareClientOptions
 * @var Cake\ORM\Query $query
 */

$pluginTitle = $lib24watchModules['BillingReports']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Billing Report';
$this->Breadcrumbs->add($pageHeading);

$datePickerPublicFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
    "datePickerPublicFormat",
    "Lib24watch",
    "YYYY-MM-DD"
);

echo $this->Html->scriptBlock("var datePickerPublicFormatFromSetting='" . $datePickerPublicFormatFromSetting . "';");
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Billing Reports',
                'items' => [
                    [
                        'name' => 'patient_name',
                        'type' => 'text',
                        'label' => 'Patient Name',
                        'function' => function($queryObj, $value) {
                            return $queryObj->matching('Patient', function ($q) use ($value) {
                                return $q->where(['OR' => [
                                        ['first_name LIKE' => '%' . $value . '%'],
                                        ['last_name LIKE' => '%' . $value . '%'],
                                        ['concat(first_name, " ", last_name) LIKE' => '%' . $value . '%']
                                    ]
                                ]);
                            });
                        }
                    ],
                    [
                        'name' => 'client_name',
                        'type' => 'select',
                        'label' => 'Client',
                        'empty' => true,
                        'options' => $healthCareClientOptions,
                        'function' => function ($queryObj, $value) {
                            return $queryObj->matching('Provider', function ($q) use ($value) {
                                return $q->where(['Provider.health_care_client_id' => $value]);
                            });
                        }
                    ],
                    [
                        'name' => 'created_from',
                        'type' => 'datetimepicker',
                        'label' => 'Date Ordered',
                        'placeholder' => 'From',
                        'function' => function ($queryObj, $value) {
                            return $queryObj->where(['DATE(LabOrders.created) >=' => $value]);
                        }
                    ],
                    [
                        'name' => 'created_to',
                        'type' => 'datetimepicker',
                        'label' => '',
                        'placeholder' => 'To',
                        'function' => function ($queryObj, $value) {
                            return $queryObj->where(['DATE(LabOrders.created) <=' => $value]);
                        }
                    ],
                    [
                        'name' => 'lab_order_type_id',
                        'type' => 'select',
                        'label' => 'Lab Order Type',
                        'options' => ['' => 'All'] + $labOrderTypesList->toArray(),
                        'function' => function ($queryObj, $value) {
                            return $queryObj->andWhere(['lab_order_type_id' => $value]);
                        }
                    ]
                ],
                $query,
                $this
            ]);
        ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <?= $pageHeading; ?>
                </h3>
                <ul class="panel-controls">
                    <li>&nbsp;</li>
                    <li><a class="panel-collapse" title="Hide" href="#"><span class="fa fa-angle-down"></span></a></li>
                </ul>
                <div class="pull-right">
                    <?php
                    echo $this->Html->link(
                        "Export to CSV",
                        [
                            'plugin' => 'BillingReports',
                            'controller' => 'BillingReports',
                            'action' => 'index',
                            'prefix' => 'admin',
                            '?' => [
                                'export' => true
                            ]
                        ],
                        [
                            'class' => 'btn btn-condensed btn-primary',
                            'escape' => false,
                            'title' => 'Export to CSV'
                        ]
                    );
                    ?>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'label' => 'Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'date'
                                    ],
                                    [
                                        'name' => 'patient_name',
                                        'label' => 'Patient name',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->patient->first_name . ' ' . $row->patient->last_name;
                                        }
                                    ],
                                    [
                                        'name' => 'client',
                                        'label' => 'Client',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->provider->health_care_client->title;
                                        }
                                    ],
                                    [
                                        'name' => 'card_id',
                                        'label' => 'Card ID',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->patient->card_id;
                                        }
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->Html->link(
                                                'View Lab Order',
                                                [
                                                    'plugin' => 'LabOrders',
                                                    'controller' => 'LabOrders',
                                                    'action' => 'view',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary'
                                                ]
                                            );
                                        }
                                    ]
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
