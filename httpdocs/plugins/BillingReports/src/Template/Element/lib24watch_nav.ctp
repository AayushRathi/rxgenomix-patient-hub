<a href="javascript:void(0)"><span class="fa fa-money"></span><span class="xn-text"><?php echo h($lib24watchModules['BillingReports']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('billing_reports_view', 'BillingReports')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Billing Report",
                [
                    'plugin' => 'BillingReports',
                    'controller' => 'BillingReports',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Billing Report'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>
