<?php
namespace LabOrders\Libs;

use Cake\Collection\Collection;
use Cake\Collection\CollectionInterface;
use Cake\Core\Configure;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\View\View;
use Labs\Libs\Lab;
use Lib24watch\Model\Table\Lib24watchSettingsTable;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\EventLogs;
use SystemManagement\Libs\Translational;

/**
 * Class LabOrders
 * @package LabOrders\Libs
 */
class LabOrders
{
    use MailerAwareTrait;

    /**
     * @var \Cake\ORM\Table
     */
    protected $LabOrders;

    /**
     * @var \Cake\ORM\Table
     */
    protected $Features;

    /**
     * @var string
     */
    protected $deidentifiedPrefix = 'de-identified_';

    protected static $mapTypes = [
        'patient' => [
            'is_active' => 1,
            'title' => 'Patient'
        ],
        'provider' => [
            'is_active' => 1,
            'title' => 'Provider'
        ]
    ];

    public static $severityLevels = [
        'high' => [
            'header' => 'Action Requested',
            'none' => 'No action recommended',
        ],
        'moderate' => [
            'header' => 'Review',
            'none' => 'None',
        ],
        'low' => [
            'header' => 'No Action Necessary',
            'none' => 'None'
        ]
    ];

    const PROCESS_LIMIT_SECONDS = 3 * 60 * 60;

    const LOCK_FILE_ALL = 'lab_orders_all.pid';

    const LOCK_DIR = ROOT . DS . 'data/run/';

    /**
     * LabOrders constructor.
     */
    public function __construct()
    {
        $this->LabOrders = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $this->Features = TableRegistry::getTableLocator()->get('HealthCareClient.HealthCareClientFeatures');
    }

    /**
     * Get the active map types
     * @return array
     */
    public static function getActiveMapTypes(): array
    {
        return (new Collection(self::$mapTypes))->filter(function($type) {
            return $type['is_active'] === 1;
        })->toArray();
    }

    public static function isValidMapType(string $type): bool
    {
        return in_array($type, array_keys(self::$mapTypes));
    }

    /**
     * @param \Cake\Collection\CollectionInterface $results
     * @param null|int $currentRiskScore
     *
     */
    public static function scoreByDrug(
        CollectionInterface $results,
        &$currentRiskScore,
        &$currentRiskMaxScore,
        &$futureRiskScore,
        &$futureRiskMaxScore,
        &$foundTsiMedicationIds,
        &$foundDrugIds
    ) {
        return $results->map(function ($row) use (
            &$currentRiskScore,
            &$currentRiskMaxScore,
            &$futureRiskScore,
            &$futureRiskMaxScore,
            &$foundTsiMedicationIds,
            &$foundDrugIds
        ) {
            if (isset($row['result_medications']) && !empty($row['result_medications'])) {
                foreach ($row['result_medications'] as $medication) {
                    if (isset($medication['medications_mapped']) && !empty($medication['medications_mapped'])) {
                        foreach ($medication['medications_mapped'] as $medicationMapped) {
                            if (isset($medicationMapped['drug_id'])) {
                                if ($row['tsi_severity'] === 'high') {
                                    $currentRiskScore += 2;
                                } else if ($row['tsi_severity'] === 'moderate') {
                                    $currentRiskScore += .5;
                                }

                                $currentRiskMaxScore += 2;

                                if (!in_array($medicationMapped['drug_id'], $foundDrugIds)) {
                                    $foundDrugIds[] = $medicationMapped['drug_id'];
                                }
                            }
                        }
                    }
                }
            }

            return $row;
        });
    }

    /**
     * @param \Cake\Collection\CollectionInterface $results
     * @param null|int $currentRiskScore
     *
     */
    public static function scoreByIssue(
        CollectionInterface $results,
        &$currentRiskScore,
        &$currentRiskMaxScore,
        &$futureRiskScore,
        &$futureRiskMaxScore,
        &$foundTsiMedicationIds,
        &$foundDrugIds
    ) {
        $uniqueMedication = false;
        return $results->map(function ($row) use (
            &$currentRiskScore,
            &$currentRiskMaxScore,
            &$futureRiskScore,
            &$futureRiskMaxScore,
            &$foundTsiMedicationIds
        ) {
            if ($row['tsi_severity'] === 'high') {
                $currentRiskScore += 2;
            } else if ($row['tsi_severity'] === 'moderate') {
                $currentRiskScore += .5;
            }

            $currentRiskMaxScore += 2;

            if (isset($row['result_medications']) && !empty($row['result_medications'])) {
                foreach ($row['result_medications'] as $medication) {
                    if (isset($medication['tsi_medication_id']) && !in_array($medication['tsi_medication_id'], $foundTsiMedicationIds)) {
                        $foundTsiMedicationIds[] = $medication['tsi_medication_id'];
                    }
                }
            }

            return $row;
        });
    }

    /**
     * @param \Cake\Collection\CollectionInterface $results
     * @param null|int $currentRiskScore
     *
     */
    public static function scoreByMedication(
        CollectionInterface $results,
        &$currentRiskScore,
        &$currentRiskMaxScore,
        &$futureRiskScore,
        &$futureRiskMaxScore,
        &$foundTsiMedicationIds,
        &$foundDrugIds,
        $patchData = [] // for estimates
    ) {
        $uniqueMedication = false;

        return $results->map(function ($row) use (
            &$currentRiskScore,
            &$currentRiskMaxScore,
            &$futureRiskScore,
            &$futureRiskMaxScore,
            &$foundTsiMedicationIds,
            $patchData
        ) {
            $alternateMedication = null;
            $alternateMedicationSeverity = null;

            // for estimates
            if (
                !empty($patchData) &&
                isset($patchData[$row->id])
            ) {
                $ref = $patchData[$row->id];

                if (isset($patchData[$row->id]['result_map_note']['alternate_drug_id']) && mb_strlen($patchData[$row->id]['result_map_note']['alternate_drug_id'] > 0)) {
                    $cleanDrugId = explode('_', $ref['result_map_note']['alternate_drug_id'])[0];
                    $row['result_map_note']['alternate_drug'] = TableRegistry::getTableLocator()->get('SystemManagement.Drugs')->find()
                        ->where([
                            'Drugs.id' => $cleanDrugId
                        ])
                        ->contain([
                            'TsiMedication'
                        ])
                        ->firstOrFail();
                } elseif (isset($patchData[$row->id]['result_map_note']['alternate_drug_id']) && mb_strlen($patchData[$row->id]['result_map_note']['alternate_drug_id']) === 0) { // blank means "Select a drug" was chosen so unset the alternate drug
                    unset($row['result_map_note']['alternate_drug']);
                }

                if (isset($patchData[$row->id]['result_map_note']['alternate_result_id']) && mb_strlen($patchData[$row->id]['result_map_note']['alternate_result_id'] > 0)) {
                    $row['result_map_note']['alternate_result'] = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults')->find()
                        ->where([
                            'LabOrderReportResults.id' => $ref['result_map_note']['alternate_result_id']
                        ])
                        ->firstOrFail();
                } elseif (isset($patchData[$row->id]['result_map_note']['alternate_result_id']) && mb_strlen($patchData[$row->id]['result_map_note']['alternate_result_id']) === 0) { // blank means "Select a drug" was chosen so unset the alternate result
                    unset($row['result_map_note']['alternate_result']);
                }
            }

            if (
                isset($row['result_map_note']['alternate_drug']) &&
                isset($row['result_map_note']['alternate_drug']['tsi_medication']) &&
                !empty($row['result_map_note']['alternate_drug']['tsi_medication']) &&
                $row['result_map_note']['alternate_drug']['tsi_medication']['id'] > 0 &&
                isset($row['result_map_note']['alternate_result']) &&
                !empty($row['result_map_note']['alternate_result']) &&
                isset($row['result_map_note']['alternate_result']['tsi_severity'])
            ) {
                $alternateMedication = $row['result_map_note']['alternate_drug']['tsi_medication'];
                $alternateMedicationSeverity = $row['result_map_note']['alternate_result']['tsi_severity'];
            }

            if (isset($row['result_medications']) && !empty($row['result_medications'])) {
                foreach ($row['result_medications'] as $medication) {
                    if (isset($medication['tsi_medication_id']) && !in_array($medication['tsi_medication_id'], $foundTsiMedicationIds)) {
                        $foundTsiMedicationIds[] = $medication['tsi_medication_id'];
                        if ($row['tsi_severity'] === 'high') {
                            $currentRiskScore += 2;
                        } else if ($row['tsi_severity'] === 'moderate') {
                            $currentRiskScore += .5;
                        }

                        if (
                            !is_null($alternateMedication) && !is_null($alternateMedicationSeverity)
                        ) {
                            if ($alternateMedicationSeverity === 'high') {
                                $futureRiskScore += 2;
                            } else if ($alternateMedicationSeverity === 'moderate') {
                                $futureRiskScore += 0.5;
                            }
                        } else {
                            if ($row['tsi_severity'] === 'high') {
                                $futureRiskScore += 2;
                            } else if ($row['tsi_severity'] === 'moderate') {
                                $futureRiskScore += .5;
                            }
                        }

                        $currentRiskMaxScore += 2;
                        $futureRiskMaxScore += 2;
                    }
                }
            }

            return $row;
        });
    }

    /**
     * @param string $riskMethod
     * @param Entity $labOrderEntity
     * @param int $currentRiskScore
     * @param int $currentRiskMaxScore
     * @param int $futureRiskScore
     * @param int $futureRiskMaxScore
     * @param $foundTsiMedicationIds
     * @param $foundDrugIds
     */
    public function calculateRiskScore(
        $riskMethod,
        \Cake\ORM\Entity $labOrderEntity,
        &$currentRiskScore,
        &$currentRiskMaxScore,
        &$futureRiskScore,
        &$futureRiskMaxScore,
        $foundTsiMedicationIds,
        $foundDrugIds
    ) {
        if ($riskMethod === 'scoreByDrug') {
            $count = count($foundDrugIds);
        } else if ($riskMethod === 'scoreByMedication') {
            $count = count($foundTsiMedicationIds);
        }

        if ($count <= 5) {
            $currentRiskScore += 1;
            $futureRiskScore += 1;
            $currentRiskMaxScore += 1;
            $futureRiskMaxScore += 1;
        } else if ($count >=6 && $count <= 10) {
            $currentRiskScore += 3;
            $currentRiskMaxScore += 3;
            $futureRiskScore += 3;
            $futureRiskMaxScore +=3;
        } else if ($count >= 11 && $count <= 15) {
            $currentRiskScore += 5;
            $currentRiskMaxScore += 5;
            $futureRiskScore += 5;
            $futureRiskMaxScore +=5;
        } else if ($count >= 16 && $count <= 20) {
            $currentRiskScore += 7;
            $currentRiskMaxScore += 7;
            $futureRiskScore += 7;
            $futureRiskMaxScore +=7;
        } else if ($count > 20) {
            $currentRiskScore += 10;
            $currentRiskMaxScore += 10;
            $futureRiskScore += 10;
            $futureRiskMaxScore +=10;
        }

        $providerCount = count($labOrderEntity->providers);

        $physicianProviders = (new Collection($labOrderEntity->providers))->filter(function($provider) {
            return $provider['is_primary_care_provider'] === true && $provider['is_ordering_physician'] === true;
        })->toArray();
        $providerCount = count($physicianProviders);

        if ($providerCount == 1) {
            $currentRiskScore += 0;
            $currentRiskMaxScore += 0;
            $futureRiskScore += 0;
            $futureRiskMaxScore +=0;
        } else if ($providerCount >= 2 && $providerCount <= 3) {
            $currentRiskScore += 3;
            $currentRiskMaxScore += 3;
            $futureRiskScore += 3;
            $futureRiskMaxScore +=3;
        } else if ($providerCount >= 4 && $providerCount <= 6) {
            $currentRiskScore += 7;
            $currentRiskMaxScore += 7;
            $futureRiskScore += 7;
            $futureRiskMaxScore +=7;
        } else if ($providerCount > 6) {
            $currentRiskScore += 10;
            $currentRiskMaxScore += 10;
            $futureRiskScore += 10;
            $futureRiskMaxScore +=10;
        }
    }

    /**
     * Check if the lab order was processed with the new post-processing map code
     * Because if it wasn't we need to reprocess it
     * TODO: add a lab_orders.is_map_ready and set to true if map notes count > 0?
     *
     * @param int $labOrderId
     */
    public function isMapReady(int $labOrderId): bool
    {
        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Medications.Drug.BrandNames',
                'Medications.Medication',
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider',
                'Reports'
            ]
        ]);

        if (!$labOrderEntity || empty($labOrderEntity->reports)) {
            return false;
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;

        $reportAdditionalTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportAdditional');
        $additionalEntity = $reportAdditionalTable
            ->findOrCreate([
                'lab_order_report_id' => $labOrderReportId
            ]);

        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->first();

        $tsiId = 2;
        if (
            $latestReportEntity &&
            isset($labOrderEntity->provider) &&
            isset($labOrderEntity->provider->health_care_client) &&
            isset($labOrderEntity->provider->health_care_client->pharmacogenomic_provider) &&
            isset($labOrderEntity->provider->health_care_client->pharmacogenomic_provider->id) &&
            $labOrderEntity->provider->health_care_client->pharmacogenomic_provider->id == $tsiId // TODO: change this
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param int $labOrderId
     * @param string $mapType
     */
    public function hasMap(int $labOrderId = null, string $mapType)
    {
        if (self::isValidMapType($mapType) === false) {
            throw new \Exception('Invalid MAP type');
        }

        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Medications.Drug.BrandNames',
                'Medications.Medication',
                'Provider.HealthCareClient.Lab',
                'Reports'
            ]
        ]);

        if (!$labOrderEntity || empty($labOrderEntity->reports)) {
            return false;
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;

        $mapFilename = $labOrderReportId . '_map_' . $mapType . '.pdf';

        $dir = ROOT . DS .  'data' . DS . 'lab_orders' . DS . 'maps';

        $fullFilename = $dir . DS . $mapFilename;

        if (!is_file($fullFilename)) {
            return false;
        } else if (!is_readable($fullFilename)) {
            return false;
        }

        return true;
    }

    /**
     * @param int $labOrderId
     * @param string $mapType
     *
     */
    public function downloadMap(int $labOrderId = null, string $mapType)
    {
        if (self::isValidMapType($mapType) === false) {
            throw new \Exception('Invalid MAP type');
        }

        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Medications.Drug.BrandNames',
                'Medications.Medication',
                'Provider.HealthCareClient.Lab',
                'Reports'
            ]
        ]);

        if (!$labOrderEntity || empty($labOrderEntity->reports)) {
            return false;
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;

        $mapFilename = $labOrderReportId . '_map_' . $mapType . '.pdf';

        $dir = ROOT . DS .  'data' . DS . 'lab_orders' . DS . 'maps';

        $fullFilename = $dir . DS . $mapFilename;

        if (!is_file($fullFilename)) {
            return false;
        }

        $this->displayPdf($fullFilename);
        exit;
    }

    /**
     * @param int $labOrderId The lab order id
     * @param string $mapType Patient or provider
     *
     * @return bool
     */
    public function generateMap(int $labOrderId = null, string $mapType): bool
    {
        if (self::isValidMapType($mapType) === false) {
            throw new \Exception('Invalid MAP type');
        }

        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Medications.Drug.BrandNames',
                'Medications.Medication',
                'Provider.HealthCareClient.Lab'
            ]
        ]);

        if (!$labOrderEntity) {
            return false;
        }

        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderEntity->id
        ])->firstOrFail();

        $additionalEntity = $latestReportEntity->report_additional;

        $labOrderReportId = $latestReportEntity->id;

        // set the full domain otherwise resources/css won't be pulled if run from non-web calls (since there is no request object in those cases)
        // DO NOT REMOVE
        if (\Cake\Core\Configure::read('App.fullBaseUrl') === false) {
            $fullBaseUrl = Lib24watchSettingsTable::readSettingStatic('fullBaseUrl', 'Lib24watch', null);
            if (is_null($fullBaseUrl)) {
                throw new \Exception('24W setting fullBaseUrl must be defined');
            }

            \Cake\Core\Configure::write('App.fullBaseUrl', 'http://' . $fullBaseUrl);
        }

        $tsiMedicationIds = (new Collection($labOrderEntity->medications))->extract('medication.tsi_medication_id')->toArray();
        $tsiMedicationIds = array_filter(array_unique($tsiMedicationIds));
        $foundTsiMedicationIds = [];
        $foundDrugIds = [];

        $resultSet = [];
        $currentRiskScore = 0;
        $currentRiskMaxScore = 0;
        $futureRiskScore = 0;
        $futureRiskMaxScore = 0;
        $riskMethod = 'scoreByDrug'; // drug or medication
        $riskMethod = 'scoreByMedication'; // drug or medication

        if (!empty($tsiMedicationIds)) {
            $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');
            $resultSet = $reportResultsTable->find('filteredReportResults', [
                'lab_order_id' => $labOrderId,
                'lab_order_report_id' => $labOrderReportId
            ])
            ->formatResults(function (\Cake\Collection\CollectionInterface $results) use (
                &$currentRiskScore,
                &$currentRiskMaxScore,
                &$futureRiskScore,
                &$futureRiskMaxScore,
                $riskMethod,
                &$foundTsiMedicationIds,
                &$foundDrugIds
            ) {
                return call_user_func_array(
                    [
                        $this,
                        $riskMethod
                    ], [
                        $results,
                        &$currentRiskScore,
                        &$currentRiskMaxScore,
                        &$futureRiskScore,
                        &$futureRiskMaxScore,
                        &$foundTsiMedicationIds,
                        &$foundDrugIds
                    ]
                );
            });

            // this is for the drug map lists at the top
            // 9.28.2020 - changed this to group by user-specified severity (required for included rows)
            $resultSetMedications = $resultSet->groupBy('result_map_note.severity_level.slug')->toArray();

            // this is for the main listing
            $resultSet = $resultSet->groupBy('id')->toArray();
        }

        $this->calculateRiskScore(
            $riskMethod,
            $labOrderEntity,
            $currentRiskScore,
            $currentRiskMaxScore,
            $futureRiskScore,
            $futureRiskMaxScore,
            $foundTsiMedicationIds,
            $foundDrugIds
        );

        $currentPercentage = (($currentRiskScore/$currentRiskMaxScore)*100);
        $futurePercentage = (($futureRiskScore/$futureRiskMaxScore)*100);

        $currentRiskLevel = $this->getRiskLevel($currentPercentage);
        $futureRiskLevel = $this->getRiskLevel($futurePercentage);

        $patientFirstName = $labOrderEntity->patient->first_name;
        $patientLastName = $labOrderEntity->patient->last_name;
        $patientDob = $labOrderEntity->patient->date_of_birth_formatted;
        $patientGender = $labOrderEntity->patient->sex->title;
        $clinician = $labOrderEntity->clinician_formatted;

        $providerName = $labOrderEntity->provider->user->full_name;
        $providerPhone = $labOrderEntity->provider->phone_formatted;
        $providerLabel = 'Provider';
        $providerTypesList = TableRegistry::getTableLocator()->get('Providers.ProviderTypes')
            ->find('dropdown');

        if (
            isset($labOrderEntity->provider) &&
            isset($labOrderEntity->provider->provider_type_id) &&
            isset($providerTypesList) &&
            !empty($providerTypesList)
        ) {
            $providerLabel = $providerTypesList[$labOrderEntity->provider->provider_type_id];

            /*
            // if "Other", set to the "Other" label
            if ($labOrderEntity->provider->provider_type_id == 5) {
                $providerLabel = $labOrderEntity->provider->provider_type_other;
            } else {
                $providerLabel = $providerTypesList[$labOrderEntity->provider->provider_type_id];
            }*/
        }

        $physicianLabel = Lib24watchSettingsTable::readSettingStatic('physicianLabel', 'LabOrders', 'Ordering Physician');

        $reportReleasedDate = date('m/d/Y');

        $sampleId = $labOrderEntity->sample_id;
        $deIdentified = false;

        if ($deIdentified) {
            $patientFirstName = substr_replace($patientFirstName, str_repeat("X", (strlen($patientFirstName) - 1)), 1, (strlen($patientFirstName) - 1));
            $patientLastName = substr_replace($patientLastName, str_repeat("X", (strlen($patientLastName) - 1)), 1, (strlen($patientLastName) - 1));
        }

        $dateFormat = '%m/%d/%Y';

        if (!is_null($labOrderEntity->collection_date)) {
            $collectionDate = lib24watchDate($dateFormat, $labOrderEntity->collection_date, +5);
        } else {
            $collectionDate = 'N/A';
        }

        if (!is_null($labOrderEntity->sample_processed_date)) {
            $sampleProcessedDate = lib24watchDate($dateFormat, $labOrderEntity->sample_processed_date, +5);
        } else {
            $sampleProcessedDate = 'N/A';
        }

        $key = 'map_' . $mapType . '_copy';
        $contentRegionContent = TableRegistry::getTableLocator()->get('ContentRegions.ContentRegions')
            ->find()
            ->where([
                'region' => $key
            ])
            ->firstOrFail();

        $loadedContentRegions = TableRegistry::getTableLocator()->get('ContentRegions.ContentRegions')
            ->find()
            ->where([
                'region IN' => ['map_footer_image', 'map_footer_content', 'map_confidentiality_content']
            ]);

        $fields = [
            'loadedContentRegions',
            'providerLabel',
            'physicianLabel',
            'mapFooterImage',
            'currentRiskLevel',
            'futureRiskScore',
            'futureRiskLevel',
            'currentPercentage',
            'futurePercentage',
            'additionalEntity',
            'currentRiskScore',
            'contentRegionContent',
            'patientFirstName',
            'patientLastName',
            'patientDob',
            'mapType',
            'patientGender',
            'sampleId',
            'providerName',
            'providerPhone',
            'clinician',
            'collectionDate',
            'sampleProcessedDate',
            'reportReleasedDate',
            'resultSet',
            'resultSetMedications',
        ];

        // since its not being passed to the template
        $headerContentRegions = TableRegistry::getTableLocator()->get('ContentRegions.ContentRegions')
            ->find()
            ->where([
                'region' => 'rxg_logo'
            ]);

        // Establish a header specific view that is scoped to be able to render a PDF header independent of everything else
        //$headerView = new View($this->request, $this->response);
        $headerView = new View();
        $headerView->loadHelper('ContentRegions.ContentRegionLoader');
        $headerView->loadHelper('Lib24watch.Less');
        // Need to set loadedContentRegions on the view level to ensure they can be properly referenced by ContentRegionLoader
        $headerView->set('loadedContentRegions', $headerContentRegions ?? null);

        // Establish markup that will be used for the PDF's header
        $headerHtml = $headerView->element(
            'LabOrders.map_header',
            compact($fields)
        );

        // Establish a temporary file for $headerHtml. It needs to be an html file for PDF generation to work
        $headerFileName = $labOrderReportId . '_' . (new \Cake\I18n\FrozenTime())->format('YmdHis') . '.html';
	$dir = WWW_ROOT . 'html' . DS . 'tmp';

        if (!is_dir($dir) || !is_writable($dir)) {
            throw new \Exception($dir . ' is not writable');
        }

        $headerFilePath = $dir. DS . $headerFileName;
        $headerFile = fopen($headerFilePath, 'w');

        if (!is_resource($headerFile)) {
            throw new \Exception('Could not generate MAP header');
        }

        fwrite(
            $headerFile,
            $headerHtml
        );

        fclose($headerFile);

        $headerUrl = \Cake\Routing\Router::url('/html/tmp/' . $headerFileName, true);

        // start footer code
        $footerView = new View();
        $footerView->loadHelper('ContentRegions.ContentRegionLoader');
        $footerView->loadHelper('Lib24watch.Less');
        // Need to set loadedContentRegions on the view level to ensure they can be properly referenced by ContentRegionLoader
        $footerView->set('loadedContentRegions', $loadedContentRegions ?? null);

        // Establish markup that will be used for the PDF's header
        $footerHtml = $footerView->element(
            'LabOrders.map_footer',
            compact($fields)
        );

        // Establish a temporary file for $headerHtml. It needs to be an html file for PDF generation to work
        $footerFileName = $labOrderReportId . '_footer_' . (new \Cake\I18n\FrozenTime())->format('YmdHis') . '.html';
	$dir = WWW_ROOT . 'html' . DS . 'tmp';

        if (!is_dir($dir) || !is_writable($dir)) {
            throw new \Exception($dir . ' is not writable');
        }

        $footerFilePath = $dir. DS . $footerFileName;
        $footerFile = fopen($footerFilePath, 'w');

        if (!is_resource($footerFile)) {
            throw new \Exception('Could not generate MAP footer');
        }

        fwrite(
            $footerFile,
            $footerHtml
        );

        fclose($footerFile);

        $footerUrl = \Cake\Routing\Router::url('/html/tmp/' . $footerFileName, true);
        // end footer code

        $cakePdf = new \CakePdf\Pdf\CakePdf();
        $cakePdf->engine([
            'className' => 'CakePdf.WkHtmlToPdf',
            'binary' => '/usr/local/bin/wkhtmltopdf',
            'pageSize' => 'Letter',
            'margin' => [
                'bottom' => 0,
                'left' => 0,
                'right' => 0,
                'top' => 0
            ],
            'title' => 'RXG: MAP For Lab Report #' . $labOrderReportId,
            'options' => [
                'print-media-type' => false,
                'outline' => false,
                'viewport-size' => '1024x768',
                'dpi' => (int)96,
		//'footer-right' => 'Page [page] of [topage]',
		//'footer-font-size' => '8',
		'header-html' => $headerFilePath,
		'footer-html' => $footerFilePath,
                //'header-spacing' => '-4',
            ]
        ]);

        $cakePdf->orientation('portrait');
        $cakePdf->theme('LabOrders');
        $right = 12;
        $left = 13;
        $bottom = 25;
        $bottom = 8;
        $right = 0;
        $top = 40;

        $cakePdf->margin($bottom, $left, $right, $top);
        #$cakePdf->layoutPath('Pdf');
        #$cakePdf->templatePath('Pdf');
        #$cakePdf->viewClass('CakePdf.PdfView');
        $layoutFile = 'map_layout';
        $templateFile = 'map';
        //$cakePdf->viewRender('LabOrders');
        $cakePdf->template($templateFile, $layoutFile);
        $cakePdf->helpers([
	    'Lib24watch.Less',
            'Lib24watch.Panel',
            'ContentRegions.ContentRegionLoader',
        ]);

        $cakePdf->viewVars(compact($fields));

        #$pdf = $cakePdf->output();

        #\header('Content-Type: application/pdf');
        #echo $pdf;
        #exit;

        $mapFilename = $labOrderReportId . '_map_' . $mapType . '.pdf';

        $dir = ROOT . DS .  'data' . DS . 'lab_orders' . DS . 'maps';
        if (!is_writable($dir)) {
            throw new \Exception('`maps` directory is not writable');
        }

        $fullFilename = $dir . DS . $mapFilename;

        // Or write it to file directly
        $pdf = $cakePdf->write($fullFilename);

        sleep(2);

	// VERY IMPORTANT - delete the temporarily generated file
	unlink(WWW_ROOT . 'html' . DS . 'tmp' . DS . $headerFileName);

	unlink(WWW_ROOT . 'html' . DS . 'tmp' . DS . $footerFileName);

        if ($pdf) {
            return true;
        }

        return false;
    }

    /**
     * @param int $labOrderId The lab order id
     * @bool $isWebView Is this webview?
     * @bool $useExisting Use the existing file?
     *
     */
    public function generateCoverLetter(int $labOrderId = null, bool $isWebView = false, bool $useExisting = true)
    {
        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames',
                'Reports'
            ]
        ]);

        if (!$labOrderEntity || empty($labOrderEntity->reports)) {
            return false;
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;
        $outputFilename = $labOrderReportId . '_full.pdf';
        $dir = ROOT . DS .  'data' . DS . 'lab_orders' . DS . 'reports';

        $healthCareClient = $labOrderEntity->provider->health_care_client;
        $deIdentified = $healthCareClient->get('deidentified');

        $patientFirstName = $labOrderEntity->patient->first_name;
        $patientLastName = $labOrderEntity->patient->last_name;
        $patientDob = $labOrderEntity->patient->date_of_birth_formatted;
        $patientGender = $labOrderEntity->patient->sex->title;
        $clinician = $labOrderEntity->clinician_formatted;

        $providerName = $labOrderEntity->provider->user->full_name;
        $providerPhone = $labOrderEntity->provider->phone_formatted;

        $sampleId = $labOrderEntity->sample_id;

        /*
         * Cover letter should be identified
        if ($deIdentified) {
            $patientFirstName = substr_replace($patientFirstName, str_repeat("X", (strlen($patientFirstName) - 1)), 1, (strlen($patientFirstName) - 1));
            $patientLastName = substr_replace($patientLastName, str_repeat("X", (strlen($patientLastName) - 1)), 1, (strlen($patientLastName) - 1));
        }
        */

        $dateFormat = '%m/%d/%Y';

        if (!is_null($labOrderEntity->collection_date)) {
            $collectionDate = lib24watchDate($dateFormat, $labOrderEntity->collection_date, +5);
        } else {
            $collectionDate = 'N/A';
        }

        if (!is_null($labOrderEntity->sample_processed_date)) {
            $sampleProcessedDate = lib24watchDate($dateFormat, $labOrderEntity->sample_processed_date, +5);
        } else {
            $sampleProcessedDate = 'N/A';
        }

        $reportReleasedDate = date('m/d/Y');

        /*
        $contentRegionContent = TableRegistry::getTableLocator()->get('ContentRegions.ContentRegions')
            ->find()
            ->where([
                'region' => 'cover_letter_content'
            ])
            ->firstOrFail();
         */

        $fields = [
            #'contentRegionContent',
            'patientFirstName',
            'patientLastName',
            'patientDob',
            'patientGender',
            'sampleId',
            'providerName',
            'providerPhone',
            'clinician',
            'collectionDate',
            'sampleProcessedDate',
            'reportReleasedDate',
        ];

        /*
        $loadableContentRegions = [
            'cover_letter_content'
        ];
         */

        $cakePdf = new \CakePdf\Pdf\CakePdf();
        $cakePdf->engine([
            'className' => 'CakePdf.WkHtmlToPdf',
            'binary' => '/usr/local/bin/wkhtmltopdf',
            'pageSize' => 'Letter',
            'margin' => [
                'bottom' => 0,
                'left' => 0,
                'right' => 0,
                'top' => 0
            ],
            'title' => 'RxG: Lab Report #' . $labOrderReportId,
            'options' => [
                'print-media-type' => false,
                'outline' => false,
                #'viewport-size' => '1024x768',
                'dpi' => (int)96,
            ]
        ]);

        $cakePdf->orientation('portrait');
        $cakePdf->theme('LabOrders');
        $cakePdf->margin(5, 19, 18, 45);
        #$cakePdf->layoutPath('Pdf');
        #$cakePdf->templatePath('Pdf');
        #$cakePdf->viewClass('CakePdf.PdfView');
        $layoutFile = 'cover_letter_layout';
        $templateFile = 'cover_letter';
        //$cakePdf->viewRender('LabOrders');
        $cakePdf->template($templateFile, $layoutFile);
        $cakePdf->helpers([
            'Lib24watch.Panel',
            'ContentRegions.ContentRegionLoader',
            #'InspectionForms.FormBuilderGroupRenderer'
        ]);
        $cakePdf->viewVars(compact($fields));

        #\header('Content-type: application/pdf');
        $pdf = $cakePdf->output();
        #echo $pdf;
        #exit;

        $coverletterFilename = $labOrderReportId . '_coverletter.pdf';

        if (!is_writable($dir)) {
            throw new \Exception('Directory is not writable');
        }

        $fullFilename = $dir . DS . $coverletterFilename;

        // Or write it to file directly
        $pdf = $cakePdf->write($fullFilename);

        // if the coverletter was generated, generate the full pdf
        if ($pdf) {
            $chdir = chdir($dir);
            $text = 'gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=' . $outputFilename . ' ' . $coverletterFilename . ' ' . $labOrderReportId . '.pdf';
            $output = shell_exec($text);

            if (!file_exists($fullFilename) || !is_readable($fullFilename)) {
                throw new \Exception('Could not find full pdf');
            }

            return true;
        }
    }

    /**
     * Render pdf for a given lab order
     *
     * @param int $labOrderId
     *
     */
    public function renderPdf(
        int $labOrderId,
        int $providerId=0
    )
    {
        if ($providerId) {
            $providerEntity = TableRegistry::getTableLocator()->get('Providers.Providers')
                ->find()
                ->where([
                    'Providers.id' => $providerId
                ])
                ->first();
        }

        $labOrderEntity = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.AssignedFeatures',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames',
                'Reports'
            ]
        ]);

        if (!$labOrderEntity || empty($labOrderEntity->reports)) {
            return false;
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;
        $outputFilename = $labOrderReportId . '_full.pdf';
        $dir = ROOT . DS .  'data' . DS . 'lab_orders' . DS . 'reports';

        // if the provider who created the lab order sees it, mark it so they don't get an email notification
        if (
            isset($providerEntity) &&
            is_null($labOrderEntity->first_seen_provider_id) &&
            $providerEntity->rxg_user_id == $labOrderEntity->provider->rxg_user_id
        ) {
            $labOrderEntity->first_seen_provider_id = $providerEntity->id;
            $labOrderEntity->enable_email_followup = false;

            if (!$this->LabOrders->save($labOrderEntity)) {
                \Cake\Log\Log::debug('Could not save first seen provider ' . $providerEntity->id);
            }
        }

        // if the fully generated one with a cover letter is available, render that
        if (is_readable($dir . DS . $outputFilename)) {
            $this->displayPdf($dir . DS . $outputFilename);
            // otherwise render the normal one
        } else {
            $this->displayPdf($dir . DS . $labOrderReportId . '.pdf');
        }

        exit;
    }

    /**
     * @param string $fullFilepath
     */
    private function displayPdf(string $fullFilepath)
    {
        if (is_readable($fullFilepath)) {
            Configure::write('debug', 0);
            \header('Content-type: application/pdf');

            echo file_get_contents($fullFilepath);
            exit;
        }
    }

    /**
     * @throws \Exception
     */
    public function checkForLabResult(int $labOrderId = null)
    {
        $labOrder = $this->getLabOrder($labOrderId);

        $healthCareClient = $labOrder->provider->health_care_client;
        $lab = new Lab($healthCareClient->lab_id);

        $connectionInformation = $healthCareClient->lab->toArray();
        $connectionInformation['submit_path'] = '';
        $connectionInformation['local_file'] = ROOT . '/data/lab_orders/results/' . $labOrder->id . '.csv';
        $connectionInformation['remote_file'] = $connectionInformation['request_path'] .
            '/' . $labOrder->sample_id . '.csv';

        try {
            $result = $lab->getConnector()->createConnection($connectionInformation)->get($connectionInformation);
        } catch (\Exception $e) {
            // couldn't get CSV
            $result = false;
            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Could not download result file from lab',
                'Failed'
            );
        }
        $parseResponse = $lab->getResponseDriver()->parseResponse($labOrder, $result);

        if ($result && $parseResponse === true) {
            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Report returned from Lab',
                'Success'
            );
        } else {
            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Report from Lab could not be processed: ' . $parseResponse['error'],
                'Failed'
            );

            $mailer = $this->getMailer('LabOrders.LabOrders');
            $mailer->send('labProcessingError', [
                $labOrder,
                $parseResponse['error'],
                'lab'
            ]);
        }
    }

    /**
     * @param int|null $labOrderId
     * @return array|bool|\Cake\Datasource\EntityInterface
     * @throws \Exception
     */
    public function createPgxReport(int $labOrderId = null)
    {
        $labOrder = $this->getLabOrder($labOrderId);

        $healthCareClient = $labOrder->provider->health_care_client;
        // after processing the return result we post to the PGx provider for this health care client
        $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

        $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();
        $payload = $pharmacogenomicProvider->getDriver()->createSubmit($labOrder);

        $result = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($pharmacogenomicConnectionInformation)
            ->post($pharmacogenomicConnectionInformation, $payload);
        if (is_array($result) && isset($result['success']) && $result['success'] == 'true') {
            $labOrder->uuid = $result['uuid'];
        } elseif ($result instanceof \SimpleXMLElement && $result->success == 'true') {
            $labOrder->uuid = $result->uuid;
        } else {
            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Submit to PgX',
                'Failed',
                (array)$result,
                $payload
            );

            $mailer = $this->getMailer('LabOrders.LabOrders');
            $mailer->send('labProcessingError', [
                $labOrder,
                (array)$result
            ]);
        }

        EventLogs::write(
            $healthCareClient->id,
            $labOrder->id,
            $labOrder->lab_order_status_id,
            'Submit to PgX',
            'Success'
        );

        $labOrder->lab_order_status_id = 4;
        $this->LabOrders->save($labOrder);
        return $labOrder;
    }

    /**
     * @param int|null $labOrderId
     * @throws \Exception
     */
    public function sendReportForQC(int $labOrderId = null)
    {
        $labOrder = $this->getLabOrder($labOrderId);
        $labOrderReportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');

        $labOrderReport = $labOrderReportsTable->find()
            ->where(['lab_order_id' => $labOrderId])
            ->order(['id' => 'DESC'])
            ->first();

        $healthCareClient = $labOrder->provider->health_care_client;

        $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/';

        if ($this->Features->hasFeature($healthCareClient->id, 'de-identified-lab-data')) {
            $labOrderReportPdfFileName .= $this->deidentifiedPrefix;
        }
        $labOrderReportPdfFileName .= $labOrderReport->id . '.pdf';

        $connectionInformation = $healthCareClient->lab->toArray();
        $connectionInformation['local_file'] = $labOrderReportPdfFileName;
        $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
        $report = file_get_contents($labOrderReportPdfFileName);
        $lab = new Lab($healthCareClient->lab_id);

        $result = $lab->getConnector()
            ->createConnection($connectionInformation)
            ->post($connectionInformation, $report);
        if ($result) {
            $labOrder->lab_order_status_id = 5;
            $this->LabOrders->save($labOrder);

            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Post Report Back to QC',
                'Success'
            );
        } else {
            EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Post Report Back to QC',
                'Fail',
                (array)$result
            );
        }
    }

    /**
     * Get a lab order by id
     *
     * @param int|null $labOrderId
     * @return array|bool|\Cake\Datasource\EntityInterface
     */
    protected function getLabOrder(int $labOrderId = null)
    {
        if ($labOrderId) {
            return $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'Patient.Sex',
                    'Patient.Ethnicity',
                    'Patient.Medications',
                    'Medications',
                    'Medications.Drug',
                    'Patient.Snps',
                    'Patient.GeneCnvs',
                    'Patient.Lifestyles',
                    'Providers',
                    'Provider.HealthCareClient.Lab',
                    'Provider.HealthCareClient.PharmacogenomicProvider'
                ]
            ]);
        }

        return false;
    }

    /**
     * @param int $labOrderId The lab order id
     *
     * @return void
     *
     */
    public static function fillInMedications(int $labOrderId = 0) {
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $labOrder = $labOrdersTable->find()
            ->contain([
                'Medications',
                'Medications.Drug',
            ])
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->firstOrFail();

        if (!empty($labOrder->medications)) {
            // temporary solution to bind old orders to have lab_order_medications.medication_ids since we want to save these (since these can be updated and we want a historical reference)
            foreach ($labOrder->medications as $medication) {
                if (!is_null($medication->medication_id) || !is_null($medication->rxcui_id)) {
                    continue;
                }

                $labOrderMedicationsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications');
                $labOrderMedicationEntity = $labOrderMedicationsTable
                    ->find()
                    ->where([
                        'LabOrderMedications.id' => $medication->id
                    ])
                    ->first();

                if ($labOrderMedicationEntity) {
                    $saved = false;
                    if (is_null($medication->medication_id) & isset($medication['drug']) && (int)$medication['drug']['medication_id'] > 0) {
                        $labOrderMedicationEntity->medication_id = (int)$medication['drug']['medication_id'];
                        $entity = $labOrderMedicationsTable->save($labOrderMedicationEntity);
                        if ($entity) {
                            $saved = true;
                        }
                    }

                    if (!$saved && is_null($medication->rxcui_id) && is_null($medication->rxcui_id) && isset($medication['drug']) && (int)($medication['drug']['rxcui_id'] > 0)) {
                        $labOrderMedicationEntity->rxcui_id = (int)$medication['drug']['rxcui_id'];
                        $entity = $labOrderMedicationsTable->save($labOrderMedicationEntity);
                        if ($entity) {
                            $saved = true;
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * Generate "note" records which are clones of map results in order to log
     *
     * @param int $labOrderReportId
     *
     */
    public static function generateNoteEntitiesForResults(int $labOrderReportId = 0)
    {
        $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');
        $labOrderReportEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports')
            ->get($labOrderReportId);

        $reportResults = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $labOrderReportId
            ])
            ->notMatching('ResultMapNotes');

        if ($reportResults->count() > 0) {
            $mapNotes = [];
            $mapNotesTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMapNotes');
            foreach ($reportResults as $result) {
                $note = [
                    'lab_order_report_result_id' => $result['id'],
                    'detail' => $result['tsi_detail']
                ];

                $mapNotes[] = $mapNotesTable->newEntity($note);
            }

            if (count($mapNotes) > 0) {
                $saved = $mapNotesTable->saveMany($mapNotes);

                return $saved;
            }
        }

        return false;
    }

    /**
     *
     * Generate "note" records which are clones of map results in order to log
     *
     * @param int $labOrderReportId
     *
     */
    public static function generateLabOrderNoteEntitiesForResults(int $labOrderReportId = 0)
    {
        $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');
        $labOrderReportEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports')
            ->get($labOrderReportId);

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->contain([
                'Medications.Medication',
                #'Reports'
            ])
            ->where([
                'LabOrders.id' => $labOrderReportEntity->lab_order_id
            ])
            ->first();

        if (is_null($labOrderEntity) && empty($labOrderEntity->medications)) {
            return false;
        }

        debug($labOrderReportId);

        $matchingReportResults = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $labOrderReportId
            ])
            ->contain(
                [
                    'ResultMapNotes',
                    'ResultMedications'
                ]
            )->matching('ResultMedications', function($q) use ($labOrderEntity) {
                $labOrderMedicationIds = (new Collection($labOrderEntity->medications))->extract('id')->toArray();
                return $q->andWhere([
                    'ResultMedications.lab_order_medication_id IS' => null,
                ]);
            });

        $excludeReportResults = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $labOrderReportId,
                'is_lab_order_medication' => true,
            ])
            ->contain([
                'ResultMedications'
            ])->matching('ResultMedications', function($q) use ($labOrderEntity) {
                if (count($labOrderEntity->medications) > 0) {
                    $labOrderMedicationIds = (new Collection($labOrderEntity->medications))->extract('id')->toArray();
                } else {
                    $labOrderMedicationIds = [0];
                }
                return $q->andWhere([
                    'lab_order_medication_id IN' => $labOrderMedicationIds,
                ]);
            });

        // Prevent reinserting the same lab order medications by pulling report results that have a lab order medication bound
        if ($excludeReportResults->count() > 0) {
            $excludeLabOrderMedicationIds = $excludeReportResults->extract('result_medications.0.lab_order_medication_id')->toArray();

            foreach ($labOrderEntity->medications as $index => $labOrderMedication) {
                if (in_array($labOrderMedication->id, $excludeLabOrderMedicationIds)) {
                    unset($labOrderEntity->medications[$index]);
                }
            }
        }

        $excludeOriginalReportResults = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $labOrderReportId,
                'is_lab_order_medication' => false,
                'is_relevant_original' => true
            ])
            ->contain([
                'ResultMedications'
            ])->matching('ResultMedications');

        // Prevent reinserting original lab order result medications
        if ($excludeOriginalReportResults->count() > 0) {
            $excludeLabOrderMedicationIds = $excludeOriginalReportResults->extract('result_medications.0.medication_id')->toArray();

            foreach ($labOrderEntity->medications as $index => $labOrderMedication) {
                if (in_array($labOrderMedication->medication_id, $excludeLabOrderMedicationIds)) {
                    unset($labOrderEntity->medications[$index]);
                }
            }
        }

        // ResultMedications are really a hasOne, not a hasMany
        $medicationIds = $matchingReportResults->filter(function($row) {
            return (bool)$row->is_relevant === false;
        })->indexBy('id')->extract('result_medications.0.medication_id')->toArray();

        $labOrderMedicationIds = (new Collection($labOrderEntity->medications))->extract('medication_id')->toArray();

        if (!empty($labOrderMedicationIds)) {
            $labOrderMedicationIds = array_filter($labOrderMedicationIds);
        }

        debug($labOrderMedicationIds);

        if (!empty($labOrderEntity->medications)) {
            $reportResultEntities = [];
            $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');

            foreach ($labOrderEntity->medications as $id => $medication) {
                $reportResult = [
                    'lab_order_report_id' => $labOrderReportId,
                    'is_relevant' => true,
                    'is_lab_order_medication' => true,
                ];

                $reportResult['result_medications'] = [
                    [
                        'lab_order_medication_id' => $medication->id,
                        'medication_id' => $medication->medication_id,
                        'tsi_medication_id' => $medication->tsi_medication_id ?? null
                    ]
                ];

                $reportResult['result_map_note'] = [
                    [
                        'is_included' => 1
                    ]
                ];

                $reportResultEntity = $reportResultsTable->newEntity($reportResult);

                $reportResultsTable->save($reportResultEntity, [
                    'associated' => [
                        'ResultMapNotes',
                        'ResultMedications'
                    ]
                ]);
            }
        }

        return false;
    }

    /**
     *
     * Get the risk level based on dividing up the graphic in 3 risk areas
     * TODO: replace this with a proper spider chart eventually
     *
     * @param int $percentage
     *
     * @return null|string
     */
    protected function getRiskLevel($percentage) {
        $riskLevel = null;

        if ($percentage >= 100 && $percentage <= 75) {
            $riskLevel = 'high';
        } else if ($percentage <= 74 && $percentage >= 34) {
            $riskLevel = 'moderate';
        } else if ($percentage <= 33) {
            $riskLevel = 'low';
        }

        return $riskLevel;
    }

    /**
     * Repull a lab report
     *
     * @param int $labOrderId
     *
     */
    public function getPgxReport(
        int $labOrderId = 0,
        $options = [],
        string $environment = ''
    )
    {
        \Cake\Log\Log::debug('Rerunning report!!');

        $defaults = [
            'updateStatus' => false,
            'logEvent' => true,
            'generateCoverLetter' => false
        ];

        $options = array_merge($defaults, $options);

        $labOrder = $this->getLabOrder($labOrderId);

        if ($labOrder === false) {
            throw new \Exception('Unable to find lab order');
        }

        if ($environment === '') {
            throw new \Exception('Environment must be passed');
        }

        // TODO: check if this overrides
        self::fillInMedications($labOrderId);

        $healthCareClient = $labOrder->provider->health_care_client;

        // get the reports, save and post back to Lab (if applicable)
        $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();

        $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

        $compCode = 'CRESTAR_COMP';
        if ($healthCareClient->lab->panel_id) {
            $compCode = $healthCareClient->lab->panel_id;
        }

        $useNewMedications = false;
        $payload = $pharmacogenomicProvider->getDriver()->createReport($labOrder, $useNewMedications, $compCode);

        $pharmacogenomicConnectionInformation['headers'] = [
            'Accept' => 'application/json',
            'Cache-Control' => 'no-cache',
        ];
        $pharmacogenomicConnectionInformation['timeout'] = 60*6; // If (useNewMedications is true), PDF report will be embedded in the response. If useNewMedications=true, response time may be up to 3 minutes while a new report is generated.
        $pharmacogenomicConnectionInformation['submit_path'] = $pharmacogenomicConnectionInformation['request_path'];

        $report = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($pharmacogenomicConnectionInformation)
            ->post($pharmacogenomicConnectionInformation, $payload);

        $validRequest = Translational::validateBundleByType('message', $report);

        $labOrderReportId = 0;

        if ($validRequest) {
            $labOrderReportId = $pharmacogenomicProvider->getDriver()->parseReport($labOrder->id, $report);

            if ((bool)$options['logEvent']) {
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Lab Report Reparsed',
                    'Success'
                );
            }

            $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';

            $diagnosticReport = Translational::getResource($report, 'DiagnosticReport');

            if ($diagnosticReport) {
                try {
                    Translational::savePdf($labOrderReportPdfFileName, $diagnosticReport['resource']['presentedForm'][0] ?? []);

                    if ((bool)$options['logEvent']) {
                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Lab Report PDF Redownloaded',
                            'Success'
                        );
                    }

                } catch (\Exception $e) {
                    var_dump($e->getMessage());

                    if ((bool)$options['logEvent']) {
                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Lab Report PDF Failed to downloaded',
                            'Error'
                        );
                    }
                }

                // generate cover letter
                if ((bool)$options['generateCoverLetter'] && is_readable($labOrderReportPdfFileName)) {
                    $isWebView = false;
                    $useExisting = false;

                    $this->generateCoverLetter($labOrder->id, $isWebView, $useExisting);
                } else if (!is_readable($labOrderReportPdfFileName)) {
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Lab Report Cover Letter Failed to generate',
                        'Error'
                    );
                }
            }
        } else {
            if ((bool)$options['logEvent']) {
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Unable to retrieve lab report',
                    'Error',
                    (array)$report
                );
            }
        }

        // post_report_back == true/false
        if ($environment === 'production' && $validRequest && $healthCareClient->lab->post_report_back && $labOrderReportId) {
            $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
            $connectionInformation = $healthCareClient->lab->toArray();
            $connectionInformation['local_file'] = $labOrderReportPdfFileName;
            $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
            $report = file_get_contents($labOrderReportPdfFileName);
            $lab = new Lab($healthCareClient->lab_id);
            $result = $lab->getConnector()
                ->createConnection($connectionInformation)
                ->post($connectionInformation, $report);
            if ($result) {
                $labOrder->lab_order_status_id = 5;
                $this->LabOrders->save($labOrder);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Post Report Back to Lab for QC',
                    'Success'
                );
            } else {
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Post Report Back to Lab for QC',
                    'Fail',
                    (array)$result
                );
            }
        }

        return $labOrderReportId;
    }


    /**
     * Enable report ready emails for this lab order
     *
     * @param \Cake\ORM\Entity $labOrder
     *
     * @return void
     *
     */
    public function triggerReportReadyEmails($labOrder) {
        // if the email followup is already disabled, ignore
        if (isset($labOrder->enable_email_followup) && (bool)$labOrder->enable_email_followup == false) {
            $labOrder->enable_email_followup = true;
            $labOrder->enable_email_followup_start_date = new \Cake\I18n\FrozenDate;
            $labOrder->last_sent_sequence_date = new \Cake\I18n\FrozenDate;

            // reset if the provider already opened this
            $labOrder->first_seen_provider_id = null;

            $this->LabOrders->save($labOrder);
        }
    }

    /**
     * Pull the reports for a lab order
     *
     * @param Entity $healthCareClient
     * @param Entity $pharmacogenomicProvider
     * @param Entity $labOrder
     * @param string $compCode
     *
     * @return int
     *
     */
    protected function getReports($healthCareClient, $pharmacogenomicProvider, $labOrder, string $compCode)
    {
        // get the reports, save and post back to Lab (if applicable)
        $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();

        $useNewMedications = false;
        if (!empty($labOrder->medications)) {
            $useNewMedications = true;
        }

        $payload = $pharmacogenomicProvider->getDriver()->createReport($labOrder, $useNewMedications, $compCode);

        $pharmacogenomicConnectionInformation['headers'] = [
            'Accept' => 'application/json',
        ];
        $pharmacogenomicConnectionInformation['submit_path'] = $pharmacogenomicConnectionInformation['request_path'];
        $pharmacogenomicConnectionInformation['timeout'] = 60*6; // If (useNewMedications is true), PDF report will be embedded in the response. If useNewMedications=true, response time may be up to 3 minutes while a new report is generated.

        $report = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($pharmacogenomicConnectionInformation)
            ->post($pharmacogenomicConnectionInformation, $payload);

        $validRequest = Translational::validateBundleByType('message', $report);

        debug($report);
        debug($validRequest);

        $labOrderReportId = 0;

        if ($validRequest) {
            $labOrderReportId = $pharmacogenomicProvider->getDriver()->parseReport($labOrder->id, $report);

            file_put_contents(ROOT . '/data/report_raw_' . $labOrderReportId . '.txt', print_r($report, true));

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Lab Report Parsed',
                'Success'
            );

            $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';

            $diagnosticReport = Translational::getResource($report, 'DiagnosticReport');

            $labOrdersInstance = new LabOrders;

            if ($diagnosticReport) {
                try {
                    Translational::savePdf($labOrderReportPdfFileName, $diagnosticReport['resource']['presentedForm'][0] ?? []);
                    debug('pdf saved');
		    \SystemManagement\Libs\EventLogs::write(
			$healthCareClient->id,
			$labOrder->id,
			$labOrder->lab_order_status_id,
			'Lab Report PDF Downloaded',
			'Success'
		    );

                    $labOrdersInstance->triggerReportReadyEmails($labOrder);

                } catch (\Exception $e) {
                    debug('pdf failed to save');
                    var_dump($e->getMessage());

		    \SystemManagement\Libs\EventLogs::write(
			$healthCareClient->id,
			$labOrder->id,
			$labOrder->lab_order_status_id,
			'Lab Report PDF Failed to downloaded',
			'Warning'
		    );
                }

                try {
                    // generate cover letter
                    if (is_readable($labOrderReportPdfFileName)) {
                        $isWebView = false;
                        $useExisting = false;

                        $generated = $labOrdersInstance->generateCoverLetter($labOrder->id, $isWebView, $useExisting);
                        debug($generated);

                        if ($generated) {
                            \SystemManagement\Libs\EventLogs::write(
                                $healthCareClient->id,
                                $labOrder->id,
                                $labOrder->lab_order_status_id,
                                'Lab Cover Letter Generated',
                                'Success'
                            );
                        }
                    }
                } catch (\Exception $e) {
                    var_dump('couldnt generate cover letter');
		    \SystemManagement\Libs\EventLogs::write(
			$healthCareClient->id,
			$labOrder->id,
			$labOrder->lab_order_status_id,
			'Lab Cover Letter failed to generate',
			'Warning'
		    );
                }
            }

        } else {
            #file_put_contents(ROOT . '/data/last_failed_report_response.txt', print_r($report, true));
            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Unable to retrieve lab report',
                'Error',
                (array)$report
            );

            $mailer = $this->getMailer('LabOrders.LabOrders');
            $mailer->send('labProcessingError', [
                $labOrder,
                'Unable to retrieve lab report'
            ]);
        }

        return $labOrderReportId;
    }

    public function processCovidLabOrders(
        array $labOrderIds = [],
        $options,
        string $environment = ''
    ) {
        $defaults = [
            'io' => false,
            'logEvent' => true,
            'generateCoverLetter' => false
        ];

        $options = array_merge($defaults, $options);

        if (!empty($labOrderIds)) {
            $where = ['LabOrders.id IN' => $labOrderIds];
        } else {
            $where = ['lab_order_status_id IN' => [3, 8]];
        }

        $lib24watchLogsTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchLogs');
        $historyTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderHistory');

        $lib24watchLogsTable->write(null, 'CheckForResults Shell', 'CheckForResults: initialized', 'Labs');

        $where[] = [
            'lab_order_type_id' => $this->LabOrders::LAB_ORDER_COVID_ID
        ];

        $labOrders = $this->LabOrders->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Patient.Snps',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.Lab',
            ])
            ->where($where);

        if (isset($options['io']) && $options['io']) {
            $options['io']->out('Processing ' . count($labOrders->toArray()) . ' records');
        }

        $processCount = [];
        foreach ($labOrders as $labOrder) {
            if (isset($options['io']) && $options['io']) {
                $options['io']->out('Processing lab order #' . $labOrder->id);
            }
            $healthCareClient = $labOrder->provider->health_care_client;

            $lab = new Lab($healthCareClient->lab_id);

            if (!isset($labOrder->sample_id) || is_null($labOrder->sample_id) || mb_strlen($labOrder->sample_id) === 0) {
                throw new \Exception('Sample id must be specified');
            }

            $connectionInformation = $healthCareClient->lab->toArray();
            $connectionInformation['submit_path'] = '';
            $connectionInformation['local_file'] = ROOT . '/data/lab_orders/results/' . $labOrder->id . '.csv';
            $connectionInformation['remote_file'] = $connectionInformation['request_path'] .
                '/' . $labOrder->sample_id . '.csv';

            if ($environment === 'production') {
                try {
                    $result = $lab->getConnector()->createConnection($connectionInformation)->get($connectionInformation);
                } catch (\Exception $e) {
                    // couldn't get CSV
                    $result = false;
                }
            } else {
                $result = file_get_contents($connectionInformation['local_file']);
            }

            if ($result) {
                $labOrder->sample_processed_date = new \Cake\I18n\FrozenDate;

                if (!$this->LabOrders->save($labOrder)) {
                    throw new \Exception('Could not save sample processed date');
                }

                $base64_csv = base64_encode($result);
                $lines = explode(PHP_EOL, $result);
                $patientGeneCnvsTable = TableRegistry::getTableLocator()->get('Patients.PatientGeneCnvs');
                $patientSnpsTable = TableRegistry::getTableLocator()->get('Patients.PatientSnps');

                $driver = $patientGeneCnvsTable->getConnection()->getDriver();
                $autoQuoting = $driver->isAutoQuotingEnabled();
                $driver->enableAutoQuoting(true);

                if ($labOrder->lab_order_status_id == 8) {
                    // retested sample so clear out old data before creating new
                    $patientSnpsTable->deleteAll(['patient_id' => $labOrder->patient_id]);
                    $patientGeneCnvsTable->deleteAll(['patient_id' => $labOrder->patient_id]);
                }

                $specimenValue = null;

                foreach ($lines as $index => $line) {
                    if ($index == 0) {
                        continue;
                    }
                    $data = str_getcsv($line);
                    if (isset($data[0])) {
                        $specimenValue = $data[0];
                    }

                    if (isset($data[1])) {
                        switch ($data[1]) {
                            case 'snp':
                                $table = $patientSnpsTable;
                                $entity = $patientSnpsTable->newEntity();
                                break;
                            case 'cnv':
                                $table = $patientGeneCnvsTable;
                                $entity = $patientGeneCnvsTable->newEntity();
                                break;
                            default:
                                $entity = false;
                                break;
                        }
                        if ($entity) {
                            $entity->patient_id = $labOrder->patient_id;
                            $entity->key = $data[2];
                            $entity->value = $data[3];
                            $table->save($entity);
                        }
                    }
                }

                $driver->enableAutoQuoting($autoQuoting);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Report returned from Lab',
                    'Success'
                );
            }
        }

        $lib24watchLogsTable->write(null, 'ProcessCovidOrders Shell', 'ProcessCovidOrders: Finished', 'LabOrders');

        return $processCount;
    }

    /**
     * @param array $options
     * @param string $environment
     * @return array
     */
    public function getAvailableFiles(array $options, string $environment = '')
    {
        $availableFilesPerLab = [];

        $labsTable = TableRegistry::getTableLocator()->get('Labs.Labs');

        $labEntities = $labsTable->find()->where(['is_active' => 1]);

        foreach ($labEntities as $labEntity) {
            $availableFilesPerLab[$labEntity->id] = [];
            $lab = new Lab($labEntity->id);
            $connectionInformation = $labEntity->toArray();
            $connectionInformation['submit_path'] = '';
            $connectionInformation['remote_file'] = $connectionInformation['request_path'] . '/';
            if ($environment === 'production') {
                try {
                    $availableFilesPerLab[$labEntity->id] = $lab->getConnector()->createConnection($connectionInformation)->getList('/' . $connectionInformation['request_path'] . '/');
                } catch (\Exception $e) {
                    // couldn't get CSV
                    if (isset($options['io']) && $options['io']) {
                        $options['io']->out('Failed to get file list for lab #' . $labEntity->lab_name);
                    }
                }
            }
        }
        return $availableFilesPerLab;
    }

    /**
     * @param Arguments $args
     * @param ConsoleIo $io
     *
     */
    public function processLabOrders(
        array $labOrderIds = [],
        $options,
        string $environment = ''
    )
    {
        $defaults = [
            'io' => false,
            'logEvent' => true,
            'generateCoverLetter' => false,
            'singleRun' => false
        ];

        $options = array_merge($defaults, $options);

        if (!empty($labOrderIds)) {
            $where = [
                'is_deleted' => 0,
                'LabOrders.id IN' => $labOrderIds
            ];
        } else {
            $where = [
                'is_deleted' => 0,
                'lab_order_status_id IN' => [3, 8],
                'LabOrders.id NOT IN' => [1034,1098]
            ];
        }

        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $lib24watchLogsTable = TableRegistry::getTableLocator()->get('Lib24watch.Lib24watchLogs');
        $historyTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderHistory');

        $lib24watchLogsTable->write(null, 'CheckForResults Shell', 'CheckForResults: initialized', 'Labs');

        $where[] = [
            'lab_order_type_id' => $labOrdersTable::LAB_ORDER_PGX_ID
        ];

        $labOrders = $labOrdersTable->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Patient.Snps',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider'
            ])
            ->where($where);

        if (isset($options['io']) && $options['io']) {
            $options['io']->out('Processing ' . count($labOrders->toArray()) . ' records');
        }

        $availableFilesPerLab = $this->getAvailableFiles($options, $environment);

        $processCount = [];
        foreach ($labOrders as $labOrder) {
            $healthCareClient = $labOrder->provider->health_care_client;

            // See if result file is in the list of available results per lab before trying to download said file
            // This dramatically reduces the number of connections made in sequence
            if (!in_array($labOrder->sample_id . '.csv', $availableFilesPerLab[$healthCareClient->lab_id])) {
                if (isset($options['io']) && $options['io']) {
                    $options['io']->out('Sample result not available, skipping #' . $labOrder->id);
                }
                continue;
            }

            // TODO: This is temporary in order to process other lab orders we need to skip resolve PGX orders
//            if ($healthCareClient->lab_id == 5) {
//                if (isset($options['io']) && $options['io']) {
//                    $options['io']->out('Skipping Resolve lab order #' . $labOrder->id);
//                }
//                continue;
//            }

            if (isset($options['io']) && $options['io']) {
                $options['io']->out('Processing lab order #' . $labOrder->id);
            }

            $lab = new Lab($healthCareClient->lab_id);

            if (!isset($labOrder->sample_id) || is_null($labOrder->sample_id) || mb_strlen($labOrder->sample_id) === 0) {
                throw new \Exception('Sample id must be specified');
            }

            $connectionInformation = $healthCareClient->lab->toArray();
            $connectionInformation['submit_path'] = '';
            $connectionInformation['local_file'] = ROOT . '/data/lab_orders/results/' . $labOrder->id . '.csv';
            $connectionInformation['remote_file'] = $connectionInformation['request_path'] .
                '/' . $labOrder->sample_id . '.csv';

            if ($environment === 'production') {
                try {
                    $result = $lab->getConnector()->createConnection($connectionInformation)->get($connectionInformation);
                } catch (\Exception $e) {
                    // couldn't get CSV
                    $result = false;
                    if (isset($options['io']) && $options['io']) {
                        $options['io']->out('Failed to get file for #' . $labOrder->id);
                    }
                }
            } else {
                $result = file_get_contents($connectionInformation['local_file']);
            }

            // If Lab supplies a copy number file we need to pull that as well.
            $resultCopyFile = false;
            if ($connectionInformation['has_copy_file'] && $connectionInformation['copy_file_suffix']) {
                $connectionInformation['local_file'] = ROOT . '/data/lab_orders/results/' . $labOrder->id . $connectionInformation['copy_file_suffix'] . '.csv';
                $connectionInformation['remote_file'] = $connectionInformation['request_path'] .
                    '/' . $labOrder->sample_id . $connectionInformation['copy_file_suffix'] . '.csv';

                if ($environment === 'production') {
                    try {
                        $resultCopyFile = $lab->getConnector()->createConnection($connectionInformation)->get($connectionInformation);
                    } catch (\Exception $e) {
                        // couldn't get CSV
                        $resultCopyFile = false;
                    }
                } else {
                    $resultCopyFile = file_get_contents($connectionInformation['local_file']);
                }
            }

            if (isset($options['singleRun']) && (bool)$options['singleRun'] && !$result) {
                $message = 'Could not find csv file';
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    $message,
                    'Fail',
                    $result
                );

                $processCount[] = $message;

                break;
            }

            if ($result) {
                if (isset($options['io']) && $options['io']) {
                    $options['io']->out('Successfully download file for #' . $labOrder->id);
                }

                $labOrder->sample_processed_date = new \Cake\I18n\FrozenDate;

                if (!$labOrdersTable->save($labOrder)) {
                    throw new \Exception('Could not save sample processed date');
                }

                $base64_csv = base64_encode($result);
                $copyFileBase64_csv = base64_encode($resultCopyFile);
                $lines = explode(PHP_EOL, $result);
                $patientGeneCnvsTable = TableRegistry::getTableLocator()->get('Patients.PatientGeneCnvs');
                $patientSnpsTable = TableRegistry::getTableLocator()->get('Patients.PatientSnps');

                $driver = $patientGeneCnvsTable->getConnection()->getDriver();
                $autoQuoting = $driver->isAutoQuotingEnabled();
                $driver->enableAutoQuoting(true);

                if ($labOrder->lab_order_status_id == 8) {
                    // retested sample so clear out old data before creating new
                    $patientSnpsTable->deleteAll(['patient_id' => $labOrder->patient_id]);
                    $patientGeneCnvsTable->deleteAll(['patient_id' => $labOrder->patient_id]);
                }

                $specimenValue = null;

                foreach ($lines as $index => $line) {
                    if ($index == 0) {
                        continue;
                    }

                    $accessionIndex = 0;
                    if ($healthCareClient->lab_id == 5) {
                        $accessionIndex = 4;
                    }

                    $data = str_getcsv($line);
                    if (isset($data[$accessionIndex])) {
                        $specimenValue = $data[$accessionIndex];
                    }

                    if (isset($data[1])) {
                        switch ($data[1]) {
                            case 'snp':
                                $table = $patientSnpsTable;
                                $entity = $patientSnpsTable->newEntity();
                                break;
                            case 'cnv':
                                $table = $patientGeneCnvsTable;
                                $entity = $patientGeneCnvsTable->newEntity();
                                break;
                            default:
                                $entity = false;
                                break;
                        }
                        if ($entity) {
                            $entity->patient_id = $labOrder->patient_id;
                            $entity->key = $data[2];
                            $entity->value = $data[3];
                            $table->save($entity);
                        }
                    }
                }

                $driver->enableAutoQuoting($autoQuoting);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Report returned from Lab',
                    'Success'
                );

                // reload lab order after saving related
                $labOrder = $labOrdersTable->get($labOrder->id, [
                    'contain' => [
                        'Patient.Sex',
                        'Patient.Medications',
                        'Medications',
                        'Medications.Drug',
                        'Patient.Snps',
                        'Patient.GeneCnvs',
                        'Patient.Lifestyles',
                        'Providers',
                        'Provider.HealthCareClient.Lab',
                        'Provider.HealthCareClient.PharmacogenomicProvider'
                    ],
                ]);

                if (empty($labOrder->providers)) {
                    throw new \Exception('Missing lab order providers');
                }

                // BACKFILL MEDICATIONS *VERY IMPORTANT*
                LabOrders::fillInMedications($labOrder->id);

                // reload lab order after saving related
                $labOrder = $labOrdersTable->get($labOrder->id, [
                    'contain' => [
                        'Patient.Sex',
                        'Patient.Medications',
                        'Medications',
                        'Medications.Drug',
                        'Patient.Snps',
                        'Patient.GeneCnvs',
                        'Patient.Lifestyles',
                        'Providers',
                        'Provider.HealthCareClient.Lab',
                        'Provider.HealthCareClient.PharmacogenomicProvider'
                    ],
                ]);

                // after processing the return result we post to the PGx provider for this health care client
                $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

                $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();
                $labOrder->base64_csv = $base64_csv;
                $labOrder->copy_file_base64_csv = $copyFileBase64_csv;
                $labOrder->specimen_value = $specimenValue;
                $labOrder->pharmacogenomic_provider_id = $healthCareClient->pharmacogenomics_provider_id;

                if ($resultCopyFile) {
                    $labOrder->copy_file_base64_csv = base64_encode($resultCopyFile);
                }

                /* MEDER
                $report = print_r_reverse(file_get_contents(ROOT . '/data/report_raw_652.txt'));
                $labOrderReportId = $pharmacogenomicProvider->getDriver()->parseReport(447, $report);
                dd($labOrderReportId);
                exit;
                */

                $pharmacogenomicConnectionInformation['headers'] = [];
                $pharmacogenomicConnectionInformation['timeout'] = 60*6;

                $deIdentified = true;
                $compCode = '';

                $compCode = 'CRESTAR_COMP';
                if ($healthCareClient->lab->panel_id) {
                    $compCode = $healthCareClient->lab->panel_id;
                }

                $payload = $pharmacogenomicProvider->getDriver()->createSubmit($labOrder, $deIdentified, $compCode);
                unset($labOrder->base64_csv);
                unset($labOrder->copy_file_base64_csv);
                unset($labOrder->specimen_value);

                debug($payload);
                //file_put_contents(ROOT . DS . 'data' . DS . 'debug' . DS . 'TSI_payload_' . $labOrder->id . '.json', $payload);

                $submitted = false;


                if (isset($options['io']) && $options['io']) {
                    $options['io']->out('Preparing to submit to PGX: #' . $labOrder->id);
                }

                try {
                    $result = $pharmacogenomicProvider
                        ->getConnector()
                        ->createConnection($pharmacogenomicConnectionInformation)
                        ->post($pharmacogenomicConnectionInformation, $payload);
                } catch (\Exception $e) {
                    $result = false;
                    if (isset($options['io']) && $options['io']) {
                        $options['io']->out('Failed to submit to PGX (timeout): #' . $labOrder->id);
                    }
                }

                debug('$result');
                debug($result);

                $historyInserts = [];
                // TSI response parse
                if (is_array($result) && isset($result['entry']) && isset($result['entry'][0])) {
                    foreach ($result['entry'] as $entry) {
                        if ($entry['resource']['resourceType'] === 'ProcedureRequest') {
                            $submitted = true;
                            if (isset($labOrder->uuid) && mb_strlen($labOrder->uuid) > 0) {
                                $historyInserts[] = [
                                    'lab_order_id' => $labOrder->id,
                                    'uuid' => $labOrder->uuid
                                ];
                            }
                            $labOrder->uuid = $entry['resource']['id'];
                        }
                    }
                // Coriell?
                } else if (is_array($result) && isset($result['success']) && $result['success'] == 'true') {
                    $submitted = true;
                    $labOrder->uuid = $result['uuid'];
                // Coriell?
                } elseif ($result instanceof \SimpleXMLElement && $result->success == 'true') {
                    $submitted = true;
                    $labOrder->uuid = $result->uuid;
                } else {
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Submit to PgX',
                        'Failed',
                        (array)$result,
                        $payload
                    );
                }

                if ($submitted) {
                    $processCount[] = $labOrder->id;
                }

                debug('submitted');
                debug($submitted);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Submit to PgX',
                    'Success'
                );

                $labOrder->lab_order_status_id = 4;
                $labOrdersTable->save($labOrder);

                if (!empty($historyInserts)) {
                    foreach ($historyInserts as $insert) {
                        $historyEntity = $historyTable->newEntity($insert);

                        if (!$historyTable->save($historyEntity)) {
                            throw new \Exception('Unable to save lab order previous uuid');
                        }
                    }
                }

                # MEDER file_put_contents(ROOT . '/data/request_raw_' . $labOrder->id . '.txt', print_r($result, true));

                // I HAD TO PUT THIS SLEEP OTHERWISE IT THROWS AN INVALID RESPONSE
                // PROBABLY HITTING THE SERVER TOO OFTEN THROUGH THE SAME URL

                if ($submitted) {
                    sleep(35);
                    $labOrderReportId = $this->getReports($healthCareClient, $pharmacogenomicProvider, $labOrder, $compCode);
                    debug($labOrderReportId);

                    // post_report_back == true/false
                    if ($environment === 'production' && $healthCareClient->lab->post_report_back && $labOrderReportId) {
                        $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
                        $connectionInformation = $healthCareClient->lab->toArray();
                        $connectionInformation['local_file'] = $labOrderReportPdfFileName;
                        $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
                        $report = file_get_contents($labOrderReportPdfFileName);
                        $result = $lab->getConnector()
                            ->createConnection($connectionInformation)
                            ->post($connectionInformation, $report);
                        if ($result) {
                            $labOrder->lab_order_status_id = 5;
                            $labOrdersTable->save($labOrder);

                            \SystemManagement\Libs\EventLogs::write(
                                $healthCareClient->id,
                                $labOrder->id,
                                $labOrder->lab_order_status_id,
                                'Post Report Back to Lab for QC',
                                'Success'
                            );
                        } else {
                            \SystemManagement\Libs\EventLogs::write(
                                $healthCareClient->id,
                                $labOrder->id,
                                $labOrder->lab_order_status_id,
                                'Post Report Back to Lab for QC',
                                'Fail',
                                $result
                            );
                        }
                    }
                }
            }
        }

        $lib24watchLogsTable->write(null, 'CheckForResults Shell', 'CheckForResults: Finished', 'Labs');

        if (empty($labOrderIds)) {
            self::removeLock();
        }
        return $processCount;
    }

    /**
     * Refs #38411
     *
     * Strip HTML to avoid getting flagged by CloudNexa
     *
     * @param Entity $latestReportEntity
     * @return void
     *
     */
    public static function stripHtml(Entity $latestReportEntity, int $reportResultId = 0) {
        if (isset($latestReportEntity->report_additional) && !empty($latestReportEntity->report_additional)) {
            $fields = [
                'additional_pharmacist_notes_prescriber',
                'additional_pharmacist_notes_patient',
                'adverse_drug_reaction_notes_prescriber',
                'adverse_drug_reaction_notes_patient',
                'lifestyle_recommendation_notes_prescriber',
                'lifestyle_recommendation_notes_patient',
            ];

            foreach ($fields as $field) {
                if (isset($latestReportEntity->report_additional->{$field})) {
                    $latestReportEntity->report_additional->{$field} = strip_tags($latestReportEntity->report_additional->{$field});
                }
            }
        }

        if (isset($latestReportEntity->results) && !empty($latestReportEntity->results)) {
            foreach ($latestReportEntity->results as $index => $result) {
                // skip any unnecessary report results if one is specified
                if ($reportResultId > 0 && $result->id !== $reportResultId) {
                    continue;
                }

                if (
                    isset($result->result_map_note) &&
                    !empty($result->result_map_note) &&
                    isset($result->result_map_note->detail)
                ) {
                    // remove html tags
                    $result->result_map_note->detail = strip_tags($result->result_map_note->detail);

                    // convert html entities into literals
                    $result->result_map_note->detail = html_entity_decode($result->result_map_note->detail, ENT_QUOTES);
                }

                if (
                    isset($result->result_map_note) &&
                    !empty($result->result_map_note) &&
                    isset($result->result_map_note->prescriber_notes)
                ) {
                    // remove html tags
                    $result->result_map_note->prescriber_notes = strip_tags($result->result_map_note->prescriber_notes);

                    // convert html entities into literals
                    $result->result_map_note->prescriber_notes = html_entity_decode($result->result_map_note->prescriber_notes, ENT_QUOTES);
                }

                if (
                    isset($result->result_map_note) &&
                    !empty($result->result_map_note) &&
                    isset($result->result_map_note->patient_notes)
                ) {
                    // remove html tags
                    $result->result_map_note->patient_notes = strip_tags($result->result_map_note->patient_notes);

                    // convert html entities into literals
                    $result->result_map_note->patient_notes = html_entity_decode($result->result_map_note->patient_notes, ENT_QUOTES);
                }
            }
        }
    }


    /**
     * Check if process lab orders is running
     *
     * @param string|null $filename The file to check against
     *
     * @return bool
     */
    public static function areResultsProcessing($filename = null)
    {
        if (is_null($filename)) {
            $filename = self::LOCK_DIR . self::LOCK_FILE_ALL;
        } else {
            $filename = self::LOCK_DIR . $filename;
        }

        if (!file_exists($filename) || !is_file($filename)) {
            return false;
        }

        $lifetime = time() - filemtime($filename);

        // If we are beyond the process limit (3 hours is the default), remove the lock and run
        if ($lifetime > self::PROCESS_LIMIT_SECONDS) {
            self::removeLock();

            return false;
        }

        return true;
    }

    /**
     * Remove the lab process lock file
     */
    public static function removeLock($filename = null)
    {
        if (is_null($filename)) {
            $filename = self::LOCK_DIR . self::LOCK_FILE_ALL;
        } else {
            $filename = self::LOCK_DIR . $filename;
        }

        unlink($filename);
    }
}
