<?php

namespace LabOrders\Libs;

use Cake\Core\Configure;
use Cake\I18n\FrozenTime;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

final class ReportReadyManager
{
    use MailerAwareTrait;

    /**
     * Seq => Days Delayed
     */
    const SCHEDULE = [
        '1' => 0,
        '2' => 1,
        '3' => 2,
        '4' => 7
    ];

    public function construct() {
    }

    /**
     * @param Entity $labOrderEntity
     */
    public function processOrder(Entity $labOrderEntity)
    {
        $jobsTable = TableRegistry::getTableLocator()->get('Queue.QueuedJobs');
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');

        $sendEmailDate = $this->determineSendEmailDate(
            $labOrderEntity->last_sent_sequence,
            $labOrderEntity->last_sent_sequence_date,
            $labOrderEntity->enable_email_followup_start_date
        );

        $updates = [];
        if ($sendEmailDate) {
            $updates['last_sent_sequence'] = $labOrderEntity->last_sent_sequence+1;
            $updates['last_sent_sequence_date'] = new FrozenTime;

            if ($labOrderEntity->last_sent_sequence === 1) {
                $cadenceNotice = 'First Reminder';
            } else if ($labOrderEntity->last_sent_sequence === 2) {
                $cadenceNotice = 'Second Reminder';
            } else if ($labOrderEntity->last_sent_sequence === 3) {
                $cadenceNotice = 'Final Reminder';

                // disable
                $updates['enable_email_followup'] = false;
            }

            $mailer = $this->getMailer('Providers.Providers');
            $email = $labOrderEntity->provider->email;

            $mailer->send('reportReady', [
                $email,
                $labOrderEntity,
                $cadenceNotice ?? ''
            ]);
        }

        if (!empty($updates)) {
            $labOrderEntity = $labOrdersTable->patchEntity(
                $labOrderEntity,
                $updates
            );

            if (!$labOrdersTable->save($labOrderEntity)) {
                throw new \Exception('Nope');
            }
        }
    }

    /**
     * @param string $lastSeqSent
     * @param date $dateSent
     * @param date $pauseEmailTillDate
     *
     */
    private function determineSendEmailDate(
        $lastSeqSent=null,
        $dateSent,
        $dateStarted,
        $pauseEmailTillDate = null
    )
    {
        $sendMail = false;

        // check to see if a valid seq number
        if ($lastSeqSent >= 0 && array_key_exists(($lastSeqSent + 1), self::SCHEDULE)) {
            debug('c');
            if (Configure::read('ENV') == 'production') {
                $emailTestUnit = 'days';
            } else {
                $emailTestUnit = Configure::read('EmailTestingUnit');
            }

            if (is_null($emailTestUnit)) {
                throw new \Exception('Email testing unit needs to be configured');
            }

            $emailDate = $dateSent->modify('+' . self::SCHEDULE[$lastSeqSent + 1] . ' ' . $emailTestUnit );
            $time = new Time();

            if ($emailTestUnit == 'days') {
                if ($pauseEmailTillDate) {
                    // if current date is greater than pause till date and scheduled date is less than or equal cur date
                    $sendMail = ($emailDate->format('Y-m-d') <= $time->format('Y-m-d'))
                        && $time->format('Y-m-d') > $pauseEmailTillDate->format('Y-m-d');
                } else {
                    $sendMail = $emailDate->format('Y-m-d') <= $time->format('Y-m-d');
                }
            } else {
                if ($pauseEmailTillDate) {
                    // if current date is greater than pause till date and scheduled date is less than or equal cur date
                    $sendMail = ($emailDate->format('Y-m-d H:i') <= $time->format('Y-m-d H:i'))
                        && $time->format('Y-m-d H:i') > $pauseEmailTillDate->format('Y-m-d H:i');
                } else {
                    $sendMail = $emailDate->format('Y-m-d H:i') <= $time->format('Y-m-d H:i');
                }
//                debug($emailDate->format('Y-m-d H:i'));
//                debug($time->format('Y-m-d H:i'));
//                debug($sendMail);
            }
        } else {
            debug('b');
        }

        return $sendMail;
    }
}
