<?php

namespace LabOrders\Mailer;

use App\Util\EmailHelper;
use Cake\Datasource\EntityInterface as EntityInterfaceAlias;
use Cake\Mailer\Mailer;
use Lib24watch\Model\Table\Lib24watchSettingsTable;

class LabOrdersMailer extends Mailer
{
    /**
     * @param EntityInterfaceAlias $labOrderEntity
     * @param $message
     * @param $type
     * @throws \Exception
     */
    protected function labProcessingError(
        EntityInterfaceAlias $labOrderEntity,
        $message,
        $type = null
    )
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this->setTo($rxgAdmins);

        $subject = "Lab order #" . $labOrderEntity->id . " failed to process with PGX";
        // in order to reuse this mailer for lab and pgx processing errors pass the 3rd param as "lab"
        if ($type != null && $type == 'lab') {
            $subject = "Lab order #" . $labOrderEntity->id . " failed to process with Lab";
        }
        $this
            ->setSubject($subject)
            ->setViewVars(
                [
                    'labOrderEntity' => $labOrderEntity,
                    'message' => is_array($message) && isset($message->details) ? $message->details : $message
                ]
            )
            ->setTemplate('LabOrders.labProcessingError')
            ->setEmailFormat('html')
            ->setLayout('RxgTheme.email');
    }

    protected function reportUnviewedLabOrder(
        EntityInterfaceAlias $labOrderEntity
    )
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        if (!isset($labOrderEntity->provider) || !isset($labOrderEntity->provider->email)) {
            throw new \Exception('Provider required');
        }

        $this
            ->setTo($rxgAdmins)
            ->setSubject("New Report Left Unviewed for 7 Days")
            ->setViewVars(
                [
                    'labOrderEntity' => $labOrderEntity
                ]
            )
            ->setTemplate('LabOrders.reportUnviewedLabOrder')
            ->setEmailFormat('html');
    }

    /**
     * @param string $emailMessage
     * @param string $adminEmail
     */
    protected function labOrderReprocess(
        string $emailMessage,
        string $adminEmail,
        bool $success = true
    )
    {
        if (\Cake\Core\Configure::read('App.fullBaseUrl') === false) {
            $fullBaseUrl = Lib24watchSettingsTable::readSettingStatic('fullBaseUrl', 'Lib24watch', null);
            if (is_null($fullBaseUrl)) {
                throw new \Exception('24W setting fullBaseUrl must be defined');
            }

            \Cake\Core\Configure::write('App.fullBaseUrl', 'http://' . $fullBaseUrl);
        }

        if ($success) {
            $subject = "Congratulations! Your lab order for Rxgenomix has been reprocessed (" . \Cake\Core\Configure::read('App.fullBaseUrl') .")";
        } else {
            $subject = "Unfortunately, Your lab order for Rxgenomix has failed to process (" . \Cake\Core\Configure::read('App.fullBaseUrl') .")";
        }

        $this
            ->setTo($adminEmail)
            ->setSubject($subject)
            ->setViewVars(
                [
                    'emailMessage' => $emailMessage
                ]
            )
            ->setTemplate('LabOrders.labOrderReprocessed')
            ->setEmailFormat('html');
    }

    /**
     * @param EntityInterfaceAlias $labOrderEntity
     * @param string $currentUserEmail
     * @param string $currentUserFirstName
     */
    protected function labOrderSubmitted(
        EntityInterfaceAlias $labOrderEntity,
        string $currentUserEmail,
        string $currentUserFirstName
    )
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        if (!isset($labOrderEntity->provider) || !isset($labOrderEntity->provider->email)) {
            throw new \Exception('Provider required');
        }

        // if the currently logged in user is different from the provider on the lab order, add the lab order physician to the cc
        // example: if mike created the lab order and meder submits it, meder gets the email and mike gets to be in the cc list
        if ($currentUserEmail !== $labOrderEntity->provider->email && !in_array($labOrderEntity->provider->email, $rxgAdmins)) {
            $rxgAdmins[] = $labOrderEntity->provider->email;
        }

        $this->setCc($rxgAdmins);

        $this
            ->setTo($currentUserEmail)
            ->setSubject("Congratulations! Your lab order for the RxGenomix Hub has been submitted.")
            ->setViewVars(
                [
                    'labOrderEntity' => $labOrderEntity,
                    'currentUserFirstName' => $currentUserFirstName
                ]
            )
            ->setTemplate('LabOrders.labOrderSubmitted')
            ->setEmailFormat('html');
    }
}
