<?php

namespace LabOrders\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class LabOrder extends Entity
{
    public function _getClinicianFormatted()
    {
        $clinician = null;

        if (isset($this->providers) && !empty($this->providers)) {
            foreach ($this->providers as $provider) {
                if ($provider->is_ordering_physician) {
                    $clinician = $provider->provider_name;
                    $clinician = explode(' ', $clinician);
                    $clinician = implode(' ', array_map('ucfirst', $clinician));
                    break;
                }
            }
        }

        return $clinician;
    }

    public function _getSpecimenCollectionDateAndTime()
    {
        $dateTime = null;
        if (isset($this->specimen_collection_date) && isset($this->specimen_time)) {
            $dateTime = new \Cake\I18n\Time($this->specimen_collection_date->format('Y-m-d') . ' ' . $this->specimen_time->format('H:i:s'));
        }

        return $dateTime;
    }

    public function _getSpecimenTypeFormatted()
    {
        $specimenType = null;
        if ($this->specimen_type_id > 0) {
            $specimenTypes = \Cake\ORM\TableRegistry::getTableLocator()->get('SystemManagement.SpecimenTypes')
                ->find()
                ->where([
                    'is_active' => 1
                ])
                ->combine('id', 'title')
                ->toArray();

            if (isset($specimenTypes[$this->specimen_type_id])) {
                $specimenType = $specimenTypes[$this->specimen_type_id];
            }
        }

        return $specimenType;
    }

    /**
     * Determine the logic to see if a lab order is skippable to the review step
     * E.g. if a patient and providers (required) have been filled out
     *
     * @return bool
     */
    public function _getIsSkippableToReview()
    {
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        if (
            $this->lab_order_type_id == $labOrdersTable::LAB_ORDER_COVID_ID &&
            isset($this->lab_order_status_id) &&
            is_null($this->sample_id) &&
            (isset($this->specimen_barcode) && mb_strlen($this->specimen_barcode) > 0) &&
            (isset($this->specimen_collection_date) && !is_null($this->specimen_collection_date)) &&
            (isset($this->specimen_time) && !is_null($this->specimen_time)) &&
            (isset($this->specimen_identifier) && mb_strlen($this->specimen_identifier) > 0) &&
            isset($this->specimen_type_id) && $this->specimen_type_id > 0
        ) {
                return true;
        } elseif (
                isset($this->lab_order_status_id) && 
                isset($this->patient) &&
                isset($this->providers) &&
                ( // Check that step 4 was completed
                    (isset($this->ship_to_provider) && $this->ship_to_provider) ||
                    (isset($this->ship_to_patient) && $this->ship_to_patient) ||
                    (isset($this->sample_id) && $this->sample_id)
                ) &&
                !empty($this->providers)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Get the selected lab order id depending on lab order type
     *
     * @return mixed
     */
    public function _getSelectedLabId()
    {
        $labId = null;

        if ( 
            $this->lab_order_type_id === \LabOrders\Model\Table\LabOrdersTable::LAB_ORDER_PGX_ID &&
            isset($this->provider->health_care_client) &&
            !empty($this->provider->health_care_client) &&
            isset($this->provider->health_care_client->lab_id) &&
            (int)$this->provider->health_care_client->lab_id > 0
        ) {
            $labId = $this->provider->health_care_client->lab_id;
        } else if ( 
            $this->lab_order_type_id === \LabOrders\Model\Table\LabOrdersTable::LAB_ORDER_COVID_ID &&
            isset($this->provider->health_care_client) &&
            !empty($this->provider->health_care_client) &&
            isset($this->provider->health_care_client->covid_lab_id) &&
            (int)$this->provider->health_care_client->lab_id > 0
        ) {
            $labId = $this->provider->health_care_client->lab_id;
        }

        if (is_null($labId)) {
            throw new \Exception('Lab not specified');
        }

        return $labId;
    }
}
