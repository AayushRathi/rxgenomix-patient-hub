<?php
namespace LabOrders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderWorkflowStatusesTable
 * @package LabOrders\Model\Table
 */
class LabOrderWorkflowStatusesTable extends Lib24watchTable
{
    /**
     * @param int $labOrderInitiatorType
     * @param string $currentAction
     * @return array|bool|\Cake\Datasource\EntityInterface|null
     */
    public function getCurrentStep(int $labOrderInitiatorType, string $currentAction)
    {
        if ($labOrderInitiatorType && $currentAction) {
            $currentAction = $this->find()->where([
                'lab_order_initiator_type' => $labOrderInitiatorType,
                'action' => $currentAction,
                'is_active' => 1
            ])->firstOrFail();

            return $currentAction;
        }
        return false;
    }

    /**
     * @param int $labOrderInitiatorType
     * @param string $currentAction
     * @param string $next
     * @return array|bool|\Cake\Datasource\EntityInterface|null
     */
    public function getNextStepByCurrentAction(int $labOrderInitiatorType, string $currentAction, string $next = '>')
    {
        if ($labOrderInitiatorType && $currentAction) {
            $sortDir = 'ASC';
            if ($next == '<') {
                $sortDir = 'DESC';
            }

            $currentAction = $this->find()->where([
                'lab_order_initiator_type' => $labOrderInitiatorType,
                'action' => $currentAction,
                'is_active' => 1
            ])->firstOrFail();


            return $this->find()->where([
                'lab_order_initiator_type' => $labOrderInitiatorType,
                'display_order ' . $next => $currentAction->display_order,
                'is_active' => 1
            ])->order(['display_order' => $sortDir])->first();
        }
        return false;
    }

    /**
     * @param int $labOrderInitiatorType
     * @return array
     */
    public function getStepNumberMapping(int $labOrderInitiatorType)
    {
        $results = $this->find()->where([
            'lab_order_initiator_type' => $labOrderInitiatorType,
            'is_active' => 1
        ])->order(['display_order']);

        $mapping = [];
        $i = 1;
        foreach ($results as $result) {
           $mapping[$result->action] = $i;
           $i++;
        }
        return $mapping;
    }

    /**
     * Get all active actions to see if we can generate a mapping
     *
     * @return array
     */
    public function getActiveActions()
    {
        return $this
            ->find()
            ->select([
                'action'
            ])->where([
                'is_active' => 1
            ])
            ->distinct()
            ->extract('action')
            ->toArray();
    }
}
