<?php
namespace LabOrders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultMedicationsTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultMedicationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->belongsTo(
            'LabOrderMedications',
            [
                'className' => 'LabOrders.LabOrderMedications',
                'foreignKey' => 'lab_order_medication_id'
            ]
        );

        $this->hasMany(
            'MedicationsMapped',
            [
                'className' => 'LabOrders.LabOrderMedications',
                'bindingKey' => 'medication_id',
                'foreignKey' => 'medication_id'
            ]
        );
    }
}
