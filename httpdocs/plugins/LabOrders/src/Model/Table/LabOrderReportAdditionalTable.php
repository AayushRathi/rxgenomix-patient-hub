<?php

namespace LabOrders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportAdditionalTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportAdditionalTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReports',
            [
                'className' => 'LabOrders.LabOrderReports',
                'foreignKey' => 'lab_order_id'
            ]
        );
    }

    public function validationReview(Validator $validator): Validator
    {
        return $validator
            ->requirePresence('additional_pharmacist_notes_prescriber')
            ->requirePresence('additional_pharmacist_notes_patient')
            ->requirePresence('adverse_drug_reaction_notes_prescriber')
            ->requirePresence('adverse_drug_reaction_notes_patient')
            ->requirePresence('lifestyle_recommendation_notes_prescriber')
            ->requirePresence('lifestyle_recommendation_notes_patient')

            ->notEmpty('additional_pharmacist_notes_prescriber', 'Additional Pharmacist Notes - Prescriber is required')
            ->notEmpty('additional_pharmacist_notes_patient', 'Additional Pharmacist Notes - Patient is required')
            ->notEmpty('adverse_drug_reaction_notes_prescriber', 'Adverse Drug Reaction Notes - Prescriber is required')
            ->notEmpty('adverse_drug_reaction_notes_patient', 'Adverse Drug Reaction Notes - Patient is required')
            ->notEmpty('lifestyle_recommendation_notes_prescriber', 'Lifestyle Recommendation Notes - Prescriber is required')
            ->notEmpty('lifestyle_recommendation_notes_patient', 'Lifestyle Recommendation Notes - Patient is required');

    }
}
