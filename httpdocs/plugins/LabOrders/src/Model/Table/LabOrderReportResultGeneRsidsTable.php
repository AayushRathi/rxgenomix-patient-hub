<?php
namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultGeneRsidsTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultGeneRsidsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReportResultGene',
            [
                'className' => 'LabOrders.LabOrderReportResultGenes',
                'foreignKey' => 'lab_order_report_result_gene_id'
            ]
        );
    }
}
