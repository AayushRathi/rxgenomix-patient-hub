<?php
namespace LabOrders\Model\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Association\HasMany;
use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrdersTable
 * @package LabOrders\Model\Table
 * @property LabOrderMedicationsTable&HasMany $Medications
 */
class LabOrdersTable extends Lib24watchTable
{
    const SAMPLE_PREFIX = 'Sample';

    const LAB_ORDER_PGX_ID = 1;

    const LAB_ORDER_COVID_ID = 2;

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Patient',
            [
                'className' => 'Patients.Patients',
                'foreignKey' => 'patient_id'
            ]
        );

        $this->belongsTo(
            'Provider',
            [
                'className' => 'Providers.Providers',
                'foreignKey' => 'provider_id'
            ]
        );

        $this->belongsTo(
            'Status',
            [
                'className' => 'LabOrders.LabOrderStatuses',
                'foreignKey' => 'lab_order_status_id'
            ]
        );

        $this->belongsTo(
            'User',
            [
                'className' => 'RxgUsers.RxgUsers',
                'foreignKey' => 'id',
                'bindingKey' => 'user_id'
            ]
        );

        $this->hasMany(
            'History',
            [
                'className' => 'LabOrders.LabOrderHistory',
                'foreignKey' => 'lab_order_id'
            ]
        );

        $this->hasMany(
            'Medications',
            [
                'className' => 'LabOrders.LabOrderMedications',
                'foreignKey' => 'lab_order_id',
                'saveStrategy' => 'replace'
            ]
        );

        $this->hasMany(
            'Providers',
            [
                'className' => 'LabOrders.LabOrderProviders',
                'foreignKey' => 'lab_order_id',
                'saveStrategy' => 'replace'
            ]
        );

        $this->hasMany(
            'Reports',
            [
                'className' => 'LabOrders.LabOrderReports',
                'foreignKey' => 'lab_order_id',
                'sort' => 'Reports.id DESC'
            ]
        );

        $this->hasOne(
            'LastReport',
            [
                'className' => 'LabOrders.LabOrderReports',
                'strategy' => 'select',
                'conditions' => function (\Cake\Database\Expression\QueryExpression $exp, \Cake\ORM\Query $query) {
                    $subquery = $query
                        ->connection()
                        ->newQuery()
                        ->select(['LOR.id'])
                        ->from(['LOR' => 'lab_order_reports'])
                        ->where([
                            'LastReport.lab_order_id' => 'LOR.lab_order_id',
                            'LastReport.id' => 'LOR.id'
                        ])
                        ->order(['LOR.id' => 'DESC'])
                        ->limit(1);

                    return $exp->add(
                        ['LastReport.id' => $subquery]);
                }
            ]
        );

        $this->hasMany(
            'Events',
            [
                'className' => 'SystemManagement.EventLogs',
                'foreignKey' => 'lab_order_id',
                'sort' => 'Events.created DESC'
            ]
        );

        $this->hasOne(
            'WorkflowStatus',
            [
                'className' => 'LabOrders.LabOrderWorkflowStatuses',
                'foreignKey' => 'id',
                'bindingKey' => 'lab_order_workflow_status_id'
            ]
        );

        $this->belongsTo(
            'Invoice',
            [
                'className' => 'Invoices.Invoices',
                'foreignKey' => 'invoice_id'
            ]
        );

        $this->belongsTo(
            'SpecimenTypes',
            [
                'className' => 'SystemManagement.SpecimenTypes',
                'foreignKey' => 'specimen_type_id'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->alphaNumeric('sample_id', __('Only alphanumeric characters are allowed.'))
            ->allowEmptyString('sample_id');
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationCovid(Validator $validator)
    {
        $validator = $this->validationDefault($validator);
        return $validator
            ->requirePresence('specimen_barcode', 'Specimen Barcode is required')
            ->notEmpty('specimen_barcode', 'Specimen Barcode is required')
            ->requirePresence('specimen_collection_date', 'Specimen Collection Date is required')
            ->notEmpty('specimen_collection_date', 'Specimen Collection Date is required')
            ->requirePresence('specimen_time', 'Specimen collection time is required')
            ->notEmpty('specimen_time', 'Specimen collection time is required')
            ->add('specimen_time', [
                'isTime' => [
                    'rule' => 'time',
                    'last' => true,
                    'required' => true,
                    'message' => 'Not a valid time'
                ]
            ]);
    }

    /**
     * @param EntityInterface|null $labOrderEntity
     * @return bool|string
     */
    public function generateUniqueSampleId(EntityInterface $labOrderEntity = null)
    {
        if ($labOrderEntity) {
            $sampleId = self::SAMPLE_PREFIX . $labOrderEntity->id;
            $duplicates = $this->find()->where(['sample_id' => $sampleId]);
            if ($duplicates->count() > 0) {
                $sampleId . '_' . date('Y-m-d');
            }
            return $sampleId;
        }
        return false;
    }

    /**
     * @param Event $event
     * @param EntityInterface $entity
     * @param ArrayObject $options
     */
    public function beforeSave(Event $event, EntityInterface $entity, \ArrayObject $options)
    {
        if ($entity->isDirty('lab_order_status_id')) {
            $entity->lab_order_status_change_date = \Cake\I18n\FrozenTime::now();
        }
    }
}
