<?php
namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultCategoriesTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultCategoriesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );
    }
}
