<?php

namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultCitationsTable
 * @package LabOrders\Model\Table
 */

class LabOrderReportResultCitationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lab_order_report_result_citations');

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );
    }
}
