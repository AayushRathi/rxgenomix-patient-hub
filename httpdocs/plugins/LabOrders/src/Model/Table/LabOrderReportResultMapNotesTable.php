<?php
namespace LabOrders\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultMapTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultMapNotesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->belongsTo(
            'MapProblemRationales',
            [
                'className' => 'SystemManagement.MapProblemRationales',
                'foreignKey' => 'map_problem_rationale_id',
            ]
        );

        $this->belongsTo(
            'AlternateResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'alternate_result_id'
            ]
        );

        $this->belongsTo(
            'AlternateDrug',
            [
                'className' => 'SystemManagement.Drugs',
                'foreignkey' => 'alternate_drug_id'
            ]
        );

        $this->belongsTo(
            'SeverityLevels',
            [
                'className' => 'LabOrders.LabOrderReportResultSeverityLevels',
                'foreignKey' => 'severity_level_id'
            ]
        );
    }

    /**
     * @param Validator $validator
     *
     * @return Validator
     *
     */
    public function validationReview(Validator $validator): Validator
    {
        return $validator
            //->requirePresence('patient_notes')
            ->requirePresence('prescriber_notes')
            ->requirePresence('map_problem_rationale_id', function($context) {// TODO: does this even work
                if (
                    (isset($context['alternate_drug_id']) && mb_strlen($context['alternate_drug_id']) > 0) ||
                    (isset($context['alternate_result_id']) && mb_strlen($context['alternate_result_id']) > 0)
                ) {
                    return 'Reason for change is required';
                }

                return false;
            })
            ->notEmpty('map_problem_rationale_id', 'Reason for change is required', function ($context) {
                if (
                    (isset($context['data']['is_included']) && $context['data']['is_included'] == 1) &&
                    (
                        (isset($context['data']['alternate_drug_id']) && mb_strlen($context['data']['alternate_drug_id']) > 0) ||
                        (isset($context['data']['alternate_result_id']) && mb_strlen($context['data']['alternate_result_id']) > 0)
                    )
                ) {
                    return 'Reason for change is required';
                }
            })
            ->notEmpty('severity_level_id', 'Severity level is required', function ($context) {
                if ($context['data']['is_included'] == 1) {
                    return true;
                }

                return false;
            })
            /*
            ->notEmpty('patient_notes', 'Patient notes is required', function ($context) {
                if ($context['data']['is_included'] == 1) {
                    return true;
                }
            })*/
            ->notEmpty('prescriber_notes', 'Patient notes is required', function ($context) {
                if ($context['data']['is_included'] == 1) {
                    return true;
                }
            });
    }

    /**
     * @param Event $event
     * @param ArrayObject $data
     * @param ArrayObject $options
     *
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        foreach ($data as $key => $value) {
            if ($key === 'alternate_drug_id') {
                $split = explode('_', $value);
                $data[$key] = $split[0];
            }
        }
    }
}
