<?php
namespace LabOrders\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use LabOrders\Libs\LabOrders;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultsTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReport',
            [
                'className' => 'LabOrders.LabOrderReports',
                'foreignKey' => 'lab_order_report_id'
            ]
        );

        $this->hasMany(
            'ResultCitations',
            [
                'className' => 'LabOrders.LabOrderReportResultCitations',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasMany(
            'ResultObservations',
            [
                'className' => 'LabOrders.LabOrderReportResultObservations',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasMany(
            'ResultGenes',
            [
                'className' => 'LabOrders.LabOrderReportResultGenes',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasOne(
            'ResultMapNotes',
            [
                'className' => 'LabOrders.LabOrderReportResultMapNotes',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasMany(
            'ResultMedications',
            [
                'className' => 'LabOrders.LabOrderReportResultMedications',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasMany(
            'Categories',
            [
                'className' => 'LabOrders.LabOrderReportResultCategories',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );
    }

    /**
     * @param int $labOrderReportId
     * @param string $method
     *
     * @return array
     *
     */
    public function getScores(
        int $labOrderReportId,
        string $riskMethod = 'scoreByMedication'
    )
    {
        $currentRiskScore = 0;
        $currentRiskMaxScore = 0;
        $futureRiskScore = 0;
        $futureRiskMaxScore = 0;
        $foundTsiMedicationIds = [];

        $labOrderReportEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports')
            ->find()
            ->where([
                'LabOrderReports.id' => $labOrderReportId
            ])
            ->firstOrFail();

        $labOrdersInstance = new LabOrders();

        $resultSet = $this->find('filteredReportResults', [
            'lab_order_report_id' => $labOrderReportId
        ])
        ->formatResults(function (\Cake\Collection\CollectionInterface $results) use (
            &$currentRiskScore,
            &$currentRiskMaxScore,
            &$futureRiskScore,
            &$futureRiskMaxScore,
            $riskMethod,
            &$foundTsiMedicationIds,
            &$foundDrugIds,
            $labOrdersInstance
        ) {
            return call_user_func_array(
                [
                    $labOrdersInstance, 
                    $riskMethod
                ], [
                    $results,
                    &$currentRiskScore,
                    &$currentRiskMaxScore,
                    &$futureRiskScore,
                    &$futureRiskMaxScore,
                    &$foundTsiMedicationIds,
                    &$foundDrugIds
                ]
            );
        })->toArray();

        $labOrder = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')->get($labOrderReportEntity->lab_order_id, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames',
                'Medications.Medication'
            ]
        ]);

        $labOrdersInstance->calculateRiskScore(
            $riskMethod,
            $labOrder,
            $currentRiskScore,
            $currentRiskMaxScore,
            $futureRiskScore,
            $futureRiskMaxScore,
            $foundTsiMedicationIds,
            $foundDrugIds
        );

        $currentPercentage = (($currentRiskScore/$currentRiskMaxScore)*100);
        $futurePercentage = (($futureRiskScore/$futureRiskMaxScore)*100);

        return compact(
            'currentRiskScore', 
            'currentRiskMaxScore', 
            'futureRiskScore', 
            'futureRiskMaxScore',
            'currentPercentage',
            'futurePercentage'
        );
    }

    /**
     * @param Query $query
     * @param array $options
     *
     * @return Query
     */
    public function findFilteredReportResults(Query $query, array $options): Query
    {
        if (is_null($options['lab_order_report_id'])) {
            throw new \Exception('Lab Order report id required');
        }

        $labOrderReportId = $options['lab_order_report_id'];

        $labOrderReportEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports')
            ->find()
            ->where([
                'LabOrderReports.id' => $labOrderReportId
            ])
            ->firstOrFail();

        $labOrderId = $labOrderReportEntity->lab_order_id;

        return $query->where([
            'LabOrderReportResults.lab_order_report_id' => $labOrderReportId
        ])
        ->select([
            'LabOrderReportResults.id',
            'lab_order_report_id',
            'tsi_detail',
            'LabOrderReportResults.tsi_severity',
        ])
        ->contain([
            'ResultCitations',
            'ResultMedications.LabOrderMedications.Drug' => [
                'fields' => [
                    'id',
                    'specificproductname'
                ]
            ],
            'ResultMedications.MedicationsMapped' => function($q) use ($labOrderId) {
                return $q->andWhere([
                    'MedicationsMapped.lab_order_id' => $labOrderId
                ]);
            },
            'ResultMedications.MedicationsMapped.Drug',
            'ResultMedications.MedicationsMapped.Medication',
            'ResultGenes',
            'ResultObservations',
            'ResultMapNotes',
            'ResultMapNotes.SeverityLevels',
            'ResultMapNotes.MapProblemRationales',

            'ResultMapNotes.AlternateDrug' => [
                'fields' => [
                    'id',
                    'specificproductname'
                ]
            ],
            'ResultMapNotes.AlternateDrug.TsiMedication' => [
                'fields' => [
                    'id',
                    'tsi_medication_id',
                ]
            ],
            'ResultMapNotes.AlternateResult' => [
                'fields' => [
                    'tsi_detail',
                    'tsi_severity',
                ]
            ],
        ])
        ->matching('ResultMedications')
        ->matching('ResultMapNotes', function ($q) {
            return $q->andWhere([
                'ResultMapNotes.is_included' => 1
            ]);
        })
        ->where([
            'LabOrderReportResults.is_relevant' => 1
        ])
        /*
        ->matching('ResultMedications.MedicationsMapped', function($q) use ($labOrderId) {
            return $q->andWhere([
                'MedicationsMapped.lab_order_id' => $labOrderId
            ]);
        })*/
        ->order(['FIELD(LabOrderReportResults.tsi_severity, "high", "moderate", "low")']);
    }
}
