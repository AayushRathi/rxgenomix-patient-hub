<?php
namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderHistoryTable
 * @package LabOrders\Model\Table
 */
class LabOrderHistoryTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'LabOrder',
            [
                'className' => 'LabOrders.LabOrders',
                'foreignKey' => 'lab_order_id'
            ]
        );
    }
}
