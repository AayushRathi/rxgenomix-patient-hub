<?php
namespace LabOrders\Model\Table;

use Cake\ORM\Query;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportsTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrder',
            [
                'className' => 'LabOrders.LabOrders',
                'foreignKey' => 'lab_order_id'
            ]
        );

        $this->hasMany(
            'Results',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_id'
            ]
        );

        $this->hasOne(
            'ReportAdditional',
            [
                'className' => 'LabOrders.LabOrderReportAdditional',
                'foreignKey' => 'lab_order_report_id'
            ]
        );

        $this->hasOne(
            'ThrombosisProfile',
            [
                'className' => 'LabOrders.LabOrderThrombosisProfiles',
                'foreignKey' => 'lab_order_status_id'
            ]
        );
    }

    /**
     * @param \Cake\ORM\Query
     * @param array $options
     *
     * @return \Cake\ORM\Query;
     */
    public function findLatestReport(Query $query, array $options)
    {
        if (!isset($options['lab_order_id'])) {
            throw new \Exception('Lab order id required');
        }

        $query->andWhere([
            'LabOrderReports.lab_order_id' => $options['lab_order_id']
        ]);

        return $query
            ->order(['LabOrderReports.id' => 'DESC'])
            ->limit(1);
    }

    /**
     * @param \Cake\ORM\Query
     * @param array $options
     *
     * @return \Cake\ORM\Query;
     */
    public function findLatestReportFull(Query $query, array $options)
    {
        return $this->find('latestReport', $options)
            ->contain([
                'ReportAdditional',
                'Results' => function($q) {
                    return $q->andWhere([
                        'Results.is_relevant' => 1
                    ])->order(['FIELD(Results.tsi_severity, "high", "moderate", "low")']);
                },
                'Results.ResultMedications.LabOrderMedications.Drug' => [
                    'fields' => [
                        'id',
                        'specificproductname',
                    ]
                ],
                'Results.ResultMedications.MedicationsMapped' => function($q) use ($options) {
                    return $q->andWhere([
                        'MedicationsMapped.lab_order_id' => $options['lab_order_id']
                    ]);
                },
                'Results.ResultMedications.MedicationsMapped.Drug',
                'Results.ResultMedications.MedicationsMapped.Medication',
                'Results.ResultMedications.MedicationsMapped.Medication.Alternates',
                'Results.ResultMedications.MedicationsMapped.Medication.Alternates.Drugs' => [
                    'fields' => [
                        'id',
                        'medication_id',
                        'specificproductname',
                    ]
                ],
                'Results.ResultMapNotes',
                'Results.ResultMapNotes.MapProblemRationales',
                'Results.ResultObservations',

                'Results.ResultMapNotes.AlternateResult' => [
                    'fields' => [
                        'tsi_detail',
                        'tsi_severity',
                    ]
                ],
                'Results.ResultMapNotes.AlternateDrug',
            ])
            ->matching('ReportAdditional')
            ->matching('Results')
            // refs #39505, reformatting structure of results array
            // so we can reliably render a new dynamically generated element (through ajax)
            // and have it save properly because "natural" indexing doesn't work (due to inconsistent ordering)
            ->formatResults(function($results) {
                return $results->map(function($row) {
                    if (!empty($row->results)) {
                        $row->results = (new \Cake\Collection\Collection($row->results))->indexBy('id')->toArray();
                    }

                    return $row;
                });
            });
    }
}
