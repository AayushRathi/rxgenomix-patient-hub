<?php

namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultSeverityLevelsTable
 * @package LabOrders\Model\Table
 */

class LabOrderReportResultSeverityLevelsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lab_order_report_result_severity_levels');
    }
}
