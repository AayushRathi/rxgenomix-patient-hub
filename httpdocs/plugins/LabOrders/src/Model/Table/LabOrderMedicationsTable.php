<?php
namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderMedicationsTable
 * @package LabOrders\Model\Table
 */
class LabOrderMedicationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Medication',
            [
                'className' => 'SystemManagement.Medications',
                'foreignKey' => 'medication_id'
            ]
        );

        $this->belongsTo(
            'Drug',
            [
                'className' => 'SystemManagement.Drugs',
                'foreignKey' => 'drug_id'
            ]
        );
    }
}
