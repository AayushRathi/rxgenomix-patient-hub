<?php
namespace LabOrders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultObservationsTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultObservationsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lab_order_report_result_observations');

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );
    }
}
