<?php
namespace LabOrders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportResultGenesTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportResultGenesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReportResult',
            [
                'className' => 'LabOrders.LabOrderReportResults',
                'foreignKey' => 'lab_order_report_result_id'
            ]
        );

        $this->hasMany(
            'Rsids',
            [
                'className' => 'LabOrders.LabOrderReportResultGeneRsids',
                'foreignKey' => 'lab_order_report_result_gene_id'
            ]
        );
    }
}
