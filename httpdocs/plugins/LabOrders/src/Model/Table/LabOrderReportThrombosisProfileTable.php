<?php
namespace LabOrders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabOrderReportThrombosisProfileTable
 * @package LabOrders\Model\Table
 */
class LabOrderReportThrombosisProfileTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'LabOrderReport',
            [
                'className' => 'LabOrders.LabOrderReports',
                'foreignKey' => 'lab_order_report_id'
            ]
        );

        $this->hasMany(
            'Alleles',
            [
                'className' => 'LabOrders.LabOrderThrombosisProfileAlleles',
                'foreignKey' => 'lab_order_report_thrombosis_profile_id'
            ]
        );
    }
}
