<?php
$pageHeading = "Browse Lab Orders";
$this->assign('title', 'Lab Orders');

$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Lab Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);

$datePickerPublicFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
    "datePickerPublicFormat",
    "Lib24watch",
    "YYYY-MM-DD"
);

echo $this->Html->scriptBlock("var datePickerPublicFormatFromSetting='" . $datePickerPublicFormatFromSetting . "';");

$labOrdersInstance = new \LabOrders\Libs\LabOrders();
?>
<style>
    /* TODO: get rid of this terrible hack, workaround for Bootstrap grid */
    .col-4 {
        width: 33.333333333%;
        float: left;
        margin-bottom: 6px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->cell('Lib24watch.Table::filter', [
            'Lab Orders',
            'items' => [
                /*[
                    'name' => 'title',
                    'type' => 'text',
                    'label' => 'Patient Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->matching('Patient', function ($q) use ($value) {
                            return $q->where([
                                'OR' => [
                                    ['first_name LIKE' => '%' . $value . '%'],
                                    ['last_name LIKE' => '%' . $value . '%'],
                                    ['CONCAT(first_name, " ", last_name) LIKE' => '%' . $value . '%']
                                ]
                            ]);
                        });
                    }
                ],*/
                [
                    'name' => 'first_name',
                    'type' => 'text',
                    'label' => 'Patient First Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->where(['Patient.first_name LIKE' => '%' . $value . '%']);
                    }
                ],
                [
                    'name' => 'last_name',
                    'type' => 'text',
                    'label' => 'Patient Last Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->where(['Patient.last_name LIKE' => '%' . $value . '%']);
                    }
                ],
                [
                    'name' => 'phone',
                    'type' => 'text',
                    'label' => 'Patient Phone',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->matching('Patient', function ($q) use ($value) {
                            return $q->where(['phone LIKE' => '%' . $value . '%']);
                        });
                    }
                ],
                [
                    'name' => 'id',
                    'alias' => 'LabOrders.',
                    'type' => 'text',
                    'label' => 'Order #',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'sample_id',
                    'type' => 'text',
                    'label' => 'Sample ID',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'lab_order_status_id',
                    'type' => 'select',
                    'label' => 'Status',
                    'empty' => true,
                    'options' => $statuses,
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'lab_order_type_id',
                    'type' => 'Select',
                    'label' => 'Test Type',
                    'options' => ['' => 'All'] + $labOrderTypesList->toArray(),
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'created',
                    'alias' => 'LabOrders.',
                    'type' => 'datetimepicker',
                    'label' => 'Date Ordered',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($q, $val) {
                        return $q->where(['DATE(LabOrders.created)' => $val]);
                    }
                ]
            ],
            $labOrders,
            $this
        ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => false /*$this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'LabOrders',
                            'controller' => 'LabOrders',
                            'action' => 'add',
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Lab Order'
                        ]
                    )*/
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if ($labOrders->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'field' => 'id',
                                        'alias' => 'LabOrders',
                                        'label' => 'Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'sample_id',
                                        'label' => 'Sample #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'patient_id',
                                        'label' => 'Patient Name',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->patient ? $row->patient->first_name . ' ' . $row->patient->last_name : '';
                                        }
                                    ],
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Kit Status',
                                        'type' => 'lookup',
                                        'list' => $statuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) use ($labOrdersInstance) {
                                            $report_available = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('status_report_complete', 'LabOrders', 7);
                                            $returnHtml = $this->Html->link(
                                                '<span class="fa fa-view"></span> View',
                                                [
                                                    'plugin' => 'LabOrders',
                                                    'controller' => 'LabOrders',
                                                    'action' => 'view',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary'
                                                ]
                                            );

                                            if((int)$row['lab_order_status_id'] === (int)$report_available) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-view"></span> Report',
                                                    [
                                                        'plugin' => 'LabOrders',
                                                        'controller' => 'LabOrders',
                                                        'action' => 'download',
                                                        $row->id
                                                    ], [
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-primary',
                                                        'target' => '_blank'
                                                    ]
                                                );

                                                if ($row['pharmacogenomic_provider_id'] == 2) {
                                                    $isMapReady = $labOrdersInstance->isMapReady($row['id']);

                                                    // TODO: change to TSI constant
                                                    if ($isMapReady) {
                                                        $mapTypes = \LabOrders\Libs\LabOrders::getActiveMapTypes();
                                                        foreach ($mapTypes as $slug => $type) {
                                                            $returnHtml .= $this->Html->link(
                                                                '<span class="fa fa-download"></span>' . $type['title'] . ' MAP',
                                                                [
                                                                    'plugin' => 'LabOrders',
                                                                    'controller' => 'LabOrders',
                                                                    'action' => 'downloadMap',
                                                                    $row->id,
                                                                    $slug
                                                                ], [
                                                                    'escape' => false,
                                                                    'class' => 'btn btn-condensed btn-primary',
                                                                    'target' => '_blank',
                                                                    'alt' => 'View the ' . $slug . ' medication action plan.'
                                                                ]
                                                            );
                                                        }
                                                    }
                                                }
                                            }

                                            $returnHtml .= $this->Html->link(
                                                'Delete',
                                                [
                                                    'plugin' => 'LabOrders',
                                                    'controller' => 'LabOrders',
                                                    'action' => 'delete',
                                                    $row->id,
                                                ],
                                                [
                                                    'class' => 'btn btn-condensed btn-danger',
                                                    'onclick' => "if(confirm('Are you sure you want to delete this lab order?')) { return true;} return false;",
                                                ]
                                            );
                                            return $returnHtml;
                                        }
                                    ]
                                ],
                                $labOrders
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to
                            <?php
                            echo $this->Html->link(
                                'Add a Lab Order',
                                [
                                    'plugin' => 'LabOrders',
                                    'controller' => 'LabOrders',
                                    'action' => 'tos',
                                ],
                                [
                                    'title' => 'Add a Lab Order'
                                ]
                            );
                            ?>.
                        </p>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
