<?php
    $this->assign('title', 'Lab Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Lab Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Lab Order');

    use App\Util\FormattingHelper;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3 class="panel-title">View Lab Order</h3>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title-decoration">Lab Order #<?= $order->id; ?></h2>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Patient Information</h3>
                                <span>Name:</span> <?= $order->patient->first_name . ' ' . $order->patient->last_name; ?>
                                <br>
                                <span>Ethnicity:</span>
                                <?php
                                    if (isset($ethnicities[$order->patient->ethnicity_id]) && $order->patient->ethnicity_id != 8) {
                                        echo $ethnicities[$order->patient->ethnicity_id];
                                    } elseif ($order->patient->ethnicity_id == 8) {
                                        echo $order->patient->ethnicity_other;
                                    }
                                    ?>
                                <br>
                                <span>Sex:</span>
                                <?php
                                    if (isset($sexes[$order->patient->sex_id])) {
                                        echo $sexes[$order->patient->sex_id];
                                    }
                                ?>
                                <br>
                                <span>State:</span> <?= $order->patient->lib24watch_state_abbr; ?>
                                <br>
<?php
                                    if (isset($order->patient->health_care_client) && !empty($order->patient->health_care_client)) {
?>
                                <span>Health Care Client:</span> <?= $order->patient->health_care_client->title; ?>
                                <br>

<?php } ?>
                                <span>Client ID:</span> <?= $order->patient->client_id; ?>
                                <br>
<?php if ($order->lab_order_type_id !== 2) {?>
                                <span>Card ID:</span> <?= $order->patient->card_id; ?>
                                <br>
                                <span>Person Code:</span> <?= $order->patient->person_code; ?>
                                <br>
<?php } ?>
                                <span>Date of Birth:</span> <?= lib24watchDate('shortDateFormat', $order->patient->date_of_birth, +5); ?>
                                <br>
                                <span><?php echo $order->lab_order_type_id === 2 ? 'Patient ' : '';?>Email:</span> <?= h($order->patient->email); ?>
                                <br>
                                <span><?php echo $order->lab_order_type_id == 2 ? 'Patient ' : '';?>Phone:</span> <?php
                                if (!is_null($order->patient->phone)) {
                                    echo h(FormattingHelper::formatPhone($order->patient->phone));
                                } else { 
                                    echo 'N/A'; 
                                } ?>

<?php
                                    if (
                                        isset($isImmutable) && 
                                        (bool)$isImmutable &&
                                        isset($order->provider) &&
                                        isset($order->provider->health_care_client) &&
                                        !empty($order->provider->health_care_client)
                                    ) {
                                        $labEntity = \Cake\ORM\TableRegistry::getTableLocator()->get('Labs.Labs')
                                            ->find()
                                            ->select([
                                                'lab_name'
                                            ])
                                            ->where([
                                                'Labs.id' => $order->provider->health_care_client->lab_id
                                            ])
                                            ->firstOrFail();

                                        echo '<br>Lab: ' . $labEntity->lab_name . '<br>';
                                    }
?>
                                <br><br>
                                <h3>Physician Information</h3>
                                <span>Name:</span> <?=$order->provider->user->first_name;?> <?=$order->provider->user->last_name;?></span><br>
                                <span>Has Viewed Report:</span> <?= $order->first_seen_provider_id == $order->provider->id ? 'Yes' : 'No';?>
                            </div>
                            <div class="col-md-6">
                                <h3>Shipping Information (if applicable)</h3>
        <?php
                                        if ($order->lab_order_type_id == 2) { ?>Onsite<?php } else {
        if ($order->ship_to_provider || $order->ship_to_patient) {
            ?>
            <span>Shipping Name:</span> <?= h($order->shipping_first_name . ' ' . $order->shipping_last_name); ?>
            <br>
            <span>Shipping Address:</span>
            <br>
            <?= h($order->shipping_address_1); ?>
            <br>
            <?= h($order->shipping_address_2); ?>
            <br>
            <?= h($order->shipping_city . ', ' . $order->shipping_lib24watch_state_abbr . ' ' . $order->shipping_postal_code); ?>
            <?php
        } else {
            ?>
            <span>Sample Kit ID #:</span> <?= h($order->sample_id); ?>
            <br>
            <span>Collection Date:</span> <?php 
                if (!is_null($order->collection_date)) {
                    echo lib24watchDate('shortDateFormat', $order->collection_date, +5);
                } else {?>N/A
            <?php
                }
        }
                                        }
?>
                            </div>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Provider Information</h3>
                                <?php
                                        echo $this->element('LabOrders.section_provider');
                                ?>
                            </div>
            <?php if ($order->lab_order_type_id === 1) { ?>
                            <div class="col-md-6">
                                <h3>Medications</h3>
                                <?php
                                echo $this->cell(
                                    'Lib24watch.Table::display',
                                    [
                                        '',
                                        'items' => [
                                            [
                                                'name' => 'drug.nondose_name',
                                                'label' => 'Name',
                                                'type' => 'text'
                                            ],
                                            [
                                                'name' => 'drug.brand_names',
                                                'label' => 'Brand Names',
                                                'type' => 'custom',
                                                'function' => function ($row) {
                                                    $brandNames = [];
                                                    foreach ($row->drug->brand_names as $brand) {
                                                        $brandNames[] = $brand->brand_name;
                                                    }
                                                    $brandNames = array_unique($brandNames);
                                                    return implode(', ', $brandNames);
                                                }
                                            ]
                                        ],
                                        $order->medications
                                    ]
                                );
                                ?>
                            </div>
<?php } else { ?>
    <div class="col-md-6">
        <h3>Covid</h3>
            <span>Specimen Barcode:</span>
            <?= h($order->specimen_barcode);?>
            <br>
            <span>Specimen Collection Date:</span> <?php 
                if (!is_null($order->specimen_collection_date)) { 
                    echo lib24watchDate('shortDateFormat', $order->specimen_collection_date, +5);
                } else { echo 'N/A';} ?>
            <br>
            <span>Specimen Collection Time:</span> <?php 
                if (!is_null($order->specimen_time)) { 
                    echo lib24watchDate('timeFormat', $order->specimen_time, 0);
                } else { echo 'N/A';} ?>
            <br>
            <span>Patient Second Identifier:</span> <?= $order->specimen_identifier; ?>
            <br>
            <span>Specimen Type:</span> <?php
                    if (!is_null($order->specimen_type_id)) {
                        echo $specimenTypesList[$order->specimen_type_id];
                    } else { echo 'N/A'; }

?>
<br><br>
<?php

            $selectedIcd10Codes = [];
            if (
                isset($order->patient->patient_icd10_codes) &&
                !empty($order->patient->patient_icd10_codes)
            ) {
                foreach ($order->patient->patient_icd10_codes as $entity) {
                    if (
                        $entity instanceof \Cake\ORM\Entity &&
                        isset($entity->icd10_code_id) &&
                        $entity->icd10_code_id > 0
                    ) {
                        $selectedIcd10Codes[] = $entity->icd10_code_id;
                    }
                }
            }

            if (isset($icd10Codes) && !empty($icd10Codes)) {
?>
<div class="icd10-wrapper pt-2">

<div>
    <label>ICD-10 CODE(S)</label>
</div>

<div class="row">
<?php
                    foreach ($icd10Codes as $icd10Code) {
                        echo $this->Form->control('patient.patient_icd10_codes.'.$icd10Code['id'], [
                            'type' => 'checkbox',
                            'disabled' => true,
                            'label' => $icd10Code['code'] . ' ' . $icd10Code['title'],
                            'checked' => in_array($icd10Code['id'], $selectedIcd10Codes),
                            'templates' => [
                                'inputContainer' => '<div class="col-md-6 input {{type}}{{required}}">{{content}}</div>',
                            ]
                        ]);
                    }

?>
</div>
<?php
            }
                ?>
</div>
<?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <h3 class="panel-title">Event Log</h3>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Status',
                                        'type' => 'lookup',
                                        'list' => $statuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => 'Action',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'message',
                                        'label' => 'Message',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'details',
                                        'label' => 'Details',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            if ($row->details != 'null') {
                                                return $row->details;
                                            } else {
                                                return false;
                                            }
                                        }
                                    ],
                                    [
                                        'name' => 'created',
                                        'label' => 'Date',
                                        'type' => 'date',
                                        'format' => 'shortDateFormat timeFormat'
                                    ]
                                ],
                                $order->events
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
