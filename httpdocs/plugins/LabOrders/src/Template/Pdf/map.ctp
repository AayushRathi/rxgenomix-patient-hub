<?php
$showDetail = false;
if (isset($_REQUEST['detail'])) {
    $showDetail = true;
}
?>
<div class="graph-container clearfix">
<?php
    echo $this->element('LabOrders.graph_chart', [
        'className' => 'graph-current',
        'riskLevel' => $currentRiskLevel,
        'percentage' => $currentPercentage,
        'riskScore' => $currentRiskScore,
        'heading' => 'Current Risk',
    ]);

    echo $this->element('LabOrders.graph_chart', [
        'className' => 'graph-future',
        'percentage' => $futurePercentage,
        'riskLevel' => $futureRiskLevel,
        'riskScore' => $futureRiskScore,
        'heading' => 'Future Risk',
    ]);
?>
</div>

<style>
.graph-background {
    background-image: url('<?php echo \Cake\Core\Configure::read('App.fullBaseUrl');?>/lab_orders/img/radial-smaller.png');
    background-repeat: no-repeat;
}

td.options {
    line-height: 2;
}

td.options small {
    font-size: 9pt;
    font-style: italic;
}

.map-footer {
    text-align: center;
}

</style>

<div class="row drug-table-container">
<?php 
if (isset($resultSetMedications) && !empty($resultSetMedications)) {
    $drugsUnique = [];
    $drugsAll = [];

    foreach ($resultSetMedications as $index => $list) {
        if ($index === '') {
            continue;
        }

        foreach ($list as $key => $item) {
            if (isset($item->result_medications) && !empty($item->result_medications)) {
				foreach ($item->result_medications as $medication) {
					if (isset($medication->lab_order_medication) && !empty($medication->lab_order_medication)  && isset($medication->lab_order_medication->drug) && !empty($medication->lab_order_medication->drug)) {
						$drugsUnique[$medication->lab_order_medication->drug_id] = [
							'severity' => $index,
							'drug' => $medication->lab_order_medication->drug
						];
					} else if (isset($medication->medications_mapped) && !empty($medication->medications_mapped)) {
						foreach ($medication->medications_mapped as $medicationMapped) {
							if (isset($medicationMapped->drug) && !empty($medicationMapped->drug) && array_key_exists($medicationMapped->drug_id, $drugsUnique) === false) {
								$drugsUnique[$medicationMapped->drug_id] = [
									'severity' => $index,
									'drug' => $medicationMapped->drug
								];
							}

							$drugsAll[] = [
								'severity' => $index,
								'drug' => $medicationMapped->drug
							];
						}
					}
				}
            }
        }
    }

    $drugsUnique = (new \Cake\Collection\Collection($drugsUnique))->groupBy('severity')->toArray();
}

foreach (\LabOrders\Libs\LabOrders::$severityLevels as $severity => $info) {
    echo $this->element('LabOrders.medication_list', [
        'severity' => $severity,
        'info' => $info,
        'drugs' => $drugsUnique[$severity] ?? []
    ]);
}
?>
</div>
<?php if (is_array($resultSet) && !empty($resultSet)) {?>

<table class="map-medication-table" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>Medication</th>
            <th>Genotype/Phenotype</th>
            <th>Implication</th>
            <th>Recommendation</th>
            <th>Accepted?</th>
        </tr>
    </thead>
    <tbody>
    <?php 
    foreach ($resultSet as $index => $result) {
        if ($result[0]['result_map_note']['is_included'] == 0) {
            continue;
        }
    ?>
        <tr>
            <td><?php
        if (
            isset($result[0]['result_medications']) && 
            !empty($result[0]['result_medications']) &&
            isset($result[0]['result_medications'][0]->medications_mapped) &&
            !empty($result[0]['result_medications'][0]->medications_mapped)
        ) {
            $drugCollection = new \Cake\Collection\Collection($result[0]['result_medications'][0]->medications_mapped);

            if ($drugCollection && $drugCollection->count() > 0) {
                $drugNames = $drugCollection->extract('drug.specificproductname')->toArray();
                echo implode(', ', $drugNames);
            }

            if (isset($result[0]->result_map_note->alternate_drug)) {
                echo '<br><br><strong>Alternate Drug Selected: </strong> ' . $result[0]->result_map_note->alternate_drug->specificproductname;
            }
        } else if (
            isset($result[0]['result_medications']) && 
            !empty($result[0]['result_medications']) &&
            isset($result[0]['result_medications'][0]['lab_order_medication']) &&
            !empty($result[0]['result_medications'][0]['lab_order_medication']) &&
            isset($result[0]['result_medications'][0]['lab_order_medication']['drug']) &&
            !empty($result[0]['result_medications'][0]['lab_order_medication']['drug'])
        ) {
            echo '<p class="drug-name">' . $result[0]['result_medications'][0]['lab_order_medication']['drug']['specificproductname'] . '</p>';
        }
        ?>
        </td>
            <td>
                <?php
                if (isset($result[0]['result_observations']) &&
                    !empty($result[0]['result_observations'])
                ) {
?>
    <ul><?php
                    foreach ($result[0]['result_observations'] as $observation) {
                    $detailText = '';
                        if ($observation->type === 'phenotype') {
                            if ($showDetail) {
                                $detailText = 'Phenotype: ';
                            }
                ?>
                    <li><?=$detailText;?><?=$observation->component_value_coding_1_display;?>:<?=$observation->value_coding_2_display;?></li>
                <?php
                        } else if ($observation->type === 'genotype') {
                            if ($showDetail) {
                                $detailText = 'Genotype: ';
                            }
?>
                            <li><?=$detailText;?><?=$observation->component_value_coding_1_display;?> <?=$observation->value_string;?></li>
    <?php
                        } else {?>
    <?php }
                }
?>
</ul>
<?php
                 } else {?>N/A<?php } ?>
            </td>
            <td class="detail"><div><?php
                     echo nl2br(trim($result[0]['result_map_note']['detail'])) ?? 'N/A';
?></div></td>
    <td><?php
        if ($mapType === 'patient') {
            echo nl2br(trim($result[0]['result_map_note']['patient_notes']));
        } else {
            echo nl2br(trim($result[0]['result_map_note']['prescriber_notes']));
        }
?></td>
            <td class="text-center options">Yes<br>No<br>Other<br><small>*detail below</small></td>
        </tr>
    </tbody>
<?php } ?>
</table>
<?php } ?>

<div class="notes">
<?php 
        echo $this->element('LabOrders.map_notes', [
            'mapType' => $mapType,
            'additionalEntity' => $additionalEntity
        ]);
?>
</div>

<div class="confidentiality">
<?php
            if ($this->ContentRegionLoader->regionExists('map_confidentiality_content')) {
                echo $this->ContentRegionLoader->getRegion('map_confidentiality_content');
            }
?>
</div>
