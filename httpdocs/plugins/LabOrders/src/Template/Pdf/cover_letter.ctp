<table style="width: 650pt;">
    <tbody>
        <tr> <td width="50%" style="vertical-align:top;">
                <p><strong>Patient:</strong> <?=$patientFirstName;?> <?=$patientLastName;?><br>
                    <strong>DOB:</strong> <?=$patientDob;?><br>
                    <strong>Gender: </strong><?=$patientGender;?><br><br>

                    <strong>Sample ID:</strong> <?=$sampleId;?><br>
                    <strong>Sample Processed:</strong> <?=$sampleProcessedDate;?><br>
                    <strong>Collection Date:</strong> <?=$collectionDate;?><br>
                    <strong>Report Released: </strong><?= $reportReleasedDate;?></p>
                </p>
            </td>
            <td width="50%" style="vertical-align:top;">
                <p><strong>Pharmacist:</strong> <?=$providerName;?><br>
                <strong>Phone:</strong> <?=$providerPhone;?><br>
                <br>
                <strong>Ordering Provider:</strong> <?=$clinician;?>
                </p>
            </td>
        </tr>
    </tbody>
</table>

<table style="width: 650pt;">
    <tbody>
        <tr>
            <td>
<br><br>
<h4 dir="ltr" style="color: rgb(3, 132, 169); text-align: center;"><b>About Pharmacogenomic Testing</b></h4>

<p>&nbsp;</p>

<p dir="ltr">Genetic variation influences the way drugs are processed and utilized by the body. The science of pharmacogenomics explores the effects specific genetic variants have on those processes.&nbsp;&nbsp;</p>

<p>&nbsp;</p>

<p dir="ltr">The above identified patient was tested using a scientifically vetted panel of genetic variants that are informative for efficacy, safety, and/or dosing information. This report provides detailed pharmacogenomic results and the correlative clinical effects for this particular patient.</p>

<p>&nbsp;</p>

<p dir="ltr">This information is intended to be used by healthcare providers (e.g. pharmacists, physicians, prescribers, etc.) for the purpose of guiding medication therapy decision-making toward improved safety and therapeutic benefit.</p>

<p>&nbsp;</p>

<p><br />
&nbsp;</p>

<p dir="ltr" style="color:#0384A9; text-align:center;"><b><i>Patients should not make any changes to medication therapy<br />
without consulting the prescriber.</i></b></p>

<br><br><br><br><br>
<br><br><br>
<i>Confidentiality Notice: This document, including any attachments, is confidential and solely for the intended recipient. The information in this document contains privileged or sensitive information, including patient information protected by HIPAA, HITECH, the &quot;Genetic Privacy Act&quot; of the state of New Jersey, and other federal and state privacy laws. Any unauthorized review, use, disclosure, or distribution is strictly prohibited. If you are not the intended recipient, please notify the sender and destroy all copies of this document, including any attachments.</i>

<br><br>

<p style="text-align:center;">RxGenomix | (615) 814-2911 | support@rxgenomix.com</p>

            </td>
        </tr>
    </tbody>
</table>
