<p>Thanks, <?= $currentUserFirstName;?>. Your order for <?=$labOrderEntity->patient->first_name;?> <?=$labOrderEntity->patient->last_name;?> has been successfully submitted and will be transmitted to the laboratory.</p>

<p>Please make sure the sample is collected using the recommended process, the iSwab vial is properly sealed, and the sample is sent to the laboratory as soon as possible. If you have any questions, contact RxGenomix right away at (615) 814-2911.</p>

<p>You will receive a notice when results are available for review.</p>
