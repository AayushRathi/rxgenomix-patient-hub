<?php
    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);

    $this->Html->script('/lab_orders/js/LabOrders/provider.js', ['block' => 'extra-scripts']);
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Provider Information</h2>
        <p>Please list the name of the Patient's health care providers.</p>
        <div>
            <?php echo $this->Form->create($order);

                $columns = [
                    [
                        'name' => 'provider_name',
                        'type' => 'text',
                        'label' => 'Provider Name',
                    ],
                    [
                        'name' => 'npi_number',
                        'type' => 'text',
                        'label' => 'NPI',
                    ]
                ];

                if ($order->lab_order_type_id !== 2) {
                    $columns[] = [
                        'name' => 'is_primary_care_provider',
                        'type' => 'checkbox',
                        'label' => 'Primary Care provider',
                        'class' => 'primary-checkbox'
                    ];

                    $columns[] = [
                        'name' => 'is_ordering_physician',
                        'type' => 'checkbox',
                        'label' => 'Ordering Physician',
                        'class' => 'physician-checkbox'
                    ];
                }

                echo $this->Form->control(
                    'providers',
                    [
                        'label' => false,
                        'type' => 'multiinput',
                        'cols' => count($columns),
                        'inputs' => $columns
                    ]
                );

                echo $this->element('form-buttons');
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
