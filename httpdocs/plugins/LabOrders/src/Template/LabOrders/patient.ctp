<?php
    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Order #' . $order->id);

    echo $this->Html->script('/rxg_theme/js/other/jquery.mask.js');
    $this->Html->script('/lab_orders/js/LabOrders/patient.js?cachebust=' . time(), ['block' => 'extra-scripts']);

    // date picker
    $datePickerFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
        "datePickerFormat",
        "Lib24watch",
        "YYYY-MM-DD"
    );
    $datePickerPublicFormatFromSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic(
        "datePickerPublicFormat",
        "Lib24watch",
        "YYYY-MM-DD"
    );

    echo $this->Html->scriptBlock("var datePickerFormatFromSetting='" . $datePickerFormatFromSetting . "';");
    echo $this->Html->scriptBlock("var datePickerPublicFormatFromSetting='" . $datePickerPublicFormatFromSetting . "';");
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Review (or Add) Your Patient Information</h2>
        <p>Please collect the following information from the Patient:</p>
        <div>
            <?= $this->Form->create($order);?>

            <div class="row">
                <div class="col-md-2">
                    <?=$this->Form->control('patient.date_of_birth' ,[
                        'type' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'placeholder' => 'MM-DD-YYYY',
                        'required' => true
                    ])?>
                </div>
            </div>

            <div class="patient-list">
                <ul></ul>
            </div>
            <div class="patient-form">
                <?=$this->Form->control('patient_id' ,[
                    'type' => 'hidden'
                ])?>
                <?=$this->Form->control('patient.first_name' ,[
                    'type' => 'text',
                    'label' => 'First Name',
                    'required' => true
                ])?>

                <?=$this->Form->control('patient.last_name' ,[
                    'type' => 'text',
                    'label' => 'Last Name',
                    'required' => true
                ])?>

                <?=$this->Form->control('patient.ethnicity_id' ,[
                    'type' => 'select',
                    'label' => 'Ethnicity',
                    'required' => true,
                    'empty' => true,
                    'options' => $ethnicities
                ])?>

                <?=$this->Form->control('patient.ethnicity_other',[
                    'type' => 'text',
                    'label' => 'Other Specify'
                ])?>

                <?=$this->Form->control('patient.sex_id' ,[
                    'type' => 'select',
                    'label' => 'Sex',
                    'empty' => true,
                    'required' => true,
                    'options' => $sexes
                ])?>

                <?php

                if ($order->selectedLabId === \Labs\Model\Table\LabsTable::LAB_ID_RESOLVE) {
                    echo $this->Form->control(
                        'patient.address_1',
                        [
                            'label' => 'Street Address',
                            'required' => true,
                            'type' => 'text'
                        ]
                    );
                    echo $this->Form->control(
                        'patient.address_2',
                        [
                            'label' => false,
                            'required' => false,
                            'type' => 'text'
                        ]
                    );
                    echo $this->Form->control(
                        'patient.city',
                        [
                            'label' => 'City',
                            'required' => true,
                            'type' => 'text'
                        ]
                    );
                }
                ?>

                <?=$this->Form->control('patient.lib24watch_state_abbr' ,[
                    'type' => 'select',
                    'label' => 'State',
                    'empty' => true,
                    'required' => true,
                    'options' => $states
                ])?>

                <?php
                if ($order->selectedLabId === \Labs\Model\Table\LabsTable::LAB_ID_RESOLVE) {
                    echo $this->Form->control(
                        'patient.postal_code',
                        [
                            'type' => 'text',
                            'label' => 'Zip',
                            'required' => true
                        ]
                    );
                }
                ?>

                <?php
                if ($hasChildGroups) {
                    echo $this->Form->control('patient.client_id' ,[
                        'type' => 'select',
                        'label' => 'Client ID',
                        'empty' => true,
                        'required' => true,
                        'options' => $clientIds
                    ]);
                } else {
                    echo $this->Form->control('client_name' ,[
                        'type' => 'text',
                        'label' => 'Client ID',
                        'disabled' => true,
                        'required' => true,
                        'value' => $clientName
                    ]);
                    echo $this->Form->control('patient.client_id' ,[
                        'type' => 'hidden',
                        'label' => false,
                        'value' => $clientIds
                    ]);
                }
                ?>

                <?php
                // PGX orders only have these
                if ($order->lab_order_type_id === \LabOrders\Model\Table\LabOrdersTable::LAB_ORDER_PGX_ID) {
                    echo $this->Form->control('patient.card_id' ,[
                        'type' => 'text',
                        'label' => 'Card ID'
                    ]); 

                    echo $this->Form->control('patient.person_code', [
                        'type' => 'text',
                        'label' => 'Person Code'
                    ]);
                }
                ?>

                <?php
                $patientEmailLabel = 'Email';
                if ($order->lab_order_type_id === \LabOrders\Model\Table\LabOrdersTable::LAB_ORDER_COVID_ID) {
                    $patientEmailLabel = 'Patient Email';
                }

                echo $this->Form->control('patient.email' ,[
                    'type' => 'text',
                    'label' => $patientEmailLabel
                ])?>

                <?php
                $patientPhoneLabel = 'Phone';
                if ($order->lab_order_type_id === \LabOrders\Model\Table\LabOrdersTable::LAB_ORDER_COVID_ID) {
                    $patientPhoneLabel = 'Patient Phone';
                }
                echo $this->Form->control('patient.phone' ,[
                    'type' => 'text',
                    'label' => $patientPhoneLabel,
                    'required' => true
                ])?>
            </div>

            <?php
                echo $this->element('form-buttons');
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
