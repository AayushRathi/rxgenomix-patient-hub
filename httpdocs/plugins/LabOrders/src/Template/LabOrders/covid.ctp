<?php
$this->assign('title', 'Orders');
$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);

echo $this->Html->script('/rxg_theme/js/other/jquery.mask.js', ['block' => 'extra-scripts']);
echo $this->Html->script('/lab_orders/js/LabOrders/covid.js', ['block' => 'extra-scripts']);
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: COVID Information</h2>
<?php echo $this->Form->create($order, [
    //'novalidate' => 'novalidate'
]);
            ?>
            <div class="row">
                <div class="col-md-12">
                <?php
                    echo $this->Form->control(
                        'specimen_barcode',
                        [
                            'label' => 'Specimen Barcode',
                            'type' => 'text',
                            'required' => true,
                            'placeholder' => 'eg., ISM010203',
                        ]
                    );

                    echo $this->Form->control(
                        'specimen_collection_date',
                        [
                            'label' => 'Specimen Collection Date',
                            'type' => 'datetimepicker',
                            'required' => true
                        ]
                    );

                    echo $this->Form->control(
                        'specimen_time',
                        [
                            'label' => 'Specimen Collection Time',
                            'type' => 'timepicker',
                            'required' => true
                        ]
                    );

                    echo $this->Form->control(
                        'specimen_identifier',
                        [
                            'label' => 'Patient DOB Identifier (write this on specimen tube)',
                            'placeholder' => 'Patient DOB',
                            'type' => 'text',
                            'required' => true
                        ]
                    );

                    echo $this->Form->control(
                        'specimen_type_id',
                        [
                            'label' => 'Specimen Type',
                            'type' => 'select',
                            'empty' => 'Select a Specimen Type',
                            'required' => true,
                            'options' => $specimenTypesList,
                        ]
                    );

                    echo $this->Form->control(
                        'test_required',
                        [
                            'disabled' => true,
                            'label' => 'SARS-CoV-2 (PCR)',
                            'type' => 'checkbox',
                            'checked' => true,
                            'required' => true,
                            'templates' => [
                                'checkboxFormGroup' => '<label>Test Requested</label> {{label}}',
                            ]
                        ]
                    );

                    echo $this->Form->control(
                        'patient.id',
                        [
                            'type' => 'hidden'
                        ]
                    );

                    $selectedIcd10Codes = [];
                    if (
                        isset($order->patient->patient_icd10_codes) &&
                        !empty($order->patient->patient_icd10_codes)
                    ) {
                        foreach ($order->patient->patient_icd10_codes as $entity) {
                            if (
                                $entity instanceof \Cake\ORM\Entity &&
                                isset($entity->icd10_code_id) &&
                                $entity->icd10_code_id > 0
                            ) {
                                $selectedIcd10Codes[] = $entity->icd10_code_id;
                            }
                        }
                    }
?>

<div class="icd10-wrapper pt-2">

<div class="required">
    <label>ICD-10 CODE(S)</label>
<?php
                    if (
                        !empty($order->getErrors()) && 
                        !empty($order->getErrors()['patient']) &&
                        isset($order->getErrors()['patient']['patient_icd10_codes']) &&
                        !empty($order->getErrors()['patient']['patient_icd10_codes'])
                    ) {
?>
<div class="error-message">ICD10 Code is required</div>
<?php
                    }
?>

</div>

<div class="row">
<?php
                    foreach ($icd10Codes as $icd10Code) {
                        echo $this->Form->control('patient.patient_icd10_codes.'.$icd10Code['id'], [
                            'type' => 'checkbox',
                            'label' => $icd10Code['code'] . ' ' . $icd10Code['title'],
                            'checked' => in_array($icd10Code['id'], $selectedIcd10Codes),
                            'templates' => [
                                'inputContainer' => '<div class="col-md-6 input {{type}}{{required}}">{{content}}</div>',
                            ]
                        ]);
                    }
                ?>
</div>
</div>
                </div>
            </div>
            <?php
            echo $this->element('form-buttons');

            echo $this->Form->end(); ?>
        </div>
    </div>
</div>
