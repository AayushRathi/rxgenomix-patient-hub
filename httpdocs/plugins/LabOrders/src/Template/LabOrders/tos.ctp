<?php
    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Terms of Service.</h2>
        <p>Please review the following terms of service with the Patient:</p>
        <div>
            <?php echo $this->Form->create($order);

            foreach ($termsOfServices as $termsOfService){
                $modalId = 'tosModal' . rand();
                ?>
                <div></div>
                <div>
                    <a href="javascript:;" data-toggle="modal" data-target="#<?=$modalId?>" style="text-decoration: underline;">
                        View Terms of Service
                    </a>
                </div>
                <div class="modal" role="dialog" id="<?=$modalId?>">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">RxGenomix, LLC Terms of Service</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body tos-body">
                                <?= $termsOfService->description ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="block-list">
                <?php
                foreach($termsOfService->checkboxes as $checkbox){
                    ?>
                    <li>
                        <?=$this->Form->control('checkbox_' . $checkbox->id ,[
                                'type' => 'checkbox',
                                'label' => $checkbox->description,
                                'required' => true
                        ])?>
                    </li>
                    <?php
                } ?>
                </ul>
                <?php
            }

            echo $this->element('form-buttons');

            echo $this->Form->end(); ?>
        </div>
    </div>
</div>
