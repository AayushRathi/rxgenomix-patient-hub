<?php
$this->assign('title', 'Orders');
$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);
?>
<script>
$(function() {

    $('#medications').on('rowAdded', function() {
        $('#medications .row:last .ui-autocomplete-input').autocomplete( "option", "autoFocus", true );
    });

    $('.ui-autocomplete-input').autocomplete( "option", "autoFocus", true );

});
</script>
<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Medications</h2>
        <p>To run all current medications against patient's genetic profile, list below:</p>
            <?php echo $this->Form->create($order);
            ?>
            <div class="row">
                <div class="col-md-12">
                <?php
                    echo $this->Form->control(
                    'medications',
                    [
                        'label' => 'Medication',
                        'type' => 'multiinput',
                        'cols' => 1,
                        'inputs' => [
                            [
                                'name' => 'drug_id',
                                'type' => 'autocomplete',
                                'label' => false,
                                'id' => 'medication-autocomplete',
                                'autocompleteVars' => [
                                    'method' => 'GET',
                                    'sourceUrl' => $this->Url->build(
                                        [
                                            'plugin' => 'SystemManagement',
                                            'controller' => 'Drug',
                                            'action' => 'autocompleteDrugs',
                                            'prefix' => false,
                                            '_method' => 'GET'
                                        ],
                                        ['escape' => false]
                                    ),
                                    //'callbackField' => $this->Form->control('drug_id', ['type' => 'hidden']),
                                ],
                                'templateVars' => [
                                    //'help' => 'Type the name of the medication'
                                ]
                            ]
                        ]
                    ]
                );
                ?>
                </div>
            </div>
            <?php
            echo $this->element('form-buttons');

            echo $this->Form->end(); ?>
        </div>
    </div>
</div>
