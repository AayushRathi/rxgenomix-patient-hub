<?php
    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Order');

    $patientName = '';
    if (
        isset($order->patient) && 
        isset($order->patient->first_name) && 
        isset($order->patient->last_name) &&
        mb_strlen($order->patient->first_name) > 0 &&
        mb_strlen($order->patient->last_name) > 0
    ) {
        $patientName = $order->patient->first_name . ' ' . $order->patient->last_name;
    }
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Confirmation</h2>
        <p></p>
        <div>
            <h3>Success! Order #<?=$order->id?> with RxGenomix has been placed.</h3>
            <p>Your order for <?=$patientName;?> has been submitted. Should any additional information be required, we'll contact you. Thank you.</p>
            <?php
                $this->Html->link(
                    'Home',
                    [
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'index'
                    ],
                    [
                        'class' => 'btn btn-condensed btn-primary pull-left'
                    ]
                );
                $this->Html->link(
                    'View Order',
                    [
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'view',
                        $order->id
                    ],
                    [
                        'class' => 'btn btn-condensed btn-primary pull-right'
                    ]
                );
            ?>
        </div>
    </div>
</div>
