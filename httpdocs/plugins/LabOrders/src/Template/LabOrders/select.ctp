<?php
$this->assign('title', 'Orders');
$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);

?>
<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Select a test type</h2>
            <div class="row">
                <div class="col-md-6">
<?php
echo $this->Form->create(null);

echo $this->Form->control('lab_order_type_id', [
    'type' => 'select',
    'options' => $labOrderTypes,
    'empty' => 'Select a test type',
    'required' => true
]);

echo $this->Form->submit('Start Lab Order', [
    'class' => 'btn btn-primary mt-2',
]);

echo $this->Form->end();
?>
            </div>
        </div>
    </div>
</div>
