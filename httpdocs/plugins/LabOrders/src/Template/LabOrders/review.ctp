<?php
    use App\Util\FormattingHelper;

    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Order #' . $order->id);
?>

<div class="row">
    <h2 class="title-decoration">Step <?= $stepNumber; ?>: Review All Information</h2>
</div>
<div class="row">
    <p>Please review the following information:</p>
</div>
<div class="row">
    <div class="col-md-6">
        <h3>
            <?php echo $this->Html->link(
                '<span class="fa fa-caret-square-o-left"></span>',
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'patient',
                    $order->id
                ],
                [
                    'escape' => false,
                    'title' => 'Edit Patient Information'
                ]
            ); ?>&nbsp;
            Patient Information</h3>
        <?php echo $this->element('LabOrders.section_patient_information');?>
    </div>
    <div class="col-md-6">
        <h3>
<?php
    if ($order->lab_order_type_id === 1) {
    echo $this->Html->link(
                '<span class="fa fa-caret-square-o-left"></span>',
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'shipping',
                    $order->id
                ],
                [
                    'escape' => false,
                    'title' => 'Edit Shipping Information'
                ]
            ); ?>&nbsp;
<?php } ?>Sample/Collection Kit</h3>
        <?php
    if ($order->lab_order_type_id === 1) {
        echo $this->element('LabOrders.section_shipping');
    } else {?>Onsite<?php }?>
    </div>
</div>
<br>
<hr>
<br>
<div class="row">
    <div class="col-md-6">
        <h3>
            <?php echo $this->Html->link(
                '<span class="fa fa-caret-square-o-left"></span>',
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'provider',
                    $order->id
                ],
                [
                    'escape' => false,
                    'title' => 'Edit Provider Information'
                ]
            ); ?>&nbsp;
            Provider Information</h3>
        <?php echo $this->element('LabOrders.section_provider');?>
    </div>

    <?php
        if ($order->lab_order_type_id === 1) {
    ?>
    <div class="col-md-6">
        <h3>
            <?php echo $this->Html->link(
                '<span class="fa fa-caret-square-o-left"></span>',
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'medications',
                    $order->id
                ],
                [
                    'escape' => false,
                    'title' => 'Edit Medications'
                ]
            ); ?>&nbsp;
            Medications</h3>
            <?php echo $this->element('LabOrders.section_medications');?>
    </div>
    <?php } else if ($order->lab_order_type_id === 2) {
?>
    <div class="col-md-6">
        <h3>
            <?php echo $this->Html->link(
                '<span class="fa fa-caret-square-o-left"></span>',
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'covid',
                    $order->id
                ],
                [
                    'escape' => false,
                    'title' => 'Edit Covid Information'
                ]
            ); ?>&nbsp;
            Covid</h3>
            <span>Specimen Barcode:</span>
            <?= h($order->specimen_barcode);?>
            <br>
            <span>Specimen Collection Date:</span> <?= lib24watchDate('shortDateFormat', $order->specimen_collection_date, +5); ?>
            <br>
            <span>Specimen Collection Time:</span> <?= lib24watchDate('timeFormat', $order->specimen_time, 0); ?>
            <br>
            <span>Patient Second Identifier:</span> <?= $order->specimen_identifier; ?>
            <br>
            <span>Specimen Type:</span> <?= $specimenTypesList[$order->specimen_type_id]; ?>

<br><br>
            <?php
            $selectedIcd10Codes = [];
            if (
                isset($order->patient->patient_icd10_codes) &&
                !empty($order->patient->patient_icd10_codes)
            ) {
                foreach ($order->patient->patient_icd10_codes as $entity) {
                    if (
                        $entity instanceof \Cake\ORM\Entity &&
                        isset($entity->icd10_code_id) &&
                        $entity->icd10_code_id > 0
                    ) {
                        $selectedIcd10Codes[] = $entity->icd10_code_id;
                    }
                }
            }

            if (isset($icd10Codes) && !empty($icd10Codes)) {
?>
<div class="icd10-wrapper pt-2">

<div>
    <label>ICD-10 CODE(S)</label>
</div>

<div class="row">
<?php
                    foreach ($icd10Codes as $icd10Code) {
                        echo $this->Form->control('patient.patient_icd10_codes.'.$icd10Code['id'], [
                            'type' => 'checkbox',
                            'disabled' => true,
                            'label' => $icd10Code['code'] . ' ' . $icd10Code['title'],
                            'checked' => in_array($icd10Code['id'], $selectedIcd10Codes),
                            'templates' => [
                                'inputContainer' => '<div class="col-md-6 input {{type}}{{required}}">{{content}}</div>',
                            ]
                        ]);
                    }

?>
</div>
<?php
            }
                ?>
</div>
    </div>
<?php } ?>
</div>

<?php
echo $this->Form->create($order);
?>
<div class="row">
    <div class="col-md-6">
        <?php
            echo $this->Form->submit(
                'Previous',
                [
                    "type" => "submit",
                    "name" => "previous",
                    "div" => false,
                    "class" => "btn btn-primary pull-left"
                ]
            );
        ?>
    </div>
    <div class="col-md-6">
        <?php
            echo $this->Form->submit(
                'Save Order',
                [
                    "type" => "submit",
                    "name" => "save-only",
                    "div" => false,
                    "class" => "btn btn-primary pull-right"
                ]
            );
        ?>

        <?php
            if ($providerStatus == 2) {
                if (isset($order->provider) && isset($order->provider->email) && strpos($order->provider->email, '@orases.com') !== false) {
                    $healthCareClient = $order->provider->health_care_client;
                    if ($order->lab_order_type_id  == \Cake\ORM\TableRegistry::getTableLocator()->get('LabOrders.LabOrders')::LAB_ORDER_PGX_ID) {
                        $labId = $healthCareClient->lab_id;
                    } else {
                        $labId = $healthCareClient->covid_lab_id;
                    }

                    $labName = '';
                    if (is_null($labId)) {
                        $labName = ' Covid lab not assigned (must select in health care client)';
                    } else {
                        $lab = new \Labs\Libs\Lab($labId);
                        if (isset($lab->labEntity)) {
                            $labName = ' to ' . $lab->labEntity->lab_name;
                        }
                    }
                }

                echo $this->Form->submit('Submit Order' . $labName,
                    [
                        "type" => "submit",
                        "name" => "submit-order",
                        "div" => false,
                        "class" => "btn btn-primary pull-right"
                    ]
                );
            }
        ?>
    </div>
</div>

<?php
echo $this->Form->end();
?>
