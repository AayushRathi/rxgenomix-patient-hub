<?php
$pageHeading = "Browse Orders";
$this->assign('title', 'Orders');

$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders');

echo $this->Html->css('/lab_orders/css/lab_order_index.css');

$labOrdersInstance = new \LabOrders\Libs\LabOrders();
?>
<style>
    /* TODO: get rid of this terrible hack, workaround for Bootstrap grid */
    .col-4 {
        width: 33.333333333%;
        float: left;
        margin-bottom: 6px;
    }

    .map-button-container {
        padding-top: 0.5rem;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <?php
        echo $this->cell('Lib24watch.Table::filter', [
            'Orders',
            'items' => [
                /*[
                    'name' => 'title',
                    'type' => 'text',
                    'label' => 'Patient Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->matching('Patient', function ($q) use ($value) {
                            return $q->where([
                                'OR' => [
                                    ['first_name LIKE' => '%' . $value . '%'],
                                    ['last_name LIKE' => '%' . $value . '%'],
                                    ['CONCAT(first_name, " ", last_name) LIKE' => '%' . $value . '%']
                                ]
                            ]);
                        });
                    }
                ],*/
                [
                    'name' => 'first_name',
                    'type' => 'text',
                    'label' => 'Patient First Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->where(['Patient.first_name LIKE' => '%' . $value . '%']);
                    }
                ],
                [
                    'name' => 'last_name',
                    'type' => 'text',
                    'label' => 'Patient Last Name',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->where(['Patient.last_name LIKE' => '%' . $value . '%']);
                    }
                ],
                [
                    'name' => 'phone',
                    'type' => 'text',
                    'label' => 'Patient Phone',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function ($query, $value) {
                        return $query->where(['Patient.phone LIKE' => '%' . $value . '%']);
                    }
                ],
                [
                    'name' => 'id',
                    'type' => 'text',
                    'label' => 'Order #',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function($query, $value){
                        return $query->where(['LabOrders.id' => $value]);
                    }
                ],
                [
                    'name' => 'sample_id',
                    'type' => 'text',
                    'label' => 'Sample ID',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'lab_order_status_id',
                    'type' => 'select',
                    'label' => 'Status',
                    'empty' => true,
                    'options' => $statuses,
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'lab_order_type_id',
                    'type' => 'Select',
                    'label' => 'Test Type',
                    'options' => ['' => 'All'] + $labOrderTypesList->toArray(),
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ]
                ],
                [
                    'name' => 'created',
                    'type' => 'datetimepicker',
                    'label' => 'Date Ordered',
                    'templates' => [
                        'inputContainer' => '<div class="input {{type}}{{required}} col-4">{{content}}</div>',
                    ],
                    'function' => function($query, $value){
                        return $query->where(['DATE(LabOrders.created)' => $value]);
                    }
                ]
            ],
            $labOrders,
            $this,
            [
                'showCollapse' => false
            ]
        ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'showCollapse' => false,
                    'extraControls' => [
                    ]
                ]
            );
            echo $this->Html->link(
                "<span class=\"fa fa-plus\"></span> Create New Order",
                [
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'select',
                ],
                [
                    'escape' => false,
                    'class' => 'btn btn-primary pull-right',
                    'title' => 'Create New Order'
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php
                    if ($labOrders->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'id',
                                        'label' => 'Order #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'sample_id',
                                        'label' => 'Sample #',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return h(strip_tags($row->sample_id));
                                        }
                                    ],
                                    [
                                        'name' => 'patient_id',
                                        'label' => 'Patient Name',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $row->patient ? h(strip_tags($row->patient->first_name . ' ' . $row->patient->last_name)) : '';
                                        }
                                    ],
                                    [
                                        'name' => 'lab_order_status_id',
                                        'label' => 'Kit Status',
                                        'type' => 'lookup',
                                        'list' => $statuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) use ($labOrdersInstance) {
                                            $order_complete = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('minimum_status_order_complete', 'LabOrders', 3);
                                            $report_available = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('status_report_complete', 'LabOrders', 7);

                                            $returnHtml = $this->Html->link(
                                                '<span class="fa fa-file"></span> View',
                                                [
                                                    'plugin' => 'LabOrders',
                                                    'controller' => 'LabOrders',
                                                    'action' => 'view',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary',
                                                    'alt' => 'View the information submitted with the order'
                                                ]
                                            );

                                            if((int)$row['lab_order_status_id'] < (int)$order_complete) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-edit"></span> Edit',
                                                    [
                                                        'plugin' => 'LabOrders',
                                                        'controller' => 'LabOrders',
                                                        'action' => 'edit',
                                                        $row->id
                                                    ], [
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-primary'
                                                    ]
                                                );
                                            }

                                            if((int)$row['lab_order_status_id'] === (int)$report_available) {
                                                $returnHtml .= $this->Html->link(
                                                    '<span class="fa fa-download"></span> Report',
                                                    [
                                                        'plugin' => 'LabOrders',
                                                        'controller' => 'LabOrders',
                                                        'action' => 'download',
                                                        $row->id
                                                    ], [
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-primary',
                                                        'target' => '_blank',
                                                        'alt' => 'View the full PGx laboratory report. This report is for use by the health care team only. Not for use by the patient.'
                                                    ]
                                                );

                                                if ($row['pharmacogenomic_provider_id'] == 2) {
                                                    $isMapReady = $labOrdersInstance->isMapReady($row['id']);

                                                    if ($isMapReady) {
                                                        $returnHtml .= $this->Html->link(
                                                            '<span class="fa fa-edit"></span> Review MAP',
                                                            [
                                                                'plugin' => 'LabOrders',
                                                                'controller' => 'LabOrders',
                                                                'action' => 'reviewMap',
                                                                $row->id
                                                            ], [
                                                                'escape' => false,
                                                                'class' => 'btn btn-condensed btn-primary',
                                                                'alt' => 'Review the medication action plan.'
                                                            ]
                                                        );

                                                        $mapTypes = \LabOrders\Libs\LabOrders::getActiveMapTypes();
                                                        $mapsHtml = '';
                                                        $mapsShown = 0;
                                                        foreach ($mapTypes as $slug => $type) {
                                                            $mapReady = $labOrdersInstance->hasMap($row->id, $slug);

                                                            // TODO: do we need a new lab order status?
                                                            if ($mapReady) {
                                                                $mapsHtml .= $this->Html->link(
                                                                    '<span class="fa fa-download"></span>' . $type['title'] . ' MAP',
                                                                    [
                                                                        'plugin' => 'LabOrders',
                                                                        'controller' => 'LabOrders',
                                                                        'action' => 'downloadMap',
                                                                        $row->id,
                                                                        $slug
                                                                    ], [
                                                                        'escape' => false,
                                                                        'class' => 'btn btn-condensed btn-primary',
                                                                        'target' => '_blank',
                                                                        'alt' => 'View the ' . $slug . ' medication action plan.'
                                                                    ]
                                                                );

                                                                $mapsShown++;
                                                            }
                                                        }

                                                        if ($mapsShown > 0) {
                                                            $returnHtml .= '<div class="map-button-container">' . $mapsHtml . '</div>';
                                                        }
                                                    }
                                                }
                                            }

                                            return $returnHtml;
                                        }
                                    ]
                                ],
                                $labOrders,
                                [
                                    'showCollapse' => false,
                                    'class' => 'table table-striped lab-orders-index',
                                ]
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to
                            <?php
                            echo $this->Html->link(
                                'Create New Order',
                                [
                                    'plugin' => 'LabOrders',
                                    'controller' => 'LabOrders',
                                    'action' => 'select',
                                ],
                                [
                                    'title' => 'Create New Order'
                                ]
                            );
                            ?>.
                        </p>
                        <?php
                    }
                    ?>
                </div>
                <div class="report-use-warning">This report is for use by the health care team only. Not for use by the patient.</div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
            <?php
            /*echo $this->Html->link(
                'Access GeneDose Live 2.0',
                'https://app.genedose.com/#home',
                [
                    'class' => 'btn btn-primary pull-right',
                    'target' => '_blank'
                ]
            );*/
            ?>
        </div>
    </div>
</div>
