<?php
$this->assign('title', 'Orders');
$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Promo Code</h2>
        <p>Does the Patient have a promotional code? If yes, please enter it in the box below:</p>
        <div class="row">
            <div class="col-md-6">
            <?= $this->Form->create($order);?>

            <?=$this->Form->control('promo_code' ,[
                'type' => 'text',
                'label' => 'Promo Code'
            ])?>
            <?=$this->Form->control('promo_form' ,[
                'type' => 'hidden',
                'value' => '1'
            ])?>
            </div>
        </div>
        <div>
            <?php
                echo $this->element('form-buttons');
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
