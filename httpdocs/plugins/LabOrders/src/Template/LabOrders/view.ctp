<?php
    use App\Util\FormattingHelper;

    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
    $this->Breadcrumbs->add('Order #' . $order->id);
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Lab Order #<?= $order->id; ?></h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Patient Information</h3>
                <?php echo $this->element('LabOrders.section_patient_information');?>
            </div>
            <div class="col-md-6">
                <h3>Shipping Information (if applicable)</h3>
        <?php 
    if ($order->lab_order_type_id === 1) { 
        echo $this->element('LabOrders.section_shipping');
    } else {?>Onsite<?php }?>
            </div>
        </div>
        <br>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-6">
                <h3>Provider Information</h3>
                <?php echo $this->element('LabOrders.section_provider');?>
            </div>
            <?php if ($order->lab_order_type_id === 1) { ?>
            <div class="col-md-6">
                <h3>Medications</h3>
                <?php echo $this->element('LabOrders.section_medications'); ?>
            </div>
            <?php } else { ?>
    <div class="col-md-6">
        <h3>Covid</h3>
            <span>Specimen Barcode:</span>
            <?= h($order->specimen_barcode);?>
            <br>
            <span>Specimen Collection Date:</span> <?php 
                if (!is_null($order->specimen_collection_date)) { 
                    echo lib24watchDate('shortDateFormat', $order->specimen_collection_date, +5);
                } else { echo 'N/A';} ?>
            <br>
            <span>Specimen Collection Time:</span> <?php 
                if (!is_null($order->specimen_time)) { 
                    echo lib24watchDate('timeFormat', $order->specimen_time, 0);
                } else { echo 'N/A';} ?>
            <br>
            <span>Patient Second Identifier:</span> <?= $order->specimen_identifier; ?>
            <br>
            <span>Specimen Type:</span> <?php
                    if (!is_null($order->specimen_type_id)) {
                        echo $specimenTypesList[$order->specimen_type_id];
                    } else { echo 'N/A'; }

?>
<br><br>
<?php

            $selectedIcd10Codes = [];
            if (
                isset($order->patient->patient_icd10_codes) &&
                !empty($order->patient->patient_icd10_codes)
            ) {
                foreach ($order->patient->patient_icd10_codes as $entity) {
                    if (
                        $entity instanceof \Cake\ORM\Entity &&
                        isset($entity->icd10_code_id) &&
                        $entity->icd10_code_id > 0
                    ) {
                        $selectedIcd10Codes[] = $entity->icd10_code_id;
                    }
                }
            }

            if (isset($icd10Codes) && !empty($icd10Codes)) {
?>
<div class="icd10-wrapper pt-2">

<div>
    <label>ICD-10 CODE(S)</label>
</div>

<div class="row">
<?php
                    foreach ($icd10Codes as $icd10Code) {
                        echo $this->Form->control('patient.patient_icd10_codes.'.$icd10Code['id'], [
                            'type' => 'checkbox',
                            'disabled' => true,
                            'label' => $icd10Code['code'] . ' ' . $icd10Code['title'],
                            'checked' => in_array($icd10Code['id'], $selectedIcd10Codes),
                            'templates' => [
                                'inputContainer' => '<div class="col-md-6 input {{type}}{{required}}">{{content}}</div>',
                            ]
                        ]);
                    }

?>
</div>
<?php
            }
                ?>
</div>
    </div>
            <?php } ?>
        </div>
    </div>
</div>
