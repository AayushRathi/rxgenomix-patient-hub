<?php
$pageHeading = "Complete MAP";
$this->assign('title', 'Orders');
#<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js"></script>

$this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
$this->Breadcrumbs->add('Orders');
$this->Html->script("/Lib24watch/js/plugins/ckeditor/ckeditor", ['block' => 'extra-scripts']);
$this->Html->script("/Lib24watch/js/plugins/ckeditor/adapters/jquery", ['block' => 'extra-scripts']);
$this->Html->script('/lab_orders/js/LabOrders/review_map.js?cachebust=' . time(), ['block' => 'extra-scripts']);
$this->Html->script('/rxg_theme/js/select2.min.js', ['block' => 'extra-scripts']);

echo $this->Html->css('/rxg_theme/css/select2.min.css');
echo $this->Html->css('/lab_orders/css/review_map.css?cachebust=' . time());

$labOrdersInstance = new \LabOrders\Libs\LabOrders();

$showProblemByDefault = true;

$alternativeMap = [];
foreach ($alternativeResults as $alternativeResult) {
    $alternativeNode = [
        'tsi_severity' => $alternativeResult['tsi_severity'],
        'tsi_detail' => $alternativeResult['tsi_detail'],
    ];

    $alternativeMap[$alternativeResult->id] = $alternativeNode;
}
?>
<script>
window.alternativeResults = <?php echo json_encode($alternativeMap);?>;
window.labOrderId = <?php echo $labOrderId;?>;
window.labOrderReportId = <?php echo $labOrderReportEntity->id;?>;
window.getMapProblemRationalsBaseUrl = '<?php echo $this->Url->build([
    'plugin' => 'LabOrders',
    'prefix' => false,
    'controller' => 'LabOrders',
    'action' => 'getMapProblemRationales',
    '_full' => true,
]);?>';

window.saveMapResultItemBaseUrl = '<?php echo $this->Url->build([
    'plugin' => 'LabOrders',
    'prefix' => false,
    'controller' => 'LabOrders',
    'action' => 'saveMapResultItem',
    '_full' => true,
]);?>';

window.addMedicationToLabOrderBaseUrl = '<?php echo $this->Url->build([
    'plugin' => 'LabOrders',
    'prefix' => false,
    'controller' => 'LabOrders',
    'action' => 'addMedicationToLabOrder',
    '_full' => true,
]);?>';

window.getMedicationInfoBaseUrl = '<?php echo $this->Url->build([
    'plugin' => 'LabOrders',
    'prefix' => false,
    'controller' => 'LabOrders',
    'action' => 'getMedicationInfo',
    '_full' => true,
]);?>';

window.showProblemByDefault = <?php echo json_encode($showProblemByDefault);?>

$(function() {
    $('.selected-drug-container select').select2();
});
</script>
<div class="col-md-12" id="map-container">
        <div class="row">
                <div class="panel panel-default tabs">
                        <?php
                        echo $this->Form->create($labOrderReportEntity, [
                            'url' => $currentUrl,
                            #'novalidate' => true
                        ]);
                        ?>
                        <div class="panel-body" style="clear:both;">
                                <div class="row">
                    <div class="col-md-12">
                        <h3>Complete MAP for Lab Order #<?=$labOrderReportEntity->lab_order_id;?></h3>

                        <p>In order to provide a complete Medication Action Plan to patients and providers, you are required to fill in all fields listed below. If you do not have a recommendation for a specific field, please type "No Recommendation" or something similar.</p>

                        <h4>Patient Information</h4>
                        <span>Name:</span> <?= h($labOrder->patient->first_name . ' ' . $labOrder->patient->last_name); ?><br><br>

                        <div class="row score-container current-risk-score">
                            <div class="col-md-6">
                                <h5>Current Risk Score</h5>
                                <span><?=$currentRiskScore;?></span>

                            </div>
                            <div class="col-md-6 future-risk-score">
                                <h5>Future Risk Score</h5>
                                <span><?=$futureRiskScore;?></span>
                            </div>
                        </div>
<?php
                            //echo $this->element('LabOrders.score_breakdown');

?>

<hr>

<div id="add-medication">
    <h2>Add Additional Medications</h2>
    <p>The medications listed on the Lab Order are already included in this MAP. To add additional medications to the MAP, use the field below. You may add multiple medications to the MAP, but they must be added one at a time.</p>
<br>

<?php
                            echo $this->Form->control('add_additional_medication', [
                                'type' => 'autocomplete',
                                'label' => 'Medication:',
                                'id' => 'add_additional_medication_search',
                                'autocompleteVars' => [
                                    'sourceUrl' => $this->Url->build(
                                        [
                                            'plugin' => 'SystemManagement',
                                            'controller' => 'Drug',
                                            'action' => 'autocompleteDrugs',
                                            'prefix' => false,
                                            '?' => [
                                                // example of passing parameters
                                                'active' => 1,
                                                'labOrderId' => $labOrderId,
                                            ],
                                            '_method' => 'GET'
                                        ],
                                        [ 'escape' => false ]
                                    ),
                                    'callbackFunction' => 'addAdditionalMedicationCallback',
                                    'cleanupCallbackFunction' => 'clearAddMedicationCallback',
                                    // example of callback field, storing id value
                                    'callbackField' => $this->Form->control(
                                        'medication_id', 
                                        [
                                            'type' => 'hidden'
                                        ]
                                    ),
                                    'method' => 'GET'
                                ],
                                'templates' => [
                                    'inputContainer' => '<div class="row input {{type}}{{required}}">{{content}}</div>',
                                    'label' => '<div class="col-md-1 pt-2"><label{{attrs}}>{{text}}</label></div>',
    'autocompleteWidget' => '<div class="col-md-4"><div class="autocomplete-widget {{type}} {{required}}" {{attrs}}>
                    <label class="control-label">{{label}}</label> {{autocompleteInput}} {{callbackField}} {{error}}
                </div><div id="add-medication-severity-level"></div></div><div class="col-md-2 pt-2"><input disabled type="submit" id="button-add-medication-submit" value="Add"><input type="submit" id="button-clear-medication" value="Clear"></div>',
                                ],
                                'placeholder' => 'Enter medication name...',
                            ]);
?>

</div>

<hr>

<?php

                            $lastIndex = count($labOrderReportEntity->results)-1;

                            foreach ($labOrderReportEntity->results as $index => $result) {
                                if (isset($result->result_map_note)) {
                                    echo $this->element('LabOrders.result_map_note_alt', [
                                        'index' => $index,
                                        'result' => $result,
                                        'lastIndex' => $lastIndex,
                                        'showProblemByDefault' => $showProblemByDefault
                                    ]);
                                }
                            }

                        echo $this->Form->control(
                            'report_additional.additional_pharmacist_notes_prescriber',
                            [
                                'class' => 'form-control editor',
                                'type' => 'textarea',
                                'label' => 'Additional Pharmacist Notes - Prescriber',
                                'required' => 'required'
                            ]
                        );

                        echo $this->Form->control(
                            'report_additional.additional_pharmacist_notes_patient',
                            [
                                'class' => 'form-control',
                                'type' => 'textarea',
                                'label' => 'Additional Pharmacist Notes - Patient',
                                'required' => 'required'
                            ]
                        );

                        echo $this->Form->control(
                            'report_additional.adverse_drug_reaction_notes_prescriber',
                            [
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'label' => 'Adverse Drug Reaction Notes - Prescriber',
                                'required' => 'required'
                            ]
                        );

                        echo $this->Form->control(
                            'report_additional.adverse_drug_reaction_notes_patient',
                            [
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'label' => 'Adverse Drug Reaction Notes - Patient',
                                'required' => 'required'
                            ]
                        );

                        echo $this->Form->control(
                            'report_additional.lifestyle_recommendation_notes_prescriber',
                            [
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'label' => 'Lifestyle Recommendation Notes - Prescriber',
                                'required' => 'required'
                            ]
                        );

                        echo $this->Form->control(
                            'report_additional.lifestyle_recommendation_notes_patient',
                            [
                                'type' => 'textarea',
                                'class' => 'form-control',
                                'label' => 'Lifestyle Recommendation Notes - Patient',
                                'required' => true
                            ]
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
                $footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
