<?php
    $this->assign('title', 'Orders');
    $this->Breadcrumbs->add('Home', [ 'plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home']);
    $this->Breadcrumbs->add('Orders', [ 'plugin' => 'LabOrders', 'controller' => 'LabOrders', 'action' => 'index']);
$this->Breadcrumbs->add('Order #' . $order->id);

    $this->Html->script('/lab_orders/js/LabOrders/shipping.js', ['block' => 'extra-scripts']);
?>
<div class="row">
    <div class="col-md-12">
        <h2 class="title-decoration">Step <?= $stepNumber; ?>: Sample/Collection Kit Administration</h2>
        <p>Please provide shipping details so that we can get your sample collection kit to you:</p>
        <div>
            <?= $this->Form->create($order);?>

            <?=$this->Form->control('kit_administered_on_site' ,[
                'type' => 'checkbox',
                'label' => 'Kit Administered on site',
            ])?>
            <div class="kit-fields">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-6">
                        <?=$this->Form->control('sample_id' ,[
                            'type' => 'text',
                            'label' => 'Sample/Collection Kit ID',
                            'required' => true
                        ])?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-6">
                        <?=$this->Form->control('collection_date' ,[
                            'type' => 'datetimepicker',
                            'label' => 'Collection Date'
                        ])?>
                    </div>
                </div>
            </div>


            <?=$this->Form->control('ship_to_provider' ,[
                'type' => 'checkbox',
                'label' => 'Ship Sample/Collection Kit to Pharmacy/Provider'
            ])?>

            <?=$this->Form->control('ship_to_patient' ,[
                'type' => 'checkbox',
                'label' => 'Ship Sample/Collection Kit to Patient'
            ])?>

            <div class="shipping-fields">
                <h3>Shipping Information:</h3>
                <?=$this->Form->control('shipping_first_name' ,[
                    'type' => 'text',
                    'label' => 'First Name',
                    'required' => true
                ])?>

                <?=$this->Form->control('shipping_last_name' ,[
                    'type' => 'text',
                    'label' => 'Last Name',
                    'required' => true
                ])?>

                <?=$this->Form->control('shipping_address_1' ,[
                    'type' => 'text',
                    'label' => 'Address',
                    'required' => true
                ])?>

                <?=$this->Form->control('shipping_address_2' ,[
                    'type' => 'text',
                    'label' => false
                ])?>

                <?=$this->Form->control('shipping_city' ,[
                    'type' => 'text',
                    'label' => 'City',
                    'required' => true
                ])?>

                <?=$this->Form->control('shipping_lib24watch_state_abbr' ,[
                    'type' => 'select',
                    'label' => 'State',
                    'empty' => true,
                    'options' => $states,
                    'required' => true
                ])?>

                <?=$this->Form->control('shipping_postal_code' ,[
                    'type' => 'text',
                    'label' => 'Zip',
                    'required' => true
                ])?>
            </div>

            <?php
                echo $this->element('form-buttons');
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
