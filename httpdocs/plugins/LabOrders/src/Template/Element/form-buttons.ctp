<div class="lab-form-buttons">
<?php

if (
    isset($order) && 
    isset($order->isSkippableToReview) &&
    $order->isSkippableToReview
) {
    echo $this->Form->submit('Skip to Review', 
        [
            'name' => 'skip_to_review',
            'class' => 'btn btn-secondary pull-right ml-2'
        ]
    );
}

echo $this->Form->submit('Continue',
    ['class' => 'btn btn-primary pull-right']
);

if (!isset($hidePrevious)) {
    echo $this->Form->submit(
        'Previous',
        [
            "type" => "submit",
            "name" => "previous",
            "div" => false,
            'class' => 'btn btn-info pull-left'
        ]
    );
}

?>
</div>
