<a href="javascript:void(0)"><span class="fa fa-medkit"></span><span class="xn-text"><?php echo h($lib24watchModules['LabOrders']['title']); ?></span></a>
<ul>
    <li>
        <?php
        echo $this->Html->link(
            "<span class=\"fa fa-list\"></span> Browse Lab Orders",
            [
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'index',
                'prefix' => 'admin'
            ],
            [
                'escape' => false,
                'title' => 'Browse Lab Orders'
            ]
        );
        ?>
    </li>
</ul>