<?php
/**
 * @var App\View\AppView $this
 * @var $index
 * @var $labOrderId
 * @var $lastIndex
 * @var $result
 */
            $baseKey = 'results.' . $result['id'];
            $key = $baseKey . '.result_map_note';
            $inputTemplate = '<input type="{{type}}" class="form-control result-inputs" name="{{name}}"{{attrs}}/>';
            $mapProblemRationalesTable = \Cake\ORM\TableRegistry::getTableLocator()->get('SystemManagement.MapProblemRationales');

            $firstClass = '';
            $lastClass = '';
            if ($index === 0) {
                $firstClass = 'map-item-first';
            }

            if ($index === $lastIndex) {
                $lastClass = 'map-item-last';
            }
?>
    <div class="row map-item <?=$lastClass;?> <?=$firstClass;?>">
            <?php
            echo $this->Form->control('results.' . $result['id'] . '.id', [
                'type' => 'hidden',
                'templates' => [
                    'input' => $inputTemplate,
                ]
            ]);

            echo $this->Form->control($key . '.id', [
                'type' => 'hidden'
            ]);

            ?>
            <div class="col-md-4">
            <?php echo $this->Form->control($key . '.is_included', [
                'label' => 'Include in MAP?',
                'type' => 'checkbox',
                'class' => 'included-checkbox',
            ]);?>
                <?php
                if (empty($result['is_relevant_original'])):
                    echo $this->Html->link(
                        __('Delete Medication'),
                        [
                            'controller' => 'LabOrders',
                            'action' => 'deleteMedicationResult',
                            'prefix' => false,
                            $labOrderId,
                            $result['id']
                        ],
                        [
                            'class' => 'btn btn-warning delete-medication-item',
                            'confirm' => 'Are you sure you wish to delete this medication from the MAP?'
                        ]
                    );
                endif; ?>

            <p><strong>Drug:</strong></p>

            <?php
            if (
                isset($result['result_medications']) &&
                !empty($result['result_medications']) &&
                isset($result['result_medications'][0]->medications_mapped) &&
                !empty($result['result_medications'][0]->medications_mapped)
            ) {
                $drugCollection = new \Cake\Collection\Collection($result['result_medications'][0]->medications_mapped);

                // Lab order medication
                if ((bool)$result['is_lab_order_medication'] === true) {
                    if (
                        isset($result['result_medications'][0]) &&
                        isset($result['result_medications'][0]['lab_order_medication']) &&
                        !empty($result['result_medications'][0]['lab_order_medication']) &&
                        isset($result['result_medications'][0]['lab_order_medication']['drug'])
                    ) {
                        echo '<p class="drug-name">' . $result['result_medications'][0]['lab_order_medication']['drug']['specificproductname'] . '</p>';
                    } else {
                        echo 'N/A';
                    }
                } else if ($drugCollection && $drugCollection->count() > 0) {
                    $drugNames = $drugCollection->extract('drug.specificproductname')->toArray();
                    echo '<p class="drug-name">' . implode(', ', $drugNames) . '</p>';
                }

                if (isset($result['result_medications'][0]->medications_mapped[0]->medication->alternates)) {
                ?>
            <?php
            $alternateMedicationIds = (new \Cake\Collection\Collection($result['result_medications'][0]->medications_mapped[0]->medication->alternates))->extract('id');

            $drugs = [];
            (new \Cake\Collection\Collection($result['result_medications'][0]->medications_mapped[0]->medication->alternates))
                ->map(function( $alternateMedication, $key) use (
                    $alternativeResults,
                    &$drugs
                ) {
                    $alternativeResult = null;

                    if (isset($alternativeResults[$alternateMedication->id])) {
                        $alternativeResult = $alternativeResults[$alternateMedication->id];
                    }

                    foreach ($alternateMedication['drugs'] as $drugEntity) {
                        $key = $drugEntity->id;
                        $alternativeResultId = 0;

                        if ($alternativeResult && isset($alternativeResult->id)) {
                            $alternativeResultId = $alternativeResult->id;
                        }

                        $key.= '_' . $alternativeResultId;

                        $value = $drugEntity->specificproductname;

                        if ($alternativeResult && isset($alternativeResult['tsi_severity'])) {
                            $value.= ' - ' . $alternativeResult['tsi_severity'];
                        } else {
                            $value.= ' - no pgx data';
                        }

                        $drugs[$key] = $value;
                    }

                })->toArray();

            $alternateValue = '';
            if (!empty($drugs)) {
    ?>
    <div class="selected-drug-container">
    <?php
                if (
                    isset($result['result_map_note']) &&
                    !empty($result['result_map_note']) &&
                    isset($result['result_map_note']['alternate_drug_id']) &&
                    isset($result['result_map_note']['alternate_result_id'])
                ) {
                    $alternateValue = $result['result_map_note']['alternate_drug_id'] . '_' . $result['result_map_note']['alternate_result_id'];
                }

                echo $this->Form->control($key . '.alternate_drug_id', [
                    'value' => $alternateValue,
                    'type' => 'select',
                    'options' => $drugs,
                    'empty' => 'Select a drug',
                    'label' => 'Alternate Drug:',
                ]);

    ?>
    </div>
    <?php
                echo $this->Form->control($key . '.alternate_result_id', [
                    'type' => 'hidden',
                    'templates' => [
                        'input' => $inputTemplate,
                    ]
                ]);
            } else {
            ?>
                <p>No alternate drugs</p>
            <?php }
            }
        } else if (
            isset($result['result_medications']) &&
            !empty($result['result_medications']) &&
            isset($result['result_medications'][0]['lab_order_medication']) &&
            !empty($result['result_medications'][0]['lab_order_medication']) &&
            isset($result['result_medications'][0]['lab_order_medication']['drug']) &&
            !empty($result['result_medications'][0]['lab_order_medication']['drug'])
        ) {
            echo '<p class="drug-name">' . $result['result_medications'][0]['lab_order_medication']['drug']['specificproductname'] . '</p>';
        }
        ?>
<br>

<p class="severity-<?=$result['tsi_severity'];?>"><strong>Severity:</strong><br>
<?=$result['tsi_severity'] ?? 'N/A';?></p>

<?php
            $severityText = '';
            $severityClass = '';

            if (isset($result['result_map_note']['alternate_result'])) {
                $severityClass = 'severity-' . $result['result_map_note']['alternate_result']['tsi_severity'];
                $severityText = ucfirst($result['result_map_note']['alternate_result']['tsi_severity']);
            }
?>
    <p class="<?=$severityClass;?> adjusted-severity"><strong>Adjusted Severity:</strong><br>
        <span><?php
            if (isset($severityText) && !is_null($severityText) && mb_strlen($severityText) > 0) {
                echo $severityText;
            } else {
                echo 'N/A';
            }
            ?>
        </span>
    </p>

        <div class="select-severity-row">
    <?php
                $isIncluded = false;
                if (isset($result['result_map_note']['is_included']) && (bool)$result['result_map_note']['is_included']) {
                    $isIncluded = true;
                }

                echo $this->Form->control($key . '.severity_level_id', [
                    'label' => 'Suggested Review Column:',
                    'type' => 'select',
                    'empty' => 'Select a MAP severity',
                    'required' => $isIncluded,
                    'options' => $severityLevelsList,
                    'templates' => [
                        'select' => '<select name="{{name}}" class="form-control select-severity" {{attrs}}>{{content}}</select>',
                    ]
                ]);
    ?>
        </div>

<br><br>

                <?php
                if (isset($result['result_observations']) &&
                    !empty($result['result_observations'])
                ) {
?>
    <ul><?php
                    foreach ($result['result_observations'] as $observation) {
                        $detailText = '';
                        if ($observation->type === 'phenotype') {
                            $detailText = 'Phenotype: ';
                        ?>
                    <li><span class="gene-label"><?=$detailText;?></span><br><?=$observation->component_value_coding_1_display;?>: <?=$observation->value_coding_2_display;?></li>
                        <?php
                        } else if ($observation->type === 'genotype') {
                            $detailText = 'Genotype: ';
                        ?>
                            <li><span class="gene-label"><?=$detailText;?></span><br><?=$observation->component_value_coding_1_display;?> <?=$observation->value_string;?></li>
                        <?php
                        } else {?>
    <?php }
                }
?>
</ul>
<?php }

$mapProblemRationale = $result['result_map_note']['map_problem_rationale'] ?? null;

if ($showProblemByDefault === false) {
    $mapProblemClass = !is_null($mapProblemRationale) ? '' : 'd-none';
}

if (
    !is_null($this->getRequest()->getData($key.'.map_problem_category')) &&
    $this->getRequest()->getData($key.'.map_problem_category')
) {
    $mapProblemClass = '';
}

$requireProblem = false;
$problemContainerClass = 'd-none';

if (isset($alternateValue) && $alternateValue !== '') {
    $requireProblem = true;
    $problemContainerClass = '';
}

if ($showProblemByDefault === true) {
    $problemContainerClass = '';
    $mapProblemClass = '';
}

?>

    <div class="reason-container <?=$problemContainerClass;?>">
<br><br>
<?php
$categoryOptions = [
    'empty' => 'Select a reason category',
    'label' => 'Reason for change:',
    'options' => $mapProblemCategoriesList,
    'type' => 'select',
    'required' => $requireProblem,
    'templates' => [
        'select' => '<select name="{{name}}" class="form-control problem-dropdown problem-category" {{attrs}}>{{content}}</select>',
        'inputContainer' => '<div class="problem-category-container input {{type}}{{required}}">{{content}}</div>',
    ],
];
                    if ($this->getRequest()->getData($key)) {
                        $categoryOptions['value'] = $this->getRequest()->getData($key)['map_problem_category'];
                    } else if (!is_null($mapProblemRationale)) {
                        $categoryOptions['value'] = $mapProblemRationale->map_problem_category_id;
                    }

                    echo $this->Form->control($key . '.map_problem_category', $categoryOptions);

                    $rationaleOptions = [
                        'templates' => [
                            'select' => '<select name="{{name}}" class="form-control problem-dropdown problem-rationale" {{attrs}}>{{content}}</select>',
                            'inputContainer' => '<div class="rationale-container input ' . $mapProblemClass . ' {{type}}{{required}}">{{content}}</div>',
                            'inputContainerError' => '<div class="rationale-container input {{type}}{{required}} error">{{content}}{{error}}</div>',
                        ],
                        'empty' => 'Select a reason',
                        'label' => false,
                        'required' => $requireProblem,
                    ];

                    if ($this->getRequest()->getData($key)) {
                        $categoryOptions['value'] = $this->getRequest()->getData($key)['map_problem_rationale_id'];
                        if (
                            !is_null($this->getRequest()->getData($key.'.map_problem_category')) &&
                            $this->getRequest()->getData($key.'.map_problem_category')
                        ) {
                            $rationaleOptions['options'] = $mapProblemRationalesTable->getList($this->getRequest()->getData($key.'.map_problem_category'));
                        }

                    } else if (isset($mapProblemRationale->id) && $mapProblemRationale->id) {
                        $rationaleOptions['value'] = $mapProblemRationale->id;
                        $rationaleOptions['options'] = $mapProblemRationalesTable->getList($mapProblemRationale->map_problem_category_id);
                    }

                    echo $this->Form->control($key . '.map_problem_rationale_id', $rationaleOptions);
?>

</div>
</div>
                                    <div class="col-md-8"><?php
                                        echo $this->Form->control(
                                            $key . '.detail',
                                            [
                                                'escape' => true,
                                                'width' => '100%',
                                                'class' => 'form-control editor editor-review-map',
                                                'style' => 'min-height: 200px;',
                                                'type' => 'textarea',
                                                'style' => 'min-height: 200px;',
                                                'label' => 'Implication',
                                            ]
                                        );
                                    ?>
<div class="row row-prescriber">
<div class="col-md-6 col-prescriber-notes">
                                    <?php echo $this->Form->control($key . '.prescriber_notes',
                                        [
                                            'escape' => true,
                                            'width' => 300,
                                            'class' => 'form-control editor editor-review-map editor-required',
                                            'type' => 'textarea',
                                            'label' => 'Prescriber Notes',
                                            'required' => true
                                        ]
                                    );?></div>
<div class="col-md-6 col-patient-notes">
                                    <?php echo $this->Form->control($key . '.patient_notes',
                                        [
                                            'escape' => true,
                                            'class' => 'form-control editor editor-review-map',
                                            'width' => 300,
                                            'type' => 'textarea',
                                            'label' => 'Patient Notes'
                                        ]
                                    );?></div>
            </div>
        </div>
</div>
