
        <?php
        if ($order->ship_to_provider || $order->ship_to_patient) {
            ?>
            <span>Shipping Name:</span> <?= h($order->shipping_first_name . ' ' . $order->shipping_last_name); ?>
            <br>
            <span>Shipping Address:</span>
            <br>
            <?= h($order->shipping_address_1); ?>
            <br>
            <?= h($order->shipping_address_2); ?>
            <br>
            <?= h($order->shipping_city . ', ' . $order->shipping_lib24watch_state_abbr . ' ' . $order->shipping_postal_code); ?>
            <?php
        } else {
            ?>
            <span>Sample Kit ID #:</span> <?= h($order->sample_id); ?>
            <br>
            <span>Collection Date:</span> <?php 
                if (!is_null($order->collection_date)) {
                    echo lib24watchDate('shortDateFormat', $order->collection_date, +5);
                } else {?>N/A
            <?php
                }
        }
