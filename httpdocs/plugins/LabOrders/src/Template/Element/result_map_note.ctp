<?php
                                    $key = 'results.' . $index . '.result_map_note';
?>
                                <tr>
<?php
                                    echo $this->Form->control('results.' . $index . '.id', [
                                        'type' => 'hidden'
                                    ]);

                                    echo $this->Form->control($key . '.id', [
                                        'type' => 'hidden'
                                    ]);
?>
                                    <td><?php echo $this->Form->control($key . '.is_included', [
                                        'label' => false,
                                        'type' => 'checkbox'
                                    ]);?></td>
                                    <td><?php 
                                        echo $this->Form->control(
                                            $key . '.detail',
                                            [
                                                'escape' => true,
                                                'class' => 'form-control editor editor-review-map',
                                                'style' => 'min-height: 200px',
                                                'type' => 'textarea',
                                                'label' => false,
                                            ]
                                        );
                                    ?></td>
                                    <td><?php echo $this->Form->control($key . '.prescriber_notes',
                                        [
                                            'escape' => true,
                                            'class' => 'form-control editor editor-review-map',
                                            'type' => 'textarea',
                                            'label' => false,
                                        ]
                                    );?></td>
                                    <td><?php echo $this->Form->control($key . '.patient_notes',
                                        [
                                            'escape' => true,
                                            'class' => 'form-control editor editor-review-map',
                                            'type' => 'textarea',
                                            'label' => false,
                                        ]
                                    );?></td>
                                </tr>
