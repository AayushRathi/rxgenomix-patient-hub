<?php
echo $this->Form->create($labOrderReportEntity, [
]);

            foreach ($labOrderReportEntity->results as $index => $result) {
                if ($result['id'] == $reportResultEntity->id) {
                    echo $this->element('LabOrders.result_map_note_alt',
                        [
                            'labOrderId' => $labOrderReportEntity->lab_order_id,
                            'index' => 0,
                            'result' => $result,
                            'lastIndex' => null,
                            'severityLevelsList' => $severityLevelsList,
                            'mapProblemCategoriesList' => $mapProblemCategoriesList,
                            'showProblemByDefault' => true,
                            'alternativeResults' => $alternativeResults,
                        ]);

                }
            }
echo $this->Form->end();
?>
