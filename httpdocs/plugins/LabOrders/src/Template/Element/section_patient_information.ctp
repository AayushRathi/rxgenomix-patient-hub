<?php
    use App\Util\FormattingHelper;
?>
        <span>Name:</span> <?= $order->patient->first_name . ' ' . $order->patient->last_name; ?>
        <br>
        <span>Ethnicity:</span>
        <?php
        if (isset($ethnicities[$order->patient->ethnicity_id]) && $order->patient->ethnicity_id != 8) {
            echo h($ethnicities[$order->patient->ethnicity_id]);
        } elseif ($order->patient->ethnicity_id == 8) {
            echo h($order->patient->ethnicity_other);
        }
        ?>
        <br>
        <span>Sex:</span>
        <?php
        if (isset($sexes[$order->patient->sex_id])) {
            echo $sexes[$order->patient->sex_id];
        }
        ?>
        <br>
        <span>Client ID:</span> <?php
        if ($hasChildGroups) {
            echo h($order->patient->client_id);
        } else {
            echo $clientName;
        }
        ?>
        <br>
<?php if ($order->lab_order_type_id !== 2) {?>
        <span>Card ID:</span> <?= h($order->patient->card_id); ?>
        <br>
        <span>Person Code:</span> <?= h($order->patient->person_code); ?>
        <br>
<?php } ?>
        <span>Date of Birth:</span> <?= lib24watchDate('shortDateFormat', $order->patient->date_of_birth, +5); ?>
        <br>
        <span><?php echo $order->lab_order_type_id === 2 ? 'Patient ' : '';?>Email:</span> <?= h($order->patient->email); ?>
        <br>
        <span><?php echo $order->lab_order_type_id == 2 ? 'Patient ' : '';?>Phone:</span> <?= h(FormattingHelper::formatPhone($order->patient->phone)); ?>
