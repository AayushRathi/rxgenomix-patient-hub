<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medication Action Plan Footer</title>
    <?php echo $this->Less->less('/less/fonts.less', ['css' => ['fullBase' => true]]); ?>
<style>
html {
    font-family: 'Open Sans';
}
div#rxg-push.page-1 {
    background-color: #deeaef;
    border-bottom-left-radius: 25px;
    display: block !important;
    padding: 60px 0 25px 30px;
    position: fixed;
    right: 0;
    top: 0;
    width: 290px;
}

body { display: none !important; }

body.last-page { 
    display: block !important; 
    font-family: 'Open Sans';
    font-size: 10pt;
    font-weight: normal;
}
</style>
<script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      var sitePages = query_strings_from_url[10];
      var lastPage = sitePages.split('=')[1];
      var rxgPush = document.getElementById('rxg-push');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);

              vars[temp_var[0]] = decodeURI(temp_var[1]);

              if (temp_var[0] === 'page') {
                  if (temp_var[1] == lastPage) {
                      document.body.className+= ' last-page';
                  }
              }
          }
      }
  }
</script>
</head>
<body onload="subst();">
    <div class="roww">
        <div class="" style="text-align:center;">
<?php /*
<div id="rxg-push"></div>
 */ ?>
    <?php
        if ($this->ContentRegionLoader->regionExists('map_footer_content')) {
?>
        <span class="map-footer-content">
        <?php
                echo $this->ContentRegionLoader->getRegion(
                    'map_footer_content'
                );
        ?>
        </span>
        <?php } 
    ?>
            <?php
        /*
                if ($this->ContentRegionLoader->regionExists('map_footer_image')) {
                    ?>
                    <span class="map-footer-image">
                        <?php
                        echo $this->ContentRegionLoader->getRegion(
                            'map_footer_image',
                            true,
                            [
                                'class' => '',
                                'fullBase' => true
                            ],
                            true,
                            [
                                'width' => 150,
                                'height' => 84
                            ]
                        );
                        ?>
                    </span>
                    <?php
                }
         */
            ?>
        </div>
    </div>
</body>
</html>
