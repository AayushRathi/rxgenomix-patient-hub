<?php

        echo $this->cell(
            'Lib24watch.Table::display',
            [
                '',
                'items' => [
                    [
                        'name' => 'drug.nondose_name',
                        'label' => 'Name',
                        'type' => 'text'
                    ],
                    [
                        'name' => 'drug.brand_names',
                        'label' => 'Brand Names',
                        'type' => 'custom',
                        'function' => function ($row) {
                            $brandNames = [];
                            foreach ($row->drug->brand_names as $brand) {
                                $brandNames[] = $brand->brand_name;
                            }
                            $brandNames = array_unique($brandNames);
                            return implode(', ', $brandNames);
                        }
                    ]
                ],
                $order->medications
            ]
        );
        ?>
