<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Medication Action Plan Header</title>
    <?php echo $this->Less->less('/less/theme-pdf.less', ['css' => ['fullBase' => true]]); ?>
<style>
body {
    margin-right: 25px;
    /*border:1px solid red;*/
}

div#rxg-push.page-1 {
    background-color: #deeaef;
    border-bottom-left-radius: 25px;
    display: block !important;
    padding: 60px 0 25px 30px;
    position: fixed;
    right: 0;
    top: 0;
    width: 290px;
}

div#rxg-push div.body { 
    font-family: 'Open Sans';
    padding-top: 25px;
    padding-right: 25px;
}
</style>
<script>
  function subst() {
      var vars = {};
      var query_strings_from_url = document.location.search.substring(1).split('&');
      for (var query_string in query_strings_from_url) {
          if (query_strings_from_url.hasOwnProperty(query_string)) {
              var temp_var = query_strings_from_url[query_string].split('=', 2);
              vars[temp_var[0]] = decodeURI(temp_var[1]);

              if (temp_var[0] === 'page') {
                  var pageClassName = 'page-' + temp_var[1];
                  var rxgPush = document.getElementById('rxg-push');
                  if (rxgPush) {
                      rxgPush.className+= ' ' + pageClassName;
                  }
              }
          }
      }
  }</script>
</head>
<body onload="subst();">
    <div class="roww">
        <div class="" style="width:70%;">
                <div id="rxg-push" style="display:none;">
                    <img src="<?php echo \Cake\Core\Configure::read('App.fullBaseUrl');?>/rxg_theme/img/rxg-logo.png" width="200">
            <?php
            // this doesnt work. feel free to fix it.
            //<img src="<?= \Cake\Core\Plugin::path('RxgTheme') . 'webroot' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'rxg-logo.png';
            $enableLogo = false;
            if ($enableLogo && $this->ContentRegionLoader->regionExists('rxg_logo')) {
                ?>
                <div>
                    <?php
                    echo $this->ContentRegionLoader->getRegion(
                        'rxg_logo',
                        true,
                        [
                            'class' => 'center-block',
                            'fullBase' => true
                        ],
                        true,
                        [
                            'width' => 357,
                            'height' => 94
                        ]
                    );
                    ?>
                </div>
                <?php
            }
            ?>
                    <div class="body"><?=$contentRegionContent->body;?></div>
                </div>
                <div class="map-header-title-section">
                        <h3>RxGenomix Action Plan for <strong><?=$patientFirstName?> <?=$patientLastName;?></strong></h3>
                </div>
                <table cellspacing="0" cellpadding="0" width="100%" class="map-header">
                <tr>
                        <th>Patient:</th>
                        <td><?=$patientFirstName;?> <?=$patientLastName;?></td>

                        <th><?=$providerLabel;?>:</th>
                        <td><?=$providerName;?></td>
                </tr>
                <tr>
                        <th>DOB:</th>
                        <td><?=$patientDob;?></td>

                        <th>Phone: </th>
                        <td><?=$providerPhone;?></td>
                </tr>
                <tr>
                        <th>Gender:</th>
                        <td><?=$patientGender;?></td>

                        <th>Sample Processed:</th>
                        <td><?=$sampleProcessedDate;?></td>
                </tr>
                <tr>
                        <th>Sample ID:</th>
                        <td><?=$sampleId;?></td>
                
                        <th>Collection Date:</th>
                        <td><?=$collectionDate;?></td>
                </tr>
                <tr>
                        <th><?=$physicianLabel;?>:</th>
                        <td> <?=$clinician;?></td>
                        <th>Report Released:</th>
                        <td><?=$reportReleasedDate;?></td>
                </tr>
                </table>
        </div>
    </div>
</body>
</html>
