    <div class="graph <?=$className;?>">
        <div class="graph-background">
            <div class="graph-dot-container"><div class="graph-dot">&bull;</div></div>
            <style>
                .<?=$className?> div.graph-dot {
                    bottom: <?=$percentage?>% !important;
                }
            </style>
        </div>
        <h4><?=$heading;?></h4>
        <h5 class="risk-level risk-level-<?=$riskLevel;?>"><?=$riskScore;?></h5>
    </div>
