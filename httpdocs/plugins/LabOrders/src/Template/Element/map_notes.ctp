<div class="map-notes-container">
<?php if ($mapType === 'patient') {?>
    <p><strong>Additional Pharmacist Notes</strong></p>
    <p><?php echo $additionalEntity->additional_pharmacist_notes_patient ?? 'N/A';?></p>

    <p><strong>Additional Drug Reaction Notes</strong></p>
    <p><?php echo $additionalEntity->adverse_drug_reaction_notes_patient ?? 'N/A';?></p>

    <p><strong>Lifestyle Recommendations</strong></p>
    <p><?php echo $additionalEntity->lifestyle_recommendation_notes_patient ?? 'N/A';?></p>
<?php } else {?>
    <p><strong>Additional Pharmacist Notes</strong></p>
    <p><?php echo $additionalEntity->additional_pharmacist_notes_prescriber ?? 'N/A';?></p>

    <p><strong>Additional Drug Reaction Notes</strong></p>
    <p><?php echo $additionalEntity->adverse_drug_reaction_notes_prescriber ?? 'N/A';?></p>

    <p><strong>Lifestyle Recommendations</strong></p>
    <p><?php echo $additionalEntity->lifestyle_recommendation_notes_prescriber ?? 'N/A';?></p>

<?php }?>
    <p><strong>Additional Physician Response (<span>detail all denial/modification of recommendations</span>)</strong></p>
    <br><br><br>
</div>

