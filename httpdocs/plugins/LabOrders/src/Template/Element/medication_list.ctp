<?php
?>
<div class="col-xs-4 drug-table-<?php echo $severity;?> drug-table">
    <h4><?=$info['header'];?> (<?php echo isset($drugs) ? count($drugs) : '';?>)</h4>
    <ul>
    <?php
    if (isset($drugs) && !empty($drugs)) {?>
        <ul>
        <?php
        foreach ($drugs as $drug) {
            echo '<li>' . $drug['drug']->specificproductname . '</li>';
        }
        ?>
        </ul>
    <?php
    } else { ?>
        <p class="none"><?=$info['none'];?></p>
    <?php 
    }
    ?>
</div>
