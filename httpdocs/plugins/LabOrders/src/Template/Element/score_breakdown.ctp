<?php if ($currentRiskScore > 1 && count($labOrderReportEntity->results) > 0) { ?>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table score-breakdown">
                                    <thead>
                                        <tr>
                                            <th>Drug/Metric</th>
                                            <th>Current Risk Score</th>
                                            <th>Future Risk Score</th>
                                            <th>Max Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        $maxCurrentScore = 0;
                                        $currentScore = 0;
                                        $alternateScore = 0;

                                        // calculate the number of medications
                                        $foundTsiMedicationIds = [];
                                        foreach ($labOrderReportEntity->results as $index => $result) {
                                            if (isset($result['result_medications']) && !empty($result['result_medications'])) {
                                                foreach ($result['result_medications'] as $medication) {
                                                    if (isset($medication['tsi_medication_id']) && !in_array($medication['tsi_medication_id'], $foundTsiMedicationIds)) {
                                                        $foundTsiMedicationIds[] = $medication['tsi_medication_id'];
                                                    }
                                                }
                                            }
                                        }

                                        foreach ($labOrderReportEntity->results as $index => $result) {
                                            if (
                                                isset($result['result_medications']) && 
                                                !empty($result['result_medications']) &&
                                                isset($result['result_medications'][0]->medications_mapped) &&
                                                !empty($result['result_medications'][0]->medications_mapped)
                                            ) {
                                                $drugCollection = new \Cake\Collection\Collection($result['result_medications'][0]->medications_mapped);
                                                $drugHtml = '';


                                                if ($drugCollection && $drugCollection->count() > 0) {
                                                    $drugNames = $drugCollection->extract('drug.specificproductname')->toArray();
                                                    $drugHtml.= '<p>' . implode(', ', $drugNames) . '</p>';
                                                }
?>
<tr>
<th><?=$drugHtml;?></th>
<td><?php
$score = 0;
$maxScoreCount = 2;
                                                if ($result['tsi_severity'] === 'high') {
                                                    $score = 2;
                                                } else if ($result['tsi_severity'] === 'moderate') {
                                                    $score = .5;
                                                } else {
                                                    $score = 0;
                                                }

                                                $maxCurrentScore+= $maxScoreCount;
                                                $currentScore+= $score;

                                                echo $score;
?></td>
    <td class="row-alternate-score-<?=$result['id'];?>"><?php
$rowAlternateScore = null;
$defaultAlternateScore = 0;
                                                if (isset($result['alternate_result'])) {
                                                    if ($result['alternate_result']['tsi_severity'] === 'high') {
                                                        $rowAlternateScore = 2;
                                                    } else if ($result['alternate_result']['tsi_severity'] === 'moderate') {
                                                        $rowAlternateScore = .5;
                                                    } else {
                                                        $rowAlternateScore = 0;
                                                    }

                                                    $alternateScore += $rowAlternateScore;
                                                } else {
                                                    if ($result['tsi_severity'] === 'high') {
                                                        $defaultAlternateScore = 2;
                                                    } else if ($result['tsi_severity'] === 'moderate') {
                                                        $defaultAlternateScore = 0.5;
                                                    }

                                                    $alternateScore += $defaultAlternateScore;
                                                }

?><?= !is_null($rowAlternateScore) ? $rowAlternateScore : '<span class="text-muted">NA</span>';?> / <span class=""><?php echo $defaultAlternateScore > 0 ? $defaultAlternateScore : '';?></span></td>
    <td><?=$maxScoreCount;?></td>
</tr>
<?php
                                            }
                                        }
                                    ?>
                                        <tr><th>Number of medications: <?=count($foundTsiMedicationIds);?></th><th style="text-align:center;" colspan="3"><?php

                                        $count = count($foundTsiMedicationIds);
                                        $countScore = 0;
        if ($count <= 5) {
            $countScore += 1;
        } else if ($count >=6 && $count <= 10) {
            $countScore += 3;
        } else if ($count >= 11 && $count <= 15) {
            $countScore += 5;
        } else if ($count >= 16 && $count <= 20) {
            $countScore += 7;
        } else if ($count > 20) {
            $countScore += 10;
        }

                                        $currentScore += $countScore;
                                        $maxCurrentScore += $countScore;
                                        $alternateScore += $countScore;

                                        echo $countScore;
?></th></tr>
    <tr><th>Totals</th><th><?=$currentScore;?></th><th class="alternate-score-sum"><span class="new-score"></span><span class="original"><?=$alternateScore;?></span></th><th><?=$maxCurrentScore;?></th></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
<br><br>
                                <?php } ?>
