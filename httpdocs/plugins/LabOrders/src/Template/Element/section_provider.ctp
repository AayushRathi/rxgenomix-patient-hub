
        <?php
    $columns = [
                    [
                        'name' => 'provider_name',
                        'label' => 'Name',
                        'type' => 'custom',
                        'function' => function ($row) {
                            return h($row->provider_name);
                        }
                    ],
                    [
                        'name' => 'npi_number',
                        'label' => 'NPI #',
                        'type' => 'custom',
                        'function' => function ($row) {
                            return h($row->npi_number);
                        }
                    ],
                ];

    if ($order->lab_order_type_id !== 2) {
        $columns[] = 
                    [
                        'name' => 'is_primary_care_provider',
                        'label' => 'Primary Care Provider',
                        'type' => 'status'
                    ];

        $columns[] = [
                        'name' => 'is_ordering_physician',
                        'label' => 'Ordering Physician',
                        'type' => 'status'
                    ];
    }

        echo $this->cell(
            'Lib24watch.Table::display',
            [
                '',
                'items' => $columns,
                $order->providers
            ]
        );
        ?>
