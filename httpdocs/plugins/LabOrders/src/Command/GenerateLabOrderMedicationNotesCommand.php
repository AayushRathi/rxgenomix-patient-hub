<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use LabOrders\Libs\LabOrders;

/**
 * GenerateLabOrderMedicationNotes command.
 */
class GenerateLabOrderMedicationNotesCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');

        $labOrders = $labOrdersTable->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Patient.Snps',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider',
                'Reports',
            ])
            ->where([
                'lab_order_status_id' => 7,
                'LabOrders.id IN' => [447,555]
            ]);

        $labOrdersInstance = new LabOrders;

        foreach ($labOrders as $order) {
            $labOrderReportId = $order->reports[0]->id;
            debug($labOrderReportId);
            $labOrdersInstance->generateLabOrderNoteEntitiesForResults($labOrderReportId);
            exit;
        }
    }
}
