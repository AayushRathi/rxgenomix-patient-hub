<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;

/**
 * ReportUnviewed command.
 */
class ReportUnviewedCommand extends Command
{
    use MailerAwareTrait;

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $dateLastWeek = new \Cake\I18n\Date;
        $dateLastWeek->modify('-7 days');
        debug($dateLastWeek);

        $labOrders = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.lab_order_status_id' => 7,
                'DATE(LabOrders.lab_order_status_change_date)' => $dateLastWeek->format('Y-m-d'),
                'LabOrders.first_seen_provider_id IS' => null,
            ])
            ->contain([
                'Patient',
                'Provider.Users' => [
                    'fields' => [
                        'id',
                        'first_name',
                        'last_name'
                    ]
                ]
            ]);

        debug('Lab Orders found: ' . count($labOrders->toArray()));

        if (!empty($labOrders)) {
            $mailer = $this->getMailer('LabOrders.LabOrders');
            foreach ($labOrders as $labOrder) {
                $mailer->send('reportUnviewedLabOrder', [
                    $labOrder
                ]);
            }
        }
    }
}
