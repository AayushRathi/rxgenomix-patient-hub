<?php
namespace LabOrders\Command;

use App\Util\EmailHelper;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use LabOrders\Libs\ReportReadyManager;

/**
 * ReportReady command.
 */
class ReportReadyCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        EmailHelper::establishBaseUrl();

        $labOrders = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.lab_order_status_id' => 7,
                'LabOrders.first_seen_provider_id IS' => null,
                'LabOrders.enable_email_followup' => true,
                'LabOrders.enable_email_followup_start_date IS NOT' => null,
                'OR' => [
                    'LabOrders.last_sent_sequence <' => max(array_keys(ReportReadyManager::SCHEDULE)),
                    'LabOrders.last_sent_sequence IS' => null
                ]
            ])
            ->contain([
                'Patient',
                'Provider'
            ]);

        debug($labOrders->toArray());

        $reportReadyManager = new ReportReadyManager;

        foreach ($labOrders as $labOrder) {
            $reportReadyManager->processOrder($labOrder);
        }
    }
}
