<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;

/**
 * CheckInactivePharmacist command.
 */
class CheckInactivePharmacistCommand extends Command
{
    use MailerAwareTrait;

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $providersTable = TableRegistry::getTableLocator()->get('Providers.Providers');
        $inactiveProviderEntities = $providersTable
            ->find()
            ->where([
                'provider_type_id' => 1,
                'provider_status_id' => 2,
                'is_active' => 1,
            ])
            ->contain('Users', function($q) {
                return $q->select([
                    'id',
                    'last_logged_in',
                    'first_name',
                    'last_name'
                ]);
            })
            ->matching('Users', function($q) {
                $dateLastMonth = new \Cake\I18n\Date;
                $dateLastMonth->modify('-30 days');

                return $q->andWhere([
                    #'Users.email' => 'mike+activetest@orases.com',
                    'Users.last_logged_in <' => $dateLastMonth->format('Y-m-d'),
                    'Users.active' => 1
                ])->order([
                    'Users.last_logged_in' => 'ASC'
                ]);
            })
            ->formatResults(function ( $results) {
                return $results->map(function( $row) {
                    $result = new \StdClass;
                    $result->name = $row->user->first_name . ' ' . $row->user->last_name;
                    $result->last_logged_in = $row->user->last_logged_in;

                    return $result;
                });
            })
            ->toArray();

        if (!empty($inactiveProviderEntities)) {
            $mailer = $this->getMailer('LabOrders.LabOrders');

            $mailer->send('reportInactiveProviders', [
                $inactiveProviderEntities
            ]);
        }
    }
}
