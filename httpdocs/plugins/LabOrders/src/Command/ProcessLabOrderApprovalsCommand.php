<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;

/**
 * ProcessLabOrderApprovals command.
 */
class ProcessLabOrderApprovalsCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = 543;

        $this->loadModel('LabOrders.LabOrders');
        $labOrder = $this->LabOrders->find()
            ->contain([
                'WorkflowStatus',
                'Patient.Sex',
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames'
            ])
            ->matching('Providers')
            ->where([
                'LabOrders.id' => $labOrderId
            ])->firstOrFail();

        if ($labOrder->isSkippableToReview && $io->ask('Lab order is skippable to review. Process?', ['y', 'n']) === 'y') {
            $labOrdersInstance = new LabOrders;
            $submitted = $labOrdersInstance->submitLabOrder(
                $labOrderId,
                [
                    'type' => 'command'
                ]
            );
        } else {
            throw new \Exception('Lab order not ready to submit');
        }
    }
}
