<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * ResetLabOrderMedicationNotes command.
 */
class ResetLabOrderMedicationNotesCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = 555;

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->contain([
                'Reports'
            ])
            ->matching('Reports')
            ->firstOrFail();

            $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
            $latestReportEntity = $reportsTable->find('latestReportFull', [
                'lab_order_id' => $labOrderId,
            ])->firstOrFail();

            $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');

            // If this medication was returned in the results, bind it
            $medicationExistsQuery = $reportResultsTable->find()
                ->where([
                    'lab_order_report_id' => $latestReportEntity->id,
                    'is_relevant_original' => 0,
                    'is_lab_order_medication' => 1,
                    'detected_issue_index IS' => null
                ])
                ->contain(
                    [
                        'ResultMedications',
                        'ResultMapNotes',
                    ]
                );

            if ($medicationExistsQuery->count() > 0) {
                $results = $medicationExistsQuery->toArray();

                if (!empty($results)) {
                    foreach ($results as $result) {
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMapNotes')->deleteOrFail($result->result_map_note);
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMedications')->deleteOrFail($result->result_medications[0]);
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults')->deleteOrFail($result);
                    }
                }
            }
    }
}
