<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;

/**
 * RestoreMapNotes command.
 */
class RestoreMapNotesCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = $io->ask('What lab order id?');

        if ((int)$labOrderId <= 0) {
            throw new \Exception('Invalid lab order id');
        }

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->contain([
                'Medications.Medication',
                'Reports'
            ])
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->firstOrFail();

        $labOrderReportId = $labOrderEntity->reports[0]->id;

        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $labOrderReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->firstOrFail();

        $reportAdditionalTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportAdditional');

        $previousReportIds = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports')
            ->find()
            ->select([
                'id'
            ])
            ->where([
                'lab_order_id' => $labOrderId,
                'id !=' => $labOrderReportId
            ])
            ->extract('id')
            ->toArray();

        /*
        debug($previousReportIds);
        exit;
        */

        foreach ($labOrderReportEntity->results as $index=>$result) {
            if (!isset($result->result_medications) || empty($result->result_medications) || empty($result->result_medications[0])) {
                continue;
            }

            $medicationId = $result->result_medications[0]['medication_id'];
            $tsiMedicationId = $result->result_medications[0]['tsi_medication_id'];

            if ($medicationId <= 0 || $tsiMedicationId <= 0) {
                continue;
            }

            if (isset($result->result_map_note)) {
                $otherResults = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults')
                    ->find()
                    ->where([
                        'lab_order_report_id IN' => !empty($previousReportIds) ? $previousReportIds : [0],
                        'detected_issue_index' => $result->detected_issue_index,
                        'is_relevant' => true
                    ])
                    ->contain([
                        'ResultMapNotes'
                    ])
                    ->matching('ResultMedications', function ($q) use ($medicationId, $tsiMedicationId) {
                        return $q->andWhere([
                            'medication_id' => $medicationId,
                            'tsi_medication_id' => $tsiMedicationId,
                        ]);
                    })
                    ->toArray();

                if (!empty($otherResults)) {
                    $options = [];
                    foreach ($otherResults as $previousResult) {
                        if (
                            $previousResult['tsi_detail'] !== $result['tsi_detail'] ||
                            $previousResult['tsi_severity'] !== $result['tsi_severity'] ||
                            $previousResult['tsi_title'] !== $result['tsi_title']
                        ) {
                            continue;
                        }

                        $options[] = $previousResult;
                    }

                    if (!empty($options)) {
                        $io->out('__________ORIGINAL___________');
                        $io->out($result->result_map_note);
                        $io->out('__________END OF ORIGINAL___________');

                        $ids = [];
                        foreach ($options as $option) {
                            $ids[] = $option->result_map_note->id;
                            $io->out($option->result_map_note);
                        }

                        $ids[] = '0';

                        $overrideId = $io->askChoice('Which report map note to restore? [Specify lab_order_report_result_map_id]', $ids);

                        if ($overrideId == 0) {
                            echo 'processing next result';
                            continue;
                        }
                        $resultMapNotesTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMapNotes');
                        foreach ($options as $option) {
                            if ($option->result_map_note->id == $overrideId) {
                                $originalResultMapNoteEntity = $resultMapNotesTable->get($result->result_map_note->id);
                                $copyFromResultMapNoteEntity = $resultMapNotesTable->get($overrideId);
                                $io->out('THIS ONE');
                                $fields = ['patient_notes', 'prescriber_notes', 'map_problem_rationale_id', 'alternate_drug_id', 'alternate_result_id', 'alternate_drug', 'alternate_result', 'map_problem_rationale'];

                                $overrides = [];
                                foreach ($fields as $field) {
                                    if (!is_null($copyFromResultMapNoteEntity->{$field})) {
                                        $overrides[$field] = $copyFromResultMapNoteEntity->{$field};
                                    }
                                }

                                $originalResultMapNoteEntity = $resultMapNotesTable->patchEntity($originalResultMapNoteEntity, $overrides);

                                if (!$resultMapNotesTable->save($originalResultMapNoteEntity)) {
                                    throw new \Exception('Unable to save');
                                }

                                $io->out('Updated result map note # ' . $result->result_map_note->id);

                                $originalResultMapNoteEntity = $resultMapNotesTable->get($result->result_map_note->id);

                                $io->out($originalResultMapNoteEntity);
                            }
                        }

                    }
                }
            }
        }

        $additionalEntity = $reportAdditionalTable
            ->find()
            ->where([
                'lab_order_report_id' => $labOrderReportId
            ])
            ->first();

        if (!is_null($additionalEntity) && !empty($previousReportIds)) {
            $previousAdditionalEntities = $reportAdditionalTable
                ->find()
                ->where([
                    'lab_order_report_id IN' => $previousReportIds
                ])
                ->toArray();

            if (!empty($previousAdditionalEntities)) {
                $additionalIds = [];
                foreach ($previousAdditionalEntities as $previousEntity) {
                    $additionalIds[] = $previousEntity->id;
                    $io->out($previousEntity);
                }
            }

            $overrideId = $io->askChoice('Which additional entity to restore to?', $additionalIds);

            foreach ($previousAdditionalEntities as $previousEntity) {
                if ($previousEntity->id == $overrideId) {
                    $fields = [
                        'additional_pharmacist_notes_prescriber', 'additional_pharmacist_notes_patient', 'adverse_drug_reaction_notes_prescriber', 'adverse_drug_reaction_notes_patient', 'lifestyle_recommendation_notes_prescriber', 'lifestyle_recommendation_notes_patient'
                    ];

                    $overrides = [];
                    foreach ($fields as $field) {
                        if (!is_null($previousEntity->{$field})) {
                            $overrides[$field] = $previousEntity->{$field};
                        }
                    }

                    $additionalEntity = $reportAdditionalTable->patchEntity($additionalEntity, $overrides);
                    $io->out($additionalEntity);

                    if (!$reportAdditionalTable->save($additionalEntity)) {
                        throw new \Exception('Unable to save');
                    }

                    $io->out('Updated additional entity #' . $additionalEntity->id);

                    $additionalEntity = $reportAdditionalTable
                        ->find()
                        ->where([
                            'lab_order_report_id' => $labOrderReportId
                        ])
                        ->first();

                    $io->out($additionalEntity);
                }
            }
        }
    }

    /*
        return $this->find('latestReport', $options)
            ->contain([
                'ReportAdditional',
                'Results' => function($q) {
                    return $q->andWhere([
                        'Results.is_relevant' => 1
                    ])->order(['FIELD(Results.tsi_severity, "high", "moderate", "low")']);
                },
                'Results.ResultMedications.LabOrderMedications.Drug' => [
                    'fields' => [
                        'id',
                        'specificproductname',
                    ]
                ],
                'Results.ResultMedications.MedicationsMapped' => function($q) use ($options) {
                    return $q->andWhere([
                        'MedicationsMapped.lab_order_id' => $options['lab_order_id']
                    ]);
                },
                'Results.ResultMedications.MedicationsMapped.Drug',
                'Results.ResultMedications.MedicationsMapped.Medication',
                'Results.ResultMedications.MedicationsMapped.Medication.Alternates',
                'Results.ResultMedications.MedicationsMapped.Medication.Alternates.Drugs' => [
                    'fields' => [
                        'id',
                        'medication_id',
                        'specificproductname',
                    ]
                ],
                'Results.ResultMapNotes',
                'Results.ResultMapNotes.MapProblemRationales',
                'Results.ResultObservations',

                'Results.ResultMapNotes.AlternateResult' => [
                    'fields' => [
                        'tsi_detail',
                        'tsi_severity',
                    ]
                ],
                'Results.ResultMapNotes.AlternateDrug',
            ])
            ->matching('ReportAdditional')
            ->matching('Results')
            // refs #39505, reformatting structure of results array
            // so we can reliably render a new dynamically generated element (through ajax)
            // and have it save properly because "natural" indexing doesn't work (due to inconsistent ordering)
            ->formatResults(function($results) {
                return $results->map(function($row) {
                    if (!empty($row->results)) {
                        $row->results = (new \Cake\Collection\Collection($row->results))->indexBy('id')->toArray();
                    }

                    return $row;
                });
            });
     */
}
