<?php
namespace LabOrders\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use LabOrders\Libs\LabOrders;

/**
 * CheckForResultsSmokeScreen command.
 */
class CheckForResultsSmokeScreenCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $environment = 'dev';

        if (
            !is_null($args->getArgumentAt(0)) && $args->getArgumentAt(0) === 'production'
        ) {
            $environment = 'production';
        }

        $labOrderIds = [447];

        $labOrders = new LabOrders();
        $processed = $labOrders->processLabOrders($labOrderIds, [
            'io' => $io
        ], $environment);

        var_dump($processed);
    }
}
