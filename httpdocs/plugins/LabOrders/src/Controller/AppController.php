<?php
namespace LabOrders\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'LabOrders', 'Lab Orders');
    }

    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'lab_orders_view' => 'View Lab Orders',
            'lab_orders_edit' => 'Edit Lab Orders',
        ];
    }
}
