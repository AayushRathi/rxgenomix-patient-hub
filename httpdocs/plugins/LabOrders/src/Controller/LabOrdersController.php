<?php

namespace LabOrders\Controller;

use App\Util\FormattingHelper;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Cake\View\View;
use LabOrders\Model\Table\LabOrdersTable;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;
use RxgUsers\Libs\UserHelper;

/**
 * Class LabOrdersController
 * @package LabOrders\Controller
 * @property LabOrdersTable $LabOrders
 */
class LabOrdersController extends AppController
{
    use MailerAwareTrait;

    protected $user;

    protected $mapping;

    /**
     * @throws \Exception
     */
    public function initialize()
    {
        $this->idpIsLib24watch5User = false;
        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->loadModel('CakeDC/Users.Users');
        $this->loadModel('LabOrders.LabOrders');
        $this->loadModel('LabOrders.LabOrderStatuses');
        $this->loadModel('LabOrders.LabOrderInitiatorTypes');
        $this->loadModel('LabOrders.LabOrderTypes');
        $this->loadModel('LabOrders.LabOrderWorkflowStatuses');
        $this->loadModel('Patients.Patients');
        $this->loadModel('Providers.Providers');

        $this->user = $this->Auth->user();
        if ($this->user) {
            // the above doesn't select all fields
            $userEntity = TableRegistry::getTableLocator()->get('RxgUsers.RxgUsers')->find()
                ->where(['email' => $this->user['email']])
                ->contain(['LabOrderInitiatorTypes'])
                ->firstOrFail();

            $doResetPassword = false;
            $errorMessage = '';

            $forcePasswordResetSetting = \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('enableForcePasswordReset', 'LabOrders', false);

            if ($forcePasswordResetSetting) {
                if ((bool)$userEntity->force_password_reset) {
                    $errorMessage = 'Password reset has been triggered.';
                    $doResetPassword = true;
                }

                if ($doResetPassword === false && $userEntity->hasPasswordExpired) {
                    $errorMessage = 'Password has expired';
                    $doResetPassword = true;
                }

                if ($doResetPassword) {
                    return $this->redirectWithDefault([
                        'plugin' => 'RxgUsers',
                        'controller' => 'RxgUsers',
                        'action' => 'changePassword',
                        'prefix' => false,
                        '?' => [
                            'expired' => '1'
                        ]
                    ]);
                }
            }

            $labOrderInitiatorTypeIds = [2,3];
            $hasAccess = false;
		$tmpUser = $userEntity->toArray();
		$this->user['lab_order_initiator_types'] = $tmpUser['lab_order_initiator_types']; 
           foreach ($labOrderInitiatorTypeIds as $id) {
                if (UserHelper::hasInitiatorTypeId($id, $tmpUser)) {
                    $hasAccess = true;
                    break;
                }
            }
            if ($hasAccess === false) {
		$this->redirect([
                    'plugin' => 'RxgTheme',
                    'controller' => 'HomePage',
                    'action' => 'home'
                ], 403, 'User does not have access to this section.');
            }

            $actions = $this->LabOrderWorkflowStatuses->getActiveActions();

            // Define a step number by:
            // 1. Detect if we are in an active workflow status "action"
            // 2. Detect that a lab order id was passed
            // 3. Grab the mapping from the initiator type from the lab order
            if (
                in_array($this->getRequest()->action, $actions) &&
                !empty($this->getRequest()->params['pass']) &&
                (int)$this->getRequest()->params['pass'][0] > 0
            ) {
                $labOrderEntity = $this->getOrder((int)$this->getRequest()->params['pass'][0]);

                $this->mapping = $this->LabOrderWorkflowStatuses->getStepNumberMapping($labOrderEntity->lab_order_initiator_type_id);

                $this->set('stepNumber', $this->mapping[$this->getRequest()->action] ?? false);
            } else {
                $this->set('stepNumber', false);
            }

            // refs #31739 reload provider data
            $this->user['provider'] = $this->Providers->get($this->user['provider']['id'])->toArray();

            if ($this->user['provider']['provider_status_id'] == 3) {
		$this->redirect([
                    'plugin' => 'RxgTheme',
                    'controller' => 'HomePage',
                    'action' => 'home'
                ], 403, 'Your account has been denied.');
            }
            $this->set('hasSeenStatusMessage', $this->user['provider']['has_seen_status_message'] ?? false);
            $this->set('providerStatus', $this->user['provider']['provider_status_id']);

            $this->loadModel('HealthCareClients.HealthCareClientGroups');
            $providerGroup = $this->HealthCareClientGroups->find()->contain(['ChildGroups'])->where([
                'id' => $this->user['provider']['health_care_client_group_id']
            ])->first();

            $hasChildGroups = false;
            $clientName = null;
            if ($providerGroup) {
                $this->user['provider']['health_care_client_group_ids'] = [$providerGroup->id];
                $this->user['provider']['client_ids'] = $providerGroup->title;
                if (count($providerGroup->child_groups) > 0) {
                    $clientIds = [$providerGroup->id];
                    $hasChildGroups = true;
                    foreach ($providerGroup->child_groups as $group) {
                        $clientIds[$group->title] = $group->title;
                        $this->user['provider']['health_care_client_group_ids'][] = $group->id;
                        $clientIds[] = $group->title;
                        #$clientIds[] = $group->id;
                    }
                } else {
                    $clientName = $providerGroup->title;
                    $clientIds[] = $providerGroup->id;
                }
                $clientIds = array_unique($clientIds);
                $this->user['provider']['client_ids'] = $clientIds;
            } else {
                $clientIds = [0];
                $this->user['provider']['client_ids'] = [0];
            }

            $this->set('hasChildGroups', $hasChildGroups);
            $this->set('clientName', $clientName);
            $this->set('clientIds', $clientIds);

            $this->viewBuilder()->setHelpers(['Flash']);
            parent::initialize();
        }

    }

    /**
     * index view
     */
    public function index()
    {

        $labOrderTypesList = $this->LabOrderTypes->find()->where([
            'is_active' => 1
        ])->order(['display_order'])->combine('id', 'title');

        $user = $this->user;
        $labOrders = $this->LabOrders->find()
            ->contain([
                'Patient',
                'Provider'
            ])
            ->where([
                'is_deleted' => 0,
                'OR' => [
                    ['lab_order_workflow_status_id >=' => 13],
                    // only display lab orders that have at least gotten to step 3
                    ['lab_order_status_id IS NOT' => null, 'lab_order_status_id >=' => 3]
                ]
            ])
            // Limit the results to only lab orders created by providers in the same HCC Group / child groups
            ->matching('Patient', function ($query) use ($user) {
                return $query->where(['Patient.client_id IN' => $user['provider']['client_ids']]);
            })
            ->order(['LabOrders.created' => 'desc']);


        if ($user['provider']['provider_status_id'] == 1) {
            // if they are not approved, only let them see the orders they submitted
            $labOrders->where(['LabOrders.provider_id' => $user['provider']['id']]);
        } elseif ($user['provider']['provider_status_id'] == 2 && $this->user['provider']['has_seen_status_message'] == 0) {
            // Now they have seen the message so we can mark it and not show again
            $provider = $this->Providers->get($user['provider']['id']);
            $provider->has_seen_status_message = 1;
            $this->Providers->save($provider);
        }

        $statuses = $this->LabOrderStatuses->find('dropdown');
        $this->set(compact('labOrders', 'statuses', 'labOrderTypesList'));
        $this->set('_serialize', ['labOrders']);
    }

    public function select()
    {
        $labOrderTypeIds = [];

        // NOTE: USER MUST LOGOUT TO SEE LAB_ORDER_TYPE_ID
        foreach ($this->user['lab_order_initiator_types'] as $type) {
            $labOrderTypeIds[] = $type['lab_order_type_id'];
        }

        $labOrderTypes = $this->LabOrderTypes->find()->where([
            'is_active' => 1,
            'LabOrderTypes.id IN' => !empty($labOrderTypeIds) ? $labOrderTypeIds : [0]
        ])->order(['display_order'])->combine('id', 'title');

        if ($labOrderTypes->count() === 1) {
            return $this->redirectWithDefault([
                'prefix' => false,
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'add',
                $labOrderTypeIds[0]
            ]);
        }

        $this->set(compact('labOrderTypes'));

        if ($this->getRequest()->getData()) {
            $labOrderTypeEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrderTypes')
                ->find()
                ->where([
                    'LabOrderTypes.id' => (int)$this->getRequest()->getData('lab_order_type_id')
                ])
                ->firstOrFail();

            return $this->redirectWithDefault([
                'prefix' => false,
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'add',
                (int)$this->getRequest()->getData('lab_order_type_id')
            ]);
        }
    }

    /**
     * add lab order
     */
    public function add(int $labOrderTypeId)
    {
        if ($labOrderTypeId === $this->LabOrders::LAB_ORDER_PGX_ID) {
            // for PGX
            $initiatorTypeId = 2;
        } else if ($labOrderTypeId === $this->LabOrders::LAB_ORDER_COVID_ID) {
            // for COVID
            $initiatorTypeId = 3;
        } else {
            throw new \Exception('Invalid lab order type');
        }

        $startingWorkflowStatus = $this->LabOrderWorkflowStatuses->find()->where([
            'lab_order_initiator_type' => $initiatorTypeId,
            'is_active' => 1
        ])->order(['display_order'])->first();

        $providerId = null;
        // Provider  default to the logged in user
        $this->loadModel('Providers.Providers');
        $provider = $this->Providers->find()->where(['rxg_user_id' => $this->user['id']])->first();
        if ($provider) {
            $providerId = $provider->id;
        }

        if ($labOrderTypeId == $this->LabOrders::LAB_ORDER_PGX_ID) {
            if ($provider->provider_status_id == 1) {
                $defaultOrderStatusId = 2;
            } else {
                $defaultOrderStatusId = 1;
            }
        } else if ($labOrderTypeId == $this->LabOrders::LAB_ORDER_COVID_ID) {
            if ($provider->provider_status_id == 1) {
                $defaultOrderStatusId = 12;
            } else {
                $defaultOrderStatusId = 11;
            }
        }

        $order = $this->LabOrders->newEntity([
            'user_id' => $this->user['id'],
            'health_care_client_id' => $this->user['health_care_client_id'],
            'lab_order_workflow_status_id' => $startingWorkflowStatus->id,
            'provider_id' => $providerId,
            'lab_order_status_id' => $defaultOrderStatusId,
            'lab_order_initiator_type_id' => $initiatorTypeId,
            'lab_order_type_id' => $labOrderTypeId
        ], [
            'contain' => [
                'Patient',
                'Providers',
                'Provider',
                'Medications'
            ]
        ]);

        // If a 24w admin is logging in as a provider, track them
        if (isset($this->user['lib24watchUserId']) && (int)$this->user['lib24watchUserId'] > 0) {
            $order->created_by_lib24watch_user_id = $this->user['lib24watchUserId'];
        }

        $this->LabOrders->save($order);

        $this->redirect([
            'action' => 'patient',
            $order->id
        ]);
    }

    /**
     * @param int $labOrderId
     */
    public function edit(int $labOrderId)
    {
        $order = $this->LabOrders->find()->where([
            'LabOrders.id' => $labOrderId,
        ])->contain([
            'WorkflowStatus',
            'Patient',
            'Providers',
            'Provider',
            'Medications'
        ])->firstOrFail();

        $this->redirectToCurrentStep($order);
    }

    /**
     * @param $order
     */
    private function redirectToCurrentStep($order)
    {
        $this->redirect([
            'action' => $order->workflow_status->action,
            $order->id
        ]);
    }

    /**
     * @param int $labOrderId
     * @return mixed
     */
    private function getOrder(int $labOrderId, bool $validateStep = false)
    {
        $order = $this->LabOrders->find()->where([
            'LabOrders.id' => $labOrderId,
        ])->contain([
                'WorkflowStatus',
                'Patient',
                'Patient.PatientIcd10Codes' => function($q) use ($labOrderId) {
                    return $q->andWhere([
                        'PatientIcd10Codes.lab_order_id' => $labOrderId
                    ]);
                }, // TODO: update this to be conditional
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames',
                'Reports'
        ])->firstOrFail();

        if ($validateStep) {
            $currentWorkflowStatus = $this->LabOrderWorkflowStatuses->getCurrentStep($order->lab_order_initiator_type_id, $this->getRequest()->action);

            if (is_null($currentWorkflowStatus)) {
                throw new \Exception('Invalid workflow access');
            }
        }

        // Check to see if they should be able to see this lab order (in the same group as the provider)
        if (
            isset($order->provider->health_care_client_group_id) &&
            isset($this->user['provider']['health_care_client_group_ids']) &&
            !in_array(
                $order->provider->health_care_client_group_id,
                $this->user['provider']['health_care_client_group_ids']
            )
        ) {
            /*$this->redirectWithDefault([
                'action' => 'index'
            ], 'You do not have permission to edit this lab order.', 'danger');
            */
        }

        if ($this->user['provider']['provider_status_id'] == 1 && $this->user['provider']['id'] != $order->provider_id) {
            // if they are not approved, they can only edit their lab orders
            /*$this->redirectWithDefault([
                'action' => 'index'
            ], 'You do not have permission to edit this lab order.', 'danger');
            */
        }

        if ($this->getRequest()->getData('previous')) {
            $this->moveBackward($order);
        }

        return $order;
    }

    /**
     * @param $order
     */
    private function moveForward($order)
    {
        $nextWorkflowStatus = $this->LabOrderWorkflowStatuses->getNextStepByCurrentAction($order->lab_order_initiator_type_id, $this->getRequest()->action);

        if (is_null($nextWorkflowStatus)) {
            throw new \Exception('Workflow status not found');
        }

        $order->workflow_status = $nextWorkflowStatus;
        $order->lab_order_workflow_status_id = $nextWorkflowStatus->id;

        $this->LabOrders->save($order);

        $this->redirectToCurrentStep($order);
    }

    /**
     * @param $order
     * @return \Cake\Http\Response|null
     */
    private function moveBackward($order)
    {
        $previousWorkflowStatus = $this->LabOrderWorkflowStatuses->getNextStepByCurrentAction($order->lab_order_initiator_type_id,
            $this->getRequest()->action, '<');
        if (is_null($previousWorkflowStatus)) {
            return $this->redirect([
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'index',
                'prefix' => false
            ]);
        }

        $order->workflow_status = $previousWorkflowStatus;
        $order->lab_order_workflow_status_id = $previousWorkflowStatus->id;
        $this->LabOrders->save($order);

        $this->redirectToCurrentStep($order);
    }

    /**
     * @param int $labOrderId
     */
    public function tos(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId);
        $this->loadModel('SystemManagement.TermsOfServices');
        $termsOfServices = $this->TermsOfServices->find()->where([
            'lab_order_initiator_type' => $order->workflow_status->lab_order_initiator_type
        ])->contain(['Checkboxes'])->toArray();

        if ($this->getRequest()->getData()) {
            $accepted = true;
            foreach ($termsOfServices as $termsOfService) {
                foreach ($termsOfService->checkboxes as $checkbox) {
                    if (!$this->getRequest()->getData('checkbox_' . $checkbox->id)) {
                        $accepted = false;
                        break;
                    }
                }
            }

            if ($accepted) {
                $order->tos_accepted = 1;
                $this->LabOrders->save($order);
                $this->moveForward($order);
            }
        }

        if ($order->tos_accepted) {
            foreach ($termsOfServices as $termsOfService) {
                foreach ($termsOfService->checkboxes as $checkbox) {
                    $order->{'checkbox_' . $checkbox->id} = 1;
                }
            }
        }

        $this->set(compact('termsOfServices'));
        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderId
     */
    public function promo(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId);
        if ($this->getRequest()->getData('promo_code')) {
            $data = $this->getRequest()->getData();
            if ($data['promo_code']) {
                // validate this is an active promo code for this health care client
                $this->loadModel('SystemManagement.PromoCodes');
                $promoCodeEntity = $this->PromoCodes->find()->where([
                    'health_care_client_id' => $this->user['health_care_client_id'],
                    'LOWER(code)' => strtolower($data['promo_code']),
                    'is_active' => 1
                ])->first();
                if (!$promoCodeEntity) {
                    $order->setError('promo_code', 'Invalid Promo Code');
                } else {
                    $order->promo_code = $data['promo_code'];
                    $this->LabOrders->save($order);
                    $this->moveForward($order);
                }
            }
        } elseif ($this->getRequest()->getData('promo_form') == 1) {
            // No promo code entered, let them move on
            $this->moveForward($order);
        }

        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderId
     */
    public function patient(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId, true);
        if ($this->getRequest()->getData()) {
            if ($this->getRequest()->getData('patient_id')) {
                // they selected an existing patient
                $order->patient_id = $this->getRequest()->getData('patient_id');
                $patient = $this->Patients->get($order->patient_id);
            } else {
                // new patient
                $patient = $this->Patients->newEntity();
                // hardcode this since tos isnt an action anymore
                $order->tos_accepted = 1;
            }
            $patientData = $this->getRequest()->getData()['patient'];
            $patientData['health_care_client_id'] = $this->user['provider']['health_care_client_id'];
            $this->Patients->patchEntity($patient, $patientData);
            if ($this->Patients->save($patient)) {
                $order->patient = $patient;
            }
            $order->setErrors($patient->getErrors());
            if (empty($order->getErrors())) {
                $this->LabOrders->save($order);

                if ($this->getRequest()->getData('skip_to_review') && $order->isSkippableToReview) {
                    return $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'review',
                        'prefix' => false,
                        $order->id
                    ]);
                } else {
                    $this->moveForward($order);
                }
            }
        }

        $this->loadModel('SystemManagement.Ethnicities');
        $this->set('ethnicities', $this->Ethnicities->find('dropdown'));

        $this->loadModel('SystemManagement.Sexes');
        $this->set('sexes', $this->Sexes->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->order([
            'abbr' => 'ASC'
        ])->toArray());

        $hidePrevious = true;
        $this->set(compact('hidePrevious', 'order'));
    }

    /**
     * @param int $labOrderId
     */
    public function covid(int $labOrderId)
    {
        $icd10Codes = TableRegistry::getTableLocator()->get('SystemManagement.Icd10Codes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->order(['display_order' => 'asc'])
            ->toArray();

        $specimenTypesList = TableRegistry::getTableLocator()->get('SystemManagement.SpecimenTypes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->order(['display_order' => 'ASC'])
            ->combine('id', 'title')
            ->toArray();

        $order = $this->getOrder($labOrderId, true);

        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            if (isset($data['patient']) && isset($data['patient']['patient_icd10_codes']) && !empty(array_filter($data['patient']['patient_icd10_codes']))) {
                $patientIcd10Codes = [];
                foreach ($data['patient']['patient_icd10_codes'] as $id => $checked) {
                    if ($checked === '1') {
                        $patientIcd10Codes[] = [
                            'lab_order_id' => $labOrderId,
                            'patient_id' => $data['patient']['id'],
                            'icd10_code_id' => $id
                        ];
                    }
                }

                if (!empty($patientIcd10Codes)) {
                    $data['patient']['patient_icd10_codes'] = $patientIcd10Codes;
                }
            }

            if (isset($data['specimen_time']) && mb_strlen($data['specimen_time'] > 0)) {
                $data['specimen_time'] = \Cake\I18n\Time::createFromFormat(
                    'H:i a',
                    $data['specimen_time'],
                    'America/New_York'
                );
            }

            $order = $this->LabOrders->patchEntity(
                $order, $data,
                [
                    'validate' => 'covid',
                    'associated' => [
                        'Patient' => [
                            'validate' => 'icd10codes',
                        ],
                        'Patient.PatientIcd10Codes',
                    ]
                ]
            );

            if (empty($order->getErrors())) {
                // delete existing patient icd10 codes based on lab order
                TableRegistry::getTableLocator()->get('Patient.PatientIcd10Codes')
                    ->deleteAll([
                        'patient_id' => $order->patient->id,
                        'lab_order_id' => $labOrderId,
                    ]);

                $this->LabOrders->save($order, [
                    'associated' => [
                        'Patient.PatientIcd10Codes'
                    ]
                ]);

                if ($this->getRequest()->getData('skip_to_review') && $order->isSkippableToReview) {
                    return $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'review',
                        'prefix' => false,
                        $order->id
                    ]);
                } else {
                    $this->moveForward($order);
                }
            }
        }

        $this->set(compact('order', 'icd10Codes', 'specimenTypesList'));
    }

    /**
     * @param int $labOrderId
     */
    public function provider(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId);

        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();
            $hasPrimaryCarePhysician = false;
            $hasOrderingPhysician = false;
            $hasNpi = false;
            foreach ($data['providers'] as $index => $provider) {
                if (trim($provider['provider_name']) == '' && trim($provider['npi_number']) == '') {
                    unset($data['providers'][$index]);
                    continue;
                }
                if (isset($provider['npi_number']) && mb_strlen(trim($provider['npi_number']) > 0)) {
                    $hasNpi = true;
                }
                if (isset($provider['is_primary_care_provider'])) {
                    $data['providers'][$index]['is_primary_care_provider'] = 1;
                    $hasPrimaryCarePhysician = true;
                }
                if (isset($provider['is_ordering_physician'])) {
                    $data['providers'][$index]['is_ordering_physician'] = 1;
                    $hasOrderingPhysician = true;
                }
            }
            $this->LabOrders->patchEntity($order, $data);
            if (empty($data['providers'])) {
                $order->setError('providers', 'At least one provider is required');
            } else {
                if (!$hasPrimaryCarePhysician && $order->lab_order_type_id !== $this->LabOrders::LAB_ORDER_COVID_ID) {
                    $order->setError('providers', 'At least one provider must be the Primary Care Physician');
                }
                if (!$hasOrderingPhysician && $order->lab_order_type_id !== $this->LabOrders::LAB_ORDER_COVID_ID) {
                    $order->setError('providers', 'At least one provider must be the Ordering Physician');
                }
                if (!$hasNpi) {
                    $order->setError('providers', 'All providers must have an NPI number');
                }
                if (!$hasOrderingPhysician && !$hasPrimaryCarePhysician && $order->lab_order_type_id !== $this->LabOrders::LAB_ORDER_COVID_ID) {
                    $order->setError('providers',
                        'At least one provider must be the Primary Care Physician and Ordering Physician');
                }
            }
            if (empty($order->getErrors())) {
                $this->LabOrders->save($order);

                if ($this->getRequest()->getData('skip_to_review') && $order->isSkippableToReview) {
                    return $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'review',
                        'prefix' => false,
                        $order->id
                    ]);
                } else {
                    $this->moveForward($order);
                }
            }

        }
        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderId
     */
    public function medications(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId, true);
        $drugTable = \Cake\ORM\TableRegistry::getTableLocator()->get('SystemManagement.Drugs');
        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();
            foreach ($data['medications'] as $index => $medication) {
                if ($medication['drug_id'] == '') {
                    unset($data['medications'][$index]);
                }
            }

            $this->LabOrders->patchEntity($order, $data);

            $this->LabOrders->save($order);

            if ($this->getRequest()->getData('skip_to_review') && $order->isSkippableToReview) {
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'review',
                    'prefix' => false,
                    $order->id
                ]);
            } else {
                $this->moveForward($order);
            }
        }
        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderId
     */
    public function shipping(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId, true);
        if (
            $order->sample_id && $order->sample_id != '' &&
            $order->ship_to_provider == 0 && $order->ship_to_patient == 0
        ) {
            $order->kit_administered_on_site = 1;
        }
        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            // sanitize the sample id for new lab orders
            if ($data['ship_to_provider'] == 1 || $data['ship_to_patient'] == 1) {
                // Auto set sample id
                $data['sample_id'] = $this->LabOrders->generateUniqueSampleId($order);
            } else if ($order->lab_order_status_id === 1 || $order->lab_order_status_id === 11) {
                $data['sample_id'] = FormattingHelper::sanitizeFilename($data['sample_id']);
            }
            $dupOrder = $this->LabOrders->find()
                ->select(['LabOrders.id'])
                ->matching('Provider.HealthCareClient', function ($query) use ($order) {
                    return $query->where(['lab_id' => $order->provider->health_care_client->lab_id]);
                })
                ->where(['sample_id' => $data['sample_id'], 'LabOrders.id <>' => $order->id])
                ->first();
            if (!empty($dupOrder)) {
                $order->setError('sample_id', __('This ID has already been used on Order #{0}.', $dupOrder->id));
            }
            $this->LabOrders->patchEntity($order, $data);
            if ($data['kit_administered_on_site'] == 0 && $data['ship_to_provider'] == 0 && $data['ship_to_patient'] == 0) {
                $order->setError('ship_to_patient', 'At least one kit administration option must be selected.');
            }

            if (isset($data['kit_administered_on_site']) && $data['kit_administered_on_site'] == 1 && trim($data['sample_id']) == '') {
                $order->setError('sample_id', 'Collection Kit is required for kits administered on site.');
            }

            if ($data['ship_to_provider'] == 1 || $data['ship_to_patient'] == 1) {
                $shippingFields = [
                    'shipping_first_name' => 'First name',
                    'shipping_last_name' => 'Last name',
                    'shipping_address_1' => 'Address',
                    'shipping_city' => 'City',
                    'shipping_lib24watch_state_abbr' => 'State',
                    'shipping_postal_code' => 'Zip'
                ];
                foreach ($shippingFields as $field => $text) {
                    if (trim($data[$field]) == '') {
                        $order->setError($field, $text . ' is required.');
                    }
                }
            }

            if (empty($order->getErrors())) {
                $this->LabOrders->save($order);
                if ($this->getRequest()->getData('skip_to_review') && $order->isSkippableToReview) {
                    return $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'review',
                        'prefix' => false,
                        $order->id
                    ]);
                } else {
                    $this->moveForward($order);
                }
            }
        }
        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->toArray());

        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function review(int $labOrderId)
    {
        $specimenTypesList = TableRegistry::getTableLocator()->get('SystemManagement.SpecimenTypes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->combine('id', 'title')
            ->toArray();

        $icd10Codes = TableRegistry::getTableLocator()->get('SystemManagement.Icd10Codes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->order(['display_order' => 'asc'])
            ->toArray();

        $order = $this->getOrder($labOrderId, true);
        $successMessage = "Your order for {$order->patient->first_name} {$order->patient->last_name} has been submitted. Should any additional information be required, we’ll contact you. Thank you.";

        if ($this->getRequest()->getData('save-only')) {

            $this->LabOrders->patchEntity($order, $this->getRequest()->getData());

            $this->LabOrders->save($order);

            $successMessage = "Your order for {$order->patient->first_name} {$order->patient->last_name} has been saved. Please edit the order and click \"Submit Order\" to submit to the lab. Thank you.";

            $this->Flash->set($successMessage);

            $this->redirectWithDefault([
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'index',
                'prefix' => false
            ], $successMessage);
        }

        if ($this->getRequest()->getData('submit-order')) {
            $this->LabOrders->patchEntity($order, $this->getRequest()->getData());
            //Reload labOrder
            $order = $this->LabOrders->find()
                ->where([
                    'LabOrders.id' => $labOrderId
                ])->contain([
                    'WorkflowStatus',
                    'Patient',
                    'Patient.PatientIcd10Codes' => function($q) use ($labOrderId) {
                        return $q->andWhere([
                            'PatientIcd10Codes.lab_order_id' => $labOrderId
                        ]);
                    }, // TODO: update this to be conditional
                    'Patient.PatientIcd10Codes.Icd10Codes',
                    'Patient.Sex',
                    'Patient.Ethnicity',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Provider.HealthCareClient.CovidLabs',
                    'Medications.Drug.BrandNames'
                ])
                ->firstOrFail();

            // Actually submit to lab here
            $healthCareClient = $order->provider->health_care_client;

            if ($order->lab_order_type_id  == \Cake\ORM\TableRegistry::getTableLocator()->get('LabOrders.LabOrders')::LAB_ORDER_PGX_ID) {
                $labId = $healthCareClient->lab_id;
            } else {
                $labId = $healthCareClient->covid_lab_id;
            }

            #$labId = $healthCareClient->lab_id;

            if (is_null($labId)) {
                throw new \Exception('No lab found');
            }

            $lab = new Lab($labId);

            if ($order->lab_order_type_id  == $this->LabOrders::LAB_ORDER_PGX_ID) {
                $order->lab_order_status_id = 2;
                $connectionInformation = $healthCareClient->lab->toArray();
            } else if ($order->lab_order_type_id == $this->LabOrders::LAB_ORDER_COVID_ID) {
                $order->lab_order_status_id = 12;
                $connectionInformation = $healthCareClient->covid_lab->toArray();
            }

            $this->LabOrders->save($order);

            // HL7 spits out warnings, silence them!
            Configure::write('debug', 0);

            $this->loadModel('HealthCareClients.HealthCareClientFeatures');
            $payload = $lab->getRequestDriver()->createLabOrder($order, $this->HealthCareClientFeatures->hasFeature($healthCareClient->id, 'de-identified-lab-data'));

            if ($order->lab_order_type_id  == $this->LabOrders::LAB_ORDER_PGX_ID && !isset($order->sample_id)) {
                throw new \Exception('Sample ID required');
            } else if ($order->lab_order_type_id == $this->LabOrders::LAB_ORDER_COVID_ID && !isset($order->specimen_barcode)) {
                throw new \Exception('Specimen barcode required');
            }

            if ($order->lab_order_type_id  == $this->LabOrders::LAB_ORDER_PGX_ID) {
                $filename = $order->sample_id;
                $remotePath = $connectionInformation['submit_path'] . '/' . $filename. '.hl7';
            } else if ($order->lab_order_type_id == $this->LabOrders::LAB_ORDER_COVID_ID) {
                $filename = $order->specimen_barcode;
                $remotePath = '/' . $filename. '.hl7';
            }

            $labOrderFileName = ROOT . '/data/lab_orders/' . $order->id . '.hl7';
            $connectionInformation['local_file'] = $labOrderFileName;
            $connectionInformation['remote_file'] = $remotePath;

            Configure::write('debug', 1);

            try {
                $result = $lab->getConnector()->createConnection($connectionInformation)->post($connectionInformation,
                $payload);
            } catch (\Exception $e) {
                // couldn't post file to lab
                $result = false;
            }

            if ($result) {
                if ($order->lab_order_type_id  == $this->LabOrders::LAB_ORDER_PGX_ID) {
                    $order->lab_order_status_id = 3;
                } else if ($order->lab_order_type_id == $this->LabOrders::LAB_ORDER_COVID_ID) {
                    $order->lab_order_status_id = 13;
                } else {
                    throw new \Exception('Unable to find a lab order status to set');
                }

                $this->LabOrders->save($order);

                $mailer = $this->getMailer('LabOrders.LabOrders');
                $mailer->send('labOrderSubmitted', [
                    $order,
                    $this->user['email'],
                    $this->user['first_name']
                ]);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $order->id,
                    $order->lab_order_status_id,
                    'Submit to Lab',
                    'Success'
                );

                $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'confirmation',
                    'prefix' => false,
                    $order->id
                ], $successMessage);


            } else {
                // submit to lab failed, log it and let the user know
                $order->lab_order_status_id = 9;
                $this->LabOrders->save($order);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $order->id,
                    $order->lab_order_status_id,
                    'Submit to Lab',
                    'Submit to Lab Failed'
                );

                $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'review',
                    'prefix' => false,
                    $order->id
                ], 'There was an issue submitting your lab order for processing, please contact RxGenomix.',
                    'danger');
            }
        }

        $this->loadModel('SystemManagement.Ethnicities');
        $this->set('ethnicities', $this->Ethnicities->find('dropdown'));

        $this->loadModel('SystemManagement.Sexes');
        $this->set('sexes', $this->Sexes->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->toArray());

        $this->set(compact('order', 'specimenTypesList', 'icd10Codes'));
    }

    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function view(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId);

        $icd10Codes = TableRegistry::getTableLocator()->get('SystemManagement.Icd10Codes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->order(['display_order' => 'asc'])
            ->toArray();

        $specimenTypesList = TableRegistry::getTableLocator()->get('SystemManagement.SpecimenTypes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->combine('id', 'title')
            ->toArray();

        $this->loadModel('SystemManagement.Ethnicities');
        $this->set('ethnicities', $this->Ethnicities->find('dropdown'));

        $this->loadModel('SystemManagement.Sexes');
        $this->set('sexes', $this->Sexes->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->toArray());

        $this->set(compact('specimenTypesList', 'icd10Codes', 'order'));
    }

    /**
     * @param int $labOrderId
     */
    public function confirmation(int $labOrderId)
    {
        $order = $this->getOrder($labOrderId);
        $this->set(compact('order'));
    }

    /**
     * @param int $labOrderReportId
     */
    public function getRiskScore(int $labOrderReportId)
    {
        $reportResultIds = [];

        // disable debugging
        Configure::write('debug', 0);
        \header('Content-type: application/json');
        $this->viewBuilder()->setLayout('ajax');

        if (!is_null($this->getRequest()->getData('resultIds'))) {
            $labOrdersInstance = new LabOrders;
            $labOrderReportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');

            $labOrderReportEntity = $labOrderReportsTable
                ->find()
                ->where([
                    'LabOrderReports.id' => $labOrderReportId
                ])
                ->first();

            $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')->get(
                $labOrderReportEntity->lab_order_id,
                [
                    'contain' => [
                        'WorkflowStatus',
                        'Patient',
                        'Patient.Sex',
                        'Providers',
                        'Provider.Users',
                        'Provider.HealthCareClient.AssignedFeatures',
                        'Medications.Drug.BrandNames',
                        'Medications.Medication',
                        'Provider.HealthCareClient.Lab'
                    ]
                ]);

            $riskScore = 0;
            $riskMaxScore = 0;
            $riskFutureScore = 0;
            $riskFutureMaxScore = 0;

            if ($labOrderReportEntity) {
                $foundDrugIds = [];
                $foundTsiMedicationIds = [];
                $riskMethod = 'scoreByMedication'; // drug or medication
                $resultIds = base64_decode($this->getRequest()->getData('resultIds'));
                parse_str($resultIds, $patchData);
                $patchData = (new Collection($patchData['results']))->indexBy('id')->toArray();
                $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');
                $resultSet = $reportResultsTable->find('filteredReportResults', [
                    'lab_order_id' => $labOrderReportEntity->lab_order_id,
                    'lab_order_report_id' => $labOrderReportEntity->id,
                ])
                ->formatResults(function (\Cake\Collection\CollectionInterface $results) use (
                    &$riskScore,
                    &$riskMaxScore,
                    &$riskFutureScore,
                    &$riskFutureMaxScore,
                    $riskMethod,
                    &$foundTsiMedicationIds,
                    &$foundDrugIds,
                    $labOrdersInstance,
                    $reportResultsTable,
                    $patchData
                ) {
                    return call_user_func_array(
                        [
                            $labOrdersInstance,
                            $riskMethod
                        ], [
                            $results,
                            &$riskScore,
                            &$riskMaxScore,
                            &$riskFutureScore,
                            &$riskFutureMaxScore,
                            &$foundTsiMedicationIds,
                            &$foundDrugIds,
                            $patchData
                        ]
                    );
                })->toArray();
            }

            $labOrdersInstance->calculateRiskScore(
                $riskMethod,
                $labOrderEntity,
                $riskScore,
                $riskMaxScore,
                $riskFutureScore,
                $riskFutureMaxScore,
                $foundTsiMedicationIds,
                $foundDrugIds
            );
        }

        echo json_encode([
            'riskScore' => $riskFutureScore ?? null
        ]);

        exit;
    }

    /**
     * @param int $labOrderReportId
     */
    public function getMapProblemRationales(int $mapProblemCategoryId)
    {
        $results = [];

        // disable debugging
        #Configure::write('debug', 0);
        #\header('Content-type: application/json');
        #$this->viewBuilder()->setLayout('ajax');

        $mapProblemRationalesTable = TableRegistry::getTableLocator()->get('SystemManagement.MapProblemRationales');
        $results = $mapProblemRationalesTable->getList($mapProblemCategoryId);

        echo json_encode([
            'success' => true,
            'results' => $results,
        ]);

        exit;
    }

    /**
     * Delete a lab order medication result entity
     *
     * @param int $labOrderId
     * @param int $resultItemId
     *
     */
    public function deleteMedicationResult(int $labOrderId = 0, int $resultItemId = 0)
    {
        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->firstOrFail();

        $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');

        // If this medication was returned in the results, bind it
        $medicationExistsQuery = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $latestReportEntity->id,
                'LabOrderReportResults.id' => $resultItemId,
                'is_relevant_original' => 0
            ])
            ->contain(
                [
                    'ResultMedications',
                    'ResultMapNotes',
                ]
            );

        if ($medicationExistsQuery->count() > 0) {
            $results = $medicationExistsQuery->toArray();

            if (!empty($results) && count ($results) < 2) {
                foreach ($results as $result) {
                    // this is an ORIGINAL result. dont delete anything
                    if ($result->detected_issue_index != null) {
                        $result->is_relevant = false;

                        // reset result map notes
                        $result->result_map_note->patient_notes = null;
                        $result->result_map_note->patient_notes = null;
                        $result->result_map_note->alternate_drug_id = null;
                        $result->result_map_note->alternate_result_id = null;
                        $result->result_map_note->lab_order_medication_id = null;

                        if (!TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults')->save($result, [
                            'associated' => 'ResultMapNotes'
                        ])) {
                            throw new \Exception('Failed to update result row');
                        }

                    // delete records for "faux" results
                    } else if ($result->is_lab_order_medication) {
                        $labOrderMedicationId = $result->result_medications[0]->lab_order_medication_id;
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMapNotes')->deleteOrFail($result->result_map_note);
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMedications')->deleteOrFail($result->result_medications[0]);
                        TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults')->deleteOrFail($result);

                        if ($labOrderMedicationId) {
                            $labOrderMed = $this->LabOrders->Medications->get(
                                $labOrderMedicationId,
                                ['contain' => 'Drug']
                            );
                            if ($this->LabOrders->Medications->delete($labOrderMed)) {
                                $drug = $labOrderMed->get('drug');
                                $this->Flash->success(
                                    __(
                                        "{0} successfully deleted from MAP.",
                                        !empty($drug) ? $drug->specificproductname : __('This medication was')
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
        return $this->redirectWithDefault(
            [
                'prefix' => false,
                'plugin' => 'LabOrders',
                'controller' => 'LabOrders',
                'action' => 'reviewMap',
                $labOrderId
            ]
        );
    }

    /**
     * Detect if a user is a test user to show the delete medications button
     *
     */
    protected function isTestUser($user) {
        if (
            !empty($user) && !empty($user['email']) &&
            ((strpos($user['email'], 'meder')) !== false || (strpos($user['email'], 'djrpanuncio')) !== false)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param int $labOrderId
     */
    public function reviewMap(int $labOrderId)
    {
        if ($labOrderId) {
            $isTestUser = $this->isTestUser($this->user);
            // validation
            $order = $this->getOrder($labOrderId);

            $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
                ->find()
                ->contain([
                    'Medications.Medication',
                    'Reports'
                ])
                ->where([
                    'LabOrders.id' => $labOrderId
                ])
                ->firstOrFail();

            $labOrderReportId = $labOrderEntity->reports[0]->id;

            $labOrdersInstance = new LabOrders;
            #$labOrdersInstance->generateLabOrderNoteEntitiesForResults($labOrderReportId);

            if (!$order || $order->lab_order_status_id < 7 || empty($order->reports)) {

                // need to manually set these
                $this->Flash->set('Lab Order not ready for review.');

                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ]);
            }

            $reportAdditionalTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportAdditional');

            $additionalEntity = $reportAdditionalTable
                ->findOrCreate([
                    'lab_order_report_id' => $order->reports[0]->id
                ]);

            $labOrder = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames',
                    'Medications.Medication'
                ]
            ]);

            $mapProblemCategoriesList = TableRegistry::getTableLocator()->get('SystemManagement.MapProblemCategories')->getList();

            $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
            $latestReportEntity = $reportsTable->find('latestReportFull', [
                'lab_order_id' => $labOrderId,
            ])->firstOrFail();

            $severityLevelsList = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultSeverityLevels')
                ->find()
                ->where([
                    'is_active' => 1
                ])
                ->combine('id', 'title')
                ->toArray();

            // refs #38411
            $labOrdersInstance::stripHtml($latestReportEntity);

            $resultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');
            $alternativeResults = $resultsTable
                ->find()
                ->select([
                    'id',
                    'lab_order_report_id',
                    'tsi_severity',
                    'tsi_detail',
                ])
                ->where([
                    'lab_order_report_id' => $latestReportEntity->id,
                    'is_relevant' => 0,
                ])
                ->contain([
                    'ResultMedications',
                    'ResultMedications.MedicationsMapped',
                    'ResultMedications.MedicationsMapped.Drug',
                    'ResultMedications.MedicationsMapped.Medication',
                    'ResultMapNotes',
                ])
                ->matching('ResultMedications')
                ->indexBy(function($row) {
                    return $row['result_medications'][0]['medication_id'];
                })
                ->toArray();

            $scores = $resultsTable->getScores($latestReportEntity->id);

            if (!is_null($scores) && isset($scores['currentRiskScore']) && isset($scores['futureRiskScore'])) {
                $this->set('currentRiskScore', $scores['currentRiskScore']);
                $this->set('futureRiskScore', $scores['futureRiskScore']);
            }

            $stripHtml = true;

            $saved = false;
            $errors = false;
            if ($this->getRequest()->getData()) {
		    \Cake\Log\Log::debug('post worked');
                $data = $this->getRequest()->getData();

                if ($stripHtml) {
                    if (isset($data['results']) && !empty($data['results'])) {
                        foreach ($data['results'] as &$result) {
                            if (isset($result['result_map_note']) && isset($result['result_map_note']['detail']) && !empty($result['result_map_note']['detail'])) {
                                $result['result_map_note']['detail'] = strip_tags($result['result_map_note']['detail']);
                            }

                            if (isset($result['result_map_note']) && isset($result['result_map_note']['prescriber_notes']) && !empty($result['result_map_note']['prescriber_notes'])) {
                                $result['result_map_note']['prescriber_notes'] = strip_tags($result['result_map_note']['prescriber_notes']);
                            }

                            if (isset($result['result_map_note']) && isset($result['result_map_note']['patient_notes']) && !empty($result['result_map_note']['patient_notes'])) {
                                $result['result_map_note']['patient_notes'] = strip_tags($result['result_map_note']['patient_notes']);
                            }
                        }
                    }

                    if (isset($data['report_additional']) && !empty($data['report_additional'])) {
                        foreach ($data['report_additional'] as &$additionalField) {
                            $additionalField = strip_tags($additionalField);
                        }
                    }
                }

                $latestReportEntity = $reportsTable->patchEntity(
                    $latestReportEntity,
                    $data,
                    [
                        'associated' => [
                            'ReportAdditional' => [
                                'validate' => 'review'
                            ],
                            'Results',
                            'Results.ResultMapNotes' => [
                                'validate' => 'review'
                            ],
                        ]
                    ]
                );

		    \Cake\Log\Log::debug($latestReportEntity->getErrors());

                if (empty($latestReportEntity->getErrors())) {
                    if ($reportsTable->save($latestReportEntity, [
                        'associated' => [
                            'ReportAdditional',
                            'Results',
                            'Results.ResultMapNotes',
                        ]
                    ])) {
                        $saved = true;
                    }
                } else {
                    $errors = true;
                }

		\Cake\Log\Log::debug('saved');
		\Cake\Log\Log::debug($saved);

                if ($saved) {
                    $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $latestReportEntity->id . '.pdf';
                    $labOrdersInstance = new LabOrders;
                    $healthCareClient = $labOrder->provider->health_care_client;

                    try {
                        // generate map
                        if (is_readable($labOrderReportPdfFileName)) {
                            $mapTypes = $labOrdersInstance->getActiveMapTypes();

                            $counter = 0;
                            foreach ($mapTypes as $mapKey => $mapType) {
                                $generatedMap = $labOrdersInstance->generateMap($labOrder->id, $mapKey);
                                if ($generatedMap) {
                                    $counter++;
                                }
				sleep(1);
                            }

                            if ($counter == 2) {
                                \SystemManagement\Libs\EventLogs::write(
                                    $healthCareClient->id,
                                    $labOrder->id,
                                    $labOrder->lab_order_status_id,
                                    'MAP Generated',
                                    'Success'
                                );
                            }

                            $successMessage = 'Medication Action Plan generated';

                            $this->Flash->set($successMessage);

                            return $this->redirectWithDefault(
                                [
                                    'plugin' => 'LabOrders',
                                    'controller' => 'LabOrders',
                                    'action' => 'index',
                                    'prefix' => false,
                                ],
                                $successMessage,
                                'success'
                            );

			} else {
				\Cake\Log\Log::debug('Could not read report pdf');
			}
                    } catch (\Exception $e) {
                        var_dump('couldnt generate map');
                        var_dump($e->getMessage());
			\Cake\Log\Log::debug($e->getMessage());
                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'MAP failed to generate',
                            'Warning'
                        );
                    }
                }
            }

            $this->set(compact('isTestUser', 'saved', 'alternativeResults', 'currentRiskScore', 'futureRiskScore', 'mapProblemCategoriesList', 'labOrder', 'labOrderId', 'severityLevelsList'));
            $this->set('labOrderReportEntity', $latestReportEntity);
            $this->set('saved', $saved);
        }
    }

    /**
     * @param int $labOrderId
     * @param string $mapType
     *
     */
    public function downloadMap(int $labOrderId, string $mapType)
    {
        if ($labOrderId) {
            // validation, dont remove
            $order = $this->getOrder($labOrderId);

            $order = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames',
                    'Medications.Medication',
                    'Reports'
                ]
            ]);

            $labOrdersInstance = new LabOrders;

            if (!$order || $order->lab_order_status_id < 7 || empty($order->reports)) {
                $this->Flash->set('Lab Order not ready for download.');
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ]);
            }

            if ($labOrdersInstance::isValidMapType($mapType) === false) {
                $this->Flash->set('Invalid map type specified.');
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ]);
            }

            //$labOrdersInstance->generateMap($order->id, $mapType);
            $download = $labOrdersInstance->downloadMap($order->id, $mapType);

            if ($download === false) {
                $this->Flash->set('Lab Order not ready for download.');
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ]);
            }
        }
    }

    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function download(int $labOrderId)
    {
        if ($labOrderId) {
            $order = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames',
                    'Reports'
                ]
            ]);

            $order = $this->getOrder($labOrderId);

            // TODO: have this look at HCC Group
            if ($order->health_care_client_id != $this->user['health_care_client_id']) {
                $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ], 'You do not have permission to view that report.', 'warning');
            }

            $coverLetter = null;
            if (!$order || $order->lab_order_status_id < 7 || empty($order->reports)) {
                $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => false,
                ], 'Lab Order not ready for download.', 'warning');
            } else {
                $isWebView = true;
                $labOrdersInstance = new LabOrders;
                $useExisting = true;

                if ($useExisting === false) {
                    try {
                        $labOrdersInstance->generateCoverLetter($order->id, $isWebView, $useExisting);
                    } catch (\Exception $e) {
                        \Cake\Log\Log::debug('Failed to generate cover letter');
                    }
                }

                try {
                    $labOrdersInstance->renderPdf($order->id, $this->user['provider']['id']);
                } catch (\Exception $e) {
                    $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'index',
                        'prefix' => false,
                    ], 'Lab Order not ready for download.', 'warning');
                }
            }

        } else {
            throw new \Exception('Lab Order ID is required');
        }
    }

    /**
     * Dynamically add a medication to a lab order (after its been processed)
     *
     */
    public function addMedicationToLabOrder(int $labOrderId = 0, int $drugId = 0)
    {
        $isTestUser = false;
        if ($this->isTestUser($this->user)) {
            $isTestUser = true;
        }

        $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');

        // disable debugging
        #Configure::write('debug', 0);
        #\header('Content-type: application/json');
        #$this->viewBuilder()->setLayout('ajax');
        #
        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->first();

        if (is_null($latestReportEntity)) {
            $response['message'] = 'Could not find report';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $drugEntity = TableRegistry::getTableLocator()->get('SystemManagement.Drugs')
            ->find()
            ->where([
                'Drugs.id' => $drugId
            ])
            ->contain([
                'TsiMedication'
            ])
            ->first();

        $response = [
            'success' => false,
            'message' => ''
        ];

        if (is_null($drugEntity)) {
            $response['message'] = 'Invalid drug entity';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->contain([
                'Reports'
            ])
            ->matching('Reports')
            ->first();

        if (is_null($labOrderEntity)) {
            $response['message'] = 'Invalid lab order entity';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        // Detect if the drug or medication already exists in the lab order (and prevent if so)
        $labOrderMedicationEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->matching('Medications', function($q) use ($drugEntity) {
                return $q->andWhere([
                    'OR' => [
                        'medication_id' => $drugEntity->medication_id, // Do not allow the same medication class to be added
                        'drug_id' => $drugEntity->id // Do not allow the same exact drug to be added
                    ]
                ]);
            })
            ->first();

        if (!is_null($labOrderMedicationEntity)) {
            $response['message'] = 'Drug already exists in lab order';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        // If this medication was returned in the results, bind it
        $medicationExistsQuery = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $latestReportEntity->id,
                'is_relevant' => 0,
            ])
            ->contain(['ResultMedications'])
            ->matching('ResultMedications', function($q) use ($drugEntity) {
                return $q->andWhere([
                    'ResultMedications.lab_order_medication_id IS' => null,
                    'ResultMedications.medication_id' => $drugEntity->medication_id
                ]);
            })
            ->matching('ResultMapNotes', function($q) {
                return $q->andWhere([
                    'ResultMapNotes.patient_notes IS' => null,
                    'ResultMapNotes.prescriber_notes IS' => null,
                    'ResultMapNotes.map_problem_rationale_id IS' => null,
                    'ResultMapNotes.alternate_drug_id IS' => null,
                    'ResultMapNotes.lab_order_medication_id IS' => null
                ]);
            });

        $labOrderMedicationsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderMedications');

        $labOrderMedicationEntity = $labOrderMedicationsTable
            ->newEntity([
                'lab_order_id' => $labOrderId,
                'drug_id' => $drugEntity->id,
                'medication_id' => $drugEntity->medication_id ?? null
            ]);

        // If a 24w admin is logging in as a provider, track them
        if (isset($this->user['lib24watchUserId']) && (int)$this->user['lib24watchUserId'] > 0) {
            $labOrderMedicationEntity->created_by_lib24watch_user_id = $this->user['lib24watchUserId'];
        }

        $labOrderReportId = $labOrderEntity->reports[0]->id;

        $reportResultId = null;
        $reportResultViewHtml = '';

        $result = $labOrderMedicationsTable->getConnection()->transactional(function () use (
            $labOrderMedicationEntity,
            $labOrderMedicationsTable,
            $medicationExistsQuery,
            $labOrderReportId,
            $reportResultsTable,
            $reportsTable,
            $latestReportEntity,
            &$reportResultId,
            &$reportResultViewHtml,
            $labOrderId,
            $isTestUser
        ) {
            $encounteredError = false;

            $labOrdersInstance = new LabOrders;

            $medicationSave = $labOrderMedicationsTable->save($labOrderMedicationEntity);

            if (!$medicationSave) {
                $encounteredError = true;
            }

            if ($medicationExistsQuery->count() > 0) {
                $reportResultEntity = $medicationExistsQuery->toArray()[0];
                $reportResultId = $reportResultEntity->id;

                $reportResultEntity = $reportResultsTable
                    ->find()
                    ->where([
                        'id' => $reportResultId
                    ])
                    ->first();

                // change report result to relevant now
                $reportResultEntity->is_relevant = true;

                if (!$reportResultsTable->save($reportResultEntity)) {
                    $encounteredError = true;
                }

            } else {
                $reportResult = [
                    'lab_order_report_id' => $labOrderReportId,
                    'is_relevant' => true,
                    'is_lab_order_medication' => true,
                ];

                $reportResult['result_medications'] = [
                    [
                        'lab_order_medication_id' => $labOrderMedicationEntity->id,
                        'medication_id' => $labOrderMedicationEntity->medication_id,
                        'tsi_medication_id' => $labOrderMedicationEntity->tsi_medication_id ?? null
                    ]
                ];

                $reportResult['result_map_note'] = [
                    [
                        'is_included' => 1,
                        'lab_order_medication_id' => $labOrderMedicationEntity->id
                    ]
                ];

                $reportResultEntity = $reportResultsTable->newEntity($reportResult);

                if (!$reportResultsTable->save($reportResultEntity, [
                    'associated' => [
                        'ResultMapNotes',
                        'ResultMedications'
                    ]
                ])) {
                    $encounteredError = true;
                } else {
                    $reportResultId = $reportResultEntity->id;
                }
            }

            $alternativeResults = $reportResultsTable
                ->find()
                ->select([
                    'id',
                    'lab_order_report_id',
                    'tsi_severity',
                    'tsi_detail',
                ])
                ->where([
                    'lab_order_report_id' => $latestReportEntity->id,
                    'is_relevant' => 0,
                ])
                ->contain([
                    'ResultMedications',
                    'ResultMedications.MedicationsMapped',
                    'ResultMedications.MedicationsMapped.Drug',
                    'ResultMedications.MedicationsMapped.Medication',
                    'ResultMapNotes',
                ])
                ->matching('ResultMedications')
                ->indexBy(function($row) {
                    return $row['result_medications'][0]['medication_id'];
                })
                ->toArray();

            $mapProblemCategoriesList = TableRegistry::getTableLocator()->get('SystemManagement.MapProblemCategories')->getList();

            // REPULL so results are refreshed after saving new ones
            $latestReportEntity = $reportsTable->find('latestReportFull', [
                'lab_order_id' => $labOrderId,
            ])->first();

            // strip html
            $labOrdersInstance::stripHtml($latestReportEntity, $reportResultEntity->id);

            try {
                $reportResultView = new View(null, null);
                $reportResultViewHtml = $reportResultView->element(
                    'LabOrders.review_map_ajax',
                    [
                        'isTestUser' => $isTestUser,
                        'labOrderReportEntity' => $latestReportEntity,
                        'mapProblemCategoriesList' => $mapProblemCategoriesList,
                        'showProblemByDefault' => true,
                        'alternativeResults' => $alternativeResults,
                        'reportResultEntity' => $reportResultEntity,
                    ]
                );
            } catch (\Exception $e) {
                $encounteredError = true;
            }

            return !$encounteredError;
        });


        if ($result === false) {
            $response['message'] = 'Unable to save lab order medication';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $scores = $reportResultsTable->getScores($latestReportEntity->id);

        if (!is_null($scores) && isset($scores['currentRiskScore']) && isset($scores['futureRiskScore'])) {
            $response['current_risk_score'] = $scores['currentRiskScore'];
            $response['future_risk_score'] = $scores['futureRiskScore'];
        }

        $response['success'] = true;

        $response['report_result_id'] = $reportResultId;
        $response['report_result_html'] = $reportResultViewHtml;

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }

    /**
     * Get information (such as severity) from a medication
     *
     */
    public function getMedicationInfo(int $labOrderId = 0, int $drugId = 0)
    {
        $reportResultsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResults');

        // disable debugging
        #Configure::write('debug', 0);
        #\header('Content-type: application/json');
        #$this->viewBuilder()->setLayout('ajax');

        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->first();

        if (is_null($latestReportEntity)) {
            $response['message'] = 'Could not find report';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $drugEntity = TableRegistry::getTableLocator()->get('SystemManagement.Drugs')
            ->find()
            ->where([
                'Drugs.id' => $drugId
            ])
            ->contain([
                'TsiMedication'
            ])
            ->first();

        $response = [
            'success' => false,
            'message' => ''
        ];

        if (is_null($drugEntity)) {
            $response['message'] = 'Invalid drug entity';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->contain([
                'Reports'
            ])
            ->matching('Reports')
            ->first();

        if (is_null($labOrderEntity)) {
            $response['message'] = 'Invalid lab order entity';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        // Detect if the drug or medication already exists in the lab order (and prevent if so)
        $labOrderMedicationEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->matching('Medications', function($q) use ($drugEntity) {
                return $q->andWhere([
                    'drug_id' => $drugEntity->id // Do not allow the same exact drug to be added
                ]);
            })
            ->first();

        if (!is_null($labOrderMedicationEntity)) {
            $response['message'] = 'Drug already exists in lab order';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        // Detect if the drug or medication already exists in the lab order (and prevent if so)
        $labOrderMedicationEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->matching('Medications', function($q) use ($drugEntity) {
                return $q->andWhere([
                    'medication_id' => $drugEntity->medication_id, // Do not allow the same medication class to be added
                ]);
            })
            ->first();

        if (!is_null($labOrderMedicationEntity)) {
            $response['message'] = 'A result for this medication class already exists';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        // If this medication was returned in the results, bind it
        $medicationExistsQuery = $reportResultsTable->find()
            ->where([
                'lab_order_report_id' => $latestReportEntity->id,
                'is_relevant' => 0,
            ])
            ->contain(['ResultMedications'])
            ->matching('ResultMedications', function($q) use ($drugEntity) {
                return $q->andWhere([
                    'ResultMedications.lab_order_medication_id IS' => null,
                    'ResultMedications.medication_id' => $drugEntity->medication_id
                ]);
            })
            ->matching('ResultMapNotes', function($q) {
                return $q->andWhere([
                    'ResultMapNotes.patient_notes IS' => null,
                    'ResultMapNotes.prescriber_notes IS' => null,
                    'ResultMapNotes.map_problem_rationale_id IS' => null,
                    'ResultMapNotes.alternate_drug_id IS' => null,
                    'ResultMapNotes.lab_order_medication_id IS' => null
                ]);
            });

        $response['severity'] = 'no pgx data';
        if (
            isset($drugEntity->tsi_medication) &&
            !empty($drugEntity->tsi_medication)
        ) {
            $response['severity'] = 'no pgx data';
            if ($medicationExistsQuery->count() > 0) {
                $medicationExistsEntity = $medicationExistsQuery->toArray()[0];

                if (isset($medicationExistsEntity->tsi_severity)) {
                    $response['severity'] = $medicationExistsEntity->tsi_severity;
                }
            }
        }

        /*
        $scores = $reportResultsTable->getScores($latestReportEntity->id);

        if (!is_null($scores) && isset($scores['currentRiskScore']) && isset($scores['futureRiskScore'])) {
            $response['current_risk_score'] = $scores['currentRiskScore'];
            $response['future_risk_score'] = $scores['futureRiskScore'];
        }
        */

        $response['success'] = true;

        return $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }

    public function saveMapResultItem(int $labOrderId = 0, int $resultItemId = 0)
    {
        $response = [];

        $labOrderEntity = TableRegistry::getTableLocator()->get('LabOrders.LabOrders')
            ->find()
            ->where([
                'LabOrders.id' => $labOrderId
            ])
            ->contain([
                'Reports'
            ])
            ->matching('Reports')
            ->first();

        if (is_null($labOrderEntity)) {
            $response['message'] = 'Invalid lab order entity';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        $reportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $latestReportEntity = $reportsTable->find('latestReportFull', [
            'lab_order_id' => $labOrderId,
        ])->first();

        if (
            is_null($latestReportEntity)
        ) {
            $response['message'] = 'Could not find report';

            return
                $this->response
                ->withType('application/json')
                ->withStringBody(json_encode($response));
        }

        if (isset($latestReportEntity->results[$resultItemId])) {
            $data = $this->getRequest()->getData();
            $mapNotesTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReportResultMapNotes');
            $mapNoteEntity = $mapNotesTable
                ->find()
                ->where([
                    'lab_order_report_result_id' => $resultItemId
                ])
                ->first();

            if (!is_null($mapNoteEntity)) {
                $mapNoteEntity = $mapNotesTable->patchEntity(
                    $mapNoteEntity,
                    $data['results'][$resultItemId]['result_map_note'],
                    [
                        'fieldList' => ['patient_notes', 'prescriber_notes', 'detail', 'map_problem_rationale_id', 'alternate_drug_id', 'alternate_result_id'],
                        'validation' => 'default'
                    ]
                );


                try {
                    $mapNotesTable->save($mapNoteEntity);
                    $response['success'] = true;
                } catch (\Exception $e) {
                }
            }
        }

        return
            $this->response
            ->withType('application/json')
            ->withStringBody(json_encode($response));
    }
}
