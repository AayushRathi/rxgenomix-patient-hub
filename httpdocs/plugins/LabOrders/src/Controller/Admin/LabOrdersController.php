<?php
namespace LabOrders\Controller\Admin;

use Cake\Http\Client;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use LabOrders\Controller\AppController;
use LabOrders\Libs\LabOrders;
use LabOrders\Model\Table\LabOrdersTable;

/**
 * Class LabOrdersController
 *
 * @property LabOrdersTable $LabOrders
 * @package LabOrders\Controller\Admin
 */
class LabOrdersController extends AppController
{
    /**
     * init
     */
    public function initialize()
    {
        $this->loadModel('LabOrders.LabOrders');
        $this->loadModel('LabOrders.LabOrderStatuses');
        $this->loadModel('LabOrders.LabOrderTypes');
        parent::initialize();
    }

    /**
     * index
     */
    public function index()
    {
        // Hack
        if ($this->request->getQuery('sort') == 'id') {
            $queryParams = $this->request->getQueryParams();
            $queryParams['sort'] = 'LabOrders.id';
            $this->request = $this->request->withQueryParams($queryParams);
        }
        $labOrders = $this->LabOrders->find()->contain([
            'WorkflowStatus',
            'Patient',
            'Providers',
            'Provider.Users',
            'Provider.HealthCareClient.Lab',
            'Medications.Drug.BrandNames'
        ])->where(['is_deleted' => 0])->order(['LabOrders.id' => 'desc']);

        $statuses = $this->LabOrderStatuses->find('dropdown');

        $labOrderTypesList = $this->LabOrderTypes->find()->where([
            'is_active' => 1
        ])->order(['display_order'])->combine('id', 'title');

        $this->set(compact('labOrders', 'statuses', 'labOrderTypesList'));
        $this->set('_serialize', ['labOrders']);
    }

    /**
     * @param int $labOrderId
     * @return Response|null
     */
    public function delete(int $labOrderId)
    {
        $labOrder = $this->LabOrders->get($labOrderId);
        $this->LabOrders->patchEntity($labOrder, ['is_deleted' => 1]);
        if ($this->LabOrders->save($labOrder)) {
            $this->Flash->success(__('Order #{0} has been deleted.', [$labOrderId]));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function view(int $labOrderId)
    {
        $isImmutable = (bool)$this->currentLib24watchUser['is_immutable'];

        $icd10Codes = TableRegistry::getTableLocator()->get('SystemManagement.Icd10Codes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->order(['display_order' => 'asc'])
            ->toArray();

        $specimenTypesList = TableRegistry::getTableLocator()->get('SystemManagement.SpecimenTypes')
            ->find()
            ->where([
                'is_active' => 1
            ])
            ->combine('id', 'title')
            ->toArray();

        $order = $this->LabOrders->get($labOrderId, [
            'contain' => [
                'WorkflowStatus',
                'Patient',
                'Patient.PatientIcd10Codes' => function($q) use ($labOrderId) {
                    return $q->andWhere([
                        'PatientIcd10Codes.lab_order_id' => $labOrderId
                    ]);
                }, // TODO: update this to be conditional
                'Providers',
                'Provider.Users',
                'Provider.HealthCareClient.Lab',
                'Medications.Drug.BrandNames',
                'Events'
            ]
        ]);

        $this->loadModel('LabOrders.LabOrderStatuses');
        $this->set('statuses', $this->LabOrderStatuses->find('dropdown'));

        $this->loadModel('SystemManagement.Ethnicities');
        $this->set('ethnicities', $this->Ethnicities->find('dropdown'));

        $this->loadModel('SystemManagement.Sexes');
        $this->set('sexes', $this->Sexes->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->toArray());

        $this->set(compact('specimenTypesList', 'icd10Codes', 'order', 'isImmutable'));
    }


    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function download(int $labOrderId)
    {
        if ($labOrderId) {
            $labOrderEntity = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient',
                    'Patient.Sex',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.AssignedFeatures',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames',
                    'Reports'
                ]
            ]);

            /*
            if (!$labOrderEntity || $labOrderEntity->lab_order_status_id < 7 || empty($labOrderEntity->reports)) {
                $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => 'admin',
                ], 'Lab Order not ready for download.', 'warning');
            } else {
             */
                $labOrdersInstance = new LabOrders;

                try {
                    $labOrdersInstance->renderPdf($labOrderId, 0);
                } catch (\Exception $e) {

                    $this->redirectWithDefault([
                        'plugin' => 'LabOrders',
                        'controller' => 'LabOrders',
                        'action' => 'index',
                        'prefix' => false,
                    ], 'Lab Order not ready for download.', 'warning');
                }
            //}
        } else {
            throw new \Exception('Lab Order ID is required');
        }
    }

    /**
     * @param int $labOrderId
     * @throws \Exception
     */
    public function downloadMap(int $labOrderId, string $mapType)
    {
        if ($labOrderId) {
            $labOrderEntity = $this->LabOrders->get($labOrderId, [
                'contain' => [
                    'WorkflowStatus',
                    'Patient',
                    'Patient.Sex',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.AssignedFeatures',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames',
                    'Reports'
                ]
            ]);

            if (!$labOrderEntity || $labOrderEntity->lab_order_status_id < 7 || empty($labOrderEntity->reports)) {
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => 'admin',
                ], 'Lab Order not ready for download.', 'warning');
            }

            $labOrdersInstance = new LabOrders;

            if ($labOrdersInstance::isValidMapType($mapType) === false) {
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => 'admin',
                ], 'Invalid map type specified', 'warning');
            }

            $download = $labOrdersInstance->downloadMap($labOrderEntity->id, $mapType);

            if ($download === false) {
                return $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'action' => 'index',
                    'prefix' => 'admin',
                ], 'MAP not ready for download.', 'warning');
            }

        } else {
            throw new \Exception('Lab Order ID is required');
        }
    }

    public function web1(string $sampleId = null)
    {
        if (!$sampleId) {
            throw new \Exception('Sample ID required');
        }

        $labOrder = $this->LabOrders->find()
            ->contain(['Patient'])
            ->where(['sample_id' => $sampleId])
            ->first();

        if (!$labOrder) {
            throw new \Exception('Sample ID not found');
        }

        $scope['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
        $authClient = new Client($scope);

        $response = $authClient->post('https://www.benecardpbf.com:8595/oauth2/endpoint/OAuthProvider/token',
            http_build_query([
                'grant_type' => 'client_credentials',
                'client_id' => 'rxGenomix',
                'client_secret' => 'secret'
            ]));
        $result = $response->getJson();
        $token = $result['access_token'];
        $this->format = 'getJson';
        $this->type = 'json';
        $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);

        $scope['headers']['Content-Type'] = 'application/json';
        $client = new Client($scope);
        $data = [
            'clientId' => $labOrder->patient->client_id,
            'cardId' => $labOrder->patient->card_id,
            'personCode' => $labOrder->patient->person_code,
            'dob' => lib24watchDate('%Y-%m-%d', $labOrder->patient->date_of_birth, +5),
            'testDate' => lib24watchDate('%Y-%m-%d', $labOrder->collection_date, +5),
            'reportUrl' => 'https://' . $this->request->host() . '/lab_orders/lab_orders/download/' . $labOrder->id,
        ];

        $response = $client->post('https://www.benecardpbf.com:8595/RxGenomixEndpoint/rest/member', json_encode($data));
        if (!$response->isOk()) {
            \SystemManagement\Libs\EventLogs::write(
                $this->jwtPayload->health_care_client_id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Posting to Benecard Webservice #1 Failed',
                'Failure',
                (array)$response->getJson()
            );
        } else {
            \SystemManagement\Libs\EventLogs::write(
                $this->jwtPayload->health_care_client_id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Posted to Benecard Webservice #1',
                'Success'
            );
        }
        dd((array)$response->getJson());
    }

}
