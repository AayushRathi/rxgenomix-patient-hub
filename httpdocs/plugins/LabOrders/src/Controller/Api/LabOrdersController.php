<?php
namespace LabOrders\Controller\Api;

use Cake\Core\Configure;
use Cake\Mailer\MailerAwareTrait;
use SystemManagement\Controller\Api\ApiController;
use PharmacogenomicProviders\Lib\PharmacogenomicProvider;
use Cake\Http\Client;
use SystemManagement\Libs\ApiLogs;

/**
 * Class LabOrdersController
 * @package LabOrders\Controller\Api
 */
class LabOrdersController extends ApiController
{
    use MailerAwareTrait;

    /**
     * @var array
     */
    protected $severityFilteringMap = [
        'OK' => [
            'OK',
            'Question',
            'Info',
            'Warning',
            'Not Found',
            'Danger'
        ],
        'Info' => [
            'Question',
            'Info',
            'Warning',
            'Not Found',
            'Danger'
        ],
        'Warning' => [
            'Question',
            'Warning',
            'Not Found',
            'Danger'
        ],
        'Danger' => [
            'Question',
            'Not Found',
            'Danger'
        ],
        'Question' => [
            'Question'
        ],
        'Not Found' => [
            'Not Found'
        ]
    ];

    /**
     *
     */
    public function initialize()
    {
        $this->loadModel('LabOrders.LabOrders');

        parent::initialize();
    }

    /**
     * index
     */
    public function index()
    {
        if ($this->checkPermission('LabOrders', 'list')) {
            $labOrders = $this->LabOrders->find()
                ->contain(['Patient', 'Provider', 'Medications'])
                ->where([
                'health_care_client_id' => $this->jwtPayload->health_care_client_id
            ]);

            $statusId = $this->getRequest()->getParam('status_id');
            if ($statusId) {
                $labOrders->where(['lab_order_status_id' => $statusId]);
            }
            $this->apiResponse['response']['successMessage'] = 'OK';
            $this->apiResponse['response']['results'] = $labOrders->toArray();
            ApiLogs::write($this->jwtPayload->api_authentication_id, $this->getRequest()->clientIp(), 'lab_orders', 'list');
        }
    }

    /**
     * view
     */
    public function view()
    {
        if ($this->checkPermission('LabOrders', 'lookup')) {
            $labOrderId = $this->getRequest()->getParam('id');

            if (!$labOrderId) {
                // return 404
                $this->return404('Resource Not Found');
            } else {
                $labOrder = $this->LabOrders->find()
                    ->contain(['Provider.HealthCareClient'])
                    ->where(['id' => $labOrderId]);
                if (!$labOrder) {
                    $this->return404('Resource Not Found');
                }
                // check to make sure the requested ID matched the authentication's health care client id
                if ($this->jwtPayload->health_care_client_id != $labOrder->provider->health_care_client_id) {
                    $this->apiResponse['response']['results'] = [];
                    ApiLogs::write($this->jwtPayload->api_authentication_id, $this->getRequest()->clientIp(),
                        'lab_orders', 'Failed - trying to access lab order outside of their scope');
                } else {
                    $this->apiResponse['response']['results'] = $labOrder->toArray();
                }
            }
        }
    }

    /**
     * results
     */
    public function results()
    {
        $labOrderId = $this->getRequest()->getParam('id');

        if (!$labOrderId) {
            // return 404
            $this->return404('Resource Not Found');
        } else {
            // get local results

        }
    }

    /**
     * member reports, as per request from Benecard they want to query for reports based on 4 values
     */
    public function memberReports()
    {
        if ($this->checkPermission('LabOrders', 'member_reports')) {
            $clientId = $this->getRequest()->getQuery('client_id');
            $cardId = $this->getRequest()->getQuery('card_id');
            $personCode = $this->getRequest()->getQuery('person_code');
            $dateOfBirth = $this->getRequest()->getQuery('dob');

            if (!$clientId || !$cardId || !$personCode || !$dateOfBirth) {
                $this->httpStatusCode = 400;
                $this->apiResponse['success'] = false;
                $this->apiResponse['successMessage'] = 'client_id, card_id, person_code, and dob are required for this resource';
                $this->apiResponse['results'] = [];
            } else {
                $memberId = implode('-', [
                    $clientId,
                    $cardId,
                    $personCode,
                    $dateOfBirth
                ]);
                $this->loadModel('PharmacogenomicProviders.ConsolidatedGeneticReports');

                $result = $this->ConsolidatedGeneticReports->find()
                    ->contain(['Drugs'])
                    ->where(['member_id' => $memberId])
                    ->order(['ConsolidatedGeneticReports.created' => 'DESC']);

                if ($result->count() == 0) {
                    $this->httpStatusCode = 404;
                    $this->apiResponse['success'] = false;
                    $this->apiResponse['successMessage'] = 'Member not found: ' . $memberId;
                    $this->apiResponse['results'] = [];
                } else {
                    $drugIds = explode(',', $this->getRequest()->getQuery('drugs'));
                    $drugIds = array_filter($drugIds);

                    $severity = $this->getRequest()->getQuery('severity');
                    $mapResults = false;
                    if ($severity) {
                        if (isset($this->severityFilteringMap[$severity])) {
                            $mapResults = $this->severityFilteringMap[$severity];
                        }
                        if ($mapResults) {
                            $result->matching('Drugs', function ($q) use ($mapResults) {
                                return $q->where(['Drugs.severity IN' => $mapResults]);
                            });
                        }
                    }
                    $result = $result->first();
                    // loop through to add drugs not found
                    $results = [];
                    if (!empty($drugIds) && $result && count($drugIds) != count($result->drugs)) {
                        foreach ($drugIds as $drugId) {
                            $found = false;
                            foreach ($result->drugs as $drug) {
                                if (
                                    sprintf('%09d', $drug['drug_id']) === $drugId &&
                                    (!$mapResults || ($mapResults && in_array($drug['severity'], $mapResults)))
                                ) {
                                    $found = true;
                                    $results[$drugId] = [
                                        'ndc' => $drugId,
                                        'severity' => $drug['severity'],
                                        'message' => $drug['message'],
                                        'activity' => $drug['activity']
                                    ];
                                }
                            }
                            if (!$found) {
                                $results[$drugId] = [
                                    'ndc' => $drugId,
                                    'severity' => 'Not Found',
                                    'message' => 'Not Found',
                                    'activity' => 'Not Found',
                                ];
                            }
                        }
                    } elseif (isset($result->drugs)) {
                        $results = $result->drugs;

                        $results = array_map( function($result) use ($mapResults) {
                            if (isset($result['drug_id']) && $result['drug_id'] != 0) {
                                if (!$mapResults || ($mapResults && in_array($result['severity'], $mapResults))) {
                                    return [
                                        'ndc' => sprintf('%09d', $result['drug_id']),
                                        'severity' => $result['severity'],
                                        'message' => $result['message'],
                                        'activity' => $result['activity']
                                    ];
                                }
                            }
                        },
                        $results);

                        $results = array_filter($results);
                    }

                    $this->httpStatusCode = 200;
                    $this->apiResponse['success'] = true;
                    $this->apiResponse['successMessage'] = 'Success';
                    $this->apiResponse['results'] = array_values($results);
                }
            }
        }
    }

    /**
     * Endpoint for Quality control status changes
     */
    public function qc()
    {
        if ($this->checkPermission('LabOrders', 'qc')) {
            $validQcStatusIds = [8, 7];
            $data = $this->getRequest()->getData();
            $sampleId = $data['id'];
            $qcStatus = $data['status_id'];

            if (!$sampleId || !$qcStatus) {
                $missingFields = [];
                if (!$sampleId) {
                    $missingFields[] = 'id';
                }
                if (!$qcStatus) {
                    $missingFields[] = 'status_id';
                }

                $this->httpStatusCode = 400;
                $this->apiResponse['success'] = false;
                $this->apiResponse['successMessage'] = 'Malformed request; Missing fields: ' .
                    implode(', ', $missingFields);
            } else {
                if (!in_array($qcStatus, $validQcStatusIds)) {
                    $this->httpStatusCode = 403;
                    $this->apiResponse['success'] = false;
                    $this->apiResponse['successMessage'] = 'Not a valid QC Status ID, must be 7 (report ready) or 8 (retesting required)';
                } else {
                    $labOrder = $this->LabOrders->find()
                        ->contain(['Patient', 'Provider'])
                        ->where(['sample_id' => $sampleId])
                        ->first();
                    if (!$labOrder) {
                        $this->httpStatusCode = 404;
                        $this->apiResponse['success'] = false;
                        $this->apiResponse['successMessage'] = 'Could not find lab order with sample ID: ' . $sampleId;
                    } else {
                        if ($labOrder->lab_order_status_id != 5) {
                            $this->httpStatusCode = 403;
                            $this->apiResponse['success'] = false;
                            $this->apiResponse['successMessage'] = 'Status not in QC review, can not be changed via API.';
                        } else {
                            $labOrder->lab_order_status_id = $qcStatus;
                            if ($this->LabOrders->save($labOrder)) {

                                // if status is changed to retesting required trigger the job to send it back to the lab
                                if ($qcStatus == 8) {
                                    //TODO: https://3.basecamp.com/3874016/buckets/10223826/messages/1826800930

                                    \SystemManagement\Libs\EventLogs::write(
                                        $labOrder->provider->health_care_client_id,
                                        $labOrder->id,
                                        $labOrder->lab_order_status_id,
                                        'Needs Retesting',
                                        'Success'
                                    );
                                }

                                if ($qcStatus == 7) {
                                    // If status changed to report ready and HCC is Benecard, post to their API the report is ready

                                    \SystemManagement\Libs\EventLogs::write(
                                        $labOrder->provider->health_care_client_id,
                                        $labOrder->id,
                                        $labOrder->lab_order_status_id,
                                        'QC Passed',
                                        'Success'
                                    );

                                    $email = $labOrder->provider->email;
                                    if ($labOrder->provider->health_care_client_id == 1) {
                                        $email = 'clinical.services@benecard.com';
                                    }
                                    // Send report ready email
                                    $mailer = $this->getMailer('Providers.Providers');
                                    $mailer->send('reportReady', [
                                        $email,
                                        $labOrder
                                    ]);

                                    // this is a special case for benecard, all other HCCs should ping the API to get the status of their lab orders
                                    if ($labOrder->provider->health_care_client_id == 1) {
                                        $scope['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
                                        $authClient = new Client($scope);

                                        $response = $authClient->post(Configure::read('Benecard.api.endpoint') . '/oauth2/endpoint/OAuthProvider/token',
                                            http_build_query([
                                                'grant_type' => 'client_credentials',
                                                'client_id' => Configure::read('Benecard.api.username'),
                                                'client_secret' => Configure::read('Benecard.api.password')
                                            ]));
                                        $result = $response->getJson();
                                        $token = $result['access_token'];
                                        $this->format = 'getJson';
                                        $this->type = 'json';
                                        $scope['headers'] = array_merge($scope['headers'], ['Authorization' => 'Bearer ' . $token]);

                                        $scope['headers']['Content-Type'] = 'application/json';
                                        $client = new Client($scope);
                                        $data = [
                                            'clientId' => $labOrder->patient->client_id,
                                            'cardId' => $labOrder->patient->card_id,
                                            'personCode' => sprintf('%02d', $labOrder->patient->person_code),
                                            'dob' => lib24watchDate('%Y-%m-%d', $labOrder->patient->date_of_birth, +5),
                                            'testDate' => lib24watchDate('%Y-%m-%d', $labOrder->collection_date, +5),
                                            'reportUrl' => 'https://' . $this->request->host() . '/lab_orders/lab_orders/download/' . $labOrder->id,
                                        ];

                                        $response = $client->post(Configure::read('Benecard.api.endpoint') . '/RxGenomixEndpoint/rest/member', json_encode($data));
                                        if (!$response->isOk()) {
                                            \SystemManagement\Libs\EventLogs::write(
                                                $labOrder->provider->health_care_client_id,
                                                $labOrder->id,
                                                $labOrder->lab_order_status_id,
                                                'Posting to Benecard Webservice #1 Failed',
                                                'Failure',
                                                (array)$response->getJson()
                                            );
                                        } else {
                                            \SystemManagement\Libs\EventLogs::write(
                                                $labOrder->provider->health_care_client_id,
                                                $labOrder->id,
                                                $labOrder->lab_order_status_id,
                                                'Posted to Benecard Webservice #1',
                                                'Success'
                                            );
                                        }
                                    }
                                }
                                $this->httpStatusCode = 200;
                                $this->apiResponse['success'] = true;
                                $this->apiResponse['successMessage'] = 'Lab Order status updated';
                            }
                        }
                    }
                }
            }
        }
    }
}
