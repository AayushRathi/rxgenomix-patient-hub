$(document).ready(function () {

    $('#patient-phone').mask('(000) 000-0000');

    function showHideEthnicityOther() {
        var group = $('#patient-ethnicity-other').closest('div.input');
        if ($('#patient-ethnicity-id').find(':selected').val() == 8) {
            group.show();
            if (!group.hasClass('required')) {
                group.addClass('required');
            }
            $('#patient-ethnicity-other').attr('required', true);
        } else {
            group.hide();
            $('#patient-ethnicity-other').attr('required', false);
            group.removeClass('required');
        }
    }

    $('#patient-ethnicity-id').on('change', function () {
        showHideEthnicityOther();
    });
    showHideEthnicityOther();

    var patientData = [];

    $('#patient-date-of-birth').on('dp.hide', function() {
        // Ajax to get patients by dob and insert into UL in .patient-list div
        let data = {};
        data.dob = $(this).val();

        $.post(
            '/patients/get_patient_by_date_of_birth',
            data,
            function (rtn) {
                let ul = $('.patient-list').find('ul');
                ul.find('li').remove();
                $('.patient-list').find('.error-message').remove();
                if (rtn.length > 0) {
                    $.each(rtn, function (index) {
                        let patient = rtn[index];
                        patientData[patient.id] = patient;
                        ul.append('<li><a href="javascript:void(0);" class="set-patient" data-patient="'+patient.id+'">'+
                            patient.first_name +' '+ patient.last_name +'</a></li>');
                    });
                } else {
                    $('.patient-list').prepend('<div class="error-message">No Patient Found with that Date of Birth.</div>');
                }
                ul.append('<li><a href="javascript:void(0);" class="btn btn-primary new-patient">Add New Patient?</a></li>');
            }
        );
    });

    $(document).on('click', '.set-patient', function () {
        let patientId = $(this).attr('data-patient');
        let patient = patientData[patientId];
        $('#patient-id').val(patientId);
        $.each(patient, function (k,v) {
            // skip DOB
            if (k != 'date_of_birth') {
                let field = $('input[name="patient['+k+']"]');
                if (field.length > 0) {
                    field.val(v);
                } else {
                    // This should just be .val(v) globally but I'll leave it to accidentally not 
                    // produce any bugs
                    //
                    // NOTE: 2020-09-10: I think if the client id is a number it doesnt work so adding it
                    if (k === 'lib24watch_state_abbr') {
                        $('select[name="patient['+k+']"]').val(v);
                    } else {
                        $('select[name="patient['+k+']"] option:eq('+v+')').prop('selected', true);
                    }
                }
            }
        });
        $('.patient-form').show();
    });

    $(document).on('click', '.new-patient', function () {
        $('input[name*="patient"]').not('input#patient-date-of-birth_hidden, input#patient-date-of-birth, input#patient-client-id').val('');
        $('select[name*="patient"] option:eq(0)').prop('selected', true);
        //$('.patient-form').find('input, select').attr('required', false);
        $('.patient-form').show();
    });

    if ($('#patient-first-name').val() == '') {
        //$('.patient-form').find('input, select').attr('required', false);
        $('.patient-form').hide();
    }

});
