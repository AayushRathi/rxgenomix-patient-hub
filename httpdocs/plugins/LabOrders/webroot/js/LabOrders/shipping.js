$(document).ready(function () {

    showHideShippingFields = function () {
        let shippingFields = $('.shipping-fields');
        let kitFields = $('.kit-fields');
        if (
            $('#ship-to-provider').is(':checked') ||
            $('#ship-to-patient').is(':checked'))
        {
            shippingFields.show();
            kitFields.find('input').val('');
            kitFields.hide();
            shippingFields.find('.input.required').find('input, select').prop('required', 'required');
            kitFields.find('.input.required').find('input, select').prop('required', null);
        } else if ($('#kit-administered-on-site').is(':checked')) {
            shippingFields.find('input').val('');
            shippingFields.find('option:selected').prop('selected', false);
            shippingFields.hide();
            kitFields.show();
            shippingFields.find('.input.required').find('input, select').prop('required', null);
            kitFields.find('.input.required').find('input, select').prop('required', 'required');
        } else {
            shippingFields.hide();
            kitFields.hide();
            shippingFields.find('.input.required').find('input, select').prop('required', null);
            kitFields.find('.input.required').find('input, select').prop('required', null);
        }
    };

    enableDisableCheckboxes = function (checkedbox) {
        if (checkedbox.is(':checked')) {
            $(':checkbox').not(checkedbox).each(function () {
                $(this).attr('disabled', true);
            });
        } else {
            $(':checkbox').each(function () {
                $(this).attr('disabled', false);
            });
        }
    };

    $('#ship-to-provider, #ship-to-patient, #kit-administered-on-site').on('change', function () {
        showHideShippingFields();
    });

    $(':checkbox').on('change', function () {
        enableDisableCheckboxes($(this));
    });

    showHideShippingFields();

    // on load
    $(':checkbox').each(function() {
        if ($(this).is(':checked')) {
            enableDisableCheckboxes($(this));
        }
    });

});
