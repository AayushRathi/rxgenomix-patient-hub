$(document).ready(function () {
    enableDisableCheckboxes = function (checkedbox) {
        let row = checkedbox.closest('.multiinput-list-item');
        let checkboxClass = checkedbox.attr('class');
        if (checkedbox.is(':checked')) {
            row.siblings().each(function () {
                console.log($(this).attr('data-idx'));
                $(this).find('.' + checkboxClass).attr('checked', false).attr('disabled', true);
            });
        } else {
            $('.' + checkboxClass).each(function () {
                $(this).attr('disabled', false);
            });
        }
    };

    // change event
    $('.multiinput-list').on('change', ':checkbox', function () {
        enableDisableCheckboxes($(this));
    });

    // on load
    $(':checkbox').each(function () {
        enableDisableCheckboxes($(this));
    });

    // uncheck new row checkboxes
    $(document).on('rowAdded', function () {
        $('.multiinput-list-item').last().find(':checkbox').each(function () {
            $(this).attr('checked', false);
            if ($('.' + $(this).attr('class') + ':checked').length > 0) {
                $(this).attr('disabled', true);
            }
        });
    });

    // enable checkboxes if row with them checked was removed
    $(document).on('rowRemoved', function () {
        $(':checkbox').each(function () {
            enableDisableCheckboxes($(this));
        });
    });

});
