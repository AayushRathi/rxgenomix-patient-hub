$(function() {

    const useCkEditor = false;

    const saveEachRowWithAjax = true;

    if (useCkEditor) {
        $('textarea.editor-review-map').ckeditor({
            customConfig: '/lab_orders/js/LabOrders/review_map_config.js'
        });
    }

    if (saveEachRowWithAjax) {
        $('#map-container').on('blur', '.editor', function(e) {
            if (this.value.length) {
                let $resultItem = $(this).closest('.map-item');

                if ($resultItem.length) {
                    let $resultItemId = $resultItem.find('.result-inputs').eq(0);

                    if ($resultItemId.length) {
                        let serialized = $resultItem.find(':input').serialize();

                        if (serialized) {
                            let isSaving = $resultItem.data('saving');

                            if (isSaving !== true) {
                                $resultItem.data('saving', true);
                                $resultItem.find('.saving-icon').remove();
                                let $savingEl = $('<i class="saving-icon fa fa-circle-o-notch fa-spin fa-fw"></i>');
                                $(this).prevAll('label').append($savingEl);
                                $.ajax({
                                    url: saveMapResultItemBaseUrl + '/' + labOrderId + '/' + $resultItemId.val(),
                                    type: 'POST',
                                    data: serialized,
                                    success: function(response) {
                                        if (response && response.success) {
                                            $savingEl.removeClass('fa-circle-o-notch').removeClass('fa-spin').addClass('fa-check');
                                            $savingEl.fadeOut(5000);
                                            $resultItem.data('saving', null);
                                        }
                                    },
                                    error: function() {
                                        $savingEl.remove();
                                        $resultItem.data('saving', null);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        });
    }

    $('#button-add-medication-submit').on('click', function(e) {
        e.preventDefault()
        e.stopPropagation();

        let $medicationEl = $('#medication-id');
        let $medicationSearch = $('#add_additional_medication_search');

        if (!$medicationEl.length) {
            return;
        }

        $.ajax({
            url: addMedicationToLabOrderBaseUrl + '/' + labOrderId + '/' + $medicationEl.val(),
            success:function(response) {
                if (
                    response.success && 
                    response.report_result_id &&
                    response.report_result_html
                ) {
                    let $reportResultHtml = $(response.report_result_html).find('.row.map-item');

                    if ($reportResultHtml.length) {

                        var $firstEl = null;
                        if ($('.row.map-item').length === 0) {
                            $firstEl = $('#add-medication').nextAll('.textarea').eq(0);
                        } else {
                            $firstEl = $('.row.map-item:first');
                        }

                        $firstEl.before($reportResultHtml).fadeIn('slow', function() {
                            $firstEl.removeClass('map-item-first');
                            $('.row.map-item').eq(0).addClass('map-item-first');

                            $medicationEl.val('');
                            $medicationSearch.val('');

                            let $drugName = $(response.report_result_html).find('p.drug-name');
                            $('#add-medication').find('.added-message').remove();
                            $('#add-medication').append(
                                $('<div class="added-message alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + $drugName.text() + ' successfully added to MAP.</div>')
                            );

                            setTimeout(function() {
                                $('.row.map-item:first .selected-drug-container select').select2();
                            }, 500);

                            $('#add-medication-severity-level').html('');
                            $('#button-add-medication-submit').prop('disabled', true);
                            $('#button-add-medication-submit').css('opacity', '0.3');
                        });
                    }

                    if (response.current_risk_score) {
                        $('.current-risk-score span').text(response.current_risk_score);
                    }

                    if (response.future_risk_score) {
                        $('.future-risk-score span').text(response.future_risk_score);
                    }

                } else if (!response.success) {
                    alert(response.message);
                }
            }
        });
    });

    $('button[type="submit"]').on('click', function(e) {
        let valid = true;
        let $firstEl = [];

        if (useCkEditor) {
            for (var instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();

                let $el = $('#' + instance);

                if ($el.length) {
                    if ($el.hasClass('editor-required')) {
                        if ($el.val().length === 0) {
                            $firstEl = $el;
                            valid = false;
                            break;
                        } else {
                            let $inputContainer = $el.closest('div.input');
                            $inputContainer.removeClass('error');
                            $inputContainer.find('.error-message').remove();
                        }
                    }
                }
            }
        }

        if (!valid && $firstEl.length) {
            e.preventDefault();

            let $inputContainer = $firstEl.closest('div.input');
            $inputContainer.addClass('error');

            if ($inputContainer.find('.error-message').length) {
                $inputContainer.find('.error-message').remove();
            }

            $('<div class="error-message">Please fill out this field</div>').appendTo($inputContainer);

            $("html, body").delay(500).animate({
                scrollTop: $inputContainer.offset().top
            }, 500);

        }
    });

    const capitalize = (s) => {
        if (typeof s !== 'string') return ''
        return s.charAt(0).toUpperCase() + s.slice(1)
    }

    $('#map-container').on('click', '.included-checkbox', function(e) {
        if (this.checked) {
            $(this).closest('.row.map-item').find('.select-severity').prop('required', true).parent().addClass('required');
        } else {
            $(this).closest('.row.map-item').find('.select-severity').prop('required', false).parent().removeClass('required');
        }
    });

    // TODO: change to live
    $('#map-container').on('change', '.rationale-container select', function(e) {
        let resetReasonText = false;

        if (this.value === '') {
            resetReasonText = true;
        }

        let $container = $(this).closest('.map-item');

        let $prescriberNoteLabel = $container.find('.col-prescriber-notes label');
        let $patientNoteLabel = $container.find('.col-patient-notes label');

        if (useCkEditor) {
            updateEditorInstance(this, resetReasonText, $prescriberNoteLabel.attr('for'));
        } else {
            updateTextarea(this, resetReasonText, $prescriberNoteLabel.attr('for'));
        }

        // Per client meeting on 6.26 - only prescriber should be prefilled
        // updateEditorInstance(this, resetReasonText, $patientNoteLabel.attr('for'));
    });

    function updateTextarea(context, resetReasonText, instanceName) {
        let $textarea = $('#' + instanceName);
        let reasonText = '';
        let regex = /\n?(Reason for change:) ([^(?:\n\r|\n|\r)]+)/i;

        if (resetReasonText === false) {
            reasonText = $(context).find(':selected').text();
        }

        if ($textarea.length) {
            let match = $textarea.val().match(regex);
            let insertNewline = true;

            if ($textarea.val().length == 0 || (match && $textarea.val().indexOf(match[0]) === 0)) {
                insertNewline = false;
            }

            if (match && reasonText) {
                if (insertNewline) {
                    $textarea.val($textarea.val().replace(match[0], '\nReason for change: ' + reasonText));
                } else {
                    $textarea.val($textarea.val().replace(match[0], 'Reason for change: ' + reasonText));
                }
            } else if (match && !reasonText) {
                $textarea.val($textarea.val().replace(match[0], ''));
            } else {
                if (insertNewline) {
                    $textarea.val($textarea.val() + '\nReason for change: ' + reasonText);
                } else {
                    $textarea.val($textarea.val() + 'Reason for change: ' + reasonText);
                }
            }
        }
    }

    function updateEditorInstance(context, resetReasonText, instanceName) {
        let noteInstance = CKEDITOR.instances[instanceName];

        if (noteInstance) {
            let html = noteInstance.getData();
            let reasonTextElement = $(html).filter('.reason-text');
            let reasonText = '';

            if (resetReasonText === false) {
                reasonText = $(context).find(':selected').text();
            }

            let newHtml = null;
            let $newHtml = null;

            let reasonTextElementNew = $('<div class="reason-text">' + reasonText + '</div>');

            if (reasonTextElement.length === 0) {
                $newHtml = $('<div class="ckeditor-manipulate-text d-none">').html(html);
                $newHtml.append(reasonTextElementNew);
                $newHtml.appendTo('body');
            } else {
                $newHtml = $('<div class="ckeditor-manipulate-text d-none">').html(html);
                $newHtml.appendTo('body');
                $newHtml.find('.reason-text').replaceWith(reasonTextElementNew);
            }

            newHtml = $newHtml.html();

            noteInstance.setData(newHtml);

            $newHtml.remove();
        }
    }

    $('#map-container').on('change', '.problem-category-container select', function(e) {
        let $problemRationalesSelectWrapper = $(this).closest('div.select').nextAll('.rationale-container');
        let $problemRationalesSelect = $problemRationalesSelectWrapper.find('select');
        e.preventDefault();

        if (this.value === '') {
            // rehide if the dropdown is blank
            if (!$problemRationalesSelectWrapper.hasClass('d-none')) {
                $problemRationalesSelectWrapper.addClass('d-none');
            }

            $problemRationalesSelect.val('');
            $problemRationalesSelect.trigger('change');

            return;
        }

        let url = getMapProblemRationalsBaseUrl + '/' + this.value;

        $.ajax({
            url: url,
            dataType: 'json',
            success: function(json) {
                if (json.success && json.results && Object.keys(json.results).length > 0) {
                    $problemRationalesSelect.hide();
                    $problemRationalesSelect.find('option').remove();
                    $('<option>').text('Select a reason').val('').appendTo($problemRationalesSelect);
                    $.each(json.results, function(index, optionText) {
                        $('<option>').text(optionText).val(index).appendTo($problemRationalesSelect);
                        $problemRationalesSelectWrapper.removeClass('d-none');
                        $problemRationalesSelect.fadeIn('slow');
                    });

                }
            },
            error: function(json) {
                $problemRationalesSelectWrapper.removeClass('d-none');
            }
        });
    });

    $('#map-container').on('change', '.selected-drug-container select:not(.problem-category)', function(e) {
        let el = this;
        e.preventDefault();

        let split = this.value.split('_');

        let resultId = split[1];

        if (resultId != '0') {
            let $el = $(this).closest('.selected-drug-container').next(':hidden');
            
            if (!$el.length) {
                alert('Unable to find alternative element');

                return false;
            }
                
            $el.val(resultId);

            let $row = $(this).closest('div.map-item');
            let $severityEl = $row.find('.adjusted-severity span');
            let currentResultId = $row.find('.result-inputs').eq(0).val();
            let $scoreTr = $('.row-alternate-score-' + currentResultId);
            let $problemContainer = $row.find('.reason-container');
            let $problemDropdowns = $row.find('select.problem-dropdown');

            if (alternativeResults[resultId] && alternativeResults[resultId]['tsi_severity']) {
                if (!$severityEl.length) {
                    alert('Could not find severity element');

                    return false;
                }

                $severityEl.parent().removeClass('severity-low').removeClass('severity-high').removeClass('severity-moderate').addClass('severity-' + alternativeResults[resultId]['tsi_severity']);
                $severityEl.text( capitalize(alternativeResults[resultId]['tsi_severity']) );

                if ($scoreTr.length) {
                    if (alternativeResults[resultId]['tsi_severity'] === 'high') {
                        $scoreTr.html('2');
                    } else if (alternativeResults[resultId]['tsi_severity'] === 'moderate') {
                        $scoreTr.html('0.5');
                    } else {
                        $scoreTr.html('0');
                    }
                }

                $problemContainer.removeClass('d-none');
                $problemContainer.find('.problem-category-container').addClass('required');
                $problemDropdowns.prop('required', true);

            } else if (resultId !== undefined) {
                alert('Could not find the alternate result');
            } else if (resultId === undefined) { // Reset adjusted severity
                $severityEl.parent().removeClass('severity-low').removeClass('severity-high').removeClass('severity-moderate');
                $severityEl.text('');

                if ($scoreTr.length) {
                    if ($severityEl.hasClass('severity-high')) {
                        $scoreTr.html('2');
                    } else if ($severityEl.hasClass('severity-moderate')) {
                        $scoreTr.html('0.5');
                    } else {
                        $scoreTr.html('0');
                    }
                }

                if (window.showProblemByDefault === false) {
                    $problemContainer.addClass('d-none');
                }

                $problemContainer.find('.problem-category-container').removeClass('required');
                $problemDropdowns.removeAttr('required');
            }

            let resultIds = $('.result-inputs, .selected-drug-container select').serialize();
            let url ='/lab_orders/lab_orders/getRiskScore/' + labOrderReportId;

            $('.future-risk-score span').hide();

            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    resultIds: btoa(resultIds)
                },
                dataType: 'json',
                success: function(json) {
                    if (json.riskScore) {
                        $('.future-risk-score span').text(json.riskScore).fadeIn();
                        $('.alternate-score-sum .new-score').text(json.riskScore).show();
                        $('.alternate-score-sum .original').hide();
                    }
                },
                error: function(json) {
                    $('.future-risk-score span').append('<br><div class="alert alert-info alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Could not generate future risk</div>').fadeIn();
                    $('.alternate-score-sum .new-score').hide();
                    $('.alternate-score-sum .original').show();
                }
            });
        }
    });

    function addAdditionalMedicationToCallback(one, two) {
        let $medicationEl = $('#medication-id');
        let $medicationSearch = $('#add_additional_medication_search');

        if (!$medicationEl.length) {
            return;
        }

        $.ajax({
            url: getMedicationInfoBaseUrl + '/' + labOrderId + '/' + $medicationEl.val(),
            success:function(response) {
                if (
                    response.success && 
                    response.hasOwnProperty('severity') &&
                    response.severity != ''
                ) {
                    let severityClass = 'severity-' + response.severity.toLowerCase();
                    $('#add-medication-severity-level').html('Severity: ' + response.severity).addClass(severityClass);
                    $('#add-medication-severity-level span').show();
                    $('#button-add-medication-submit').prop('disabled', false);
                    $('#button-add-medication-submit').css('opacity', 1);

                } else if (!response.success || !response.hasOwnProperty('severity')) {
                    $('#add-medication-severity-level').html('');
                    $('#add-medication-severity-level')[0].className = '';
                    $('#add-medication').append(
                        $('<div class="warning-message alert alert-warning alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message + '</div>')
                    );

                    $('#button-add-medication-submit').prop('disabled', true);
                    $('#button-add-medication-submit').css('opacity', '0.3');
                }
            },
            error: function() {
                $('#add-medication-severity-level').html('');
            }
        });
    }

    function clearAddMedicationCallback() {
        /*
        $('#add-medication-severity-level').html('');
        $('#button-add-medication-submit').prop('disabled', true);
        $('#button-add-medication-submit').css('opacity', '0.3');
        */
    }

    $('#button-clear-medication').on('click', function(e) {
        e.preventDefault()
        e.stopPropagation();

        $('#add-medication-severity-level').html('');
        $('#button-add-medication-submit').prop('disabled', true);
        $('#button-add-medication-submit').css('opacity', '0.3');
        $('#add_additional_medication_search').val('');
        $('#medication-id').val('');
        $('#add-medication .warning-message').remove();
    });

    $('#add_additional_medication_search').on('blur', function(event, ui) {
        if (this.value === '') {
            $('#add-medication-severity-level').html('');
            $('#button-add-medication-submit').prop('disabled', true);
            $('#button-add-medication-submit').css('opacity', '0.3');
        }
    });

    window.addAdditionalMedicationCallback = addAdditionalMedicationToCallback;
    window.clearAddMedicationCallback = clearAddMedicationCallback;

    $(this).click(monitorActivity);
    //$(this).mousemove(monitorActivity);
    $(this).keypress(monitorActivity);

    let lastActive = null;

    const EXPIRATION_IN_MINUTES = 20;

    // keep the user on the page by sending a signal if the user interacts with the page
    function monitorActivity() {
        let triggerSignal = false;
        if (lastActive === null) {
            lastActive = new Date;
        } else {
            let diff = Math.abs(new Date() - lastActive);
            let minutes = Math.floor((diff/1000)/60);
            console.log(minutes);

            if (minutes > 5 && minutes < EXPIRATION_IN_MINUTES) {
                lastActive = new Date;
                triggerSignal = true;
            }
        }

        if (triggerSignal) {
            $.ajax({
                url: '/lab_orders/lab_orders/signal',
                dataType: 'json',
                success:function(json) {
                }
            });
        }
    }
});
