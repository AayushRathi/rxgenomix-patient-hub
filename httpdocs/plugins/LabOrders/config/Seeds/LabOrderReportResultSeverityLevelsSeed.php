<?php
use Migrations\AbstractSeed;

/**
 * LabOrderReportResultSeverityLevels seed.
 */
class LabOrderReportResultSeverityLevelsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'title' => 'High',
                'slug' => 'high',
                'display_order' => 1,
                'color_hex' => 'ffffff',
                'is_active' => 1
            ],
            [
                'id' => 2,
                'title' => 'Moderate',
                'slug' => 'moderate',
                'display_order' => 2,
                'color_hex' => 'ffffff',
                'is_active' => 1
            ],
            [
                'id' => 3,
                'title' => 'Low',
                'slug' => 'low',
                'display_order' => 3,
                'color_hex' => 'ffffff',
                'is_active' => 1
            ]
        ];

        $table = $this->table('lab_order_report_result_severity_levels');
        $table->insert($data)->save();
    }
}
