<?php
use Migrations\AbstractSeed;

/**
 * LabOrderInitiatorTypes seed.
 */
class LabOrderStatusesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*
            [
                'id' => '1',
                'title' => 'Created',
                'is_active' => '1',
                'display_order' => '0',,
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '2',
                'title' => 'Created - Awaiting Provider Account Approval',
                'is_active' => '1',
                'display_order' => '1',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '3',
                'title' => 'Submitted to Lab',
                'is_active' => '1',
                'display_order' => '2',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '4',
                'title' => 'Submitted to Processor',
                'is_active' => '1',
                'display_order' => '3',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '5',
                'title' => 'Quality Assurance Review',
                'is_active' => '1',
                'display_order' => '4',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '6',
                'title' => 'Healthcare Provider Review',
                'is_active' => '1',
                'display_order' => '5',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '7',
                'title' => 'Report Ready',
                'is_active' => '1',
                'display_order' => '6',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '8',
                'title' => 'Retesting Required',
                'is_active' => '1',
                'display_order' => '7',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '9',
                'title' => 'Submit to Lab Failed',
                'is_active' => '1',
                'display_order' => '8',
                'lab_order_type_id' => 1,
            ],
            [
                'id' => '10',
                'title' => 'Submit to Processor Failed',
                'is_active' => '1',
                'display_order' => '9',
                'lab_order_type_id' => 1,
            ],
             */
            [
                'id' => '11',
                'title' => 'Created',
                'is_active' => '1',
                'display_order' => '1',
                'lab_order_type_id' => 2,
            ],
            [
                'id' => '12',
                'title' => 'Created - Awaiting Provider Account Approval',
                'is_active' => '1',
                'display_order' => '2',
                'lab_order_type_id' => 2,
            ],
            [
                'id' => '13',
                'title' => 'Submitted to Lab',
                'is_active' => '1',
                'display_order' => '3',
                'lab_order_type_id' => 2,
            ],
            [
                'id' => '14',
                'title' => 'Results Ready',
                'is_active' => '1',
                'display_order' => '4',
                'lab_order_type_id' => 2,
            ]
        ];

        $table = $this->table('lab_order_statuses');
        $table->insert($data)->save();
    }
}
