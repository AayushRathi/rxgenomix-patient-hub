<?php
use Migrations\AbstractSeed;

/**
 * SpecimenTypesSeed seed.
 */
class SpecimenTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*
            [
                'id' => '1',
                'title' => 'Nasopharyngeal Swab',
                'is_active' => '1',
                'display_order' => '1'
            ],
            [
                'id' => '2',
                'title' => 'Anterior Nares (Nasal) Swab',
                'is_active' => '1',
                'display_order' => '2'
            ],
             */
            [
                'id' => '3',
                'title' => 'Saliva',
                'is_active' => '1',
                'display_order' => '3'
            ]
        ];

        $table = $this->table('specimen_types');
        $table->insert($data)->save();
    }
}
