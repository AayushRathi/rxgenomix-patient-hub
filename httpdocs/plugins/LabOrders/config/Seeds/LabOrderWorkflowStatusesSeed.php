<?php
use Migrations\AbstractSeed;

/**
 * LabOrderWorkflowStatuses seed.
 */
class LabOrderWorkflowStatusesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*
            [
                'id' => '1',
                'title' => 'Agree to Terms of Service',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '1',
            ],
            [
                'id' => '2',
                'title' => 'Promo Code',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '2',
            ],
            [
                'id' => '3',
                'title' => 'Review Patient Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '3',
            ],
            [
                'id' => '4',
                'title' => 'Provider Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '4',
            ],
            [
                'id' => '5',
                'title' => 'Add Medications',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '5',
            ],
            [
                'id' => '6',
                'title' => 'Shipping Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '6',
            ],
            [
                'id' => '7',
                'title' => 'Payment Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '7',
            ],
            [
                'id' => '8',
                'title' => 'Review All Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '8',
            ],
            [
                'id' => '9',
                'title' => 'Confirmation',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '9',
            ],
            [
                'id' => '10',
                'title' => 'Complete',
                'is_active' => '1',
                'lab_order_initiator_type' => '1',
                'display_order' => '10',
            ],
            [
                'id' => '11',
                'title' => 'Agree to Terms of Service',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '1',
            ],
            [
                'id' => '12',
                'title' => 'Promo Code',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '2',
            ],
            [
                'id' => '13',
                'title' => 'Review Patient Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '3',
            ],
            [
                'id' => '14',
                'title' => 'Provider Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '4',
            ],
            [
                'id' => '15',
                'title' => 'Add Medications',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '5',
            ],
            [
                'id' => '16',
                'title' => 'Shipping Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '6',
            ],
            [
                'id' => '17',
                'title' => 'Review All Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '7',
            ],
            [
                'id' => '18',
                'title' => 'Confirmation',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '8',
            ],
            [
                'id' => '19',
                'title' => 'Complete',
                'is_active' => '1',
                'lab_order_initiator_type' => '2',
                'display_order' => '9',
            ],
             */
            [
                'id' => '20',
                'title' => 'Review Patient Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '1',
            ],
            [
                'id' => '21',
                'title' => 'Provider Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '2',
            ],
            [
                'id' => '22',
                'title' => 'COVID Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '2',
            ],
            [
                'id' => '23',
                'title' => 'Shipping Information',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '3',
            ],
            [
                'id' => '24',
                'title' => 'Review All Info',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '4',
            ],
            [
                'id' => '25',
                'title' => 'Confirmation',
                'is_active' => '1',
                'lab_order_initiator_type' => '3',
                'display_order' => '5',
            ]
        ];

        $table = $this->table('lab_order_workflow_statuses');
        $table->insert($data)->save();
    }
}
