<?php
use Migrations\AbstractSeed;

/**
 * LabOrderTypes seed.
 */
class LabOrderTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'title' => 'PGX Panel',
                'is_active' => '1',
                'display_order' => '1'
            ],
            [
                'id' => '2',
                'title' => 'COVID-19 Test',
                'is_active' => '1',
                'display_order' => '2'
            ]
        ];

        $table = $this->table('lab_order_types');
        $table->insert($data)->save();
    }
}
