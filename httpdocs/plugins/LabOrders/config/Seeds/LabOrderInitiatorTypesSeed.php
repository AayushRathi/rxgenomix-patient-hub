<?php
use Migrations\AbstractSeed;

/**
 * LabOrderInitiatorTypes seed.
 */
class LabOrderInitiatorTypesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /*
            [
                'id' => '1',
                'title' => 'Direct-to-Consumer',
                'is_active' => '1',
                'display_order' => '3',
            ],
            [
                'id' => '2',
                'title' => 'Provider',
                'is_active' => '1',
                'display_order' => '1',
            ],
             */
            [
                'id' => '3',
                'title' => 'Covid',
                'is_active' => '1',
                'display_order' => '2'
            ]
        ];

        $table = $this->table('lab_order_initiator_types');
        $table->insert($data)->save();
    }
}
