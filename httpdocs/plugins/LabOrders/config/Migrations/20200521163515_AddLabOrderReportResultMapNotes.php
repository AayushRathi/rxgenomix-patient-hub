<?php
use Migrations\AbstractMigration;

class AddLabOrderReportResultMapNotes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_map_notes');
        $table->addColumn('id', 'integer', [
            'autoIncrement' => true,
            'limit' => 11,
            'null' => false,
            'signed' => false,
        ])
        ->addPrimaryKey(['id'])
        ->addColumn('lab_order_report_result_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'after' => 'id',
                'null' => false,
                'signed' => false,
        ])
        ->addColumn('detail', 'text', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('created', 'date')
        ->addColumn('modified', 'date')
        ->create();

        $table->addForeignKey(
            'lab_order_report_result_id',
            'lab_order_report_results',
            'id'
        )->update();
    }
}
