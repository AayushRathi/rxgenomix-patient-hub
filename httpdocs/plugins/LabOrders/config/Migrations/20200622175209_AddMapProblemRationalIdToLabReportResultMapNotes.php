<?php
use Migrations\AbstractMigration;

class AddMapProblemRationalIdToLabReportResultMapNotes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_map_notes')
            ->addColumn('map_problem_rationale_id', 'integer', [
                'limit' => 10,
                'default' => null,
                'null' => true,
                'signed' => false,
            ])->addForeignKey('map_problem_rationale_id', 'map_problem_rationales', 'id')->update();
    }
}
