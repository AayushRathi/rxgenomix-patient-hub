<?php
use Migrations\AbstractMigration;

class CreateLabOrderInitiatorTypesUsers extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_initiator_types_users')
            ->addColumn('id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'autoIncrement' => true,
                'null' => false
            ])
            ->addColumn('user_id', 'char', [
                'limit' => 36,
                'null' => false
            ])
            ->addColumn('lab_order_initiator_type_id', 'integer', [
                'limit' => 11,
                'null' => false
            ])
            ->addPrimaryKey(['id'])
            ->addForeignKey('user_id', 'users', 'id')
            ->addForeignKey('lab_order_initiator_type_id', 'lab_order_initiator_types', 'id')
            ->create();

        $this->execute('INSERT INTO lab_order_initiator_types_users (user_id, lab_order_initiator_type_id) SELECT id, lab_order_initiator_type FROM users');
    }
}
