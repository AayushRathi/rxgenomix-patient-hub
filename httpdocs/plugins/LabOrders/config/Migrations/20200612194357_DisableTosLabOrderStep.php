<?php
use Migrations\AbstractMigration;

class DisableTosLabOrderStep extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->query('UPDATE lab_order_workflow_statuses SET is_active=0 WHERE action="tos" AND lab_order_initiator_type=2');
    }
}
