<?php
use Migrations\AbstractMigration;

class AddDisplayOrderToLabOrderInitiatorTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_initiator_types')
            ->addColumn('display_order', 'integer', [ 'limit' => 10, 'default' => null, 'null' => true ])
            ->update();

        $this->execute('UPDATE lab_order_initiator_types SET display_order = 1, title="PGX" WHERE id = 2');
        $this->execute('UPDATE lab_order_initiator_types SET display_order = 2 WHERE id = 3');
        $this->execute('UPDATE lab_order_initiator_types SET is_active = 0, display_order=3 WHERE id = 1');

        $table = $this->table('lab_order_initiator_types')
            ->changeColumn('display_order', 'integer', [ 'limit' => 10, 'null' => false ])
            ->update();
    }
}
