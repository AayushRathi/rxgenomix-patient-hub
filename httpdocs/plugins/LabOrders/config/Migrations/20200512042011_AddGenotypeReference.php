<?php
use Migrations\AbstractMigration;

class AddGenotypeReference extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_order_report_result_observations')
            ->addColumn('genotype_reference', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->update();
    }
}
