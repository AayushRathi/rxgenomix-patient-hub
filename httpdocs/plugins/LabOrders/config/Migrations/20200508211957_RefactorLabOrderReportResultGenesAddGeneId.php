<?php
use Migrations\AbstractMigration;

class RefactorLabOrderReportResultGenesAddGeneId extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('lab_order_report_result_genes');
        $table->changeColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])->addColumn('gene_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->removeColumn('gene')
            ->removeColumn('reference')
            ->removeColumn('value')
            ->update();

        $table->addForeignKey('gene_id', 'genes', 'id')->update();
    }

    public function down()
    {
        $table = $this->table('lab_order_report_result_genes');
        $table->dropForeignKey('gene_id')->removeColumn('gene_id')
            ->addColumn('gene', 'text')
            ->addColumn('reference', 'text')
            ->addColumn('value', 'text')
            ->update();
    }
}
