<?php
use Migrations\AbstractMigration;

class AddLabOrderInitiatorTypeIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addColumn('lab_order_initiator_type_id', 'integer', [
                'limit' => 11,
                'null' => false,
                'signed' => true,
                'default' => 0
            ])
            ->update();

        $this->execute('UPDATE lab_orders SET lab_order_initiator_type_id = 2 WHERE lab_order_initiator_type_id = 0');
    }
}
