<?php
use Migrations\AbstractMigration;

class AddTsiFieldsToLabOrderReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->addColumn('tsi_title', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_description', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_rxcui', 'integer', [
                'limit' => 10,
                'default' => null,
                'null' => true,
                'signed' => false,
            ])
            ->addColumn('tsi_detected_issue_id', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_evidence_strength', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_actcode_code', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_actcode_display', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_disease_name', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_issue_type_model1', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_issue_type_model2', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_issue_type_model3', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_issue_type_model4', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_severity', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('tsi_detail', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('tsi_alleles_tested', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addForeignKey(
                'medication_id',
                'medications',
                'id'
            )->update();
    }
}
