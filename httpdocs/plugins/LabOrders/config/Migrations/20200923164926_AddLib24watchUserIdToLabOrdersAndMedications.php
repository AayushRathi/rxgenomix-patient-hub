<?php
use Migrations\AbstractMigration;

class AddLib24watchUserIdToLabOrdersAndMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addForeignKey('created_by_lib24watch_user_id', 'lib24watch_users', 'id')
            ->update();

        $table = $this->table('lab_order_medications')
            ->addColumn('created_by_lib24watch_user_id', 'integer', [
                'limit' => 11,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('created_by_lib24watch_user_id', 'lib24watch_users', 'id')
            ->update();
    }
}
