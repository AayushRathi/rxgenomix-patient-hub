<?php
use Migrations\AbstractMigration;

class AddMedicationIdToLabOrderMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_medications')
            ->addColumn('medication_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => true,
                'signed' => false,
            ])
            ->update();

        $this->table('lab_order_medications')
            ->addForeignKey('medication_id', 'medications', 'id')
            ->update();
    }
}
