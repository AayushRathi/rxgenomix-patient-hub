<?php
use Migrations\AbstractMigration;

class AddUserIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('user_id', 'char', ['limit' => 36 ]);
        $table->update();
    }
}
