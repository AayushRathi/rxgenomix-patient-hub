<?php
use Migrations\AbstractMigration;

class RemoveTsiMedicationFieldsFromLabOrderReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->removeColumn('medication_id')
            ->removeColumn('tsi_medication_id')
            ->removeColumn('tsi_medication_category')
            ->removeColumn('tsi_medication_class')
            ->removeColumn('tsi_medication_tradenames')
            ->update();
    }
}
