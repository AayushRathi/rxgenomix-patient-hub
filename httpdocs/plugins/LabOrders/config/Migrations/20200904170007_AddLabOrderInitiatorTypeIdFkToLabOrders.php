<?php
use Migrations\AbstractMigration;

class AddLabOrderInitiatorTypeIdFkToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addForeignKey('lab_order_initiator_type_id', 'lab_order_initiator_types', 'id')
            ->update();
    }
}
