<?php
use Migrations\AbstractMigration;

class AddStepFieldsToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('tos_accepted', 'integer', [ 'limit' => 1 ])
            ->addColumn('promo_code', 'string');
        $table->update();
    }
}
