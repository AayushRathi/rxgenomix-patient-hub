<?php
use Migrations\AbstractMigration;

class AddComponentFieldsToObservations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_observations')
            ->addColumn('component_coding_1_system', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('component_coding_1_code', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('component_coding_1_display', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('component_value_coding_1_system', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('component_value_coding_1_code', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('component_value_coding_1_display', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->update();
    }
}
