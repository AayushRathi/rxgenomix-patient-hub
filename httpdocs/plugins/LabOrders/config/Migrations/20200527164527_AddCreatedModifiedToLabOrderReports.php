<?php
use Migrations\AbstractMigration;

class AddCreatedModifiedToLabOrderReports extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_reports')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('update', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
