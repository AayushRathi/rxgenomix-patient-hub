<?php
use Migrations\AbstractMigration;

class AddLabOrderHistory extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_history');

        $table
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'limit' => 11,
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('lab_order_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'after' => 'id',
                'null' => false,
                'signed' => false,
            ])
            ->addColumn('uuid', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('created', 'date', [
                'null' => false
            ])
            ->addPrimaryKey(['id'])
            ->create();

        $table->addForeignKey(
            'lab_order_id',
            'lab_orders',
            'id'
        )->update();
    }
}
