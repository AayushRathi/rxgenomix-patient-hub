<?php

use Migrations\AbstractMigration;

class AddIsDeletedToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_orders')
            ->addColumn(
                'is_deleted',
                'boolean',
                [
                    'default' => false,
                    'null' => false,
                ]
            )
            ->addIndex(['is_deleted'])
            ->update();
    }
}
