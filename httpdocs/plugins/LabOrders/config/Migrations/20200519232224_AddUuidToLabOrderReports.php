<?php
use Migrations\AbstractMigration;

class AddUuidToLabOrderReports extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_reports')
            ->addColumn('uuid', 'text', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
