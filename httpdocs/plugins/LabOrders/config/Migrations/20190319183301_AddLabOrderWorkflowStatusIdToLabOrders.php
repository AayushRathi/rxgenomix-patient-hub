<?php
use Migrations\AbstractMigration;

class AddLabOrderWorkflowStatusIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('lab_order_workflow_status_id', 'integer', [ 'limit' => 11 ]);
        $table->update();
    }
}
