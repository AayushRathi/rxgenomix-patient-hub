<?php
use Migrations\AbstractMigration;

class AddLabOrderTypeToLabOrderStatuses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_statuses')
            ->addColumn('lab_order_type_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'default' => 1,
                'signed' => false
            ])
            ->addForeignKey('lab_order_type_id', 'lab_order_types', 'id')
            ->update();
    }
}
