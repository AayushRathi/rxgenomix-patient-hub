<?php
use Migrations\AbstractMigration;

class AddSpecimenFieldsToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addColumn('specimen_barcode', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true,
            ])
            ->addColumn('specimen_collection_date', 'date', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('specimen_time', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('specimen_identifier', 'string', [
                'limit' => 255,
                'default' => null,
                'null' => true
            ])
            ->addColumn('specimen_type_id', 'integer', [
                'limit' => 11,
                'default' => null,
                'null' => true,
                'signed' => false
            ])
            ->update();
    }
}
