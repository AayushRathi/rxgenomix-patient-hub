<?php
use Migrations\AbstractMigration;

class CreateLabOrderReportResultMedications extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_medications');
        $table->addColumn('id', 'integer', [
            'autoIncrement' => true,
            'limit' => 11,
            'null' => false,
            'signed' => false,
        ])
        ->addColumn('lab_order_report_result_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'after' => 'id',
                'null' => false,
                'signed' => false,
        ])
        ->addColumn('medication_id', 'integer', [
            'signed' => false,
            'default' => null,
            'null' => true,
            'limit' => 10
        ])
        ->addColumn('tsi_medication_id', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 10
        ])
        ->addColumn('category', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 255
        ])
        ->addColumn('class', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 255
        ])
        ->addColumn('tradenames', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 255
        ])
        ->addPrimaryKey(['id'])
        ->create();

        $table->addForeignKey(
            'medication_id',
            'medications',
            'id'
        )->addForeignKey(
            'lab_order_report_result_id',
            'lab_order_report_results',
            'id'
        )->update();
    }
}
