<?php
use Migrations\AbstractMigration;

class AddCovidInitiatorTypeToLabOrderInitiatorTypesUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('
            INSERT INTO lab_order_initiator_types_users (id, user_id, lab_order_initiator_type_id)
                SELECT NULL as id,
                user_id,
                3
                FROM lab_order_initiator_types_users
        ');
    }
}
