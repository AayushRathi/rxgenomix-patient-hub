<?php
use Migrations\AbstractMigration;

class AddLabOrderMedicationsToLabOrderReportResultMapNotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_map_notes')
            ->addColumn('lab_order_medication_id', 'integer', [
                'null' => true,
                'limit' => 10,
                'signed' => false,
            ])
            ->changeColumn('lab_order_report_result_id', 'integer', [
                'null' => true,
                'limit' => 10,
                'signed' => false,
            ])
            ->addForeignKey('lab_order_medication_id', 'lab_order_medications', 'id')
            ->update();
    }
}
