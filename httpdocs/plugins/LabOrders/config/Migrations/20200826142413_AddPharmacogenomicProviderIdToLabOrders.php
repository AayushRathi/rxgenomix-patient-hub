<?php
use Migrations\AbstractMigration;

class AddPharmacogenomicProviderIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addColumn('pharmacogenomic_provider_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
            ])
            ->addForeignKey('pharmacogenomic_provider_id', 'pharmacogenomic_providers', 'id')
            ->update();

        $this->execute('UPDATE lab_orders SET pharmacogenomic_provider_id = 2 WHERE id > 410');
        $this->execute('UPDATE lab_orders SET pharmacogenomic_provider_id = 1 WHERE id < 410');
    }
}
