<?php
use Migrations\AbstractMigration;

class AddIsRelevantOriginalToLabOrderReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->addColumn('is_relevant_original', 'boolean', [
                'default' => false
            ])
            ->update();

        $this->execute('UPDATE lab_order_report_results SET is_relevant_original = is_relevant');
    }
}
