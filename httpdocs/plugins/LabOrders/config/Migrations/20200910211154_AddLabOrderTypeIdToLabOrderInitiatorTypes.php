<?php
use Migrations\AbstractMigration;

class AddLabOrderTypeIdToLabOrderInitiatorTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_initiator_types')
            ->addColumn('lab_order_type_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => false,
                'default' => 1
            ])
            ->addForeignKey('lab_order_type_id', 'lab_order_types', 'id')
            ->update();

        $this->execute('UPDATE lab_order_initiator_types SET lab_order_type_id = 2 WHERE id = 3 LIMIT 1');
    }
}
