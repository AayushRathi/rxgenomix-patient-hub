<?php
use Migrations\AbstractMigration;

class AddStatusChangeDateLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_orders')
            ->addColumn('lab_order_status_change_date', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
