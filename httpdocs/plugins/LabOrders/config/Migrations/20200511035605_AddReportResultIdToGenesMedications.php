<?php
use Migrations\AbstractMigration;

class AddReportResultIdToGenesMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        /*
        $this->table('lab_order_report_result_genes')->addColumn('lab_order_report_result_id', 'integer', [
                'default' => null,
                'after' => 'id',
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addForeignKey(
                'lab_order_report_result_id',
                'lab_order_report_results',
                'id'
            )->update();
         */

        /*
        $this->table('lab_order_report_result_medications')->addColumn('lab_order_report_result_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'after' => 'id',
                'null' => false,
                'signed' => false,
            ])
            ->addForeignKey(
                'lab_order_report_result_id',
                'lab_order_report_results',
                'id'
            )->update();
         */
    }
}
