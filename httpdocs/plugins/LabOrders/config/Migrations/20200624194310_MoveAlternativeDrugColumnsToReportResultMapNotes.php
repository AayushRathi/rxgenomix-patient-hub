<?php
use Migrations\AbstractMigration;

class MoveAlternativeDrugColumnsToReportResultMapNotes extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->dropForeignKey('alternate_drug_id')
            ->dropForeignKey('alternate_result_id')
            ->update();

        $table = $this->table('lab_order_report_results')
            ->removeColumn('alternate_drug_id')
            ->removeColumn('alternate_result_id')
            ->update();

        $table = $this->table('lab_order_report_result_map_notes')
            ->addColumn('alternate_drug_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addColumn('alternate_result_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('alternate_drug_id', 'drugs', 'id')
            ->addForeignKey('alternate_result_id', 'lab_order_report_results', 'id')
            ->update();
    }
}
