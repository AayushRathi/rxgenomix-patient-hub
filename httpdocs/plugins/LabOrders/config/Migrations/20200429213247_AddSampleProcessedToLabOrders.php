<?php
use Migrations\AbstractMigration;

class AddSampleProcessedToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('sample_processed_date', 'date', [
            'default' => null,
            'null' => true,
            'after' => 'collection_date'
        ])->update();
    }
}
