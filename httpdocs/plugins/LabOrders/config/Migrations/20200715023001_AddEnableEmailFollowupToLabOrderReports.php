<?php
use Migrations\AbstractMigration;

class AddEnableEmailFollowupToLabOrderReports extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_reports')
            ->addColumn('enable_email_followup', 'boolean', [
                'default' => false,
            ])
            ->update();

        $table = $this->table('lab_orders')
            ->addColumn('enable_email_followup', 'boolean', [
                'default' => false,
            ])
            ->update();
    }
}
