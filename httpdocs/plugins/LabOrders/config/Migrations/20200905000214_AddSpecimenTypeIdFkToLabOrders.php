<?php
use Migrations\AbstractMigration;

class AddSpecimenTypeIdFkToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addForeignKey('specimen_type_id', 'specimen_types', 'id')
            ->update();
    }
}
