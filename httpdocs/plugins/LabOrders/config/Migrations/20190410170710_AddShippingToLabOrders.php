<?php
use Migrations\AbstractMigration;

class AddShippingToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('ship_to_provider', 'integer', [
            'limit' => 1,
            'default' => 0
        ]);
        $table->addColumn('ship_to_patient', 'integer', [
            'limit' => 1,
            'default' => 0
        ]);
        $table->addColumn('shipping_first_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_last_name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_address_1', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_address_2', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_city', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_lib24watch_state_abbr', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->addColumn('shipping_postal_code', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ]);
        $table->update();


    }
}
