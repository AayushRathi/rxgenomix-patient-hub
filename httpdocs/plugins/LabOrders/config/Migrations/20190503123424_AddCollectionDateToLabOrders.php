<?php
use Migrations\AbstractMigration;

class AddCollectionDateToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('collection_date', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
