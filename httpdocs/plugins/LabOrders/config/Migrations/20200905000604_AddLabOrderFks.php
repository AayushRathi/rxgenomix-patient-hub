<?php
use Migrations\AbstractMigration;

class AddLabOrderFks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_orders')
            ->addForeignKey('lab_order_workflow_status_id', 'lab_order_workflow_statuses', 'id')
            ->addForeignKey('lab_order_status_id', 'lab_order_statuses', 'id')
            ->addForeignKey('patient_id', 'patients', 'id')
            ->addForeignKey('provider_id', 'providers', 'id')
            ->update();
    }
}
