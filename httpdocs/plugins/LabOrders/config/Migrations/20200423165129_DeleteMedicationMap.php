<?php
use Migrations\AbstractMigration;

class DeleteMedicationMap extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('lab_orders')->removeColumn('medication_map')->update();
    }

    public function down()
    {
        $table = $this->table('lab_orders')
            ->addColumn('medication_map', 'text', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
