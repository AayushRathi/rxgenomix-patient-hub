<?php
use Migrations\AbstractMigration;

class AddAllelesTestedToLabOrderReportResultGenes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_genes')
            ->addColumn('alleles_tested', 'text', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
