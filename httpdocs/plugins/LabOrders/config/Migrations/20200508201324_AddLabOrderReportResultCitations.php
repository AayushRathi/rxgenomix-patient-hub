<?php
use Migrations\AbstractMigration;

class AddLabOrderReportResultCitations extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_citations');

        $table->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
                'signed' => false,
        ])->addColumn('lab_order_report_result_id', 'integer', [
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('description', 'text')
            ->create();

        $table->addForeignKey(
            'lab_order_report_result_id',
            'lab_order_report_results',
            'id'
        )->update();
    }
}
