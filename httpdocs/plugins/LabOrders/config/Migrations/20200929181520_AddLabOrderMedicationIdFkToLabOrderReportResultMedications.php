<?php
use Migrations\AbstractMigration;

class AddLabOrderMedicationIdFkToLabOrderReportResultMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        /*
         * On dev might need to unset, eg
         *
         * update lab_order_report_result_medications
         * set lab_order_medication_id = null
         * where lab_order_medication_id is not null;
         */
        $table = $this->table('lab_order_report_result_medications')
            ->addForeignKey('lab_order_medication_id', 'lab_order_medications', 'id')
            ->update();
    }
}
