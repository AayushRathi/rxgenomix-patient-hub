<?php
use Migrations\AbstractMigration;

class AddValueableConceptFieldsReferenceDisplayToObservations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_observations')
            ->addColumn('reference', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
                'after' => 'lab_order_report_result_id'
            ])
            ->addColumn('display', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
                'after' => 'reference'
            ])
            ->addColumn('value_coding_1_system', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('value_coding_1_code', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('value_coding_1_display', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('value_coding_2_system', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('value_coding_2_code', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('value_coding_2_display', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->update();
    }
}
