<?php
use Migrations\AbstractMigration;

class AddLabOrderMedicationIdToLabOrderReportResultMedications extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_medications')
            ->addColumn('lab_order_medication_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'default' => null,
                'null' => true
            ])->update();
    }
}
