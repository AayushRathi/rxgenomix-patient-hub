<?php
use Migrations\AbstractMigration;

class AddEnableEmailFollowupStartDate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_order_reports')
            ->addColumn('enable_email_followup_start_date', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->update();

        $this->table('lab_orders')
            ->addColumn('enable_email_followup_start_date', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
