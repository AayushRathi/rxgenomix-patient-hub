<?php
use Migrations\AbstractMigration;

class AddSequenceMetadataToLabOrderReports extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_reports')
            ->addColumn('last_sent_sequence', 'integer', [
                'null' => true,
                'default' => 0,
                'limit' => 10,
            ])
            ->addColumn('last_sent_sequence_date', 'datetime', [
                'null' => true,
                'default' => null,
            ])
            ->update();

        $table = $this->table('lab_orders')
            ->addColumn('last_sent_sequence', 'integer', [
                'null' => true,
                'default' => 0,
                'limit' => 10,
            ])
            ->addColumn('last_sent_sequence_date', 'datetime', [
                'null' => true,
                'default' => null,
            ])
            ->update();
    }
}
