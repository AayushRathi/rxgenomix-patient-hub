<?php
use Migrations\AbstractMigration;

class AddTypeAndCodingTextToObservations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_observations')
            ->addColumn('type', 'enum', [
                'values' => ['genotype', 'phenotype', 'other']
            ])
            ->addColumn('value_string', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
            ])
            ->addColumn('coding_text', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255,
                'after' => 'label_display'
            ])
            ->update();
    }
}
