<?php
use Migrations\AbstractMigration;

class AddLabOrderTypeIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addColumn('lab_order_type_id', 'integer', [
                'default' => 1,
                'null' => false,
                'limit' => 11,
                'signed' => false
            ])
            ->update();

        //$this->execute('UPDATE lab_orders SET lab_order_type_id = 1 WHERE lab_order_type_id IS NULL');
    }
}
