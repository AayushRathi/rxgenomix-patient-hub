<?php
use Migrations\AbstractMigration;

class AddSeenUserIdAndFksToLabOrderReports extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_reports')
            ->addColumn('first_seen_provider_id', 'integer', [
                'null' => true,
                'limit' => 10,
                'signed' => false,
                'default' => null
            ])
            ->update();

        $this->table('lab_orders')
            ->addColumn('first_seen_provider_id', 'integer', [
                'null' => true,
                'limit' => 10,
                'signed' => false,
                'default' => null
            ])
            ->update();

        $this->table('lab_order_reports')
            ->addForeignKey('first_seen_provider_id', 'providers', 'id')
            ->update();

        $this->table('lab_orders')
            ->addForeignKey('first_seen_provider_id', 'providers', 'id')
            ->update();
    }
}
