<?php
use Migrations\AbstractMigration;

class AddIsRelevantToLabOrderReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->addColumn('is_relevant', 'boolean', [
                'default' => 0,
            ])
            ->update();
    }
}
