<?php
use Migrations\AbstractMigration;

class AddDetectedIssueIndexToLabOrderReportResults extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->addColumn('detected_issue_index', 'integer', [
                'limit' => 11,
                'null' => true,
                'default' => null,
                'signed' => false,
            ])
            ->update();
    }
}
