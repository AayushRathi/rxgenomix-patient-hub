<?php
use Migrations\AbstractMigration;

class AddAlternativeDrugAndResultIdsToReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results');

        $table
            ->addColumn('alternate_drug_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addColumn('alternate_result_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('alternate_drug_id', 'drugs', 'id')
            ->addForeignKey('alternate_result_id', 'lab_order_report_results', 'id')
            ->update();
    }
}
