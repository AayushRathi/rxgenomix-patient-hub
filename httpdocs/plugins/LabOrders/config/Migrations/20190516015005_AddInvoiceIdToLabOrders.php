<?php
use Migrations\AbstractMigration;

class AddInvoiceIdToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('invoice_id', 'integer', [
            'default' => null,
            'limit' => 10,
            'null' => true,
            'signed' => false
        ]);
        $table->addIndex('invoice_id');
        $table->addForeignKey('invoice_id', 'invoices', 'id', ['delete' => 'SET NULL', 'update' => 'CASCADE']);
        $table->update();
    }
}
