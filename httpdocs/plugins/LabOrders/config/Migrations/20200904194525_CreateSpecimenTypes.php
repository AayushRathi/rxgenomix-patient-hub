<?php
use Migrations\AbstractMigration;
use Migrations\Migrations;

class CreateSpecimenTypes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('specimen_types')
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'signed' => false,
                'autoIncrement' => true,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'limit' => 255,
                'default' => null
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => true
            ])
            ->addColumn('display_order', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true
            ])
            ->create();
    }
}
