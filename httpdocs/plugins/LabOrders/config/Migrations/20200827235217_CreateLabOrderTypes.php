<?php
use Migrations\AbstractMigration;

class CreateLabOrderTypes extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_types')
            ->addColumn('id', 'integer', [
                'null' => false,
                'limit' => 11,
                'signed' => false,
                'autoIncrement' => true
            ])
            ->addColumn('title', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('display_order', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => true
            ])
            ->addPrimaryKey(['id'])
            ->create();
    }
}
