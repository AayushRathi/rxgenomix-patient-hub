<?php
use Migrations\AbstractMigration;

class AddRxcuiIdToLabOrderMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_medications')->addColumn('rxcui_id', 'integer', [
            'limit' => 10,
            'null' => true,
            'signed' => false
        ])->update();

        $this->table('lab_order_medications')->addForeignKey('rxcui_id', 'rxcuis','id')->update();
    }
}
