<?php
use Migrations\AbstractMigration;

class AddIsIncludedPrescriberNotesPatientNotesToMapNotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_map_notes')
            ->addColumn('is_included', 'boolean', [
                'default' => 1
            ])->addColumn('patient_notes', 'text', [
                'null' => true,
                'default' => null,
            ])->addColumn('prescriber_notes', 'text', [
                'null' => true,
                'default' => null,
            ])
            ->update();
    }
}
