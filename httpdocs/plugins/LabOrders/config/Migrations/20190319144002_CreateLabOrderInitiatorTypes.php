<?php
use Migrations\AbstractMigration;

class CreateLabOrderInitiatorTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_initiator_types');
        $table->addColumn('title', 'string')
            ->addColumn('is_active', 'integer', [ 'limit' => 1 ]);
        $table->create();
    }
}
