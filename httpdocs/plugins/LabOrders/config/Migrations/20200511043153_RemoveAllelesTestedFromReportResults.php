<?php
use Migrations\AbstractMigration;

class RemoveAllelesTestedFromReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->removeColumn('tsi_alleles_tested')
            ->removeColumn('tsi_rxcui')
            ->update();
    }
}
