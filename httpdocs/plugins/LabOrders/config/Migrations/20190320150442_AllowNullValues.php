<?php
use Migrations\AbstractMigration;

class AllowNullValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->changeColumn('tos_accepted', 'integer', [ 'limit' => 1, 'null' => true ])
            ->changeColumn('promo_code', 'string', [ 'null' => true ]);
        $table->update();
    }
}
