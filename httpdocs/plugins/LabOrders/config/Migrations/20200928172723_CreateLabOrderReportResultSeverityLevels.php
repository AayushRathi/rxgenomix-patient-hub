<?php
use Migrations\AbstractMigration;

class CreateLabOrderReportResultSeverityLevels extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_result_severity_levels')
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'signed' => false,
                'null' => false,
                'autoIncrement' => true
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('slug', 'string', [
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('display_order', 'integer', [
                'limit' => 11,
                'signed' => false,
                'null' => false
            ])
            ->addColumn('color_hex', 'char', [
                'limit' => 6,
                'null' => false,
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => true
            ])
            ->create();
    }
}
