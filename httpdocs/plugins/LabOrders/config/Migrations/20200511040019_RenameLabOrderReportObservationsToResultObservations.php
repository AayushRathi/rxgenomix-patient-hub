<?php
use Migrations\AbstractMigration;

class RenameLabOrderReportObservationsToResultObservations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_observations');

        $table->rename('lab_order_report_result_observations')->update();

        $this->table('lab_order_report_result_observations')
            ->dropForeignKey('lab_order_report_id')
            ->update();

        $this->table('lab_order_report_result_observations')
            ->removeColumn('lab_order_report_id')
            ->addColumn('lab_order_report_result_id', 'integer', [
                    'default' => null,
                    'after' => 'id',
                    'limit' => 10,
                    'null' => false,
                    'signed' => false,
                ])
            ->addForeignKey(
                'lab_order_report_result_id',
                'lab_order_report_results',
                'id'
            )->update();
    }
}
