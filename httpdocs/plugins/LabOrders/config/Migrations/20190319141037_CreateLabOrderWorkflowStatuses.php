<?php
use Migrations\AbstractMigration;

class CreateLabOrderWorkflowStatuses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_workflow_statuses');
        $table->addColumn('title', 'string')
            ->addColumn('is_active', 'integer', [ 'limit' => 1 ])
            ->addColumn('lab_order_initiator_type', 'integer', [ 'limit' => 10 ])
            ->addColumn('display_order', 'integer', [ 'limit' => 10 ]);
        $table->create();
    }
}
