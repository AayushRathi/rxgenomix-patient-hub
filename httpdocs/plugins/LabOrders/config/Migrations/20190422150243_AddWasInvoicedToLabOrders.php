<?php
use Migrations\AbstractMigration;

class AddWasInvoicedToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders');
        $table->addColumn('was_invoiced', 'integer', [
            'default' => 0,
            'limit' => 1,
            'null' => false,
        ]);
        $table->update();
    }
}
