<?php
use Migrations\AbstractMigration;

class AddIsLabOrderMedicationToLabOrderReportResults extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_results')
            ->addColumn('is_lab_order_medication', 'boolean', [
                'default' => false,
                'null' => false
            ])->update();
    }
}
