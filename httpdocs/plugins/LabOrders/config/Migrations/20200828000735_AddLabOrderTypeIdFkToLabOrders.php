<?php
use Migrations\AbstractMigration;

class AddLabOrderTypeIdFkToLabOrders extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_orders')
            ->addForeignKey('lab_order_type_id', 'lab_order_types', 'id')
            ->update();
    }
}
