<?php
use Migrations\AbstractMigration;

class AddMedicationMapToLabOrders extends AbstractMigration
{
    public $autoId = false;
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_orders')
            ->addColumn('medication_map', 'text', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
