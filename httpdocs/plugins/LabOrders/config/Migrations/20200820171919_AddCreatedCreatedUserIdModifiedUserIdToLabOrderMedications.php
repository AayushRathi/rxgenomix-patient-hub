<?php
use Migrations\AbstractMigration;

class AddCreatedCreatedUserIdModifiedUserIdToLabOrderMedications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_medications')
            ->addColumn('created', 'timestamp', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('modified', 'timestamp', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('created_user_id', 'char', [
                'default' => null,
                'limit' => 36,
                'signed' => false,
                'null' => true
            ])
            ->addColumn('modified_user_id', 'char', [
                'default' => null,
                'limit' => 36,
                'signed' => false,
                'null' => true
            ])
            ->update();

        $this->execute('UPDATE lab_order_medications as m INNER JOIN lab_orders l ON ( l.id = m.lab_order_id) SET m.created = l.created WHERE m.created IS NULL');
        //$this->execute('UPDATE lab_order_medications as m INNER JOIN lab_orders l ON ( l.id = m.lab_order_id) SET m.modified = l.created WHERE m.modified IS NULL');
        $this->execute('UPDATE lab_order_medications as m INNER JOIN lab_orders l ON ( l.id = m.lab_order_id) INNER JOIN providers p ON ( l.provider_id = p.id) SET m.created_user_id = p.rxg_user_id WHERE m.created_user_id IS NULL');
        //$this->execute('UPDATE lab_order_medications as m INNER JOIN lab_orders l ON ( l.id = m.lab_order_id) INNER JOIN providers p ON ( l.provider_id = p.id) SET m.modified_user_id = p.rxg_user_id WHERE m.modified_user_id IS NULL');
    }
}
