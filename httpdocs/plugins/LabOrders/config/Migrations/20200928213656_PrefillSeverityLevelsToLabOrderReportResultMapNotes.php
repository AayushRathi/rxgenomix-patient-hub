<?php
use Migrations\AbstractMigration;

class PrefillSeverityLevelsToLabOrderReportResultMapNotes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('lab_order_report_result_map_notes')
            ->addColumn('severity_level_id', 'integer', [
                'limit' => 11,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('severity_level_id', 'lab_order_report_result_severity_levels', 'id')
            ->update();

        $this->query('
            UPDATE lab_order_report_result_map_notes
            INNER JOIN lab_order_report_results ON (lab_order_report_result_map_notes.alternate_result_id=lab_order_report_results.id)
            SET severity_level_id = (SELECT id FROM lab_order_report_result_severity_levels WHERE LOWER(slug) = LOWER(lab_order_report_results.tsi_severity))
            WHERE lab_order_report_results.tsi_severity IS NOT NULL
        ');
    }
}
