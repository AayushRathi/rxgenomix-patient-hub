<?php
use Migrations\AbstractMigration;

class AddLabOrderReportAdditional extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('lab_order_report_additional');

        $table
            ->addColumn('id', 'integer', [
                'limit' => 11,
                'autoIncrement' => true,
                'null' => false,
                'signed' => false
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('lab_order_report_id', 'integer', [
                'limit' => 10,
                'null' => false,
                'signed' => false
            ])
            ->addColumn('additional_pharmacist_notes_prescriber', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('additional_pharmacist_notes_patient', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('adverse_drug_reaction_notes_prescriber', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('adverse_drug_reaction_notes_patient', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('lifestyle_recommendation_notes_prescriber', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('lifestyle_recommendation_notes_patient', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('created', 'date')
            ->addColumn('modified', 'date')
            ->create();

        $table->addForeignKey(
            'lab_order_report_id',
            'lab_order_reports',
            'id'
        )->update();
    }
}
