<?php

use Cake\Routing\Router;

Router::plugin('RxgTheme', function ($routes) {
    $routes->connect('/css/compiled/*', [], ['routeClass' => 'RxgTheme\Routing\Route\SassCompilerRoute']);
    $routes->fallbacks(DashedRoute::class);
});
