jQuery(function ($) {

    "use strict";

	$('.filter__select li').on('click', function(){
		$('.filter__select li').removeClass('active');
		$(this).addClass('active');
	})

	// Portfolio isotope grid
	var $grid1 = $('.grid-portfolio').isotope({
		itemSelector: '.grid-portfolio__item',
		masonry: {
		  columnWidth: '.grid-portfolio__sizer',
		  gutter: 35
		},
        hiddenStyle: {
            opacity: 0
        },
        visibleStyle: {
            opacity: 1
        }
	});

	// Portfolio isotope filtering
	$('.filter-button-group a').on('click', function() {
		var filterValue = $(this).attr('data-filter');
		$('.filter-button-group a').removeClass('active');
		$(".grid-portfolio__item").removeClass("animated");
		$(".grid-portfolio__item").removeClass("fadeInUp");
		$(this).addClass('active');
		$grid1.isotope({ filter: filterValue });
	});

	// Categories isotope grid
	var $grid2 = $('.grid-cat').isotope({
		itemSelector: '.grid-cat__item',
		percentPosition: true,
		masonry: {
			columnWidth: 25
		}
	});

	// Categories isotope filtering
	$('.filter__select li').on('click', function() {
		var filterValue = $(this).attr('data-filter');
		$(".grid-cat__item").removeClass("animated");
		$(".grid-cat__item").removeClass("fadeInUp");
		$grid2.isotope({ filter: filterValue });
	});


	var $services_grid = $('.grid-services').isotope({
		itemSelector: '.grid-services-item',
		filter: $('.filter-button-menu a.active').data('filter'),
		percentPosition: true,
		masonry: {
			gutter: '.grid-services-gutter',
			columnWidth: '.grid-services-sizer',
		},
		hiddenStyle: {
			opacity: 0
		},
		visibleStyle: {
			opacity: 1
		},
		stagger: 30,
		transitionDuration: '0.5s'
	});

	// Portfolio isotope filtering
	$('.filter-button-menu a').on('click', function() {
		var filterValue = $(this).attr('data-filter');
		$('.filter-button-menu a').removeClass('active');
		$(".grid-services-item").removeClass("animated");
		$(".grid-services-item").removeClass("fadeInUp");
		$(this).addClass('active');
		$services_grid.isotope({ filter: filterValue });
	});

	$(document).ajaxComplete(function(){
		$services_grid.isotope();
	});



	$('.blog-masonry').masonry({
		itemSelector: '.blog-masonry-item', // use a separate class for itemSelector, other than .col-
		columnWidth: '.blog-masonry-item',
		percentPosition: true
	});



    function animate_news_card_message(){
        if (screen.width >= '1200') {$('.news-card-message__carousel .owl-item.active').next().animated('fadeInUp');}
    };

    if (screen.width >= '1200') {
        $('.news-card-message__carousel .news-card-message').css('opacity', '0');
        $('.news-card-message__carousel .owl-item.active').animated("fadeInUp");

        var waypont51235134615 = $('.news-card-message__carousel').waypoint(function(direction) {
            var delay3 = 200;
            $(".news-card-message").each( function( index , element){
                setTimeout(function(){
                    $(element).animated("fadeInUp");
                }, delay3);
                delay3 += 250;

            });
        }, {
            offset: '85%'
        });
    }

	$('.portfolio-cards').owlCarousel({
		onTranslate: animateNextCardsItem,
		responsive:{
			0:{
				items:1
			},
			768:{
				items:2
			},
			992:{
				items:3
			},
			1600:{
				items:4
			}
		}
	});

	function animateNextCardsItem(){
		$('.portfolio-cards .owl-item.active').next().animated('fadeInUp');
	};

	$('.cards-beyond').owlCarousel({
		stagePadding: 100,
		center: true,
		loop:true,
		responsive:{
			0:{
				items:1,
				stagePadding: 0,
			},
			577:{
				items:2,
				stagePadding: 0,
			},
			768:{
				items:2,
				stagePadding: 0
			},
			992:{
				items:3,
				stagePadding: 50
			},
			1600:{
				items:4
			}
		}
	});

	$('.person').owlCarousel({
		center: true,
		loop:true,
		onTranslate: animateNextPersonItem,
		responsive:{
			0:{
				items:1
			},
			577:{
				items:1
			},
			768:{
				items:2
			},
			992:{
				items:3
			}
		}
	});
	function animateNextPersonItem(){
		$('.person .owl-item.active').next().animated('fadeInUp');
	};

	// Animations
	if (screen.width >= '1200') {

		// Portfolio animation
		$(".grid-portfolio__item").css('opacity', '0');
		var delay = 100;
		$(".grid-portfolio__item").each( function( index , element){
			setTimeout(function(){
				$(element).animated("fadeInUp");
			}, delay);
			delay += 100;
		});
		$('.portfolio__container button').animated("fadeInUp");

		// Categories animation
		$(".grid-cat__item").css('opacity', '0');

		var waypont = $('.grid-cat').waypoint(function(direction) {
			var delay = 200;
			$(".grid-cat__item").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 200;
			});
		}, {
			offset: '85%'
		});

		// Grid big animation
		$('.grid-big').animated("fadeIn");

		// Portfolio cards animation
		$('.portfolio-cards .portfolio-cards__item').css('opacity', '0');
		$('.portfolio-cards .owl-item').css('opacity', '0');
		$('.portfolio-cards .owl-item.active').animated("fadeInUp");
		var waypont = $('.portfolio-cards').waypoint(function(direction) {
			var delay = 250;
			$(".portfolio-cards__item").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 250;

			});
		}, {
			offset: '85%'
		});

		// Cards beyond animation
		$(".cards-beyond__item").css('opacity', '0');
		var waypont = $('.cards-beyond').waypoint(function(direction) {
			var delay = 250;
			$(".cards-beyond__item").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 250;

			});
		}, {
			offset: '85%'
		});

		// Cards beyond animation
		$(".person .person__item").css('opacity', '0');
		$('.person .owl-item').css('opacity', '0');
		$('.person .owl-item.active').animated("fadeInUp");
		var waypont = $('.person').waypoint(function(direction) {
			var delay = 200;
			$(".person__item").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 200;

			});
		}, {
			offset: '85%'
		});

		// Cards beyond animation
		$(".person .person__item").css('opacity', '0');
		$('.person .owl-item').css('opacity', '0');
		$('.person .owl-item.active').animated("fadeInUp");
		var waypont = $('.person').waypoint(function(direction) {
			var delay = 200;
			$(".person__item").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 200;

			});
		}, {
			offset: '85%'
		});

        var skillsIteration = true;
        $('.pix-progress-bar .pix-progress-bar-count').css('opacity', '0');
        var waypont = $('.pix-progress-bar-section').waypoint(function(direction) {
        	if(skillsIteration) {
                $(".pix-progress-bar").each(function (index, element) {
                    var percent = $(this).data('percent');
                    $(this).animate({
                        width: percent + '%'
                    }, 800);
                    setTimeout(function(){
                        $('.pix-progress-bar .pix-progress-bar-count').css('opacity', '1');
                    }, 800);

                });
                skillsIteration = false;
            }
        }, {
            offset: '85%'
        });

	}






	// Menu
	$( "ul.js-menu li:has(ul.submenu)" ).each(function( index ){
		$(this).addClass('arrow');
	});

	// Add menu
	$('.js-addmenu').on('click',  function() {
		$('.addmenu').fadeToggle(100);
	});




	// Mobile menu
	$('.js-search-toggle').on('click', function(){
		$('.menu-mobile__header img').toggleClass('hide');
		$('.menu-mobile__header .js-mobile-toggle').toggleClass('hide');
		$('.menu-mobile__header .cart-container').toggleClass('hide');
		$('.js-search-toggle').next().toggleClass('show');
		if( $('.js-search-toggle').next().hasClass('show') ){
			$('.js-search-toggle + .search-container > .input-container').html(pix_js_vars.search_form);
            $('.js-search-toggle').removeClass('icon-magnifier').addClass('icon-close');
		} else {
            $('.js-search-toggle').removeClass('icon-close').addClass('icon-magnifier');
		}
	});

	$('.js-mobile-toggle').on('click', function(){
		$( ".menu-mobile__list").toggle();
		$('body').toggleClass('pix-body-fixed');
		function fadeMenu(){
			$( ".menu-mobile__list" ).toggleClass('show');
		}
		setTimeout(fadeMenu, 1);
	});
	$(".pix-mobile-menu-container ul li a[href^='#']").on('click', function(event){
		$('.js-mobile-toggle').click();
	});
	$('.pix-mobile-menu-container ul li:not(.js-mobile-menu)').on('click', function(event){
		event.stopPropagation();
	});
	$('.js-mobile-menu').on('click', function(event){
		event.stopPropagation();
		$('.mobile-submenu:first', this).slideToggle();
		$(this).toggleClass('purple');
	});
	$( ".js-mobile-menu" ).each(function( index ){
		$(this).append($( '<i class="fa fa-angle-down" aria-hidden="true">' ));
	});



	$('#js-search-container>a').on('click', function(){
		$('.search-container > .input-container').html(pix_js_vars.search_form);
        $('.search-container').addClass('show');
        $('.menu-logo').addClass('hide');
        $('.pix-header nav').addClass('hide');
    });

	$('.pix-search-close').on('click', function(){
		$('.search-container').removeClass('show');
		$('.menu-logo').removeClass('hide');
		$('.pix-header nav').removeClass('hide');
	});

    if (screen.width <= '600'){
        // if ($(window).scrollTop() <= 46) {
        //     $('.admin-bar .menu-mobile').css('top', (46-$(window).scrollTop())+'px' );
        // }
        // $(window).scroll(function() {
        //     // Fixed mobile menu
        //     if ($(window).scrollTop() <= 46) {
        //         $('.admin-bar .menu-mobile').css({ top: (46-$(window).scrollTop())+'px' });
        //     } else if($(window).scrollTop() > 46){
        //         $('.menu-mobile').css('top', '0' );
        //     }
        // });

        var previousScroll = 0,
            headerOrgOffset = $('.menu-mobile').offset().top;

        $(window).scroll(function () {
            var currentScroll = $(this).scrollTop();
            //console.log(currentScroll + " and " + previousScroll + " and " + headerOrgOffset);
            if (currentScroll > headerOrgOffset) {
                if (currentScroll >= previousScroll) {
                    $('.menu-mobile.fixed').fadeOut();
                } else {
                    $('.menu-mobile').fadeIn();
                    $('.menu-mobile').addClass('fixed');
                }
            } else {
                $('.menu-mobile').removeClass('fixed');
            }
            previousScroll = currentScroll;
        });
    }

    // Sticky header
    if( $('.pix-header.sticky').hasClass('pix-levels') ){
        var classes = $('.pix-header .pix-header-menu').attr('class');
        var style = getComputedStyle(document.body);
        var height = style.getPropertyValue('--pix-header-height').replace ( /[^\d.]/g, '' );
        if ( $(window).scrollTop() > height ){
            $('.pix-header.sticky').addClass('fixed');
            $('.pix-header .pix-header-menu').removeClass().addClass('pix-header-menu');
        }
        $(window).scroll(function() {
            if ($(window).scrollTop() > height){
                $('.pix-header.sticky').addClass('fixed');
                $('.pix-header .pix-header-menu').removeClass().addClass('pix-header-menu');
            } else {
                $('.pix-header.sticky').removeClass('fixed');
                $('.pix-header .pix-header-menu').removeClass().addClass(classes);
            }
        });
    } else {
        if ( $(window).scrollTop() > 0 ){
            $('.pix-header.sticky').addClass('fixed');
        }
        $(window).scroll(function() {
            if ($(window).scrollTop() > 0){
                $('.pix-header.sticky').addClass('fixed');
            } else {
                $('.pix-header.sticky').removeClass('fixed');
            }
        });
    }

    // Sticky on Up Scroll
    if( $('.pix-header').hasClass('sticky-up') ) {

        var previousScroll = 0,
            headerOrgOffset = $('.pix-header.sticky-up').offset().top;

        $(window).scroll(function () {
            var currentScroll = $(this).scrollTop();
            //console.log(currentScroll + " and " + previousScroll + " and " + headerOrgOffset);
            if (currentScroll > headerOrgOffset) {
                if (currentScroll > previousScroll) {
                    $('.pix-header.sticky-up').fadeOut();
                } else {
                    $('.pix-header.sticky-up').fadeIn();
                    $('.pix-header.sticky-up').addClass('fixed');
                }
            } else {
                $('.pix-header.sticky-up').removeClass('fixed');
            }
            previousScroll = currentScroll;
        });
    }


	// Sidebar menu
	var sideMenu = $('.menu-left');
	var aroundMenu = $('.menu-aroundblock');
	var allow;
	$('#js-slide-menu').on('click', function(){
		aroundMenu.css('opacity', '0');
		sideMenu.css('left', '0px');
		$('body').css('overflow', 'hidden');
	});
	$(document).mouseup(function (event) {
	  if ((sideMenu.has(event.target).length === 0) && ($('.menu-aroundblock').has(event.target).length === 0)){
		aroundMenu.css('opacity', '1');
		sideMenu.css('left', '-500px');
		$('body').css('overflow', 'auto');
		$('.menu-left-submenu').css("visibility", "hidden");
		$('.menu-left-submenu').css("left", "-300px");
		$('.menu-left-submenu').css("box-shadow", "none");
	  }
	});







	$('.news-card-latest__container').owlCarousel({
		margin: 30,
        stagePadding: 15,
		navContainer: '#news-card-latest-nav',
		navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
		dots: false,
		onTranslate: animate_news_card_latest,
		responsive:{
			0:{
				items:1
			},
			768:{
				items:2
			}
		}
	});

	function animate_news_card_latest(){
		if (screen.width >= '1200') {$('.news-card-latest__container .owl-item.active').last().next().animated('fadeInUp');}
	};



	function animate_news_card_long(){
		if (screen.width >= '1200') {$('.news-card-long__carousel .owl-item.active').last().next().animated('fadeInUp');}
	};

	if (screen.width >= '1200') {
		$('.news-card-long__carousel .news-card-long').css('opacity', '0');
		$('.news-card-long__carousel .owl-item.active').animated("fadeInUp");

		var waypont = $('.news-card-long__carousel').waypoint(function(direction) {
			var delay = 200;
			$(".news-card-long").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 250;

			});
		}, {
			offset: '85%'
		});
	}



	function animate_news_card_centered(){
		if (screen.width >= '1200') {$('.news-card-centered__carousel .owl-item.active').last().next().animated('fadeInUp');}
	};

	if (screen.width >= '1200') {
		$('.news-card-centered__carousel .news-card-centered').css('opacity', '0');
		$('.news-card-centered__carousel .owl-item.active').animated("fadeInUp");

		var waypont = $('.news-card-centered__carousel').waypoint(function(direction) {
			var delay = 200;
			$(".news-card-centered").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 250;

			});
		}, {
			offset: '85%'
		});
	}









	$('.news-card-gradient__carousel').owlCarousel({
		onTranslate: animateNextGradientCard,
		responsive:{
			0:{
				items:1
			},
			576:{
				items:2
			},
			768:{
				items:3
			}
		}
	});

	function animateNextGradientCard(){
		if (screen.width >= '1200') {$('.news-card-gradient__carousel .owl-item.active').next().animated('fadeInUp');}
	};

	if (screen.width >= '1200') {
		$('.news-card-gradient__carousel .news-card-gradient').css('opacity', '0');
		$('.news-card-gradient__carousel .owl-item.active').animated("fadeInUp");

		var waypont = $('.news-card-gradient__carousel').waypoint(function(direction) {
			var delay = 200;
			$(".news-card-gradient").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay);
				delay += 250;

			});
		}, {
			offset: '85%'
		});
	}




    if (screen.width >= '1200' && !$('.news-card-feedback-container').hasClass('animation-off') ) {
		$('.news-card-feedback-container').animated("fadeInUp");
	}


	function animate_news_card_people(){
		if (screen.width >= '1200') {$('.news-card-people__carousel .owl-item.active').last().next().animated('fadeInUp');}
	};

	if (screen.width >= '1200' && !$('.news-card-people__carousel').hasClass('animation-off') ) {
		$('.news-card-people__carousel .pix-animation-container').css('opacity', '0');
		$('.news-card-people__carousel .owl-item.active').animated("fadeInUp");

		var waypont1523512351235 = $('.news-card-people__carousel').waypoint(function(direction) {
			var delay1 = 200;
			$(".pix-animation-container").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay1);
				delay1 += 250;

			});
		}, {
			offset: '85%'
		});
	}








	$('.news-card-profile__carousel').owlCarousel({
		onTranslate: animate_news_card_profile,
		responsive:{
			0:{
				items:1
			},
			576:{
				items:2
			},
			768:{
				items:3
			}
		}
	});

	function animate_news_card_profile(){
		if (screen.width >= '1200') {$('.news-card-profile__carousel .owl-item.active').next().animated('fadeInUp');}
	};

	if (screen.width >= '1200') {
		$('.news-card-profile__carousel .news-card-profile').css('opacity', '0');
		$('.news-card-profile__carousel .owl-item.active').animated("fadeInUp");

		var waypont12351261532 = $('.news-card-profile__carousel').waypoint(function(direction) {
			var delay2 = 200;
			$(".news-card-profile").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay2);
				delay2 += 250;

			});
		}, {
			offset: '85%'
		});
	}









	if (screen.width >= '1200') {
		$('.news-card-price__container .news-card-price').css('opacity', '0');

		var waypont12351251 = $('.news-card-price__container').waypoint(function(direction) {
			var delay4 = 200;
			$(".news-card-price").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay4);
				delay4 += 250;

			});
		}, {
			offset: '85%'
		});
	}













	if (screen.width >= '1200') {
		$('.news-item-price-long__container .news-card-price').css('opacity', '0');

		var waypont1234 = $('.news-item-price-long__container').waypoint(function(direction) {
			var delay5 = 200;
			$(".news-item-price-long").each( function( index , element){
				setTimeout(function(){
					$(element).animated("fadeInUp");
				}, delay5);
				delay5 += 250;

			});
		}, {
			offset: '85%'
		});
	}













	$('.news-card-feedback__navigate button.next').on('click', function(){
		$('.news-card-feedback__navigate button.prev').removeClass('disabled');
		var activeBlock = $('.news-card-feedback__user.active');
		if (activeBlock.is(':last-child')) {
			return;
		}
		activeBlock.removeClass('active');
		activeBlock.next().addClass('active');
	});

	$('.news-card-feedback__navigate button.prev').on('click', function(){
		$('.news-card-feedback__navigate button.prev').removeClass('disabled');
		var activeBlock = $('.news-card-feedback__user.active');
		if (activeBlock.is(':first-child')) {
			return;
		}
		activeBlock.removeClass('active');
		activeBlock.prev().addClass('active');
	});









	$('.news-card-price__select a').on('click', function(){
		var priceContainer = $(this).attr('price-container');

		$('.news-card-price__select a').removeClass('active');
		$('.news-card-price__container').hide();

		$(this).addClass('active');
		$('.card-price-container-'+priceContainer).css('display', 'flex');
	});


	$('.news-item-price-long__select a').on('click', function(){
		var priceLongContainer = $(this).attr('price-long-container');

		$('.news-item-price-long__select a').removeClass('active');
		$('.news-item-price-long__container').hide();

		$(this).addClass('active');
		$('.price-long-container-'+priceLongContainer).css('display', 'flex');

	});









	$('.blog-article__carousel').owlCarousel({
		items: 1
	});

	$('.blog-article__related-sidebar-carousel').owlCarousel({
		onTranslate: animateNextRelatedArticleSidebar,
		dots: false,
		navContainer: '#blog-article__related-nav',
		navText : ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
		responsive:{
			0:{
				items:1
			},
			576:{
				items:1
			},
			768:{
				items:2
			}
		}
	});

	function animateNextRelatedArticleSidebar(){
		if (screen.width >= '1200') {$('.blog-article__related-sidebar-carousel .owl-item.active').next().animated('fadeInUp')}
	}
	if (screen.width >= '1200') {
		$('.blog-article__related-sidebar-carousel .owl-item.active').animated("fadeInUp");
	}

	$('.blog-article__related-full-carousel').owlCarousel({
		onTranslate: animateNextRelatedArticleFull,
		dots: false,
		navContainer: '#blog-article__related-nav',
		navText : ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
		responsive:{
			0:{
				items:1
			},
			576:{
				items:2
			},
			768:{
				items:3
			}
		}
	});

	function animateNextRelatedArticleFull(){
		if (screen.width >= '1200') {$('.blog-article__related-full-carousel .owl-item.active').next().animated('fadeInUp')}
	}
	if (screen.width >= '1200') {
		$('.blog-article__related-full-carousel .owl-item.active').animated("fadeInUp");
	}



    var $carousel = $('[data-owl-options]');
    if ($carousel.length) {
        $carousel.each(function(index, el) {
        	var options = $(this).data('owl-options');
        	if(options.onTranslate != null)
            	options.onTranslate = eval(options.onTranslate);
            $(this).owlCarousel(options);
        });
    }



	function fullWidthSection() {

        var windowWidth = $(window).width();
        var widthContainer = $('.home-template > .container, .portfolio-section  > .container , .page-content  > .container').width() + 30 ;

        var fullWidthMargin = windowWidth - widthContainer;
        var fullWidthMarginHalf = fullWidthMargin / 2;

        $('.wpb_column.pix-col-content-right').css('padding-left', fullWidthMarginHalf+15);
        $('.wpb_column.pix-col-content-right').css('padding-right', '15px');

        $('.wpb_column.pix-col-content-left').css('padding-right', fullWidthMarginHalf+15);
        $('.wpb_column.pix-col-content-left').css('padding-left', '15px');

        $('.wpb_column.pix-col-content-center').css('padding-right', fullWidthMarginHalf/2);
        $('.wpb_column.pix-col-content-center').css('padding-left', fullWidthMarginHalf/2);

		var $this_owl = $('div[data-vc-stretch-content=true] .vc_col-sm-12 .owl-carousel');
        $this_owl.css("width", windowWidth);
        $this_owl.owlCarousel().trigger('refresh.owl.carousel');

    }

    fullWidthSection();
    $(window).resize(function() {
       fullWidthSection()
    });



    $(document).ready(function() {
		$('.pix-video-popup').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,

			fixedContentPos: false
		});
	});



	var youtube = document.querySelectorAll( ".pix-video.embed" );

	for (var i = 0; i < youtube.length; i++) {

		var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed;

		youtube[i].addEventListener( "click", function() {

			var iframe = document.createElement( "iframe" );

			iframe.setAttribute( "frameborder", "0" );
			iframe.setAttribute( "allowfullscreen", "" );
			iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );

			this.innerHTML = "";
			this.appendChild( iframe );
		} );
	};



	// isotope grid
	var $grid3 = $('.service__filter').isotope();

	// Filtering
	$('.service__select a').on('click', function() {
		var filterValue = $(this).attr('data-filter');
		$('.service__select a').removeClass('active');
		$(this).addClass('active');
		$grid3.isotope({ filter: filterValue });
	});





	$('.service-page__carousel-1').owlCarousel({
		items: 1,
		autoplay: true
	});

	$('.service-page__carousel-2').owlCarousel({
		items: 1,
		autoplay: true
	});

	$('.service-page__ourworks-carousel').owlCarousel({
		dots: false,
		navContainer: '#blog-article__related-nav',
		navText : ['<i class="fa fa-chevron-left" aria-hidden="true"></i>','<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
		responsive:{
			0:{
				items:1
			},
			576:{
				items:2
			},
			770:{
				items:3
			}
		}
	});


	$(window).on('load', function () {
		var $preloader = $('#page-preloader');
		$preloader.delay(350).fadeOut(800);
	});



	/////////////////////////////////////
	//  Chars Start
	/////////////////////////////////////

    if ($('body').length) {
		$(window).on('scroll', function() {
				var winH = $(window).scrollTop();

				$('.counter-item').waypoint(function() {
					$('.chart').each(function() {
							CharsStart();
					});
				}, {
					offset: '80%'
				});
		});
    }

	function CharsStart() {
		$('.chart').easyPieChart({
				barColor: false,
				trackColor: false,
				scaleColor: false,
				scaleLength: false,
				lineCap: false,
				lineWidth: false,
				size: false,
				animate: 7000,

				onStep: function(from, to, percent) {
						$(this.el).find('.percent').text(Math.round(percent));
				}
		});
	}


	$(document).ready(function() {
		// Test for placeholder support
		$.support.placeholder = (function(){
			var i = document.createElement('input');
			return 'placeholder' in i;
		})();

		// Hide labels by default if placeholders are supported
		if($.support.placeholder) {
			$('.form-label').each(function(){
				$(this).addClass('js-hide-label');
			});

			// Code for adding/removing classes here
			$('.form-group').find('input, textarea').on('keyup blur focus', function(e){

				// Cache our selectors
				var $this = $(this),
					$parent = $this.parent().find("label");

					switch(e.type) {
						case 'keyup': {
							 $parent.toggleClass('js-hide-label', $this.val() == '');
						} break;
						case 'blur': {
							if( $this.val() == '' ) {
						$parent.addClass('js-hide-label');
							} else {
								$parent.removeClass('js-hide-label').addClass('js-unhighlight-label');
							}
						} break;
						case 'focus': {
							if( $this.val() !== '' ) {
								$parent.removeClass('js-unhighlight-label');
							}
						} break;
							default: break;
						}

			});
		}
	});

	// WooCommerce


	// Responsive
	if(screen.width <= 480){
		$('div[class*="vc_custom_"]:has(div.pix-video)').css('padding', '0 !important');
	}

	// if(screen.width >= 1200) {
    //     $('.sidebar').stickySidebar({
	// 		containerSelector: '.col-xl-3',
	// 		innerWrapperSelector: '.widget',
    //         topSpacing: 140,
    //         bottomSpacing: 120,
    //     });
    // }

    //sidebar categories accordion
    $('.pix-li-btn').on('click', function () {
        if($(this).hasClass('pix-active')){
            $(this).removeClass('pix-active');
            $(this).next('ul').slideUp(250);
            $(this).text('+');
        }else{
            $(this).addClass('pix-active');
            $(this).next('ul').slideDown(250);
            $(this).text('-');
        }
    });
    //------------------------------------------------

	cssVars({
	  onlyLegacy: true // default
	});

	// When you click the submit button on a form and there is an invalid field, the browser scrolls to the field.
	// The follow header often covers the field being focused, so this offsets the scroll to below the header.
	var delay = 0;
	var offset = 100;

	document.addEventListener('invalid', function(e){
		$(e.target).addClass("invalid");
		$('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
	}, true);
	document.addEventListener('change', function(e){
		$(e.target).removeClass("invalid")
	}, true);

});