<?php

namespace RxgTheme\Controller;

use Cake\Core\Plugin;
use Cake\Utility\Inflector;
use RxgTheme\Lib\SassCompiler;

/**
 * Class SassCompilerController
 * @package Theme\Controller
 */
class SassCompilerController extends AppController
{

    public function generateCss()
    {
        $required = [
            'plugin',
            'basename',
            'timestamp',
        ];

        $compilerParameters = $this->getRequest()->getParam('compiler_parameters');

        foreach ($required as $key) {
            if (!array_key_exists($key, $compilerParameters)) {
                throw new \Exception(
                    "Missing 'compiler_parameters'  \"$key\"; required keys are: " .
                    implode(", ", $required)
                );
            } elseif (
                strlen($compilerParameters[$key]) > 0 &&
                !preg_match("/^[a-z0-9_]+/i", $compilerParameters[$key])
            ) {
                throw new \Exception(
                    "Named parameter $key with value \"" .
                    $this->getParam('compiler_parameters')[$key] . "\" is potentially unsafe"
                );
            }
        }

        $pluginPath = Plugin::path(Inflector::camelize($compilerParameters['plugin'])); // with trailing slash
        $scssPath = str_replace('-', '/', $compilerParameters['basename']);
        $scssPath = str_replace('__', '-', $scssPath);

        $sassCompiler = new SassCompiler();

        // Here should know the real path to read and right, those infos need to be passed by the url
        // Using same way handled by SassCompilerHelper

        $css = $sassCompiler->generateCss(
            $pluginPath . "webroot" . DS . $scssPath . '.scss',
            $pluginPath . 'webroot/css/compiled/' . $compilerParameters['basename'],
            $compilerParameters['timestamp']
        );

        if($css) {
            return $this->response
                ->withType('text/css')
                ->withStringBody($css);
        }

        exit;

        $compiledName = str_replace('/', '-', trim(trim($scssUrl, '/'), '.scss'));
        // Absolute path for the plugin
        $pluginPath = Plugin::path(Inflector::camelize($pluginName)); // with trailing slash
        // Where Cake synlink the plugin under main webroot
        $pluginSymLinkName = Inflector::underscore($pluginName);
        // When $scssUrl contains Plugin symlink name, remove it
        $scssPathUnderPlugin =  preg_replace('/^\/' . $pluginSymLinkName . '\/(.*)/', '${1}', $scssUrl);
        // Here are absolute path
        $scssIn = $pluginPath . 'webroot' . DS . $scssPathUnderPlugin;
        $cssStyleSourceMap = $pluginPath . 'webroot' . DS . 'css' . DS . 'compiled' . DS . $compiledName;

        // get time stamp from SassCompiler Lib
        $timestamp = SassCompiler::getMaxFileUpdatedTimeWithSourceMap($scssIn, $cssStyleSourceMap);
    }
}