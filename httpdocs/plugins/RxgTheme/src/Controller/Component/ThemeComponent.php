<?php

namespace RxgTheme\Controller\Component;

use Cake\Controller\Component;

class ThemeComponent extends Component
{
    public $components = ['ContentRegions.ContentRegionLoader'];

    public function initialize(array $config)
    {
        parent::initialize($config);

        if ($this->ContentRegionLoader->regionExists('account_approved_html')) {
            $this->ContentRegionLoader->loadRegions(['account_approved_html']);
        }

        if ($this->ContentRegionLoader->regionExists('account_denied_html')) {
            $this->ContentRegionLoader->loadRegions(['account_denied_html']);
        }

        if ($this->ContentRegionLoader->regionExists('account_pending_html')) {
            $this->ContentRegionLoader->loadRegions(['account_pending_html']);
        }
    }

    /**
     * Load Middle End Components
     *
     * @return void
     * @throws \Exception
     */
    public function loadMiddleEndComponents(){

    }
}