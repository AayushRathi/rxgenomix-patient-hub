<?php

namespace RxgTheme\Controller;

use Cake\Event\Event;
use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithVersionRequirement
{
    public function beforeFilter(Event $event)
    {
        return parent::beforeFilter($event);

        $this->getEventManager()->off($this->Csrf);
    }

    public static function  getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'RxgTheme', 'RxgTheme');
    }

    public static function getPluginMinimum24WatchVersion()
    {
        return '6.8.0';
    }
}
