<?php
namespace RxgTheme\Controller;

class HomePageController extends AppController
{
    public function initialize()
    {
        if (!isset($this->ContentRegionLoader)) {
            $this->loadComponent('ContentRegions.ContentRegionLoader');
        }

        parent::initialize();
    }

    public function home()
    {
        if (!isset($this->Auth)) {
            $this->loadComponent('ContentRegions.ContentRegionLoader');
        }

        $this->loadComponent('CakeDC/Users.UsersAuth');
        $this->loadModel('RxgUsers.RxgUsers');

        $userId = $this->Auth->user('id');
        if ($userId) {
            // stay a while and listen!
            $userWithProvider = $this->RxgUsers->get($userId, [
                'contain' => [
                    'Provider',
                    'LabOrderInitiatorTypes'
                ]
            ]);
            $this->Auth->setUser($userWithProvider->toArray());
            $this->set("providerStatus", $userWithProvider->provider->provider_status_id);
            $this->set("userWithProvider", $userWithProvider);
            $this->redirectWithDefault([
                    'plugin' => 'LabOrders',
                    'controller' => 'LabOrders',
                    'prefix' => false,
                    'action' => 'index'
                ]
            );
        } else {
            $url = [
                'plugin' => 'RxgUsers',
                'controller' => 'RxgUsers',
                'prefix' => false,
                'action' => 'login'
            ];
            if ($this->getRequest()->getQuery('redirect')) {
              $url['redirect'] = $this->getRequest()->getQuery('redirect');
            }
            $this->redirectWithDefault($url);
        }
    }

    public function privacy() {
        if ($this->ContentRegionLoader->regionExists('privacy')) {
            $this->ContentRegionLoader->loadRegions(['privacy']);
        }
    }

    public function consent(){
        if ($this->ContentRegionLoader->regionExists('consent')) {
            $this->ContentRegionLoader->loadRegions(['consent']);
        }
    }

    public function tos(){
        if ($this->ContentRegionLoader->regionExists('tos')) {
            $this->ContentRegionLoader->loadRegions(['tos']);
        }
    }

    public function noticeOfPrivacy(){
        if ($this->ContentRegionLoader->regionExists('npp')) {
            $this->ContentRegionLoader->loadRegions(['npp']);
        }
    }
}
