<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <meta charset="UTF-8">
    <?php
    $user = $this->request->getSession()->read('Auth.User');
    ?>

    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?php
    echo $this->element('Lib24watch.metadata');
    echo $this->element('Lib24watch.site_settings', [
        'siteName' => $siteName ?? null,
        'siteSettings' => $siteSettings ?? null
    ]);
    ?>

    <meta name='robots' content='noindex,follow'/>
    <link rel='dns-prefetch' href='//s.w.org'/>
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11.2.0\/svg\/","svgExt":".svg","source":{"concatemoji":"\/rxg_theme\/js\/wp-emoji-release.min.js"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

    <?= $this->Html->css('RxgTheme.bootstrap/bootstrap.css')?>
    <?= $this->Html->css('RxgTheme.nordis/block-library/style.min.css', ['id' => 'wp-block-library-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/style.css', ['id' => 'style-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/pixtheme/fonts.css', ['id' => 'pixtheme-fonts-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/grid.min.css', ['id' => 'grid-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/fonts/font-awesome.min.css', ['id' => 'font-awesome-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/fonts/simple-line-icons.css', ['id' => 'simple-line-icons-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/pixtheme/main.css', ['id' => 'pixtheme-main-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/pixtheme/responsive.css', ['id' => 'pixtheme-responsive-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/owl-carousel/owl.carousel.min.css', ['id' => 'owl.carousel-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/owl-carousel/owl.theme.default.min.css', ['id' => 'owl.theme.default-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/animate.css', ['id' => 'animate-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/magnific-popup.css', ['id' => 'magnific-popup-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.nordis/pixtheme/dynamic-styles.css', ['id' => 'pixtheme-dynamic-styles-css', 'media' => 'all', 'type' => 'text/css']) ?>
    <?= $this->Html->css('RxgTheme.jquery/jquery-ui.min')?>

    <?= $this->Html->script('RxgTheme.jquery.min.js') ?>
    <?= $this->Html->script('RxgTheme.jquery-ui.min.js')?>
    <?= $this->Html->script('RxgTheme.jquery-migrate.min.js') ?>
    <?= $this->Html->script('RxgTheme.bootstrap.min.js') ?>

    <style type="text/css">.recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }</style>

    <?php
    // Custom CSS
        echo $this->Html->css(
            $this->SassCompiler->generateCss('/scss/style.scss', 'RxgTheme')
        );
    ?>

    <?= $this->Html->css("Lib24watch.bootstrap-datetimepicker"); ?>
    <?= $this->fetch('extra-css') ?>

</head>

<body class="home blog pix-square pix-rounded-buttons <?= ($user) ? 'logged-in' : 'logged-out' ?>"
      data-scrolling-animations="true">


<!-- Loader -->
<div id="page-preloader">
    <div class="pix-pulse"></div>
</div>
<!-- Loader end -->


<div class="wrapper layout animated-css page-layout-normal woo-layout-default">


    <header class="menu-mobile">
        <div class="row">
            <div class="col-12">
                <div class="menu-mobile__header white">

                    <?= $this->Html->link(
                        $this->Html->image('RxgTheme.rxg-logo.svg', ['class' => 'normal-logo']),
                            ['plugin' => 'RxgTheme', 'controller' => 'HomePage', 'action' => 'home'],
                            ['escape' => false, 'class' => 'navbar-brand scroll']
                    ) ?>

                    <div class="hamburger--4 js-mobile-toggle" onclick="this.classList.toggle('is-active');">
                        <div class="bar--1"></div>
                        <div class="bar--2"></div>
                        <div class="bar--3"></div>
                    </div>


<!--                    <i class="icon-magnifier js-search-toggle"></i>-->
<!--                    <div class="search-container">-->
<!--                        <div class="input-container"></div>-->
<!--                    </div>-->

                </div>
                <div class="menu-mobile__list">
                    <div class="pix-mobile-menu-container">
                        <ul id="mobile-menu">
                            <?php if ($user) { ?>
                                <?php if ($providerStatus != 3) { ?>
                                    <li class="profile">
                                        <a href="/lab_orders/lab_orders/select" title="Lab Orders">
                                            <i class="fas fa fa-plus"></i> Create New Order
                                        </a>
                                    </li>
                                    <li class="profile">
                                        <a href="/lab_orders" title="Lab Orders">
                                            <i class="fas fa fa-flask"></i> Lab Orders
                                        </a>
                                    </li>
                                <?php } ?>
                                <li class="profile">
                                    <a href="/profile" title="Profile">
                                        <i class="fas fa fa-user"></i> Profile
                                    </a>
                                </li>
                                <li class="log-out">
                                    <a href="/logout" title="Logout">
                                        <i class="fas fa fa-sign-out"></i> Logout
                                    </a>
                                </li>

                            <?php } else { ?>

                                <li class="log-in">
                                    <a href="/login" title="Login">
                                        <i class="fas fa fa-sign-in"></i> Login
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                    <div class="overlay"></div>
                </div>
            </div>
        </div>
    </header>
    <header class="pix-header white sticky">


<?php
    // If a backend user is logged in as another user, show this message
    if (isset($user['isLoggedInAs']) && (bool)$user['isLoggedInAs']) {
?>
        <div class="alert alert-secondary alert-logged-in">
            <div class="container">
                <div style="display:grid;">
                    <p>Logged in as <strong><?=$user['first_name'];?> <?=$user['last_name'];?></strong></p>
                </div>
                <div class="logout">
                    <p><a href="/logout" title="Logout"><strong>Logout</strong></a></p>
                </div>
            </div>
        </div>
<?php
    }
?>
        <div class="container">

            <div class="menu-logo">
                <?= $this->Html->link(
                    $this->Html->image('RxgTheme.rxg-logo.svg', ['class' => 'pix-header-logo']) .
                    $this->Html->image('RxgTheme.rxg-logo.svg', ['class' => 'pix-header-logo pix-sticky']),
                    'https://rxgenomix.com',
                    ['escape' => false, 'class' => 'navbar-brand scroll', 'style' => 'width:146px;', 'target' => '_blank']
                ) ?>
            </div>

            <nav class="pix-main-menu ">
                <ul id="menu-primary" class="nav navbar-nav ">
                    <?php if ($user) { ?>
                        <?php if ($providerStatus != 3) { ?>
                            <li class="profile">
                                <a href="/lab_orders/lab_orders/select" title="Lab Orders" class="btn btn-primary">
                                    <i class="fas fa fa-plus"></i> Create New Order
                                </a>
                            </li>
                            <li class="profile">
                                <a href="/lab_orders" title="Lab Orders">
                                    <i class="fas fa fa-flask"></i> Lab Orders
                                </a>
                            </li>
                        <?php } ?>
                        <li class="profile">
                            <a href="/profile" title="Profile">
                                <i class="fas fa fa-user"></i> Profile
                            </a>
                        </li>
                        <?php if (!isset($user['isLoggedInAs'])) { ?>
                        <li class="log-out">
                            <a href="/logout" title="Logout">
                                <i class="fas fa fa-sign-out"></i> Logout
                            </a>
                        </li>
                        <?php } ?>

                    <?php } else { ?>

                        <li class="log-in">
                            <a href="/login" title="Login">
                                <i class="fas fa fa-sign-in"></i> Login
                            </a>
                        </li>

                    <?php } ?>
                </ul>
            </nav>

<!--            <nav>-->
<!--                <ul class="main-menu-elements">-->
<!---->
<!--                    <li id="js-search-container" class="search">-->
<!--                        <a><i class="icon-magnifier"></i></a>-->
<!--                    </li>-->
<!---->
<!--                    <li class="cart">-->
<!--                    </li>-->
<!---->
<!--                </ul>-->
<!--            </nav>-->
<!--            <div class="search-container">-->
<!--                <div class="input-container"></div>-->
<!--                <a class="pix-search-close"><i class="icon-close"></i></a>-->
<!--            </div>-->
        </div>
    </header>

    <!-- ========================== -->
    <!-- Top header -->
    <!-- ========================== -->
    <div class="custom-header">
        <span class="vc_row-overlay"></span>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="pix-header-tab-box text-white-color">


                        <div class="pix-header-title text-">
                            <h1 class="pix-h1">
                                <?php
                                if (isset($hideTitle) && $hideTitle) {
                                    // nothing
                                } else {
                                    echo $this->fetch('title');
                                }
                                ?>
                            </h1>

                        </div>

                        <div class="pix-header-breadcrumbs text-">
                            <?= $this->Breadcrumbs->render(
                                    ['class' => 'pix-breadcrumbs-path inline-list'],
                                    ['separator' => ' | ']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div><!--./top header -->


    <section>
        <div class="container">
            <div class="flash-messages">
                <?php

                if(!isset($suppressThemeFlashMessages)){
                    echo $this->Flash->render();
                }

                if (isset($hasSeenStatusMessage) && $hasSeenStatusMessage == 0) {
                    if($providerStatus === 1) { ?>

                        <div class="portal-await-approval">
                            <?php
                            if ($this->ContentRegionLoader->regionExists('account_pending_html')) {
                                echo $this->ContentRegionLoader->getRegion('account_pending_html');
                            } ?>
                        </div>

                    <?php } elseif($providerStatus === 2) { ?>

                        <div class="portal-approved">
                            <?php
                            if ($this->ContentRegionLoader->regionExists('account_approved_html')) {
                                echo $this->ContentRegionLoader->getRegion('account_approved_html');
                            } ?>
                        </div>

                    <?php } elseif ($providerStatus === 3) { ?>

                        <div class="portal-denied">
                            <?php
                            if ($this->ContentRegionLoader->regionExists('account_denied_html')) {
                                echo $this->ContentRegionLoader->getRegion('account_denied_html');
                            } ?>
                        </div>

                    <?php
                    }
                } ?>
            </div>
            <?= $this->fetch('content') ?>
        </div>
    </section>


    <!-- Footer section -->
    <footer class="pix-footer">
        <div class="container">


            <div class="pix-footer__bottom">
                <div class="footer-copyright">
                    &copy; <?= date('Y'); ?> <span>All Rights Reserved</span> |
                    <?= $this->Html->link(
                        'Privacy Policy',
                        'https://www.rxgenomix.com/privacy',
                        [ 'target' => '_blank' ]
                    ) ?> |
                    <?= $this->Html->link(
                        'Informed Consent',
                        'https://www.rxgenomix.com/informed-consent',
                        [ 'target' => '_blank' ]
                    ) ?> |
                    <?= $this->Html->link(
                        'Terms of Service',
                        'https://www.rxgenomix.com/terms',
                        [ 'target' => '_blank' ]
                    ) ?> |
                    <?= $this->Html->link(
                        'Notice of Privacy Practices',
                        'https://www.rxgenomix.com/privacy-practices',
                        [ 'target' => '_blank' ]
                    ) ?>
                </div>
                <div class="footer-created_by">
                    <a href="https://compliancy-group.com/hipaa-compliance-verification" target="_blank"><img src="https://compliancy-group.com/wp-content/uploads/2016/08/HIPAA-Compliance-Verification.png" alt="HIPAA Compliance Verification" class="hipaa-compliance" /></a>
                </div>
            </div>

        </div>
    </footer>


</div>

<?= $this->Html->script('RxgTheme.css-vars-ponyfill.min.js') ?>
<?= $this->Html->script('RxgTheme.magnific-popup/jquery.magnific-popup.min.js') ?>
<?= $this->Html->script('RxgTheme.animate/animate-css.js') ?>
<?= $this->Html->script('RxgTheme.owl-carousel/owl.carousel.min.js') ?>
<?= $this->Html->script('RxgTheme.isotope/isotope.pkgd.min.js') ?>
<?= $this->Html->script('RxgTheme.waypoints/waypoints.min.js') ?>
<?= $this->Html->script('RxgTheme.easypiechart/jquery.easypiechart.min.js') ?>
<?= $this->Html->script('RxgTheme.sticky-sidebar/jquery.sticky-sidebar.min.js') ?>

<script type='text/javascript'>
    /* <![CDATA[ */
    var pix_js_vars = {"search_form": "\r\n\t<form method=\"get\" id=\"searchform\" class=\"searchform\" action=\"/\">\r\n\t\t<div>\r\n\t\t\t<input type=\"text\" placeholder=\"Search\" value=\"\" name=\"s\" id=\"search\">\r\n\t\t\t<input type=\"submit\" id=\"searchsubmit\" value=\"\">\r\n\t\t<\/div>\r\n\t<\/form>"};
    /* ]]> */
</script>

<?= $this->Html->script('RxgTheme.common.js') ?>
<?= $this->Html->script('RxgTheme.wp-embed.min.js') ?>

<?= $this->fetch('script') ?>
<?= $this->fetch('extra-scripts') ?>
<?= $this->fetch('modals'); ?>

</body>
</html>
