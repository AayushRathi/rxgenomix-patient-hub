<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
    <head>
        <title>Email/html</title>
    </head>
    <body>

    <div style="background-color:rgb(0, 65, 129);box-shadow:rgba(0, 0, 0, 0.3) 0px 1px 5px 0px;box-sizing:border-box;color:rgb(0, 47, 102);display:block;height:82px;padding-bottom:0px;padding-left:15px;padding-right:15px;padding-top:13px;width:100%;">
        <img src="<?php echo \Cake\Core\Configure::read('App.fullBaseUrl');?>/rxg_theme/img/rxg-logo.png" width="170"/>

    </div>

    <div style="font-family: Roboto, sans-serif; padding: 30px 50px 50px 50px;color:#333;">
        <?= $this->fetch('content') ?>
    </div>

    <div style="font-family: Roboto, sans-serif;width:100%;padding:20px;background:#eee;color:#333;">
        <?= \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('email_legal_footer', 'RxgTheme', 'Copyright © ' . date('Y') . ' All Rights Reserved') ?>
    </div>

    </body>
</html>
