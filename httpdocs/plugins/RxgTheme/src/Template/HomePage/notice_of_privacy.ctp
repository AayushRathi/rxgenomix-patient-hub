<?php
    $this->assign('title', "RxGenomix Notice of Privacy Practices");

    if ($this->ContentRegionLoader->regionExists('npp')) {
        echo $this->ContentRegionLoader->getRegion('npp');
    }
?>
