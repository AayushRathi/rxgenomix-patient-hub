<?php
    $this->assign('title', "RxGenomix Terms of Service");

    if ($this->ContentRegionLoader->regionExists('tos')) {
        echo $this->ContentRegionLoader->getRegion('tos');
    }
?>