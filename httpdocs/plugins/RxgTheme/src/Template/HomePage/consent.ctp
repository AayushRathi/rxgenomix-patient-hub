<?php
    $this->assign('title', "RxGenomix Informed Consent");

    if ($this->ContentRegionLoader->regionExists('consent')) {
        echo $this->ContentRegionLoader->getRegion('consent');
    }
?>
