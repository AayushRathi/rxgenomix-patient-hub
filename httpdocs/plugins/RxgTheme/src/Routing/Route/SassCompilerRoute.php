<?php

namespace RxgTheme\Routing\Route;

use Cake\Routing\Route\Route;

/**
 * Class SassCompilerRoute
 * @package Theme\Routing\Route
 */
class SassCompilerRoute extends Route
{
    /**
     * @param string $url
     * @param string $method
     * @return array|bool
     */
    public function parse($url, $method = '')
    {
        $retval = false;

        $regex =
            "/^" .
            "\/(?<plugin>[0-9a-z_]+)" .
            "\/css" .
            "\/compiled" .
            "\/(?<basename>[0-9a-z_-]+)" .
            "-(?<timestamp>[0-9]+)" .
            "\.css$/i";

        if (preg_match($regex, $url, $match)) {
            $retval = [
                'plugin' => 'RxgTheme',
                'controller' => 'SassCompiler',
                'action' => 'generateCss',
                'pass' => []
            ];

            foreach ($match as $key => $value) {
                if (!is_numeric($key)) {
                    $retval['compiler_parameters'][$key] = $value;
                }
            }
        }

        return $retval;
    }
}
