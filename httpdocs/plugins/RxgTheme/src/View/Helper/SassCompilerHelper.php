<?php
namespace RxgTheme\View\Helper;

use Cake\Core\Plugin;
use Cake\Utility\Inflector;
use Cake\View\Helper;
use RxgTheme\Lib\SassCompiler;

class SassCompilerHelper extends Helper
{
    public $helpers = array(
        'Url'
    );

    /**
     * @param $scssUrl
     * @param $pluginName
     * @return mixed
     */
    public function generateCss($scssUrl, $pluginName = null)
    {
        // TODO::handle none plugin name (directly under webroot)

        // Absolute path for the plugin
        $pluginPath = Plugin::path(Inflector::camelize($pluginName)); // with trailing slash

        // Where Cake synlink the plugin under main webroot
        $pluginSymLinkName = Inflector::underscore($pluginName);

        // When $scssUrl contains Plugin symlink name, remove it
        $scssPathUnderPlugin =  preg_replace('/^\/' . $pluginSymLinkName . '\/(.*)/', '${1}', $scssUrl);

        // Compiled name for the target css
        // To make the name can be reverted back by Compiler controller, replace '-' to '__' first.
        $compiledName = trim($scssPathUnderPlugin, '/');
        $compiledName = str_replace('-', '__', $compiledName);
        $compiledName = str_replace('/', '-', $compiledName);
        $compiledName = preg_replace('/(.*)\.scss$/', '${1}', $compiledName);

        // Here are absolute path
        $scssIn = $pluginPath . 'webroot' . DS . $scssPathUnderPlugin;
        $cssStyleSourceMapWithoutExt = $pluginPath . 'webroot' . DS . 'css' . DS . 'compiled' . DS . $compiledName;

        // get time stamp from SassCompiler Lib
        $cssStyleSourceMap = SassCompiler::getSourceMapFilePath($cssStyleSourceMapWithoutExt);
        $timestamp = SassCompiler::getMaxFileUpdatedTimeWithSourceMap($scssIn, $cssStyleSourceMap);

        $url =  DS . $pluginSymLinkName . DS . "css" . DS . "compiled" . DS . $compiledName . "-" . $timestamp . '.css';

        return $this->Url->build($url);
    }
}