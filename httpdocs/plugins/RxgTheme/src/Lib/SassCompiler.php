<?php
/**
 * Created by PhpStorm.
 * User: dcai
 * Date: 6/20/2018
 * Time: 9:05 PM
 */

namespace RxgTheme\Lib;

use Leafo\ScssPhp\Compiler;

class SassCompiler extends Compiler
{
    protected $filemtime = 0;

    protected function importFile($path, $out)
    {
        $this->filemtime = max($this->filemtime, filemtime($path));
        parent::importFile($path, $out);
    }

    // TODO :: read from source map to determine most recent modified plus the $scssIn
    public function getMaxFilemtime($cssOut)
    {
        $sourceMapWriteTo = $this->getSourceMapWriteTo($cssOut);
        if (!is_readable($sourceMapWriteTo)) {
            throw new \Exception('Unable to read source map ' . $sourceMapWriteTo);
        }
    }

    protected function getSourceMapWriteTo($cssOut)
    {
        return $cssOut . ".map";
    }

    /**
     * @param $scssIn
     * @param $cssOut css file full path. The name should have max updated timestamp appended.
     * @return int
     */
    public function generateCss($scssIn, $cssOutWithoutExtension, $timestampVerification =  null)
    {
        if (!file_exists($scssIn)) {
            throw new \Exception('Unable to locate ' . $scssIn);
        }
        if (!is_readable($scssIn)) {
            throw new \Exception('Unable to read ' . $scssIn);
        }

        $cssStyleSourceMap = $this->getSourceMapFilePath($cssOutWithoutExtension);

        $maxFileUpdatedTime = $this->getMaxFileUpdatedTimeWithSourceMap($scssIn, $cssStyleSourceMap);

        if(!is_null($timestampVerification) && $timestampVerification != $maxFileUpdatedTime) {
            throw new \Exception('Timestamp is wrong for the requested CSS');
        }

        $cssOut = $cssOutWithoutExtension . '-' . $maxFileUpdatedTime . '.css';

        //TODO::pass previous file, and delete after success creation of new css
        if(!file_exists($cssOut)) {
            return $this->reCompileCss($scssIn, $cssOut, $cssStyleSourceMap);
        }
    }

    public static function getSourceMapFilePath($cssOutWithoutExtension)
    {
        return $cssOutWithoutExtension . '.css.map';
    }

    /**
     * @param $scssIn
     * @param $cssStyleSourceMap
     * @return int
     */
    public static function getMaxFileUpdatedTimeWithSourceMap($scssIn, $cssStyleSourceMap)
    {
        $maxFileUpdatedTime = filemtime($scssIn);

        if (file_exists($cssStyleSourceMap)) {
            $jsonContent = file_get_contents($cssStyleSourceMap);
            $sourceMapJson = json_decode($jsonContent);
            foreach ($sourceMapJson->sources as $source) {
                $fullPathSource = dirname($scssIn) . DS . basename($source);
                if (file_exists($fullPathSource)) {
                    $maxFileUpdatedTime = max($maxFileUpdatedTime, filemtime($fullPathSource));
                } else {
                    // if source file get deleted, the main scss timestamp will reflect the change.
                }
            }
        }

        return $maxFileUpdatedTime;
    }

    /**
     * @param $scssIn
     * @param $cssOut css file full path. The name should have max updated timestamp appended.
     * @return int
     */
    public function reCompileCss($scssIn, $cssOut, $cssStyleSourceMap)
    {
        $this->setImportPaths(dirname($scssIn));

        $this->setSourceMap(
            new SourceMapGenerator([
                'sourceMapWriteTo'  => $cssStyleSourceMap,
                'sourceMapURL'      => basename($cssStyleSourceMap),
                'sourceMapFilename' => $cssOut,  // url location of .css file
                'sourceMapBasepath' => dirname($scssIn),  // difference between file & url locations, removed from ALL source files in .map
                'sourceMapRootpath' => '/theme/scss/',
                'sourceRoot'        => '/'
            ])
        );

        $out = $this->compile(file_get_contents($scssIn));
        file_put_contents($cssOut, $out);

        return $out;
    }


}