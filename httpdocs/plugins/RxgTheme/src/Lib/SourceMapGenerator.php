<?php
/**
 * Created by PhpStorm.
 * User: dcai
 * Date: 6/21/2018
 * Time: 1:11 PM
 */

namespace RxgTheme\Lib;

use Leafo\ScssPhp\SourceMap\SourceMapGenerator as BaseSourceMapGenerator;
use Leafo\ScssPhp\SourceMap\Base64VLQEncoder;
use Leafo\ScssPhp\Exception\CompilerException;


class SourceMapGenerator extends BaseSourceMapGenerator
{
    private $options;

    public function __construct(array $options = [])
    {
        $this->options = array_merge($this->defaultOptions, $options);
        $this->encoder = new Base64VLQEncoder();
    }

    protected function normalizeFilename($filename)
    {
        $filename = $this->fixWindowsPath($filename);
        $rootpath = $this->options['sourceMapRootpath'];
        $basePath = $this->options['sourceMapBasepath'];

        // "Trim" the 'sourceMapBasepath' from the output filename.
        if (strpos($filename, $basePath) === 0) {
            $filename = substr($filename, strlen($basePath));
        }

        // Remove extra leading path separators.
        if (strpos($filename, '\\') === 0 || strpos($filename, '/') === 0) {
            $filename = substr($filename, 1);
        }

        return $rootpath . $filename;
    }

    public function saveMap($content)
    {
        $file = $this->options['sourceMapWriteTo'];
        $dir  = dirname($file);

        // directory does not exist
        if (! is_dir($dir)) {
            // FIXME: create the dir automatically?
            throw new CompilerException(sprintf('The directory "%s" does not exist. Cannot save the source map.', $dir));
        }

        // FIXME: proper saving, with dir write check!
        if (file_put_contents($file, $content) === false) {
            throw new CompilerException(sprintf('Cannot save the source map to "%s"', $file));
        }

        return $this->options['sourceMapURL'];
    }
}