<?php
use Migrations\AbstractMigration;

class RemoveHealthCareClientGroupAsRequired extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('providers')->changeColumn('health_care_client_group_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->update();
    }
}
