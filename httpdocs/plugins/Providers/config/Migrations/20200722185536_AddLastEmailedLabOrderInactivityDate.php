<?php
use Migrations\AbstractMigration;

class AddLastEmailedLabOrderInactivityDate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('providers')
            ->addColumn('last_emailed_lab_order_activity_date', 'date', [
                'default' => null,
                'null' => true
            ])
            ->update();
    }
}
