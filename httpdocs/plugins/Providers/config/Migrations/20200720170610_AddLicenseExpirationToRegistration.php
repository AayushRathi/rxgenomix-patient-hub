<?php
use Migrations\AbstractMigration;

class AddLicenseExpirationToRegistration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('providers')
            ->addColumn('license_expiration_date', 'date', [
                'default' => null,
                'null' => true,
            ])
            ->update();
    }
}
