<a href="javascript:void(0)"><span class="fa fa-user-md"></span><span class="xn-text"><?php echo h($lib24watchModules['Providers']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('providers_view', 'Providers')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Providers",
                [
                    'plugin' => 'Providers',
                    'controller' => 'Providers',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Providers'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('providers_edit', 'Providers')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Provider",
                [
                    'plugin' => 'Providers',
                    'controller' => 'Providers',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Provider'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>