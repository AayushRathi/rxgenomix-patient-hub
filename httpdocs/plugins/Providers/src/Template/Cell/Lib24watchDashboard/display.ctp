<?php
if ($canApprove) {
?>
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title-box">
                    <h3>Account Approval Queue</h3>
                    <span>the following provider accounts are awaiting approval by an administrator</span>
                </div>
                <ul style="margin-top: 2px;" class="panel-controls">
                    <li><a class="panel-collapse" href="#" title="Collapse"><span class="fa fa-angle-down"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
                <?php
                if ($providers->count() > 0) {
                ?>
                    <div class="table-responsive">
                        <table class="table" role="grid">
                            <thead>
                                <tr>
                                    <th class="">Provider Name</th>
                                    <th class="">Type</th>
                                    <th class="">Date</th>
                                    <th class="">License ID</th>
                                    <th class="">State</th>
                                    <th class="">NABPE-P ID</th>
                                    <th class=""></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($providers as $provider) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            if ($provider->user) {
                                                echo $provider->user->first_name . ' ' . $provider->user->last_name;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $provider->type->title;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo lib24watchDate('%m/%d/%Y', $provider->created);
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $provider->provider_license_id;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $provider->provider_lib24watch_state_abbr;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $provider->nabpep_id;
                                            ?>
                                        </td>
                                        <td class="actions">
                                            <?php
                                            echo $this->Html->link(
                                                '<span class="fa fa-search"></span>',
                                                [
                                                    'plugin' => 'Providers',
                                                    'controller' => 'Providers',
                                                    'action' => 'edit',
                                                    'prefix' => 'admin',
                                                    $provider->id
                                                ],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-default',
                                                    'title' => 'View'
                                                ]
                                            );
                                            echo $this->Html->link(
                                                '<span class="fa fa-check-circle-o"></span>',
                                                [
                                                    'plugin' => 'Providers',
                                                    'controller' => 'Providers',
                                                    'action' => 'approve_provider',
                                                    'prefix' => 'admin',
                                                    $provider->id,
                                                    'fromDashboard' => 1
                                                ],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-success',
                                                    'title' => 'Approve'
                                                ]
                                            );
                                            echo $this->Html->link(
                                                '<span class="fa fa-times-circle-o"></span>',
                                                [
                                                    'plugin' => 'Providers',
                                                    'controller' => 'Providers',
                                                    'action' => 'deny_provider',
                                                    'prefix' => 'admin',
                                                    $provider->id,
                                                    'fromDashboard' => 1
                                                ],
                                                [
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-danger',
                                                    'title' => 'Deny'
                                                ]
                                            );
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                        echo $this->cell(
                            "Lib24watch.Panel::footer",
                            [
                                "submit" => false,
                                "showPagination" => true,
                                "extraButtons" => false
                            ]
                        );
                        ?>
                    </div>
                <?php
                } else {
                    ?>
                    There are currently no providers pending approval.
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
<?php
}
?>

