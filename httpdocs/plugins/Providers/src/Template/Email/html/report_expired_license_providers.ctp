<?php
if (!empty($providerEntities)) {
?>
<ul>
<?php
    foreach ($providerEntities as $provider) {
?>
    <li>It looks like the credentials for <?=$provider->name;?> may have expired. Time to check in with them. They are past the last expiration date (expired on <?=lib24watchDate('shortDateFormat', $provider->license_expiration_date);?>) entered and haven't been updated.</li>


<?php
    }
?>
</ul>
<?php
} else {
?>
    <p>No expired providers</p>
<?php } ?>

