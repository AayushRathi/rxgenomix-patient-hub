<?php
if (!empty($providerEntities)) {
?>
<ul>
<?php
    foreach ($providerEntities as $provider) {
?>
    <li><?=$provider->name;?> has not logged in since <?=lib24watchDate('shortDateFormat', $provider->last_logged_in);?></li>
<?php
    }
?>
</ul>
<?php
} else {
?>
    <p>No inactive providers</p>
<?php } ?>

