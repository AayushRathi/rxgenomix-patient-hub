<?php 
if (isset($cadenceNotice)) {
    echo '<p><strong>' . $cadenceNotice . '</strong></p>';
} 
?>

<p>Results are now available for Lab Order #<?= $labOrder->id ?>. Please log into hub.rxgenomix.com to view the report.<br>
If you have any questions about the Hub, please contact us at <a href="mailto:support@rxgenomix.com">support@rxgenomix.com</a>.</p>

