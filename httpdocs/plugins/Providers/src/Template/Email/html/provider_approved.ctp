Congratulations! Your registration for the RxGenomix Hub has been approved by RxGenomix. You can now create new lab
orders, access existing orders, manage your account and start putting the power of pharmacogenomics to work for everyone
who counts on you. If you need help getting started or have any questions about the Hub, please contact us
at <a href="mailto:support@rxgenomix.com">support@rxgenomix.com</a>.