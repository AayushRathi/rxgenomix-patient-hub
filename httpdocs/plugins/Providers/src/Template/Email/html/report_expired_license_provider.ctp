<?php if (isset($provider)) {?>
<p>It looks like the credentials for <?=$provider->name;?> may have expired. Time to check in with them. They are past the last expiration date (expired on <?=lib24watchDate('shortDateFormat', $provider->license_expiration_date);?>) entered and haven't been updated.</p>

<?php
} else {
?>
    <p>No expired providers</p>
<?php } ?>

