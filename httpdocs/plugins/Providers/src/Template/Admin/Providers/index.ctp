<?php
$pluginTitle = $lib24watchModules['Providers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Providers';
$this->Breadcrumbs->add($pageHeading);
$this->Html->script('/lib24watch/js/plugins/bootstrap/bootstrap-select.js', ['once' => true,'block' => 'extra-scripts']);

$canLoginAs = $this->Lib24watchPermissions->hasPermission('login_as', 'Providers');
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Providers',
                'items' => [
                    [
                        'name' => 'provider_type_id',
                        'type' => 'select',
                        'empty' => true,
                        'label' => 'Provider Type',
                        'options' => $providerTypes,
                        'class' => 'form-control selectpicker'
                    ],
                    [
                        'name' => 'user_first_name',
                        'field' => 'first_name',
                        'alias' => 'Users.',
                        'type' => 'text',
                        'label' => 'First Name'
                    ],
                    [
                        'name' => 'user_last_name',
                        'field' => 'last_name',
                        'alias' => 'Users.',
                        'type' => 'text',
                        'label' => 'Last Name'
                    ],
                    [
                        'name' => 'npi_number',
                        'type' => 'text',
                        'label' => 'NPI #'
                    ],
                    [
                        'name' => 'user_email',
                        'field' => 'email',
                        'alias' => 'Users.',
                        'type' => 'text',
                        'label' => 'Email'
                    ],
                    [
                        'name' => 'is_active',
                        'type' => 'select',
                        'options' => ['' => 'Both', '1' => 'Active', '0' => 'Inactive'],
                        'label' => 'Active'
                    ]
                ],
                $providersQuery,
                $this
            ]);
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'Providers',
                            'controller' => 'Providers',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Provider'
                        ]
                    )
                ]
            );
            ?>
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($providersQuery->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'provider_type_id',
                                        'label' => 'Provider Type',
                                        'type' => 'lookup',
                                        'list' => $providerTypes
                                    ],
                                    [
                                        'name' => 'user.last_name',
                                        'label' => 'Last Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'user.first_name',
                                        'label' => 'First Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'email',
                                        'label' => 'Email',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'npi_number',
                                        'label' => 'NPI #',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'provider_status_id',
                                        'label' => 'Provider Status',
                                        'type' => 'lookup',
                                        'list' => $providerStatuses
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) use (
                                            $canLoginAs,
                                            $isImmutable
                                        ) {
                                            $buttons = $this->Html->link(
                                                '<span class="fa fa-edit"></span> Edit',
                                                [
                                                    'plugin' => 'Providers',
                                                    'controller' => 'Providers',
                                                    'action' => 'edit',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'class' => 'btn btn-condensed btn-primary'
                                                ]
                                            );

                                            if ($isImmutable || $canLoginAs) {
                                                $buttons .= $this->Html->link(
                                                    '<span class="fa fa-user"></span> Login As',
                                                    [
                                                        'plugin' => 'Providers',
                                                        'controller' => 'Providers',
                                                        'action' => 'loginAs',
                                                        'prefix' => 'admin',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-success'
                                                    ]
                                                );
					    }

                                            if (in_array($row->provider_status_id, [1,3])) {
                                                $buttons .= $this->Html->link(
                                                    '<span class="fa fa-check-circle-o"></span> Approve',
                                                    [
                                                        'plugin' => 'Providers',
                                                        'controller' => 'Providers',
                                                        'action' => 'approve_provider',
                                                        'prefix' => 'admin',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-success'
                                                    ]
                                                );
                                            }
                                            if (in_array($row->provider_status_id, [1,2])) {
                                                $buttons .= $this->Html->link(
                                                    '<span class="fa fa-times-circle-o"></span> Deny',
                                                    [
                                                        'plugin' => 'Providers',
                                                        'controller' => 'Providers',
                                                        'action' => 'deny_provider',
                                                        'prefix' => 'admin',
                                                        $row->id
                                                    ],[
                                                        'escape' => false,
                                                        'class' => 'btn btn-condensed btn-warning'
                                                    ]
                                                );
                                            }
                                            $buttons .= $this->Html->link(
                                                '<span class="fa fa-trash-o"></span> Delete',
                                                [
                                                    'plugin' => 'Providers',
                                                    'controller' => 'Providers',
                                                    'action' => 'delete',
                                                    'prefix' => 'admin',
                                                    $row->id
                                                ],[
                                                    'escape' => false,
                                                    'onclick' => "if(confirm('Are you sure you want to delete this item?')) { return true;} return false;",
                                                    'class' => 'btn btn-condensed btn-danger'
                                                ]
                                            );
                                            return $buttons;
                                        }
                                    ]
                                ],
                                $providersQuery
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to
                            <?php
                            echo $this->Html->link(
                                'Add a Provider',
                                [
                                    'plugin' => 'Providers',
                                    'controller' => 'Providers',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Provider'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer',
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
