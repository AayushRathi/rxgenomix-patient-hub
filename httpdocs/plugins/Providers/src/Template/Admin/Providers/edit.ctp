<?php
$pluginTitle = $lib24watchModules['Providers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Providers"),
    [
        'plugin' => 'Providers',
        'controller' => 'Providers',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($providerId ? "Edit" : "Add") . " Provider";
$this->Breadcrumbs->add($pageHeading);

echo $this->Html->scriptBlock('var healthCareClientGroups = ' . json_encode($healthCareClientGroups));
echo $this->Html->scriptBlock('var groupId = ' . ($providerEntity->health_care_client_group_id ? $providerEntity->health_care_client_group_id : 'null'));
echo $this->Html->script('/providers/js/admin/Providers/edit.js', ['block' => 'extra-scripts']);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($providerEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
            				echo $this->Form->control(
            					'user.first_name',
            					[
            						'type' => 'text',
            						'label' => 'First Name',
                                    'required' => true
            					]
            				);
            				echo $this->Form->control(
            					'user.last_name',
            					[
            						'type' => 'text',
            						'label' => 'Last Name',
                                    'required' => true
            					]
            				);
                            if ($this->Lib24watchPermissions->hasPermission('providers_set_health_care_client', 'Providers')) {
                                echo $this->Form->control(
                                    'health_care_client_id',
                                    [
                                        'type' => 'select',
                                        'label' => 'Health Care Client',
                                        'empty' => true,
                                        'options' => $healthCareClients
                                    ]
                                );
                                echo $this->Form->control(
                                    'health_care_client_group_id',
                                    [
                                        'label' => 'Group Name',
                                        'type' => 'select',
                                        'empty' => true,
                                        'options' => []
                                    ]
                                );
                            }
            				echo $this->Form->control(
            					'provider_type_id',
            					[
            						'type' => 'select',
            						'label' => 'Provider Type',
                                    'empty' => true,
                                    'options' => $providerTypes
            					]
            				);
                            echo $this->Form->control(
                                'provider_type_other',
                                [
                                    'type' => 'text',
                                    'label' => 'Other'
                                ]
                            );
                            echo $this->Form->control(
                                'npi_number',
                                [
                                    'type' => 'text',
                                    'label' => 'NPI #',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'provider_license_id',
                                [
                                    'type' => 'text',
                                    'label' => 'Provider License ID',
                                    'required' => true
                                ]
                            );

                            echo $this->Form->control(
                                'license_expiration_date',
                                [
                                    'type' => 'datetimepicker',
                                    'label' => 'License Expiration Date',
                                    'required' => true, // just set it to true for now
                                    'placeholder' => 'MM-DD-YYYY',
                                ]
                            );

                            echo $this->Form->control(
                                'provider_lib24watch_state_abbr',
                                [
                                    'type' => 'select',
                                    'label' => 'State of Provider License',
                                    'empty' => true,
                                    'options' => $states
                                ]
                            );
                            echo $this->Form->control(
                                'facility_name',
                                [
                                    'type' => 'text',
                                    'label' => 'Facility Name',
                                    'required' => false
                                ]
                            );
                            echo $this->Form->control(
                                'nabpep_id',
                                [
                                    'type' => 'text',
                                    'label' => 'NABPe-P ID',
                                    'required' => true
                                ]
                            );
                            // TODO: file upload

                            echo $this->Form->control(
                                'email',
                                [
                                    'type' => 'text',
                                    'label' => 'Email',
                                    'required' => true
                                ]
                            );
                            if ($providerEntity->isNew()) {
                                echo $this->Form->control(
                                    'user.password',
                                    [
                                        'type' => 'password',
                                        'label' => 'Password',
                                        'required' => true
                                    ]
                                );
                            }
                            echo $this->Form->control(
                                'phone',
                                [
                                    'type' => 'text',
                                    'label' => 'Phone',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'address_1',
                                [
                                    'type' => 'text',
                                    'label' => 'Address',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'address_2',
                                [
                                    'type' => 'text',
                                    'label' => 'Address Cont.',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'city',
                                [
                                    'type' => 'text',
                                    'label' => 'City',
                                    'required' => true
                                ]
                            );
                            echo $this->Form->control(
                                'lib24watch_state_abbr',
                                [
                                    'type' => 'select',
                                    'label' => 'State',
                                    'empty' => true,
                                    'options' => $states
                                ]
                            );
                            echo $this->Form->control(
                                'postal_code',
                                [
                                    'type' => 'text',
                                    'label' => 'Zip',
                                    'required' => true
                                ]
                            );
            				echo $this->Form->control(
            					'is_active',
            					[
            						'type' => 'checkbox',
            						'label' => 'Active'
            					]
            				);
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
