<?php

namespace Providers\Mailer;

use App\Util\EmailHelper;
use Cake\Datasource\EntityInterface as EntityInterfaceAlias;
use Cake\Mailer\Mailer;

class ProvidersMailer extends Mailer
{
    /**
     * @param array $providerEntities
     *
     * @return bool
     *
     */
    protected function reportLabOrderInactiveProviders(EntityInterfaceAlias $providerEntity)
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this
            ->setTo($providerEntity->user->email)
            ->setCc($rxgAdmins)
            ->setSubject("Lab Order Inactivity")
            ->setTemplate('Providers.reportLabOrderInactiveProviders')
            ->setEmailFormat('html');

        return true;
    }

    /**
     * @param EntityInterfaceAlias $provider
     */
    protected function providerApproved(EntityInterfaceAlias $provider)
    {
        $this
            ->to($provider->email)
            ->setSubject('Congratulations! Your registration for the RxGenomix Hub has been approved by RxGenomix.')
            ->setViewVars(['provider' => $provider])
            ->setTemplate('Providers.providerApproved')
            ->setEmailFormat('html');
    }

    /**
     * @param EntityInterfaceAlias $provider
     */
    protected function providerDenied(EntityInterfaceAlias $provider)
    {
        $this
            ->to($provider->email)
            ->setSubject("Your RxGenomix Hub Account Status")
            ->setViewVars(['provider' => $provider])
            ->setTemplate('Providers.providerDenied')
            ->setEmailFormat('html');
    }

    /**
     * @param string $toEmailAddress
     * @param EntityInterfaceAlias $labOrder
     */
    protected function reportReady(
        string $toEmailAddress,
        EntityInterfaceAlias $labOrder,
        string $cadenceNotice = ''
    )
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this->setCc($rxgAdmins);

        $this
            ->to($toEmailAddress)
            ->setSubject("A New Result is Ready in the RxGenomix Hub")
            ->setViewVars([
                'labOrder' => $labOrder,
                'cadenceNotice' => $cadenceNotice
            ])
            ->setTemplate('Providers.reportReady')
            ->setEmailFormat('html');
    }

    /**
     * @param array $providerEntities
     *
     * @return bool
     *
     */
    protected function reportExpiredLicenseProviders(array $providerEntities=[])
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this
            ->setTo($rxgAdmins)
            ->setSubject("Expired license pharmacists")
            ->setViewVars(
                compact('providerEntities')
            )
            ->setTemplate('Providers.reportExpiredLicenseProviders')
            ->setEmailFormat('html');

        return true;
    }

    /**
     * @param $provider
     *
     * @return bool
     *
     */
    protected function reportExpiredLicenseProvider($provider)
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this
            ->setTo($rxgAdmins)
            ->setSubject("Expired license pharmacist")
            ->setViewVars(
                compact('provider')
            )
            ->setTemplate('Providers.reportExpiredLicenseProvider')
            ->setEmailFormat('html');

        return true;
    }

    /**
     * @param array $providerEntities
     *
     * @return bool
     *
     */
    protected function reportInactiveProviders(array $providerEntities=[])
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this
            ->setTo($rxgAdmins)
            ->setSubject("Inactive pharmacists")
            ->setViewVars(
                compact('providerEntities')
            )
            ->setTemplate('Providers.reportInactiveProviders')
            ->setEmailFormat('html');

        return true;
    }

    /**
     * @param array $providerEntities
     *
     * @return bool
     *
     */
    protected function reportInactiveProvider($provider)
    {
        $rxgAdmins = EmailHelper::getRxgAdmins();

        if (is_null($rxgAdmins)) {
            throw new \Exception('No Rxg admins found');
        }

        $this
            ->setTo($rxgAdmins)
            ->setSubject("Inactive pharmacist")
            ->setViewVars(
                compact('provider')
            )
            ->setTemplate('Providers.reportInactiveProvider')
            ->setEmailFormat('html');

        return true;
    }

}
