<?php
namespace Providers\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'login_as' => 'Login As',
            'providers_view' => 'View Providers',
            'providers_edit' => 'Edit Providers',
            'providers_set_health_care_client' => 'Set Providers Health Care Client',
            'providers_delete' => 'Delete Providers',
            'providers_can_approve' => 'Can Approve/Deny Providers'
        ];
    }
}
