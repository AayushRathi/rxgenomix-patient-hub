<?php
namespace Providers\Controller\Admin;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;
use Labs\Libs\Lab;
use Providers\Controller\AppController;
use Providers\Libs\TokenHandler;
use Cake\Cache\Cache;

/**
 * Class ProvidersController
 * @package Providers\Controller\Admin
 */
class ProvidersController extends AppController
{
    use MailerAwareTrait;
    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'Providers' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Providers.Providers');
    }

    /**
     * index
     */
    public function index()
    {
        $isImmutable = (bool)$this->currentLib24watchUser['is_immutable'];
        $this->requirePluginPermission('providers_view', 'Providers',
            'You do not have permission to view Provider Accounts');

        $providersQuery = $this->Providers->find()->contain(['Users']);
        $this->paginate($providersQuery);
        $this->set('providersQuery', $providersQuery);

        $this->loadModel('Providers.ProviderTypes');
        $this->set('providerTypes', $this->ProviderTypes->find('dropdown'));

        $this->loadModel('Providers.ProviderStatuses');
        $this->set('providerStatuses', $this->ProviderStatuses->find('dropdown'));

        $this->set(compact('isImmutable'));
    }

    /**
     * @param int|null $providerId
     * @throws \Exception
     */
    public function edit(int $providerId = null)
    {
        $this->requirePluginPermission('providers_edit', 'Providers',
            'You do not have permission to edit Provider Accounts');

        if ($providerId) {
            $entity = $this->Providers->get($providerId, [
                'contain' => [
                    'Users'
                ]
            ]);
        } else {
            $entity = $this->Providers->newEntity($this->getRequest()->getData(), [
                'associated' => [
                    'Users'
                ],
                'validate' => false
            ]);
        }


        if($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            $data['user']['email'] = $data['email'];
            $data['user']['health_care_client_id'] = $data['health_care_client_id'];
            $data['user']['health_care_client_group_id'] = $data['health_care_client_group_id'];

            // Only Providers right now
            $data['user']['lab_order_initiator_type'] = 2;

            $entity = $this->Providers->patchEntity($entity, $data, [
                'associated' => [
                    'Users'
                ]
            ]);

            $errors = $entity->getErrors();

            if (empty($errors)) {
                if (!$this->Providers->save($entity)) {
                    throw new \Exception('Could not save Provider');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'Providers',
                            'controller' => 'Providers',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Provider ' . ($providerId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $this->set('providerId', $providerId);
        $this->set('providerEntity', $entity);

        $this->loadModel('Providers.ProviderStatuses');
        $this->set('providerStatuses', $this->ProviderStatuses->find('dropdown'));

        $this->loadModel('Providers.ProviderTypes');
        $this->set('providerTypes', $this->ProviderTypes->find('dropdown'));

        $this->loadModel('Lib24watch.Lib24watchStates');
        $this->set('states', $this->Lib24watchStates->find('list', [
            'keyField' => 'abbr',
            'valueField' => 'title'
        ])->order(['abbr' => 'ASC'])->toArray());

        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1,
            'has_contract' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());


        $this->loadModel('HealthCareClients.HealthCareClientGroups');
        $healthCareClientGroups = [];
        $groups = $this->HealthCareClientGroups->find();
        foreach ($groups as $group) {
            $healthCareClientGroups[$group->health_care_client_id][$group->id] = $group->title;
        }
        $this->set('healthCareClientGroups', $healthCareClientGroups);
    }

    /**
     * Log in as an admin user of a site of league/program
     *
     * @param string $uuid   uuid of user
     *
     * @return \Cake\Http\Response
     */
    public function logInAs(int $userId= 0)
    {
        $this->requirePluginPermission('login_as', 'Providers');

        $currentUser = $this->getCurrentLib24watchUserFromSession();

        $message = __('Logged in as admin');
        $status = 'success';

        $tokenHandler = new TokenHandler();

        $tokenValue = $tokenHandler->saltToken($userId);

        $this->loadModel('Lib24watchUserTokens');
        $lib24watchUserToken = $this->Lib24watchUserTokens->newEntity();
        $lib24watchUserToken->lib24watch_user_id = $currentUser->id;
        $lib24watchUserToken->token_type = 'LogInAs';
        $lib24watchUserToken->token_value = $tokenValue;

        if (!$this->Lib24watchUserTokens->save($lib24watchUserToken)) {
            $message = __('Failure to generate token');
            $status = 'danger';

            return $this->redirectWithDefault(
                [
                    'prefix' => 'admin',
                    'plugin' => 'Providers',
                    'controller' => 'Providers',
                    'action' => 'index',
                ],
                $message,
                $status
            );
        }

        return $this->redirectWithDefault(
            [
                'prefix' => false,
                'plugin' => 'RxgUsers',
                'controller' => 'RxgUsers',
                'action' => 'loginAs',
                $tokenValue
            ],
            $message,
            $status
        );
    }

    /**
     * @param int|null $providerId
     * @throws \Exception
     */
    public function approve_provider(int $providerId = null)
    {
        $this->requirePluginPermission('providers_can_approve', 'Providers',
            'You do not have permission to approve Provider Accounts');

        $providerEntity = $this->Providers->get($providerId, [
            'contain' => [
            ]
        ]);
        $providerEntity->provider_status_id = 2;
        $providerEntity->has_seen_status_message = 0;
        $providerEntity->date_approved = date('Y-m-d');
        if ($this->Providers->save($providerEntity)) {
            $mailer = $this->getMailer('Providers.Providers');
            $mailer->send(
                'providerApproved',
                [
                    $providerEntity,
                ]
            );

            // Auto submit any requests that were submitted prior to approval.
            $this->loadModel('LabOrders.LabOrders');
            $labOrders = $this->LabOrders->find()
                ->contain([
                    'WorkflowStatus',
                    'Patient.Sex',
                    'Providers',
                    'Provider.Users',
                    'Provider.HealthCareClient.Lab',
                    'Medications.Drug.BrandNames'
                ])
                ->where([
                    'lab_order_status_id' => 2,
                    'provider_id' => $providerId
            ]);

            foreach ($labOrders as $labOrder) {
                // Actually submit to lab here
                $healthCareClient = $labOrder->provider->health_care_client;
                $lab = new Lab($healthCareClient->lab_id);

                if ($labOrder->isSkippableToReview) {
                    // HL7 spits out warnings, silence them!
                    Configure::write('debug', 0);

                    $payload = $lab->getRequestDriver()->createLabOrder($labOrder);
                    $connectionInformation = $healthCareClient->lab->toArray();

                    $labOrderFileName = ROOT . '/data/lab_orders/' . $labOrder->id . '.hl7';
                    $connectionInformation['local_file'] = $labOrderFileName;
                    $connectionInformation['remote_file'] = $connectionInformation['submit_path'] . '/' . $labOrder->id . '.hl7';

                    Configure::write('debug', 1);
                    $result = $lab->getConnector()
                        ->createConnection($connectionInformation)
                        ->post($connectionInformation, $payload);

                    if (isset($result) && $result) {
                        $labOrder->lab_order_status_id = 3;
                        $this->LabOrders->save($labOrder);

                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Submit to Lab',
                            'Success'
                        );
                    } else {
                        // submit to lab failed, log it
                        $labOrder->lab_order_status_id = 9;
                        $this->LabOrders->save($labOrder);

                        if (is_bool($result) === true) {
                            $result  = null;
                        }

                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Submit to Lab',
                            'Submit to Lab Failed',
                            $result
                        );
                    }
                } else {
                    // submit to lab failed, log it
                    $labOrder->lab_order_status_id = 9;
                    $this->LabOrders->save($labOrder);

                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Submit to Lab',
                        'Submit to Lab Failed - incomplete lab order',
                        $result
                    );
                }
            }

            Cache::delete('dashboard_Providers');

            if ($this->getRequest()->getQuery('fromDashboard')) {
                $this->redirectWithDefault(
                    [
                        "plugin" => "Lib24watch",
                        "controller" => "Lib24watch",
                        "action" => "index",
                        "prefix" => "admin"
                    ],
                    "Approved Account"
                );
            } else {
                $this->redirectWithDefault(
                    [
                        "plugin" => "Providers",
                        "controller" => "Providers",
                        "action" => "index",
                        "prefix" => "admin"
                    ],
                    "Approved Account"
                );
            }
        }
    }

    /**
     * @param int|null $providerId
     */
    public function deny_provider(int $providerId = null)
    {
        $this->requirePluginPermission('providers_can_approve', 'Providers',
            'You do not have permission to deny provider accounts');

        $providerEntity = $this->Providers->get($providerId, [
            'contain' => [
            ]
        ]);
        $providerEntity->provider_status_id = 3;
        $providerEntity->has_seen_status_message = 0;
        if ($this->Providers->save($providerEntity)) {

            $mailer = $this->getMailer('Providers.Providers');
            $mailer->send(
                'providerDenied',
                [
                    $providerEntity
                ]
            );
            Cache::delete('dashboard_Providers');

            if ($this->getRequest()->getQuery('fromDashboard')) {
                $this->redirectWithDefault(
                    [
                        "plugin" => "Lib24watch",
                        "controller" => "Lib24watch",
                        "action" => "index",
                        "prefix" => "admin"
                    ],
                    "Denied Provider Account"
                );
            } else {
                $this->redirectWithDefault(
                    [
                        "plugin" => "Providers",
                        "controller" => "Providers",
                        "action" => "index",
                        "prefix" => "admin"
                    ],
                    "Denied Provider Account"
                );
            }
        }
    }

    /**
     * @param int|null $providerId
     * @throws \Exception
     */
    public function change_status(int $providerId = null)
    {
        $this->requirePluginPermission('providers_edit', 'Providers',
            'You do not have permission to edit Providers');

        $providerEntity = $this->Providers->get($providerId);


        if ($providerEntity->is_active) {
            $providerEntity->is_active = 0;
            $text = 'inactive';
        } else {
            $providerEntity->is_active = 1;
            $text = 'active';
        }

        if (!$this->Providers->save($providerEntity)) {
            throw new \Exception("Couldn't update active status for Provider with ID ({$providerId})");
        }

        $this->Lib24watchLogger->write("Changed status of Provider '" . $providerEntity->email . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'Providers',
                'controller' => 'Providers',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Provider '" . $providerEntity->email . "' is now " . $text
        );
    }

    /**
     * @param int|null $providerId
     * @throws \Exception
     */
    public function delete(int $providerId = null)
    {
        $this->requirePluginPermission('providers_delete', 'Providers',
            'You do not have permission to delete Providers');

        $providerEntity = $this->Providers->get($providerId);

        $rxgUserId = $providerEntity->rxg_user_id;

        if (!$this->Providers->delete($providerEntity)) {
            throw new \Exception("Couldn't delete provider with ID ({$providerId})");
        }

        $usersTable = TableRegistry::getTableLocator()->get('RxgUsers.RxgUsers');

        $userEntity = $usersTable->find()
            ->where([
                'id' => $rxgUserId
            ])
            ->firstOrFail();

        if (!$usersTable->delete($userEntity)) {
            throw new \Exception("Couldn't delete provider (user) with ID ({$providerId})");
        }

        $this->Lib24watchLogger->write("Deleted Provider '" . $providerEntity->email . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'Providers',
                'controller' => 'Providers',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Provider '" . $providerEntity->email . "' deleted."
        );
    }

}
