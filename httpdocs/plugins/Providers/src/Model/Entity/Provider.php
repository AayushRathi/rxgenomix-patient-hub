<?php

namespace Providers\Model\Entity;

use Cake\ORM\Entity;

class Provider extends Entity
{
    public function _getPhoneFormatted()
    {
        $phone = null;
        if (isset($this->phone)) {
            $phoneRegex = '/(\d+)-(\d+)-(\d+)/';
            if (preg_match($phoneRegex, $this->phone)) {
                $phone = preg_replace($phoneRegex, "($1) $2-$3", $this->phone);
            } else if(preg_match('/^(?:\+\d)?(\d{3})(\d{3})(\d{4})$/', $this->phone,  $matches)) {
                $phone = $matches[1] . ' ' . $matches[2] . '-' . $matches[3];
            } else {
                $phone = $this->phone;
            }
        }

        return $phone;
    }
}
