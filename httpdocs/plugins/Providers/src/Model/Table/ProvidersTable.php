<?php
namespace Providers\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;
use Cake\ORM\TableRegistry;

/**
 * Class ProvidersTable
 * @package Providers\Model\Table
 */
class ProvidersTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Type',
            [
                'className' => 'Providers.ProviderTypes',
                'foreignKey' => 'provider_type_id'
            ]
        );

        $this->belongsTo(
            'Users',
            [
                'className' => 'RxgUsers.RxgUsers',
                'foreignKey' => 'rxg_user_id'
            ]
        );

        $this->belongsTo(
            'HealthCareClient',
            [
                'className' => 'HealthCareClients.HealthCareClients',
                'foreignKey' => 'health_care_client_id'
            ]
        );

        $this->belongsTo(
            'HealthCareClientGroup',
            [
                'className' => 'HealthCareClients.HealthCareClientGroups',
                'foreignKey' => 'health_care_client_group_id'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $userValidator = TableRegistry::get('RxgUsers.RxgUsers')
            ->getValidator('default');

        return $validator
            ->addNested('users', $userValidator)
            ->requirePresence('provider_type_id')
            ->notEmpty('provider_type_id', 'Provider Type is required')
            ->requirePresence('npi_number')
            ->notEmpty('npi_number', 'NPI # is required')
            ->requirePresence('health_care_client_id')
            ->notEmpty('health_care_client_id', 'Health Care Client is required')
            ->requirePresence('health_care_client_group_id', [$this, 'validateGroups'])
            ->notEmpty('health_care_client_group_id', 'Group is required', [$this, 'validateGroups'])
            ->requirePresence('provider_license_id')
            ->notEmpty('provider_license_id', 'Provider License ID is required')
            ->requirePresence('provider_lib24watch_state_abbr')
            ->notEmpty('provider_lib24watch_state_abbr', 'State of Provider License is required')
            ->requirePresence('license_expiration_date')
            ->notEmpty('license_expiration_date', 'License expiration date is required')
            ->requirePresence('email')
            ->email('email', false, 'Must be a valid Email Address')
            ->notEmpty('email', 'Email is required')
            ->requirePresence('phone')
            ->notEmpty('phone', 'Phone is required')
            ->requirePresence('address_1')
            ->notEmpty('address_1', 'Address is required')
            ->requirePresence('city')
            ->notEmpty('city', 'City is required')
            ->requirePresence('lib24watch_state_abbr')
            ->notEmpty('lib24watch_state_abbr', 'State is required')
            ->requirePresence('postal_code')
            ->notEmpty('postal_code', 'Zip is required')
            ;
    }     

    /**
     * Validate whether a health care client group id is required based on if there are groups for a health care client
     *
     * @param array $context
     *
     * @return bool
     */
    public function validateGroups($context) {
        if (
            isset($context['data']['health_care_client_id']) &&
            (int)$context['data']['health_care_client_id'] > 0
        ) {
            $groupCount = TableRegistry::getTableLocator()->get('HealthCareClients.HealthCareClientGroups')
                ->find()
                ->where([
                    'HealthCareClientGroups.health_care_client_id' => $context['data']['health_care_client_id']
                ])
                ->count();

            if ($groupCount > 0) {
                return true;
            }
        }

        return false;
    }
}
