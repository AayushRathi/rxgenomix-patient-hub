<?php
namespace Providers\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use Lib24watch\Model\Table\Lib24watchSettingsTable;

/**
 * CheckLabOrderInactivityPharmacists command.
 */
class CheckLabOrderInactivityPharmacistsCommand extends Command
{
    use MailerAwareTrait;

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $providersTable = TableRegistry::getTableLocator()->get('Providers.Providers');

        $cadenceSettingKey = 'EMAIL_NOTIFICATION_LAB_ORDER_INACTIVITY_DURATION_CADENCE';
        $intervalSettingKey = 'EMAIL_NOTIFICATION_LAB_ORDER_INACTIVITY_DURATION_INTERVAL';

        $cadenceSetting = Lib24watchSettingsTable::readSettingStatic($cadenceSettingKey, 'Providers', 'days');
        $intervalSetting = Lib24watchSettingsTable::readSettingStatic($intervalSettingKey, 'Providers', '-14');

        if (is_null($cadenceSetting)) {
            throw new \Exception('Setting `' . $cadenceSettingKey . '` required');
        }

        if (is_null($intervalSetting)) {
            throw new \Exception('Setting `' . $intervalSettingKey . '` required');
        }

        $dateInterval = new \Cake\I18n\Date;
        $dateInterval->modify($intervalSetting . ' ' . $cadenceSetting);

        $inactiveProviderEntities = $providersTable
            ->find()
            ->where([
                'provider_type_id' => 1,
                'provider_status_id' => 2,
                'is_active' => 1,
            ])
            ->contain('Users', function($q) {
                return $q->select([
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                ]);
            })
            ->matching('Users', function($q) {
                return $q->andWhere([
                    'Users.active' => 1,
                    #'Users.email LIKE' => '%djrpanuncio%'
                ]);
            })
            ->where([
                #'OR' => [
                    #'Providers.last_emailed_lab_order_activity_date IS' => null,
                    'Providers.last_emailed_lab_order_activity_date' => $dateInterval
                #]
            ])
            ->toArray();

        foreach ($inactiveProviderEntities as $key => $provider) {
            $labOrder = TableRegistry::getTableLocator()
                ->get('LabOrders.LabOrders')
                ->find()
                ->where([
                    'LabOrders.provider_id' => $provider->id
                ])
                ->order([
                    'LabOrders.created' => 'DESC',
                ])
                ->limit(1)
                ->first();

            $timestamp = \Cake\I18n\Time::UNIX_TIMESTAMP_FORMAT;

            if (isset($labOrder)) {
                debug($labOrder->created->format('Y-m-d'));
                debug($dateInterval->format('Y-m-d'));
            }

            if (
                $labOrder && 
                isset($labOrder->created) && 
                $labOrder->created->format('Y-m-d') != $dateInterval->format('Y-m-d')
                #$labOrder->created->i18nFormat($timestamp) < $dateInterval->i18nFormat($timestamp)
            ) {
                unset($inactiveProviderEntities[$key]);
            } else if (is_null($labOrder)) {
                unset($inactiveProviderEntities[$key]);
            }
        }

        debug($inactiveProviderEntities);

        if (count($inactiveProviderEntities) > 0) {
            foreach ($inactiveProviderEntities as $provider) {
                debug($provider);
                $mailer = $this->getMailer('Providers.Providers');

                $mailer->send('reportLabOrderInactiveProviders', [
                    $provider
                ]);
            }
        }

        /*
        $labOrdersSubquery = $providersTable->getConnection()->newQuery()
            ->select(['lab_orders.id'])
            ->from('lab_orders')
            ->order(['lab_orders.created' => 'DESC'])
            ->limit(1);

        $providers = $providersTable
            ->find()
            ->select([
                'Providers.id',
                'LatestLabOrders.id'
            ])
            // Need to get the greatest possible TPromDate related to the dispatch
            ->join([
                'table' => $labOrdersSubquery,
                'type' => 'INNER',
                'alias' => 'LatestLabOrders',
                'conditions' => 'LatestLabOrders.provider_id = Providers.id'
            ])
            ->contain([
                'Users' => function (\Cake\ORM\Query $query) {
                    return $query->select([
                        'Users.id',
                    ]);
                }
            ])
            ->toArray();
        */
    }
}
