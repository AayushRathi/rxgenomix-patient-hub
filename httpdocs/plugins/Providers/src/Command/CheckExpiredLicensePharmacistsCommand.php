<?php
namespace Providers\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Cake\Mailer\MailerAwareTrait;

/**
 * CheckExpiredLicensePharmacists command.
 */
class CheckExpiredLicensePharmacistsCommand extends Command
{
    use MailerAwareTrait;

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $providersTable = TableRegistry::getTableLocator()->get('Providers.Providers');
        $today = new \Cake\I18n\Date;
        $expiredProviderEntities = $providersTable
            ->find()
            ->where([
                'provider_type_id' => 1,
                'provider_status_id' => 2,
                'is_active' => 1,
                'license_expiration_date' => $today->format('Y-m-d')
            ])
            ->contain('Users', function($q) {
                return $q->select([
                    'id',
                    'last_logged_in',
                    'first_name',
                    'last_name'
                ]);
            })
            ->matching('Users', function($q) {
                return $q->andWhere([
                    'Users.active' => 1
                ]);
            })
            ->formatResults(function ( $results) {
                return $results->map(function( $row) {
                    $result = new \StdClass;
                    $result->name = $row->user->first_name . ' ' . $row->user->last_name;
                    $result->license_expiration_date = $row->license_expiration_date;

                    return $result;
                });
            })
            ->toArray();

        debug($expiredProviderEntities);

        if (!empty($expiredProviderEntities)) {
            $mailer = $this->getMailer('Providers.Providers');
            if (count($expiredProviderEntities) < 10) {
                foreach ($expiredProviderEntities as $provider) {
                    $mailer->send('reportExpiredLicenseProvider', [
                        $provider
                    ]);
                }
            } else {
                $mailer->send('reportExpiredLicenseProviders', [
                    $expiredProviderEntities
                ]);
            }
        } else {
            debug('No expired pharmacists found');
        }
    }
}
