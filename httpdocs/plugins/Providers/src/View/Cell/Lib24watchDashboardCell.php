<?php
namespace Providers\View\Cell;

use Cake\Datasource\Paginator;
use Cake\View\Cell;
use Cake\Routing\Router;
use Cake\Cache\Cache;

class Lib24watchDashboardCell extends Cell
{
    public $paginate = [
        'Providers' => [
            'conditions' => [
                'provider_status_id' => 1
            ]
        ]
    ];

    public function display()
    {
        $this->viewBuilder()->setHelpers(['Paginator']);

        try {
            $currentLib24watchUserRequest = Router::getRequest();
            if (!is_null($currentLib24watchUserRequest)) {
                $modifiedByLib24watchUser = $currentLib24watchUserRequest->is('currentLib24watchUser');
            }
        } catch (\Exception $e) {
            \Cake\Log\Log::warning('Could not find active user');
        }

        $this->set('currentLib24watchUser', $modifiedByLib24watchUser);
        $this->set('canApprove', $this->canApprovePermissions($modifiedByLib24watchUser));

        // Providers awaiting approval
        $this->loadModel('Providers.Providers');
        $providers = $this->Providers->find()
            ->contain([
                'Type',
                'Users'
            ])->where([
                'provider_status_id' => 1
        ]);
        $paginator = new Paginator();
        $providers = $paginator->paginate($providers, $this->request->getQueryParams());

        $paging = $paginator->getPagingParams() + (array)$this->request->getParam('paging');
        $this->request = $this->request->withParam('paging', $paging);
        $this->set('providers', $providers);
    }

    public function javascript()
    {

    }

    protected function canApprovePermissions($lib24watchUser = null): bool
    {
        $permissionKey = 'providers_can_approve';
        $plugin = 'Providers';
        $retval = false;
        if ($lib24watchUser->is_superuser) {
            $retval = true;
        } else {
            foreach ($lib24watchUser->lib24watch_user_groups as $userGroup) {
                foreach ($userGroup->lib24watch_user_group_plugin_permissions as $permission) {
                    if ($permission->plugin == $plugin && $permission->permission_key == $permissionKey) {
                        $retval = true;
                        break 2;
                    }
                }
            }
        }
        return $retval;
    }
}
