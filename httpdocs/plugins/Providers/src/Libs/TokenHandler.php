<?php

namespace Providers\Libs;

use Cake\Utility\Text;

/**
 * Class TokenHandler
 *
 * @package Providers\Libs
 */
class TokenHandler
{

    protected $tokenDelimiter = ':';

    /**
     * Returns a token with a random string at the end separated by the
     * delimiter, base64encoded
     *
     * @param string $value value to be salted
     *
     * @return string token
     */
    public function saltToken($value)
    {
        $textUtility = new Text();

        return base64_encode($value . $this->tokenDelimiter . $textUtility->uuid());
    }

    /**
     * Decodes the token transforming it back into it's value
     *
     * @param string $token token to decode
     *
     * @return string value
     */
    public function decodeToken($token)
    {
        $token = base64_decode($token);
        $tokenArray = explode($this->tokenDelimiter, $token);

        return $tokenArray[0];
    }

    /**
     * Gets class delimiter
     *
     * @return string
     */
    public function getDelimiter()
    {
        return $this->tokenDelimiter;
    }

    /**
     * Sets class delimiter
     *
     * @param string $delimiter delimiter
     *
     * @return TokenHandler
     */
    public function setDelimiter(string $delimiter)
    {
        if ($delimiter === '') {
            throw new \InvalidArgumentException(
                'setDelimiter only accepts strings with non-empty values'
            );
        }

        $this->tokenDelimiter = $delimiter;

        return $this;
    }
}
