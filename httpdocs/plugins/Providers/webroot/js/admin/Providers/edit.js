$(document).ready(function () {

    function showHideProviderTypeOther()
    {
        var group = $('#provider-type-other').closest('.form-group');
        if ($('#provider-type-id').find(':selected').val() == 5) {
            group.show();
            if (!group.hasClass('required')) {
                group.addClass('required');
            }
        } else {
            group.hide();
            group.removeClass('required');
        }
    }
    $('#provider-type-id').on('change', function () {
        showHideProviderTypeOther();
    });
    showHideProviderTypeOther();

    var typeField = $('#health-care-client-id');
    var subTypeField = $('#health-care-client-group-id');
    var subTypeWrapper = subTypeField.closest('div.form-group');
    subTypeWrapper.hide();

    typeField.on('change', function () {
        var typeId = $(this).find(':selected').val();
        if (healthCareClientGroups[typeId]) {
            subTypeField.find('option').remove();
            subTypeField.append($('<option>').text('').attr('value', ''));
            $.each(healthCareClientGroups[typeId], function (i, value) {
                let option = $('<option>').text(value).attr('value', i);
                if (groupId && groupId == i) {
                    option.prop('selected', true);
                }
                subTypeField.append(option);
            });
            subTypeWrapper.show();
        } else {
            subTypeField.find('option').remove();
            subTypeWrapper.hide();
        }
    });

    typeField.trigger('change');
});
