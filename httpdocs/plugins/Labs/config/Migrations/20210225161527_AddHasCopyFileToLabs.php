<?php
use Migrations\AbstractMigration;

class AddHasCopyFileToLabs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('labs');
        $table->addColumn('has_copy_file', 'boolean', [
            'default' => '0',
            'null' => false,
        ]);
        $table->update();
    }
}
