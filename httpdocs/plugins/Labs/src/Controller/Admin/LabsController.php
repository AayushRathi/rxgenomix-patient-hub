<?php
namespace Labs\Controller\Admin;

use App\Drivers\HL7;
use Labs\Controller\AppController;
use SystemManagement\Libs\EventLogs;

/**
 * Class LabsController
 * @package Labs\Controller\Admin
 */
class LabsController extends AppController
{

    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'Labs' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Labs.Labs');
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('labs_view', 'Labs', 'You do not have permission to access this resource.');
        $query = $this->Labs->find();
        $this->paginate($query);
        $this->set('query', $query);
    }

    /**
     * @param int|null $labId
     * @throws \Exception
     */
    public function edit(int $labId = null)
    {
        $this->requirePluginPermission('labs_edit', 'Labs', 'You do not have permission to access this resource.');
        if ($labId) {
            $entity = $this->Labs->get($labId, [
                'contain' => [
                ]
            ]);
        } else {
            $entity = $this->Labs->newEntity();
        }

        $this->set('labId', $labId);
        $this->set('labEntity', $entity);

        if($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            if (!isset($data['is_active'])) {
                $data['is_active'] = 0;
            }
            if (!isset($data['has_copy_file'])) {
                $data['has_copy_file'] = 0;
            }

            $entity = $this->Labs->patchEntity($entity, $data, [
                'contain' => [
                ]
            ]);

            $errors = $entity->getErrors();
            if (empty($errors)) {
                if (!$this->Labs->save($entity)) {
                    throw new \Exception('Could not save Lab');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'Labs',
                            'controller' => 'Labs',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Lab ' . ($labId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $this->loadModel('SystemManagement.AuthenticationTypes');
        $this->set('authenticationTypes', $this->AuthenticationTypes->find('dropdown'));

        $this->loadModel('SystemManagement.ConnectionTypes');
        $this->set('connectionTypes', $this->ConnectionTypes->find('dropdown'));

        $this->loadModel('SystemManagement.Drivers');
        $this->set('drivers', $this->Drivers->find('dropdown'));
    }

    public function test()
    {

        dd(EventLogs::write(1,2,5,'Test', 'Testing Again', []));

        /*error_reporting(1);
        ini_set('display_errors', 1);
        $parser = new \Smalot\PdfParser\Parser();
        $pdf = $parser->parseFile(ROOT .'/data/sample-snps.pdf');
        dd($pdf->getDetails());
        $pages = $pdf->getPages();
        foreach ($pages as $page) {
            dd($page->getText());
        }*/


        $this->loadModel('LabOrders.LabOrders');

        $labOrderEntity = $this->LabOrders->get(1, [
            'contain' => [
                'Patient.Sex',
                'Provider.HealthCareClient',
                'Medications.Drug.Ndc'
            ]
        ]);

        $hl7 = new \App\Drivers\HL7();
        $message = $hl7->createLabOrder($labOrderEntity);

        file_put_contents(ROOT . '/data/sample_lab_order.hl7', $message);

        die($message);
        /*
        $client = new \Cake\Http\Client();

        $response = $client->get('https://book.cakephp.org/3.next/_downloads/en/CakePHPCookbook.pdf');
        //file_put_contents(ROOT . '/data/test.pdf', $response->getStringBody());
        //exit;
        $file = new \Cake\Filesystem\File(ROOT . '/data/test.pdf', true);
        $file->write($response->getStringBody());
        //dd($response->getStringBody());

        $file->close();
        exit;
        dd(date('Y-m-d', strtotime(19680222)));
        $order = file_get_contents(ROOT . '/data//crestar-sample-order.hl7');

        $hl7 = new HL7($order);

        $pid = $hl7->getMessage()->getSegmentsByName('PID');
        dd($pid[0]->getPatientName());*/
    }
}
