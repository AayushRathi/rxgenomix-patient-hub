<a href="javascript:void(0)"><span class="fa fa-flask"></span><span class="xn-text"><?php echo h($lib24watchModules['Labs']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('labs_view', 'Labs')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Labs",
                [
                    'plugin' => 'Labs',
                    'controller' => 'Labs',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Labs'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('labs_edit', 'Labs')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Lab",
                [
                    'plugin' => 'Labs',
                    'controller' => 'Labs',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Lab'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>