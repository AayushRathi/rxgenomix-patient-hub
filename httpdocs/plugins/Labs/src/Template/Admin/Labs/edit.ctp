<?php
$pluginTitle = $lib24watchModules['Labs']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Labs"),
    [
        'plugin' => 'Labs',
        'controller' => 'Labs',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($labId ? "Edit" : "Add") . " Lab";
$this->Breadcrumbs->add($pageHeading);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($labEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
            				echo $this->Form->control(
            					'lab_name',
            					[
            						'type' => 'text',
            						'label' => 'Name',
                                    'required' => true
            					]
            				);
                            echo $this->Form->control(
                                'request_driver_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Submission Driver',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $drivers
                                ]
                            );
                            echo $this->Form->control(
                                'response_driver_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Result Driver',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $drivers
                                ]
                            );
                            echo $this->Form->control(
                                'connection_type_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Connection Type',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $connectionTypes
                                ]
                            );
                            echo $this->Form->control(
                                'authentication_type_id',
                                [
                                    'type' => 'select',
                                    'label' => 'Authentication Type',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => $authenticationTypes
                                ]
                            );
                            echo $this->Form->control(
                                'scheme',
                                [
                                    'type' => 'select',
                                    'label' => 'Scheme',
                                    'required' => true,
                                    'empty' => true,
                                    'options' => ['http' => 'http', 'https' => 'https', 'ftp' => 'ftp', 'sftp' => 'sftp']
                                ]
                            );
                            echo $this->Form->control(
                                'host',
                                [
                                    'type' => 'text',
                                    'label' => 'Host',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'port',
                                [
                                    'type' => 'text',
                                    'label' => 'Port',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'submit_path',
                                [
                                    'type' => 'text',
                                    'label' => 'Submit Path',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'request_path',
                                [
                                    'type' => 'text',
                                    'label' => 'Request Path',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'report_path',
                                [
                                    'type' => 'text',
                                    'label' => 'Report Path',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'has_copy_file',
                                [
                                    'type' => 'checkbox',
                                    'label' => 'Has Copy File'
                                ]
                            );
                            echo $this->Form->control(
                                'copy_file_suffix',
                                [
                                    'type' => 'text',
                                    'label' => 'Suffix of Copy File',
                                    'required' => false,
                                ]
                            );
                            echo $this->Form->control(
                                'panel_id',
                                [
                                    'type' => 'text',
                                    'label' => 'Panel ID',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'username',
                                [
                                    'type' => 'text',
                                    'label' => 'Username',
                                    'required' => true,
                                ]
                            );
                            echo $this->Form->control(
                                'password',
                                [
                                    'type' => 'password',
                                    'label' => 'Password'
                                ]
                            );
                            echo $this->Form->control(
                                'confirm_password',
                                [
                                    'type' => 'password',
                                    'label' => 'Confirm Password'
                                ]
                            );
            				echo $this->Form->control(
            					'is_active',
            					[
            						'type' => 'checkbox',
            						'label' => 'Active'
            					]
            				);
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
