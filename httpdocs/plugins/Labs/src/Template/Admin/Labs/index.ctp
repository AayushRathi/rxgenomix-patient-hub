<?php
$pluginTitle = $lib24watchModules['Labs']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Labs';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Labs',
                'items' => [
                    [
                        'name' => 'title',
                        'type' => 'text',
                        'label' => 'Lab Name'
                    ],
                    [
                        'name' => 'is_active',
                        'type' => 'checkbox',
                        'label' => 'Active'
                    ]
                ],
                $query,
                $this
            ]);                
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'Labs',
                            'controller' => 'Labs',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Lab'
                        ]
                    )
                ]
            );
            ?>            
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'lab_name',
                                        'label' => 'Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'Labs',
                                                'controller' => 'Labs',
                                                'row' => $row
                                            ]);
                                        }
                                    ]                               
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to 
                            <?php
                            echo $this->Html->link(
                                'Add a Lab',
                                [
                                    'plugin' => 'Labs',
                                    'controller' => 'Labs',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Lab'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
