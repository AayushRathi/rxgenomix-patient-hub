<?php
namespace Labs\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use LabOrders\Libs\LabOrders;

/**
 * GenerateCoverLetters command.
 */
class GenerateCoverLetterCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = $args->getArgumentAt(1);
        $io->out('Generating cover letter for lab order #' . $labOrderId);

        if (is_null($labOrderId)) {
            throw new \Exception('Please specify a lab order id');
        }

        $labOrders = new LabOrders;

        $isWebView = false;
        $generate = $labOrders->generateCoverLetter($labOrderId, $isWebView);

        debug($generate);
        exit;
    }
}
