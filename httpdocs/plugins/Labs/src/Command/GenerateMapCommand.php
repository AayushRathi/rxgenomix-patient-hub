<?php
namespace Labs\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use LabOrders\Libs\LabOrders;

/**
 * GenerateMap command.
 */
class GenerateMapCommand extends Command
{
    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = $args->getArgumentAt(0);

        if (is_null($labOrderId)) {
            throw new \Exception('Please specify a lab order id');
        }

        $labOrdersInstance = new LabOrders;

        $mapTypes = $labOrdersInstance->getActiveMapTypes();

        foreach ($mapTypes as $mapKey => $mapType) {
            $generatedMap = $labOrdersInstance->generateMap($labOrderId, $mapKey);
            debug($generatedMap);
            if ($generatedMap) {
                echo 'generated ' . $mapKey . ' map for lab order #' . $labOrderId;
            } else {
                echo 'failed to generate map';
            }
        }

    }
}
