<?php
namespace Labs\Command;

use App\Application;
use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\CommandRunner;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class TestCommand
 * @package Labs\Command
 */
class TestCommand extends Command
{

    /**
     * Check Lab providers for results on lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');

        $labOrders = $labOrdersTable->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Patient.Snps',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider'
            ])
            ->where(['lab_order_status_id' => 4]);

        foreach ($labOrders as $labOrder) {
            $healthCareClient = $labOrder->provider->health_care_client;

            $labOrderReportId = 24;
            $lab = new Lab($healthCareClient->lab_id);

            // post_report_back == true/false
            if ($healthCareClient->lab->post_report_back) {
                $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
                $connectionInformation = $healthCareClient->lab->toArray();
                $connectionInformation['local_file'] = $labOrderReportPdfFileName;
                $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
                $report = file_get_contents($labOrderReportPdfFileName);
                $result = $lab->getConnector()
                    ->createConnection($connectionInformation)
                    ->post($connectionInformation, $report);
                if ($result) {
                    $labOrder->lab_order_status_id = 5;
                    $labOrdersTable->save($labOrder);

                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Post Report Back to HCC',
                        'Success'
                    );
                } else {
                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Post Report Back to HCC',
                        'Fail',
                        $result
                    );
                }
            }
        }
    }
}
