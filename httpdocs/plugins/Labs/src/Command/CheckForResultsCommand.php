<?php
namespace Labs\Command;

use App\Application;
use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\CommandRunner;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;
use PharmacogenomicProviders\Command\GetReportsCommand;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\RxNav;
use SystemManagement\Libs\Translational;

/**
 * Class CheckForResultsCommand
 * @package Labs\Command
 */
class CheckForResultsCommand extends Command
{
    /**
     * Check Lab providers for results on lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $environment = 'dev';

        if (
            !is_null($args->getArgumentAt(0)) && $args->getArgumentAt(0) === 'production'
        ) {
            $environment = 'production';
        }

        $labOrderIds = [];

        $labOrders = new LabOrders();

        if ($environment === 'dev') {
            $labOrderIds = [608];
            $isRunning = false;
        } else {
            $isRunning = $labOrders->areResultsProcessing();
        }



        if ($isRunning) {
            $io->out('Already running, exiting');
            exit;
        }

        // Create a lock file so CheckForResults doesn't run concurrently
        if (empty($labOrderIds)) {
            file_put_contents(LabOrders::LOCK_DIR . LabOrders::LOCK_FILE_ALL, posix_getpid());
        }

        // Attempt to remove the lock if PHP shuts down (not bulletproof; need to check cases where this doesn't work)
        register_shutdown_function([$labOrders, 'removeLock']);

        $labOrders->processLabOrders($labOrderIds, [
            'io' => $io
        ], $environment);
    }
}
