<?php
namespace Labs\Libs;

use Cake\ORM\TableRegistry;

/**
 * Class Lab
 * @package Labs\Libs
 */
class Lab
{

    /**
     * @var array|\Cake\Datasource\EntityInterface
     */
    public $labEntity;

    /**
     * @var
     */
    protected $driver;
    protected $connector;

    /**
     * Lab constructor.
     * @param int|null $labId
     * @throws \Exception
     */
    public function __construct(int $labId = null)
    {
        if (!$labId) {
            throw new \Exception('lab ID is required.');
        }
        $labTable = TableRegistry::getTableLocator()->get('Labs.Labs');

        $this->labEntity = $labTable->get($labId,
            [
                'contain' => [
                    'RequestDriver',
                    'ResponseDriver',
                    'ConnectionType',
                    'AuthenticationType'
                ]
            ]
        );
        $this->request_driver = new $this->labEntity->request_driver->class();
        $this->response_driver = new $this->labEntity->response_driver->class();
        $this->connector = new $this->labEntity->connection_type->class();
     }

    /**
     * @return mixed
     */
    public function getRequestDriver()
    {
        return $this->request_driver;
    }

    /**
     * @return mixed
     */
    public function getResponseDriver()
    {
        return $this->response_driver;
    }

    /**
     * @return mixed
     */
    public function getConnector()
    {
        return $this->connector;
    }
}
