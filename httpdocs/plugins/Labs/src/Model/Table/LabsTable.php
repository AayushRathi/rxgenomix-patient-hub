<?php
namespace Labs\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class LabsTable
 * @package Labs\Model\Table
 */
class LabsTable extends Lib24watchTable
{

    const LAB_ID_RESOLVE = 5;

    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'RequestDriver',
            [
                'className' => 'SystemManagement.Drivers',
                'foreignKey' => 'request_driver_id'
            ]
        );

        $this->belongsTo(
            'ResponseDriver',
            [
                'className' => 'SystemManagement.Drivers',
                'foreignKey' => 'response_driver_id'
            ]
        );

        $this->belongsTo(
            'ConnectionType',
            [
                'className' => 'SystemManagement.ConnectionTypes',
                'foreignKey' => 'connection_type_id'
            ]
        );

        $this->belongsTo(
            'AuthenticationType',
            [
                'className' => 'SystemManagement.AuthenticationTypes',
                'foreignKey' => 'authentication_type_id'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('lab_name')
            ->notEmpty('lab_name', 'Lab Name is required')
            ->requirePresence('request_driver_id')
            ->notEmpty('request_driver_id', 'Submission driver is required')
            ->requirePresence('response_driver_id')
            ->notEmpty('response_driver_id', 'Result driver is required')
            ->requirePresence('connection_type_id')
            ->notEmpty('connection_type_id', 'Connection type is required')
            ->requirePresence('authentication_type_id')
            ->notEmpty('authentication_type_id', 'Authentication type is required')
            ->requirePresence('scheme')
            ->notEmpty('scheme', 'Scheme is required')
            ->requirePresence('host')
            ->notEmpty('host', 'Host is required')
            ->requirePresence('port')
            ->notEmpty('port', 'Port is required')
            ->notEmpty('copy_file_suffix', 'Copy File Suffix is Required', function ($context) {
                if (
                    empty($context['data']['copy_file_suffix']) &&
                    (isset($context['data']['has_copy_file']) && $context['data']['has_copy_file'] == 1)
                ) {
                    return true;
                }
                return false;
            })
            ->requirePresence('username')
            ->notEmpty('username', 'Username is required')
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'Password is required', 'create')
            ->requirePresence('confirm_password', 'create')
            ->notEmpty('confirm_password', 'Confirm Password is required', 'create')
            ->sameAs('password', 'confirm_password', 'Password and Confirm Password do not match', 'create')
            ->requirePresence('panel_id')
            ->notEmpty('panel_id', 'Panel ID is required')
            ;
    }

}
