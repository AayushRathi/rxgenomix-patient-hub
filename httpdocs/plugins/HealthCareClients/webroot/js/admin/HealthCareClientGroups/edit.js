$(document).ready(function () {

    var typeField = $('#health-care-client-id');
    var subTypeField = $('#parent-id');
    var subTypeWrapper = subTypeField.closest('div.form-group');
    subTypeWrapper.hide();

    typeField.on('change', function () {
        var typeId = $(this).find(':selected').val();
        if (healthCareClientGroups[typeId]) {
            subTypeField.find('option').remove();
            subTypeField.append($('<option>').text('').attr('value', ''));
            $.each(healthCareClientGroups[typeId], function (i, value) {
                subTypeField.append($('<option>').text(value).attr('value', i));
            });
            subTypeWrapper.show();
        } else {
            subTypeField.find('option').remove();
            subTypeWrapper.hide();
        }
    });
    typeField.trigger('change');
});