$(document).ready(function () {

    showHideFields = function(feature) {
        let featureFields = $(this).closest('.feature-group').find('.feature-fields');
        if ($(this).is(':checked')) {
            featureFields.show();
        } else {
            featureFields.hide();
        }
    }

    $('.feature').on('click', function () {
        showHideFields($(this));
    });

    $.each($('.feature'), function () {
        showHideFields($(this));
    });
});
