<?php
namespace HealthCareClients\Controller\Admin;

use Cake\Http\Client;
use HealthCareClients\Controller\AppController;

/**
 * Class HealthCareClientsController
 * @package HealthCareClients\Controller\Admin
 */
class HealthCareClientsController extends AppController
{

    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'HealthCareClients' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('HealthCareClients.HealthCareClients');

        $this->loadModel('Labs.Labs');
        $this->set('labs', $this->Labs->find('list', [
            'keyField' => 'id',
            'valueField' => 'lab_name'
        ])->where(['is_active' => 1])->toArray());

        $this->loadModel('PharmacogenomicProviders.PharmacogenomicProviders');
        $this->set('pharmacogenomics', $this->PharmacogenomicProviders->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where(['is_active' => 1])->toArray());
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('healthcareclients_view', 'HealthCareClients', 'You do not have permission to access this resource.');
        $query = $this->HealthCareClients->find();
        $this->paginate($query);
        $this->set('query', $query);
    }

    /**
     * @param int|null $healthCareClientId
     * @throws \Exception
     */
    public function edit(int $healthCareClientId = null)
    {
        $this->requirePluginPermission('healthcareclients_edit', 'HealthCareClients', 'You do not have permission to access this resource.');
        if ($healthCareClientId) {
            $entity = $this->HealthCareClients->get($healthCareClientId, [
                'contain' => [
                    'CovidLabs',
                    'AssignedFeatures',
                    'AssignedFeatures.FeatureConfigurations'
                ]
            ]);

            /*
            $entity = $this->HealthCareClients->find()
                ->where([
                    'HealthCareClients.id' => $healthCareClientId
                ])
                ->contain([
                    'CovidLab'
                ])
                ->firstOrFail();
             */
        } else {
            $entity = $this->HealthCareClients->newEntity([], [
                'contain' => [
                    'CovidLabs',
                    'AssignedFeatures',
                    'AssignedFeatures.FeatureConfigurations'
                ]
            ]);
        }

        $this->set('healthCareClientId', $healthCareClientId);
        $this->set('healthCareClientEntity', $entity);

        if ($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();
            if (!isset($data['is_active'])) {
                $data['is_active'] = 0;
            }

            if (!isset($data['has_contract'])) {
                $data['has_contract'] = 0;
            }

            if (!isset($data['assigned_features'])) {
                $data['assigned_features'] = [];
            }

            $entity = $this->HealthCareClients->patchEntity($entity, $data, [
                'contain' => [
                    'AssignedFeatures',
                    'AssignedFeatures.FeatureConfigurations'
                ],
                // TODO: do assigned features save correctly
                'associated' => [
                    'CovidLabs',
                ]
            ]);

            $errors = $entity->getErrors();
            if (empty($errors)) {
                if (!$this->HealthCareClients->save($entity, [
                    'associated' => [
                        'CovidLabs'
                    ]
                ])) {
                    throw new \Exception('Could not save Health Care Client');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'HealthCareClients',
                            'controller' => 'HealthCareClients',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Health Care Client ' . (($healthCareClientId) ? 'edited.' : 'added.')
                    );
                }
            }
        }

        $this->set('daysDropdown', array_combine(range(1,28), range(1, 28)));
        $this->loadModel('Invoices.InvoiceFrequencies');
        $this->set('invoiceFrequency', $this->InvoiceFrequencies->find('dropdown'));

        $this->loadModel('PharmacogenomicProviders.PharmacogenomicProviderProcessingTargets');
        $this->set('processingTargets', $this->PharmacogenomicProviderProcessingTargets->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->toArray());

        $this->loadModel('HealthCareClients.Features');
        $features = $this->Features->find()->contain(['Fields'])->where(['is_active' => 1]);
        $this->set('features', $features);
    }

    /**
     * @param int|null $healthCareClientId
     * @throws \Exception
     */
    public function change_status(int $healthCareClientId = null)
    {
        $this->requirePluginPermission('healthcareclients_edit', 'HealthCareClients',
            'You do not have permission to edit Health Care Clients');

        $healthCareClientEntity = $this->HealthCareClients->get($healthCareClientId);

        if ($healthCareClientEntity->is_active) {
            $healthCareClientEntity->is_active = 0;
            $text = 'active';
        } else {
            $healthCareClientEntity->is_active = 1;
            $text = 'inactive';
        }

        if (!$this->HealthCareClients->save($healthCareClientEntity)) {
            throw new \Exception("Couldn't update active status for Health Care Client with ID ({$healthCareClientId})");
        }

        $this->Lib24watchLogger->write("Changed status of Health Care Client '" . $healthCareClientEntity->code . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'HealthCareClients',
                'controller' => 'HealthCareClients',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Health Care Client '" . $healthCareClientEntity->code . "' is now " . $text
        );
    }

    /**
     * @param int|null $healthCareClientId
     * @throws \Exception
     */
    public function delete(int $healthCareClientId = null)
    {
        $this->requirePluginPermission('healthcareclients_delete', 'HealthCareClients',
            'You do not have permission to delete Health Care Clients');

        $healthCareClientEntity = $this->HealthCareClients->get($healthCareClientId);

        if (!$this->HealthCareClients->delete($healthCareClientEntity)) {
            throw new \Exception("Couldn't update active status for Health Care Client with ID ({$healthCareClientId})");
        }

        $this->Lib24watchLogger->write("Deleted Health Care Client '" . $healthCareClientEntity->title . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'HealthCareClients',
                'controller' => 'HealthCareClients',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Health Care Client '" . $healthCareClientEntity->title . "' deleted."
        );
    }
}
