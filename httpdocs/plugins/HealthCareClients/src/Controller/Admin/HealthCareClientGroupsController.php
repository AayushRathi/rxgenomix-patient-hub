<?php
namespace HealthCareClients\Controller\Admin;

use HealthCareClients\Controller\AppController;

/**
 * Class HealthCareClientGroupsController
 * @package HealthCareClients\Controller\Admin
 */
class HealthCareClientGroupsController extends AppController
{

    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'HealthCareClientGroups' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('HealthCareClients.HealthCareClientGroups');
        $this->loadModel('HealthCareClients.HealthCareClients');
        $this->set('healthCareClients', $this->HealthCareClients->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->where([
            'is_active' => 1
        ])->order([
            'title' => 'ASC'
        ])->toArray());
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('healthcareclientgroups_view', 'HealthCareClients', 'You do not have permission to access this resource.');
        $query = $this->HealthCareClientGroups->find();
        $this->paginate($query);
        $this->set('query', $query);


        $this->set('groups', $this->HealthCareClientGroups->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ])->toArray());
    }

    /**
     * @param int|null $healthCareClientId
     * @throws \Exception
     */
    public function edit(int $healthCareClientGroupId = null)
    {
        $this->requirePluginPermission('healthcareclients_edit', 'HealthCareClients', 'You do not have permission to access this resource.');
        if ($healthCareClientGroupId) {
            $entity = $this->HealthCareClientGroups->get($healthCareClientGroupId, [
                'contain' => [
                ]
            ]);
        } else {
            $entity = $this->HealthCareClientGroups->newEntity();
        }

        $this->set('healthCareClientGroupId', $healthCareClientGroupId);
        $this->set('healthCareClientGroupEntity', $entity);

        if($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            $entity = $this->HealthCareClientGroups->patchEntity($entity, $data, [
                'contain' => [
                ]
            ]);

            // validate title is unique within this HCC
            $duplicates = $this->HealthCareClientGroups->find()->where([
                'health_care_client_id' => $data['health_care_client_id'],
                'title' => $data['title']
            ]);
            if ($duplicates->count() > 0) {
                $entity->setError('title', 'That name already exists for that Health Care Client.');
            }


            $errors = $entity->getErrors();
            if (empty($errors)) {
                if (!$this->HealthCareClientGroups->save($entity)) {
                    throw new \Exception('Could not save Health Care Client Group');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'HealthCareClients',
                            'controller' => 'HealthCareClientGroups',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Health Care Client Group ' . ($healthCareClientGroupId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $healthCareClientGroups = [];
        $groups = $this->HealthCareClientGroups->find();
        foreach ($groups as $group) {
            $healthCareClientGroups[$group->health_care_client_id][$group->id] = $group->title;
        }
        $this->set('healthCareClientGroups', $healthCareClientGroups);

        $groups = $this->HealthCareClientGroups->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ]);
        if ($healthCareClientGroupId) {
            $groups->where(['id IS NOT' => $healthCareClientGroupId]);
        }
        $this->set('groups', $groups->toArray());
    }

    public function change_status(int $healthCareClientGroupId){
        $this->requirePluginPermission('healthcareclients_edit', 'HealthCareClients', 'You do not have permission to access this resource.');
        if ($healthCareClientGroupId) {
            $entity = $this->HealthCareClientGroups->get($healthCareClientGroupId);
        } else {
            $this->redirectWithDefault(
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClientGroups',
                    'action' => 'index',
                    'prefix' => 'admin',
                ],
                'Could not change group status.'
            );
        }

        if($entity['is_active'] == 1){
            $entity['is_active'] = 0;
        } else {
            $entity['is_active'] = 1;
        }

        if($this->HealthCareClientGroups->save($entity)) {
            $this->redirectWithDefault(
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClientGroups',
                    'action' => 'index',
                    'prefix' => 'admin',
                ],
                'Group updated.'
            );
        } else {
            $this->redirectWithDefault(
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClientGroups',
                    'action' => 'index',
                    'prefix' => 'admin',
                ],
                'Could not change group status.'
            );
        }
    }
}