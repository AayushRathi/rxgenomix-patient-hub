<?php
namespace HealthCareClients\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'HealthCareClients', 'Health Care Clients');
    }

    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'healthcareclients_view' => 'View Health Care Clients',
            'healthcareclients_edit' => 'Edit Health Care Clients',
            'healthcareclients_delete' => 'Delete Health Care Clients',
            'healthcareclientgroups_view' => 'View Health Care Client Groups',
            'healthcareclientgroups_edit' => 'Edit Health Care Client Group'
        ];
    }
}
