<?php
namespace HealthCareClients\Controller;

use Cake\Core\Configure;

/**
 * Class HealthCareClientsController
 * @package HealthCareClients\Controller
 */
class HealthCareClientsController extends AppController
{

    /**
     * @throws \HttpRequestException
     */
    public function autocompleteHealthCareClients()
    {
        if (!$this->request->is('ajax')) {
            throw new \HttpRequestException('Invalid request');
        }
        $list = [];

        $this->loadModel('HealthCareClients.HealthCareClients');

        // auto-fill checking
        $id = $this->request->getQuery('id');
        if ($id) {
            $client = $this->HealthCareClients->get($id);
            $list[] = [
                'id' => $client->id,
                'value' => $client->title,
                'label' => $client->title,
            ];
        }

        if (!is_null($this->request->getQuery('q', null))) {
            $clientsQuery = $this->HealthCareClients->find(
                "all",
                [
                    "fields" => [
                        "HealthCareClients.id",
                        "HealthCareClients.title",
                    ],
                    "order" => [
                        "HealthCareClients.title" => "ASC",
                    ]
                ]
            );

            $clientsQuery
                ->select([
                    'id' => 'HealthCareClients.id',
                    'value' => 'HealthCareClients.title',
                    'label' => 'HealthCareClients.title'
                ])
                ->limit(20)
                ->order([
                    'HealthCareClients.title'
                ]);
            $searchValues = [mb_strtolower(trim($this->request->getQuery('q', null)))];
            foreach ($searchValues as $searchTerm) {
                $clientsQuery->andWhere(function ($exp) use ($searchTerm) {
                    return $exp->or_([
                        'HealthCareClients.title LIKE' =>
                            '%' . str_replace('%', '%%', trim($searchTerm)) . '%'
                    ]);
                });
            }
            $list = $clientsQuery->toArray();
        }

        // disable debugging
        Configure::write('debug', 0);
        \header('Content-type: application/json');
        $this->viewBuilder()->setLayout('ajax');

        echo json_encode($list);
        exit;
    }
}