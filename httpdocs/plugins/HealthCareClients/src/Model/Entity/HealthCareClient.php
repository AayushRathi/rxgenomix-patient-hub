<?php

namespace HealthCareClients\Model\Entity;

use Cake\ORM\Entity;

class HealthCareClient extends Entity
{
    const DEIDENTIFIED_FEATURE_ID = 1;

    protected function _getDeidentified()
    {
        $deIdentified = false;

        if (!isset($this->assigned_features) || empty($this->assigned_features)) {
            return $deIdentified;
        }

        foreach ($this->assigned_features as $feature) {
            if ($feature->feature_id === self::DEIDENTIFIED_FEATURE_ID) {
                $deIdentified = true;
                break;
            }
        }

        return $deIdentified;
    }
}
