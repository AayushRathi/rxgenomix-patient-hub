<?php
namespace HealthCareClients\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class FeatureFieldsTable
 * @package HealthCareClients\Model\Table
 */
class FeatureFieldsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        /*$this->belongsTo(
            'Fields',
            [
                'className' => 'HealthCareClients.FeatureFields',
                'foreignKey' => 'feature_id'
            ]
        );*/
    }
}
