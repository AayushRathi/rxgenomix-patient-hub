<?php
namespace HealthCareClients\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class HealthCareClientsTable
 * @package HealthCareClients\Model\Table
 */
class HealthCareClientFeaturesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'Features',
            [
                'className' => 'HealthCareClients.Features',
                'foreignKey' => 'feature_id'
            ]
        );

        $this->hasMany(
            'FeatureConfigurations',
            [
                'className' => 'HealthCareClients.HealthCareClientFeatureConfiguration',
                'foreignKey' => 'health_care_client_feature_id',
                'saveStrategy' => 'replace'
            ]
        );
    }

    /**
     * @param int|null $healthCareClientId
     * @param string|null $featureCode
     * @return bool
     */
    public function hasFeature(int $healthCareClientId = null, string $featureCode = null) :bool
    {
        return ($this->find()
            ->where(['health_care_client_id' => $healthCareClientId])
            ->matching('Features', function ($query) use ($featureCode) {
                return $query->where(['code' => $featureCode]);
            })
            ->count() > 0);
    }
}
