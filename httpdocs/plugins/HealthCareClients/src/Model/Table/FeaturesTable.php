<?php
namespace HealthCareClients\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class HealthCareClientsTable
 * @package HealthCareClients\Model\Table
 */
class FeaturesTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('features');

        $this->hasMany(
            'Fields',
            [
                'className' => 'HealthCareClients.FeatureFields',
                'foreignKey' => 'feature_id'
            ]
        );
    }
}
