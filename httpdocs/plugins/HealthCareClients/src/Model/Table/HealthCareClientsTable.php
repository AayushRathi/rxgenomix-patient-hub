<?php
namespace HealthCareClients\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class HealthCareClientsTable
 * @package HealthCareClients\Model\Table
 */
class HealthCareClientsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Lab',
            [
                'className' => 'Labs.Labs',
                'foreignKey' => 'lab_id'
            ]
        );

        $this->belongsTo(
            'CovidLabs',
            [
                'className' => 'Labs.Labs',
                'foreignKey' => 'covid_lab_id'
            ]
        );

        $this->belongsTo(
            'PharmacogenomicProvider',
            [
                'className' => 'PharmacogenomicProviders.PharmacogenomicProviders',
                'foreignKey' => 'pharmacogenomics_provider_id'
            ]
        );

        $this->hasMany(
            'AssignedFeatures',
            [
                'className' => 'HealthCareClients.HealthCareClientFeatures',
                'foreignKey' => 'health_care_client_id',
                'saveStrategy' => 'replace'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('title')
            ->notEmpty('title', 'Name is required')
            ->requirePresence('lab_id')
            ->notEmpty('lab_id', 'Lab Provider is required')
            ->requirePresence('pharmacogenomics_provider_id')
            ->notEmpty('pharmacogenomics_provider_id', 'Bioinformatics Provider is required')
            ->requirePresence('fee_per_lab_order')
            ->notEmpty('fee_per_lab_order', 'Fee per lab order is required')
            ->requirePresence('invoice_day_of_month')
            ->notEmpty('invoice_day_of_month', 'Day of Month to Invoice is required')
            ->requirePresence('invoice_email')
            ->notEmpty('invoice_email', 'Invoice email is required')
            ->email('invoice_email', false, 'Invoice email address must be a valid email address.')
            ->requirePresence('invoice_frequency_id')
            ->notEmpty('invoice_frequency_id', 'Invoice frequency is required')
            ;
    }
}
