<?php
namespace HealthCareClients\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class HealthCareClientGroupsTable
 * @package HealthCareClients\Model\Table
 */
class HealthCareClientGroupsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'HealthCareClient',
            [
                'className' => 'HealthCareClients.HealthCareClients',
                'foreignKey' => 'health_care_client_id'
            ]
        );

        $this->hasMany(
            'ChildGroups',
            [
                'className' => 'HealthCareClients.HealthCareClientGroups',
                'foreignKey' => 'parent_id',
                'bindingKey' => 'id'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('title')
            ->notEmpty('title', 'Name is required')
            ->requirePresence('health_care_client_id')
            ->notEmpty('health_care_client_id', 'Health Care Client is required')
            ;
    }

    /**
     * @param int|null $healthCareClientId
     * @return array|bool|\Cake\ORM\Query
     */
    public function getGroupsByHealthCareClientId(int $healthCareClientId = null)
    {
        if ($healthCareClientId) {
            return $this->find()
                ->contain(['ChildGroups'])
                ->where([
                'health_care_client_id' => $healthCareClientId,
                'is_active' => 1
            ])->order([
                'parent_id' => 'ASC',
                'title' => 'ASC'
            ]);
        }
        return false;
    }
}
