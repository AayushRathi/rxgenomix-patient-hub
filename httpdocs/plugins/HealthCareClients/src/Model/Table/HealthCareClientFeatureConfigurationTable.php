<?php
namespace HealthCareClients\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class HealthCareClientFeatureConfigurationTable
 * @package HealthCareClients\Model\Table
 */
class HealthCareClientFeatureConfigurationTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->belongsTo(
            'HealthCareClient',
            [
                'className' => 'HealthCareClients.HealthCareClients',
                'foreignKey' => 'health_care_client_id'
            ]
        );

        $this->belongsTo(
            'Field',
            [
                'className' => 'HealthCareClients.FeatureFields',
                'foreignKey' => 'field_id'
            ]
        );
    }
}
