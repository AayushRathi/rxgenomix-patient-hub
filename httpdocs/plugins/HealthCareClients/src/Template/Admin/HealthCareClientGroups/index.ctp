<?php
$pluginTitle = $lib24watchModules['HealthCareClients']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Health Care Client Groups';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Health Care Client Groups',
                'items' => [
                    [
                        'name' => 'title',
                        'type' => 'text',
                        'label' => 'Name'
                    ],
                    [
                        'name' => 'health_care_client_id',
                        'type' => 'select',
                        'label' => 'Health Care CLient',
                        'empty' => true,
                        'options' => $healthCareClients
                    ]
                ],
                $query,
                $this
            ]);                
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'HealthCareClients',
                            'controller' => 'HealthCareClientGroups',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Group'
                        ]
                    )
                ]
            );
            ?>            
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'title',
                                        'label' => 'Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'health_care_client_id',
                                        'label' => 'Health Care Client',
                                        'type' => 'lookup',
                                        'list' => $healthCareClients
                                    ],
                                    [
                                        'name' => 'parent_id',
                                        'label' => 'Parent Group',
                                        'type' => 'lookup',
                                        'list' => $groups
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'HealthCareClients',
                                                'controller' => 'HealthCareClientGroups',
                                                'row' => $row
                                            ]);
                                        }
                                    ]                               
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to 
                            <?php
                            echo $this->Html->link(
                                'Add a Health Care Client Group',
                                [
                                    'plugin' => 'HealthCareClients',
                                    'controller' => 'HealthCareClientGroups',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Health Care Client Group'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
