<?php
$pluginTitle = $lib24watchModules['Providers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Health Care Client Groups"),
    [
        'plugin' => 'HealthCareClients',
        'controller' => 'HealthCareClientGroups',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($healthCareClientGroupId ? "Edit" : "Add") . " Health Care Client Group";
$this->Breadcrumbs->add($pageHeading);

echo $this->Html->scriptBlock('var healthCareClientGroups = ' . json_encode($healthCareClientGroups));
$this->Html->script('/health_care_clients/js/admin/HealthCareClientGroups/edit.js', ['block' => 'extra-scripts']);
?>
<div class="col-md-12">
	<div class="row">
		<div class="panel panel-default tabs">
			<?php
				echo $this->Form->create($healthCareClientGroupEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
			?>
			<div class="panel-body">
				<div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
                            echo $this->Form->control(
                                'title',
                                [
                                    'type' => 'text',
                                    'label' => 'Name'
                                ]
                            );
            				echo $this->Form->control(
            					'health_care_client_id',
            					[
            						'type' => 'select',
            						'label' => 'Health Care Client',
                                    'empty' => true,
                                    'options' => $healthCareClients
            					]
            				);
            				echo $this->Form->control(
            					'parent_id',
            					[
            						'type' => 'select',
            						'label' => 'Parent Group',
                                    'empty' => true,
                                    'options' => $groups
            					]
            				);
                            echo $this->Form->control(
                                'is_active',
                                [
                                    'type' => 'checkbox',
                                    'label' => 'Active'
                                ]
                            );
                        ?>
                    </div>
            	</div>
            </div>
            <?php
            	$footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
            	echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
