<?php
$pluginTitle = $lib24watchModules['HealthCareClients']['title'];
$this->Breadcrumbs->add($pluginTitle);
$pageHeading = 'Browse Health Care Clients';
$this->Breadcrumbs->add($pageHeading);
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo $this->cell('Lib24watch.Table::filter', [
                'Health Care Clients',
                'items' => [
                    [
                        'name' => 'title',
                        'type' => 'text',
                        'label' => 'Name',
                        'function' => function ($q, $value) {
                            return $q->where(['title LIKE' => '%' . $value . '%']);
                        }
                    ],
                    [
                        'name' => 'lab_id',
                        'type' => 'select',
                        'label' => 'Lab Provider',
                        'empty' => true,
                        'options' => $labs
                    ],
                    [
                        'name' => 'pharmacogenomics_provider_id',
                        'type' => 'select',
                        'label' => 'Bio-Informatics Provider',
                        'empty' => true,
                        'options' => $pharmacogenomics
                    ],
                    [
                        'name' => 'has_contract',
                        'type' => 'select',
                        'label' => 'Has Contract',
                        'empty' => true,
                        'options' => ['1' => 'Yes', '0' => 'No']
                    ],
                    [
                        'name' => 'is_active',
                        'type' => 'select',
                        'label' => 'Active',
                        'empty' => true,
                        'options' => ['1' => 'Yes', '0' => 'No']
                    ]
                ],
                $query,
                $this
            ]);                
        ?>
        <div class="panel panel-default">
            <?php
            echo $this->cell(
                'Lib24watch.Panel::heading', [
                    'header' => $pageHeading,
                    'showPagination' => false,
                    'extraControls' => $this->Html->link(
                        "<span class=\"fa fa-plus\"></span>",
                        [
                            'plugin' => 'HealthCareClients',
                            'controller' => 'HealthCareClients',
                            'action' => 'edit',
                            'prefix' => 'admin'
                        ],
                        [
                            'class' => 'panel-action',
                            'escape' => false,
                            'title' => 'Add Health Care Client'
                        ]
                    )
                ]
            );
            ?>            
            <div class="panel-body">
                <div class="table-responsive">
                   <?php
                    if ($query->count() > 0) {
                        echo $this->cell(
                            'Lib24watch.Table::display',
                            [
                                '',
                                'items' => [
                                    [
                                        'name' => 'title',
                                        'label' => 'Name',
                                        'type' => 'text'
                                    ],
                                    [
                                        'name' => 'lab_id',
                                        'label' => 'Lab Provider',
                                        'type' => 'lookup',
                                        'list' => $labs
                                    ],
                                    [
                                        'name' => 'pharmacogenomics_provider_id',
                                        'label' => 'Bioinformatics Provider',
                                        'type' => 'lookup',
                                        'list' => $pharmacogenomics
                                    ],
                                    [
                                        'name' => 'has_contract',
                                        'label' => 'Has Contract',
                                        'type' => 'status'
                                    ],
                                    [
                                        'name' => 'is_active',
                                        'label' => 'Active',
                                        'type' => 'status'
                                    ],
                                    [
                                        'name' => 'action',
                                        'label' => '',
                                        'tdClass' => 'actions',
                                        'type' => 'custom',
                                        'function' => function ($row) {
                                            return $this->element('SystemManagement.SystemManagement/actions', [
                                                'plugin' => 'HealthCareClients',
                                                'controller' => 'HealthCareClients',
                                                'row' => $row
                                            ]);
                                        }
                                    ]                               
                                ],
                                $query
                            ]
                        );
                    } else {
                        ?>
                        <p>
                            No results, would you like to 
                            <?php
                            echo $this->Html->link(
                                'Add a Health Care Client',
                                [
                                    'plugin' => 'HealthCareClients',
                                    'controller' => 'HealthCareClients',
                                    'action' => 'edit',
                                    'prefix' => 'admin'
                                ],
                                [
                                    'title' => 'Add a Health Care Client'
                                ]
                            );
                            ?>.
                        </p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            echo $this->cell('Lib24watch.Panel::footer', 
                ['submit' => false, 'showPagination' => true, false, []]
            )
            ?>
        </div>
    </div>
</div>
