<?php

/**
 * Variables defined outside of CTP:
 *
 * @var $healthCareClientId
 * @var $healthCareClientEntity
 * @var $currentUrl
 * @var $labs
 * @var $pharmacogenomics
 * @var $processingTargets
 * @var $invoiceFrequency
 * @var $daysDropdown
 */

$pluginTitle = $lib24watchModules['Providers']['title'];
$this->Breadcrumbs->add($pluginTitle);
$this->Breadcrumbs->add(
    \Cake\Utility\Inflector::pluralize("Browse Health Care Clients"),
    [
        'plugin' => 'HealthCareClients',
        'controller' => 'HealthCareClients',
        'action' => 'index',
        'prefix' => 'admin'
    ]
);
$pageHeading = ($healthCareClientId ? "Edit" : "Add") . " Health Care Client";
$this->Breadcrumbs->add($pageHeading);

//echo $this->Html->scriptBlock('var featureFields = ' . json_encode($features));
$this->Html->script('/health_care_clients/js/admin/HealthCareClients/edit.js', ['block' => 'extra-scripts']);
?>
<div class="col-md-12">
        <div class="row">
                <div class="panel panel-default tabs">
                        <?php
                                echo $this->Form->create($healthCareClientEntity, ['url' => $currentUrl, 'novalidate' => true]);
                echo $this->cell('Lib24watch.Panel::heading', ['header' => $pageHeading]);
                        ?>
                        <div class="panel-body">
                                <div class="row">
                    <div class="col-md-10">
                        <h3>General Information</h3>
                        <?php
                        echo $this->Form->control(
                            'title',
                            [
                                'type' => 'text',
                                'label' => 'Name'
                            ]
                        );
                        echo $this->Form->control(
                            'lab_id',
                            [
                                'type' => 'select',
                                'label' => 'Lab Provider',
                                'empty' => true,
                                'options' => $labs
                            ]
                        );

                        echo $this->Form->control(
                            'covid_lab_id',
                            [
                                'type' => 'select',
                                'label' => 'COVID Lab Provider',
                                'empty' => true,
                                'options' => $labs
                            ]
                        );

                        echo $this->Form->control(
                            'crestar_client_id',
                            [
                                'type' => 'text',
                                'label' => 'Lab Client ID'
                            ]
                        );

                        echo $this->Form->control(
                            'pharmacogenomics_provider_id',
                            [
                                'type' => 'select',
                                'label' => 'Bioinformatics Provider',
                                'empty' => true,
                                'options' => $pharmacogenomics
                            ]
                        );

                        echo $this->Form->control(
                            'pharmacogenomics_provider_processing_target_id',
                            [
                                'type' => 'select',
                                'label' => 'Bioinformatics Permission Association',
                                'empty' => true,
                                'options' => $processingTargets
                            ]
                        );

                        echo $this->Form->control(
                            'fee_per_lab_order',
                            [
                                'type' => 'text',
                                'label' => 'Fee per Lab Order'
                            ]
                        );

                        echo $this->Form->control(
                            'invoice_frequency_id',
                            [
                                'type' => 'select',
                                'label' => 'Invoice Frequency',
                                'empty' => true,
                                'options' => $invoiceFrequency
                            ]
                        );
                        echo $this->Form->control(
                            'invoice_day_of_month',
                            [
                                'type' => 'select',
                                'label' => 'Day of Month To Invoice',
                                'empty' => true,
                                'options' => $daysDropdown
                            ]
                        );
                        echo $this->Form->control(
                            'invoice_email',
                            [
                                'type' => 'text',
                                'label' => 'Invoice Email Address'
                            ]
                        );
                        echo $this->Form->control(
                            'has_contract',
                            [
                                'type' => 'checkbox',
                                'label' => 'Has Contract'
                            ]
                        );
                        echo $this->Form->control(
                            'is_active',
                            [
                                'type' => 'checkbox',
                                'label' => 'Active'
                            ]
                        );
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <h3>Configuration</h3>
                        <?php
                        $featureData = [];
                        $selectedFeatureIds = [];
                        foreach ((array)$healthCareClientEntity->assigned_features as $feature) {
                            $selectedFeatureIds[] = $feature->feature_id;
                            $featureData[$feature->id] = [];
                            foreach ($feature->feature_configurations as $field) {
                                $featureData[$feature->id][$field->id] = $field->field_value;
                            }
                        }

                        foreach ($features as $featureIndex => $feature) {
                            ?>
                            <div class="feature-group">
                                <?php
                                $checked = false;

                                if (in_array($feature->id, $selectedFeatureIds)) {
                                    $checked = true;
                                }

                                echo $this->Form->control(
                                    'assigned_features['.$featureIndex.'][feature_id]',
                                    [
                                        'type' => 'checkbox',
                                        'label' => $feature->title,
                                        'value' => $feature->id,
                                        'class' => 'feature',
                                        'checked' => $checked
                                    ]
                                );
                                ?>
                                <div class="feature-fields">
                                    <?php
                                    foreach ($feature->fields as $index => $field) {

                                        echo $this->Form->control(
                                            'feature_configurations['.$index.'][feature_field_id]',
                                            [
                                                'type' => 'hidden',
                                                'value' => $field->id
                                            ]
                                        );
                                        $options = [
                                            'type' => $field->type,
                                            'label' => $field->label,
                                            'value' => $featureData[$feature->id][$field->id] ?? ''
                                        ];

                                        if ($field->type == 'select' && $field->options) {
                                            $options['options'] = json_decode($field->options);
                                        }
                                        echo $this->Form->control(
                                            'feature_configurations['.$index.'][field_value]',
                                            $options
                                        );
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
                $footer = [
                    'submit' => 'Save',
                    'showPagination' => false
                ];
                echo $this->cell('Lib24watch.Panel::footer', $footer);
                echo $this->Form->end();
            ?>
        </div>
    </div>
</div>
