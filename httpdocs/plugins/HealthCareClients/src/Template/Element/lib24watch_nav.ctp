<a href="javascript:void(0)"><span class="fa fa-hospital-o"></span><span class="xn-text"><?php echo h($lib24watchModules['HealthCareClients']['title']); ?></span></a>
<ul>
    <li class="xn-title">Clients</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('healthcareclients_view', 'HealthCareClients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Health Care Clients",
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClients',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Health Care Clients'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('healthcareclients_edit', 'HealthCareClients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Health Care Client",
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClients',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Health Care Client'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
    <li class="xn-title">Groups</li>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('healthcareclientgroups_view', 'HealthCareClients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Groups",
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClientGroups',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Health Care Client Groups'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('healthcareclientgroups_edit', 'HealthCareClients')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Group",
                [
                    'plugin' => 'HealthCareClients',
                    'controller' => 'HealthCareClientGroups',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Health Care Client Group'
                ]
            );
            ?>
        </li>
        <?php
    }
    ?>
</ul>