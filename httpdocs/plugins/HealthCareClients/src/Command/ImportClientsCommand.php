<?php
namespace HealthCareClients\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;

/**
 * Class ImportClientsCommand
 * @package HealthCareClients\Command
 */
class ImportClientsCommand extends Command
{

    /**
     * Initial Import for Clients
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $start = microtime(true);

        $csvs = ['health-care-clients.csv', 'hospital-list.csv'];

        $defaults = [
            'lab_id' => 1,
            'pharmacogenomics_provider_id' => 1,
            'is_active' => 1,
            'pharmacogenomics_provider_processing_target_id' => 1,
            'fee_per_lab_order' => 0,
            'has_contract' => 0,
            'invoice_day_of_month' => 1,
            'invoice_email' => 'noreply@temp.com',
            'invoice_frequency_id' => 1
        ];

        foreach ($csvs as $csv) {
            $csv = ROOT . '/data/' . $csv;

            $handle = fopen($csv, 'r');

            $table = TableRegistry::getTableLocator()->get('HealthCareClients.HealthCareClients');

            while (($data = fgetcsv($handle, 0, ',')) !== false) {
                $title = trim($data[0]);
                if ($title != '') {
                    $entity = $table->find()->where(['title' => $title])->first();

                    if (!$entity) {
                        $entity = $table->newEntity($defaults);
                        $entity->title = $title;

                        if(!$table->save($entity)) {
                            $io->error('Could not save entity: ' . $title);
                        }
                    }
                }
            }
        }

        $end = microtime(true);
        $time = $end - $start;
        $io->success('Import Completed. Took ' . round($time, 4) . 's');
    }
}