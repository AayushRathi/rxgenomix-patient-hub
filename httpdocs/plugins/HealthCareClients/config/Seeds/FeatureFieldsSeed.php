<?php
use Migrations\AbstractSeed;

/**
 * Features seed.
 */
class FeatureFieldsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'feature_id' => 2,
                'field_name' => 'api_endpoint',
                'label' => 'API Endpoint',
                'type' => 'text',
            ],
            [
                'feature_id' => 2,
                'field_name' => 'api_username',
                'label' => 'API Username',
                'type' => 'text',
            ],
            [
                'feature_id' => 2,
                'field_name' => 'api_password',
                'label' => 'API Password',
                'type' => 'text',
            ],
        ];

        $table = $this->table('feature_fields');
        $table->insert($data)->save();
    }
}
