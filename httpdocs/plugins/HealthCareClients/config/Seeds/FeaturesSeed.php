<?php
use Migrations\AbstractSeed;

/**
 * Features seed.
 */
class FeaturesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'title' => 'De-identified data sent to Lab',
                'code' => 'de-identified-lab-data',
                'is_active' => 1,
                'display_order' => 0,
            ],
            [
                'title' => 'Report ready API call',
                'code' => 'report-ready-api-call',
                'is_active' => 1,
                'display_order' => 1,
            ],
        ];

        $table = $this->table('features');
        $table->insert($data)->save();
    }
}
