<?php
use Migrations\AbstractMigration;

class AddCovidLabToHealthCareClients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('health_care_clients')
            ->addColumn('covid_lab_id', 'integer', [
                'limit' => 10,
                'signed' => false,
                'null' => true,
                'default' => null
            ])
            ->addForeignKey('covid_lab_id', 'labs', 'id')
            ->update();
    }
}
