<?php
use Migrations\AbstractMigration;

class CreateHealthCareClientFeatures extends AbstractMigration
{
    public $autoId = false;

    public function up()
    {
        $this->table('health_care_client_features')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('health_care_client_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
            ])->addColumn('feature_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->create();
    }

    public function down()
    {
        $this->table('health_care_client_features')->drop()->save();
    }
}
