<?php
use Migrations\AbstractMigration;

class CreateHealthCareClientFeatureConfiguration extends AbstractMigration
{
    public $autoId = false;

    public function up()
    {
        $this->table('health_care_client_feature_configuration')
            ->addColumn('id', 'integer', [
            'autoIncrement' => true,
            'default' => null,
            'limit' => 10,
            'null' => false,
            'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('health_care_client_feature_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('feature_field_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('field_value', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->create();
    }

    public function down()
    {
        $this->table('health_care_client_feature_configuration')->drop()->save();
    }
}
