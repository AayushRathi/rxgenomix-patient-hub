<?php
use Migrations\AbstractMigration;

class AddHasContractToHealthCareClients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('health_care_clients');
        $table->addColumn('has_contract', 'boolean', [
            'default' => null,
            'null' => false,
        ]);
        $table->update();
    }
}
