<?php
use Migrations\AbstractMigration;

class CreateFeatureFields extends AbstractMigration
{
    public $autoId = false;

    public function up()
    {
        $this->table('feature_fields')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('feature_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('label', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('field_name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('type', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('options', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->create();
    }

    public function down()
    {
        $this->table('feature_fields')->drop()->save();
    }
}
