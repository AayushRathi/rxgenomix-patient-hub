<?php
use Migrations\AbstractMigration;

class AddFeePerLabOrderToHealthCareClients extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('health_care_clients');
        $table->addColumn('fee_per_lab_order', 'decimal', [
            'default' => null,
            'null' => false,
        ]);
        $table->update();
    }
}
