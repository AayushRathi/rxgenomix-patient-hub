<?php
use Migrations\AbstractMigration;

class CreateFeatures extends AbstractMigration
{

    public $autoId = false;

    public function up()
    {
        $this->table('features')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 10,
                'null' => false,
                'signed' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('code', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('is_active', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('display_order', 'integer', [
                'default' => '0',
                'limit' => 10,
                'null' => true,
                'signed' => false,
            ])
            ->create();
    }

    public function down()
    {
        $this->table('features')->drop()->save();
    }
}
