<?php
namespace PharmacogenomicProviders\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\Translational;

/**
 * Class LabOrderPostProcessCommand
 * @package PharmacogenomicProviders\Command
 */
class LabOrderPostProcessCommand extends Command
{

    /**
     * Check PgX providers for updated data on patient and lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrderId = null;
        #$labOrderId = 453;

        if (is_null($labOrderId) || $labOrderId <= 0) {
            throw new \Exception('Specify a lab order');
        }

        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');

        // This should already be filled in
        #LabOrders::fillInMedications($labOrderId);

        $labOrder = $labOrdersTable->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Medications',
                'Medications.Drug',
                'Patient.Snps',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.Lab',
                'Provider.HealthCareClient.PharmacogenomicProvider'
            ])
            ->where(['LabOrders.id' => $labOrderId])
            ->firstOrFail();

        // attempt to detect prior consolidated records
        $consolidatedGeneticReportsTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.ConsolidatedGeneticReports');
        $memberId = $labOrder->patient->getMemberId();
        $existingRecords = $consolidatedGeneticReportsTable
            ->find()
            ->where([
                'member_id' => $memberId
            ])
            ->toArray();

        debug('existing consolidated records');
        debug(count($existingRecords));

        #debug($existingRecords);
        #exit;

        $healthCareClient = $labOrder->provider->health_care_client;

        $isProduction = false;
        if (!is_null($args->getArgumentAt(0)) && $args->getArgumentAt(0) === 'production') {
            $isProduction = true;
        }

        debug('is production');
        debug($isProduction);

        // get the reports, save and post back to Lab (if applicable)
        $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();

        $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

        $compCode = '';

        if ($isProduction) {
            $compCode = 'CRESTAR_COMP';
        }

        $useNewMedications = false;
        $payload = $pharmacogenomicProvider->getDriver()->createReport($labOrder, $useNewMedications, $compCode);

        debug($payload);

        $pharmacogenomicConnectionInformation['headers'] = [
            'Accept' => 'application/json',
            //'Content-Type' => 'application/json',
            'Cache-Control' => 'no-cache',
        ];

        $pharmacogenomicConnectionInformation['timeout'] = 60*6; // If (useNewMedications is true), PDF report will be embedded in the response. If useNewMedications=true, response time may be up to 3 minutes while a new report is generated. 
        $pharmacogenomicConnectionInformation['submit_path'] = $pharmacogenomicConnectionInformation['request_path'];

        $report = $pharmacogenomicProvider
            ->getConnector()
            ->createConnection($pharmacogenomicConnectionInformation)
            ->post($pharmacogenomicConnectionInformation, $payload);

        $validRequest = Translational::validateBundleByType('message', $report);

        $labOrderReportId = 0;

        if ($validRequest) {
            $labOrderReportId = $pharmacogenomicProvider->getDriver()->parseReport($labOrder->id, $report);

            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Lab Report Parsed',
                'Success'
            );

            $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';

            $diagnosticReport = Translational::getResource($report, 'DiagnosticReport');

            if ($diagnosticReport) {
                try {
                    Translational::savePdf($labOrderReportPdfFileName, $diagnosticReport['resource']['presentedForm'][0] ?? []);

                    \SystemManagement\Libs\EventLogs::write(
                        $healthCareClient->id,
                        $labOrder->id,
                        $labOrder->lab_order_status_id,
                        'Lab Report PDF Downloaded',
                        'Success'
                    );

                } catch (\Exception $e) {
                    var_dump($e->getMessage());

		    \SystemManagement\Libs\EventLogs::write(
			$healthCareClient->id,
			$labOrder->id,
			$labOrder->lab_order_status_id,
			'Lab Report PDF Failed to downloaded',
			'Success'
		    );
                }

                /*
                // generate cover letter
                if (is_readable($labOrderReportPdfFileName)) {
                    $labOrdersInstance = new LabOrders;
                    $isWebView = false;
                    $useExisting = false;

                    $labOrdersInstance->generateCoverLetter($labOrder->id, $isWebView, $useExisting);
                }*/
            }
        } else {
            \SystemManagement\Libs\EventLogs::write(
                $healthCareClient->id,
                $labOrder->id,
                $labOrder->lab_order_status_id,
                'Unable to retrieve lab report',
                'Error'
            );
        }

        // post_report_back == true/false
        if ($isProduction && $validRequest && $healthCareClient->lab->post_report_back && $labOrderReportId) {
            $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
            $connectionInformation = $healthCareClient->lab->toArray();
            $connectionInformation['local_file'] = $labOrderReportPdfFileName;
            $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
            $report = file_get_contents($labOrderReportPdfFileName);
            $lab = new Lab($healthCareClient->lab_id);
            $result = $lab->getConnector()
                ->createConnection($connectionInformation)
                ->post($connectionInformation, $report);
            if ($result) {
                $labOrder->lab_order_status_id = 5;
                $labOrdersTable->save($labOrder);

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Post Report Back to Lab for QC',
                    'Success'
                );
            } else {
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Post Report Back to Lab for QC',
                    'Fail',
                    $result
                );
            }
        }

        return $labOrderReportId;
    }
}
