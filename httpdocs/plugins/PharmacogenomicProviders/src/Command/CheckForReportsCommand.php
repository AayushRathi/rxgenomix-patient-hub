<?php
namespace PharmacogenomicProviders\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Labs\Libs\Lab;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;
use SystemManagement\Libs\Translational;

/**
 * Class CheckForReportsCommand
 * @package PharmacogenomicProviders\Command
 */
class CheckForReportsCommand extends Command
{

    /**
     * Check PgX providers for updated data on patient and lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $labOrdersTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrders');
        $labOrderReportsTable = TableRegistry::getTableLocator()->get('LabOrders.LabOrderReports');
        $drugsTable = TableRegistry::getTableLocator()->get('SystemManagement.Drugs');

        $labOrders = $labOrdersTable->find()
            ->contain([
                'Patient.Sex',
                'Patient.Medications',
                'Patient.Snps',
                'Medications',
                'Medications.Drug',
                'Patient.GeneCnvs',
                'Patient.Lifestyles',
                'Provider.HealthCareClient.PharmacogenomicProvider',
                'Provider.HealthCareClient.Lab'
            ])
            ->where([
                'LabOrders.uuid IS NOT' => null,
                'LabOrders.lab_order_status_id >' => 3
            ]);

        foreach ($labOrders as $labOrder) {
            $healthCareClient = $labOrder->provider->health_care_client;

            $tsiId = 2;
            if ($healthCareClient->pharmacogenomics_provider_id != $tsiId) {
                continue;
            }

            $pharmacogenomicProvider = new PharmacogenomicProvider($healthCareClient->pharmacogenomics_provider_id);

            // get the reports, save and post back to Lab (if applicable)
            $pharmacogenomicConnectionInformation = $healthCareClient->pharmacogenomic_provider->toArray();

            $useNewMedications = true;
            $payload = $pharmacogenomicProvider->getDriver()->createReport($labOrder, $useNewMedications);

            $pharmacogenomicConnectionInformation['headers'] = [ 
                'Accept' => 'application/json',
            ];
            $pharmacogenomicConnectionInformation['submit_path'] = $pharmacogenomicConnectionInformation['request_path'];
            $pharmacogenomicConnectionInformation['timeout'] = 60*6; // If (useNewMedications is true), PDF report will be embedded in the response. If useNewMedications=true, response time may be up to 3 minutes while a new report is generated. 

            $report = $pharmacogenomicProvider
                ->getConnector()
                ->createConnection($pharmacogenomicConnectionInformation)
                ->post($pharmacogenomicConnectionInformation, $payload);

            $validRequest = Translational::validateBundleByType('message', $report);

            $labOrderReportId = 0;

            if ($validRequest) {
                $labOrderReportId = $pharmacogenomicProvider->getDriver()->parseReport($labOrder->id, $report);

                file_put_contents(ROOT . '/data/report_raw_' . $labOrderReportId . '.txt', print_r($report, true));

                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Lab Report Parsed',
                    'Success'
                );

                $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';

                $diagnosticReport = Translational::getResource($report, 'DiagnosticReport');

                if ($diagnosticReport) {
                    try {
                        Translational::savePdf($labOrderReportPdfFileName, $diagnosticReport['resource']['presentedForm'][0] ?? []);
                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Lab Report PDF Downloaded',
                            'Success'
                        );
                    } catch (\Exception $e) {
                        var_dump($e->getMessage());

                        \SystemManagement\Libs\EventLogs::write(
                            $healthCareClient->id,
                            $labOrder->id,
                            $labOrder->lab_order_status_id,
                            'Lab Report PDF Failed to downloaded',
                            'Success'
                        );
                    }

                    // post_report_back == true/false
                    if ($healthCareClient->lab->post_report_back && $labOrderReportId) {
                        $lab = new Lab($healthCareClient->lab_id);
                        $labOrderReportPdfFileName = ROOT . '/data/lab_orders/reports/' . $labOrderReportId . '.pdf';
                        $connectionInformation = $healthCareClient->lab->toArray();
                        $connectionInformation['local_file'] = $labOrderReportPdfFileName;
                        $connectionInformation['remote_file'] = $healthCareClient->lab->report_path . '/' . $labOrder->sample_id . '.pdf';
                        $report = file_get_contents($labOrderReportPdfFileName);
                        $result = $lab->getConnector()
                            ->createConnection($connectionInformation)
                            ->post($connectionInformation, $report);
                        if ($result) {
                            $labOrder->lab_order_status_id = 5;
                            $labOrdersTable->save($labOrder);

                            \SystemManagement\Libs\EventLogs::write(
                                $healthCareClient->id,
                                $labOrder->id,
                                $labOrder->lab_order_status_id,
                                'Post Report Back to Lab for QC',
                                'Success'
                            );
                        } else {
                            \SystemManagement\Libs\EventLogs::write(
                                $healthCareClient->id,
                                $labOrder->id,
                                $labOrder->lab_order_status_id,
                                'Post Report Back to Lab for QC',
                                'Fail',
                                $result
                            );
                        }
                    }
                }

            } else {
                #file_put_contents(ROOT . '/data/last_failed_report_response.txt', print_r($report, true));
                \SystemManagement\Libs\EventLogs::write(
                    $healthCareClient->id,
                    $labOrder->id,
                    $labOrder->lab_order_status_id,
                    'Unable to retrieve lab report',
                    'Error'
                );
            }
        }
    }
}
