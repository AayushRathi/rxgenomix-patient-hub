<?php
namespace PharmacogenomicProviders\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use PharmacogenomicProviders\Libs\PharmacogenomicProvider;

/**
 * Class GetProcessingTargetsCommand
 * @package PharmacogenomicProviders\Command
 */
class GetProcessingTargetsCommand extends Command
{

    /**
     * Check PgX providers for updated data on patient and lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $io->out('Starting Job');
        $start = microtime(true);
        $pharmacogenomicProvidersTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');
        $pharmacogenomicProviderProcessingTargetsTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviderProcessingTargets');

        $pgxProviders = $pharmacogenomicProvidersTable->find()
            ->where(['is_active' => 1]);
        $io->out('Found ' . $pgxProviders->count() . ' providers with post processing targets.');
        foreach ($pgxProviders as $provider) {
            if ($provider->postprocessing_path) {

                $pharmacogenomicProvider = new PharmacogenomicProvider($provider->id);
                $connectionInformation = $provider->toArray();
                $connectionInformation['request_path'] = $connectionInformation['postprocessing_path'];
                $results = $pharmacogenomicProvider->getConnector()->createConnection($connectionInformation)->get($connectionInformation);
                foreach ($results as $result) {
                    $targetEntity = $pharmacogenomicProviderProcessingTargetsTable->find()->where(['target' => $result['id']])->first();
                    if (!$targetEntity) {
                        $targetEntity = $pharmacogenomicProviderProcessingTargetsTable->newEntity();
                        $targetEntity->target = $result['id'];
                    }
                    $targetEntity->pharmacogenomic_provider_id = $provider->id;
                    $targetEntity->title = $result['name'];
                    $pharmacogenomicProviderProcessingTargetsTable->save($targetEntity);
                    $io->out('Target Saved');
                }
            }
        }
        $end = microtime(true);
        $duration = $end - $start;
        $io->out('Complete, took: ' . round($duration, 4) . 's');
    }
}