<?php
namespace PharmacogenomicProviders\Command;

use Cake\Console\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\ORM\TableRegistry;
use Labs\Libs\Lab;
use LabOrders\Libs\LabOrders;

/**
 * Class GetReportsCommand
 * @package PharmacogenomicProviders\Command
 */
class GetReportsCommand extends Command
{

    /**
     * Check PgX providers for updated data on patient and lab tests
     *
     * @param Arguments $args
     * @param ConsoleIo $io
     * @return int|void|null
     * @throws \Exception
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        if (is_null($labOrderId) || $labOrderId <= 0) {
            throw new \Exception('Specify a lab order');
        }

        $labOrders = new LabOrders;
        $environment = 'dev';

        if (!is_null($args->getArgumentAt(0)) && $args->getArgumentAt(0) === 'production') {
            $environment = 'production';
        }

        $labOrderReportId = $labOrders->getPgxReport($labOrderId, $environment);

        if ($labOrderReportId) {
            debug('PULLED IN!');
        } else {
            debug('FAILED TO PULL');
        }
    }
}
