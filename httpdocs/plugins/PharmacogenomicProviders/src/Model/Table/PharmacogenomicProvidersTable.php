<?php
namespace PharmacogenomicProviders\Model\Table;

use Cake\Validation\Validator;
use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class PharmacogenomicProvidersTable
 * @package PharmacogenomicProviders\Model\Table
 */
class PharmacogenomicProvidersTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->belongsTo(
            'Driver',
            [
                'className' => 'SystemManagement.Drivers',
                'foreignKey' => 'driver_id'
            ]
        );

        $this->belongsTo(
            'Connector',
            [
                'className' => 'SystemManagement.ConnectionTypes',
                'foreignKey' => 'connection_type_id'
            ]
        );

        $this->belongsTo(
            'AuthenticationType',
            [
                'className' => 'SystemManagement.AuthenticationTypes',
                'foreignKey' => 'authentication_type_id'
            ]
        );
    }

    /**
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        return $validator
            ->requirePresence('title')
            ->notEmpty('title', 'Name is required')
            ->requirePresence('driver_id')
            ->notEmpty('driver_id', 'Driver is required')
            ->requirePresence('connection_type_id')
            ->notEmpty('connection_type_id', 'Connection type is required')
            ->requirePresence('authentication_type_id')
            ->notEmpty('authentication_type_id', 'Authentication type is required')
            ->requirePresence('scheme')
            ->notEmpty('scheme', 'Scheme is required')
            ->requirePresence('host')
            ->notEmpty('host', 'Host is required')
            ->requirePresence('port')
            ->notEmpty('port', 'Port is required')
            ->requirePresence('username')
            ->notEmpty('username', 'Username is required')
            ->requirePresence('password', 'create')
            ->notEmpty('password', 'Password is required', 'create')
            ->requirePresence('confirm_password', 'create')
            ->notEmpty('confirm_password', 'Confirm Password is required', 'create')
            ->sameAs('password', 'confirm_password', 'Password and Confirm Password do not match', 'create')
            ;
    }
}
