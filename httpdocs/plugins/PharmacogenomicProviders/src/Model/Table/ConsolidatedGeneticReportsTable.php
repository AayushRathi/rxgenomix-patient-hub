<?php
namespace PharmacogenomicProviders\Model\Table;

use Lib24watch\Model\Table\Lib24watchTable;

/**
 * Class ConsolidatedGeneticReportsTable
 * @package PharmacogenomicProviders\Model\Table
 */
class ConsolidatedGeneticReportsTable extends Lib24watchTable
{
    /**
     * @param array $config
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Lib24watch.Author');

        $this->hasMany(
            'Drugs',
            [
                'className' => 'PharmacogenomicProviders.ConsolidatedGeneticReportDrugs',
                'foreignKey' => 'consolidated_genetic_report_id',
                'saveStrategy' => 'replace'
            ]
        );
    }

}
