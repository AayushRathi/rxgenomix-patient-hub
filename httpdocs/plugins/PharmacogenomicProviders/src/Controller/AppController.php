<?php
namespace PharmacogenomicProviders\Controller;

use Lib24watch\Controller\AppController as BaseController;

class AppController extends BaseController implements
    \CustomNamedPlugin,
    \PluginWithIcon,
    \PluginWithMigrations,
    \PluginWithPermissions
{
    public static function getPluginName()
    {
        return \Lib24watch\Model\Table\Lib24watchSettingsTable::readSettingStatic('title', 'PharmacogenomicProviders', 'Bioinformatics');
    }

    public static function getPluginIcon()
    {
        // FIXME does nothing
    }

    public static function getPluginMigrations()
    {
        // FIXME does nothing
    }

    public static function getAvailablePermissions()
    {
        return [
            'pharmacogenomic_providers_view' => 'View Bioinformatics Providers',
            'pharmacogenomic_providers_edit' => 'Edit Bioinformatics Providers'
        ];
    }
}
