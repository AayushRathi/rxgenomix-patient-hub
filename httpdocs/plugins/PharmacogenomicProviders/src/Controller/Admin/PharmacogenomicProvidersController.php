<?php
namespace PharmacogenomicProviders\Controller\Admin;

use PharmacogenomicProviders\Controller\AppController;

/**
 * Class PharmacogenomicProvidersController
 * @package PharmacogenomicProviders\Controller\Admin
 */
class PharmacogenomicProvidersController extends AppController
{

    /**
     * @var array
     */
    public $helpers = ['Paginator'];

    /**
     * @var array
     */
    public $paginate = [
        'PharmacogenomicProviders' => [
            'order' => [
                'created' => 'DESC'
            ]
        ]
    ];

    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('PharmacogenomicProviders.PharmacogenomicProviders');
    }

    /**
     * index
     */
    public function index()
    {
        $this->requirePluginPermission('pharmacogenomic_providers_view', 'PharmacogenomicProviders', 'You do not have permission to access this resource.');
        $query = $this->PharmacogenomicProviders->find();
        $this->paginate($query);
        $this->set('query', $query);
    }

    /**
     * @param int|null $pharmacogenomicProviderId
     * @throws \Exception
     */
    public function edit(int $pharmacogenomicProviderId = null)
    {
        $this->requirePluginPermission('pharmacogenomic_providers_edit', 'PharmacogenomicProviders', 'You do not have permission to access this resource.');
        if ($pharmacogenomicProviderId) {
            $entity = $this->PharmacogenomicProviders->get($pharmacogenomicProviderId, [
                'contain' => [
                ]
            ]);
        } else {
            $entity = $this->PharmacogenomicProviders->newEntity();
        }

        $this->set('pharmacogenomicProviderId', $pharmacogenomicProviderId);
        $this->set('pharmacogenomicProviderEntity', $entity);

        if($this->getRequest()->getData()) {
            $data = $this->getRequest()->getData();

            if (!isset($data['is_active'])) {
                $data['is_active'] = 0;
            }

            $entity = $this->PharmacogenomicProviders->patchEntity($entity, $data, [
                'contain' => [
                ]
            ]);

            $errors = $entity->getErrors();
            if (empty($errors)) {
                if (!$this->PharmacogenomicProviders->save($entity)) {
                    throw new \Exception('Could not save Lab');
                } else {
                    $this->redirectWithDefault(
                        [
                            'plugin' => 'PharmacogenomicProviders',
                            'controller' => 'PharmacogenomicProviders',
                            'action' => 'index',
                            'prefix' => 'admin',
                        ],
                        'Bio-Informatic Provider ' . ($pharmacogenomicProviderId) ? 'edited.' : 'added.'
                    );
                }
            }
        }

        $this->loadModel('SystemManagement.AuthenticationTypes');
        $this->set('authenticationTypes', $this->AuthenticationTypes->find('dropdown'));

        $this->loadModel('SystemManagement.ConnectionTypes');
        $this->set('connectionTypes', $this->ConnectionTypes->find('dropdown'));

        $this->loadModel('SystemManagement.Drivers');
        $this->set('drivers', $this->Drivers->find('dropdown'));
    }

    /**
     * @param int|null $pharmacogenomicProviderId
     * @throws \Exception
     */
    public function change_status(int $pharmacogenomicProviderId = null)
    {
        $this->requirePluginPermission('pharmacogenomic_providers_edit', 'PharmacogenomicProviders',
            'You do not have permission to edit Bioinformatic Providers');

        $entity = $this->PharmacogenomicProviders->get($pharmacogenomicProviderId);


        if ($entity->is_active) {
            $entity->is_active = 0;
            $text = 'active';
        } else {
            $entity->is_active = 1;
            $text = 'inactive';
        }

        if (!$this->PharmacogenomicProviders->save($entity)) {
            throw new \Exception("Couldn't update active status for Bioinformatic Provider with ID ({$pharmacogenomicProviderId})");
        }

        $this->Lib24watchLogger->write("Changed status of Bioinformatic Provider '" . $entity->title . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'PharmacogenomicProviders',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Bioinformatic Provider '" . $entity->title . "' is now " . $text
        );
    }

    /**
     * @param int|null $pharmacogenomicProviderId
     * @throws \Exception
     */
    public function delete(int $pharmacogenomicProviderId = null)
    {
        $this->requirePluginPermission('providers_delete', 'PharmacogenomicProviders',
            'You do not have permission to delete Bioinformatic Providers');

        $entity = $this->PharmacogenomicProviders->get($pharmacogenomicProviderId);

        if (!$this->PharmacogenomicProviders->delete($entity)) {
            throw new \Exception("Couldn't update active status for Bioinformatic Provider with ID ({$pharmacogenomicProviderId})");
        }

        $this->Lib24watchLogger->write("Deleted Bioinformatic Provider '" . $entity->title . "'");

        $this->redirectWithDefault(
            [
                'plugin' => 'SystemManagement',
                'controller' => 'PharmacogenomicProviders',
                'action' => 'index',
                'prefix' => 'admin',
            ],
            "Bioinformatic Provider '" . $entity->title . "' deleted."
        );
    }

}
