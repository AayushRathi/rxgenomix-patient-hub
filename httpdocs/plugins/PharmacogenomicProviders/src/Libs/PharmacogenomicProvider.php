<?php
namespace PharmacogenomicProviders\Libs;

use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * Class PharmacogenomicProvider
 * @package PharmacogenomicProviders\Libs
 */
class PharmacogenomicProvider
{

    /**
     * @var array|\Cake\Datasource\EntityInterface
     */
    public $pharmacogenomicProviderEntity;

    /**
     * @var
     */
    protected $driver;
    protected $connector;

    /**
     * Lab constructor.
     * @param int|null $pharmacogenomicProviderId
     * @throws \Exception
     */
    public function __construct(int $pharmacogenomicProviderId = null)
    {
        if (!$pharmacogenomicProviderId) {
            throw new \Exception('Pharmacogenomic Provider ID is required.');
        }
        $pharmacogenomicProviderTable = TableRegistry::getTableLocator()->get('PharmacogenomicProviders.PharmacogenomicProviders');

        $this->pharmacogenomicProviderEntity = $pharmacogenomicProviderTable->get($pharmacogenomicProviderId,
            [
                'contain' => [
                    'Driver',
                    'Connector',
                    'AuthenticationType'
                ]
            ]
        );

        if (!isset($this->pharmacogenomicProviderEntity->driver) || is_null($this->pharmacogenomicProviderEntity->driver)) {
            throw new \Exception('Pharmacogenomic driver not found');
        }

        if (!isset($this->pharmacogenomicProviderEntity->connector) || is_null($this->pharmacogenomicProviderEntity->connector)) {
            throw new \Exception('Pharmacogenomic connector not found');
        }

        if (!isset($this->pharmacogenomicProviderEntity->authentication_type) || is_null($this->pharmacogenomicProviderEntity->authentication_type)) {
            throw new \Exception('Pharmacogenomic authentication type not found');
        }

        $this->driver = new $this->pharmacogenomicProviderEntity->driver->class();
        $this->connector = new $this->pharmacogenomicProviderEntity->connector->class();
     }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @return mixed
     */
    public function getConnector()
    {
        return $this->connector;
    }
}
