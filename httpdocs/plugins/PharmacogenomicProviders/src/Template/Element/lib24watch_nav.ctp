<a href="javascript:void(0)"><span class="fa fa-warning"></span><span class="xn-text"><?php echo h($lib24watchModules['PharmacogenomicProviders']['title']); ?></span></a>
<ul>
    <?php
    if ($this->Lib24watchPermissions->hasPermission('pharmacogenomic_providers_view', 'PharmacogenomicProviders')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-list\"></span> Browse Bioinformatics Providers",
                [
                    'plugin' => 'PharmacogenomicProviders',
                    'controller' => 'PharmacogenomicProviders',
                    'action' => 'index',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Browse Bioinformatics Providers'
                ]
            );
            ?>
        </li>
        <?php
    }
    if ($this->Lib24watchPermissions->hasPermission('pharmacogenomic_providers_edit', 'PharmacogenomicProviders')) {
        ?>
        <li>
            <?php
            echo $this->Html->link(
                "<span class=\"fa fa-edit\"></span> Add Bioinformatics Provider",
                [
                    'plugin' => 'PharmacogenomicProviders',
                    'controller' => 'PharmacogenomicProviders',
                    'action' => 'edit',
                    'prefix' => 'admin'
                ],
                [
                    'escape' => false,
                    'title' => 'Add Bioinformatics Provider'
                ]
            );
            ?>
        </li>
    <?php
    }
    ?>
</ul>
